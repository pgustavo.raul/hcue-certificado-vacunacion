/* 
 *
 *@autor Luis Malquin
 */

(function($){     
    
    $.fn.extend({
       modalForm:function(options,arg){
           
          if(options && typeof(options)=='object'){                                    
              return this.each(function(){
                 if($(this).data('modalFormPlugin')===undefined){ 
                    // Crear una nueva instancia del plugin
                    // Pasar el elemento DOM y las opciones proporcionadas por el usuario como argumentos
                    var modalform=new $.modalForm(this,options);                   
                    
                    //Almacenar una referencia al objeto Plugin
                    //Posteriormente puede acceder al plugin y sus métodos y propiedades similares
                    $(this).data('modalFormPlugin',modalform);
                    
                  }
                 
              });             
          }else if(options && typeof(options)=='string'){
              return this.each(function(){
                   var instance = $(this).data('modalFormPlugin'); // Get instance
                   
                    if(instance===undefined){ // verificamos si existe una instancia
                        options = options || {};
                        instance=new $.modalForm(this,options);
                        $(this).data('modalFormPlugin',instance);
                    }              
                    if (options == 'show') {                        
                        $.modalForm.methods.show(this,instance.settings);
                    }
                    else if (options == 'hide') {
                        $.modalForm.methods.hide(this,instance.settings);
                    }                    
              });
                
          }else{
              return this.each(function(){
                    var instance = $(this).data('modalFormPlugin'); // Get instance
                    if(instance===undefined){ // verificamos si existe una instancia
                        options = options || {};
                        this.data('modalFormPlugin',new $.modalForm(this,options));
                    }                    
              });
          }           
       } 
    }); 
    
    
    //Definicion del plugin modelform
    $.modalForm=function(element,options){
        
        //Defaults
        var defaults={
                   //url de carga de la vista
                   url:'',                   
                   //titulo del dialog
                   title:'',          
                   //id del Grid
                   grid:null,
                    //indica si el modal se muestra automaticamente despues ce haberse construido
                   autoOpen:false,
                   //ancho del modal
                   width:'',
                   //alto del modal
                   height:'',
                   //alineación horizontal del modal
                   align:'center', //center,left,right
                   //alineacion vertical
                   verticalAlign:'top', //top, middle
                   //Botones de accion
                   buttons:{
                       Cancel: {caption:'Cancelar','class':'',click:null},
                       Submit: {caption:'Guardar','class':'',click:null}
                   },
                   //Indica si se debe validar el formulario
                   validForm:true,
                   //Url de envio para el submit del formulario
                   urlSubmit:'',                   
                   //metodo que se ejecuta cuando se carga la vista o peticion
                   onLoad:function(){},
                   //metodo que se ejecuta antes de abrir el dialog
                   onBeforeOpen:function(){},
                   //metodo que se ejecuta cuando se abre el dialog
                   onOpen:function(){},
                   //metodo que se ejecuta antes de enviar el formulario                   
                   onBeforeSend:function(){},             
                   //metodo a ejecturse cuando se realiza el submit del formulario
                   onSubmit:function(){},
                   //metodo que se ejecuta cuando se obtuvo una respuesta satisfactoria del submit
                   onSuccess:function(){}    ,
                   //metodo que se ejecuta cuando se cierra el modal
                    onClose:function(){}
                   
                 };                        
                  
         var plugin=this;
         
         plugin.settings={};

         
         // El método "constructor" que se llama cuando se crea el objeto
         plugin.init=function(){
             // propiedades finales del plugin, predeterminados y fusionados
             // opciones proporcionadas por el usuario (si los hay)
             plugin.settings=$.extend({},defaults,options);               
         }                          
        
         
        plugin.init();
        
        //verificamos si se requiere abrir el modal automaticamente
         if(plugin.settings.autoOpen){
             $.modalForm.methods.show(element,this.settings);
         }
        
    };
  
    $.modalForm.methods={
                //inicializa el modalForm
                initUI:function(elem,options){            
                    var modaldialog=$('<div>',{'class':'modal-dialog-form modal-dialog'});
                    var modalcontent=$('<div>',{'class':'modal-content'});
                    var modalheader=$('<div>',{'class':'modal-header'});
                    var modalbody=$('<div>',{'class':'modal-body'});
                    var modalmessage=$('<div>',{'class':'modal-message hide'});
                    var modalform=$('<div>',{'class':'modal-form'});
                    var modalfooter=$('<div>',{'class':'modal-footer hide'});
                    var modalloading=$('<div>',{'class':'modal-loading center hide'}).css({'width':'100%'});
                    
                    //align
                    if(options.align!='')$(modaldialog).addClass('pull-'+options.align);
                    
                    //haader
                    modalheader.html('<button type="button" class="close" data-dismiss="modal">&times;</button>');
                    if(options.title!=''){
                        modalheader.append('<h4 class="blue bigger">'+options.title+'</h4>');
                    }     
                    
                    //asignamos el contenido del elemento div a la seccion del form
                    modalform.html($(elem).html());
                    
                    //body
                    modalloading.append('<div class="fa fa-spinner fa-2x fa-spin orange"></div>');                                        
                    modalbody.append(modalloading);
                    modalbody.append(modalmessage).append(modalform);
                    
                    
                    //footer buttons
                    if(typeof(options.buttons)=='object'){                        
                        var btnCancel=$("<button>",{'class':'btn btn-sm','data-dismiss':'modal'});
                        var btnSubmit=$("<button>",{'class':'btn btn-sm btn-primary'});                       
                        
                        btnCancel.html('<i class="ace-icon fa fa-times"></i>'+options.buttons.Cancel.caption);
                        btnSubmit.html('<i class="ace-icon fa fa-check"></i>'+options.buttons.Submit.caption);                
                        
                        //evento click submit
                        btnSubmit.click(function(){
                            $.modalForm.methods.submit(elem,options);
                        });
                        modalfooter.html(btnCancel).append(btnSubmit);
                    }
                    
                    //modal content
                    modalcontent.html(modalheader).append(modalbody).append(modalfooter);
                    //modal dialog
                    modaldialog.html(modalcontent);                   
                    //propiedades del modal
                    $(elem).attr({'data-backdrop':'static','data-keyboard':false,'tabindex':'-1','role':'dialog'})
                            .css({'overflow-y':'visible'});  
                    
                    
                    $(elem).addClass('modal fade').html(modaldialog);
                    
                    
                },
                //muestra el modalForm
                show:function(modal,options){
                    
                    //iniciamos UI del dialogForm
                    $.modalForm.methods.initUI(modal,options);
                    
                    //Verificamos si el paŕametro es una funcion y la llamamos
                    if($.isFunction(options.onBeforeOpen)){
                        if(options.onBeforeOpen.call(modal)===false){
                            return false;
                        }
                    }            
                    //Verificamos si se define la url de carga del form
                    //caso contrario inicializa el contenido del div
                    
                    if(options.url){
                        //Cargamos el formulario
                        $.modalForm.methods.showLoading(modal);
                        $('.modal-form',modal).load(options.url,{},function(response, status, xhr){
                             
                             //verificamos si se produjo un error
                             if(status=='error'){
                                 //agregamos el mensaje en la cabecera del contenido
                                 var message = xhr.responseText;
                                 if(xhr.status != undefined && xhr.status != 401){
                                     message = xhr.statusText;
                                 }
                                 $.modalForm.methods.showError(modal,message,xhr.status);
                                 
                             }else{
                                //Verificamos si el parámetro es una funcion y la llamamos 
                                if ( $.isFunction( options.onLoad ) ) {
                                    options.onLoad.call(modal);
                                }
                                //mostramos lo botones del formulario                                
                                $.modalForm.methods.showFooter(modal);
                            }
                            //Ocultamos el icono de carga
                            $.modalForm.methods.hideLoading(modal);
                            
                            //aplicamos la validacion al formulario
                            var form=$('form',modal).get(0);
                            $.modalForm.methods.applyValidationForm(form);
                            //centrado vertical
                            if(options.verticalAlign=='middle'){
                               setTimeout(function(){$.modalForm.methods.verticalPosition(modal)},50);
                            }
                            
                        });
                    }else{
                        
                         var form=$('form',modal);
                         if(form.length){
                            //Verificamos si el parámetro es una funcion y la llamamos 
                            if ( $.isFunction( options.onLoad ) ) {
                                    options.onLoad.call(modal);
                            } 
                            //mostramos lo botones del formulario                                
                            $.modalForm.methods.showFooter(modal); 
                            //aplicamos la validacion al formulario
                            $.modalForm.methods.applyValidationForm(form.get(0));
                            
                         }
                    }
                    
                    //Verificamos si el parámetro es una funcion y inicializamos el metodo show del modal
                    if($.isFunction(options.onOpen)){
                        $(modal).on('show.bs.modal',function(e){
                            options.onOpen.call(modal);                                                                                                                                          
                            //centrado vertical
                            if(options.verticalAlign=='middle'){
                                $.modalForm.methods.verticalPosition(modal);
                            }
                            
                        }); 
                    }   
                    
                    
                    //inicializamos evento hide para que elimine el modal
                     $(modal).on('hidden.bs.modal', function(){
                         if($.isFunction(options.onClose)){
                             if(options.onClose.call(modal)===false){
                                 return false;
                             }
                         }

                          $(this).remove();                          
                    });                    
                    
                    //definicion opciones del modal
                    var modalcss={};
                    if(options.width){
                        modalcss['width']=options.width;   
                    }
                    if(options.height){
                        modalcss['height']=options.height;   
                    }
                    
                    //mostrar modal                    
                     $(".modal-dialog",modal).css(modalcss); 
                     $(modal).modal('show');  
                    

                },
                //oculta el modalForm
                hide:function(modal){
                    $(modal).modal('hide');
                }, 
                verticalPosition: function(modal){
                    var dialog = $('.modal-dialog',modal);                             
                    var initModalHeight = dialog.outerHeight();
                    var userScreenHeight = $(document).outerHeight();
                    
                    
                    if (initModalHeight > userScreenHeight) {
                        dialog.css('overflow', 'auto'); 
                    } else {
                         
                        //var  offset       = ($(window).height() - dialog.height()) / 2,
                         var  offset       = ($(window).outerHeight() - dialog.outerHeight()) / 2,
                         bottomMargin = parseInt(dialog.css('marginBottom'), 10);

        
                        if(offset < bottomMargin) offset = bottomMargin;
                        dialog.css("margin-top", offset);
                    }
                     
                },
                applyValidationForm:function(form){                    
                    $(form).initInputsForm().validationForm();
                },
                validForm:function(form,options){                     
                     if(form.length){
                        return $(form).valid();                     
                     }
                     return false;
                },
                ajaxSubmitForm:function(modal,form,options){
                   
                    var urlSubmit=($(form).attr('action'))?$(form).attr('action'):'';
                    if(options.urlSubmit){
                        urlSubmit=options.urlSubmit;
                    }
                    $.ajax({
                       url:urlSubmit,
                       data:$(form).serialize(),
                       type:'POST',                       
                       dataType:'json',
                       async:false,
                       beforeSend:function(){                            
                             $.modalForm.methods.showProgressBar(modal);
                       },
                       success:function(response){ 
                           
                           var error='';
                            if(response==0 || response==1){
                                if (!parseInt(response)) {                                    
                                    error+='Error';
                                }else {                                    
                                    if(options.idGrid){
                                        $.modalForm.methods.reloadGrid(options.idGrid);
                                    }
                                }
                            }else {                                                                                   
                                if(response.status>0){                                                                      
                                  if(options.idGrid){
                                        $.modalForm.methods.reloadGrid(options.idGrid);
                                  }                                 
                                }
                                else{   
                                    
                                    error+=(response.message)?response.message:'-';
                                }
                                
                            }                            
                            $.modalForm.methods.hideProgressBar(modal);
                            
                            if(error){ 
                                $.modalForm.methods.showError(modal,error);
                            }else{
                                $.modalForm.methods.hideError();
                                //verificamos si existe una funcion una vez obtenida una respuesta y ejecutamos
                                if($.isFunction(options.onSuccess))options.onSuccess.call(response);
                                $.modalForm.methods.hide(modal);
                                
                                //verificamos si existe un grid definido y lo recargamos
                                if(options.grid){
                                   $.modalForm.methods.reloadGrid(options.grid);
                                }                                
                                
                                //vaerificamos si existe un mensaje de respuesta y lo mostramos
                                if(response.message!=undefined){
                                    if(response.message){
                                         $.gritter.add({
                                                title: 'Información!',                                               
                                                text: response.message,
                                                time:3000,
                                                class_name: 'gritter-success gritter-light'
                                        });
                                    }                                    
                                }
                                
                            }                          
                            
                       },
                       // error:function(xhr, ajaxOptions, thrownError){
                       //     $.modalForm.methods.hideProgressBar(modal);
                       //     var message=xhr.status + " " + xhr.statusText;
                       //     $.modalForm.methods.showError(modal,message);
                       // }
                    });                   
                    
                },
                submit:function(modal,options){
                          
                    var form=$('form',modal);                    
                    //limpiar espacio en los inputs del formulario antes de enviar
                    $("input[type=text],textarea",form).each(function(){
                         $(this).val($.trim(this.value))
                    });
                                        
                     //Llamado de metodo antes de enviar
                    if($.isFunction(options.onBeforeSend)){
                        if(options.onBeforeSend.call(modal)===false){
                            return false;
                        }
                    }
                   
                    if($.isFunction(options.buttons.Submit.click)){                        
                        if(options.buttons.Submit.click.call(form,modal)===false){
                            return false;
                        }
                    }else if(options.validForm){
                           //validar y enviar form
                            if($.modalForm.methods.validForm(form,options)){
                                $.modalForm.methods.ajaxSubmitForm(modal,form,options)
                            }                         
                    }else{
                        //Enviar form
                       $.modalForm.methods.ajaxSubmitForm(modal,form,options);
                       
                    }                   
                    
                },
                reloadGrid:function(grid){                     
                    
                        var objGrid=null;
                        if(typeof(grid)=='string'){
                            objGrid=$(grid).dataTable();
                        }else if(typeof(grid)=='object'){
                            objGrid=grid;
                        }
                        if(objGrid){
                            objGrid.ajax.reload();
                        }                   
                                       
                },
                showError:function(modal,error,code){
                    var message = error;
                    var clase = 'info';

                    if(code != undefined && code != 401){
                        clase = 'danger';
                        message="<strong>Se ha producido un error:</strong> " +code+' '+error
                    }

                    $('.modal-message',modal).removeClass('hide').html('<div class="alert alert-'+clase+'">'+message+'</div>');
                },
                hideError:function(modal){
                    $('.modal-message',modal).addClass('hide');
                },
                showProgressBar:function(modal){
                    //$('.modal-progress',modal).removeClass('hide');                    
                    $(modal).addClass('hide');
                    $.processWait.show();                    
                },
                hideProgressBar:function(modal){
                    //$('.modal-progress',modal).addClass('hide');
                    $(modal).removeClass('hide');
                     $.processWait.hide();           
                },
                showLoading:function(modal){
                    $('.modal-loading',modal).removeClass('hide');
                },
                hideLoading:function(modal){
                     $('.modal-loading',modal).addClass('hide');  
                },
                showFooter:function(modal){
                    $('.modal-footer',modal).removeClass('hide');  
                },
                hideFooter:function(modal){
                    $('.modal-footer',modal).addClass('hide');  
                }
                
                
         };  
 
})(jQuery);



/**
 * Archivo de inicializacion de eventos y plugins de ayudante para formularios
 * Requiere:
 *      -jQuery
 *      -jquery.includeMany-1.2.2.js
 *      -helpers.main.js
 * Fecha de creacion: 04/08/2016
 * Fecha de modificación: 04/08/2016       
 * Creado por: Luis Malquin (luisor_mlt@hotmail.com)
 * Modificado por: Luis Malquin
 **/

(function ( $ ) {
    
    /* Aplica el plugin de limiter a los input con las clase ".limited" que se encuentran dentro de un formulario,
     * muestra información acerca del número de caracteres ingresados y número de caracteres permitidos 
     * Aplicacion form: $('form').inputLimiterForm();
     * Aplicacion input: <input type='text' class='limited' maxlength=100 />  
     * @returns object form
     */
    $.fn.inputLimiterForm=function(){        
        
        var selector=$('.limited',this);
        
        selector.includeFileExistObjects('jquery.inputlimiter',
            function(){
                 selector.inputlimiter({
                    remText: '%n caracteres restantes...',
                    limitText: 'Máximo permitido: %n.'
                 });  
            }
        );
    };   
     
    /*
     * Aplica el plugin de máscara del o los inputs definida por el atributo mask 
     * que se encuentran dentro de un formulario
     * Aplicacion: $('form').inputMaskForm()  
     * Atributo input: <input type='text' mask='(999) 999-9999' />     
     * @returns object form
     */
    $.fn.inputMaskForm=function(){        
        var selector=$("input[mask]",this);
        
        selector.includeFileExistObjects('jquery.maskedinput',function(){                        
            $.mask.definitions['~']='[+-]';        
            selector.each(function(){                
                $(this).mask($(this).attr("mask"));
            });                                   
        });        
                       
    };
    
    /*
     * Aplica el plugin de máscara de moneda para los inputs con la clase .money     
     * Aplicacion form: $('form').inputMaskForm()  
     * Atributo input: <input type='text' mask='(999) 999-9999' />     
     * @returns object form
     */
    $.fn.inputMaskMoneyForm=function(){        
        var selector=$(".currency",this);
        
        selector.includeFileExistObjects('jquery.maskMoney',function(){            
            selector.maskMoney('destroy');
            selector.each(function(){                                
                $(this).maskMoney({
                    decimal: ".",
                    thousands: "",
                    precision: 2
                });
            });                                   
        });        
                       
    };
    
    /*
     * Convierte el texto o valida el ingreso de información , tambien aplica el plugin alphanum a los objetos de un formulario
     * Aplicacion form: $('form').inputAlphanumForm()
     * Aplicacion inputs:
     *      <input type='text' class='upper' />
     *      <input type='text' class='lower' />
     *      <input type='text' class='capitalize' />
     *      <input type='text' class='first-capitalize' />      
     *      <input type='text' class='alpha' />
     *      <input type='text' class='alpha' />
     *      <input type='text' class='alphanumeric' />
     *      <input type='text' class='numeric' />
     *      <input type='text' class='decimal' />
     *      <input type='text' class='alphaspace' />
     *      <input type='text' class='alphanumericspace' />     
     *                 
     * @returns object form
     */
    $.fn.inputAlphanumForm=function(){
        var $form=this;
        
        //transformacion de textos        
        /*
         * Convierte todo el texto introducido en cuadro de texto en mayúsculas    
         * HOLA MUNDO CRUEL
         */        
        $(".upper",$form).each(function(){
            $(this).keyup(function(){
                $(this).val($(this).val().toUpperCase());
            })
        });
	
        /*
         * Convierte todo el texto introducido en cuadro de texto en minusculas    
         * hola mundo cruel
         */        
        $(".lower",$form).each(function(){
            $(this).keyup(function(){
                $(this).val($(this).val().toLowerCase());
            })
        });
        
        /*
         * Capitaliza la primera cadena de caracteres en mayúsculas
         * Ejemplo: Hola mundo cruel
         */        
        $(".capitalize",$form).each(function(){
            $(this).keyup(function() {
		var caps = $(this).val(); 
		caps = caps.charAt(0).toUpperCase() + caps.slice(1);
                $(this).val(caps);                
            });
        });
	
        /*
         * Capitaliza el primer caracter de cada palabra en mayúsculas
         * Ejemplo: Hola Mundo Cruel
         */        
         $(".first-capitalize",$form).each(function(){
             $(this).keyup(function(){
                var str = $(this).val();       		
		var spart = str.split(" ");
		for ( var i = 0; i < spart.length; i++ )
		{
			var j = spart[i].charAt(0).toUpperCase();
			spart[i] = j + spart[i].substr(1);
		}
                $(this).val(spart.join(" "));
             });
         });        
        
        //Valida el ingreso de textos alfanumericos
        var selectors=$(".alpha,.alphanumeric,.numeric,.decimal,.alphaspace,.alphanumericspace,.letterswithbasicpunc",$form);
        selectors.includeFileExistObjects('jquery.alphanum',function(){
            selectors.each(function(){
                var obj=$(this);
                //alpha.- Permite le ingreso de solo letras
                if(obj.hasClass('alpha')){
                    obj.alpha({
                        allowSpace: false, 
                        allowUpper: true  
                    });
                }
                //alphanumeric.- Permite el ingreso de solo letras y numeros  
                if(obj.hasClass('alphanumeric')){
                    obj.alphanum({
                        allowSpace: false, 
                        allowUpper: true  
                    });
                }
                //letterswithbasicpunc.- Permite el ingreso de solo letras con espacios y signos de puntuacion
                if(obj.hasClass('letterswithbasicpunc')){                   
                    obj.alpha({
                        allow:'.-',
                        allowSpace: true, 
                        allowUpper: true  
                    });
                }
                
                
                //numeric.- Permite le ingreso de solo numeros enteros
                if(obj.hasClass('numeric')){
                    obj.numeric("integer");
                }
                //decimal.- permite el ingreso de numero con decimales
                if(obj.hasClass('decimal')){
                    obj.numeric({
                        allowThouSep: false,
                        allowDecSep:  true
                    });
                }
                //alphaspace.- permite el ingreso de solo letras con espcacios
                if(obj.hasClass('alphaspace')){
                    obj.alpha({
                        allowSpace: true, 
                        allowUpper: true  
                    });
                }
                //alphanumericspace.- permite el ingreso de solo letras y numeros con espacios
                 if(obj.hasClass('alphanumericspace')){
                    obj.alphanum({
                        allowSpace: true, 
                        allowUpper: true  
                    });
                }
                
                
            });
        });            
        
    }
    
    /*
     * Aplica los plugin date-picker, datetime-picker, timer-picker, date-range-picker a los inputs definida por las clases
     * y que se encuentran dentro de un formulario
     * Aplicacion form: $('form').inputDatePickerForm()  
     * Aplicacion input: 
     *          <input type='text' class='date-picker' />     
     *          <input type='text' class='datetime-picker' />     
     *          <input type='text' class='time-picker' />     
     *          <input type='text' class='date-range-picker' />     
     * @returns object form
     */
    $.fn.inputDatePickerForm=function(){
         var $form=this;
         
          //implementacion date-picker
         $('.date-picker',$form).includeFileExistObjects('bootstrap.datepicker',function(){
                    $('.date-picker',$form).each(function(){
                         var startDate=$(this).attr('minDate');
                         var endDate=$(this).attr('maxDate');
                         var val=$(this).val();
                         
                         $(this).datepicker({
                         autoclose: true,
                         todayHighlight: true,
                         language: 'es',
                         startDate:(startDate)?startDate:null,
                         endDate:(endDate)?endDate:null
                         }).next().on('click', function(){
                            $(this).prev().focus();
                         }); 
                         
                         if(val){
                             $(this).datepicker("setDate", val);
                         }
                    });
         });           
         
         //Se incluye la libreria moment necesaria para datetimepicker y daterangepicker
        // $('.datetime-picker,.date-range-picker',$form).includeFileExistObjects('moment',function(){
             
                //implementacion datetime-picker
                $('.datetime-picker',$form).includeFileExistObjects('bootstrap.datetimepicker',function(){
                    
                       $('.datetime-picker',$form).each(function(){
                           
                                var minDate=$(this).attr('minDate');
                                var maxDate=$(this).attr('maxDate');
                                
                                $(this).datetimepicker({
                                    //format: 'YYYY/MM/DD h:mm A',//use this option to display seconds                                    
                                    icons: {
                                           time: 'fa fa-clock-o',
                                           date: 'fa fa-calendar',
                                           up: 'fa fa-chevron-up',
                                           down: 'fa fa-chevron-down',
                                           previous: 'fa fa-chevron-left',
                                           next: 'fa fa-chevron-right',
                                           today: 'fa fa-arrows ',
                                           clear: 'fa fa-trash',
                                           close: 'fa fa-times'
                                    }
                                }).next().on('click', function(){
                                    $(this).prev().focus();
                                });
                                
                                if(minDate && minDate!='undefined'){
                                    $(this).datetimepicker( "option", "minDate",moment(minDate)); 
                                }
                                if(maxDate && maxDate!='undefined'){                                   
                                    $(this).datetimepicker( "option", "maxDate",moment(maxDate)); 
                                    
                                }
                                
                       });                
                       
                });
                
                 //implementacion date-range-picker
                $('.date-range-picker',$form).includeFileExistObjects('daterangepicker',function(){
                       $('.date-range-picker',$form).daterangepicker({
                               'applyClass' : 'btn-sm btn-success',
                               'cancelClass' : 'btn-sm btn-default',
                               locale: {
                                       applyLabel: 'Aplicar',
                                       cancelLabel: 'Cancelar',
                               }
                       })
                       .prev().on('click', function(){
                               $(this).next().focus();
                       });
                });    
         //});                   
                      
        
        //implementacion time-picker
         $('.time-picker',$form).includeFileExistObjects('bootstrap.timepicker',function(){
               $('.time-picker',$form).timepicker({
                      minuteStep: 1,
                      showSeconds: true,
                      showMeridian: false,
                      disableFocus: true,
                      icons: {
                              up: 'fa fa-chevron-up',
                              down: 'fa fa-chevron-down'
                      }
              }).on('focus', function() {
                      $(this).timepicker('showWidget');
              }).next().on('click', function(){
                      $(this).prev().focus();
              });
         });                
                              
    }
    
       
     /*
     * Aplica el plugin de validacion de inputs de formularios 
     * Aplicacion form: $('form').validationForm({options})       
     * @returns object form
     */
    $.fn.validationForm=function(rules,messages,callback){
        var $form=this       
        
        $.include_once('jquery.validate',function(){                                                                  
            //Inicializamos la validación del formulario
            $.each($form,function(){
                    $(this).validate({
                            errorElement: 'div',
                            errorClass: 'help-block',
                            focusInvalid: true,
                            ignore: "",
                            messages: (messages) ? messages:{},
                            rules: (rules) ? rules:{},
                            highlight: function (e) {
                                    $(e).closest('.form-group').removeClass('has-info').removeClass('has-success').addClass('has-error');                            

                            },

                            success: function (e,el) {
                                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');                            
                                    $(e).remove();
                            },

                            errorPlacement: function (error, element) {
                                    if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                                            var controls = element.closest('div[class*="col-"]');
                                            if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                                            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                                    }
                                    else if(element.is('.select2')) {
                                            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                                    }
                                    else if(element.is('.chosen-select')) {
                                            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                                    }
                                    else {
                                        //form vertical
                                        //error.insertAfter(element.parent());
                                        //form horizontal    
                                        var parent=element.parent();
                                        if($(parent).hasClass('input-group')){
                                            error.insertAfter(parent);
                                        }else{
                                             $(parent).append(error);
                                        }
                                    }


                            },
                            invalidHandler: function (form,validations) {
                                    var errors = validations.numberOfInvalids();
                                    if (errors>0) {
                                            var invalidElements = validations.invalidElements();
                                            var firstelem = invalidElements[0];
                                            // Encuentra la pestaña padre del elemento que está en el interior y obtiene el id
                                            var $closest = ($(firstelem).closest('.tab-pane').length) ? $(firstelem).closest('.tab-pane') : $(firstelem).closest('.ui-tabs-panel');

                                            if($closest.length){                                    
                                                var id = $closest.attr('id'); 
                                                // Buscar el enlace que corresponde al panel y lo muestra o activa
                                                $('.nav a[href="#'+id+'"]').tab('show');
                                                $('.ui-tabs-nav a[href="#'+id+'"]').trigger('click');
                                            }
                                    }
                            },
                            submitHandler:function(form){
                                //limpiar espacio en los inputs del formulario antes de enviar
                                $("input[type=text],textarea",form).each(function(){
                                    $(this).val($.trim(this.value))
                                });
                                if($(form).valid()){
                                    form.submit();
                                }
                                return false;
                            }
                    });              
                }); 
                if(typeof callback!='undefined')callback();
        });                            
        return this;
    };
    
     /* Aplica los plugins necesarios para el ingreso da la informacion en los objetos que se encuentran dentro de un formulario,
     * la aplicación de los plugins se realiza por medio de una clase o un atributo
     * Aplicacion form: $('form').initInputsForm()
     * Aplicacion input: 
     *        <input type='text' class='limited' maxlength=100 /> 
     *        <input type='text' mask='(999) 999-9999' /> 
     *        <input type='text' class='upper' >
     *        <input type='text' class='lower' >
     *        <input type='text' class='capitalize' >
     *        <input type='text' class='first-capitalize' >
     *        <textarea class="autosize">
     *        <select class='chosen-select'>
     *        
     * @returns object form
     */
    $.fn.initInputsForm = function(callback) {
        
        var $form=this;
        
        $("input,textarea",$form).on("drop",function(){
            return false; 
        });
        
        //incializo tooltips y pophover de los input en el form
        $('[data-rel=tooltip]',$form).tooltip();
        $('[data-rel=popover]',$form).popover({html:true});
        
        //añade el * para los campos requeridos      
        $('.asterisk',$form).remove();
        
        $(".form-group .control-label.required",$form).each(function(){                       
            $(this).append('<i style="color:red" class="asterisk">*</i>');
        });                                
                
                        
        $($form).inputLimiterForm();
        $($form).inputMaskForm();
        
        //autosize text area
        $('textarea.autosize',$form).includeFileExistObjects('autosize',function(){             
            autosize($('textarea.autosize',$form));
        });
        
        //knob  
        
        $('.knob',$form).includeFileExistObjects('jquery.knob',function(){
            $(".knob",$form).knob();
        });
        
        //Choosen select
        $('.chosen-select',$form).includeFileExistObjects(['jquery.chosen','themecss'],function(){            
            $('.chosen-select',$form).chosen({allow_single_deselect:true});             
            $(window)
                .off('resize.chosen')
                .on('resize.chosen', function() {
                        $('.chosen-select').each(function() {
                                 var $this = $(this);
                                 //$this.next().css({'width': $this.parent().width()});
                                 $this.next().css({'width': '100%'});
                        })
                }).trigger('resize.chosen');
            
        });
        
        //colorpicker
        $('.color-picker',$form).includeFileExistObjects('bootstrap.colorpicker',function(){
            $('.color-picker',$form).colorpicker() ;
            //$('.color-picker').last().css('z-index', 2000);//if colorpicker is inside a modal, its z-index should be higher than modal'safe
        });
        
        //inicializa los inputs tipo date y tiempo
        $($form).inputDatePickerForm();                                
             
        //inicializa la mascara de moneda 
        $($form).inputMaskMoneyForm();                                
             
               
        //inicializa los inputs para caracteres alfabeticos, numericos,alfanumericos,decimal, etc.
        $($form).inputAlphanumForm();
        
        if(typeof callback!='undefined')callback();
        return this;
    };
    
    //Crea la peticion ajax y carga los resultados en los combos dependientes o en cascada
    $.fn.cascadeCombobox = function(options) {

            var obj = this;
            var optionsdefault = {parents_id: '', id: $(this).attr('id'), using: 'id,descripcion', 
                urlLoad: '#',onLoad:null, method:'GET',data:{}
            };
            options = $.extend(optionsdefault, options);


            if (options.parents_id) {
                var strparents = '';
                var parentsparts = options.parents_id.split(',')


                if (parentsparts.length > 0) {
                    $.each(parentsparts, function() {
                        strparents += (strparents) ? ',' : '';
                        strparents += '#' + this;
                    });
                }

                var parentobjs = $(strparents);
                var defaultoption = '';
                $.each($('option', obj), function() {
                    if ($(this).attr('value') === '') {
                        defaultoption = this;
                    }
                });
                $(obj).after("<i id='load_" + options.id + "' class='fa fa-spinner fa-spin orange bigger-110' style='display:none'></i>");
                $(strparents).change(function() {
                    $(obj).empty();
                    if (defaultoption) {
                        $(obj).append(defaultoption);
                    }
                    if (parentobjs.length) {
                        var countvaluesparent = 0;
                        var strvalues = '';
                        $.each(parentobjs, function() {
                            var value = $(this).val();
                            if (value)
                                countvaluesparent++;
                            strvalues += (strvalues) ? '/' : '';
                            strvalues += value;
                        });
                        var using = options.using.split(',');

                        if (countvaluesparent == parentobjs.length) {
                            $.ajax({
                                url: options.urlLoad + '/' + strvalues,
                                type: options.method,
                                dataType: 'json',
                                aync: false,
                                data:options.data,
                                contentType: 'application/json; charset=utf-8',
                                success: function(json) {

                                    if (json.length > 0) {
                                        var item=null,strattr='';
                                        if ($.isArray(json)) {
                                            for (var i = 0; i < json.length; i++) {
                                                item = json[i];
                                                strattr = '';
                                                if (item['attr'] != undefined) {
                                                    $.each(item['attr'], function(key, value) {
                                                        strattr += " " + key + "=" + value;
                                                    });
                                                }
                                                if (item[using[0]] != undefined && item[using[1]] != undefined) {

                                                    $(obj).append('<option value=\"' + item[using[0]] + '\" ' + strattr + '>' + item[using[1]] + '</option>');
                                                }
                                            }
                                        } else {
                                            item = json;
                                            strattr = '';
                                            if (item['attr'] != undefined) {

                                                $.each(item['attr'], function(key, value) {
                                                    strattr += " " + key + "=" + value;
                                                });
                                            }
                                            if (item[using[0]] != undefined && item[using[1]] != undefined) {
                                                $(obj).append('<option value=\"' + item[using[0]] + '\" ' + strattr + '>' + item[using[1]] + '</option>');
                                            }
                                        }
                                        if(options.onLoad){                                    
                                            options.onLoad();
                                        }
                                    }
                                    obj.change();
                                    $('#load_' + options.id).hide();
                                },
                                beforeSend: function() {
                                    $('#load_' + options.id).css('display', 'inline-block');

                                },
                                // error: function(xhr, ajaxOptions, thrownError) {
                                //
                                //     $.alert(thrownError ,'danger');
                                //     $('#load_' + options.id).hide();
                                // }
                            });
                        } else {
                            obj.change();
                        }
                    }
                });
            }
        }
    
    
}( jQuery ));


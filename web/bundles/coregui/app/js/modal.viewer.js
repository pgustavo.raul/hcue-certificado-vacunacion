/* 
 *
 *@autor Luis Malquin
 */

(function($){     
    
    $.fn.extend({
       modalViewer:function(options,arg){
           
          if(options && typeof(options)=='object'){                                    
              return this.each(function(){
                 if($(this).data('modalViewerPlugin')===undefined){ 
                    // Crear una nueva instancia del plugin
                    // Pasar el elemento DOM y las opciones proporcionadas por el usuario como argumentos
                    var modalviewer=new $.modalViewer(this,options);                   
                    
                    //Almacenar una referencia al objeto Plugin
                    //Posteriormente puede acceder al plugin y sus métodos y propiedades similares
                    $(this).data('modalViewerPlugin',modalviewer);
                    
                  }
                 
              });             
          }else if(options && typeof(options)=='string'){
              return this.each(function(){
                   var instance = $(this).data('modalViewerPlugin'); // Get instance
                   
                    if(instance===undefined){ // verificamos si existe una instancia
                        options = options || {};
                        instance=new $.modalViewer(this,options);
                        $(this).data('modalViewerPlugin',instance);
                    }              
                    if (options == 'show') {                        
                        $.modalViewer.methods.show(this,instance.settings);
                    }
                    else if (options == 'hide') {
                        $.modalViewer.methods.hide(this,instance.settings);
                    }                    
              });
                
          }else{
              return this.each(function(){
                    var instance = $(this).data('modalViewerPlugin'); // Get instance
                    if(instance===undefined){ // verificamos si existe una instancia
                        options = options || {};
                        this.data('modalViewerPlugin',new $.modalViewer(this,options));
                    }                    
              });
          }           
       } 
    }); 
    
    
    //Definicion del plugin modelform
    $.modalViewer=function(element,options){
        
        //Defaults
        var defaults={
                   //url de carga de la vista
                   url:'',                   
                   //titulo del dialog
                   title:'',   
                   //indica si el modal se muestra automaticamente despues ce haberse construido
                   autoOpen:false,
                    //ancho del modal
                   width:'',
                   //alto del modal
                   height:'',
                    //alineación horizontal del modal
                   align:'center', //center,left,right
                   //alineacion vertical
                   verticalAlign:'top', //top, middle
                   //Botones de accion
                   buttons:{
                       Close: {caption:'Cerrar','class':'',click:null},                       
                   },                   
                   //metodo que se ejecuta cuando se carga la vista o peticion
                   onLoad:function(){},
                   //metodo que se ejecuta antes de abrir el dialog
                   onBeforeOpen:function(){},
                   //metodo que se ejecuta cuando se abre el dialog
                   onOpen:function(){},
                   //metodo que se ejecuta antes de enviar el formulario                   
                   onBeforeSend:function(){},
                   //metodo que se ejecuta cuando se cierra el modal
                   onClose:function(){}
                   
                 };                        
                  
         var plugin=this;
         
         plugin.settings={};

         // El método "constructor" que se llama cuando se crea el objeto
         plugin.init=function(){
             // propiedades finales del plugin, predeterminados y fusionados
             // opciones proporcionadas por el usuario (si los hay)
             plugin.settings=$.extend({},defaults,options);               
         }                         
         plugin.init();
         
         //verificamos si se requiere abrir el modal automaticamente
         if(plugin.settings.autoOpen){
             $.modalViewer.methods.show(element,this.settings);
         }
    };
  
    $.modalViewer.methods={
                //inicializa el modalViewer
                initUI:function(elem,options){            
                    var modaldialog=$('<div>',{'class':'modal-dialog'});
                    var modalcontent=$('<div>',{'class':'modal-content'});
                    var modalheader=$('<div>',{'class':'modal-header'});
                    var modalbody=$('<div>',{'class':'modal-body'});
                    var modalmessage=$('<div>',{'class':'modal-message hide'});
                    var modalviewer=$('<div>',{'class':'modal-viewer'});
                    var modalfooter=$('<div>',{'class':'modal-footer'});
                    var modalloading=$('<div>',{'class':'modal-loading center hide'}).css({'width':'100%'});
                    
                    
                    //haader
                    modalheader.html('<button type="button" class="close" data-dismiss="modal">&times;</button>');
                    if(options.title!=''){
                        modalheader.append('<h4 class="blue bigger">'+options.title+'</h4>');
                    }     
                    
                    //asignamos el contenido del elemento div a la seccion del form
                    modalviewer.html($(elem).html());
                    
                    //body
                    modalloading.append('<div class="fa fa-spinner fa-2x fa-spin orange"></div>');
                    modalbody.append(modalloading);
                    modalbody.append(modalmessage).append(modalviewer);
                    
                    
                    //footer buttons
                    if(typeof(options.buttons)=='object'){                        
                        var btnClose=$("<button>",{'class':'btn btn-sm','data-dismiss':'modal'});                                                
                        btnClose.html('<i class="ace-icon fa fa-times"></i>'+options.buttons.Close.caption);                                                                       
                        modalfooter.html(btnClose);
                    }
                    
                    //modal content
                    modalcontent.html(modalheader).append(modalbody).append(modalfooter);
                    //modal dialog
                    modaldialog.html(modalcontent);                   
                    //propiedades del modal
                    $(elem).attr({'data-backdrop':'static','data-keyboard':false,'tabindex':'-1','role':'dialog'})
                            .css({'overflow-y':'visible'});                    
                    $(elem).addClass('modal fade').html(modaldialog);
                    
                    
                },
                //muestra el modalViewer
                show:function(modal,options){
                    
                    //iniciamos UI del dialogForm
                    $.modalViewer.methods.initUI(modal,options);
                    
                    //Verificamos si el paŕametro es una funcion y la llamamos
                    if($.isFunction(options.onBeforeOpen)){
                        if(options.onBeforeOpen.call(modal)===false){
                            return false;
                        }
                    }            
                    //Verificamos si se define la url de carga del form
                    if(options.url){                       
                        //Cargamos el formulario
                        $.modalViewer.methods.showLoading(modal);
                        $('.modal-viewer',modal).load(options.url,{},function(response, status, xhr){
                             
                             //verificamos si se produjo un error
                             if(status=='error'){
                                 //agregamos el mensaje en la cabecera del contenido
                                 var message = xhr.responseText;
                                 if(xhr.status != undefined && xhr.status != 401){
                                     message = xhr.statusText;
                                 }
                                 $.modalViewer.methods.showError(modal,message,xhr.status);
                                 
                             }else{
                                //Verificamos si el parámetro es una funcion y la llamamos 
                                if ( $.isFunction( options.onLoad ) ) {
                                    options.onLoad.call(modal);
                                }                               
                            }
                            //Ocultamos el icono de carga
                            $.modalViewer.methods.hideLoading(modal); 
                            
                            //alineacion vertical                            
                            if(options.verticalAlign=='middle'){
                               setTimeout(function(){$.modalViewer.methods.verticalPosition(modal)},50);
                            }
                            
                            
                        });
                    }else{
                          //Verificamos si el parámetro es una funcion y la llamamos 
                          if ( $.isFunction( options.onLoad ) ) {
                                options.onLoad.call(modal);
                          }  
                    }

                    //Verificamos si el parámetro es una funcion y inicializamos el metodo show del modal
                    if($.isFunction(options.onOpen)){
                        $(modal).on('show.bs.modal',function(e){
                            options.onOpen.call(modal);
                            //alineacion vertical                            
                            if(options.verticalAlign=='middle'){
                                $.modalViewer.methods.verticalPosition(modal);
                            }
                        }); 
                    }


                    //inicializamos evento hide para que elimine el modal
                    $(modal).on('hidden.bs.modal', function(){
                        if($.isFunction(options.onClose)){
                            if(options.onClose.call(modal)===false){
                                return false;
                            }
                        }

                        $(this).remove();
                    });

                    //inicializamos evento hide para que elimine el modal
                    $(modal).on('hidden.bs.modal', function(){                         
                          $(this).remove();                          
                    });
                    
                    //definicion opciones del modal
                    var modalcss={};
                    if(options.width){
                        modalcss['width']=options.width;   
                    }
                    if(options.height){
                        modalcss['height']=options.height;   
                    }
                    
                    //mostrar modal     
                     $(".modal-dialog",modal).css(modalcss); 
                     $(modal).modal('show');  

                },
                //oculta el modalViewer
                hide:function(modal){
                    $(modal).modal('hide');
                }, 
                verticalPosition: function(modal){
                     var dialog = $('.modal-dialog',modal);        

                    var initModalHeight = dialog.outerHeight();
                    var userScreenHeight = $(document).outerHeight();
                    
                    
                    if (initModalHeight > userScreenHeight) {
                        $('.modal-dialog',modal).css('overflow', 'auto'); 
                    } else {
                         
                        var  offset       = ($(window).height() - dialog.height()) / 2,
                         bottomMargin = parseInt(dialog.css('marginBottom'), 10);

        
                        if(offset < bottomMargin) offset = bottomMargin;
                        dialog.css("margin-top", offset);
                    }
                     
                },
                showError:function(modal,error,code){
                    var message = error;
                    var clase = 'info';
                    if(code != undefined && code != 401){
                        clase = 'danger';
                        message="<strong>Se ha producido un error:</strong> " +code+' '+error
                    }
                    $('.modal-message',modal).removeClass('hide').html('<div class="alert alert-'+clase+'">'+message+'</div>');
                },
                hideError:function(modal){
                    $('.modal-message',modal).addClass('hide');
                },                
                showLoading:function(modal){
                    $('.modal-loading',modal).removeClass('hide');
                },
                hideLoading:function(modal){
                     $('.modal-loading',modal).addClass('hide');  
                }                           
                
         };  
    
    
    
    
    
    
})(jQuery);



/**
 * Agrega nuevos metodos de validación al validador
 * Requiere:
 *      -jQuery
 *      -jquery.validate
 * Fecha de creacion: 18/08/2016
 * Fecha de modificación: 18/08/2016    
 * Creado por: Luis Malquin (luisor_mlt@hotmail.com)
 * Modificado por: Luis Malquin
 **/

//Incluimos el metodo para la validacion de la cedula
jQuery.validator.addMethod("cedula", function(value, element) {
    var sum = 0;
    var sum_total = 0;
    var residuo = 0;
    var digito = 0;
    if (value.length > 0 && !isNaN(parseInt(value))) {
        
        //obtenemos el numero de la region
        var nRegion=parseInt(value.substring(0,2));

        
        //verificamos el numero de caracteres sea igual a 10 
        //los 2 primeros digitos deben pertenecer al numero de la provincia donde fue expedida
        //agregamos la excepcion de numero de region 30 para la expedicion en el extrangero
        if (value.length == 10  && ((nRegion>=1 && nRegion<=24) || nRegion==30))
        {
            for (var n = 0; n < value.length - 1; n++)
            {
                if ((n + 1) % 2 != 0)
                    sum = value.charAt(n) * 2;
                else
                    sum = value.charAt(n) * 1;
                if (sum > 9)
                    sum = sum - 9;
                sum_total = sum + sum_total;
            }
            residuo = sum_total % 10;
            if (residuo != 0)
                digito = 10 - residuo;

            if (value.charAt(9) == digito)
                return true;
        }
        return false;
    }
    if (value.length > 0 && isNaN(parseInt(value))) {
        return false;
    }
    return true;
}, "El número de cédula es incorrecto");

//Incluimos el metodo para la validacion del ruc
jQuery.validator.addMethod("ruc", function(value, element) {

                if (value.length > 0 && !isNaN(parseInt(value.substr(0, 10)))) {
                    var sum = 0;
                    var sum_total = 0;
                    var residuo = 0;
                    var digito = 0;
                    var coefp = [3, 2, 7, 6, 5, 4, 3, 2];
                    var coefj = [4, 3, 2, 7, 6, 5, 4, 3, 2];
                    var n=0;
                    if (value.length == 13)
                    {
                        var dig = value.substr(2, 1);
                        if (dig < 6)
                        {
                            if ($.validator.methods.cedula.call(element, value.substr(0, 10)))
                            {
                                for (var i = 10; i < 13; i++)
                                {
                                    if (isNaN(parseInt(value.charAt(i))))
                                        return false;
                                }
                                return true;
                            }
                        }
                        else if (dig == 6)
                        {


                            for (n = 0; n < 8; n++)
                            {
                                sum = value.charAt(n) * coefp[n];
                                sum_total = sum + sum_total;
                            }
                            residuo = sum_total % 11;
                            if (residuo != 0)
                                digito = 11 - residuo;

                            if (value.charAt(8) == digito)
                                return true;

                        }
                        else if (dig == 9)
                        {


                            for (n = 0; n < 9; n++)
                            {
                                sum = value.charAt(n) * coefj[n];
                                sum_total = sum + sum_total;
                            }
                            residuo = sum_total % 11;
                            if (residuo != 0)
                                digito = 11 - residuo;

                            if (value.charAt(9) == digito)
                                return true;
                        }

                    }
                    return false;
                } else if (value.length > 0 && isNaN(parseInt(value))) {
                    return false;
                }
                return true;
            }
,"El RUC es incorrecto");


jQuery.validator.addMethod("alphaspace", function( value, element ) {        
	return this.optional( element ) || /^[a-z áéíóúñü]+$/i.test( value );
}, "Por favor, ingrese letras y espacios unicamente");

jQuery.validator.addMethod("alphaspacespecial", function( value, element ) {
	return this.optional( element ) || /^[a-z/ áéíóúñü]+$/i.test( value );
}, "Por favor, ingrese letras y espacios unicamente");

jQuery.validator.addMethod("alphanumericspace", function( value, element ) {
	return this.optional( element ) || /^[a-z áéíóúñü\d]+$/i.test( value );
}, "Por favor, ingrese letras, números y espacios unicamente");

jQuery.validator.addMethod('alphanumeric', function(value, element) {
    return this.optional(element) || /^[a-z0-9áéíóúñü\d]+$/i.test(value);
}, "Por favor, ingrese letras y números unicamente");
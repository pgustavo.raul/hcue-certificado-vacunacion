# HCUE-CERTIFICADO-VACUNACION V1.0.0
SOFTWARE PARA LA GENERACION DE CERTIFICADOS DE VACUNACIÓN

### Pre-requisitos 📋
Para le ejecución del sistema se necesitan los siguientes elementos:
* PHP 7.1 
* Oracle Express 19


## Contribuyendo 🖇️
Por favor leer el documento [CONTRIBUTING.md]

## Ver cambios con git log
git log --pretty=format:"##### %B %n> Fecha:  %ad con hash %h Realizado por %an" --date=short > CHANGELOG-temp.md

## Autores

* **Jipson Montalban** - *Developer* 
* **Gustavo Panchi** - *Reporter* 

## Licencia 📄
Este proyecto está bajo la Licencia (GNU) - mira el archivo [LICENSE.md](https://gitlab.com/MSP_EC/rdacaa/-/blob/master/LICENSE.md) para detalles


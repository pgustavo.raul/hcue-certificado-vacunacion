<?php
/**
 * Controlador con metodos para la obtecion de archivos
 * @author Richard Veliz
 */

namespace Core\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use \Symfony\Component\HttpFoundation\Response;


class ArchivoController extends Controller
{
    /**
     * Consume el webservis de la foto del registro civil
     * @Route("/fotoregistrocivil/{cedula}", name="coreapp_archivo_fotoregistrocivil" , requirements={"cedula" = "\d+"} , defaults={"cedula" = 0})
     * @param integer $cedula cédula de ciudadania del paciente
     * @return Response
     */
    public function fotoRegistrocivilAction($cedula)
    {
        $base64 = "";
        try {
            $managerArchivo = $this->get("core.app.entity.manager.archivo");
            $archivo = $managerArchivo->getArchivoByIdentificacion($cedula);
            if ($archivo) {
                $base64 = $archivo->getArchivo();
            } else {
                $service = $this->get("core.app.service.fotoregistrocivil");
                $result = $service->consumirws($cedula);
                $status = $result["status"];
                if ($status && array_key_exists("fotobase64", $result)) {
                    $base64 = $result["fotobase64"];
                    $archivo = $managerArchivo->create();
                    $archivo->setIdentificacion($cedula);
                    $archivo->setArchivo($base64);
                    $managerArchivo->saveArchivo($archivo, true);
                }
            }
            $managerPersona = $this->get("core.app.entity.manager.persona");
            $persona = $managerPersona->getPersonaByIdentificacion($cedula);
            if ($persona && $archivo && empty($persona->getFotoId())) {                
                    $persona->setFoto($archivo);
                    $managerPersona->savePersona($persona, true);                
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() > 0)
                ? "Se ha producido un error en el sistema, no es posible mostrar la foto"
                : $ex->getMessage();
            $this->addFlash("danger", $message);
        }
        $response = new Response();
        if ($base64 == "") {
            $rootDir = $this->get('kernel')->getRootDir();
            $base64 = base64_encode(file_get_contents($rootDir . '/../web/bundles/coregui/app/images/fotodefault.png'));
        }
        $base64 = base64_decode($base64);
        $response->setContent($base64);
        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;

    }

}

<?php
/**
 * Controlador con metodos para la obtecion de catalogos 
 * @author Luis Malquin 
 */

namespace Core\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use \Symfony\Component\HttpFoundation\Response;



class CatalogoController extends Controller
{
    const SERIALIZER='serializer';

    /**
     * Obtiene la lista de paises
     * @Route("/pais/combobox", name="coreapp_catalogo_paiscombobox")
     * @Method({"GET"})
     * @return json lista de paises en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function paisComboboxAction()
    {
           //obtnemos el servicio de ubicacion
          $servicioubicacion=  $this->get("core.app.entity.manager.pais");
          //Obtenemos los paises desde el servicio
          $paises=$servicioubicacion->getPaisesCombobox();
          //Serializamos a formato json el array de objetos
          $paises = $this->get(self::SERIALIZER)->serialize($paises, 'json');          
          return new Response($paises);
    }
    
    /**
     * Obtiene la lista de nacionalidades
     * @Route("/nacionalidad/combobox", name="coreapp_catalogo_nacionalidadcombobox")
     * @Method({"GET"})
     * @return json lista de nacionalidades en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function nacionalidadComboboxAction()
    {
           //obtnemos el servicio de ubicacion
          $servicioubicacion=  $this->get("core.app.entity.manager.pais");
          //Obtenemos los paises desde el servicio
          $nacionalidades=$servicioubicacion->getNacionalidadesCombobox();          
          //Serializamos a formato json el array de objetos
          $nacionalidades = $this->get(self::SERIALIZER)->serialize($nacionalidades, 'json');                    
          //retornamos el array en formato json
          return new Response($nacionalidades);
    }
    
    /**
     * Obtiene la lista de provincias
     * @Route("/provincia/combobox", name="coreapp_catalogo_provinciacombobox")
     * @Method({"GET"})
     * @return json lista de provincias en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function provinciaComboboxAction()
    {
           //obtnemos el servicio de ubicacion
          $servicioubicacion=  $this->get("core.app.entity.manager.provincia");
          //Obtenemos los paises desde el servicio
          $provincias=$servicioubicacion->getProvinciasCombobox();
          //Serializamos a formato json el array de objetos
          $provincias = $this->get(self::SERIALIZER)->serialize($provincias, 'json');                    
          //retornamos el array en formato json
          return new Response($provincias);
    }
    
    /**
     * Obtiene la lista de cantones identificados por el id de la provincia
     * @Route("/canton/combobox/{provincia_id}", name="coreapp_catalogo_cantoncombobox" , requirements={"provincia_id" = "\d+"}, defaults={"provincia_id"="0"})
     * @Method({"GET"})
     * @param integer $provincia_id id de la provincia       
     * @return json lista de cantones en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function cantonComboboxAction($provincia_id)
    {
           //obtnemos el servicio de ubicacion
          $servicioubicacion=  $this->get("core.app.entity.manager.canton");
          //Obtenemos los paises desde el servicio
          $cantones=$servicioubicacion->getCantonesCombobox($provincia_id);          
          //Serializamos a formato json el array de objetos
          $cantones = $this->get(self::SERIALIZER)->serialize($cantones, 'json');     
          //retornamos el array en formato json
          return new Response($cantones);
          
    }
    
     /**
     * Obtiene la lista de parroquias identificados por el id del canton
     * @Route("/parroquia/combobox/{canton_id}", name="coreapp_catalogo_parroquiacombobox", requirements={"canton_id" = "\d+"}, defaults={"canton_id"="0"})
     * @Method({"GET"})
     * @param integer $canton_id id del canton
     * @return json lista de parroquias en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function parroquiaComboboxAction($canton_id)
    {
           //obtnemos el servicio de ubicacion
          $servicioubicacion=  $this->get("core.app.entity.manager.parroquia");
          //Obtenemos los paises desde el servicio
          $parroquias=$servicioubicacion->getParroquiasCombobox($canton_id);
          //Serializamos a formato json el array de objetos
          $parroquias = $this->get(self::SERIALIZER)->serialize($parroquias, 'json');     
          //retornamos el array en formato json
          return new Response($parroquias);
          
    }
    
     /**
     * Obtiene la lista de catalogos identificados por el id del tipo de catalogo o id del catalogo padre , NOTA : este método esta compuesto de 2 rutas
     * @Route("/combobox/{tipocatalogo_id}", name="coreapp_catalogo_tipocatalogocombobox", requirements={"tipocatalogo_id" = "\d+"},defaults={"tipocatalogo_id"="0","catalogo_id"="0"})
     * @Route("/comboboxchildrens/{catalogo_id}", name="coreapp_catalogo_childrens_catalogocombobox", requirements={"catalogo_id" = "\d+"},defaults={"tipocatalogo_id"="0","catalogo_id"="0"})
     * @Method({"GET"})
     * @param integer $tipocatalogo_id id del tipo catalogo
     * @param integer $catalogo_id id del catalogo padre
     * @return json lista de catalogos en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function catalogoComboboxAction($tipocatalogo_id,$catalogo_id)
    {
           //obtnemos el servicio de catalogo
          $catalogomanager=  $this->get("core.app.entity.manager.catalogo");
          //Obtenemos los catalogos desde el manager
          $catalogos=array();
          if($tipocatalogo_id>0){
              $catalogos=$catalogomanager->getCatalogoCombobox($tipocatalogo_id);
          }else if($catalogo_id>0){
              $catalogos=$catalogomanager->getCatalogoChildrensCombobox($catalogo_id);
          }
          $catalogos = $this->get(self::SERIALIZER)->serialize($catalogos, 'json');
          return new Response($catalogos);
    }




    
}

<?php
/**
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @author Luis Malquin
*/

namespace Core\AppBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Core\AppBundle\Entity\Secuencialdocumento;

/**
 * Clase de manipulación del repositorio Secuencialdocumento
 */
class SecuencialdocumentoManager extends BaseManager{

    /**
     * Guarda los datos del Secuencialdocumento en la Base de Datos
     * @param  $objSecuencialdocumento objeto
     * @return boolean <true> si la informacion se guardo con exito
     * @throws \Exception
     */
    public function saveSecuencialdocumento(Secuencialdocumento $objSecuencialdocumento) {
        $status = 0;
        $this->beginTransaction();
        try {
            $status=$this->save($objSecuencialdocumento);
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }
        return $status;
    }

    /**
     * Devuelve el secuencial de un tipo de documento
     * @param integer $tipodocumento_id Id del tipo de documento
     * @param integer $entidad_id Id de la Entidad
     * @return integer $anio Año del cual desea obtener el secuencial
     */
    public function getSecuencial($tipodocumento_id,$entidad_id=0,$anio=0,$update=false){
        $response=0;
        $secuencial=$this->getRepository()->getSecuencial($tipodocumento_id,$entidad_id,$anio);
        if($secuencial){
            if(preg_match("/\bSECPADRE\b/i",$secuencial->getFormato())){
                $response=$this->getSecuencialPadre($tipodocumento_id,$entidad_id,$anio,$update);
            }else{
                $max=$this->getRepository()->getMaxSecuencial($tipodocumento_id,$entidad_id,$anio);
                $response=str_pad($max,$secuencial->getLongitud(),'0',STR_PAD_LEFT);
                if($update){
                    $secuencial->setSecuencial($max+1);
                    $this->saveSecuencialdocumento($secuencial);
                }
            }
        }
        return $response;
    }

    /**
     * Devuelve el secuencial del tipo de documento deacuerdo a su formato
     * @param integer $tipodocumento_id Id del tipo de documento
     * @param integer $entidad_id Id de la Entidad
     * @param integer $anio año en el que se generó el secuencial en base de datos
     * @param boolean $update true=actualiza el secuencial a su siguiente valor, false=solo obtiene el secuencial actual y no actualiza
     * @return string $response contiene el resutlado del secuencia
     */
    public function getSecuencialFormat($tipodocumento_id,$entidad_id=0,$anio=0,$update=false){
        $response="";
        $secuencial=$this->getRepository()->getSecuencial($tipodocumento_id,$entidad_id,$anio);
        if($secuencial){
            $response=$secuencial->getFormato();
            $numsecuencial=$this->getSecuencial($tipodocumento_id,$entidad_id,$anio,$update);
            $response=str_replace("#VTIPODOCUMENTO",$secuencial->getCttipodocumento()->getValor(),$response);
            $response=str_replace("#EUNICODIGO",str_pad($secuencial->getEntidadId(),6,'0',STR_PAD_LEFT),$response);
            $response=str_replace("#ANIO",$secuencial->getAnio(),$response);
            $response=str_replace("#SECPADRE",$numsecuencial,$response);
            $response=str_replace("#SEC",$numsecuencial,$response);
            $response=str_replace("#CODZONA",$this->getCodZona(),$response);
            $response=str_replace("#CODDISTRITO",$this->getCodDistrito(),$response);
        }
        return $response;
    }

    /**
     * Devuelve el secuencial del tipo de documento padre
     * @param integer $tipodocumento_id Id del tipo de documento
     * @param integer $entidad_id Id de la Entidad
     * @return integer $anio Año del cual desea obtener el secuencial
     */
    public function getSecuencialPadre($tipodocumento_id,$entidad_id=0,$anio=0,$update=false){
        $response=0;
        $secuencial=$this->getRepository()->getSecuencial($tipodocumento_id,$entidad_id,$anio);
        if($secuencial){
            $secuencialpadre=$secuencial->getSecuencialdocumentoPadre();
            if($secuencialpadre){
                $tipodocumentopadre_id=$secuencialpadre->getCttipodocumentoId();
                if($tipodocumentopadre_id>0){
                    $max=$this->getRepository()->getMaxSecuencial($tipodocumentopadre_id,$entidad_id,$anio);
                    $response=str_pad($max,$secuencialpadre->getLongitud(),'0',STR_PAD_LEFT);
                    if($update){
                        $secuencialpadre->setSecuencial($max+1);
                        $this->saveSecuencialdocumento($secuencialpadre);
                    }
                }

            }
        }
        return $response;
    }

    /**
     * Devuelve el código de zona del token de sesión de usuario
     * @return string
     */
    public function getCodZona(){

        $service_security = $this->getContainer()->get('core.seguridad.service.security_authorization');
        $entidad = $service_security->getSecurityToken()->getEntidad();

        $entidadPadreManager= $this->getContainer()->get('core.app.entity.manager.entidad');
        $entidadPadre=$entidadPadreManager->find($entidad->getEntidadId());

        $distrito = ($entidadPadre) ?  $entidadPadre->getCircuito()->getDistrito() : $entidad->getCircuito()->getDistrito();

        return $distrito->getZona()->getCodigo();        
    }

    /**
     * Devuelve el código de distrito del token de sesión de usuario
     * @return string
     */
    public function getCodDistrito(){

        $service_security = $this->getContainer()->get('core.seguridad.service.security_authorization');
        $entidad = $service_security->getSecurityToken()->getEntidad();

        $entidadPadreManager= $this->getContainer()->get('core.app.entity.manager.entidad');
        $entidadPadre=$entidadPadreManager->find($entidad->getEntidadId());

        $distrito = ($entidadPadre) ?  $entidadPadre->getCircuito()->getDistrito() : $entidad->getCircuito()->getDistrito();

        return $distrito->getCodigo();
        
    }

}
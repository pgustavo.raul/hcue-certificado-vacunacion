<?php

/**
 * Servicio con metodos que devuelven y guardan los datos de las personas
 * Descripcion de PersonaManager
 * @author Richard Veliz
 */

namespace Core\AppBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Persona;

class PersonaManager extends BaseManager
{
    /**
     * Guarda los datos de una Persona en la Base de Datos
     * @param Persona $persona datos de las persona
     * @param boolean $flush datos de las persona  default=false
     * @return $persona objeto Persona
     */
    public function savePersona(Persona $persona, $flush = false)
    {
        $this->save($persona, $flush);
        return $persona;
    }

    /**
     * Inactiva o elimina un registro basado en el modelo Persona
     * @param integer $id Id del registro Persona
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $persona =  $this->find($id);
        if (!$persona) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }
        $persona->setActivo(0);
        $this->save($persona, $flush);
        return true;
    }


    /**
     * Obtiene una persona por identificación
     * @param string $identificacion numero de identificación
     * @return null || "Core\AppBundle\Entity\Persona"
     */
    public function obtenerPersonaPorIdentificacion($identificacion, $arrayResult = false)
    {
        return $this->getRepository()->getPersonaPorIdentificacion($identificacion, $arrayResult);
    }

    /**
     * Obtiene la persona por el numero de identificacion
     * @param integer $identificacion Numero de Identificación
     * @return Objeto de tipo "Core\AppBundle\Entity\Persona"
     */
    public function getPersonaByIdentificacion($identificacion)
    {
        return $this->findOneBy(array("numeroidentificacion"=>$identificacion));
    }

    /**
     * Verifica si existe la persona por numero de identificacion
     * @param string $identificacion número de identificación personal
     * @return Boolean
     */
    public function existPersona($identificacion)
    {
        return $this->getRepository()->existPersona($identificacion);
    }

    /**
     * Cambia el estado de una persona por motivo de fallecimiento basado en el modelo Persona
     * @param integer $id Id del registro Persona
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function updateEstadoById($id, $flush = true)
    {
        $persona =  $this->find($id);
        if (!$persona) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }
        $persona->setEstado(0);
        $this->save($persona, $flush);
        return true;
    }

    /**
     * Obtiene la persona por el numero de identificacion
     * @param integer $identificacion Numero de Identificación
     * @return Objeto de tipo "Core\AppBundle\Entity\Persona"
     */
    public function getPersonaByArrayIdentificacion($arrayIdentificacion)
    {
        return  $this->getRepository()->getPersonaByArrayIdentificacion($arrayIdentificacion);
    }
}

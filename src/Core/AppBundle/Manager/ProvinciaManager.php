<?php
/** 
 * Clase de manipulación del repositorio Provincia 
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;
use \Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class ProvinciaManager extends BaseManager{    
     
     /**
     * Obtiene la lista de Provincias           
     * @param boolean $incluir_codigo Indica si se incluye el código de la Provincia en la descripcion
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getProvinciasCombobox($incluir_codigo=false){        
        $results=$this->getRepository()->getProvincias();
        $arrayResult=array();
        foreach ($results as $obj){                       
            $row=new Combobox();
            $descripcion=($incluir_codigo)?$obj->getCodigoAndDescripcion():$obj->getDescripcion();             
            $row->setId($obj->getId());
            $row->setDescripcion($descripcion);
            $arrayResult[]=$row;            
              
        }                                
        return $arrayResult;
    }
    
    /**
     * Obtiene la provincia por la descripcion
     * @param string $descripcion descripcion de la provincia
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Provincia"
     */
    public function searchProvincia($descripcion){
        return $this->getRepository()->searchProvincia($descripcion);
    }
    
   
}

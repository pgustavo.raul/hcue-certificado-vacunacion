<?php
/** 
 * Clase de manipulación de repositorios  
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;
use Doctrine\ORM\EntityManager;

class BaseManager {
    
    private $em;     
    private $repository;
    private $container;
    
    /**
     * Constructor.
     * @param EntityManager $em     
     * @param string $entityName nombre entidad
     */
    public function __construct(EntityManager $em,$entityName) {
        $this->em = $em;        
        $this->repository=$em->getRepository($entityName);
        $metadata=$em->getClassMetadata($entityName);        
        $this->class=$metadata->name;
    }
    
    /*
     * Set contenedor
     */
    public function setContainer($container) {
        $this->container = $container;
    }
    
    /*
     * Get contenedor
     */
    protected function getContainer() {
        return $this->container;
    }
    
    /*
     * Get repository
     */
    protected function getRepository() {
        return $this->repository;
    }
    
    /*
     * Obtener disparador de eventos desde el container
     */
    protected function getDispatcher() {
        return $this->getContainer()->get('event_dispatcher');
    }
    
    /**
    * Crea un objeto del modelo 
    * @return model
    */    
    public function create() {   
        $class=  $this->getClass();
        return new $class;
    }
    
       
     /**
     * Persistencia del modelo
     * @param $model
     * @param boolean $flush Indica si se realiza el flush de la persistencia
     * @return model
     */
    public function save($model, $flush=true) {
        $this->em->persist($model);
        if ($flush) {
            $this->em->flush();
        }
        return $model;
    }

         
     /**
     * Elimina un registro basado en el modelo  
     * @param model $model
     * @param boolean $flush Indica si se realiza el flush
     */
     public function delete($model, $flush = true) {
        $this->em->remove($model);
        if ($flush) {
            $this->em->flush();
        }
    }
    
    /**
     * Recarga los datos del modelo Aplicacion
     * @param model $model
     */
    public function reload($model) {
        $this->em->refresh($model);
    }

    /**
     * Obtiene un registro por medio de la clave primaria (Id)
     * @param $id
     * @return model
     */
    public function find($id) {                
        return $this->repository->findOneBy(array('id' => $id));                        
    }
    
     /**
     * Obtiene datos por criterios de búsqueda 
     * @param array $criteria Criterios de búsqueda 
     * @return array|null model
     */
    public function findBy(array $criteria = array(),array $orderBy=array())
    {
        return $this->repository->findBy($criteria,$orderBy);
    }
    
    /**
     * Obtiene datos de un registro por criterios de búsqueda 
     * @param array $criteria Criterios de búsqueda 
     * @return model||null 
     */
    public function findOneBy(array $criteria = array(),array $orderBy=array())
    {
        return $this->repository->findOneBy($criteria,$orderBy);
    }
    
     /**
     * Envia todos los cambios a la base de datos
     */
     protected function flushAllChanges()
    {
        $this->em->flush();
    }
    
     /**
     * Retorna el nombre completo de la clase con el namespace.
     * @return string
     */
     protected function getClass() {
        return $this->class;
    }
    
    /**
     * Retorna el nombre de la clase
     * @return string
     */
    protected function getClassName(){
        $class=  $this->getClass();
        return join('', array_slice(explode('\\', $class), -1));
    }
    
    /**
     * Retorna el EntityManager 
     * @return EntityManager
     */
    protected function getEntityManager(){
        return $this->em;
    }
    
    /**
     * Inicia una transaccion
     * @return EntityManager
     */
    protected function beginTransaction(){
        $this->em->getConnection()->beginTransaction();
        return $this->em;
    }
    
     /**
     * Persiste una transaccion
     * @return EntityManager
     */
     protected function commit(){
        $this->em->getConnection()->commit();
        return $this->em;        
    }
    
     /**
     * Deshace los cambios realizados en una transaccion
     * @return EntityManager
     */
     protected function rollback(){
        $this->em->getConnection()->rollback();
        return $this->em;
    }
}

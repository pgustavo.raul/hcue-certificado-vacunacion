<?php
/** 
 * Clase de manipulación del repositorio Detalleserviciotarifario 
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;
use \Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class DetalleserviciotarifarioManager extends BaseManager{

    /**
     * Obtiene los registros en  jerarquia descendente  por serviciotarifario_id
     * @param integer $serviciotarifario_id
     * @return array
     */
    public function getChildrenByServiciotarifarioId($serviciotarifario_id)
    {

        return  $this->getRepository()->getChildrenByServiciotarifarioId($serviciotarifario_id);
        
    }
    
   
}

<?php
/**
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @author Luis Malquin
*/

namespace Core\AppBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Clase de manipulación del repositorio Organico
 */
class OrganicoManager extends BaseManager{

    /**
     * Crea el Array del path que se desea dibujar en el TreeView
     * @param integer $active_id
     * @return Array Lista de Organicos para dibujar en TreeView
     */

    public function createTree($active_id){
        $tree = array();
        $organicos = $this->getRepository()->getOrganicos(true);
        if(!empty($organicos)){
            foreach ($organicos as $organico) {
                $id = $organico['id'];
                $organico_id = $organico['organico_id'];
                $active = ($active_id == $id);
                if(!$organico_id) {
                    $tree[$id] = array(
                        'key' => $id,
                        'title' => $organico['descripcion'],
                        'active' => $active
                    );
                    $arrayreturn = $this->obtenerHijos($organicos, $id, $active_id);
                    if(!empty($arrayreturn)){
                        $tree[$id]['children'] = $arrayreturn;
                    }
                }
            }
        }
        return array_values($tree);
    }

    /**
     * Retorna el array de organicos hijos
     * @param array $organicos
     * @param integer $id Id del organico padre
     * @param integer $active_id Id del organico activo en el tree
     * @return array de organicos hijos
     */
    public function obtenerHijos($organicos, $id, $active_id){
        $tree = array();
        foreach ($organicos as $organico) {
            $idOrganico = $organico["id"];
            $organicoPadre = $organico["organico_id"];
            $active = ($active_id == $idOrganico);
            if($organicoPadre == $id) {
                $tree[$idOrganico] = array(
                    'key' => $idOrganico,
                    'title' => $organico['descripcion'],
                    'active' => $active
                );
                $arrayreturn = $this->obtenerHijos($organicos, $idOrganico, $active_id);
                if(!empty($arrayreturn)){
                    $tree[$idOrganico]['children'] = $arrayreturn;
                }
            }
        }
        return array_values($tree);
    }

    /**
     * Obtiene el QueryBuilder de la Estructura Orgánica Raíz
     * @param integer $estado
     * @return QueryBuilder
     */
    public function obtenerParentOrganico($estado){
        return $this->getRepository()->getParentOrganico($estado);        
    }
}
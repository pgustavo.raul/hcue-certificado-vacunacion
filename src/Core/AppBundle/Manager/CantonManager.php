<?php
/** 
 * Clase de manipulación del repositorio canton 
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;
use \Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class CantonManager extends BaseManager{    
     
     /**
     * Obtiene la lista de cantones           
     * @param integer $provincia_id Id de la provincia
     * @param boolean $incluir_codigo Indica si se incluye el código del canton en la descripcion
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getCantonesCombobox($provincia_id,$incluir_codigo=false){        
        $results=$this->getRepository()->getCantones($provincia_id);
        $arrayResult=array();
        foreach ($results as $obj){                       
            $row=new Combobox();
            $descripcion=($incluir_codigo)?$obj->getCodigoAndDescripcion():$obj->getDescripcion();
            $row->setId($obj->getId());
            $row->setDescripcion($descripcion);
            $arrayResult[]=$row;     
        }                                
        return $arrayResult;
    }
    
    /**
     * Obtiene el canton por la descripcion
     * @param integer $provincia_id Id de la provincia
     * @param string $descripcion descripcion del canton
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Canton"
     */
    public function searchCanton($provincia_id,$descripcion){
        return $this->getRepository()->searchCanton($provincia_id,$descripcion);
    }
    
   
}

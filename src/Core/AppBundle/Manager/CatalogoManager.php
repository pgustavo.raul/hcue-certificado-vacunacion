<?php
/**
 * Clase de manipulación del repositorio Catalogo
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;

use \Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class CatalogoManager extends BaseManager
{

    /**
     * Obtiene la lista de Catalogos identificado por un tipo de catalogo
     * @param integer $tipocatalogo_id id del tipo de catalogo
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getCatalogoCombobox($tipocatalogo_id)
    {
        $results = $this->getRepository()->getCatalogo($tipocatalogo_id);
        $arrayResult = array();
        foreach ($results as $obj) {
            $row = new Combobox();
            $row->setId($obj->getId());
            $row->setDescripcion($obj->getDescripcion());
            $arrayResult[] = $row;
        }

        return $arrayResult;
    }


    /**
     * Obtiene la lista de Catalogos identificado por un tipo de catalogo
     * @param integer $tipocatalogo_id id del tipo de catalogo
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Catalogo"
     */
    public function getCatalogo($tipocatalogo_id, $arrayResult = false)
    {
        return $this->getRepository()->getCatalogo($tipocatalogo_id, $arrayResult);
    }

    /**
     * Obtiene la lista de Catalogos identificados por un catálogo padre
     * @param integer $catalogo_id id del catalogo padre
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getCatalogoChildrensCombobox($catalogo_id)
    {
        $results = $this->getRepository()->getCatalogoChildrens($catalogo_id);
        $arrayResult = array();
        foreach ($results as $obj) {
            $row = new Combobox();
            $row->setId($obj->getId());
            $row->setDescripcion($obj->getDescripcion());
            $arrayResult[] = $row;
        }
        return $arrayResult;
    }

    /**
     * Obtiene la lista de Catalogos identificados por un tipo de catalogo o un catálogo padre
     * @param integer $tipocatalogo_id id del tipo de catalogo
     * @param integer $catalogo_id id del del catalogo padre
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getCatalogoAllChildrensCombobox($tipocatalogo_id, $catalogo_id = 0)
    {
        $results = $this->getRepository()->getCatalogoAllChildrens($tipocatalogo_id, $catalogo_id);
        $arrayResult = array();
        foreach ($results as $obj) {
            $row = new Combobox();
            $row->setId($obj->getId());
            $row->setDescripcion($obj->getDescripcion());
            $arrayResult[] = $row;
        }
        return $arrayResult;
    }

    /**
     * Obtiene el catalogo por la descripcion
     * @param string $descripcion descripcion del catalogo
     * @param integer $tipocatalogo_id Id del tipo de catalogo
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Catalogo"
     */
    public function searchCatalogo($descripcion, $tipocatalogo_id = 0)
    {
        return $this->getRepository()->searchCatalogo($descripcion, $tipocatalogo_id);
    }

    public function getAvailableCatalogo()
    {
        $repository = $this->getRepository();
        return $repository->getAvailableCatalogoQueryBuilder()->getArrayResult();        
    }

    /**
     * Obtiene un registro de un Catalogo por la descripcion
     * @param integer $catalogo_id Id del detalle catalogo
     * @return $catalogo retorna el objeto catalogo
     */
    public function getAllChilldrenDetcatalogo($catalogo_id)
    {
        return $this->getRepository()->getAllChilldrenDetcatalogo($catalogo_id);
    }
}

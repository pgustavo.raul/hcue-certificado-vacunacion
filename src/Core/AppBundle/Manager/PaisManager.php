<?php
/** 
 * Clase de manipulación del repositorio Catalogo 
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;
use \Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class PaisManager extends BaseManager{    
     
     /**
     * Obtiene la lista de Paises           
     * @param boolean $incluir_codigo Indica si se incluye el código del País en la descripcion
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getPaisesCombobox($incluir_codigo=false){        
        $results=$this->getRepository()->getPaises();
        $arrayResult=array();
        foreach ($results as $obj){           
            $row=new Combobox();
            $descripcion=($incluir_codigo)?$obj->getCodigoAndNombre():$obj->getNombre();                                                
            $row->setId($obj->getId());
            $row->setDescripcion($descripcion);
            $arrayResult[]=$row;             
        }                                
        return $arrayResult;
    }    
     
    /**
     * Obtiene la lista de Nacionalidades           
     * @param boolean $incluir_codigo Indica si se incluye el código de la Provincia en la descripcion
     * @return array Lista de Nacionalidades array(array("Id"=>"","Descripcion"=>""),...)   
     */
    public function getNacionalidadesCombobox($incluir_codigo=false){
        
        $results=$this->getRepository()->getPaises();
        $arrayResult=array();
        foreach ($results as $obj){           
            $row=new Combobox();
            $descripcion=($incluir_codigo)?$obj->getCodigoAndNacionalidad():$obj->getNacionalidad();
            $row->setId($obj->getId());
            $row->setDescripcion($descripcion);
            $arrayResult[]=$row;             
        }                                
        return $arrayResult;
    }
    
    /**
     * Obtiene la nacionalidad por la descripcion
     * @param string $descripcion descripcion de la nacinalidad
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Pais"
     */
    public function searchNacionalidad($descripcion){
        return $this->getRepository()->searchNacionalidad($descripcion);
    }

    /**
     * Obtiene el pais por la descripcion
     * @param string $descripcion descripcion del pais
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Pais"
     */
    public function searchPais($descripcion){
        return $this->getRepository()->searchPais($descripcion);
    }
}

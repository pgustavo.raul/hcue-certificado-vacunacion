<?php 
/**
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @author Luis Malquin
*/

namespace Core\AppBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
* Clase de manipulación del repositorio Semanaepi
*/
class SemanaepiManager extends BaseManager{

	/**
        * Crea el objeto formulario para el modelo Semanaepi            
        * @param integer $id Id del registro Semanaepi     
        * @return formType  
        */
	public function createForm($id=0){
        $semanaepi= $this->create();
        if($id){
            $semanaepi=  $this->find($id);
            if (empty($semanaepi)) {
                    throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create('semanaepi', $semanaepi);
    }

	/**
        * Inactiva o elimina un registro basado en el modelo Semanaepi            
        * @param integer $id Id del registro Semanaepi 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
        $semanaepi=  $this->find($id);
        if (!$semanaepi) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }
        $semanaepi->setActivo(0);
        $this->save($semanaepi,$flush);        

        return true;
    }

    /**
     * Obtiene el registro de semana epidemiológica
     * @param date $fecha
     * @return semanaepi Registro de semana epidemiológica
     */
    public function getSemanaEpi($fecha){

        return $this->getRepository()->getSemanaEpiByfecha($fecha);
    }

}
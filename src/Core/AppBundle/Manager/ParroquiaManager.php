<?php
/** 
 * Clase de manipulación del repositorio Parroquia 
 * @author Luis Malquin
 */
namespace Core\AppBundle\Manager;
use \Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class ParroquiaManager extends BaseManager{    
     
     /**
     * Obtiene la lista de Parroquias 
     * @param integer $canton_id Id del canton          
     * @param boolean $incluir_codigo Indica si se incluye el código de la Parroquia en la descripcion
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getParroquiasCombobox($canton_id,$incluir_codigo=false){        
        $results=$this->getRepository()->getParroquias($canton_id);
        $arrayResult=array();
        foreach ($results as $obj){                                   
            $row=new Combobox();
            $descripcion=($incluir_codigo)?$obj->getCodigoAndDescripcion():$obj->getDescripcion();
            $row->setId($obj->getId());
            $row->setDescripcion($descripcion);
            $arrayResult[]=$row;
        }                                
        return $arrayResult;
    }
    
    /**
     * Obtiene la parroquia por la descripcion
     * @param intger $canton_id Id del canton
     * @param string $descripcion descripcion de la parroquia
     * @return array Lista de objetos de tipo "Core\AppBundle\Entity\Parroquia"
     */
    public function searchParroquia($canton_id,$descripcion){
        return $this->getRepository()->searchParroquia($canton_id,$descripcion);
    }
    
   
}

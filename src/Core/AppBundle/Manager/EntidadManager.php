<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\AppBundle\Manager;
use Core\AppBundle\Entity\Combobox;

/**
 * Description of EntidadManager
 *
 * @author danny
 */
class EntidadManager extends BaseManager {

    public function getEntidadUsuario() {
        $entidad_usuario_id = 1;
        return $this->getRepository()
            ->find($entidad_usuario_id);
    }

    /**
     * Obtiene la lista de entidades hijas de una entidad determinado
     * @param integer $entidad_id para retornar las entidades de acuerdo a su entidad padre
     * @param boolean $dql <true> para retornar el dql del QueryBuilder
     * @return
     */
    public function obtenerEntidadAllChildrens($entidad_id) {
        $results=$this->getRepository()->getEntidadAllChildrens($entidad_id);
        $arrayResult=array();
        foreach ($results as $obj){
            $arrayResult[]= $obj->getId();
        }
        return $arrayResult;
    }

    /**
     * Obtiene la entidad por Id
     * @param integer @id Id de la entidad
     * @return Objeto de tipo "Core\AppBundle\Entity\Entidad"
     */
    public function getEntidadById($id) {
        return $this->getRepository()->getEntidadById($id);
    }

    /**
     * Obtiene la entidad por Id
     * @param integer @distrito_id Id del distrito
     * @return Objeto de tipo "Core\AppBundle\Entity\Entidad"
     */
    public function getListEntidadesByDistrito($distrito_id) {
        $array= $this->getRepository()->getIdsEntidadesByDistrito($distrito_id);
        $list=array();
        for ($x=0;$x<count($array);$x++){
            $list[]=$array[$x]['id'];
        }
        $lst= implode(",",$list);
        return $lst;
    }

    /**
     * Obtiene la entidad por Id del Distrito
     * @param integer @distrito_id Id del distrito
     * @return Objeto de tipo "Core\AppBundle\Entity\Entidad"
     */
    public function getEntidadesByDistrito($distrito_id) {
        return $this->getRepository()->getEntidadesByDistrito($distrito_id);
    }

    /**
     * Obtiene las parroquias del Distrito tomando en cuenta los establecimientos
     * @param string $distrito_id Id del distrito
     * @return array
     */
    public function getParroquiasByDistrito($distrito_id){
        return $this->getRepository()->getParroquiasByDistrito($distrito_id);
    }

    /**
     * Obtiene las parroquias del Distrito tomando en cuenta los establecimientos
     * @param string $distrito_id Id del distrito
     * @return array
     */
    public function getEntidadByArray($array)
    {
        return $this->getRepository()->getEntidadByArray($array);
    }
}

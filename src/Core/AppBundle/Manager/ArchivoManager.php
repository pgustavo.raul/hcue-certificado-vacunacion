<?php

/**
 * Servicio con metodos que devuelven y guardan los datos de archivos
 * Descripcion de ArchivoManager
 * @author Richard Veliz
 */

namespace Core\AppBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Archivo;

class ArchivoManager extends BaseManager{


    /**
     * Guarda los datos del Archivo en la Base de Datos
     * @param Archivo $archivo datos del archivo
     * @param boolean $flush datos de las persona  default=false
     * @return $archivo objeto Archivo
     */
    public function saveArchivo(Archivo $archivo,$flush=false) {
        
        $this->save($archivo, $flush);        
        return $archivo;
    }

    /**
     * Obtiene el archivo por el numero de identificacion
     * @param integer $identificacion Numero de Identificación
     * @return Objeto de tipo "Core\AppBundle\Entity\Archivo"
     */
    public function getArchivoByIdentificacion($identificacion){
        return $this->findOneBy(array("identificacion"=>$identificacion));
    }


}

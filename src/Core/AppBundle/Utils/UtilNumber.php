<?php

/**
 * Clase de utilidades para el manejo de numeros
 * @author Richard Veliz
 */

namespace Core\AppBundle\Utils;

use Symfony\Component\Config\Definition\Exception\Exception;

class UtilNumber
{

    private static $UNIDADES = array(
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    );
    private static $DECENAS = array(
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    );
    private static $CENTENAS = array(
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    );
    private static $MONEDAS = array(
        array(
            'country' => 'Colombia',
            'currency' => 'COP',
            'singular' => 'PESO COLOMBIANO',
            'plural' => 'PESOS COLOMBIANOS',
            'symbol',
            '$'
        ),
        array(
            'country' => 'Estados Unidos',
            'currency' => 'USD',
            'singular' => 'DÓLAR',
            'plural' => 'DÓLARES',
            'symbol',
            'US$'
        ),
        array('country' => 'Europa', 'currency' => 'EUR', 'singular' => 'EURO', 'plural' => 'EUROS', 'symbol', '€'),
        array(
            'country' => 'México',
            'currency' => 'MXN',
            'singular' => 'PESO MEXICANO',
            'plural' => 'PESOS MEXICANOS',
            'symbol',
            '$'
        ),
        array(
            'country' => 'Perú',
            'currency' => 'PEN',
            'singular' => 'NUEVO SOL',
            'plural' => 'NUEVOS SOLES',
            'symbol',
            'S/'
        ),
        array(
            'country' => 'Reino Unido',
            'currency' => 'GBP',
            'singular' => 'LIBRA',
            'plural' => 'LIBRAS',
            'symbol',
            '£'
        ),
        array('country' => 'Argentina', 'currency' => 'ARS', 'singular' => 'PESO', 'plural' => 'PESOS', 'symbol', '$')
    );
    private static $separator = '.';
    private static $decimal_mark = ',';
    private static $glue = ' CON ';

    /**
     * Evalua si el número contiene separadores o decimales
     * formatea y ejecuta la función conversora
     * @param $number número a convertir
     * @param $miMoneda clave de la moneda
     * @return string completo
     */
    public static function to_word($number, $miMoneda = null)
    {
        if (strpos($number, static::$decimal_mark) === false) {
            $convertedNumber = array(
                static::convertNumbertoLetter($number, $miMoneda, 'entero')
            );
        } else {
            $number = explode(static::$decimal_mark, str_replace(static::$separator, '', trim($number)));
            $convertedNumber = array(
                static::convertNumbertoLetter($number[0], $miMoneda, 'entero'),
                static::convertNumbertoLetter($number[1], $miMoneda, 'decimal'),
            );
        }
        return implode(static::$glue, array_filter($convertedNumber));
    }

    /**
     * Convierte número a letras
     * @param $number
     * @param $miMoneda

     * @return $converted string convertido
     */
    public static function convertNumbertoLetter($number, $miMoneda = null)
    {

        $converted = '';
        if ($miMoneda !== null) {
            try {

                $moneda = array_filter(static::$MONEDAS, function ($m) use ($miMoneda) {
                    return ($m['currency'] == $miMoneda);
                });
                $moneda = array_values($moneda);
                if (count($moneda) <= 0) {
                    throw new Exception("Tipo de moneda inválido");                    
                }
                ($number < 2 ? $moneda = $moneda[0]['singular'] : $moneda = $moneda[0]['plural']);
            } catch (\Exception $e) {
                echo $e->getMessage();
                return false;
            }
        } else {
            $moneda = '';
        }
        if (($number < 0) || ($number > 999999999)) {
            return false;
        }
        $numberStr = (string)$number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else {
                if (intval($millones) > 0) {
                    $converted .= sprintf('%sMILLONES ', static::convertGroup($millones));
                }
            }
        }

        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else {
                if (intval($miles) > 0) {
                    $converted .= sprintf('%sMIL ', static::convertGroup($miles));
                }
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else {
                if (intval($cientos) > 0) {
                    $converted .= sprintf('%s ', static::convertGroup($cientos));
                }
            }
        }        
        return $converted.$moneda;
    }

    /**
     * Define el tipo de representación decimal (centenas/millares/millones)
     * @param $n
     * @return $output
     */
    public static function convertGroup($n)
    {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else {
            if ($n[0] !== '0') {
                $output = static::$CENTENAS[$n[0] - 1];
            }
        }
        $k = intval(substr($n, 1));
        if ($k <= 20) {
            $output .= static::$UNIDADES[$k];
        } else {
            if (($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', static::$DECENAS[intval($n[1]) - 2], static::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', static::$DECENAS[intval($n[1]) - 2], static::$UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }

}

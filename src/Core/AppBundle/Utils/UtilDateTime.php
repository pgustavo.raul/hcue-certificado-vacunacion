<?php
/**
 * Clase de utilidades para el manejo de fechas y tiempo
 * @author Luis Malquin
 */

namespace Core\AppBundle\Utils;

class UtilDateTime
{
    /**
     * Calcula el tiempo transcurrido entre dos fechas
     * @param string $fecha_inicio fecha inicial
     * @param string $fecha_fin fecha fin
     * @return array array("anios"=>..,"meses"=>..,"dias"=>..,"horas"=>..,"minutos"=>..,"segundos"=>..)
     */
    public static function tiempoEntreFechas($fecha_inicio, $fecha_fin = "", $formato = "Y-m-d")
    {

        if (empty($fecha_fin)) {
            $fecha_fin = date($formato);
        }

        if (!static::validarFecha($fecha_inicio, $formato)) {
            throw new \Exception("Fecha inicial ($fecha_inicio) no válida");
        }
        if (!static::validarFecha($fecha_fin, $formato)) {
            throw new \Exception("Fecha final ($fecha_fin) no válida");
        }

        $finicio = \DateTime::createFromFormat($formato, $fecha_inicio);
        $ffin = \DateTime::createFromFormat($formato, $fecha_fin);
        $interval = $finicio->diff($ffin);
        $anios = $interval->y;
        $meses = $interval->m;
        $dias = $interval->d;
        $total_dias = $interval->days;
        $horas = $interval->h;
        $minutos = $interval->i;
        $segundos = $interval->s;

        return array(
            'anios' => $anios,
            'meses' => $meses,
            'total_meses' => ($anios > 0) ? ($anios * 12) + $meses : $meses,
            'dias' => $dias,
            'total_dias' => $total_dias,
            'horas' => $horas,
            'minutos' => $minutos,
            'segundos' => $segundos
        );

    }

    /**
     * Calcula el tiempo transcurrido entre dos fechas
     * @param string $date fecha
     * @param string $formato formato de la fecha Ejm. Y-m-d H:i:s
     * @return boolean true si la fecha y formato es correcto
     */
    public static function validarFecha($date, $formato = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($formato, $date);
        return $d && $d->format($formato) == $date;
    }

    /**
     * Realiza la conversion de segundos a horas/minutos/segundos
     * @param integer $segundos
     * @return array array("horas"=>..,"minutos"=>..,"segundos"=>..,)
     */
    public static function convertirSegundosHoras($segundos, $arrayResult = true)
    {
        $horas = str_pad(floor($segundos / 3600), 2, "0", STR_PAD_LEFT);
        $minutos = str_pad(floor(($segundos - ($horas * 3600)) / 60), 2, "0", STR_PAD_LEFT);
        $segundos = str_pad($segundos - ($horas * 3600) - ($minutos * 60), 2, "0", STR_PAD_LEFT);
        if ($arrayResult) {
            return array('horas' => $horas, 'minutos' => $minutos, 'segundos' => $segundos);
        } else {
            return static::convertirHoraTexto("$horas:$minutos:$segundos");
        }
    }

    /**
     * Realiza la conversion de horas a texto
     * @param string $hora ej: 24:30:15
     * @return string ej: 24 horas, 30 minutos, 25 segundos
     */
    public static function convertirHoraTexto($hora)
    {
        $strReturn = "";
        $delimiter = ", ";
        $array = explode(":", $hora);
        foreach ($array as $key => $value) {
            if ($key == 0) {
                $time = ($value > 1) ? ' horas' : ' hora';
            }
            if ($key == 1) {
                $time = ($value > 1) ? ' minutos' : ' minuto';
            }
            if ($key == 2) {
                $time = ($value > 1) ? ' segundos' : ' segundo';
            }
            if ($value > 0) {
                if (!empty($strReturn) && $key < 3) {
                    $strReturn = $strReturn . $delimiter;
                }
                $strReturn .= $value . $time;
            }
        }
        return $strReturn;
    }

    /**
     *Suma o resta dias a una fecha
     * @param $date Fecha
     * @param $format Formato de la Fecha
     * @ndays numeros de dias
     * $operation operacion a realizar
     * @return false|string
     */
    public static function operationDaysDate($date, $format = "Y-m-d", $ndays = 0, $operation = "+")
    {

        return date($format, strtotime("$operation" . $ndays . ' day', strtotime($date)));

    }
}

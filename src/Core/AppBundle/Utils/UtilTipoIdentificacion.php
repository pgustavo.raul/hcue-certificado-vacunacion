<?php
/**
 * Clase de utilidades para validacion de tipos de identificacion
 * @author Luis Malquin
 */

namespace Core\AppBundle\Utils;

class UtilTipoIdentificacion
{
    /**
     * Valida el numero de identificacion tipo cedula
     * @param string $cedula cédula de identificacion
     * @return boolean true si es correcto
     */
    public static function validarCedula($cedula)
    {
        $sum = 0;
        $sum_total = 0;
        $residuo = 0;
        $digito = 0;
        $valido = false;
        $error = "";
        if (strlen($cedula) == 10 && is_numeric($cedula)) {
            //obtenemos el numero de la region 
            $nRegion = substr($cedula, 0, 2);

            //Verificamos si el tercer digito debe ser menor a 6 
            //Los 2 primeros digitos deben pertenecer al numero de la provincia donde fue expedida
            //Agregamos la excepcion de numero de region 30 para la expedicion en el extrangero
            if (($nRegion >= 1 && $nRegion <= 24) || $nRegion == 30) {
                for ($n = 0; $n < strlen($cedula) - 1; $n++) {
                    if (($n + 1) % 2 != 0) {
                        $sum = substr($cedula, $n, 1) * 2;
                    } else {
                        $sum = substr($cedula, $n, 1) * 1;
                    }
                    if ($sum > 9) {
                        $sum = $sum - 9;
                    }
                    $sum_total = $sum + $sum_total;
                }
                $residuo = $sum_total % 10;
                if ($residuo != 0) {
                    $digito = 10 - $residuo;
                } else {
                    $digito = 0;
                }

                if (substr($cedula, 9, 1) == $digito) {
                    $valido = true;
                } else {
                    $error = 'Número de cédula incorrecto';
                }
            } else {
                $error = "No coincide el número del lugar de expedición debe ser entre 1 y 24 o 30";
            }
        } else {
            $error = "El número de cedula debe contener 10 caracteres numéricos";
        }


        return array("validacion" => $valido, 'mensaje' => $error);
    }


}

<?php

/**
 * Clase de utilidades para el manejo de string
 * @author Richard Veliz
 */

namespace Core\AppBundle\Utils;

use Symfony\Component\Config\Definition\Exception\Exception;

class UtilString
{
    /**
     * Remueve las tildes de un texto
     * @param $text
     * @return string
     */
    public static function removeAccent($text)
    {
        $replace = array(
            'a' => 'á',
            'e' => 'é',
            'i' => 'í',
            'o' => 'ó',
            'u' => 'ú',
            'A' => 'Á',
            'E' => 'É',
            'I' => 'Í',
            'O' => 'Ó',
            'U' => 'Ú'
        );

        foreach ($replace as $k => $value) {
            $text = str_replace($value, $k, $text);
        }
        return $text;
    }
}

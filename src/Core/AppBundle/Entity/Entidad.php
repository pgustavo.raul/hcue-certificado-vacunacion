<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table(name="hcue_sistema.entidad")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\EntidadRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Entidad{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.entidad_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $codigo
     * @ORM\Column(name="codigo", type="string",length=20)
     **/
    private $codigo;

    /**
     * @var string $ruc
     * @ORM\Column(name="ruc", type="string",length=20)
     **/
    private $ruc;

    /**
     * @var string $nombreoficial
     * @ORM\Column(name="nombreoficial", type="string",length=250, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo nombreoficial")
     **/
    private $nombreoficial;

    /**
     * @var string $nombrecomercial
     * @ORM\Column(name="nombrecomercial", type="string",length=150)
     **/
    private $nombrecomercial;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer")
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad_padre;

    /**
     * @var integer $organico_id
     * @ORM\Column(name="organico_id", type="integer", nullable=false)
     **/
    private $organico_id;

    /**
     * @ORM\ManyToOne(targetEntity="Organico")
     * @ORM\JoinColumn(name="organico_id", referencedColumnName="id")
     **/
    private $organico;

    /**
     * @var integer $ctclasificacion_id
     * @ORM\Column(name="ctclasificacion_id", type="integer", nullable=false)
     **/
    private $ctclasificacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctclasificacion_id", referencedColumnName="id")
     **/
    private $ctclasificacion;

    /**
     * @var integer $tipoestablecimiento_id
     * @ORM\Column(name="tipoestablecimiento_id", type="integer")
     **/
    private $tipoestablecimiento_id;

    /**
     * @ORM\ManyToOne(targetEntity="Tipoestablecimiento")
     * @ORM\JoinColumn(name="tipoestablecimiento_id", referencedColumnName="id")
     **/
    private $tipoestablecimiento;

    /**
     * @var string $direccion
     * @ORM\Column(name="direccion", type="string",length=255)
     **/
    private $direccion;

    /**
     * @var string $direccionreferencia
     * @ORM\Column(name="direccionreferencia", type="string",length=255)
     **/
    private $direccionreferencia;

    /**
     * @var string $numestablecimiento
     * @ORM\Column(name="numestablecimiento", type="string",length=5)
     **/
    private $numestablecimiento;

    /**
     * @var string $telefono
     * @ORM\Column(name="telefono", type="string",length=20)
     **/
    private $telefono;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string",length=20)
     **/
    private $email;

    /**
     * @var integer $parroquia_id
     * @ORM\Column(name="parroquia_id", type="integer", nullable=false)
     **/
    private $parroquia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Parroquia")
     * @ORM\JoinColumn(name="parroquia_id", referencedColumnName="id")
     **/
    private $parroquia;

    /**
     * @var integer $circuito_id
     * @ORM\Column(name="circuito_id", type="integer", nullable=false)
     **/
    private $circuito_id;

    /**
     * @ORM\ManyToOne(targetEntity="Circuito")
     * @ORM\JoinColumn(name="circuito_id", referencedColumnName="id")
     **/
    private $circuito;

    /**
     * @var integer $cttipocoordenada_id
     * @ORM\Column(name="cttipocoordenada_id", type="integer")
     **/
    private $cttipocoordenada_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipocoordenada_id", referencedColumnName="id")
     **/
    private $cttipocoordenada;

    /**
     * @var string $latgps
     * @ORM\Column(name="latgps", type="string",length=40)
     **/
    private $latgps;

    /**
     * @var string $longps
     * @ORM\Column(name="longps", type="string",length=40)
     **/
    private $longps;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer")
     **/
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer")
     **/
    private $usuariomodificacion_id;

    /**
     * @var \Datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime")
     **/
    private $fechamodificacion;

    /**
     * @var \Datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $z17s
     * @ORM\Column(name="z17s", type="integer")
     **/
    private $z17s;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set codigo
     * @param string $codigo
     * @return entidad
     **/
    public function setCodigo($codigo){
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * Get codigo
     * @return string
     **/
    public function getCodigo(){
        return $this->codigo;
    }

    /**
     * Set ruc
     * @param string $ruc
     * @return entidad
     **/
    public function setRuc($ruc){
        $this->ruc = $ruc;
        return $this;
    }

    /**
     * Get ruc
     * @return string
     **/
    public function getRuc(){
        return $this->ruc;
    }

    /**
     * Set nombreoficial
     * @param string $nombreoficial
     * @return entidad
     **/
    public function setNombreoficial($nombreoficial){
        $this->nombreoficial = $nombreoficial;
        return $this;
    }

    /**
     * Get nombreoficial
     * @return string
     **/
    public function getNombreoficial(){
        return $this->nombreoficial;
    }

    /**
     * Set nombrecomercial
     * @param string $nombrecomercial
     * @return entidad
     **/
    public function setNombrecomercial($nombrecomercial){
        $this->nombrecomercial = $nombrecomercial;
        return $this;
    }

    /**
     * Get nombrecomercial
     * @return string
     **/
    public function getNombrecomercial(){
        return $this->nombrecomercial;
    }

    /**
     * Set entidad_id
     * @param integer $entidad_id
     * @return entidad
     **/
    public function setEntidadId($entidad_id){
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId(){
        return $this->entidad_id;
    }

    /**
     * Set entidad Padre
     * @param Entidad $entidadPadre
     * @return entidad
     **/
    public function setEntidadPadre(Entidad $entidadPadre){
        $this->entidad_padre = $entidadPadre;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidadPadre(){
        return $this->entidad_padre;
    }

    /**
     * Set organico_id
     * @param integer $organico_id
     * @return entidad
     **/
    public function setOrganicoId($organico_id){
        $this->organico_id = $organico_id;
        return $this;
    }

    /**
     * Get organico_id
     * @return integer
     **/
    public function getOrganicoId(){
        return $this->organico_id;
    }

    /**
     * Set organico
     * @param Organico $organico
     * @return entidad
     **/
    public function setOrganico(Organico $organico){
        $this->organico = $organico;
        return $this;
    }

    /**
     * Get organico
     * @return \Core\AppBundle\Entity\Organico
     **/
    public function getOrganico(){
        return $this->organico;
    }

    /**
     * Set ctclasificacion_id
     * @param integer $ctclasificacion_id
     * @return entidad
     **/
    public function setCtclasificacionId($ctclasificacion_id){
        $this->ctclasificacion_id = $ctclasificacion_id;
        return $this;
    }

    /**
     * Get ctclasificacion_id
     * @return integer
     **/
    public function getCtclasificacionId(){
        return $this->ctclasificacion_id;
    }

    /**
     * Set ctclasificacion
     * @param Catalogo $ctclasificacion
     * @return entidad
     **/
    public function setCtclasificacion(Catalogo $ctclasificacion){
        $this->ctclasificacion = $ctclasificacion;
        return $this;
    }

    /**
     * Get ctclasificacion
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtclasificacion(){
        return $this->ctclasificacion;
    }

    /**
     * Set tipoestablecimiento_id
     * @param integer $tipoestablecimiento_id
     * @return entidad
     **/
    public function setTipoestablecimientoId($tipoestablecimiento_id){
        $this->tipoestablecimiento_id = $tipoestablecimiento_id;
        return $this;
    }

    /**
     * Get tipoestablecimiento_id
     * @return integer
     **/
    public function getTipoestablecimientoId(){
        return $this->tipoestablecimiento_id;
    }

    /**
     * Set tipoestablecimiento
     * @param Tipoestablecimiento $tipoestablecimiento
     * @return entidad
     **/
    public function setTipoestablecimiento(Tipoestablecimiento $tipoestablecimiento){
        $this->tipoestablecimiento = $tipoestablecimiento;
        return $this;
    }

    /**
     * Get tipoestablecimiento
     * @return \Core\AppBundle\Entity\Tipoestablecimiento
     **/
    public function getTipoestablecimiento(){
        return $this->tipoestablecimiento;
    }

    /**
     * Set direccion
     * @param string $direccion
     * @return entidad
     **/
    public function setDireccion($direccion){
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * Get direccion
     * @return string
     **/
    public function getDireccion(){
        return $this->direccion;
    }

    /**
     * Set direccionreferencia
     * @param string $direccionreferencia
     * @return entidad
     **/
    public function setDireccionreferencia($direccionreferencia){
        $this->direccionreferencia = $direccionreferencia;
        return $this;
    }

    /**
     * Get direccionreferencia
     * @return string
     **/
    public function getDireccionreferencia(){
        return $this->direccionreferencia;
    }

    /**
     * Set numestablecimiento
     * @param string $numestablecimiento
     * @return entidad
     **/
    public function setNumestablecimiento($numestablecimiento){
        $this->numestablecimiento = $numestablecimiento;
        return $this;
    }

    /**
     * Get numestablecimiento
     * @return string
     **/
    public function getNumestablecimiento(){
        return $this->numestablecimiento;
    }

    /**
     * Set telefono
     * @param string $telefono
     * @return entidad
     **/
    public function setTelefono($telefono){
        $this->telefono = $telefono;
        return $this;
    }

    /**
     * Get telefono
     * @return string
     **/
    public function getTelefono(){
        return $this->telefono;
    }

    /**
     * Set email
     * @param string $email
     * @return entidad
     **/
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     * @return string
     **/
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set parroquia_id
     * @param integer $parroquia_id
     * @return entidad
     **/
    public function setParroquiaId($parroquia_id){
        $this->parroquia_id = $parroquia_id;
        return $this;
    }

    /**
     * Get parroquia_id
     * @return integer
     **/
    public function getParroquiaId(){
        return $this->parroquia_id;
    }

    /**
     * Set parroquia
     * @param Parroquia $parroquia
     * @return entidad
     **/
    public function setParroquia(Parroquia $parroquia){
        $this->parroquia = $parroquia;
        return $this;
    }

    /**
     * Get parroquia
     * @return \Core\AppBundle\Entity\Parroquia
     **/
    public function getParroquia(){
        return $this->parroquia;
    }

    /**
     * Set circuito_id
     * @param integer $circuito_id
     * @return entidad
     **/
    public function setCircuitoId($circuito_id){
        $this->circuito_id = $circuito_id;
        return $this;
    }

    /**
     * Get circuito_id
     * @return integer
     **/
    public function getCircuitoId(){
        return $this->circuito_id;
    }

    /**
     * Set circuito
     * @param Circuito $circuito
     * @return entidad
     **/
    public function setCircuito(Circuito $circuito){
        $this->circuito = $circuito;
        return $this;
    }

    /**
     * Get circuito
     * @return \Core\AppBundle\Entity\Circuito
     **/
    public function getCircuito(){
        return $this->circuito;
    }

    /**
     * Set cttipocoordenada_id
     * @param integer $cttipocoordenada_id
     * @return entidad
     **/
    public function setCttipocoordenadaId($cttipocoordenada_id){
        $this->cttipocoordenada_id = $cttipocoordenada_id;
        return $this;
    }

    /**
     * Get cttipocoordenada_id
     * @return integer
     **/
    public function getCttipocoordenadaId(){
        return $this->cttipocoordenada_id;
    }

    /**
     * Set cttipocoordenada
     * @param Catalogo $cttipocoordenada
     * @return entidad
     **/
    public function setCttipocoordenada(Catalogo $cttipocoordenada){
        $this->cttipocoordenada = $cttipocoordenada;
        return $this;
    }

    /**
     * Get cttipocoordenada
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCttipocoordenada(){
        return $this->cttipocoordenada;
    }

    /**
     * Set latgps
     * @param string $latgps
     * @return entidad
     **/
    public function setLatgps($latgps){
        $this->latgps = $latgps;
        return $this;
    }

    /**
     * Get latgps
     * @return string
     **/
    public function getLatgps(){
        return $this->latgps;
    }

    /**
     * Set longps
     * @param string $longps
     * @return entidad
     **/
    public function setLongps($longps){
        $this->longps = $longps;
        return $this;
    }

    /**
     * Get longps
     * @return string
     **/
    public function getLongps(){
        return $this->longps;
    }

    /**
     * Set estado
     * @param integer $estado
     * @return entidad
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer $activo
     * @return entidad
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer $usuariomodificacion_id
     * @return entidad
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer $usuariocreacion_id
     * @return entidad
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Get z17s
     * @return integer
     **/
    public function getZ17s(){
        return $this->z17s;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->activo=1;
        $this->fechacreacion=new \DateTime();

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

    /**
     * Obtiene el codigo y el nombre de la parroquia concatenado
     * @return string
     **/
    public function getCodigoAndNombreoficial() {
        return $this->codigo.' '.$this->nombreoficial;
    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descripcion y definicion de tipos de catalogos
 * @ORM\Table(name="hcue_catalogos.catalogo")
 * @ORM\Entity()
 */
class TipocatalogoBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     **/
    private $descripcion;

    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion()
    {
        return $this->descripcion;
    }

}
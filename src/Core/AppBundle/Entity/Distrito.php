<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.distrito")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\DistritoRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Distrito{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.distrito_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $zona_id
	* @ORM\Column(name="zona_id", type="integer", nullable=false)
	**/
	private $zona_id;

	/**
	* @ORM\ManyToOne(targetEntity="Zona")
	* @ORM\JoinColumn(name="zona_id", referencedColumnName="id")
	**/
	private $zona;

	/**
	* @var string $codigo
	* @ORM\Column(name="codigo", type="string",length=20, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo codigo")
	**/
	private $codigo;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=100, nullable=true)
	**/
	private $descripcion;

	/**
	* @var string $distribucion
	* @ORM\Column(name="distribucion", type="string",length=255, nullable=true)
	**/
	private $distribucion;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set zona_id
	* @param integer zona_id
	* @return distrito
	**/
	public function setZonaId($zona_id){
		 $this->zona_id = $zona_id;
		 return $this;
	}

	/**
	* Get zona_id
	* @return integer
	**/
	public function getZonaId(){
		 return $this->zona_id;
	}

	/**
	* Set zona
	* @param \Core\AppBundle\Entity\Zona $zona
	**/
	public function setZona(\Core\AppBundle\Entity\Zona $zona){
		 $this->zona = $zona;
		 return $this;
	}

	/**
	* Get zona
	* @return \Core\AppBundle\Entity\Zona
	**/
	public function getZona(){
		 return $this->zona;
	}

	/**
	* Set codigo
	* @param string codigo
	* @return distrito
	**/
	public function setCodigo($codigo){
		 $this->codigo = $codigo;
		 return $this;
	}

	/**
	* Get codigo
	* @return string
	**/
	public function getCodigo(){
		 return $this->codigo;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return distrito
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set distribucion
	* @param string distribucion
	* @return distrito
	**/
	public function setDistribucion($distribucion){
		 $this->distribucion = $distribucion;
		 return $this;
	}

	/**
	* Get distribucion
	* @return string
	**/
	public function getDistribucion(){
		 return $this->distribucion;
	}

	/**
	* Set estado
	* @param integer estado
	* @return distrito
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return distrito
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return distrito
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->fechamodificacion=new \DateTime();
$this->fechacreacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
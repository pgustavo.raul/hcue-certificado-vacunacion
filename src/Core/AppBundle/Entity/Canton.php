<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de canton
 * @author Luis Malquin
 * @ORM\Table(name="hcue_catalogos.canton")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\CantonRepository")
 */
class Canton {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_catalogos.canton_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $codcanton
     * @ORM\Column(name="codcanton", type="string",length=4, nullable=true)
     * */
    private $codcanton;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=true)
     * */
    private $estado;

    /**
     * @var integer $provincia_id
     * @ORM\Column(name="provincia_id", type="integer", nullable=true)
     * */
    private $provincia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     * */
    private $provincia;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=50, nullable=true)
     * */
    private $descripcion;

    /**
     * @var string $coddistrito
     * @ORM\Column(name="coddistrito", type="string",length=5, nullable=true)
     * */
    private $coddistrito;
    
    
    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Set codcanton
     * @param string codcanton
     * @return canton
     * */
    public function setCodcanton($codcanton) {
        $this->codcanton = $codcanton;
        return $this;
    }

    /**
     * Get codcanton
     * @return string
     * */
    public function getCodcanton() {
        return $this->codcanton;
    }

    /**
     * Set estado
     * @param integer estado
     * @return canton
     * */
    public function setEstado($estado) {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set provincia_id
     * @param integer provincia_id
     * @return canton
     * */
    public function setProvinciaId($provincia_id) {
        $this->provincia_id = $provincia_id;
        return $this;
    }

    /**
     * Get provincia_id
     * @return integer
     * */
    public function getProvinciaId() {
        return $this->provincia_id;
    }

    /**
     * Set provincia
     * @param \Core\AppBundle\Entity\Provincia $provincia
     * */
    public function setProvincia(\Core\AppBundle\Entity\Provincia $provincia) {
        $this->provincia = $provincia;
        return $this;
    }

    /**
     * Get provincia
     * @return \Core\AppBundle\Entity\Provincia
     * */
    public function getProvincia() {
        return $this->provincia;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return canton
     * */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion() {
        return $this->descripcion;
    }

    
    /**
     * Set coddistrito
     * @param string coddistrito
     * @return canton
     * */
    public function setCoddistrito($coddistrito) {
        $this->coddistrito = $coddistrito;
        return $this;
    }

    /**
     * Get coddistrito
     * @return string
     * */
    public function getCoddistrito() {
        return $this->coddistrito;
    }    
    
    /**
    * Obtiene el codigo y el nombre del canton concatenado  
    * @return string
    **/
    public function getCodigoAndDescripcion() {
            return $this->codcanton.' '.$this->descripcion;
    }
    
    public function __toString() {
        return $this->descripcion;
    }
}
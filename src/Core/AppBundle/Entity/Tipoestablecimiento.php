<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.tipoestablecimiento")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\TipoestablecimientoRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Tipoestablecimiento{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.tipoestablecimiento_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $tipoestablecimiento_id
	* @ORM\Column(name="tipoestablecimiento_id", type="integer", nullable=true)
	**/
	private $tipoestablecimiento_id;

	/**
	* @ORM\ManyToOne(targetEntity="Tipoestablecimiento")
	* @ORM\JoinColumn(name="tipoestablecimiento_id", referencedColumnName="id")
	**/
	private $tipoestablecimiento;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=255, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo descripcion")
	**/
	private $descripcion;

	/**
	* @var string $simbologia
	* @ORM\Column(name="simbologia", type="string",length=20, nullable=true)
	**/
	private $simbologia;

	/**
	* @var integer $nivel
	* @ORM\Column(name="nivel", type="integer", nullable=true)
	**/
	private $nivel;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var datetime $fechamodificiacion
	* @ORM\Column(name="fechamodificiacion", type="datetime", nullable=true)
	**/
	private $fechamodificiacion;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set tipoestablecimiento_id
	* @param integer tipoestablecimiento_id
	* @return tipoestablecimiento
	**/
	public function setTipoestablecimientoId($tipoestablecimiento_id){
		 $this->tipoestablecimiento_id = $tipoestablecimiento_id;
		 return $this;
	}

	/**
	* Get tipoestablecimiento_id
	* @return integer
	**/
	public function getTipoestablecimientoId(){
		 return $this->tipoestablecimiento_id;
	}

	/**
	* Set tipoestablecimiento
	* @param \Core\AppBundle\Entity\Tipoestablecimiento $tipoestablecimiento
	**/
	public function setTipoestablecimiento(\Core\AppBundle\Entity\Tipoestablecimiento $tipoestablecimiento){
		 $this->tipoestablecimiento = $tipoestablecimiento;
		 return $this;
	}

	/**
	* Get tipoestablecimiento
	* @return \Core\AppBundle\Entity\Tipoestablecimiento
	**/
	public function getTipoestablecimiento(){
		 return $this->tipoestablecimiento;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return tipoestablecimiento
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set simbologia
	* @param string simbologia
	* @return tipoestablecimiento
	**/
	public function setSimbologia($simbologia){
		 $this->simbologia = $simbologia;
		 return $this;
	}

	/**
	* Get simbologia
	* @return string
	**/
	public function getSimbologia(){
		 return $this->simbologia;
	}

	/**
	* Set nivel
	* @param integer nivel
	* @return tipoestablecimiento
	**/
	public function setNivel($nivel){
		 $this->nivel = $nivel;
		 return $this;
	}

	/**
	* Get nivel
	* @return integer
	**/
	public function getNivel(){
		 return $this->nivel;
	}

	/**
	* Set estado
	* @param integer estado
	* @return tipoestablecimiento
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set fechamodificiacion
	* @param datetime fechamodificiacion
	* @return tipoestablecimiento
	**/
	public function setFechamodificiacion($fechamodificiacion){
		 $this->fechamodificiacion = $fechamodificiacion;
		 return $this;
	}

	/**
	* Get fechamodificiacion
	* @return datetime
	**/
	public function getFechamodificiacion(){
		 return $this->fechamodificiacion;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return tipoestablecimiento
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return tipoestablecimiento
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		$this->fechacreacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
		
	}


	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.serviciotarifario")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\ServiciotarifarioRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Serviciotarifario{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.serviciotarifario_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $tarifario_id
	* @ORM\Column(name="tarifario_id", type="integer", nullable=false)
	**/
	private $tarifario_id;

	/**
	* @ORM\ManyToOne(targetEntity="Tarifario")
	* @ORM\JoinColumn(name="tarifario_id", referencedColumnName="id")
	**/
	private $tarifario;

	/**
	* @var integer $serviciotarifario_id
	* @ORM\Column(name="serviciotarifario_id", type="integer", nullable=true)
	**/
	private $serviciotarifario_id;

	/**
	* @ORM\ManyToOne(targetEntity="Serviciotarifario")
	* @ORM\JoinColumn(name="serviciotarifario_id", referencedColumnName="id")
	**/
	private $serviciotarifario;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=2000, nullable=true)
	**/
	private $descripcion;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=true)
	**/
	private $activo;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set tarifario_id
	* @param integer tarifario_id
	* @return serviciotarifario
	**/
	public function setTarifarioId($tarifario_id){
		 $this->tarifario_id = $tarifario_id;
		 return $this;
	}

	/**
	* Get tarifario_id
	* @return integer
	**/
	public function getTarifarioId(){
		 return $this->tarifario_id;
	}

	/**
	* Set tarifario
	* @param \Core\AppBundle\Entity\Tarifario $tarifario
	**/
	public function setTarifario(\Core\AppBundle\Entity\Tarifario $tarifario){
		 $this->tarifario = $tarifario;
		 return $this;
	}

	/**
	* Get tarifario
	* @return \Core\AppBundle\Entity\Tarifario
	**/
	public function getTarifario(){
		 return $this->tarifario;
	}

	/**
	* Set serviciotarifario_id
	* @param integer serviciotarifario_id
	* @return serviciotarifario
	**/
	public function setServiciotarifarioId($serviciotarifario_id){
		 $this->serviciotarifario_id = $serviciotarifario_id;
		 return $this;
	}

	/**
	* Get serviciotarifario_id
	* @return integer
	**/
	public function getServiciotarifarioId(){
		 return $this->serviciotarifario_id;
	}

	/**
	* Set serviciotarifario
	* @param \Core\AppBundle\Entity\Serviciotarifario $serviciotarifario
	**/
	public function setServiciotarifario(\Core\AppBundle\Entity\Serviciotarifario $serviciotarifario){
		 $this->serviciotarifario = $serviciotarifario;
		 return $this;
	}

	/**
	* Get serviciotarifario
	* @return \Core\AppBundle\Entity\Serviciotarifario
	**/
	public function getServiciotarifario(){
		 return $this->serviciotarifario;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return serviciotarifario
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return serviciotarifario
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return serviciotarifario
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set activo
	* @param integer activo
	* @return serviciotarifario
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->activo=1;
$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
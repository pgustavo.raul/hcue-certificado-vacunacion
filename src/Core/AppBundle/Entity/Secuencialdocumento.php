<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.secuencialdocumento")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\SecuencialdocumentoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Secuencialdocumento{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.secuencialdocumento_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=true)
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var integer $secuencialdocumento_id
     * @ORM\Column(name="secuencialdocumento_id", type="integer", nullable=true)
     **/
    private $secuencialdocumento_id;

    /**
     * @ORM\ManyToOne(targetEntity="Secuencialdocumento")
     * @ORM\JoinColumn(name="secuencialdocumento_id", referencedColumnName="id")
     **/
    private $secuencialdocumento_padre;

    /**
     * @var integer $cttipodocumento_id
     * @ORM\Column(name="cttipodocumento_id", type="integer", nullable=false)
     **/
    private $cttipodocumento_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipodocumento_id", referencedColumnName="id")
     **/
    private $cttipodocumento;

    /**
     * @var integer $anio
     * @ORM\Column(name="anio", type="integer", nullable=true)
     **/
    private $anio;

    /**
     * @var string $formato
     * @ORM\Column(name="formato", type="string",length=100, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo formato")
     **/
    private $formato;

    /**
     * @var integer $longitud
     * @ORM\Column(name="longitud", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo longitud")
     **/
    private $longitud;

    /**
     * @var integer $secuencial
     * @ORM\Column(name="secuencial", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo secuencial")
     **/
    private $secuencial;

    /**
     * @var string $categoria
     * @ORM\Column(name="categoria", type="string",length=5, nullable=true)
     **/
    private $categoria;

    /**
     * @var boolean $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var \Datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var \Datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set entidad_id
     * @param integer $entidad_id
     * @return secuencialdocumento
     **/
    public function setEntidadId($entidad_id){
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId(){
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     * @return secuencialdocumento
     **/
    public function setEntidad(Entidad $entidad){
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidad(){
        return $this->entidad;
    }

    /**
     * Set secuencialdocumento_id
     * @param integer $secuencialdocumento_id
     * @return secuencialdocumento
     **/
    public function setSecuencialdocumentoId($secuencialdocumento_id){
        $this->secuencialdocumento_id = $secuencialdocumento_id;
        return $this;
    }

    /**
     * Get secuencialdocumento_id
     * @return integer
     **/
    public function getSecuencialdocumentoId(){
        return $this->secuencialdocumento_id;
    }

    /**
     * Set secuencialdocumentoPadre
     * @param \Core\AppBundle\Entity\Secuencialdocumento $secuencialdocumentoPadre
     * @return secuencialdocumento
     **/
    public function setSecuencialdocumentoPadre(Secuencialdocumento $secuencialdocumentoPadre=null) {
        $this->secuencialdocumento_padre = $secuencialdocumentoPadre;
        return $this;
    }

    /**
     * Get secuencialdocumento_padre
     * @return \Core\AppBundle\Entity\Secuencialdocumento
     **/
    public function getSecuencialdocumentoPadre() {
        return $this->secuencialdocumento_padre;
    }

    /**
     * Set cttipodocumento_id
     * @param integer $cttipodocumento_id
     * @return secuencialdocumento
     **/
    public function setCttipodocumentoId($cttipodocumento_id){
        $this->cttipodocumento_id = $cttipodocumento_id;
        return $this;
    }

    /**
     * Get cttipodocumento_id
     * @return integer
     **/
    public function getCttipodocumentoId(){
        return $this->cttipodocumento_id;
    }

    /**
     * Set cttipodocumento
     * @param \Core\AppBundle\Entity\Catalogo $cttipodocumento
     * @return secuencialdocumento
     **/
    public function setCttipodocumento(Catalogo $cttipodocumento){
        $this->cttipodocumento = $cttipodocumento;
        return $this;
    }

    /**
     * Get cttipodocumento
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCttipodocumento(){
        return $this->cttipodocumento;
    }

    /**
     * Set anio
     * @param integer $anio
     * @return secuencialdocumento
     **/
    public function setAnio($anio){
        $this->anio = $anio;
        return $this;
    }

    /**
     * Get anio
     * @return integer
     **/
    public function getAnio(){
        return $this->anio;
    }

    /**
     * Set formato
     * @param string $formato
     * @return secuencialdocumento
     **/
    public function setFormato($formato){
        $this->formato = $formato;
        return $this;
    }

    /**
     * Get formato
     * @return string
     **/
    public function getFormato(){
        return $this->formato;
    }

    /**
     * Set longitud
     * @param integer $longitud
     * @return secuencialdocumento
     **/
    public function setLongitud($longitud){
        $this->longitud = $longitud;
        return $this;
    }

    /**
     * Get longitud
     * @return integer
     **/
    public function getLongitud(){
        return $this->longitud;
    }

    /**
     * Set secuencial
     * @param integer $secuencial
     * @return secuencialdocumento
     **/
    public function setSecuencial($secuencial){
        $this->secuencial = $secuencial;
        return $this;
    }

    /**
     * Get secuencial
     * @return integer
     **/
    public function getSecuencial(){
        return $this->secuencial;
    }

    /**
     * Set categoria
     * @param string $categoria
     * @return secuencialdocumento
     **/
    public function setCategoria($categoria){
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * Get categoria
     * @return string
     **/
    public function getCategoria(){
        return $this->categoria;
    }

    /**
     * Set estado
     * @param integer $estado
     * @return secuencialdocumento
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer $activo
     * @return secuencialdocumento
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer $usuariomodificacion_id
     * @return secuencialdocumento
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer $usuariocreacion_id
     * @return secuencialdocumento
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechamodificacion=new \DateTime();
        $this->fechacreacion=new \DateTime();
        $this->activo=1;

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
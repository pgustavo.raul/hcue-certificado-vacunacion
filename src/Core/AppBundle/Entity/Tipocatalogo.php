<?php 

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Descripcion y definicion de tipos de catalogos
* @author Luis Malquin
* @ORM\Table(name="hcue_catalogos.catalogo")
* @ORM\Entity()
* @ORM\HasLifecycleCallbacks()
*/

class Tipocatalogo{   
    
	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	**/
	private $id;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=250, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo descripcion")
	**/
	private $descripcion;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;
                
        
	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return tipocatalogo
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set estado
	* @param integer estado
	* @return tipocatalogo
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

}
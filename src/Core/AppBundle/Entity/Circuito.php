<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.circuito")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\CircuitoRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Circuito{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.circuito_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $distrito_id
	* @ORM\Column(name="distrito_id", type="integer", nullable=false)
	**/
	private $distrito_id;

	/**
	* @ORM\ManyToOne(targetEntity="Distrito")
	* @ORM\JoinColumn(name="distrito_id", referencedColumnName="id")
	**/
	private $distrito;

	/**
	* @var string $codigo
	* @ORM\Column(name="codigo", type="string",length=20, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo codigo")
	**/
	private $codigo;

	/**
	* @var string $poblacion
	* @ORM\Column(name="poblacion", type="string",length=20, nullable=true)
	**/
	private $poblacion;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set distrito_id
	* @param integer distrito_id
	* @return circuito
	**/
	public function setDistritoId($distrito_id){
		 $this->distrito_id = $distrito_id;
		 return $this;
	}

	/**
	* Get distrito_id
	* @return integer
	**/
	public function getDistritoId(){
		 return $this->distrito_id;
	}

	/**
	* Set distrito
	* @param \Core\AppBundle\Entity\Distrito $distrito
	**/
	public function setDistrito(\Core\AppBundle\Entity\Distrito $distrito){
		 $this->distrito = $distrito;
		 return $this;
	}

	/**
	* Get distrito
	* @return \Core\AppBundle\Entity\Distrito
	**/
	public function getDistrito(){
		 return $this->distrito;
	}

	/**
	* Set codigo
	* @param string codigo
	* @return circuito
	**/
	public function setCodigo($codigo){
		 $this->codigo = $codigo;
		 return $this;
	}

	/**
	* Get codigo
	* @return string
	**/
	public function getCodigo(){
		 return $this->codigo;
	}

	/**
	* Set poblacion
	* @param string poblacion
	* @return circuito
	**/
	public function setPoblacion($poblacion){
		 $this->poblacion = $poblacion;
		 return $this;
	}

	/**
	* Get poblacion
	* @return string
	**/
	public function getPoblacion(){
		 return $this->poblacion;
	}

	/**
	* Set estado
	* @param integer estado
	* @return circuito
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return circuito
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return circuito
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->fechamodificacion=new \DateTime();
$this->fechacreacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}


	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
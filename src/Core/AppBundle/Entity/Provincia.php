<?php 

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Descripcion de provincia
* @author Luis Malquin
* @ORM\Table(name="hcue_catalogos.provincia")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\ProvinciaRepository")
*/
class Provincia{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.provincia_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=250, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo descripcion")
	**/
	private $descripcion;

	/**
	* @var string $codprovincia
	* @ORM\Column(name="codprovincia", type="string",length=150, nullable=true)
	**/
	private $codprovincia;


        /**
        * @var integer $ctregion_id
        * @ORM\Column(name="ctregion_id", type="integer", nullable=true)
        * */
        private $ctregion_id;

        /**
        * @ORM\ManyToOne(targetEntity="Catalogo")
        * @ORM\JoinColumn(name="ctregion_id", referencedColumnName="id")
        * */
        private $ctregion;
        
        /**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)			
	**/
	private $estado;
        
        
	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return provincia
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set codprovincia
	* @param string codprovincia
	* @return provincia
	**/
	public function setCodprovincia($codprovincia){
		 $this->codprovincia = $codprovincia;
		 return $this;
	}

	/**
	* Get codprovincia
	* @return string
	**/
	public function getCodprovincia(){
		 return $this->codprovincia;
	}

        
        /**
        * Set ctregion_id
        * @param integer ctregion_id
        * @return provincia
        * */
        public function setCtregionId($ctregion_id) {
            $this->ctregion_id = $ctregion_id;
            return $this;
        }

        /**
         * Get ctregion_id
         * @return integer
         * */
        public function getCtregionId() {
            return $this->ctregion_id;
        }

        /**
         * Set ctregion
         * @param \Core\AppBundle\Entity\Catalogo $ctregion
         * */
        public function setProvincia(\Core\AppBundle\Entity\Catalogo $ctregion) {
            $this->ctregion = $ctregion;
            return $this;
        }

        /**
         * Get ctregion
         * @return \Core\AppBundle\Entity\Catalogo
         * */
        public function getCtregion() {
            return $this->ctregion;
        }       
        
        
        /**
        * Set estado
        * @param integer estado
        * @return provincia
        * */
        public function setEstado($estado) {
            $this->estado = $estado;
            return $this;
        }
        
        /**
        * Get estado
        * @return integer
        * */
        public function getEstado() {
            return $this->estado;
        }
 
        /**
        * Obtiene el codigo y el nombre de la provincia concatenado  
        * @return string
        * */
        public function getCodigoAndDescripcion() {
            return $this->codprovincia.' '.$this->descripcion;
        }
        
        public function __toString() {
           return $this->descripcion;
        }
}
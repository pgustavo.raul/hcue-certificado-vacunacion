<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descripcion de persona
 * @ORM\Table(name="hcue_sistema.persona")
 * @ORM\Entity()
 */
class PersonaBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var string $numeroidentificacion
     * @ORM\Column(name="numeroidentificacion", type="string")
     **/
    private $numeroidentificacion;

    /**
     * @var string $apellidopaterno
     * @ORM\Column(name="apellidopaterno", type="string")
     **/
    private $apellidopaterno;

    /**
     * @var string $apellidomaterno
     * @ORM\Column(name="apellidomaterno", type="string")
     **/
    private $apellidomaterno;

    /**
     * @var string $primernombre
     * @ORM\Column(name="primernombre", type="string")
     **/
    private $primernombre;

    /**
     * @var string $segundonombre
     * @ORM\Column(name="segundonombre", type="string")
     **/
    private $segundonombre;

    /**
     * @var \Datetime $fechanacimiento
     * @ORM\Column(name="fechanacimiento", type="datetime")
     **/
    private $fechanacimiento;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogoBasic")
     * @ORM\JoinColumn(name="ctsexo_id", referencedColumnName="id")
     **/
    private $ctsexo;

    /**
     * @ORM\ManyToOne(targetEntity="ArchivoBasic")
     * @ORM\JoinColumn(name="foto_id", referencedColumnName="id")
     **/
    private $foto;

    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get numeroidentificacion
     * @return string
     **/
    public function getNumeroidentificacion()
    {
        return $this->numeroidentificacion;
    }

    /**
     * Get apellidopaterno
     * @return string
     **/
    public function getApellidopaterno()
    {
        return $this->apellidopaterno;
    }

    /**
     * Get apellidomaterno
     * @return string
     **/
    public function getApellidomaterno()
    {
        return $this->apellidomaterno;
    }

    /**
     * Get primernombre
     * @return string
     **/
    public function getPrimernombre()
    {
        return $this->primernombre;
    }

    /**
     * Get segundonombre
     * @return string
     **/
    public function getSegundonombre()
    {
        return $this->segundonombre;
    }

    /**
     * Get nombrecompleto
     * @return string
     **/
    public function getNombrecompleto()
    {
        return $this->primernombre.' '. $this->segundonombre.' '.$this->apellidopaterno.' '. $this->apellidomaterno;
    }

    public function getNombres()
    {
        return $this->primernombre.' '. $this->segundonombre;
    }

    public function getApellidos()
    {
        return $this->apellidopaterno.' '. $this->apellidomaterno;
    }

    /**
     * Get fechanacimiento
     * @return \Datetime
     **/
    public function getFechanacimiento()
    {
        return $this->fechanacimiento;
    }

    /**
     * Get ctsexo
     * @return CatalogoBasic
     **/
    public function getCtsexo()
    {
        return $this->ctsexo;
    }

    /**
     * Get foto
     * @return ArchivoBasic
     **/
    public function getFoto()
    {
        return $this->foto;
    }

}
<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Establecimientos de salud
 * @ORM\Table(name="hcue_sistema.entidad")
 * @ORM\Entity()
 */
class EntidadBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var string $codigo
     * @ORM\Column(name="codigo", type="string")
     **/
    private $codigo;

    /**
     * @var string $nombreoficial
     * @ORM\Column(name="nombreoficial", type="string")
     **/
    private $nombreoficial;

    /**
     * @ORM\ManyToOne(targetEntity="ParroquiaBasic")
     * @ORM\JoinColumn(name="parroquia_id", referencedColumnName="id")
     **/
    private $parroquia;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get codigo
     * @return string
     **/
    public function getCodigo(){
        return $this->codigo;
    }

    /**
     * Get nombreoficial
     * @return string
     **/
    public function getNombreoficial(){
        return $this->nombreoficial;
    }

    /**
     * Get parroquia
     * @return ParroquiaBasic
     **/
    public function getParroquia(){
        return $this->parroquia;
    }

}
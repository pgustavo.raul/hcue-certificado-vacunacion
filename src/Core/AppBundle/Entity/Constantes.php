<?php

namespace Core\AppBundle\Entity;

/*
 * Clase para la definición de constantes
 * @autor Luis Malquin
 */

class Constantes
{
    /*
     * Constantes generales
     */

    /**
     * @const integer indica el estado general habilitado o activo
     */
    const ESTADOGENERALHABILITADO = 1;

    /**
     * @const integer indica el estado general deshabilitado o inactivo
     */
    const ESTADOGENERALDESHABILITADO = 0;

    /**
     * @const integer Id del organico de primer nivel de atención
     */
    const OG_PRIMERNIVELATENCION = 7;

    /**
     * @const integer Id del tipo de establecimiento de primer nivel de atención
     */
    const TE_PRIMERNIVELATENCION = 2;

    /**
     * @const integer Id del tipo de establecimiento de primer nivel de atención
     */
    const TE_SEGUNDONIVELATENCION = 17;

    /**
     * @const integer Id del tipo de establecimiento de primer nivel de atención
     */
    const TE_TERCERNIVELATENCION = 23;

    /*
     * Constantes de tipo de catalogo
     * TC Abreviatura de Tipo Catalogo
     */

    /**
     * @const integer Id del tipo de catalogo para los estados generales
     */
    const TC_ESTADOGENERAL = 1;

    /**
     * @const integer Id del tipo de catalogo para los estados de decision
     */
    const TC_ESTADODECISION = 2;

    /**
     * @const integer Id del tipo de catalogo para los tipos de identificacion
     */
    const TC_TIPOIDENTIFICACION = 3;

    /**
     * @const integer Id del tipo de catalogo para los estados civiles
     */
    const TC_ESTADOCIVIL = 4;

    /**
     * @const integer Id del tipo de catalogo para el sexo
     */
    const TC_SEXO = 5;

    /**
     * @const integer Id del tipo catalogo para la orientación sexual
     */
    const TC_ORIENTACIONSEXUAL = 36;

    /**
     * @const integer Id del tipo catalogo para la identidad de género
     */
    const TC_IDENTIDADGENERO = 37;

    /**
     * @const integer Id del tipo de catalogo para autoidentificacion
     */
    const TC_AUTOIDENTIFICACIONETNICA = 6;

    /**
     * @const integer Id del tipo de catalogo para las regiones naturales del Ecuador
     */
    const TC_REGIONESNATURALES = 15;

    /**
     * @const integer Id del tipo de catalogo para áreas de residencias
     */
    const TC_AREASRESIDENCIA = 9;

    /**
     * @const integer Id del tipo de catalogo para tipos de empresa
     */
    const TC_TIPOSEMPRESA = 10;

    /**
     * @const integer Id del tipo de catalogo para tipos de seguros de salud
     */
    const TC_TIPOSSEGUROSALUD = 11;

    /**
     * @const integer Id del tipo de catalogo para tipos de bonos del estado
     */
    const TC_TIPOSBONOESTADO = 12;

    /**
     * @const integer Id del tipo de catalogo para los niveles de instrucción
     */
    const TC_NIVELESINSTRUCCION = 13;


    /**
     * @const integer Id del tipo catalogo para identificar las secciones de los formularios
     */
    const TC_TIPODOCUMENTOS = 38;

    /**
     * @const integer Id del tipo de catalogo opciones de discapacidad
     */
    const TC_DISCAPACIDAD = 72;

    /**
     * @const integer Id del tipo de catalogo para los tipos de discapacidad
     */
    const TC_TIPODISCAPACIDAD = 68;
    

    /*
     * Constantes de detalle catalogos
     * CT Abreviatura de catalogos
     */


    /**
     * @const integer Id del registro catalogo de estado general activo
     */
    const CT_ESTADOGENERALACTIVO = 1;

    /**
     * @const integer Id del registro catalogo de estado general inactivo
     */
    const CT_ESTADOGENERALINACTIVO = 2;

    /**
     * @const integer Id del registro catalogo de tipo de identificacion no identificado
     */
    const CT_TIPOIDENTNOIDENTIFICADO = 5;

    /**
     * @const integer Id del registro catalogo de tipo de identificacion cédula
     */
    const CT_TIPOIDENTCEDULA = 6;

    /**
     * @const integer Id del registro catalogo de tipo de identificacion pasaporte
     */
    const CT_TIPOIDENTPASAPORTE = 7;

    /**
     * @const integer Id del registro catalogo de tipo de identificacion visa
     */
    const CT_TIPOIDENTVISA = 8;

    /**
     * @const integer Id del registro catalogo de tipo de identificacion carnet de refugiado
     */
    const CT_TIPOIDENTCARNETREFUGIADO = 9;

    /**
     * @const integer Id del registro detalle catalogo nacionalidad ecuatoriana
     */
    const CT_NACIONALIDAD_ECUATORIANA = 1;

    /**
     * @const integer Id del registro detalle de sexo hombre
     */
    const CT_SEXO_HOMBRE = 16;

    /**
     * @const integer Id del registro detalle de sexo mujer
     */
    const CT_SEXO_MUJER = 17;

    /**
     * @const integer Id del registro detalle de sexo mujer
     */
    const CT_SEXO_INTERSEXUAL = 114;

    /**
     * @const integer Id  Detalle Catalogo para registrar la identidad de género transmasculino
     */
    const CT_IDENTIDAD_GENERO_TRANSMASCULINO = 677;

    /**
     * @const integer Id  Detalle Catalogo para registrar la identidad de género transfemenina
     */
    const CT_IDENTIDAD_GENERO_TRANSFEMENINA = 678;

    /**
     * @const integer Id  Detalle Catalogo para registrar la orientación sexual lesbiana
     */
    const CT_ORIENTACION_SEXUAL_LESBIANA = 671;

    /**
     * @const integer Id  Detalle Catalogo para registrar la orientación sexual gay
     */
    const CT_ORIENTACION_SEXUAL_GAY = 672;

    /**
     * @const integer Id del registro detalle catalogo seguro ISSFA
     */
    const CT_SEGURO_ISSFA = 76;

    /**
     * @const integer Id del registro detalle catalogo seguro ISSPOL
     */
    const CT_SEGURO_ISSPOL = 77;

    /**
     * @const integer Id del registro detalle catalogo seguro ISSPOL
     */
    const CT_SEGURO_NO_APORTA = 82;

    /**
     * @const integer Id del registro detalle de catalogo webservis de discapacidad sin respuesta
     */
    const CT_WS_DISCAPACIDAD_SIN_RESPUESTA = 1130;

    /**
     * @const integer Id del registro detalle de catalogo webservis de discapacidad no aplica
     */
    const CT_WS_DISCAPACIDAD_NO_APLICA = 1129;

    /**
     * @const integer Id del registro detalle de catalogo webservis de discapacidad respuesta NO
     */
    const CT_WS_DISCAPACIDAD_RESPUESTA_NO = 1128;

    /**
     * @const integer Id del registro detalle de catalogo webservis de discapacidad respuesta SI
     */
    const CT_WS_DISCAPACIDAD_RESPUESTA_SI = 1127;

    /**
     * @const integer Id del registro detalle de catalogo webservis de discapacidad respuesta SI
     */
    const CT_CLASIFICACION_UNIDAD_ADMINISTRATIVA = 232;


    /**
     * @const integer Id del usuario de sistema para registro por defecto
     */
    const CT_USUARIO_REGISTRO_DEFECTO = 1;

    /**
     * @const id de la tabla profesion para nivel de instrucción se ignora
     */
    const PROFESION_NINGUNA = 442;

    /**
     * @const id de la tabla catalogo para nivel de instrucción se ignora
     */
    const CT_NIVEL_INSTRUCCION_SE_IGNORA=119;

    /**
     * @const id de la tabla detallecatalogo para nivel para el tipo de bono ninguno
     */
    const CT_TIPO_BONO_NINGUNO=88;

    /**
     * @const id de la tabla detallecatalogo  para identificar el origen del dato RDACAA 2.0
     */
    const CT_TIPO_EMPRESA_TRABAJO_NINGUNO=74;

    /**
     * @const id de la tabla detallecatalogo para identificar el parentesco del contacto otro familiar
     */
    const CT_PARENTESCO_CONTACTO_OTRO_FAMILIAR=68;

    /**
     * @const id de la tabla detallecatalogo para identificar una etnia que no sabe no responde
     */
    const CT_ETNIA_NO_SABE_RESPONDE=26;

    /**
     * @const id de la tabla detallecatalogo para identificar una Unidad Operativa
     */
    const CTCLASIFICACION_ID = 231;
}

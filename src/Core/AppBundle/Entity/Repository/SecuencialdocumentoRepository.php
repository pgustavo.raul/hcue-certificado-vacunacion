<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\AppBundle\Entity\Constantes;

class SecuencialdocumentoRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Secuencialdocumento
     * @return QueryBuilder
     */
    public function getSecuencialdocumentoQueryBuilder() {
        return $this->createQueryBuilder('sd')
            ->select('sd');
    }

    /**
     * Obtiene el QueryBuilder de la lista de hijos de una Entidad
     * @param integer $entidad_id Entidad a la que pertenecen los secuenciales
     * @param integer $parent_id
     * @param boolean $todos true Permite devolver todos los secuenciales asociados a la Entidad
     * @return QueryBuilder
     */
    public function obtenerhijosSecuenciales($entidad_id, $parent_id=0, $todos=false){
        $qb=$this->createQueryBuilder('sd')
            ->select('sd')
            ->Where('sd.estado = :estado_general')
            ->andWhere('sd.entidad_id = :entidad')
            ->andWhere('sd.activo = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'entidad' => $entidad_id
            ]);

        if(!$todos){
            if(!empty($parent_id) && is_numeric($parent_id)){
                $qb->andWhere('sd.secuencialdocumento_id = :secuencialdocumento_padre')
                    ->setParameter('secuencialdocumento_padre', $parent_id);
            }
            else{
                $qb->andWhere('sd.secuencialdocumento_id is null');
            }
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * Obtiene el maximo del secuncial por tipo documento
     * @param integer $tipodocumento_id Id del tipo de documento
     * @param integer $entidad_id Id de la entidad
     * @param integer $anio año del documento
     * @return integer
     */
    public function getMaxSecuencial($tipodocumento_id,$entidad_id=0,$anio=0){
        $queryBuilder=$this->getSecuencialdocumentoQueryBuilder();
        $query=$queryBuilder->select('MAX(sd.secuencial) secuencial')
            ->Where('sd.activo = :estado_general')
            ->andWhere('sd.cttipodocumento_id=:tipodocumento_id')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'tipodocumento_id' => $tipodocumento_id
            ]);
        if($entidad_id>0){
            $query->andWhere('sd.entidad_id=:entidad_id')
                ->setParameter('entidad_id',$entidad_id);
        }else {
            $query
                ->andWhere('sd.entidad_id is null');
        }
        if($anio>0){
            $query->andWhere('sd.anio=:anio')
                ->setParameter('anio',$anio);
        }else {
            $query
                ->andWhere('sd.anio is null');
        }
        $response=$query->getQuery()->getSingleScalarResult();
        return ($response)?$response:1;
    }

    /**
     * Obtiene el secuncial por tipo documento
     * @param integer $tipodocumento_id Id del tipo de documento
     * @param integer $entidad_id Id de la entidad
     * @param integer $anio año del documento
     * @return object
     */
    public function getSecuencial($tipodocumento_id,$entidad_id=0,$anio=0)
    {
        $queryBuilder = $this->getSecuencialdocumentoQueryBuilder();
        $query = $queryBuilder
            ->Where('sd.activo = :estado_general')
            ->andWhere('sd.cttipodocumento_id=:tipodocumento_id')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'tipodocumento_id' => $tipodocumento_id
            ]);
        if ($entidad_id > 0) {
            $query->andWhere('sd.entidad_id=:entidad_id')
                ->setParameter('entidad_id', $entidad_id);
        } else {
            $query
                ->andWhere('sd.entidad_id is null');
        }
        if ($anio > 0) {
            $query->andWhere('sd.anio=:anio')
                ->setParameter('anio', $anio);
        }else{
            $query
                ->andWhere('sd.anio is null');
        }
        return $query->getQuery()->getOneOrNullResult();
    }
}
<?php

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Descripcion de CantonRepository
 * @author Luis Malquin
 * Modificado por: Diego Orellana
 */
class PaisRepository extends EntityRepository
{    
    
    /**
     * Obtiene la lista de paises
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Pais"
     */
    public function getPaises($arrayResult=false) {
        $qb = $this->getPaisQueryBuilder();
        $q = $qb->getQuery();
        
        $resultId=($arrayResult)?"result_array_paises":"result_paises";
        $q->useQueryCache(true)
              ->useResultCache(true,null,$resultId);
        
        $result=null;
        if ($arrayResult) {
           $result=$q->getArrayResult();
        } else {
            $result = $q->getResult();
        }
        return $result;
    }

        
    /**
     * Obtiene el QueryBuilder de la lista de paises
     * @return QueryBuilder
     */
    public function getPaisQueryBuilder() {
        return $this->createQueryBuilder('p')
                        ->select('p')
                        ->orderBy('p.nombre', 'ASC')
            ;
    }
    
    /**
     * Obtiene el QueryBuilder de la lista de nacionalidades
     * @return QueryBuilder
     */
    public function getNacionalidadQueryBuilder() {
        return $this->createQueryBuilder('p')
                        ->select('p')
                        ->orderBy('p.nacionalidad', 'ASC')
            ;
    }
    
    /**
     * Obtiene un registro de una Pais por la nacionalidad
     * @param integer $nacionalidad nacinalidad
     * @return $parroquia retorna el objeto parroquia
     */
    public function searchNacionalidad($nacionalidad) {
        $qb=  $this->createQueryBuilder('p');
        $qb
                ->andWhere($qb->expr()->like('p.nacionalidad',':nacionalidad'))
                ->setParameter('nacionalidad', '%'.$nacionalidad.'%')
                ->setMaxResults(1)
                ;
        $query=$qb->getQuery();
        $desc=  str_replace(" ",'', $nacionalidad);
        $resultId="result_search_nacionalidad_$desc";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);
        return $query->getOneOrNullResult();
    }

    /**
     * Obtiene un registro de un Pais por la descripcion
     * @param integer $descripcion descripcion
     * @return $pais retorna el objeto pais
     */
    public function searchPais($descripcion) {
        $qb = $this->createQueryBuilder('p');
        $query=$qb
            ->Where("p.nombre =:pais_descripcion")
            ->setParameter('pais_descripcion', $descripcion)
            ->getQuery();
        $query->useQueryCache(true)
            ->useResultCache(true, null, "pais_id");
        return $query->getOneOrNullResult();
    }
}
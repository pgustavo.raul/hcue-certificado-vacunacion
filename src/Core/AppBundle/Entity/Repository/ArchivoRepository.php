<?php
/**
 * @autor Richard Veliz
 */

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ArchivoRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Tipoestablecimiento
     * @return QueryBuilder
     */
    public function getArchivoQueryBuilder() {
        return $this->createQueryBuilder('ar')
            ->select('ar')
            ->where("activo=1");
    }
}
<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ZonaRepository extends EntityRepository{

	/**
    * Obtiene el QueryBuilder de la lista de Zona
    * @return QueryBuilder
    */
	public function getZonaQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');
    }
}
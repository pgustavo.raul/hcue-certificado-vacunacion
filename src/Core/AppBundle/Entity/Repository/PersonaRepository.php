<?php

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Descripcion de PersonaRepository
 * @author Luis Malquin
 */
class PersonaRepository extends EntityRepository
{
    /**
     * Obtiene una persona por medio del numero de identificacion
     * @param string $identificacion número de identificación personal
     * @param boolean $arrayResult <true> para retornar el modelo o entidad como un array
     * @return Array u Objeto de tipo "Core\AppBundle\Entity\Persona"
     */
    public function getPersonaPorIdentificacion($identificacion, $arrayResult = false)
    {
        $qb = $this->getPersonaPorIdentificacionQueryBuilder($identificacion);
        $q = $qb->getQuery();
        return ($arrayResult)?$q->getArrayResult():$q->getOneOrNullResult();
    }

    /**
     * Obtiene el QueryBuilder de la consulta
     * @param string $identificacion número de identificación personal
     * @return QueryBuilder
     */
    public function getPersonaPorIdentificacionQueryBuilder($identificacion)
    {
        return $this->createQueryBuilder('pe')
            ->select('pe')
            ->where('pe.numeroidentificacion=:identificacion')
            ->setParameter('identificacion', $identificacion);

    }

    /**
     * Verifica si existe la persona por numero de identificacion
     * @param string $identificacion número de identificación personal
     * @return Boolean
     */
    public function existPersona($identificacion)
    {
        $qb=$this->createQueryBuilder('pe')
            ->select('count(pe.id)')
            ->where('pe.numeroidentificacion=:identificacion')
            ->setParameter('identificacion', $identificacion)
            ->setMaxResults(1);

        return (intval($qb->getQuery()->getSingleScalarResult())>0);
    }

    /**
     * Obtiene la lista de personas por medio del arreglo de identificaciones
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Persona"
     */
    public function getPersonaByArrayIdentificacion($arrayIdentificacion) {
        $qb=$this->createQueryBuilder('pe')
            ->select("pe")
            ->Where("pe.numeroidentificacion in(:arrayIdentificacion)")
            ->setParameter('arrayIdentificacion', $arrayIdentificacion)
        ;
        $query = $qb->getQuery();
        return $query->getArrayResult();
    }
}
<?php

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\AppBundle\Entity\Constantes;

/**
 * Descripcion de ParroquiaRepository
 * @author Luis Malquin
 */
class ParroquiaRepository extends EntityRepository {

    /**
     * Obtiene la lista de parroquias identificados por un canton
     * @param int $canton_id id del canton
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Parroquia"
     */
    public function getParroquias($canton_id,$arrayResult=false) {
        $qb = $this->getParroquiasQueryBuilder($canton_id);
        $q = $qb->getQuery();
        $resultId=($arrayResult)?"result_array_parroquias_by_canton_$canton_id":"result_parroquias_by_canton_$canton_id";
        $q->useQueryCache(true)
            ->useResultCache(true,null,$resultId);

        $result=null;
        if ($arrayResult) {
            $result=$q->getArrayResult();
        } else {
            $result = $q->getResult();
        }
        return $result;
    }

    /**
     * Obtiene el QueryBuilder de la lista de parroquias identificados por un canton
     * @param int $canton_id id del canton
     * @return QueryBuilder
     */
    public function getParroquiasQueryBuilder($canton_id) {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where("p.canton_id=:canton_id")
            ->andWhere("p.estado=:estado")
            ->setParameter('canton_id', $canton_id)
            ->setParameter('estado',Constantes::ESTADOGENERALHABILITADO)
            ->orderBy('p.descripcion','asc')
            ;
    }

    /**
     * Obtiene el QueryBuilder de la ubicacion (parroquia,canton y provincia) por id de la parroquia
     * @param int $id id de de la parroquia
     * @return QueryBuilder
     */
    public function getUbicacionByIdQueryBuilder($id) {
        return $this->createQueryBuilder('p')
            ->select('p,c,pr')
            ->innerJoin('p.canton','c')
            ->innerJoin('c.provincia','pr')
            ->where("p.id=:id")
            ->andWhere("p.estado=:estado")
            ->setParameter('id', $id)
            ->setParameter('estado',Constantes::ESTADOGENERALHABILITADO)
            ->setMaxResults(1)
            ;
    }


    /**
     * Obtiene la ubicacion (parroquia,canton y provincia) por id de la parroquia
     * @param int $id id de la parroquia
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Parroquia"
     */
    public function getUbicacionById($id,$arrayResult=false) {
        $qb = $this->getUbicacionByIdQueryBuilder($id);
        $q = $qb->getQuery();
        $resultId=($arrayResult)?"result_array_parroquia_ubicacion_by_id_$id":"result_parroquia_ubicacion_by_id_$id";
        $q->useQueryCache(true)
            ->useResultCache(true,null,$resultId);

        $result=null;
        if ($arrayResult) {
            $result=$q->getArrayResult();
        } else {
            $result = $q->getOneOrNullResult();
        }
        return $result;
    }

    /**
     * Obtiene un registro de una Parroquia por la descripcion
     * @param integer $descripcion descripcion
     * @return $parroquia retorna el objeto parroquia
     */
    public function searchParroquia($canton_id,$descripcion) {
        $qb=  $this->createQueryBuilder('p')

            ->where("p.estado=:estado")
            ->andWhere("p.descripcion=:parroquia_descripcion")
            ->andWhere("p.canton_id=:canton_id")
            ->setParameter('parroquia_descripcion', $descripcion)
            ->setParameter('canton_id', $canton_id)
            ->setParameter('estado',Constantes::ESTADOGENERALHABILITADO)
            ->setMaxResults(1);

        $query=$qb->getQuery();
        $desc=  str_replace(" ",'', $descripcion);
        $resultId="result_search_parroquia_by_canton_{$canton_id}_$desc";
        $query->useQueryCache(true)
            ->useResultCache(true,null,$resultId);
        return $query->getOneOrNullResult();
    }
}

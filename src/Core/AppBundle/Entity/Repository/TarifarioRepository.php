<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class TarifarioRepository extends EntityRepository{

	/**
    * Obtiene el QueryBuilder de la lista de Tarifario
    * @return QueryBuilder
    */
	public function getTarifarioQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');
    }
}
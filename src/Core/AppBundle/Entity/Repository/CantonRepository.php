<?php
namespace Core\AppBundle\Entity\Repository;

use Core\AppBundle\Entity\Constantes;
use Doctrine\ORM\EntityRepository;

/**
 * Descripcion de CantonRepository
 * @author Luis Malquin
 */
class CantonRepository extends EntityRepository {
    
    /**
     * Obtiene la lista de cantones identificados por una provincia
     * @param integer $provincia_id id de la provincia
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Canton"
     */
    public function getCantones($provincia_id,$arrayResult=false) {
        $qb = $this->getCantonesQueryBuilder($provincia_id);
        $q = $qb->getQuery();
        
        $resultId=($arrayResult)?"result_array_cantones_by_provincia_$provincia_id":"result_cantones_by_provincia_$provincia_id";
        $q->useQueryCache(true)
              ->useResultCache(true,null,$resultId);
        
        $result=null;
        if ($arrayResult) {
           $result=$q->getArrayResult();
        } else {
            $result = $q->getResult();
        }
        return $result;
    }

    /**
     * Obtiene el QueryBuilder de la lista de cantones identificados por una provincia
     * @param integer $provincia_id id de la provincia
     * @return QueryBuilder
     */
    public function getCantonesQueryBuilder($provincia_id) {
        return $this->createQueryBuilder('c')
                        ->select('c')
                        ->where('c.provincia_id=:provincia_id')
                        ->andWhere("c.estado= :estado")
                        ->orderBy('c.descripcion', 'ASC')
                        ->setParameter('provincia_id', $provincia_id)
            ->setParameter("estado",Constantes::CT_ESTADOGENERALACTIVO)
            ;

    }
    
    /**
     * Obtiene un registro de un Canton por la descripcion
     * @param integer $provincia_id Id de la provincia
     * @param integer $descripcion descripcion
     * @return $canton retorna el objeto canton
     */     
    public function searchCanton($provincia_id,$descripcion) {
        $qb=  $this->createQueryBuilder('c');
        $query= $qb
                ->Where($qb->expr()->like("c.descripcion",":canton_descripcion"))
                ->andWhere("c.estado=1")
                ->andWhere("c.provincia_id=:provincia_id")
                ->setParameter('canton_descripcion', "%".$descripcion."%")
                ->setParameter('provincia_id', $provincia_id)
                ->setMaxResults(1)
                ->getQuery();
        
        $desc=  str_replace(" ",'', $descripcion);
        $resultId="result_search_canton_by_provincia_$provincia_id"."_$desc";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);               
        
        return $query->getOneOrNullResult();
    }
}

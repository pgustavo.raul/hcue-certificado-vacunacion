<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\AppBundle\Entity\Constantes;

class OrganicoRepository extends EntityRepository{

    /**
     * Obtiene la Estructura Orgánica
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @param integer $estado para retornar las entidades de acuerdo a su estado (activo, inactivo)
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Organico"
     */
    public function getOrganicos($arrayResult=false, $estado=Constantes::ESTADOGENERALHABILITADO) {

        $qb = $this->createQueryBuilder('o')
            ->select('o')
            ->where('o.estado = :estado')
            ->setParameter('estado', $estado);
        $query = $qb->getQuery();
        $resultId=($arrayResult)?"result_array_organico_by_estado_$estado":"result_organico_by_estado_$estado";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de la Estructura Orgánica Raíz
     * @param integer $estado
     * @return QueryBuilder
     */
    public function getParentOrganico($estado){

        $qb = $this->createQueryBuilder("o")
                   ->select("o")
                   ->where("o.organico_id is null")
                   ->orderBy("o.nivel","ASC");

        if(!empty($estado) && is_numeric($estado)){
            $qb->andWhere("o.estado = $estado");
        }
        $query=$qb->getQuery();
        $resultId="result_organico_parents_by_estado_$estado";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);

        return $query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de los hijos de un Estructura Orgánica
     * @param integer $organico_id
     * @return QueryBuilder
     */
    public function getChildsOrganico($organico_id){

        $qb = $this->createQueryBuilder("o")
                   ->select("o")
                   ->Where("o.organico_id = $organico_id")
                   ->orderBy("o.nivel","ASC");

        $query = $qb->getQuery();
        $resultId="result_organico_childrens_by_organico_$organico_id";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);

        return $query->getResult();

    }

    /**
     * Obtiene todos los hijos y sus hijos del organico
     * @param integer $organico_id
     * @param integer $getIds
     * @return QueryBuilder
     */
    public function getAllChildsOrganico($organico_id){

        $query ="
           select ID, ORGANICO_ID, CODIGO,  DESCRIPCION,  NIVEL, ESTADO,  HOMOLOGACION
           from HCUE_CATALOGOS.ORGANICO a
             start with id = $organico_id
            connect by prior a.id = A.organico_id
            order by level"
        ;

        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare($query);
        $statement->execute();
        $arraysertar = $statement->fetchAll();

        return $arraysertar;


    }


}
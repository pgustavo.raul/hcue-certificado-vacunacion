<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class DetalleserviciotarifarioRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Detalleserviciotarifario
     * @return QueryBuilder
     */
    public function getDetalleserviciotarifarioQueryBuilder() {
        return $this->createQueryBuilder('t')
            ->select('t');
    }


    /**
     * Obtiene los registros en  jerarquia descendente  por serviciotarifario_id
     * @param integer $atencionmedica_id
     * @return boolean
     */
    public function getChildrenByServiciotarifarioId($serviciotarifario_id,$arrayResult=false)
    {
        $result=null;
        $lst=$this->getSqlNativeAllChildrens($serviciotarifario_id);
        if($lst){
            $qb=$this->createQueryBuilder('t')
                ->select('t')
                ->where("t.serviciotarifario_id in ($lst)")
                ->orderBy('t.serviciotarifario_id');
            $q = $qb->getQuery();

            $resultId=($arrayResult)?"result_array_tarifario_by_tipo_$serviciotarifario_id":"result_tarifario_by_tipo_$serviciotarifario_id";
            $q->useQueryCache(true)
                ->useResultCache(true,86400,$resultId);

            $result= ($arrayResult)?$q->getArrayResult():$q->getResult();
        }

        return $result;
    }


    private function getSqlNativeAllChildrens($serviciotarifario_id){
        $query = " SELECT id
              FROM HCUE_CATALOGOS.SERVICIOTARIFARIO
              START WITH SERVICIOTARIFARIO_ID = $serviciotarifario_id
              CONNECT BY PRIOR id = SERVICIOTARIFARIO_ID";
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare($query);
        $statement->execute();
        $arraysertar = $statement->fetchAll();

        $lst="";
        if($arraysertar){
            foreach ($arraysertar as $sertar ){
                $arrayids[]=$sertar['ID'];
            }
            $lst=implode(",",$arrayids);
        }
        return $lst;
    }
}
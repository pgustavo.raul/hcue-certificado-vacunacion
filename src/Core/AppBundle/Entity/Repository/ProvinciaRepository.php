<?php

namespace Core\AppBundle\Entity\Repository;

use Core\AppBundle\Entity\Constantes;
use Doctrine\ORM\EntityRepository;

/**
 * Descripcion de ProvinciaRepository
 * @author Luis Malquin
 */
class ProvinciaRepository extends EntityRepository
{    
   
    /**
     * Obtiene la lista de provincias
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Provincia"
     */
    public function getProvincias($arrayResult=false,$estado=null) {
        $qb = $this->getProvinciaQueryBuilder($estado);
        $q = $qb->getQuery();
        
        $resultId=($arrayResult)?"result_array_provincias":"result_provincias";
        $q->useQueryCache(true)
              ->useResultCache(true,null,$resultId);
        
        $result=null;
        if ($arrayResult) {
           $result=$q->getArrayResult();
        } else {
            $result = $q->getResult();
        }
        return $result;
    }

     /**
     * Obtiene el QueryBuilder de la lista de provincias
     * @return QueryBuilder
     */
    public function getProvinciaQueryBuilder($estado=null) {
        $qb=$this->createQueryBuilder('p')
                        ->select('p')
                        ->orderBy('p.descripcion', 'ASC');

        if($estado){
            $qb ->Where("p.estado = :estado")
                ->setParameter("estado",Constantes::CT_ESTADOGENERALACTIVO)
            ;
        }
        return $qb;
    }

     /**
     * Obtiene un registro de una Provincia por la descripcion
     * @param integer $descripcion descripcion
     * @return $provincia retorna el objeto provincia
     */
    public function searchProvincia($descripcion) {
        $qb=  $this->createQueryBuilder('p');
        $query=$qb
                ->andWhere($qb->expr()->like("p.descripcion",":provincia_descripcion"))
                ->setParameter('provincia_descripcion', $descripcion."%")
                ->setMaxResults(1)
                ->getQuery();
        $desc=  str_replace(" ",'', $descripcion);
        $resultId="result_search_provincia_$desc";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);
        return $query->getOneOrNullResult();
    }
}
<?php
namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Descripcion de ProfesionRepository
 * @author Richard Veliz
 */
class ProfesionRepository extends EntityRepository {
    
    /**
     * Obtiene el QueryBuilder de la lista de profesiones
     * @return QueryBuilder
     */
    public function getProfesionQueryBuilder() {
        return $this->createQueryBuilder('p')
                        ->select('p')
                        ->orderBy('p.nombre', 'ASC')
            ;
    }

    /**
     * Obtiene la lista de profesiones
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Profesion"
     */
    public function getProfesiones($arrayResult=false) {
        $qb = $this->getProfesionQueryBuilder();
        $q = $qb->getQuery();
        $resultId=($arrayResult)?"result_array_profesiones":"result_profesiones";
        $q->useQueryCache(true)
            ->useResultCache(true,null,$resultId);

        $result=null;
        if ($arrayResult) {
            $result=$q->getArrayResult();
        } else {
            $result = $q->getResult();
        }
        return $result;
    }
}
<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class CircuitoRepository extends EntityRepository{

	/**
    * Obtiene el QueryBuilder de la lista de Circuito
    * @return QueryBuilder
    */
	public function getCircuitoQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');
    }
}
<?php
namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\AppBundle\Entity\Constantes;

/**
 * Descripcion de CatalogoRepository
 * @author Luis Malquin
 */
class CatalogoRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de catalogos identificados por un tipo de catálogo
     * @param integer $tipocatalogo_id identifica el tipo de catálogo
     * @return QueryBuilder
     */
    public function getCatalogoQueryBuilder($tipocatalogo_id)
    {
        return $this->createQueryBuilder('ct')
            ->Where('ct.estado=:estado')
            ->andWhere('ct.tipocatalogo_id=:tipocatalogo_id')
            ->andWhere('ct.catalogo_id is null')
            ->orderBy('ct.orden, ct.descripcion', 'ASC')
            ->setParameter('tipocatalogo_id', $tipocatalogo_id)
            ->setParameter('estado',Constantes::ESTADOGENERALHABILITADO);
    }

    /**
     * Obtiene la lista de catalogos identificados por un tipo de catálogo
     * @param integer $tipocatalogo_id identifica el tipo de catálogo
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Catalogo"
     */
    public function getCatalogo($tipocatalogo_id, $arrayResult = false)
    {
        $queryBuilder = $this->getCatalogoQueryBuilder($tipocatalogo_id);
        $q = $queryBuilder->getQuery();
        $resultId = ($arrayResult) ? "result_array_catalogo_by_tipo_$tipocatalogo_id":"result_catalogo_by_tipo_$tipocatalogo_id";
        $q->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de catalogos identificados por el catálogo padre
     * @param integer $catalogo_id identifica el catálogo padre
     * @return QueryBuilder
     */
    public function getCatalogoChildrensQueryBuilder($catalogo_id)
    {
        return $this->createQueryBuilder('ct')
            ->Where('ct.estado=:estado')
            ->andWhere('ct.catalogo_id=:catalogo_id')
            ->orderBy('ct.orden, ct.descripcion', 'ASC')
            ->setParameter('catalogo_id', $catalogo_id)
            ->setParameter('estado',Constantes::ESTADOGENERALHABILITADO);
    }

    /**
     * Obtiene la lista de catalogos identificados por un catálogo padre
     * @param integer $catalogo_id identifica el catálogo padre
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "AppBundle\Entity\Catalogo"
     */
    public function getCatalogoChildrens($catalogo_id, $arrayResult = false)
    {
        $queryBuilder = $this->getCatalogoChildrensQueryBuilder($catalogo_id);
        $q = $queryBuilder->getQuery();
        $resultId = ($arrayResult) ? "result_array_children_by_catalogo_$catalogo_id" : "result_children_by_catalogo_$catalogo_id";
        $q->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene la lista de catalogos recursiva identificados por tipo de catalogo o por un catálogo padre
     * @param integer $tipocatalogo_id identifica el tipo de catálogo
     * @param integer $catalogo_id identifica el catálogo padre
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "AppBundle\Entity\Catalogo"
     */
    public function getCatalogoAllChildrens($tipocatalogo_id, $catalogo_id = 0, $arrayResult = false)
    {
        $aResult = ($tipocatalogo_id > 0) ? $this->getCatalogo($tipocatalogo_id,
            $arrayResult) : $this->getCatalogoChildrens($catalogo_id, $arrayResult);
        $arrayReturn = array();
        foreach ($aResult as $obj) {
            $arrayReturn[] = $obj;
            $aResult1 = $this->getCatalogoAllChildrens(0, $obj->getId(), $arrayResult);
            if (!empty($aResult1)) {
                $arrayReturn = array_merge($arrayReturn, $aResult1);
            }
        }

        return $arrayReturn;
    }

    /**
     * Obtiene un registro de un Catalogo por la descripcion
     * @param integer $descripcion descripcion
     * @param integer $tipocatalogo_id Id del tipo de catalogo
     * @return $catalogo retorna el objeto catalogo
     */
    public function searchCatalogo($descripcion, $tipocatalogo_id = 0)
    {
        $qb = $this->createQueryBuilder('ct');
        $catalago = $qb
            ->Where('ct.estado=:estado')
            ->andWhere($qb->expr()->like("ct.descripcion", ":catalogo_descripcion"))
            ->setParameter('catalogo_descripcion', "%" . $descripcion . "%")
            ->setParameter('estado',Constantes::ESTADOGENERALHABILITADO)
            ->setMaxResults(1);
        if ($tipocatalogo_id > 0) {
            $query = $catalago
                ->andWhere("ct.tipocatalogo_id = :tipocatalogo_id")
                ->setParameter('tipocatalogo_id', $tipocatalogo_id)
                ->getQuery();
        } else {
            $query = $catalago->getQuery();
        }
        $desc = str_replace(' ', '', $descripcion);
        $resultId = "result_catalogo_search_$desc";

        $query->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return $query->getOneOrNullResult();
    }

    /**
     * Obtiene un registro de un Catalogo por la descripcion
     * @param integer $catalogo_id Id del detalle catalogo
     * @return $catalogo retorna el objeto catalogo
     */
    public function getAllChilldrenDetcatalogo($catalogo_id)
    {
        $estadogeneral = Constantes::ESTADOGENERALHABILITADO;

        $query = "SELECT c.ID, c.DESCRIPCION, c.DETALLECATALOGO_ID, level 
                    FROM HCUE_CATALOGOS.DETALLECATALOGO c WHERE c.ESTADO=$estadogeneral
                    START WITH c.ID = $catalogo_id
                    CONNECT BY PRIOR c.ID = DETALLECATALOGO_ID";

        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare($query);
        $statement->execute();
        $arrayReturn = $statement->fetchAll();
        return $arrayReturn;
    }
}

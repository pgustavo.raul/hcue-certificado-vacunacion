<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class SemanaepiRepository extends EntityRepository{

	/**
    * Obtiene el QueryBuilder de la lista de Semanaepi
    * @return QueryBuilder
    */
	public function getSemanaepiQueryBuilder() {
        return $this->createQueryBuilder('t')
               ->select('t');
    }

    /**
     * Obtiene el registro de semana epidemiológica en base a una fecha
     * @param date @fecha fecha de referencia para el calculo de la semana epidemiológica
     * @return Objeto de tipo "Core\AppBundle\Entity\Semanaepi"
     */
    public function getSemanaEpiByfecha($fecha) {
        $queryBuilder= $this->createQueryBuilder('sepi')
            ->select("sepi")
            ->Where("sepi.fechainicio <= :fechai")
            ->andWhere("sepi.fechafin >= :fechaf")
            ->setParameter(':fechai', $fecha)
            ->setParameter(':fechaf', $fecha)
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
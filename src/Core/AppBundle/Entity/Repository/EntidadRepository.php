<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\AppBundle\Entity\Constantes;
use HCUE\InscripcionBundle\Entity\Constantes as ConstatesInscripcion;

class EntidadRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Entidad
     * @param integer @estado para retornar las entidades de acuerdo a su estado (activo, inactivo)
     * @return QueryBuilder
     */
    public function getEntidadQueryBuilder($estado, $id) {

        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->Where('e.activo = :estado_general');

        if(!empty($id) && is_numeric($id)){
            $qb->andWhere('e.id = :id')
                ->andWhere('e.estado = :estado_general')
                ->setParameter('id', $id);
        }
        else{
            $qb->andWhere('e.entidad_id is null');
        }

        if(!empty($estado) && is_numeric($estado)){
            $qb->andWhere('e.estado = :estado')
                ->setParameter('estado', $estado);
        }

        $qb->setParameter('estado_general', Constantes::CT_ESTADOGENERALACTIVO);
        return $qb;
    }

    /**
     * Obtiene el QueryBuilder de la lista de Entidad por el id de la parroquia
     * @param integer $parroquia_id Id de la parroquia
     * @return QueryBuilder
     */
    public function getEntidadporParroquiaQueryBuilder($parroquia_id) {
        $qb=  $this->getEntidadQueryBuilder();
        $qb->innerJoin("CoreAppBundle:Tipoestablecimiento", "te","WITH","te.id=t.tipoestablecimiento")
            ->where("t.parroquia_id=:parroquia_id")
            ->setParameter("parroquia_id", $parroquia_id);
        return $qb;
    }

    /**
     * Obtiene el QueryBuilder de la lista de Entidad por el id del canton
     * @param integer $canton_id Id del canton
     * @return QueryBuilder
     */
    public function getEntidadporCantonQueryBuilder($canton_id) {
        $qb=  $this->getEntidadQueryBuilder();
        $qb->innerJoin("t.parroquia", 'p','WITH','t.parroquia_id=p.id')
            ->where("p.canton_id=:canton_id")
            ->setParameter("canton_id", $canton_id);
        return $qb;
    }

    /**
     * Obtiene la lista de entidades
     * @param integer @estado para retornar las entidades de acuerdo a su estado (activo, inactivo)
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Entidad"
     */
    public function getEntidades($estado=1, $id=0, $arrayResult=false) {
        $qb = $this->getEntidadQueryBuilder($estado, $id);
        $query = $qb->getQuery();
        $resultId=($arrayResult)?"result_array_entidades_by_estado_or_id_{$estado}_{$id}":"result_entidades_by_estado_or_id_{$estado}_$id";
        $query->useQueryCache(true)
            ->useResultCache(true,null,$resultId);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();
    }

    /**
     * Obtiene la entidad por Id
     * @param integer @id Id de la entidad
     * @return Objeto de tipo "Core\AppBundle\Entity\Entidad"
     */
    public function getEntidadById($id) {
        $queryBuilder= $this->createQueryBuilder('e')
            ->select("e,tpe,pq,ct,pv,org")
            ->innerJoin('e.tipoestablecimiento', 'tpe')
            ->innerJoin('e.organico', 'org')
            ->innerJoin('e.parroquia', 'pq')
            ->innerJoin('pq.canton', 'ct')
            ->innerJoin('ct.provincia', 'pv')
            ->Where("e.id=:id")
            ->setParameter('id', $id)
            ->setMaxResults(1)
        ;
        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de hijos de una entidad
     * @param integer $parent_id
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getEntidadChildrens($parent_id, $arrayResult=false){
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->Where('e.estado = :estado_general')
            ->andWhere('e.entidad_id = :entidad_padre')
            ->andWhere('e.activo = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'entidad_padre' => $parent_id
            ]);

        $query = $qb->getQuery();
        $resultId=($arrayResult)?"result_array_entidades_childrens_by_entidad_$parent_id":"result_entidades_childrens_by_entidad_$parent_id";
        $query->useQueryCache(true)
            ->useResultCache(true,null,$resultId);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de la lista de hijos de una entidad
     * @param integer $parent_id
     * @return QueryBuilder
     */
    public function getEntidadAllChildrens($id, $parent_id=0, $arrayResult=false){
        $aResult=($id>0)?$this->getEntidades(1, $id, $arrayResult) :$this->getEntidadChildrens($parent_id, $arrayResult);

        $arrayReturn=array();
        foreach ($aResult as $obj){
            $arrayReturn[]=$obj;
            $aResult1=  $this->getEntidadAllChildrens(0, $obj->getId(), $arrayResult);

            if(!empty($aResult1)){
                $arrayReturn = array_merge($arrayReturn,$aResult1);
            }
        }

        return $arrayReturn;
    }

    /**
     * Obtiene el QueryBuilder de la lista de Entidad por distrito
     * @param integer $distrito_id Id de la parroquia
     * @return QueryBuilder
     */
    public function getIdsEntidadesByDistrito($distrito_id) {
        $qb=$this->createQueryBuilder('e')
            ->select('e.id')
            ->Where('e.estado = :estado_general')
            ->andWhere('e.activo = :estado_general')
            ->innerJoin('e.circuito', 'cir')
            ->innerJoin('cir.distrito', 'dis')
            ->andWhere("dis.id=:distrito_id")
            ->setParameter("distrito_id", $distrito_id)
            ->setParameter("estado_general", Constantes::ESTADOGENERALHABILITADO);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de Entidad por distrito
     * @param integer $distrito_id Id del distrito
     * @return QueryBuilder
     */
    public function getEntidadesByDistrito($distrito_id) {
        $qb=$this->createQueryBuilder('e')
            ->select('e')
            ->Where('e.estado = :estado_general')
            ->andWhere('e.activo = :estado_general')
            ->innerJoin('e.circuito', 'cir')
            ->innerJoin('cir.distrito', 'dis')
            ->andWhere("dis.id=:distrito_id")
            ->setParameter("distrito_id", $distrito_id)
            ->setParameter("estado_general", Constantes::ESTADOGENERALHABILITADO)
            ->orderBy("e.nombreoficial");

        return $qb->getQuery()->getArrayResult();
    }


    /**
     * Obtiene las parroquias del Distrito tomando en cuenta los establecimientos
     * @param string $distrito_id Id del distrito
     * @return array
     */
    public function getParroquiasByDistrito($distrito_id){
        $qb=$this->createQueryBuilder('e')
            ->select('DISTINCT(e.parroquia_id) as parroquia_id, par.descripcion as parroquia_descripcion, par.codparroquia as parroquia_codigo, par.cttipo_id,cat.descripcion as cttipo_descripcion')
            ->innerJoin('e.circuito', 'cir')
            ->innerJoin('cir.distrito', 'dis')
            ->innerJoin('e.parroquia', 'par')
            ->innerJoin('par.cttipo', 'cat')
            ->Where('e.estado = :estado_general')
            ->andWhere('e.activo = :estado_general')
            ->andWhere('par.estado = :estado_general')
            ->andWhere("dis.id=:distrito_id")
            ->setParameter("distrito_id", $distrito_id)
            ->setParameter("estado_general", Constantes::ESTADOGENERALHABILITADO)
            ->orderBy("par.descripcion");

        $query = $qb->getQuery();
        $resultId="result_array_parroquias_by_distrito_$distrito_id";
        $query->useQueryCache(true)
            ->useResultCache(true,null,$resultId);

        return $query->getArrayResult();
    }

    /**
     * Obtiene la lista de entidades
     * @param integer @estado para retornar las entidades de acuerdo a su estado (activo, inactivo)
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Entidad"
     */
    public function getEntidadByArray($arrayEntidades) {
        $qb=$this->createQueryBuilder('e')
            ->select("e,tpe,pq,ct,pv,org")
            ->innerJoin('e.tipoestablecimiento', 'tpe')
            ->innerJoin('e.organico', 'org')
            ->innerJoin('e.parroquia', 'pq')
            ->innerJoin('pq.canton', 'ct')
            ->innerJoin('ct.provincia', 'pv')
            ->Where("e.id in(:arrayEntidades)")
            ->setParameter('arrayEntidades', $arrayEntidades)
            ->orderBy("pv.id")
        ;
        $query = $qb->getQuery();
        return $query->getResult();
    }

    /**
     * Obtiene la lista de entidades Tipos de Establecimiento Nivel 1, 2, 3, 4 y sólo Unidades Operativas
     * @param integer $parent_id
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getEntidadbyNivel($arrayResult=false){
        $tipoestablecimientoRepository = $this->getEntityManager()->getRepository("CoreAppBundle:Tipoestablecimiento");
        $qb1 = $tipoestablecimientoRepository->createQueryBuilder('te');

        $qb1->select('te.id')
            ->where('te.tipoestablecimiento_id in (2,17,23,27)')
            ->andWhere('te.estado = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO
            ]);

        $qb = $this->createQueryBuilder('e');

        $qb->select('e')
            ->Where('e.estado = :estado_general')
            ->andWhere('e.activo = :estado_general')
            ->andWhere('e.ctclasificacion_id = :unidad_operativa')
            ->andWhere(
                $qb->expr()->in('e.tipoestablecimiento_id', $qb1->getDQL())
            )
            ->setParameters([
                'unidad_operativa' => Constantes::CTCLASIFICACION_ID,
                'estado_general' => Constantes::ESTADOGENERALHABILITADO
            ]);

        $query = $qb->getQuery();
        $query->useQueryCache(true)
            ->useResultCache(true,null);

            return ($arrayResult) ? $query->getArrayResult() : $query->getResult();

    }

}
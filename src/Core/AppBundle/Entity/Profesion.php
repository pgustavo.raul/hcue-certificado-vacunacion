<?php 

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Descripcion de Profesion
* @author Richard Veliz
* @ORM\Table(name="hcue_catalogos.profesion")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\ProfesionRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Profesion{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	**/
	private $id;

	/**
	* @var integer $codigo
	* @ORM\Column(name="codigo", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo codigo")
	**/
	private $codigo;

	
	/**
	* @var string $nombre
	* @ORM\Column(name="descripcion", type="string",length=500, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nombre")
	**/
	private $nombre;

	/**
	* @var string $estado
	* @ORM\Column(name="estado", type="string", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;
        
        /**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
        * @Assert\NotNull(message="Debe ingresar el campo fecha de creacion")
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var string $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="string", nullable=false)
        * @Assert\NotNull(message="Debe ingresar el campo usuario de creacion")
	**/
	private $usuariocreacion_id;

	/**
	* @var string $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="string", nullable=true)
	**/
	private $usuariomodificacion_id;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set codigo
	* @param integer codigo
	* @return profesion
	**/
	public function setCodigo($codigo){
		 $this->codigo = $codigo;
		 return $this;
	}

	/**
	* Get codigo
	* @return integer
	**/
	public function getCodigo(){
		 return $this->codigo ;
	}
        
        /**
	* Set nombre
	* @param string nombre
	* @return profesion
	**/
	public function setNombre($nombre){
		 $this->nombre = $nombre;
		 return $this;
	}

	/**
	* Get nombre
	* @return string
	**/
	public function getNombre(){
		 return $this->nombre;
	}
        
        /**
	* Set estado
	* @param string estado
	* @return profesion
	**/
	public function setEstado($estado=1){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return string
	**/
	public function getEstado(){
		 return $this->estado;
	}
        
        /**
	* Get fechacreacion
	* @return datetime
	**/
	public function getFechacreacion(){
		 return $this->fechacreacion;
	}

	/**
	* Set fechamodificacion
        * @ORM\PreUpdate
	* @return profesion
	**/
	public function setFechamodificacion(){
		 $this->fechamodificacion = new \DateTime();
		 return $this;
	}

	/**
	* Get fechamodificacion
	* @return datetime
	**/
	public function getFechamodificacion(){
		 return $this->fechamodificacion;
	}

	/**
	* Set usuariocreacion_id
	* @param string usuariocreacion_id
	* @return profesion
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return string
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}


	/**
	* Set usuariomodificacion_id
	* @param string usuariomodificacion_id
	* @return profesion
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return string
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

        /**
        * Obtiene el codigo y el nombre de la profesion  
        * @return string
        **/
        public function getCodigoAndNombre() {
                return $this->codigo.' '.$this->nombre;
        }
	
	 public function __toString() {
           return $this->nombre;
        }

}
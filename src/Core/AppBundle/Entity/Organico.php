<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.organico")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\OrganicoRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Organico{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.organico_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $organico_id
	* @ORM\Column(name="organico_id", type="integer", nullable=true)
	**/
	private $organico_id;

	/**
	* @ORM\ManyToOne(targetEntity="Organico")
	* @ORM\JoinColumn(name="organico_id", referencedColumnName="id")
	**/
	private $organico;

	/**
	* @var string $codigo
	* @ORM\Column(name="codigo", type="string",length=20, nullable=true)
	**/
	private $codigo;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=255, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo descripcion")
	**/
	private $descripcion;

	/**
	* @var integer $nivel
	* @ORM\Column(name="nivel", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nivel")
	**/
	private $nivel;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var string $homologacion
	* @ORM\Column(name="homologacion", type="string",length=20, nullable=true)
	**/
	private $homologacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set organico_id
	* @param integer organico_id
	* @return organico
	**/
	public function setOrganicoId($organico_id){
		 $this->organico_id = $organico_id;
		 return $this;
	}

	/**
	* Get organico_id
	* @return integer
	**/
	public function getOrganicoId(){
		 return $this->organico_id;
	}

	/**
	* Set organico
	* @param \Core\AppBundle\Entity\Organico $organico
	**/
	public function setOrganico(\Core\AppBundle\Entity\Organico $organico){
		 $this->organico = $organico;
		 return $this;
	}

	/**
	* Get organico
	* @return \Core\AppBundle\Entity\Organico
	**/
	public function getOrganico(){
		 return $this->organico;
	}

	/**
	* Set codigo
	* @param string codigo
	* @return organico
	**/
	public function setCodigo($codigo){
		 $this->codigo = $codigo;
		 return $this;
	}

	/**
	* Get codigo
	* @return string
	**/
	public function getCodigo(){
		 return $this->codigo;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return organico
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set nivel
	* @param integer nivel
	* @return organico
	**/
	public function setNivel($nivel){
		 $this->nivel = $nivel;
		 return $this;
	}

	/**
	* Get nivel
	* @return integer
	**/
	public function getNivel(){
		 return $this->nivel;
	}

	/**
	* Set estado
	* @param integer estado
	* @return organico
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set homologacion
	* @param string homologacion
	* @return organico
	**/
	public function setHomologacion($homologacion){
		 $this->homologacion = $homologacion;
		 return $this;
	}

	/**
	* Get homologacion
	* @return string
	**/
	public function getHomologacion(){
		 return $this->homologacion;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return organico
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return organico
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->fechamodificacion=new \DateTime();
$this->fechacreacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de archivo
 * @author Richard Veliz
 * @ORM\Table(name="hcue_archivos.archivo")
// * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\ArchivoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Archivo{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_archivos.archivo_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $identificacion
     * @ORM\Column(name="identificacion", type="string", nullable=false)
     **/

    private $identificacion;
    /**
     * @var string $archivo
     * @ORM\Column(name="archivo", type="string", nullable=true)
     **/
    private $archivo;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fecha de creacion")
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo usuario de creacion")
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo activo")
     **/
    private $activo;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set identificacion
     * @param string identificacion
     * @return archivo
     **/
    public function setIdentificacion($identificacion){
        $this->identificacion = $identificacion;
        return $this;
    }

    /**
     * Get identificacion
     * @return string
     **/
    public function getIdentificacion(){
        return $this->identificacion;
    }

    /**
     * Set archivo
     * @param string archivo
     * @return archivo
     **/
    public function setArchivo($archivo){
        $this->archivo = $archivo;
        return $this;
    }

    /**
     * Get archivo
     * @return string
     **/
    public function getArchivo(){
        return $this->archivo;
    }


    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return archivo
     **/
    public function setFechacreacion(){
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Set fechamodificacion
     * @ORM\PreUpdate
     * @return archivo
     **/
    public function setFechamodificacion(){
        $this->fechamodificacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechamodificacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return archivo
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }


    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return archivo
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }


    /**
     * Set activo
     * @param integer activo
     * @return aplicacion
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;

    }
    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }


}
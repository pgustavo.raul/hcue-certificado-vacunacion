<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descripcion de parroquia
 * @ORM\Table(name="hcue_catalogos.parroquia")
 * @ORM\Entity()
 */
class ParroquiaBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * */
    private $id;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     * */
    private $descripcion;

    /**
     * @var integer $canton_id
     * @ORM\Column(name="canton_id", type="integer")
     * */
    private $canton_id;

    /**
     * @ORM\ManyToOne(targetEntity="CantonBasic")
     * @ORM\JoinColumn(name="canton_id", referencedColumnName="id")
     * */
    private $canton;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Get canton_id
     * @return integer
     * */
    public function getCantonId() {
        return $this->canton_id;
    }

    /**
     * Get canton
     * @return CantonBasic
     * */
    public function getCanton() {
        return $this->canton;
    }

}
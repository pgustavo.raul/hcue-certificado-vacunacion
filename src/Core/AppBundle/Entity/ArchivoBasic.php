<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descripcion de archivo
 * @ORM\Table(name="hcue_archivos.archivo")
 * @ORM\Entity()
 */
class ArchivoBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var string $archivo
     * @ORM\Column(name="archivo", type="string")
     **/
    private $archivo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get archivo
     * @return string
     **/
    public function getArchivo(){
        return $this->archivo;
    }

}
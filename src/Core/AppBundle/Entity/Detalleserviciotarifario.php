<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.detalleserviciotarifario")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\DetalleserviciotarifarioRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Detalleserviciotarifario{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.detalleserviciotarifario_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $serviciotarifario_id
	* @ORM\Column(name="serviciotarifario_id", type="integer", nullable=true)
	**/
	private $serviciotarifario_id;

	/**
	* @ORM\ManyToOne(targetEntity="Serviciotarifario")
	* @ORM\JoinColumn(name="serviciotarifario_id", referencedColumnName="id")
	**/
	private $serviciotarifario;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=2000, nullable=true)
	**/
	private $descripcion;

	/**
	* @var string $codigo
	* @ORM\Column(name="codigo", type="string",length=100, nullable=true)
	**/
	private $codigo;

	/**
	* @var integer $primerniveluvr
	* @ORM\Column(name="primerniveluvr", type="integer", nullable=true)
	**/
	private $primerniveluvr;

	/**
	* @var integer $primernivelfcm
	* @ORM\Column(name="primernivelfcm", type="integer", nullable=true)
	**/
	private $primernivelfcm;

	/**
	* @var integer $primernivelvalor
	* @ORM\Column(name="primernivelvalor", type="integer", nullable=true)
	**/
	private $primernivelvalor;

	/**
	* @var integer $segundoniveluvr
	* @ORM\Column(name="segundoniveluvr", type="integer", nullable=true)
	**/
	private $segundoniveluvr;

	/**
	* @var integer $segundonivelfcm
	* @ORM\Column(name="segundonivelfcm", type="integer", nullable=true)
	**/
	private $segundonivelfcm;

	/**
	* @var integer $segundonivelvalor
	* @ORM\Column(name="segundonivelvalor", type="integer", nullable=true)
	**/
	private $segundonivelvalor;

	/**
	* @var integer $tercerniveluvr
	* @ORM\Column(name="tercerniveluvr", type="integer", nullable=true)
	**/
	private $tercerniveluvr;

	/**
	* @var integer $tercernivelfcm
	* @ORM\Column(name="tercernivelfcm", type="integer", nullable=true)
	**/
	private $tercernivelfcm;

	/**
	* @var integer $tercernivelvalor
	* @ORM\Column(name="tercernivelvalor", type="integer", nullable=true)
	**/
	private $tercernivelvalor;

	/**
	* @var string $observacion
	* @ORM\Column(name="observacion", type="string",length=2000, nullable=true)
	**/
	private $observacion;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=true)
	**/
	private $activo;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set serviciotarifario_id
	* @param integer serviciotarifario_id
	* @return detalleserviciotarifario
	**/
	public function setServiciotarifarioId($serviciotarifario_id){
		 $this->serviciotarifario_id = $serviciotarifario_id;
		 return $this;
	}

	/**
	* Get serviciotarifario_id
	* @return integer
	**/
	public function getServiciotarifarioId(){
		 return $this->serviciotarifario_id;
	}

	/**
	* Set serviciotarifario
	* @param \Core\AppBundle\Entity\Serviciotarifario $serviciotarifario
	**/
	public function setServiciotarifario(\Core\AppBundle\Entity\Serviciotarifario $serviciotarifario){
		 $this->serviciotarifario = $serviciotarifario;
		 return $this;
	}

	/**
	* Get serviciotarifario
	* @return \Core\AppBundle\Entity\Serviciotarifario
	**/
	public function getServiciotarifario(){
		 return $this->serviciotarifario;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return detalleserviciotarifario
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set codigo
	* @param string codigo
	* @return detalleserviciotarifario
	**/
	public function setCodigo($codigo){
		 $this->codigo = $codigo;
		 return $this;
	}

	/**
	* Get codigo
	* @return string
	**/
	public function getCodigo(){
		 return $this->codigo;
	}

    /**
     * Get codigo
     * @return string
     **/
    public function getCodigodescripcion(){
        return $this->codigo. " - ".$this->getServiciotarifario()->getDescripcion()." - ". $this->descripcion;
    }

	/**
	* Set primerniveluvr
	* @param integer primerniveluvr
	* @return detalleserviciotarifario
	**/
	public function setPrimerniveluvr($primerniveluvr){
		 $this->primerniveluvr = $primerniveluvr;
		 return $this;
	}

	/**
	* Get primerniveluvr
	* @return integer
	**/
	public function getPrimerniveluvr(){
		 return $this->primerniveluvr;
	}

	/**
	* Set primernivelfcm
	* @param integer primernivelfcm
	* @return detalleserviciotarifario
	**/
	public function setPrimernivelfcm($primernivelfcm){
		 $this->primernivelfcm = $primernivelfcm;
		 return $this;
	}

	/**
	* Get primernivelfcm
	* @return integer
	**/
	public function getPrimernivelfcm(){
		 return $this->primernivelfcm;
	}

	/**
	* Set primernivelvalor
	* @param integer primernivelvalor
	* @return detalleserviciotarifario
	**/
	public function setPrimernivelvalor($primernivelvalor){
		 $this->primernivelvalor = $primernivelvalor;
		 return $this;
	}

	/**
	* Get primernivelvalor
	* @return integer
	**/
	public function getPrimernivelvalor(){
		 return $this->primernivelvalor;
	}

	/**
	* Set segundoniveluvr
	* @param integer segundoniveluvr
	* @return detalleserviciotarifario
	**/
	public function setSegundoniveluvr($segundoniveluvr){
		 $this->segundoniveluvr = $segundoniveluvr;
		 return $this;
	}

	/**
	* Get segundoniveluvr
	* @return integer
	**/
	public function getSegundoniveluvr(){
		 return $this->segundoniveluvr;
	}

	/**
	* Set segundonivelfcm
	* @param integer segundonivelfcm
	* @return detalleserviciotarifario
	**/
	public function setSegundonivelfcm($segundonivelfcm){
		 $this->segundonivelfcm = $segundonivelfcm;
		 return $this;
	}

	/**
	* Get segundonivelfcm
	* @return integer
	**/
	public function getSegundonivelfcm(){
		 return $this->segundonivelfcm;
	}

	/**
	* Set segundonivelvalor
	* @param integer segundonivelvalor
	* @return detalleserviciotarifario
	**/
	public function setSegundonivelvalor($segundonivelvalor){
		 $this->segundonivelvalor = $segundonivelvalor;
		 return $this;
	}

	/**
	* Get segundonivelvalor
	* @return integer
	**/
	public function getSegundonivelvalor(){
		 return $this->segundonivelvalor;
	}

	/**
	* Set tercerniveluvr
	* @param integer tercerniveluvr
	* @return detalleserviciotarifario
	**/
	public function setTercerniveluvr($tercerniveluvr){
		 $this->tercerniveluvr = $tercerniveluvr;
		 return $this;
	}

	/**
	* Get tercerniveluvr
	* @return integer
	**/
	public function getTercerniveluvr(){
		 return $this->tercerniveluvr;
	}

	/**
	* Set tercernivelfcm
	* @param integer tercernivelfcm
	* @return detalleserviciotarifario
	**/
	public function setTercernivelfcm($tercernivelfcm){
		 $this->tercernivelfcm = $tercernivelfcm;
		 return $this;
	}

	/**
	* Get tercernivelfcm
	* @return integer
	**/
	public function getTercernivelfcm(){
		 return $this->tercernivelfcm;
	}

	/**
	* Set tercernivelvalor
	* @param integer tercernivelvalor
	* @return detalleserviciotarifario
	**/
	public function setTercernivelvalor($tercernivelvalor){
		 $this->tercernivelvalor = $tercernivelvalor;
		 return $this;
	}

	/**
	* Get tercernivelvalor
	* @return integer
	**/
	public function getTercernivelvalor(){
		 return $this->tercernivelvalor;
	}

	/**
	* Set observacion
	* @param string observacion
	* @return detalleserviciotarifario
	**/
	public function setObservacion($observacion){
		 $this->observacion = $observacion;
		 return $this;
	}

	/**
	* Get observacion
	* @return string
	**/
	public function getObservacion(){
		 return $this->observacion;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return detalleserviciotarifario
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return detalleserviciotarifario
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set activo
	* @param integer activo
	* @return detalleserviciotarifario
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->activo=1;
$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}


	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
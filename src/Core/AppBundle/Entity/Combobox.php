<?php 

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Clase para definir datos de un combobox
* @author Luis Malquin
*/
class Combobox{

	/**
        * Propiedad del valor de identificacion en el combobox
	* @var integer $id	
	**/
	private $id;

	/**
        * Propiedad de valor a mostrar del combobox
	* @var string $descripcion	
	**/
	private $descripcion;
	
        /**
	* set id
	* @return Combobox
	**/
	public function setId($id){
           $this->id=$id;
           return $this;
	}
        
	/**
	* Get id
	* @return integer
	**/
	public function getId(){
            return $this->id;
	}

	/**
	* Set descripcion
	* @param string $descripcion
	* @return Combobox
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}
	        
        public function __toString() {
           return $this->descripcion;
        }
}
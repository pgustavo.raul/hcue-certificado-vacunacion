<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Core\AppBundle\Entity\Catalogo;
use Core\AppBundle\Entity\Pais;
use Core\AppBundle\Entity\Parroquia;
use Core\AppBundle\Entity\Archivo;

/**
 * Descripcion de persona
 * @author Luis Malquin
 * @ORM\Table(name="hcue_sistema.persona")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\PersonaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Persona
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.persona_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $cttipoidentificacion_id
     * @ORM\Column(name="cttipoidentificacion_id", type="integer", nullable=false)
     **/
    private $cttipoidentificacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipoidentificacion_id", referencedColumnName="id")
     **/
    private $cttipoidentificacion;

    /**
     * @var string $numeroidentificacion
     * @ORM\Column(name="numeroidentificacion", type="string",length=20, nullable=true)
     **/
    private $numeroidentificacion;

    /**
     * @var string $noidentificado
     * @ORM\Column(name="noidentificado", type="string",length=20, nullable=true)
     **/
    private $noidentificado;

    /**
     * @var string $apellidopaterno
     * @ORM\Column(name="apellidopaterno", type="string",length=50, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo apellidopaterno")
     **/
    private $apellidopaterno;

    /**
     * @var string $apellidomaterno
     * @ORM\Column(name="apellidomaterno", type="string",length=50, nullable=true)
     **/
    private $apellidomaterno;

    /**
     * @var string $primernombre
     * @ORM\Column(name="primernombre", type="string",length=50, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo primernombre")
     **/
    private $primernombre;

    /**
     * @var string $segundonombre
     * @ORM\Column(name="segundonombre", type="string",length=50, nullable=true)
     **/
    private $segundonombre;

    /**
     * @var string $nombrecompleto
     * @ORM\Column(name="nombrecompleto", type="string",length=50, nullable=true)
     **/
    private $nombrecompleto;

    /**
     * @var integer $pais_id
     * @ORM\Column(name="pais_id", type="integer", nullable=false)
     **/
    private $pais_id;

    /**
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumn(name="pais_id", referencedColumnName="id")
     **/
    private $pais;

    /**
     * @var integer $provincia_id
     **/
    private $provincia_id;

    /**
     * @var integer $parroquia_id
     * @ORM\Column(name="parroquia_id", type="integer", nullable=true)
     **/
    private $parroquia_id;


    /**
     * @ORM\ManyToOne(targetEntity="Parroquia")
     * @ORM\JoinColumn(name="parroquia_id", referencedColumnName="id")
     **/
    private $parroquia;

    /**
     * @var string $lugarnacimiento
     * @ORM\Column(name="lugarnacimiento", type="string",length=250, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo lugarnacimiento")
     **/
    private $lugarnacimiento;

    /**
     * @var \Datetime $fechanacimiento
     * @ORM\Column(name="fechanacimiento", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fechanacimiento")
     **/
    private $fechanacimiento;

    /**
     * @var integer $ctsexo_id
     * @ORM\Column(name="ctsexo_id", type="integer", nullable=false)
     **/
    private $ctsexo_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctsexo_id", referencedColumnName="id")
     **/
    private $ctsexo;

    /**
     * @var integer $ctestadocivil_id
     * @ORM\Column(name="ctestadocivil_id", type="integer", nullable=true)
     **/
    private $ctestadocivil_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestadocivil_id", referencedColumnName="id")
     **/
    private $ctestadocivil;

    /**
     * @var integer $ctniveleducacion_id
     * @ORM\Column(name="ctniveleducacion_id", type="integer", nullable=false)
     **/
    private $ctniveleducacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctniveleducacion_id", referencedColumnName="id")
     **/
    private $ctniveleducacion;


    /**
     * @var string $nombremadre
     * @ORM\Column(name="nombremadre", type="string",length=200, nullable=true)
     **/
    private $nombremadre;

    /**
     * @var string $nombrepadre
     * @ORM\Column(name="nombrepadre", type="string",length=200, nullable=true)
     **/
    private $nombrepadre;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fecha de creacion")
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo usuario de creacion")
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo activo")
     **/
    private $activo;

    /**
     * @var integer $foto_id
     * @ORM\Column(name="foto_id", type="integer", nullable=true)
     **/
    private $foto_id;

    /**
     * @ORM\ManyToOne(targetEntity="Archivo")
     * @ORM\JoinColumn(name="foto_id", referencedColumnName="id")
     **/
    private $foto;


    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cttipoidentificacion_id
     * @param integer cttipoidentificacion_id
     * @return persona
     **/
    public function setCttipoidentificacionId($cttipoidentificacion_id)
    {
        $this->cttipoidentificacion_id = $cttipoidentificacion_id;
        return $this;
    }

    /**
     * Get cttipoidentificacion_id
     * @return integer
     **/
    public function getCttipoidentificacionId()
    {
        return $this->cttipoidentificacion_id;
    }

    /**
     * Set cttipoidentificacion
     * @param Catalogo $cttipoidentificacion
     **/
    public function setCttipoidentificacion(Catalogo $cttipoidentificacion)
    {
        $this->cttipoidentificacion = $cttipoidentificacion;
        return $this;
    }

    /**
     * Get cttipoidentificacion
     * @return Catalogo
     **/
    public function getCttipoidentificacion()
    {
        return $this->cttipoidentificacion;
    }

    /**
     * Set numeroidentificacion
     * @param string numeroidentificacion
     * @return persona
     **/
    public function setNumeroidentificacion($numeroidentificacion)
    {
        $this->numeroidentificacion = $numeroidentificacion;
        return $this;
    }

    /**
     * Get numeroidentificacion
     * @return string
     **/
    public function getNumeroidentificacion()
    {
        return $this->numeroidentificacion;
    }

    /**
     * Set noidentificado
     * @param string noidentificado
     * @return persona
     **/
    public function setNoidentificado($noidentificado)
    {
        $this->noidentificado = $noidentificado;
        return $this;
    }

    /**
     * Get noidentificado
     * @return string
     **/
    public function getNoidentificado()
    {
        return $this->noidentificado;
    }

    /**
     * Set apellidopaterno
     * @param string apellidopaterno
     * @return persona
     **/
    public function setApellidopaterno($apellidopaterno)
    {
        $this->apellidopaterno = $apellidopaterno;
        return $this;
    }

    /**
     * Get apellidopaterno
     * @return string
     **/
    public function getApellidopaterno()
    {
        return $this->apellidopaterno;
    }

    /**
     * Set apellidomaterno
     * @param string apellidomaterno
     * @return persona
     **/
    public function setApellidomaterno($apellidomaterno)
    {
        $this->apellidomaterno = $apellidomaterno;
        return $this;
    }

    /**
     * Get apellidomaterno
     * @return string
     **/
    public function getApellidomaterno()
    {
        return $this->apellidomaterno;
    }

    /**
     * Set primernombre
     * @param string primernombre
     * @return persona
     **/
    public function setPrimernombre($primernombre)
    {
        $this->primernombre = $primernombre;
        return $this;
    }

    /**
     * Get primernombre
     * @return string
     **/
    public function getPrimernombre()
    {
        return $this->primernombre;
    }

    /**
     * Set segundonombre
     * @param string segundonombre
     * @return persona
     **/
    public function setSegundonombre($segundonombre)
    {
        $this->segundonombre = $segundonombre;
        return $this;
    }

    /**
     * Get segundonombre
     * @return string
     **/
    public function getSegundonombre()
    {
        return $this->segundonombre;
    }

    /**
     * Set nombrecompleto
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @return persona
     **/
    public function setNombrecompleto()
    {
        $this->nombrecompleto = $this->apellidopaterno . ' '
            . $this->apellidomaterno . ' '
            . $this->primernombre . ' '
            . $this->segundonombre;
        return $this;
    }

    /**
     * Get nombrecompleto
     * @return string
     **/
    public function getNombrecompleto()
    {
        return $this->nombrecompleto;
    }

    public function getNombres()
    {
        return $this->primernombre . ' ' . $this->segundonombre;
    }

    public function getApellidos()
    {
        return $this->apellidopaterno . ' ' . $this->apellidomaterno;
    }

    /**
     * Set pais_id
     * @param integer pais_id
     * @return persona
     **/
    public function setPaisId($pais_id)
    {
        $this->pais_id = $pais_id;
        return $this;
    }

    /**
     * Get pais_id
     * @return integer
     **/
    public function getPaisId()
    {
        return $this->pais_id;
    }

    /**
     * Set pais
     * @param Pais $pais
     **/
    public function setPais(Pais $pais)
    {
        $this->pais = $pais;
        return $this;
    }

    /**
     * Get pais
     * @return Pais
     **/
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set provincia_id
     * @param integer provincia_id
     * @return persona
     **/
    public function setProvinciaId($provincia_id)
    {
        $this->provincia_id = $provincia_id;
        return $this;
    }

    /**
     * Get provincia_id
     * @return integer
     **/
    public function getProvinciaId()
    {
        return $this->provincia_id;
    }

    /**
     * Set parroquia_id
     * @param integer parroquia_id
     * @return persona
     **/
    public function setParroquiaId($parroquia_id)
    {
        $this->parroquia_id = $parroquia_id;
        return $this;
    }

    /**
     * Get parroquia_id
     * @return integer
     **/
    public function getParroquiaId()
    {
        return $this->parroquia_id;
    }

    /**
     * Set parroquia
     * @param Parroquia $parroquia
     **/
    public function setParroquia(Parroquia $parroquia = null)
    {
        $this->parroquia = $parroquia;
        return $this;
    }

    /**
     * Get parroquia
     * @return Parroquia
     **/
    public function getParroquia()
    {
        return $this->parroquia;
    }

    /**
     * Set lugarnacimiento
     * @param string lugarnacimiento
     * @return persona
     **/
    public function setLugarnacimiento($lugarnacimiento)
    {
        $this->lugarnacimiento = $lugarnacimiento;
        return $this;
    }

    /**
     * Get lugarnacimiento
     * @return string
     **/
    public function getLugarnacimiento()
    {
        return $this->lugarnacimiento;
    }

    /**
     * Set fechanacimiento
     * @param \Datetime fechanacimiento
     * @return persona
     **/
    public function setFechanacimiento($fechanacimiento)
    {
        $this->fechanacimiento = ($fechanacimiento);
        return $this;
    }

    /**
     * Get fechanacimiento
     * @return datetime
     **/
    public function getFechanacimiento()
    {
        return $this->fechanacimiento;
    }

    /**
     * Set ctsexo_id
     * @param integer ctsexo_id
     * @return persona
     **/
    public function setCtsexoId($ctsexo_id)
    {
        $this->ctsexo_id = $ctsexo_id;
        return $this;
    }

    /**
     * Get ctsexo_id
     * @return integer
     **/
    public function getCtsexoId()
    {
        return $this->ctsexo_id;
    }

    /**
     * Set ctsexo
     * @param Catalogo $ctsexo
     **/
    public function setCtsexo(Catalogo $ctsexo)
    {
        $this->ctsexo = $ctsexo;
        return $this;
    }

    /**
     * Get ctsexo
     * @return Catalogo
     **/
    public function getCtsexo()
    {
        return $this->ctsexo;
    }

    /**
     * Set ctestadocivil_id
     * @param integer ctestadocivil_id
     * @return persona
     **/
    public function setCtestadocivilId($ctestadocivil_id)
    {
        $this->ctestadocivil_id = $ctestadocivil_id;
        return $this;
    }

    /**
     * Get ctestadocivil_id
     * @return integer
     **/
    public function getCtestadocivilId()
    {
        return $this->ctestadocivil_id;
    }

    /**
     * Set ctestadocivil
     * @param Catalogo $ctestadocivil
     **/
    public function setCtestadocivil(Catalogo $ctestadocivil)
    {
        $this->ctestadocivil = $ctestadocivil;
        return $this;
    }

    /**
     * Get ctestadocivil
     * @return Catalogo
     **/
    public function getCtestadocivil()
    {
        return $this->ctestadocivil;
    }

    /**
     * Set ctniveleducacion_id
     * @param integer ctniveleducacion_id
     * @return persona
     **/
    public function setCtniveleducacionId($ctniveleducacion_id)
    {
        $this->ctniveleducacion_id = $ctniveleducacion_id;
        return $this;
    }

    /**
     * Get ctniveleducacion_id
     * @return integer
     **/
    public function getCtniveleducacionId()
    {
        return $this->ctniveleducacion_id;
    }

    /**
     * Set ctniveleducacion
     * @param Catalogo $ctniveleducacion
     **/
    public function setCtniveleducacion(Catalogo $ctniveleducacion)
    {
        $this->ctniveleducacion = $ctniveleducacion;
        return $this;
    }

    /**
     * Get ctniveleducacion
     * @return Catalogo
     **/
    public function getCtniveleducacion()
    {
        return $this->ctniveleducacion;
    }

    /**
     * Set profesion
     * @param string profesion
     * @return persona
     **/
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;
        return $this;
    }

    /**
     * Get profesion
     * @return string
     **/
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set nombremadre
     * @param string nombremadre
     * @return persona
     **/
    public function setNombremadre($nombremadre)
    {
        $this->nombremadre = $nombremadre;
        return $this;
    }

    /**
     * Get nombremadre
     * @return string
     **/
    public function getNombremadre()
    {
        return $this->nombremadre;
    }

    /**
     * Set nombrepadre
     * @param string nombrepadre
     * @return persona
     **/
    public function setNombrepadre($nombrepadre)
    {
        $this->nombrepadre = $nombrepadre;
        return $this;
    }

    /**
     * Get nombrepadre
     * @return string
     **/
    public function getNombrepadre()
    {
        return $this->nombrepadre;
    }

    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return persona
     **/
    public function setFechacreacion()
    {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Set fechamodificacion
     * @ORM\PreUpdate
     * @return persona
     **/
    public function setFechamodificacion()
    {
        $this->fechamodificacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechamodificacion
     * @return datetime
     **/
    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return persona
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }


    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return persona
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }


    /**
     * Set estado
     * @param integer $estado Estado
     * @return persona
     **/
    public function setEstado($estado = 1)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return persona
     **/
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set foto_id
     * @param integer foto_id
     * @return persona
     **/
    public function setFotoId($foto_id)
    {
        $this->foto_id = $foto_id;
        return $this;
    }

    /**
     * Get foto_id
     * @return integer
     **/
    public function getFotoId()
    {
        return $this->foto_id;
    }

    /**
     * Set foto
     * @param Archivo $foto
     * @return Archivo
     **/
    public function setFoto(Archivo $foto)
    {
        $this->foto = $foto;
        return $this;
    }

    /**
     * Get foto
     * @return Archivo
     **/
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
        $this->estado = 1;
        $this->activo = 1;

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }


}
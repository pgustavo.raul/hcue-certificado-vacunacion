<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descripcion de Catalogo
 * @ORM\Table(name="hcue_catalogos.detallecatalogo")
 * @ORM\Entity()
 */
class CatalogoBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     **/
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogoBasic")
     * @ORM\JoinColumn(name="detallecatalogo_id", referencedColumnName="id")
     **/
    private $catalogo;

    /**
     * @var string $valor
     * @ORM\Column(name="valor", type="string")
     **/
    private $valor;

    /**
     * @var integer $orden
     * @ORM\Column(name="ordenvisualizacion", type="integer")
     **/
    private $orden;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Get catalogo
     * @return CatalogoBasic
     **/
    public function getCatalogo(){
        return $this->catalogo;
    }

    /**
     * Get valor
     * @return string
     **/
    public function getValor(){
        return $this->valor;
    }

    /**
     * Get orden
     * @return integer
     **/
    public function getOrden(){
        return $this->orden;
    }

}
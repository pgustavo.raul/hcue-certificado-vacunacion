<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipos de establecimiento de salud
 * @ORM\Table(name="hcue_catalogos.tipoestablecimiento")
 * @ORM\Entity()
 */
class TipoestablecimientoBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     **/
    private $descripcion;

    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion()
    {
        return $this->descripcion;
    }

}
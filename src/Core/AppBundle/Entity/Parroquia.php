<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de parroquia
 * @author Luis Malquin
 * @ORM\Table(name="hcue_catalogos.parroquia")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\ParroquiaRepository")
 */
class Parroquia {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="syapp.parroquia_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $codparroquia
     * @ORM\Column(name="codparroquia", type="string",length=20, nullable=true)
     * */
    private $codparroquia;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=100, nullable=true)
     * */
    private $descripcion;

    /**
     * @var integer $canton_id
     * @ORM\Column(name="canton_id", type="integer", nullable=false)
     * */
    private $canton_id;

    /**
     * @ORM\ManyToOne(targetEntity="Canton")
     * @ORM\JoinColumn(name="canton_id", referencedColumnName="id")
     * */
    private $canton;

    /**
     * @var integer estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * */
    private $estado;
    
    /**
     * @var integer $cttipo_id
     * @ORM\Column(name="cttipo_id", type="integer", nullable=true)
     **/
	private $cttipo_id;
        
	
    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipo_id", referencedColumnName="id")
     **/
    private $cttipo;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Set codparroquia
     * @param string codparroquia
     * @return parroquia
     * */
    public function setCodparroquia($codparroquia) {
        $this->codparroquia = $codparroquia;
        return $this;
    }

    /**
     * Get codparroquia
     * @return string
     * */
    public function getCodparroquia() {
        return $this->codparroquia;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return parroquia
     * */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set canton_id
     * @param integer canton_id
     * @return parroquia
     * */
    public function setCantonId($canton_id) {
        $this->canton_id = $canton_id;
        return $this;
    }

    /**
     * Get canton_id
     * @return integer
     * */
    public function getCantonId() {
        return $this->canton_id;
    }

    /**
     * Set canton
     * @param \Core\AppBundle\Entity\canton $canton
     * */
    public function setCanton(\Core\AppBundle\Entity\Canton $canton) {
        $this->canton = $canton;
        return $this;
    }

    /**
     * Get canton
     * @return \Core\AppBundle\Entity\canton
     * */
    public function getCanton() {
        return $this->canton;
    }

     /**
     * Set estado
     * @param integer estado
     * @return parroquia
     * */
     public function setEstado($estado) {
        $this->estado = $estado;
        return $this;
     }
    
    /**
     * Get estado
     * @return integer
     * */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set cttipoId
     * @param integer $cttipo_id
     * @return parroquia
     * */
    public function setCttipoId($cttipo_id) {
        $this->cttipo_id = $cttipo_id;
        return $this;
    }

    /**
     * Get cttipo_id
     * @return string
     * */
    public function getCttipoId() {
        return $this->cttipo_id;
    }
    
    /**
     * Set cttipoId
     * @param integer $cttipo
     * @return parroquia
     * */
    public function setCttipo(Catalogo $cttipo) {
        $this->cttipo = $cttipo;
        return $this;
    }

    /**
     * Get cttipo
     * @return Catalogo
     * */
    public function getCttipo() {
        return $this->cttipo;
    }
    
    /**
    * Obtiene el codigo y el nombre de la parroquia concatenado  
    * @return string
    **/
    public function getCodigoAndDescripcion() {
            return $this->codparroquia.' '.$this->descripcion;
    }
    
    public function __toString() {
           return $this->descripcion;
    }
}
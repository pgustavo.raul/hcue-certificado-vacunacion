<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_catalogos.tarifario")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\TarifarioRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Tarifario{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.tarifario_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=2000, nullable=true)
	**/
	private $descripcion;

	/**
	* @var datetime $vigenciadesde
	* @ORM\Column(name="vigenciadesde", type="datetime", nullable=true)
	**/
	private $vigenciadesde;

	/**
	* @var datetime $vigenciahasta
	* @ORM\Column(name="vigenciahasta", type="datetime", nullable=true)
	**/
	private $vigenciahasta;

	/**
	* @var datetime $fechacarga
	* @ORM\Column(name="fechacarga", type="datetime", nullable=true)
	**/
	private $fechacarga;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=true)
	**/
	private $estado;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=true)
	**/
	private $activo;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return tarifario
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set vigenciadesde
	* @param datetime vigenciadesde
	* @return tarifario
	**/
	public function setVigenciadesde($vigenciadesde){
		 $this->vigenciadesde = $vigenciadesde;
		 return $this;
	}

	/**
	* Get vigenciadesde
	* @return datetime
	**/
	public function getVigenciadesde(){
		 return $this->vigenciadesde;
	}

	/**
	* Set vigenciahasta
	* @param datetime vigenciahasta
	* @return tarifario
	**/
	public function setVigenciahasta($vigenciahasta){
		 $this->vigenciahasta = $vigenciahasta;
		 return $this;
	}

	/**
	* Get vigenciahasta
	* @return datetime
	**/
	public function getVigenciahasta(){
		 return $this->vigenciahasta;
	}

	/**
	* Set fechacarga
	* @param datetime fechacarga
	* @return tarifario
	**/
	public function setFechacarga($fechacarga){
		 $this->fechacarga = $fechacarga;
		 return $this;
	}

	/**
	* Get fechacarga
	* @return datetime
	**/
	public function getFechacarga(){
		 return $this->fechacarga;
	}

	/**
	* Set estado
	* @param integer estado
	* @return tarifario
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return tarifario
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return tarifario
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set activo
	* @param integer activo
	* @return tarifario
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->fechamodificacion=new \DateTime();
$this->fechacreacion=new \DateTime();
		$this->activo=1;

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}



	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descripcion de canton
 * @ORM\Table(name="hcue_catalogos.canton")
 * @ORM\Entity()
 */
class CantonBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * */
    private $id;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     * */
    private $descripcion;

    /**
     * @var integer $provincia_id
     * @ORM\Column(name="provincia_id", type="integer")
     * */
    private $provincia_id;

    /**
     * @ORM\ManyToOne(targetEntity="ProvinciaBasic")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     * */
    private $provincia;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Get provincia_id
     * @return integer
     * */
    public function getProvinciaId() {
        return $this->provincia_id;
    }

    /**
     * Get provincia
     * @return ProvinciaBasic
     * */
    public function getProvincia() {
        return $this->provincia;
    }

}
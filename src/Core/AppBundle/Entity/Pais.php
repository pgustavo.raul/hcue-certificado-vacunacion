<?php 

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Descripcion de Pais
* @author Luis Malquin
* @ORM\Table(name="hcue_catalogos.pais")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\PaisRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Pais{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_catalogos.pais_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $nombre
	* @ORM\Column(name="descripcion", type="string",length=50, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nombre")
	**/
	private $nombre;

	/**
	* @var string $nacionalidad
	* @ORM\Column(name="nacionalidad", type="string",length=100, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nacionalidad")
	**/
	private $nacionalidad;

	/**
	* @var string $codigo
	* @ORM\Column(name="codigo", type="string",length=5, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo codigo")
	**/
	private $codigo;

        /**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)			
	**/
	private $estado;
        
	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set nombre
	* @param string nombre
	* @return pais
	**/
	public function setNombre($nombre){
		 $this->nombre = $nombre;
		 return $this;
	}

	/**
	* Get nombre
	* @return string
	**/
	public function getNombre(){
		 return $this->nombre;
	}

	/**
	* Set nacionalidad
	* @param string nacionalidad
	* @return pais
	**/
	public function setNacionalidad($nacionalidad){
		 $this->nacionalidad = $nacionalidad;
		 return $this;
	}

	/**
	* Get nacionalidad
	* @return string
	**/
	public function getNacionalidad(){
		 return $this->nacionalidad;
	}

	/**
	* Set codigo
	* @param string codigo
	* @return pais
	**/
	public function setCodigo($codigo){
		 $this->codigo = $codigo;
		 return $this;
	}

	/**
	* Get codigo
	* @return string
	**/
	public function getCodigo(){
		 return $this->codigo;
	}
        
        /**
        * Set estado
        * @param integer estado
        * @return pais
        * */
        public function setEstado($estado) {
            $this->estado = $estado;
            return $this;
        }
        
        /**
        * Get estado
        * @return integer
        * */
        public function getEstado() {
            return $this->estado;
        }
        
        /**
        * Obtiene el codigo y el nombre del país concatenado  
        * @return string
        * */
        public function getCodigoAndNombre() {
            return $this->codigo.' '.$this->nombre;
        }
        
        /**
        * Obtiene el codigo y la nacionalidad concatenado  
        * @return string
        * */
        public function getCodigoAndNacionalidad() {
            return $this->codigo.' '.$this->nacionalidad;
        }
        
        public function __toString() {
           return $this->nombre;
        }
}
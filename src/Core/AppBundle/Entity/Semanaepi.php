<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_vigilancia.semanaepi")
* @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\SemanaepiRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Semanaepi{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_vigilancia.semanaepi_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $anio
	* @ORM\Column(name="anio", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo anio")
	**/
	private $anio;

	/**
	* @var integer $semana
	* @ORM\Column(name="semana", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo semana")
	**/
	private $semana;

	/**
	* @var datetime $fechainicio
	* @ORM\Column(name="fechainicio", type="datetime", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo fechainicio")
	**/
	private $fechainicio;

	/**
	* @var datetime $fechafin
	* @ORM\Column(name="fechafin", type="datetime", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo fechafin")
	**/
	private $fechafin;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=true)
	**/
	private $estado;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=false)
	**/
	private $activo;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set anio
	* @param integer anio
	* @return semanaepi
	**/
	public function setAnio($anio){
		 $this->anio = $anio;
		 return $this;
	}

	/**
	* Get anio
	* @return integer
	**/
	public function getAnio(){
		 return $this->anio;
	}

	/**
	* Set semana
	* @param integer semana
	* @return semanaepi
	**/
	public function setSemana($semana){
		 $this->semana = $semana;
		 return $this;
	}

	/**
	* Get semana
	* @return integer
	**/
	public function getSemana(){
		 return $this->semana;
	}

	/**
	* Set fechainicio
	* @param datetime fechainicio
	* @return semanaepi
	**/
	public function setFechainicio($fechainicio){
		 $this->fechainicio = $fechainicio;
		 return $this;
	}

	/**
	* Get fechainicio
	* @return datetime
	**/
	public function getFechainicio(){
		 return $this->fechainicio;
	}

	/**
	* Set fechafin
	* @param datetime fechafin
	* @return semanaepi
	**/
	public function setFechafin($fechafin){
		 $this->fechafin = $fechafin;
		 return $this;
	}

	/**
	* Get fechafin
	* @return datetime
	**/
	public function getFechafin(){
		 return $this->fechafin;
	}

	/**
	* Set estado
	* @param integer estado
	* @return semanaepi
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return semanaepi
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set activo
	* @param integer activo
	* @return semanaepi
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return semanaepi
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->activo=1;
$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}


	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de Catalogo
 * @author Luis Malquin
 * @ORM\Table(name="hcue_catalogos.detallecatalogo")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Entity\Repository\CatalogoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Catalogo{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     **/
    private $id;

    /**
     * @var integer $tipocatalogo_id
     * @ORM\Column(name="catalogo_id", type="integer", nullable=false)
     **/
    private $tipocatalogo_id;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=250, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo descripcion")
     **/
    private $descripcion;

    /**
     * @var string $valor
     * @ORM\Column(name="valor", type="string",length=100, nullable=true)
     **/
    private $valor;

    /**
     * @var integer $catalogo_id
     * @ORM\Column(name="detallecatalogo_id", type="integer", nullable=true)
     **/
    private $catalogo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumn(name="detallecatalogo_id", referencedColumnName="id")
     **/
    private $catalogo;

    /**
     * @var integer $orden
     * @ORM\Column(name="ordenvisualizacion", type="integer", nullable=true)
     **/
    private $orden;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set tipocatalogo_id
     * @param integer $tipocatalogo_id
     * @return catalogo
     **/
    public function setTipocatalogoId($tipocatalogo_id){
        $this->tipocatalogo_id = $tipocatalogo_id;
        return $this;
    }

    /**
     * Get tipocatalogo_id
     * @return integer
     **/
    public function getTipocatalogoId(){
        return $this->tipocatalogo_id;
    }


    /**
     * Set descripcion
     * @param string $descripcion
     * @return catalogo
     **/
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Set valor
     * @param string $valor
     * @return catalogo
     **/
    public function setValor($valor){
        $this->valor = $valor;
        return $this;
    }

    /**
     * Get valor
     * @return string
     **/
    public function getValor(){
        return $this->valor;
    }

    /**
     * Set catalogo_id
     * @param integer $catalogo_id
     * @return catalogo
     **/
    public function setCatalogoId($catalogo_id){
        $this->catalogo_id = $catalogo_id;
        return $this;
    }

    /**
     * Get catalogo_id
     * @return integer
     **/
    public function getCatalogoId(){
        return $this->catalogo_id;
    }

    /**
     * Set catalogo
     * @param catalogo $catalogo
     * @return catalogo
     **/
    public function setCatalogo(catalogo $catalogo){
        $this->catalogo = $catalogo;
        return $this;
    }

    /**
     * Get catalogo
     * @return catalogo
     **/
    public function getCatalogo(){
        return $this->catalogo;
    }

    /**
     * Set orden
     * @param integer $orden
     * @return catalogo
     **/
    public function setOrden($orden){
        $this->orden = $orden;
        return $this;
    }

    /**
     * Get orden
     * @return integer
     **/
    public function getOrden(){
        return $this->orden;
    }

    /**
     * Set estado
     * @param integer $estado
     * @return catalogo
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

}
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Tests\Utils;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Core\AppBundle\Utils\UtilDateTime;

/**
 * Description of UtilDatetimeTest
 * @author Luis Malquin
 */
class UtilDateTimeTest extends WebTestCase{
    
    public function testTiempoFecha(){
        $fecha_inicio='1984-02-12';        
        $result=UtilDateTime::tiempoEntreFechas($fecha_inicio);
        
        $this->assertEquals(Array("anios" => 32,"meses" => 7,"dias" => 11),$result);        
    }
    
   
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Tests\Utils;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Core\AppBundle\Utils\UtilTipoIdentificacion;

/**
 * Description of UtilTipoIdentificacion
 * @author Luis Malquin
 */
class UtilTipoIdentificacionTest extends WebTestCase{
    
    public function testValidarCedula(){
        $cedula='2222222222';        
        $result=  UtilTipoIdentificacion::validarCedula($cedula);        
        $this->assertEquals(Array("validacion" => true,"mensaje" => ""),$result);        
    }
    
   
}

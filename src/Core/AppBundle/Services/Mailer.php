<?php
/** 
 * Clase para el envío de emails
 * @author Luis Malquin
 */

namespace Core\AppBundle\Services;

class Mailer {

    private $swiftMailer;
    private $swiftMessage;
    
    /**
     * Constructor
     * @param \Swift_Mailer $swiftMailer
     * @param \Swift_Message $swiftMessage
     */
    public function __construct(\Swift_Mailer $swiftMailer, \Swift_Message $swiftMessage) {
        $this->swiftMailer = $swiftMailer;
        $this->swiftMessage = $swiftMessage;
    }
    
    /**
     * Asigna elementos de un mensaje al mailer
     * @param string/array $to destinatario
     * @param text/html $body cuerpo del 
     * @param string $subject
     * @param boolean $send true si se realiza automaticamente el envio
     * @return null||boolean true si se envio el mensaje
     */    
    public function setMessage($to, $body, $subject = '',$send=false){          
          $this->swiftMessage->setContentType('text/html');
          $this->swiftMessage->setCharset('utf-8');
          $this->swiftMessage->setTo($to);
          if($subject){
              $this->swiftMessage->setSubject($subject);
          }
          $this->swiftMessage->setBody($body);
          if ($send) {
            return $this->send();
          }
          return $this;
    }
    
    /**
     * Asigna un mensaje al mailer
     * @param \Swift_Message $message  
     * @param boolean $send true si se realiza automaticamente el envio 
     * @return null||boolean true si se envio el mensaje
     */
    public function setObjectMessage(\Swift_Message $message,$send=false) {
        $this->swiftMessage=$message;        
        if ($send) {
            return $this->send();
        }
        return $this;
    }
    
    
    /**
     * @return boolean true si el envío del mensaje se realizo satisfactoriamente
     */
    public function send(){          
          return $this->swiftMailer->send($this->swiftMessage);        
    }

    
}
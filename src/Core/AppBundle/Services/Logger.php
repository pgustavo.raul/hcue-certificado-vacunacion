<?php

/**
 * Clase de para el registro de loggs
 * @author Luis Malquin
 */

namespace Core\AppBundle\Services;
use \Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

class Logger {
    private $logger;

    /**      
     * @param \Core\AppBundle\Services\LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    public function addRecord(\Exception $exception)
    {
        $messageLog=  $this->getMessage($exception);                   
        $this->logger->error($messageLog);
        
                         
    }
    
    private function getMessage(\Exception $exception){
        return sprintf(
            'Se ha producido una excepción en la línea %s en el archivo %s: %s código: %s',
            $exception->getLine(),
            $exception->getFile(),    
            $exception->getMessage(),
            $exception->getCode()
        );
        
    }
}

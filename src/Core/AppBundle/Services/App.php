<?php
/** 
 * Clase para declara los metodos a usar en todas las aplicaciones
 * @author Luis Malquin
 */

namespace Core\AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;

class App {

    /**
     * Variable de tipo Container para el acceso  al contenedor
     * @var Container $container
     */
    private $container;

    /**
     * Constructor
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container=$container;
    }

    /**
     * Retorna un parametro declarado en app/config/parameters.yml
     * @param string $parameter parametro a retornar ej: app_globals.app_id
     * @return string
     */
    public function getParameter($parameter){
        $value="";
        if(strpos($parameter, '.')){
            $array = explode('.', $parameter);
            $globals = $this->container->getParameter($array[0]);
            if(array_key_exists($array[1], $globals)){
                $value = $globals[$array[1]];
            }
        }
        else{
            $value = $this->container->getParameter($parameter);
        }
        return $value;
    }


    /**
     * Retorna el nombre de la Aplicación
     * @return $nombre
     */
    public function getAplicacion(){
        $aplicacion_id = $this->getParameter('app_globals.app_id');
        if(empty($aplicacion_id)){
            $nombre = "Panel de Aplicaciones";
        }
        else{
            $aplicacionManager = $this->container->get('core.seguridad.manager.aplicacion');
            $nombre = $aplicacionManager->find($aplicacion_id)->getNombre();
        }
        $security = $this->container->get('core.seguridad.service.security_token');
        $security->setAttribute('nombre_aplicacion', $nombre);
        return $nombre;
    }

}
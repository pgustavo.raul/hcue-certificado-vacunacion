<?php

/**
 * Servicio con metodos para manejar la lógica de las personas
 * Descripcion de PersonaService
 * @author Richard Veliz
 */

namespace Core\AppBundle\Services;

use Core\AppBundle\Entity\Constantes;
use Core\AppBundle\Entity\Persona;
use Symfony\Component\DependencyInjection\Container;

class PersonaService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function setFotoPersona(Persona $objPersona)
    {
        $tipoidentificacion = ($objPersona->getCttipoidentificacionId()) ? $objPersona->getCttipoidentificacionId() : $objPersona->getCttipoidentificacion()->getId();
        $numeroidentificacion = $objPersona->getNumeroidentificacion();
        if ($tipoidentificacion == Constantes::CT_TIPOIDENTCEDULA && !$objPersona->getFotoId()) {
            $managerArchivo = $this->container->get("core.app.entity.manager.archivo");
            $archivo = $managerArchivo->getArchivoByIdentificacion($numeroidentificacion);
            if ($archivo) {
                $objPersona->setFoto($archivo);
            }
        }

        return $objPersona;
    }

    /**
     * Consume el servicio del webservis del Registro Civil y devuelve si esta fallecido o no
     * @param $cedula
     * @return bool
     */
    public function esFallecido($cedula)
    {
        $response = false;
        $container = $this->container;
        try {
            $service = $container->get("core.wsc.registrocivil");
            $serviceresult = $service->getInformacionPersonaPorNUI($cedula);
            $serviceresult = get_object_vars($serviceresult);
            $serviceresult = get_object_vars($serviceresult["return"]);
            if ($serviceresult["CodigoMensaje"] == "000") {
                $serviceresult = get_object_vars($serviceresult["Ciudadano"]);
                $condicion = $serviceresult["CondicionCedulado"];
                if ($condicion == "FALLECIDO" || $condicion == "EXTRANJERO FALLECIDO") {
                    $response = true;
                }
            }

        } catch (\Exception $exc) {
            $container->get('core.app.logger')->addRecord($exc);
        }

        return $response;
    }
}

<?php

/**
 * Servicio con metodos para manjear la lógica de Inscripciones
 * Descripcion de InscripcionService
 * @author Richard Veliz
 */

namespace Core\AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Core\AppBundle\Entity\Constantes;
use HCUE\InscripcionBundle\Entity\Inscripcion;

class FotoRegistrocivilService {

    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * Consume el webservice de FotoRegistrocivil
     * @param string $cedula Cedula de la persona
     * @return array con los resultados del consumo
     */
    public function consumirws($cedula) {
        $result = array();
        $service = $this->container->get("core.wsc.msp.fotoregistrocivil");
        try {
            $resultservice = $service->FotoRequest($cedula);
            $resultservice = get_object_vars($resultservice->return);
            if(is_array($resultservice)){
                $codigo = $resultservice["CodigoMensaje"];
                $mensaje = $resultservice["Mensaje"];
                $result = array("codigo" => $codigo, "mensaje" => $mensaje, "status" => 0);
                if ($codigo == "000") {
                    $foto=(array_key_exists("Foto",$resultservice))?get_object_vars($resultservice["Foto"]):array();
                    if(array_key_exists("FotoBase64",$foto)){
                        $result["fotobase64"]=$foto["FotoBase64"];
                        $result["status"]=1;
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->container->get('core.app.logger')->addRecord($ex);
            $message="Se ha producido un error en el sistema al consumir el servicio web de la foto";
            $result = array("mensaje" => $message,"status"=>0);
        }
        return $result;
    }

}

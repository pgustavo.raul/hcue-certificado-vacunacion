<?php

/**
 * Clase controlador para la ejecución de acciones de acceso de usuario
 * @author Luis Malquin 
 */

namespace Core\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Core\SeguridadBundle\Entity\Constantes;
use Core\SeguridadBundle\Event\UsuarioEvent;
use Core\SeguridadBundle\Entity\Auditoria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class LoginController extends Controller {

    /**
     * Realiza el autologeo de la aplicacion por medio de un token enviado desde el panel de aplicaciones
     * @Route("/autologin",name="core_seguridad_login_autologin")
     * @Method({"POST"})
     * @param Request $request
     * @return
     **/
    public function autologinAction(Request $request){
        $mngToken = $this->get('core.seguridad.manager.token');
        try {
            $aplicacion_id = $this->get('core.app.service.app')->getParameter('app_globals.app_id');
            $strtoken = $request->get('token');
            $resptoken = $mngToken->verifyToken(base64_decode($strtoken), $aplicacion_id);
            if($resptoken){
                $service_security = $this->get('core.seguridad.service.security_token');
                if($this->getUser()){
                    $service_security->setUsuarioPerfil();
                }
                else{
                    $user = $resptoken->getUsuario();
                    session_id($resptoken->getUidsesion());
                    $service_security->setAutentication($user);
                }
                return $this->redirect($this->generateUrl('after_login'));
            }
            else{
                throw new \Exception('No se encontraron coincidencias para el token '.base64_decode($strtoken).'  o no es un token válido.');
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
        }
        return $this->redirect($this->generateUrl('login'));
    }

    /**
     * @Route("/login", name="core_seguridad_login_login")online
     * @param Request $request
     * @Template()
     * @return text/html CoreSeguridadBundle:Login:login.html.twig
     */
    public function loginAction(Request $request){
        $usuario = $this->getUser();
        if($usuario){
            return $this->redirect($this->generateUrl('after_login'));
        }
        else{
            if($request->get('timeout')){
                $request->getSession()->set(Security::AUTHENTICATION_ERROR, new CustomUserMessageAuthenticationException('Su sesión ha expirado'));
            }
            $authenticationUtils = $this->get('security.authentication_utils');
            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();

            if(empty($error)) {
                $request->getSession()->set(Security::LAST_USERNAME, '');
            }
            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();
            // Limpiarmos el Token
            $this->get('security.token_storage')->setToken(null);
            return array(
                'last_username' => $lastUsername,
                'error' => $error,
            );
        }
    }

    /**
     * @Route("login_check", name="login_check")
     * @Method({"POST"})
     * @param Request $request
     */
    public function loginCheckAction(Request $request)
    {
        $usuarioManager = $this->get('core.seguridad.manager.usuario');
        $usuarioPerfilManager = $this->get('core.seguridad.manager.usuarioperfil');
        $authenticationUtils = $this->get('security.authentication_utils');
        $username = $request->get('usuario');
        $password = $request->get('clave');
        $user = $usuarioManager->getUsuarioByUsername($username);
        $max_intentos = $this->get('core.app.service.app')->getParameter('app_globals.attempts_userlogin');

        $session = $request->getSession();
        if(!$session->has('user.intentos')){
            $session->set('user.intentos', 0);
        }

        if ($user) {

            /** Si el estado es diferente de Bloqueado se Resetea la variable de Sesión de intentos de autenticación permitidos **/
            if($user->getCtestadoId() != Constantes::CT_ESTADOUSUARIOBLOQUEADO && $session->get('user.intentos') > $max_intentos){
                $session->set('user.intentos', 0);
            }

            /** Se Verifica que el Usuario Autenticado no se encuentre Bloqueado **/
            if($user->getCtestadoId() == Constantes::CT_ESTADOUSUARIOBLOQUEADO) {
                $request->getSession()->set(Security::AUTHENTICATION_ERROR, new CustomUserMessageAuthenticationException("Usuario bloqueado, por favor comuniquese con Soporte Técnico."));
            }
            else {

                $encoder_service = $this->container->get('security.password_encoder');

                /** Se Valida que el Password es correcto **/
                if($encoder_service->isPasswordValid($user, $password)) {

                    /** Se Verifica si el usuario debe cambiar su contraseña obligatoriamente **/
                    if($user->getCambiarclave()){
                        $session->set('user.intentos', 0);
                        $usuario_id = base64_encode($user->getId());
                        return $this->redirect($this->generateUrl('core_seguridad_login_change_pass', array('usuario_id'=>$usuario_id)));
                    }

                    /** Se Verifica si el usuario tiene asignado al menos un perfil **/
                    elseif(!$usuarioPerfilManager->hasUsuarioPerfil($user->getId())) {
                        $request->getSession()->set(Security::AUTHENTICATION_ERROR, new CustomUserMessageAuthenticationException("Su Usuario no tiene un perfil asignado, por favor comuniquese con Soporte Técnico."));
                    }

                    /** Se crea la autenticación del Usuario al Sistema y se redirecciona al Login **/
                    else {

                        $session->set('user.intentos', 0);
                        $this->get('core.seguridad.service.security_token')->setAutentication($user);
                        return $this->redirect($this->generateUrl('after_login'));

                    }

                }

                /** Se Contabiliza el número de intentos de autenticación permitidos por Usuario **/
                else {

                    if($username != $authenticationUtils->getLastUsername()) {
                        $session->set('user.intentos', 0);
                    }

                    $intentos=$session->get('user.intentos')+1;
                    $session->set('user.intentos', $intentos);

                    if($intentos >= $max_intentos) {
                        $user->setCtestadoId(Constantes::CT_ESTADOUSUARIOBLOQUEADO);
                        $usuarioManager->save($user);
                        $request->getSession()->set(Security::AUTHENTICATION_ERROR, new CustomUserMessageAuthenticationException("Usuario bloqueado, por favor comuniquese con Soporte Técnico."));
                        $request->getSession()->set(Security::LAST_USERNAME, $username);
                        $this->get('event_dispatcher')->dispatch('core.seguridad.event.usuario.sendmailblockusuario', new UsuarioEvent($user));
                    }
                    else {
                        $request->getSession()->set(Security::AUTHENTICATION_ERROR, new CustomUserMessageAuthenticationException('Sus Credenciales no son válidas'));
                        $request->getSession()->set(Security::LAST_USERNAME, $username);
                    }

                }

            }

        }
        else{

            $request->getSession()->set(Security::AUTHENTICATION_ERROR, new CustomUserMessageAuthenticationException('Sus Credenciales no son válidas'));

        }
        return $this->redirect($this->generateUrl('login'));
    }

    /**
     * @Route("/logout", name="core_seguridad_login_logout")
     * @Template()
     * @param Request $request
     * @return text/html CoreSeguridadBundle:Login:login.html.twig
     */
    public function logoutAction(Request $request){

        $app_service = $this->get('core.app.service.app');
        $url_login = $app_service->getParameter('app_globals.url_login');

        $this->get('core.seguridad.service.security_token')->destroyAutentication($this->getUser());

        $timeout = ($request->get('timeout'))?1:0;

        if($url_login){
            return $this->redirect($url_login.'/logout?timeout='.$timeout);
        }

        return $this->redirect($this->generateUrl('login').'?timeout='.$timeout);

    }

    /**
     * @Route("/forgot_pass", name="core_seguridad_login_forgot_pass")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse {"status","message"}
     */
    public function forgotPassAction(Request $request) {
        $status = 0;
        $message="";
        $usuarioManager = $this->get('core.seguridad.manager.usuario');
        $username= $request->get('username');
        $email=$request->get('email');
        $usuario = $usuarioManager->getUsuarioByUsername($username);
        if(empty($usuario)){
            $message="El usuario ingresado no existe.";
        }
        else{
            if($email != $usuario->getCorreo()){
                $message="El email ingresado no corresponde al usuario.";
            }
            else{
                $usuarioManager->resetPassword($usuario);
                $message="Se ha enviado un correo con su clave temporal.!";
                $status = 1;
            }
        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * @Route("/change_pass/{usuario_id}", name="core_seguridad_login_change_pass")
     * @Method({"GET","POST"})
     * @Template()
     * @param integer $usuario_id
     * @return text/html CoreSeguridadBundle:Login:changepass.html.twig
     */
    public function changepassAction($usuario_id) {
        $usuarioManager = $this->get('core.seguridad.manager.usuario');
        $id = base64_decode($usuario_id);
        $usuario = $usuarioManager->find($id);
        if($usuario->getCambiarclave()){
            return array(
                "status" => 1,
                "usuario"=> $usuario
            );
        }
        else{
            return $this->redirect($this->generateUrl('login'));
        }
    }

    /**
     * @Route("/change_pass_token/{token}", name="core_seguridad_login_change_pass_by_token")
     * @Method({"GET","POST"})
     * @Template()
     * @param string $token
     * @return text/html CoreSeguridadBundle:Default:changepass.html.twig
     */
    public function changepassbytokenAction($token) {
        $status = 0;
        $usuario=0;
        $tokenManager = $this->get('core.seguridad.manager.token');
        $resptoken=$tokenManager->verifyToken($token);
        if($resptoken){
            $status=1;
            $usuario=$resptoken->getUsuario();
        }
        return $this->render('CoreSeguridadBundle:Login:changepass.html.twig', array(
            "status" => $status,
            "usuario"=> $usuario
        ));
    }

    /**
     * @Route("/save_new_pass", name="core_seguridad_login_save_new_pass")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse {"status","message"}
     */
    public function savenewpassAction(Request $request) {
        $usuario_id= $request->get('usuario_id');
        $clave=$request->get('clave');
        $status = 0;
        $message="";
        $usuarioManager=$this->get('core.seguridad.manager.usuario');
        $usuario=$usuarioManager->find($usuario_id);

        if(empty($usuario)){
            $message="El usuario ingresado no existe.";
        }
        else{
            $usuarioManager->changePassword($usuario, $clave);
            $message="Su contraseña ha sido actualizada correctamente.!";
            $status = 1;
        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

}
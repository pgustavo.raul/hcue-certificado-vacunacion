<?php
/**
 * Clase controlador para la ejecución de acciones de carga de menus de la aplicacion
 * @author Luis Malquin 
 */

namespace Core\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MenuController extends Controller {

    /**
     * Retorna la vista de los menus pertenecientes a una aplicación
     * @Route("/sidebar",name="core_seguridad_menu_sidebar")
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html CoreSeguridadBundle::sidebar.html.twig
     **/
    public function sidebarAction(){
        $aplicacion_id = $this->get('core.app.service.app')->getParameter('app_globals.app_id');
        $user = $this->getUser();
        $menuManager = $this->get('core.seguridad.manager.menu');
        $menus = $menuManager->ObtenerMenuSidebar($aplicacion_id, $user->getId(), $user->getUsuarioPerfil()->getId());        
        $routeMaster = $this->container->get('request_stack')->getMasterRequest()->getPathInfo();
        return $this->render('CoreSeguridadBundle::sidebar.html.twig', array(
            'menus' => $menus,
            'routeMaster' => $routeMaster,
        ));
    }
}
<?php

/**
 * Clase controlador para la ejecución de acciones de informacion yb permisos de usuario
 * @author Luis Malquin 
 */

namespace Core\SeguridadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Core\SeguridadBundle\Entity\Token;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="core_seguridad_default_index")
     * @Method({"GET"})
     * @Template()
     * @return text/html CoreSeguridadBundle:Default:index_resp.html.twig
     **/
    public function indexAction()
    {
        return $this->render('CoreSeguridadBundle:Default:index_resp.html.twig');
    }

    /**
     * Retorna la vista que muestra el navbar de la Aplicación
     * @Route("/navbar",name="core_seguridad_default_navbar")
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html CoreSeguridadBundle::navbar.html.twig
     **/
    public function navbarAction(){
        $session = $this->get('session');
        // Obtenermos el Id y el Nombre de la aplicación en uso
        $app_service = $this->get('core.app.service.app');
        $app_id = $app_service->getParameter('app_globals.app_id');

        $modPerfil=($app_id)?"modalPerfil_$app_id":'modalPerfil';

        if(!$session->has($modPerfil)){
            $session->set($modPerfil, 0);
        }
        // Obtenemos los servicios de UsuarioperfilManager.php, App.php y SecurityToken.php
        $usuarioperfilManager = $this->get('core.seguridad.manager.usuarioperfil');
        $security = $this->get('core.seguridad.service.security_token');

        // Obtenemos el Usuario, Usuario Perfil activo y la Entidad a la que pertece
        $usuario = $security->getUser();
        $perfilentidad = ($app_id)?$security->getPerfilEntidad():'';
        $entidad = ($app_id)?$security->getEntidad():'';
        // Obtenemos las aplicaciones a la cual tiene acceso el usuario autenticado
        $apps = ($app_id)?$this->get('core.seguridad.manager.usuario')->getAplicaciones($usuario->getId()):'';
        $aplicacion = $security->getAttribute('nombre_aplicacion');
        // Obtenemos los perfiles asignados al usuario autenticado
        $perfiles = ($app_id)?$usuarioperfilManager->hasUsuarioPerfil($usuario->getId()):0;
        $persona = ($app_id)?$security->getPersona():$usuario->getPersona();
        return $this->render('CoreSeguridadBundle::navbar.html.twig', array(
            'apps'=> $apps,
            'perfiles' => (int) $perfiles,
            'aplicacion' => $aplicacion,
            'entidad' => $entidad,
            'perfilentidad' => $perfilentidad,
            'persona' => $persona
        ));
    }

    /**
     * Retorna la vista de los perfiles asociados a un Usuario
     * @Route("/perfiles",name="core_seguridad_default_perfiles",options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Template()
     * @return text/html CoreSeguridadBundle::perfiles.html.twig
     **/
    public function perfilesAction(){
        // Obtenermos el Id y el Nombre de la aplicación en uso
        $app_service = $this->get('core.app.service.app');
        $app_id = $app_service->getParameter('app_globals.app_id');
        $modPerfil=($app_id)?"modalPerfil_$app_id":'modalPerfil';
        $this->get('session')->set($modPerfil, 1);
        $service_security = $this->get('core.seguridad.service.security_token');
        $usuarioperfil_id = $service_security->getUsuarioPerfilId();
        $usuarioperfilManager=  $this->get('core.seguridad.manager.usuarioperfil');
        $perfiles = $usuarioperfilManager->getUsuarioPerfiles($this->getUser()->getId());

        return $this->render('CoreSeguridadBundle::perfiles.html.twig', array(
            'usuarioperfil' => $usuarioperfil_id,
            'perfiles' => $perfiles,
        ));
    }

    /**
     * Cambia el Perfil del Usuario
     * @Route("/cambiarPerfil",name="core_seguridad_default_cambiarPerfil",options={"expose"=true})
     * @Method({"GET", "POST"})
     * @param Request $request
     **/
    public function cambiarPerfilAction(Request $request){
        $status = 0;
        $message = "";
        try {
            $securityToken = $this->get('core.seguridad.service.security_token');
            $id = $request->get('perfil');
            $securityToken->setUsuarioPerfil($id);
            $status = 1;
        }catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode())?"Se ha producido un error en el sistema, no es posible cambiar el perfil del usuario.":$ex->getMessage();
        }
        return new JsonResponse(array('id'=>$id, 'status'=>$status, "message" => $message));
    }

    /**
     * Permite Generar un token para acceder a una aplicacion
     * @Route("/generatetoken/{app_id}", name="core_seguridad_default_generatetoken", requirements={"app_id" = "\d+"}, defaults={"app_id" = 0})
     * @Method({"GET"})
     * @param integer $app_id
     * @return JsonResponse
     */
    public function generatetokenAction($app_id){
        $status = 0;
        $message = '';

        try {
            $idsession = $this->get('session')->getId();
            $serviceToken = $this->get("core.seguridad.service.token");
            $managerToken = $this->get("core.seguridad.manager.token");
            $em = $this->container->get('doctrine.orm.entity_manager');
            $token = new Token();

            $usuario_id =  $this->getUser()->getId();
            $strtoken = $serviceToken->getToken(20);
            $segundos = $this->get('core.app.service.app')->getParameter('app_globals.timeout_token_autologin');
            $fechacreacion = date('Y-m-d H:i:s');
            $fechaexpiracion = date('Y-m-d H:i:s', strtotime($fechacreacion) + $segundos);

            $token->setUsuario($em->getReference('CoreSeguridadBundle:Usuario', $usuario_id));
            $token->setAplicacion($em->getReference('CoreSeguridadBundle:Aplicacion', $app_id));
            $token->setFechacreacion(new \DateTime($fechacreacion));
            $token->setFechaexpiracion(new \DateTime($fechaexpiracion));
            $token->setToken($strtoken);
            $token->setTiemposeg($segundos);
            $token->setUidsesion($idsession);

            $managerToken->save($token);
            $status = 1;
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode())?"Se ha producido un error en el sistema, no es posible generar un nuevo token.":$ex->getMessage();
        }
        return new JsonResponse(array('token' => base64_encode($strtoken), 'status' => $status, "message" => $message));
    }

    /**
     * @Route("/change_pass", name="core_seguridad_default_change_pass")
     * @Method({"GET","POST"})
     * @Template()
     * @return text/html CoreSeguridadBundle:Default:changepass.html.twig
     */
    public function changepassAction(){
        $app_service = $this->get('core.app.service.app');
        $app_id = $app_service->getParameter('app_globals.app_id');
        $security = $this->get('core.seguridad.service.security_token');
        $persona = ($app_id)?$security->getPersona():$this->getUser()->getPersona();
        return array("status" => 1, 'persona'=> $persona);
    }

    /**
     * @Route("/save_new_pass", name="core_seguridad_default_save_new_pass")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse {"status","message"}
     */
    public function savenewpassAction(Request $request){
        $encoder_service = $this->get('security.password_encoder');
        $usuarioManager = $this->get('core.seguridad.manager.usuario');

        $status = 0;
        $message="";
        //clave ingresada por el usuario como clave actual.
        $clave_actual = $request->get('clave_actual');
        $nueva_clave = $request->get('nueva_clave');
        $conf_nueva_clave= $request->get('conf_nueva_clave');

        $usuario_id = $this->getUser()->getId();
        $usuario = $usuarioManager->find($usuario_id);

        if(empty($usuario)){
            $message="El usuario ingresado no existe.";
        }
        else{
            if($encoder_service->isPasswordValid($usuario, $clave_actual)){
                if($clave_actual==$nueva_clave){
                    $message="La nueva contraseña no puede ser la misma que la contraseña anterior.";
                }
                else{
                    if($nueva_clave!=$conf_nueva_clave){
                        $message="Las nuevas contraseñas ingresadas no coinciden.";
                    }
                    else{
                        $usuarioManager->changePassword($usuario, $nueva_clave);
                        $message="Su contraseña ha sido actualizada correctamente.!";
                        $status = 1;
                    }
                }
            }
            else{
                $message="La contraseña ingresada no corresponde a su contraseña actual.";
            }
        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * @Route("/check_pass_actual", name="core_seguridad_default_check_pass_actual")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse {"status","message"}
     */
    public function checkpassactualAction(Request $request){
        $encoder_service = $this->get('security.password_encoder');
        $usuarioManager = $this->get('core.seguridad.manager.usuario');

        $status = 0;
        $message="";
        //clave ingresada por el usuario como clave actual.
        $clave_actual = $request->get('clave_actual');

        $usuario_id = $this->getUser()->getId();
        $usuario = $usuarioManager->find($usuario_id);

        if(!$encoder_service->isPasswordValid($usuario, $clave_actual)){
            $message="La contraseña ingresada no corresponde a su contraseña actual.";
        }
        else{
            $status = 1;
            $message="";
        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /**
     * @Route("/check_new_pass", name="core_seguridad_default_check_new_pass")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse {"status","message"}
     */
    public function checknewpassAction(Request $request){
        $status = 0;
        $message="";
        //clave ingresada por el usuario como clave actual.
        $clave_actual = $request->get('clave_actual');
        $nueva_clave = $request->get('nueva_clave');
        if($clave_actual==$nueva_clave){
            $message="La nueva contraseña no puede ser la misma que la contraseña anterior.";
        }
        else{
            $status = 1;
            $message="";
        }
        return new JsonResponse(array("status" => $status, "message" => $message));
    }

}
<?php

/**
 * Description of BaseVoter
 * @author Luis Malquin
 */

namespace Core\SeguridadBundle\Security\Authorization\Voter;

use Core\SeguridadBundle\Entity\Usuario;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\DependencyInjection\Container;
use Core\SeguridadBundle\Entity\Constantes;

class AppVoter implements VoterInterface {
    
    const MENUROL_ID_NAME = 'menurol_id';
    const MENUROL_NAME = 'menurol';
    const VIEW = 'view';
    const ADD ='add';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const EXPORT = 'export';
    const HAS_PERMISSION = 'hasPermission';
        
    const INFO_INSTITUCION = 'info_institucion';
    const INFO_ZONA = 'info_zona';
    const INFO_DISTRITO = 'info_distrito';
    const INFO_CIRCUITO = 'info_circuito';
    const INFO_PROVINCIA = 'info_provincia';
    const INFO_CANTON = 'info_canton';
    const INFO_PARROQUIA = 'info_parroquia';
    const INFO_ESTABLECIMIENTO = 'info_establecimiento';
    const INFO_USUARIO = 'info_usuario';
    
    private $container;
    private $securityToken;
    private $menurol;
    
    /**     
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container = $container;
        $this->securityToken = $this->container->get('core.seguridad.service.security_token');
    }
    
    public function supportsAttribute($attribute) {
        return in_array($attribute, 
           array(
               self::HAS_PERMISSION,
               self::VIEW, self::EDIT,self::ADD,self::DELETE,self::EXPORT,
               self::INFO_INSTITUCION,self::INFO_ZONA,self::INFO_DISTRITO,self::INFO_CIRCUITO,
               self::INFO_PROVINCIA,self::INFO_CANTON,self::INFO_PARROQUIA,self::INFO_ESTABLECIMIENTO,self::INFO_USUARIO
            ));
    }

    public function supportsClass($class) {
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes) {

        $hasPermission=VoterInterface::ACCESS_DENIED;

        // comprueba si el metodo se usa correctamente, solo permite un atributo        
        if (1 !== count($attributes)) {
            throw new \InvalidArgumentException(
                'Sólo se permite un atributo para HAS_PERMISSION, ADD, VIEW, EDIT,EXPORT o DELETE'
            );
        }
        
         // establece el atributo para comprobar
        $attribute = $attributes[0];

        // Comprobar si el atributo es soportado
        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }
        
        // Obtener acceso al usuario actual
        $user = $token->getUser();
        
        // Asegúrese de que hay un objeto de usuario (es decir, que el usuario ha iniciado sesión)
        if (!$user instanceof UserInterface) {
           return VoterInterface::ACCESS_DENIED;
        }
                
        // compruebe que el objeto User es la entidad esperada (esto
         // sólo ocurre cuando no configuraste correctamente el sistema de seguridad)
        if (!$user instanceof Usuario) {
            throw new \LogicException('El Usuario no es una clase de tipo Core/SeguridadBundle/Entity/Usuario !');
        }
        
        switch($attribute) {
            case self::HAS_PERMISSION:
                $hasPermission= $this->hasPermission();
                break;
            
            case self::ADD:
                $hasPermission= $this->canAdd();
                break;
            
            case self::VIEW:
                $hasPermission= $this->canView();
                break;

            case self::EDIT:
                $hasPermission= $this->canEdit();
                break;
            
            case self::DELETE:
                $hasPermission= $this->canDelete();
                break;
            case self::EXPORT:
                $hasPermission= $this->canExport();
                break;
            
            case self::INFO_INSTITUCION:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_INSTITUCION);
                break;
            case self::INFO_ZONA:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_ZONA);
                break;
            case self::INFO_DISTRITO:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_DISTRITO);
                break;
            case self::INFO_CIRCUITO:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_CIRCUITO);
                break;
            case self::INFO_PROVINCIA:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_PROVINCIA);
                break;
            case self::INFO_CANTON:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_CANTON);
                break;
            case self::INFO_PARROQUIA:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_PARROQUIA);
                break;
            case self::INFO_ESTABLECIMIENTO:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_ESTABLECIMIENTO);
                break;
            case self::INFO_USUARIO:
                $hasPermission= $this->canAccessInfo(Constantes::CT_INFO_USUARIO);
                break;
            default:
                $hasPermission=VoterInterface::ACCESS_DENIED;
        }

        return $hasPermission;
    }
    
    
    private function hasPermission(){
        $hasPermission=VoterInterface::ACCESS_DENIED;
        if($this->canAdd()===VoterInterface::ACCESS_GRANTED || 
                $this->canView()===VoterInterface::ACCESS_GRANTED || 
                $this->canEdit()===VoterInterface::ACCESS_GRANTED || 
                $this->canDelete()===VoterInterface::ACCESS_GRANTED ||
                $this->canExport()===VoterInterface::ACCESS_GRANTED
                ){            
            $hasPermission=VoterInterface::ACCESS_GRANTED;            
        }
        return $hasPermission;
    }
    
    private function canAdd()
    {        
        $hasPermission=VoterInterface::ACCESS_DENIED;
        $menurolobj=$this->getMenuRol();               
        if($menurolobj && $menurolobj->getAgregar()){             
                $hasPermission=VoterInterface::ACCESS_GRANTED;            
        }
        return $hasPermission;
    }
    
    private function canEdit()
    {
        $hasPermission=VoterInterface::ACCESS_DENIED; 
        $menurolobj=$this->getMenuRol();   
        
        if($menurolobj && $menurolobj->getEditar()){             
                $hasPermission=VoterInterface::ACCESS_GRANTED;            
        }
        return $hasPermission;
    }
    
    private function canView()
    {
        $hasPermission=VoterInterface::ACCESS_DENIED;     
        $menurolobj=$this->getMenuRol();                
        if($menurolobj && $menurolobj->getVer()){           
            $hasPermission= VoterInterface::ACCESS_GRANTED;            
        }       
        return $hasPermission;
    }
    
    private function canExport()
    {   $hasPermission=VoterInterface::ACCESS_DENIED;       
        $menurolobj=$this->getMenuRol();               
        if($menurolobj && $menurolobj->getExportar()){             
                $hasPermission= VoterInterface::ACCESS_GRANTED;            
        }
        return $hasPermission;
    }
    
    private function canDelete()
    {   $hasPermission=VoterInterface::ACCESS_DENIED;                     
        $menurolobj=$this->getMenuRol();              
        if($menurolobj && $menurolobj->getEliminar()){            
            $hasPermission= VoterInterface::ACCESS_GRANTED;            
        }
        return $hasPermission;
    }
    
    private function canAccessInfo($ctverinfo_id){
        $hasPermission=VoterInterface::ACCESS_DENIED;
        $menurolobj=$this->getMenuRol();              
        if($menurolobj && $menurolobj->getCtverinfoId()){                                              
                
                if($menurolobj->getCtverinfoId()==$ctverinfo_id){
                    $hasPermission= VoterInterface::ACCESS_GRANTED;
                }else{
                    $ctverinfo=$menurolobj->getCtverinfo();
                    $catalogo_manager = $this->container->get('core.app.entity.manager.catalogo');
                    $ctverinfoaccess = $catalogo_manager->find($ctverinfo_id);
                    if($ctverinfoaccess->getOrden()>$ctverinfo->getOrden()){                                    
                        $hasPermission= VoterInterface::ACCESS_GRANTED;
                    }
                }              
            
            
        }
        return $hasPermission;
    }  
            
    
    private function getMenuRol(){

        if(!$this->menurol){
            $menurolManager = $this->container->get('core.seguridad.manager.menurol');
            $this->menurol = $menurolManager->verificarPermisos();
        }

        return $this->menurol;
    }

}
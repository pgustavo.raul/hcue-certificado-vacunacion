<?php
/**
 * Descripcion de escucha de usuario
 * @author Luis Malquin
 */

namespace Core\SeguridadBundle\EventListener;

use Core\SeguridadBundle\Event\UsuarioEvent;
use Symfony\Component\DependencyInjection\Container;

class UsuarioListener{
    
    private $container;
    
    /**
     * Constructor
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container=$container;
    }
    
    /**
     * Envia un email con informacion del cambio de clave del usuario
     * @return boolean true si se envío el correo
     */
    public function sendMailChangePassword(UsuarioEvent $usuarioEvent){
        $mailerService=  $this->container->get('core.seguridad.service.mailer');
        return $mailerService->sendChangePassword($usuarioEvent->getUsuario());
    }

    /**
     * Envia un email con informacion del cambio de clave del usuario
     * @return boolean true si se envío el correo
     */
    public function sendBlockUsuario(UsuarioEvent $usuarioEvent){
        $mailerService=  $this->container->get('core.seguridad.service.mailer');
        return $mailerService->sendBlockUsuario($usuarioEvent->getUsuario());
    }
}
<?php
 
namespace Core\SeguridadBundle\EventListener;

use Core\SeguridadBundle\Event\TokenEvent;
use Symfony\Component\DependencyInjection\Container;

class TokenListener{
    
    private $container;
    
    /**
     * Constructor
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container=$container;
    }
    
    /**
     * Envia un email con informacion de la cuenta de usuario
     * @param TokenEvent $tokenEvent
     * @return boolean true si se envío el correo
     */
    public function sendMailResetPassword(TokenEvent $tokenEvent){
        $mailerService=  $this->container->get('core.seguridad.service.mailer');
        return $mailerService->sendRestPassword($tokenEvent->getToken());
    }
}
<?php
/**
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;

/**
 * Clase de manipulación del repositorio Perfil
 */
class PerfilManager extends BaseManager {

    /**
     * Obtiene la lista de perfiles hijos de un perfil determinado
     * @param integer $perfil_id para retornar los perfiles de acuerdo a su perfil padre
     * @param boolean $dql <true> para retornar el dql del QueryBuilder
     * @return
     */
    public function obtenerChildsPerfil($perfil_id) {
        return $this->getRepository()->getPerfilChildrens($perfil_id);

    }

    /**
     * Obtiene la lista de perfiles hijos de un perfil determinado
     * @param integer $perfil_id para retornar los perfiles de acuerdo a su perfil padre
     * @param boolean $dql <true> para retornar el dql del QueryBuilder
     * @return
     */
    public function obtenerPerfilAllChildrens($perfil_id) {
        $results = $this->getRepository()->getPerfilAllChildrens($perfil_id);
        $arrayResult=array();
        foreach ($results as $obj){
            $arrayResult[]= $obj->getId();
        }
        return $arrayResult;
    }

}
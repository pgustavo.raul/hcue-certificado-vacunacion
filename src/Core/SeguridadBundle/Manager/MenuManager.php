<?php
/**
 * Clase de manipulación del repositorio Menu
 * @author Luis Malquin
 */
namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;

class MenuManager extends BaseManager{

    /**
     * Crea el Array de Menus pertenecientes a una aplicación
     * @param integer $app_id Id de la aplicación a la que pertenecen los menus
     * @param integer $usuario_id
     * @param integer $usuarioPerfil Id del perfil utilizado
     * @return Array de Menus para dibujar en Sidebar
     */
    public function ObtenerMenuSidebar($app_id, $usuario_id, $usuarioPerfil){

        $menus = $this->getRepository()->getMenusAplicacionByUsuario($app_id, $usuario_id, $usuarioPerfil);

        $menuSidebar = array();
        if(!empty($menus)){
            foreach ($menus as $menu) {
                $id = $menu["id"];
                $menu_id = $menu["menu_id"];
                if(!$menu_id) {
                    $menuSidebar[$id] = array(
                        'menurol_id'=>$menu['menurol_id'],
                        'titulo'=>$menu['titulo'],
                        'icono'=>$menu['icono'],
                        'url'=>$menu['url'],
                        'key'=>$id);

                    $arrayreturn = $this->obtenerHijos($menus, $id);
                    if(!empty($arrayreturn)){
                        $menuSidebar[$id]['hijos'] = $arrayreturn;
                    }
                }
            }
        }
        return $menuSidebar;

    }

    /**
     * Retorna el array de menus hijos de un menu
     * @param Array $menus
     * @param integer $id Id del menu padre
     * @return Array de menus hijoss
     */
    public function obtenerHijos($menus, $id) {
        $menuSidebar = array();

        foreach ($menus as $menu) {
            $idMenu = $menu["id"];
            $menuPadre = $menu["menu_id"];
            if($menuPadre == $id) {
                $menuSidebar[$idMenu] = array(
                    'menurol_id'=>$menu['menurol_id'],
                    'titulo'=>$menu['titulo'],
                    'icono'=>$menu['icono'],
                    'url'=>$menu['url'],
                    'key'=>$idMenu);

                $arrayreturn = $this->obtenerHijos($menus, $idMenu);
                if(!empty($arrayreturn)){
                    $menuSidebar[$idMenu]['hijos'] = $arrayreturn;
                }
            }
        }

        return $menuSidebar;
    }

}

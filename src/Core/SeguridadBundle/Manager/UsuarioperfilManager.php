<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;

/**
 * Clase de manipulación del repositorio Usuarioperfil
 */
class UsuarioperfilManager extends BaseManager{

     /**
     * Verifica si un Usuario tiene asignado un Perfil para pode iniciar sesión
     * @param integer $usuario_id
     * @return boolean
     */
    public function getUsuarioPerfiles($usuario_id){
        $aplicacion_id = $this->getContainer()->get('core.app.service.app')->getParameter('app_globals.app_id');
        return $this->getRepository()->getUsuarioPerfiles($usuario_id, $aplicacion_id);
    }

    /**
     * Verifica si un Usuario tiene asignado un Perfil para pode iniciar sesión
     * @param integer $usuario_id
     * @return boolean
     */
    public function hasUsuarioPerfil($usuario_id){
        $aplicacion_id = $this->getContainer()->get('core.app.service.app')->getParameter('app_globals.app_id');
        return $this->getRepository()->hasUsuarioPerfil($usuario_id, $aplicacion_id);
    }

    /**
     * Verifica si un Usuario tiene asignado un Perfil para pode iniciar sesión
     * @param integer $usuario_id
     * @param boolean $all <true> para retornar el objeto con todas sus relaciones
     * @return boolean
     */
    public function firstUsuarioPerfil($usuario_id, $all=true){
        $aplicacion_id = $this->getContainer()->get('core.app.service.app')->getParameter('app_globals.app_id');
        return $this->getRepository()->firstUsuarioPerfil($usuario_id, $aplicacion_id, $all);
    }

    /**
     * Permite la intereacción con el Repository para obtener los perfiles asociados a un usuario
     * @param string $usuario_id Id del Usuario
     * @param boolean $arrayResult <true> para retornar el modelo o entidad como un array
     * @return Array u Objeto de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function obtenerPerfilesPorEntidad($usuario_id, $estado=true, $arrayResult = false){
        $security = $this->getContainer()->get("core.seguridad.service.security_token");
        $autorizacion = $this->getContainer()->get("core.seguridad.service.security_authorization");

        $perfil_id = $security->getPerfil()->getId();
        $perfilManager = $this->getContainer()->get("core.seguridad.manager.perfil");
        $perfiles = $perfilManager->obtenerPerfilAllChildrens($perfil_id);
        // Query Builder por default
        $qb = $this->getRepository()->getPerfilesporEntidad($usuario_id, $perfiles, $perfil_id, $estado);

        $qb->select('up,pe,e')
            ->leftJoin('pe.entidad','e');

        if(!$autorizacion->canAccessInfoInstitucion()){

            // Ubicación Geografica del Usuario Entidad/Circuito/Distrito/Zona && Provincia/Cantón/Parroquia
            $ubicacion = $security->getUbicacionGeografica();

            if($autorizacion->canAccessInfoZona()){
                $qb->join('e.circuito','c')
                    ->join('c.distrito','d')
                    ->andWhere('d.zona_id = :zona')
                    ->setParameter('zona', $ubicacion['zona']->getId());
            }
            elseif($autorizacion->canAccessInfoDistrito()){
                $qb->join('e.circuito','c')
                    ->andWhere('c.distrito_id = :distrito')
                    ->setParameter('distrito', $ubicacion['distrito']->getId());
            }
            elseif($autorizacion->canAccessInfoCircuito()){
                $qb->andWhere('e.circuito_id = :circuito')
                    ->setParameter('circuito', $ubicacion['circuito']->getId());
            }
            elseif($autorizacion->canAccessInfoEstablecimiento()){
                $qb->andWhere('pe.entidad_id = :entidad')
                    ->setParameter('entidad', $ubicacion['entidad']->getId());
            }

        }

        $query = $qb->getQuery()->useQueryCache(true);
        return ($arrayResult)?$query->getArrayResult():$query->getResult();
    }

    /**
     * Obtiene el usuarioperfil deseado con todas sus relaciones
     * @param integer $id Id del Usuarioperfil seleccionado
     * @return
     */
    public function getUsuarioperfil($id) {
        return $this->getRepository()->getUsuarioperfil($id);
    }

}
<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use \Core\SeguridadBundle\Entity\Token;

/**
 * Clase de manipulación del repositorio Token
 */
class TokenManager extends BaseManager{

     /**
     * Verifica un y actualiza la validacion de un Token
     * @param string $token Token
     * @param integer $app_id Id de la aplicacion
     * @return boolean true si se realizó la validación con éxito
     */
    public function verifyToken($token,$app_id=0) {
        $response=false;
        if($app_id){
            $objtoken = $this->getRepository()->validTokenAplicacion($token,$app_id);
        }
        else{
            $objtoken = $this->getRepository()->validToken($token);
        }
        if ($objtoken) {
            $response=$objtoken;
            $objtoken->setEstado(0);
            $this->save($objtoken);
        }
        return $response;
    }

    /**
     * Crea el token al resetear el Password
     * @param Model $usuario
     * @return boolean true si se realizó la operación con éxito
     */
    public function generateToken($usuario){
        $response=false;
        try {
            $serviceToken = $this->getContainer()->get("core.seguridad.service.token");
            $token = new Token();
            $strtoken = $serviceToken->getToken(20);
            $segundos = $this->getContainer()->get('core.app.service.app')->getParameter('app_globals.timeout_token_reset_pass');
            $fechacreacion = date('Y-m-d H:i:s');
            $fechaexpiracion = date('Y-m-d H:i:s', strtotime($fechacreacion) + $segundos);
            //Almacena los datos del Token
            $token->setUsuario($this->getEntityManager()->getReference('CoreSeguridadBundle:Usuario', $usuario->getId()));
            $token->setFechacreacion(new \DateTime($fechacreacion));
            $token->setFechaexpiracion(new \DateTime($fechaexpiracion));
            $token->setToken($strtoken);
            $token->setTiemposeg($segundos);
            $this->save($token);
            $response=$token;
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

        return $response;
    }
}
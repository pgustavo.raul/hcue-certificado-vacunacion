<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Core\SeguridadBundle\Entity\Sesion;
use Core\SeguridadBundle\Entity\Constantes;

/**
 * Clase de manipulación del repositorio Sesion
 */
class SesionManager extends BaseManager{

    /**
     * Crea la sesion del usuario
     * @param integer $usuario_id
     * @return boolean true si se realizó la operación con éxito
     */
    public function saveSesion($usuario_id){
        $request = $this->getContainer()->get('request_stack')->getCurrentRequest();
        $sesion = new Sesion();

        $aplicacion_id = $this->getContainer()->get('core.app.service.app')->getParameter('app_globals.app_id');
        $navegador = $request->headers->get('User-Agent');
        $uidsesion = $this->getContainer()->get('session')->getId();

        $sesion->setUsuario($this->getEntityManager()->getReference('CoreSeguridadBundle:Usuario', $usuario_id));
        $sesion->setNavegador($navegador);
        $sesion->setUidsesion($uidsesion);

        if($aplicacion_id){
            $sesion->setAplicacion($this->getEntityManager()->getReference('CoreSeguridadBundle:Aplicacion', $aplicacion_id));
        }

        $this->save($sesion);
        return true;
    }

    /**
     * Cierra las sesiones
     * @return boolean true si se realizó la operación con éxito
     */
    public function closeSesiones(){
        $em = $this->getEntityManager();
        $uidsesion = $this->getContainer()->get('session')->getId();
        $sesion = $em->getRepository('CoreSeguridadBundle:Sesion');
        $qb = $sesion->createQueryBuilder('s');
        $qb->update()
            ->where('s.uidsesion = :uidsesion')
            ->andWhere('s.estado = :estado_general')
            ->set('s.estado', Constantes::ESTADOGENERALDESHABILITADO)
            ->setParameters([
                'uidsesion' => $uidsesion,
                'estado_general' => Constantes::ESTADOGENERALHABILITADO
            ]);
        return $qb->getQuery()->execute();
    }

}
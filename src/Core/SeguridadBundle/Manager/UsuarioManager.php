<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Core\SeguridadBundle\Entity\Usuario;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Core\SeguridadBundle\Event\UsuarioEvent;
use Core\SeguridadBundle\Event\TokenEvent;

/**
* Clase de manipulación del repositorio Usuario
*/
class UsuarioManager extends BaseManager{

    /**
    * Verifica si el login se puede establecer correctamente.
    * @param string $usuario Usuario del registro Usuario 
    * @param string $clave Clave del registro Usuario   
    * @return Usuario $user si se realizó la operación con éxito  
    */
    public function login($usuario, $clave){
        $user =$this->getRepository()->validarUsuario($usuario, $clave);
        if (!$user){
            throw  new NotFoundHttpException('Usuario no encontrado!');
        }
        return $user;
    }

    /**
     * Permite interactuar con el Repositorio de Usuarios para obtener las aplicaciones a las cuales tiene acceso
     * @param integer $usuario_id Identificador del Usuario
     * @param integer $app_id Id de la Aplicación en la cual esta autenticado
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\Aplicacion"
     */
    public function getAplicaciones($usuario_id, $app_id=0, $usuarioPerfil=0) {
       return $this->getRepository()->getAplicaciones($usuario_id, $app_id, $usuarioPerfil);

    }
    
    /**
     * Permite interactuar con el Repositorio de Usuarios para obtener el ID del usuario a través del username
     * @param string $username 
     * @return Array Lista de Objeto de tipo "Core\SeguridadBundle\Entity\Usuario"
     */
    public function getUsuarioByUsername($username) {        
       return $this->getRepository()->getUsuarioByUsername($username);

    }

    /**
     * Permite resetear la clave de un usuario y enviar una confirmación a su correo
     * @param Model $usuario
     * @return boolean
     */
    public function resetPassword($usuario){
        $response = false;
        $this->beginTransaction();
        try{
            $tokenManager = $this->getContainer()->get('core.seguridad.manager.token');
            $token = $tokenManager->generateToken($usuario);
            /*********************************/
            $usuario->setCambiarclave(1);
            $this->save($usuario);
            /*********************************/
            if($token){
                $this->getDispatcher()->dispatch('core.seguridad.event.token.sendmailresetpassword', new TokenEvent($token));
            }
            $this->commit();
            $response = true;
        }catch (\Exception $ex){
            $this->rollback();
            throw $ex;
        }
        return $response;
    }
    
    /**
     * Permite actualizar la contraseña del usuario y enviar correo indicando ésta acción.
     * @param Model $usuario
     * @param string $clave
     * @return boolean
     */
    public function changePassword($usuario, $clave){
        $response = false;
        $this->beginTransaction();
        try{
            $encoder = $this->getContainer()->get('security.password_encoder');
            $encoded = $encoder->encodePassword($usuario, $clave);
            $usuario->setClave($encoded);
            $usuario->setCambiarclave(0);
            $this->save($usuario);
            if($usuario){
                $this->getDispatcher()->dispatch('core.seguridad.event.usuario.sendmailchangepassword', new UsuarioEvent($usuario));
            }
            $this->commit();
            $response = true;
        }catch (\Exception $ex){
            $this->rollback();
            throw $ex;
        }
        return $response;
    }

    /**
     * Permite actualizar la última fecha de acceso del Usuario
     * @param Usuario $usuario
     * @return boolean
     */
    public function updateFechaAcceso($usuario){
        $usuario->setFechaultacceso(new \DateTime());
        $this->save($usuario);
        return true;
    }

}
<?php
/**
 * Clase de manipulación del repositorio Aplicacion
 * Esta clase ha sido autogenerada por la aplicacion AppGen-v.2.0
 * @author Luis Malquin
 */
namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Core\AppBundle\Entity\Combobox;

class AplicacionManager extends BaseManager{

    /**
     * Obtiene la lista de aplicaciones para el combobox
     * @return Array Lista de objetos de tipo "Core\AppBundle\Entity\Combobox"
     */
    public function getAplicacionesCombobox($estado=""){
        $results=$this->getRepository()->getAplicaciones($estado);
        $arrayResult=array();
        foreach ($results as $obj){
            $row=new Combobox();
            $row->setId($obj->getId());
            $row->setDescripcion($obj->getNombre());
            $arrayResult[]=$row;
        }
        return $arrayResult;
    }

}

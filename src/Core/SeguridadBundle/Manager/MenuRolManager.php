<?php
/**
 * Clase de manipulación del repositorio Menu rol
 * @author Luis Malquin
 */
namespace Core\SeguridadBundle\Manager;

use Core\AppBundle\Manager\BaseManager;

class MenuRolManager extends BaseManager{

    /**
     * Devuelve el Objeto MenuRol solicitado
     * @param integer $menurol_id Id delMenuRol
     * @return \Core\SeguridadBundle\Entity\MenuRol
     */
    public function obtenerMenuRol($menurol_id) {
        $security = $this->getContainer()->get('core.seguridad.service.security_token');
        $usuarioperfil_id = $security->getUsuarioPerfilId();
        return $this->getRepository()->obtenerMenuRol($menurol_id, $usuarioperfil_id);
    }

    /**
     * Devuelve el Objeto MenuRol solicitado
     * @return \Core\SeguridadBundle\Entity\MenuRol
     */
    public function verificarPermisos(){
        $security = $this->getContainer()->get('core.seguridad.service.security_token');
        $menurol = $security->getAttribute('menurol');
        if($menurol) {
            $urlRequest = $security->getAttribute('PathUrl');
            $urlBase = $menurol->getMenu()->getUrl();
            return ($urlBase == $urlRequest)?$menurol:null;
        }
        return null;
    }

}
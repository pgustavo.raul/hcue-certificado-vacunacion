<?php

namespace Core\SeguridadBundle\Services;

use Core\AppBundle\Services\Mailer as AppMailer;
use Symfony\Bridge\Twig\TwigEngine;
use Core\SeguridadBundle\Entity\Token as EntityToken;
use Core\SeguridadBundle\Entity\Usuario as EntityUsuario;
use Core\AppBundle\Utils\UtilDateTime;

class Mailer {

    protected $appmailer;
    protected $engine;

    public function __construct(AppMailer $appMailer,  TwigEngine $engine) {
        $this->appmailer=$appMailer;
        $this->engine=$engine;
    }

    /**
     * Envía el email al usuario acerca de la información de la cuenta de acceso
     * @param Token $token
     */    
    public function sendRestPassword(EntityToken $token){
        $tiempo = UtilDateTime::convertirSegundosHoras($token->getTiemposeg(), false);
        $usuario = $token->getUsuario();

        $to = array("{$usuario->getCorreo()}" => "{$usuario->getPersona()->getNombrecompleto()}");
        $body=  $this->engine->render('CoreSeguridadBundle:Mailers:mailer_resetpassword.html.twig',array(
            'token'=>$token->getToken(),
            'tiempo'=>$tiempo,
            'nombres'=> $usuario->getPersona()->getNombrecompleto()
        ));
        $this->appmailer->setMessage($to, $body,'Clave Temporal');
        return $this->appmailer->send();
    }
    
    /**
     * Envía el email al usuario indicando el cambio de clave
     * @param Usuario $usuario
     */    
    public function sendChangePassword(EntityUsuario $usuario){

        $to = array("{$usuario->getCorreo()}" => "{$usuario->getPersona()->getNombrecompleto()}");
        $body=  $this->engine->render('CoreSeguridadBundle:Mailers:mailer_changepassword.html.twig',array(
            'nombres'=> $usuario->getPersona()->getNombrecompleto()
        ));
        $this->appmailer->setMessage($to, $body,'Clave Actualizada');
        return $this->appmailer->send();
    }

    /**
     * Envía el email al usuario indicando que su usuario ha sido bloqueado
     * @param Usuario $usuario
     */
    public function sendBlockUsuario(EntityUsuario $usuario){

        $to = array("{$usuario->getCorreo()}" => "{$usuario->getPersona()->getNombrecompleto()}");
        $body=  $this->engine->render('CoreSeguridadBundle:Mailers:mailer_blockusuario.html.twig',array(
            'nombres'=> $usuario->getPersona()->getNombrecompleto()
        ));
        $this->appmailer->setMessage($to, $body,'Usuario Bloqueado');
        return $this->appmailer->send();
    }

}
<?php
/**
 * Clase para la generacion de tokens
 * @author luis Malquin
 */

namespace Core\SeguridadBundle\Services;

class Token{

    private function getRandomCharacters($array){
        //obtén clave aleatoria
        $keyRandom = array_rand($array, 1);
        //devolver ítem aleatorio
        return $array[$keyRandom];
    }
    
    /**
     * Codifica un caracter concatenado con el tiempo actual a md5
     * @param type $char
     * @return string md5
     */
    private function getCharactersMD5($char){
        //Codificar el carácter y el tiempo POSIX (timestamp) en md5
        $md5Car = md5($char . Time());
        //Convertir a array el md5
        $arrChar = str_split(strtoupper($md5Car));
        //obtener un ítem aleatoriamente
        return $this->getRandomCharacters($arrChar);        
    }
    
    /**     
     * @param type $long número de caracteres base de generacion del token
     * @return strin Toke
     */
    public function getToken($long=10) {
        //crear alfabeto
        $upper = "ABCDEFGHIJKMNPQRSTUVWXYZ";
        //Convertir a array
        $uppers = str_split($upper);
        //crear array de numeros 0-9
        $numbers = range(0, 9);
        //revolver arrays
        shuffle($uppers);
        shuffle($numbers);
        //Unir arrays
        $arraymerge = array_merge($uppers, $numbers);
        $newToken = "";

        for ($i = 0; $i <= $long; $i++){
            $char = $this->getRandomCharacters($arraymerge);
            $newToken .= $this->getCharactersMD5($char);
        }
        return $newToken;
    }
}
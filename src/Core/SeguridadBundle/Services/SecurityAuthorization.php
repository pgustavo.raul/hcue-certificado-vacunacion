<?php
/*
 * Esta clase se encarga del manejo de los permisos de usuario
 * @autor: Luis Malquin
 */

namespace Core\SeguridadBundle\Services;
use Symfony\Component\DependencyInjection\Container;
use Core\SeguridadBundle\Security\Authorization\Voter\AppVoter;


class SecurityAuthorization {
    
    /**     
     * @var Container 
     */
    private $container;
    
    /**     
     * @var Container
     */
    private $authorizationChecker;
    
    /**     
     * @var SecurityToken 
     */
    private $securityToken;
        
    /**     
     * @param Container $container
     */
    public function __construct(Container $container) {
        $this->container=$container;
        $this->securityToken=$this->container->get('core.seguridad.service.security_token');
        $this->authorizationChecker=$this->container->get('security.authorization_checker');
    }
    
    /**
     * Obtiene el securityToken
     * @return securityToken
     */
    public function getSecurityToken(){
        return $this->securityToken;
    }
    
        
    /**
     * Asigan el Id del menurol activo 
     * @param integer $menurol_id
     */
    public function setActiveMenuRolId($menurol_id){
        $menurolManager = $this->container->get('core.seguridad.manager.menurol');
        $baseUrl = $this->container->get('request_stack')->getMasterRequest()->getBaseUrl();
        $urlRequest = $this->container->get('request_stack')->getMasterRequest()->get('_route');

        $url = $this->container->get('router')->generate($urlRequest);               
        $url = substr_replace($url,'',0,  strlen($baseUrl));
        $attrmenurol_id = $this->securityToken->getAttribute(AppVoter::MENUROL_ID_NAME);

        if($attrmenurol_id!=$menurol_id){
            $menurol = $menurolManager->obtenerMenuRol($menurol_id);
            $this->securityToken->setAttribute(AppVoter::MENUROL_ID_NAME, $menurol_id);
            $this->securityToken->setAttribute(AppVoter::MENUROL_NAME, $menurol);
            $this->securityToken->setAttribute('PathUrl', $url);
        }

    }

    
    /**
     * Retoran el Id del menu rol activo
     * @return integer Id del Menurol activo
     */
    public function getActiveMenuRolId(){
        return $this->securityToken->getAttribute(AppVoter::MENUROL_ID_NAME);
    }
    
    /**
     * Retorna el objeto entidad de menurol
     * @return MenuRol 
     */
    public function getActiveMenurol(){
        $menurolManager = $this->container->get('core.seguridad.manager.menurol');
        return $menurolManager->find($this->getActiveMenuRolId());
    }
    
    /**
     * Retorna si el usuario tiene permiso a al menos una opcion 
     * @return boolean
     */
    public function hasPermission(){       
        return $this->authorizationChecker->isGranted(AppVoter::HAS_PERMISSION);
    }
    
    /**
     * Retorna si el usuario puede visualizar registros en el menu actual
     * @return boolean
     */
    public function canExport(){
        return $this->authorizationChecker->isGranted(AppVoter::EXPORT);
    }
    
    /**
     * Retorna si el usuario puede visualizar registros en el menu actual
     * @return boolean
     */
    public function canView(){
        return $this->authorizationChecker->isGranted(AppVoter::VIEW);
    }
    
    /**
     * Retorna si el usuario puede agregar registros en el menu actual
     * @return boolean
     */
    public function canAdd(){
        return $this->authorizationChecker->isGranted(AppVoter::ADD);
    }
    
    /**
     * Retorna si el usuario puede editar registros en el menu actual
     * @return boolean
     */
    public function canEdit(){
        return $this->authorizationChecker->isGranted(AppVoter::EDIT);
    }
    
    /**
     * Retorna si el usuario puede eliminar registros en el menu actual
     * @return boolean
     */
    public function canDelete(){
        return $this->authorizationChecker->isGranted(AppVoter::DELETE);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion de toda la institucion o a toda la informacion
     * @return boolean
     */
    public function canAccessInfoInstitucion(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_INSTITUCION);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion de la zona
     * @return boolean
     */
    public function canAccessInfoZona(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_ZONA);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion del distrito
     * @return boolean
     */
    public function canAccessInfoDistrito(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_DISTRITO);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion del circuito
     * @return boolean
     */
    public function canAccessInfoCircuito(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_CIRCUITO);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion de la provincia
     * @return boolean
     */
    public function canAccessInfoProvincia(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_PROVINCIA);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion del canton
     * @return boolean
     */
    public function canAccessInfoCanton(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_CANTON);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion de toda la parroquia 
     * @return boolean
     */
    public function canAccessInfoParroquia(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_PARROQUIA);
    }
    
    /**
     * Retorna si el usuario tiene acceso a la informacion del establecimiento  al cual pertence
     * @return boolean
     */
    public function canAccessInfoEstablecimiento(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_ESTABLECIMIENTO);
    }
    
    /**
     * Retorna si el usuario tiene acceso unicamente a la informacion que le pertenece
     * @return boolean
     */
    public function canAccessInfoUsuario(){
        return $this->authorizationChecker->isGranted(AppVoter::INFO_USUARIO);
    }
    
}
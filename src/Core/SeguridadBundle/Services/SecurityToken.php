<?php
/*
 * Esta clase se encarga del manejo de la autentificacion en la aplicacion
 * @autor: Luis Malquin
 */

namespace Core\SeguridadBundle\Services;
use Symfony\Component\DependencyInjection\Container;
use Core\SeguridadBundle\Entity\Usuario;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Core\SeguridadBundle\Entity\Auditoria;

class SecurityToken {

    /**
     * @var Container
     */
    private $container;

    /**
     * @var security.token
     */
    private $securityToken;
    /**
     * @var integer
     */
    private $app_id;

    /**
     * Constructor
     * @param Container $container
     */

    public function __construct(Container $container) {
        $this->container = $container;
        $this->securityToken = $this->container->get('security.token_storage');
        $this->app_id = $this->container->get('core.app.service.app')->getParameter('app_globals.app_id');
    }

    /**
     * Retorna el valor del atributo
     * @param string $name Nombre del atributo
     * @return value|boolean
     */
    public function getAttribute($name){
        $token = $this->securityToken->getToken();
        if($token->hasAttribute($name)){
            return $token->getAttribute($name);
        }
        return false;
    }

    /**
     * Asigna el usuario al token
     * @param \Core\SeguridadBundle\Entity\Usuario $user
     */
    public function setUser(Usuario $user){
        $token = $this->securityToken->getToken();
        $token->setUser($user);
    }

    /**
     * Retorna el Usuario asignado al token
     * @return \Core\SeguridadBundle\Entity\Usuario
     */
    public function getUser(){
        $user=null;
        if($this->app_id){
            $em = $this->container->get('doctrine.orm.entity_manager');
            $usuarioperfil_id = $this->getAttribute('usuarioperfil_id');
            $user = $this->securityToken->getToken()->getUser()->setUsuarioPerfil($em->getReference('CoreSeguridadBundle:Usuarioperfil', $usuarioperfil_id));
        }
        else{
            $user=$this->securityToken->getToken()->getUser();
        }
        return $user;
    }

    /**
     * Retorna el Usuario asignado al token
     * @return \Core\SeguridadBundle\Entity\Usuario
     */
    public function getPersona(){
        return $this->getAttribute('persona');
    }

    /**
     * Asigna el usuario al token
     * @param integer $usuarioperfil_id
     */
    public function setUsuarioPerfil($usuarioperfil_id=0){
        $usuarioPerfilManager = $this->container->get("core.seguridad.manager.usuarioperfil");
        $usuario_id = $this->securityToken->getToken()->getUser()->getId();
        if($this->app_id){
            if($usuarioperfil_id){
                $usuarioperfil = $usuarioPerfilManager->getUsuarioperfil($usuarioperfil_id);
            }else{
                $usuarioperfil = $usuarioPerfilManager->firstUsuarioPerfil($usuario_id);
            }
            $this->setAttribute('persona', $usuarioperfil->getUsuario()->getPersona());
            $this->setAttribute('perfilentidad', $usuarioperfil->getPerfilentidad());
            $this->setAttribute('usuarioperfil_id', $usuarioperfil->getId());
        }
        else {
            $usuarioperfil = $usuarioPerfilManager->findOneBy(['usuario_id' => $usuario_id, 'estado' => 1, 'activo' => 1], ['id' => 'ASC']);
            $this->setAttribute('usuarioperfil_id', $usuarioperfil->getId());
        }
    }

    /**
     * Retorna el array de Objeto UsuarioPerfil Activo en la Sesión del Usuario
     * @return \Core\SeguridadBundle\Entity\Usuarioperfil
     */
    public function getUsuarioPerfilId(){
        $usuario = $this->getUser();
        if($usuario){
            return $this->getAttribute('usuarioperfil_id');
        }
        return false;
    }

    /**
     * Retorna el array de Objeto Perfilentidad Activo en la Sesión del Usuario
     * @return \Core\SeguridadBundle\Entity\Perfilentidad
     */
    public function getPerfilEntidad(){
        return $this->getAttribute('perfilentidad');
    }

    /**
     * Retorna el array de Objeto Perfil Activo en la Sesión del Usuario
     * @return \Core\SeguridadBundle\Entity\Perfil
     */
    public function getPerfil(){
        return $this->getPerfilEntidad()->getPerfil();
    }

    /**
     * Retorna el array de Objeto Entidad Activo en la Sesión del Usuario
     * @return \Core\AppBundle\Entity\Entidad
     */
    public function getEntidad(){
        return $this->getPerfilEntidad()->getEntidad();
    }

    /**
     * Retorna la Ubicación Geografica en la cual se encuentra el Usuario
     * @return
     */
    public function getUbicacionGeografica(){
        $result = array();
        $entidad = $this->getEntidad();
        $circuito = ($entidad)?$entidad->getCircuito():null;
        $distrito = ($circuito)?$circuito->getDistrito():null;
        $zona = ($distrito)?$distrito->getZona():null;
        //Provincia-Canton-Parroquia
        $parroquia = ($entidad)?$entidad->getParroquia():null;
        $canton = ($parroquia)?$parroquia->getCanton():null;
        $provincia = ($canton)?$canton->getProvincia():null;
        $result['entidad']=$entidad;
        $result['circuito']=$circuito;
        $result['distrito']=$distrito;
        $result['zona']=$zona;
        $result['provincia']=$provincia;
        $result['canton']=$canton;
        $result['parroquia']=$parroquia;
        return $result;
    }

    /**
     * Asigna un atributo con valor al token
     * @param string $name
     * @param value $value
     */
    public function setAttribute($name,$value){
        $token = $this->securityToken->getToken();
        $token->setAttribute($name,$value);
    }

    /**
     * Genera la Autenticacion del Usuario
     * @param Usuario $user
     */
    public function setAutentication(Usuario $user){
        $session = $this->container->get('session');
        $token = new UsernamePasswordToken($user, $user->getPassword(), "secured_area", $user->getRoles());
        $session->set('_security_secured_area', serialize($token));
        $session->save();

        $this->securityToken->setToken($token);
        $this->setUsuarioPerfil();

        // now dispatch the login event
        $request = $this->container->get("request_stack")->getCurrentRequest();
        $event = new InteractiveLoginEvent($request, $token);
        $this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        // Verificamos si se ecnuentra habilitada la auditoria y guardamos el Inicio de Sesión

        $audit = $this->container->getParameter('core_seguridad.audit.enabled');

        if($audit){
            $em = $this->container->get('doctrine.orm.entity_manager');
            $aplicacion = $this->container->get('core.app.service.app')->getAplicacion();
            $auditoria = new Auditoria();
            $auditoria->setAccion('login');
            $auditoria->setUsuarioperfil($em->getReference('CoreSeguridadBundle:Usuarioperfil', $this->getUsuarioPerfilId()));
            $auditoria->setDescripcion("Inicio de Sesión - $aplicacion");
            $em->persist($auditoria);
            $em->flush();
        }
        // Actualizamos la Fecha de Acceso del Usuario
        $this->container->get('core.seguridad.manager.usuario')->updateFechaAcceso($this->getUser());
    }


    /**
     * Elimina la autenticación del usuario
     * @param Usuario $user
     */
    public function destroyAutentication(Usuario $user=null){
        // Guardamos el Cierre de Sesión en Auditoría
        if($user && $this->container->getParameter('core_seguridad.audit.enabled')){
            $aplicacion = $this->container->get('core.app.service.app')->getAplicacion();
            $em = $this->container->get('doctrine.orm.entity_manager');
            $auditoria = new Auditoria();
            $auditoria->setAccion('logout');
            $auditoria->setUsuarioperfil($em->getReference('CoreSeguridadBundle:Usuarioperfil', $this->getUsuarioPerfilId()));
            $auditoria->setDescripcion("Cierre de Sesión - $aplicacion");
            $em->persist($auditoria);
            $em->flush();
        }      

        /*
         * Limpiarmos el Token
         * Cancelamos la Sesión
         * Redireccionamos al Login
         */
        $this->container->get('security.token_storage')->setToken(null);
        $this->container->get('request_stack')->getCurrentRequest()->getSession()->invalidate();
    }
}
<?php 

namespace Core\SeguridadBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Core\SeguridadBundle\Entity\Usuario;

class UsuarioEvent extends Event{
     protected $usuario;
     
     public function __construct(Usuario $usuario){
     	$this->usuario=$usuario;
     }

     public function getUsuario(){
     	return $this->usuario;
     }
}
<?php 

namespace Core\SeguridadBundle\Event;

use symfony\Component\EventDispatcher\Event;
use Core\SeguridadBundle\Entity\Token;

class TokenEvent extends Event{
     protected $token;
     
     public function __construct(Token $token){
     	$this->token=$token;
     }

     public function getToken(){
     	return $this->token;
     }
}
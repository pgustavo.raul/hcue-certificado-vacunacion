<?php

/**
 * Description de Suscripcion de Request
 * @author Luis Mendoza
 */

namespace Core\SeguridadBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\HttpFoundation\Response;

class RequestSubscriber implements EventSubscriberInterface
{

    /**
     * @var authorization
     */
    protected $authorization;

    /**
     * @var securityToken
     */
    protected $securityToken;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var maxIdleTime
     */
    protected $maxIdleTime;

    public function __construct(Container $container)
    {
        $this->container= $container;
        $this->authorization= $this->container->get('security.authorization_checker');
        $this->securityToken= $this->container->get('security.token_storage');
        $this->maxIdleTime = $this->container->getParameter('timeout_session');
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return array(
            KernelEvents::REQUEST => array(
                array('onKernelRequest', 7)
            )
        );

    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $session = $request->getSession();
        $isFullyAuthenticated = $this->authorization->isGranted('IS_AUTHENTICATED_FULLY');
        if ($this->maxIdleTime > 0) {
            $lapse = time() - $session->getMetadataBag()->getLastUsed();
            if($isFullyAuthenticated){

                if ($lapse > $this->maxIdleTime) {
                    if($request->isXmlHttpRequest()) {
                        $event->setResponse(
                            new Response(
                                'Su sesión ha expirado, vuelva a iniciar sesión!.',
                                Response::HTTP_UNAUTHORIZED,
                                array('content-type' => 'text/plain')
                            )
                        );
                    }else{
                        $event->setResponse(new RedirectResponse(
                            $this->container->get('router')->generate('app_logout').'?timeout=1'
                        ));
                    }
                }
            }

        }

    }
}
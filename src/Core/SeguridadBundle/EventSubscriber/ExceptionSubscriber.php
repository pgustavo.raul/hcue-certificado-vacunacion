<?php

/**
 * Description de Suscripcion de excepciones
 * @author Luis Malquin
 */

namespace Core\SeguridadBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;


class ExceptionSubscriber implements EventSubscriberInterface {

     /**
     * @var Container 
     */
    protected $container;
    
    public function __construct(Container $container){

        $this->container = $container;

    }

    public static function getSubscribedEvents() {

        // return the subscribed events, their methods and priorities
        return array(
            KernelEvents::EXCEPTION => array(
                array('onKernelRequest', 7)
            )
        );

    }

    public function onKernelRequest(GetResponseForExceptionEvent $event) {

        // a listener might have replaced the exception
        $ex = $event->getException();
        $engine = $this->container->get('templating');
        $code = '';
        if ( method_exists($ex, 'getStatusCode') ) {
            $code = $ex->getStatusCode();
        }


        if($code == Response::HTTP_NOT_FOUND || $code == Response::HTTP_METHOD_NOT_ALLOWED) {

           
            $content = $engine->render("CoreGUIBundle::error-$code.html.twig");
            $event->setResponse(new Response($content, $code));
            $event->stopPropagation();

        }

        if($code == Response::HTTP_INTERNAL_SERVER_ERROR) {
            $content = $engine->render("CoreGUIBundle::error-$code.html.twig");
            $event->setResponse(new Response($content, $code));
            $event->stopPropagation();
        }



    }

}
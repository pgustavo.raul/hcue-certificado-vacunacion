<?php

/**
 * Description de Suscripcion de auditoria
 * @author Luis Malquin
 */

namespace Core\SeguridadBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\DependencyInjection\Container;
use Core\SeguridadBundle\Entity\Usuario;
use Core\SeguridadBundle\Entity\Auditoria;

class AuditSubscriber implements EventSubscriber{

    const CREATE='creación';
    const UPDATE='actualización';
    const DELETE='eliminación';

    /**
     * @var $securityToken
     */
    protected $securityToken;

    /**
     * @var $securityAuthorization
     */
    protected $securityAuthorization;

    /**
     * @var Container
     */
    protected $container;



    public function __construct(Container $container){
        $this->container= $container;
        $this->securityAuthorization = $this->container->get('core.seguridad.service.security_authorization');
        $this->securityToken= $this->container->get('core.seguridad.service.security_token');
    }

    public function getSubscribedEvents(){
        $audit=$this->container->getParameter('core_seguridad.audit.enabled');
        if($audit)
        {
           return [Events::prePersist,Events::preUpdate,Events::onFlush];
        }
        return [Events::prePersist,Events::preUpdate];
    }

    /**
     * Se ejecuta antes de realizar el persist de cada entidad o modelo
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args){
        $entity=$args->getEntity();
        $security=$this->container->get('security.token_storage');
        $user = $security->getToken()->getUser();

        if(method_exists($entity, 'setUsuariocreacionId') && ($user instanceof Usuario))
        {

                $idUser = $user->getId();
                $entity->setUsuariocreacionId($idUser);

        }
        if(method_exists($entity, 'setUsuariocreacion') && ($user instanceof Usuario))
        {
                $entity->setUsuariocreacion($user);
        }
        if ($this->isAuditable($entity))
        {
            //Seteamos los ids relacionales
            $this->setIdsRelations($args->getEntityManager(), $entity);
        }

    }

    /**
     * Se ejecuta antes de realizar el update de cada entidad o modelo
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args){
        $entity=$args->getEntity();
        $security=$this->container->get('security.token_storage');
        $user = $security->getToken()->getUser();

        if(method_exists($entity, 'setUsuariomodificacionId') && ($user instanceof Usuario))
        {

                $idUser = $user->getId();
                $entity->setUsuariomodificacionId($idUser);

        }
        if(method_exists($entity, 'setUsuariomodificacion') && ($user instanceof Usuario))
        {
            $entity->setUsuariomodificacion($user);
        }
        if ($this->isAuditable($entity))
        {
            //Seteamos los ids relacionales
            $this->setIdsRelations($args->getEntityManager(), $entity);
        }
    }

    /**
     * Verifica si la auditoria se encuentra habilitada y un objeto es auditable
     * @param object $entity
     * @return boolean
     */
    private function isAuditable($entity){       
        $audit = $this->container->getParameter('core_seguridad.audit.enabled');
        if($audit && !($entity instanceof Auditoria))
        {
            $unaudited_entities = $this->container->getParameter('core_seguridad.audit.unaudited_entities');
            $entityName = ClassUtils::getClass($entity);
            return !in_array($entityName, $unaudited_entities);        
        }

        return false;
    }

    /**
     * Asignamos los id a los campos de relacion de la entidad
     * @param EntityManager $em
     * @param type $entity
     */
    private function setIdsRelations(EntityManager $em,$entity){
        $metadata=$em->getClassMetadata(get_class($entity));

        $assocnames=$metadata->getAssociationNames();

        if(count($assocnames)){
            foreach ($assocnames as $fieldName)
            {
                if($metadata->hasAssociation($fieldName) && $metadata->isSingleValuedAssociation($fieldName))
                {
                    $colName = $metadata->getSingleAssociationJoinColumnName($fieldName);
                    if($metadata->hasField($colName))
                    {
                        $valueField=$metadata->getFieldValue($entity, $colName);
                        if(!$valueField)
                        {
                            $valueAssoc=$metadata->getFieldValue($entity, $fieldName);
                            if(method_exists($valueAssoc, 'getId'))
                            {
                                $metadata->setFieldValue($entity,$colName, $valueAssoc->getId());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Este evento se ejecuta al realizar el flush en el entity manager
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {

        $em=$args->getEntityManager();
        $uow=$em->getUnitOfWork();

        //Obtenemos las entidades que estan siendo insertadas
        foreach ($uow->getScheduledEntityInsertions() as $entity)
        {

            if ($this->isAuditable($entity))
            {
                $this->insert($em, $entity, $uow->getEntityChangeSet($entity));
            }
        }

        //Obtenemos las entidades que estan siendo actualizadas
        foreach ($uow->getScheduledEntityUpdates() as $entity)
        {
            if ($this->isAuditable($entity))
            {
                $this->update($em, $entity, $uow->getEntityChangeSet($entity));
            }
        }

        //Obtenemos las entidades eliminadas
        foreach ($uow->getScheduledEntityDeletions() as $entity)
        {
            if ($this->isAuditable($entity))
            {
                $uow->initializeObject($entity);
                $this->remove($em,$entity);
            }
        }
        $uow->computeChangeSets();
    }

    /**
     * Realiza el persist de la entidad auditoria con los datos de la insercion del registro
     * @param EntityManager $em
     * @param object $entity
     * @param array $ch Cambios en el modelo
     */
    private function insert(EntityManager $em, $entity, array $ch){
        $meta = $em->getClassMetadata(get_class($entity));
        $table=$meta->getTableName();

        try {
            $usuarioperfil_id = $this->securityToken->getAttribute('usuarioperfil_id');
            $menurol_id =  $this->securityAuthorization->getActiveMenuRolId();
            $values = $this->getValues($em, $entity);

            $registro=  $this->jsonSerialize($values);
            $valuesCh=  $this->getDiffValues($em, $entity, $ch);

            $cambios= $this->jsonSerialize($valuesCh);

            $auditrepo=$em->getRepository('CoreSeguridadBundle:Auditoria');

            $audit=new Auditoria();
            $id=0;
            if(method_exists($entity, 'getId'))
            {
                $id=$entity->getId();
                $audit->setIdregistro($id);
                $audit->setVersion($auditrepo->getNextVersion($id));
            }
            $audit->setAccion(self::CREATE);
            $audit->setTabla($table);
            $audit->setDescripcion("Nuevo registro agregado en la entidad: $table");

            if($usuarioperfil_id)
            {
                $audit->setUsuarioperfil($em->getReference('CoreSeguridadBundle:Usuarioperfil', $usuarioperfil_id));
            }
            if($menurol_id)
            {
                $audit->setMenurol($em->getReference('CoreSeguridadBundle:MenuRol', $menurol_id));
            }

            $audit->setRegistro($registro);
            $audit->setCambio($cambios);
            $em->persist($audit);

        } catch (\Exception $exc) {
            $logger = $this->container->get('logger');
            $logger->error("Ha ocurrido un error guardando la auditoria de la entidad $table : ".$exc->getMessage());
        }
    }

    /**
     * Realiza el persist de la entidad auditoria con los datos de la actualizacion del registro
     * @param EntityManager $em
     * @param type $entity
     * @param array $ch Cambios en el modelo
     */
    private function update(EntityManager $em, $entity, array $ch){
        $meta = $em->getClassMetadata(get_class($entity));
        $table=$meta->getTableName();

        try {
            $usuarioperfil_id = $this->securityToken->getAttribute('usuarioperfil_id');
            $menurol_id =  $this->securityAuthorization->getActiveMenuRolId();

            $values=$this->getValues($em, $entity);
            $registro=  $this->jsonSerialize($values);
            $valuesCh=  $this->getDiffValues($em, $entity, $ch);
            $cambios= $this->jsonSerialize($valuesCh);

            $auditrepo=$em->getRepository('CoreSeguridadBundle:Auditoria');

            $audit=new Auditoria();
            $id=0;
            if(method_exists($entity, 'getId'))
            {
                $id=$entity->getId();
                $audit->setIdregistro($id);
                $audit->setVersion($auditrepo->getNextVersion($id));
            }

            if(method_exists($entity, 'getActivo') && !$entity->getActivo())
            {
                $audit->setAccion(self::DELETE);
                $audit->setDescripcion("Eliminación lógica del registro de Id: $id de la entidad: $table");
            }else{
                $audit->setAccion(self::UPDATE);
                $audit->setDescripcion("Actualización del registro de Id: $id en la entidad: $table");
            }

            $audit->setTabla($table);
            if($usuarioperfil_id)
            {
                $audit->setUsuarioperfil($em->getReference('CoreSeguridadBundle:Usuarioperfil', $usuarioperfil_id));
            }
            if($menurol_id)
            {
                $audit->setMenurol($em->getReference('CoreSeguridadBundle:MenuRol', $menurol_id));
            }
            $audit->setRegistro($registro);
            $audit->setCambio($cambios);
            $em->persist($audit);


        } catch (\Exception $exc) {
            $logger = $this->container->get('logger');
            $logger->error("Ha ocurrido un error guardando la auditoria de la entidad $table : ".$exc->getMessage());
        }
    }

    /**
     * Realiza el persist de la entidad auditoria con los datos de la eliminacion del registro
     * @param EntityManager $em
     * @param type $entity
     */
    private function remove(EntityManager $em, $entity){
        $meta = $em->getClassMetadata(get_class($entity));
        $table=$meta->getTableName();

        try {
            $usuarioperfil_id = $this->securityToken->getAttribute('usuarioperfil_id');
            $menurol_id =  $this->securityAuthorization->getActiveMenuRolId();

            $values = $this->getValues($em, $entity);
            $registro = $this->jsonSerialize($values);
            $auditrepo = $em->getRepository('CoreSeguridadBundle:Auditoria');

            $audit = new Auditoria();
            $id = 0;
            if(method_exists($entity, 'getId'))
            {
                $id=$entity->getId();
                $audit->setIdregistro($id);
                $audit->setVersion($auditrepo->getNextVersion($id));
            }
            $audit->setAccion(self::DELETE);
            $audit->setTabla($table);
            $audit->setDescripcion("Eliminación del registro de Id: $id de la entidad: $table");
            $audit->setUsuarioperfil($em->getReference('CoreSeguridadBundle:Usuarioperfil', $usuarioperfil_id));
            $audit->setMenurol($em->getReference('CoreSeguridadBundle:MenuRol', $menurol_id));
            $audit->setRegistro($registro);
            $em->persist($audit);

        } catch (\Exception $exc) {
            $logger = $this->container->get('logger');
            $logger->error("Ha ocurrido un error guardando la auditoria de la entidad $table : ".$exc->getMessage());
        }
    }

    /**
     * * Serializa un objeto u array a json
     * @param EntityManager $em
     * @param type $entity
     * @param type $data
     * @return type
     */
    private function jsonSerialize($data){
        $encoders = array('json'=>new JsonEncoder());
        $objNormalizer=new ObjectNormalizer();
        $normalizers = array($objNormalizer);
        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($data,'json');
    }

    /**
     * Devuelve los valores del registro
     * @param EntityManager $em
     * @param type $entity
     * @return type
     */
    private function getValues(EntityManager $em,$entity){
        $meta = $em->getClassMetadata(get_class($entity));

        $fieldNames=$meta->getFieldNames();

        $values=[];
        foreach ($fieldNames as $fieldName)
        {
            if ($meta->hasField($fieldName))
            {
                $mapping = $meta->getFieldMapping($fieldName);
                $columName=$mapping['columnName'];
                $value=$meta->getFieldValue($entity, $fieldName);
                $values[$columName]=  $this->getValue($em, Type::getType($mapping['type']), $value);
            }
        }

        return $values;
    }

    /**
     * Develueve las diferencias de cambio en los campos
     * @param EntityManager $em
     * @param type $entity
     * @param array $ch
     * @return array
     */
    private function getDiffValues(EntityManager $em, $entity, array $ch)
    {

        $meta = $em->getClassMetadata(get_class($entity));
        $diffValues = [];
        foreach ($ch as $fieldName=>$field)
        {
            $old=$field[0];
            $new=$field[1];
            if ($meta->hasField($fieldName))
            {
                $mapping = $meta->getFieldMapping($fieldName);
                $valueOld=$this->getValue($em, Type::getType($mapping['type']), $old);
                $valueNew=$this->getValue($em, Type::getType($mapping['type']), $new);
                $diffValues[$fieldName]=array($valueOld,$valueNew);
            }
        }

        return $diffValues;
    }

    /**
     * Formatea el valor del campo
     * @param EntityManager $em
     * @param Type $type
     * @param type $value
     * @return type value
     */
    private function getValue(EntityManager $em, Type $type, $value)
    {
        $platform = $em->getConnection()->getDatabasePlatform();
        $result=null;

        if($type->getName()==Type::BOOLEAN)
        {
            $result=$type->convertToPHPValue($value, $platform);
        }else{
            $result=$type->convertToDatabaseValue($value, $platform);
        }

        return $result;
    }

}

<?php

namespace Core\SeguridadBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class CoreSeguridadExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
         if ($config['audit']['enabled']) {
             $container->setParameter('core_seguridad.audit.enabled', true);
             $container->setParameter('core_seguridad.audit.unaudited_entities', $config['audit']['unaudited_entities']);
         }else{
             $container->setParameter('core_seguridad.audit.enabled', false);
         }                       
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}

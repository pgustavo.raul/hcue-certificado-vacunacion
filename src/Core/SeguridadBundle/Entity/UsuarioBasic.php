<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_sistema.usuario")
 * @ORM\Entity()
 */
class UsuarioBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\PersonaBasic")
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     * */
    private $persona;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Get persona
     * @return \Core\AppBundle\Entity\PersonaBasic
     * */
    public function getPersona() {
        return $this->persona;
    }

}
<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.aplicacion")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\AplicacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Aplicacion{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.aplicacion_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=50, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo nombre")
     **/
    private $nombre;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=100, nullable=true)
     **/
    private $descripcion;

    /**
     * @var integer $orden
     * @ORM\Column(name="orden", type="integer", nullable=true)
     **/
    private $orden;

    /**
     * @var boolean $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var string $icono
     * @ORM\Column(name="icono", type="string",length=100, nullable=true)
     **/
    private $icono;

    /**
     * @var string $url
     * @ORM\Column(name="url", type="string",length=150, nullable=true)
     **/
    private $url;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return aplicacion
     **/
    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     **/
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return aplicacion
     **/
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Set orden
     * @param integer orden
     * @return aplicacion
     **/
    public function setOrden($orden){
        $this->orden = $orden;
        return $this;
    }

    /**
     * Get orden
     * @return integer
     **/
    public function getOrden(){
        return $this->orden;
    }

    /**
     * Set estado
     * @param integer estado
     * @return aplicacion
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set icono
     * @param string icono
     * @return aplicacion
     **/
    public function setIcono($icono){
        $this->icono = $icono;
        return $this;
    }

    /**
     * Get icono
     * @return string
     **/
    public function getIcono(){
        return $this->icono;
    }

    /**
     * Set url
     * @param string url
     * @return aplicacion
     **/
    public function setUrl($url){
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     * @return string
     **/
    public function getUrl(){
        return $this->url;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return aplicacion
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return aplicacion
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return aplicacion
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
    }
    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

    public function __toString() {
        return $this->nombre;
    }

}
<?php

namespace Core\SeguridadBundle\Entity;

/*
 * Clase para la definición de constantes del Aplicativo de Seguridad
 * @autor Luis Malquin
 */

use Core\AppBundle\Entity\Constantes as AppConstantes;

class Constantes extends AppConstantes {

    /***********************************
    * Constantes de tipo de catalogo
    * TC Abreviatura de Tipo Catalogo
    ************************************/

    /**
     * @const integer Id del tipo de catalogo para los estado de usuario
     */
    const TC_ESTADOSUSUARIO = 17;

    /**
     * @const integer Id del tipo de catalogo para filtros de informacion institucional (permisos)
     */
    const TC_PERMISOSFILTROINFORMACION = 20;

    /***********************************
    * Constantes de detalle catalogos
    * CT Abreviatura de catalogos
    ************************************/

    /**
     * @const integer Id del registro catalogo de estado usuario activo
     */
    const CT_ESTADOUSUARIOACTIVO = 181;

    /**
     * @const integer Id del registro catalogo de estado usuario inactivo
     */
    const CT_ESTADOUSUARIOINACTIVO = 182;

    /**
     * @const integer Id del registro catalogo de estado usuario bloqueado
     */
    const CT_ESTADOUSUARIOBLOQUEADO = 183;

    /***********************************************
     * Constantes de permisos información usuario
     ***********************************************/

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion de la institucion
     */
    const CT_INFO_INSTITUCION =207;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion de zona
     */
    const CT_INFO_ZONA = 208;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion del distrito
     */
    const CT_INFO_DISTRITO = 209;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion del circuito
     */
    const CT_INFO_CIRCUITO = 210;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion de la provincia
     */
    const CT_INFO_PROVINCIA = 211;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion del canton
     */
    const CT_INFO_CANTON = 212;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion de la parroquia
     */
    const CT_INFO_PARROQUIA = 213;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion del establecimiento
     */
    const CT_INFO_ESTABLECIMIENTO= 214;

    /**
     * @const integer Id del catalogo detalle de permiso de accesso a la informacion del usuario
     */
    const CT_INFO_USUARIO= 215;

}

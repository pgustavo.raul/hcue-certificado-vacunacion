<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.perfilrol")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\PerfilrolRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Perfilrol{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.perfilrol_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $perfil_id
     * @ORM\Column(name="perfil_id", type="integer", nullable=false)
     **/
    private $perfil_id;

    /**
     * @ORM\ManyToOne(targetEntity="Perfil")
     * @ORM\JoinColumn(name="perfil_id", referencedColumnName="id")
     **/
    private $perfil;

    /**
     * @var integer $rol_id
     * @ORM\Column(name="rol_id", type="integer", nullable=false)
     **/
    private $rol_id;

    /**
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     **/
    private $rol;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set perfil_id
     * @param integer perfil_id
     * @return perfilrol
     **/
    public function setPerfilId($perfil_id){
        $this->perfil_id = $perfil_id;
        return $this;
    }

    /**
     * Get perfil_id
     * @return integer
     **/
    public function getPerfilId(){
        return $this->perfil_id;
    }

    /**
     * Set perfil
     * @param \Core\SeguridadBundle\Entity\Perfil $perfil
     * @return perfilrol
     **/
    public function setPerfil(\Core\SeguridadBundle\Entity\Perfil $perfil){
        $this->perfil = $perfil;
        return $this;
    }

    /**
     * Get perfil
     * @return \Core\SeguridadBundle\Entity\Perfil
     **/
    public function getPerfil(){
        return $this->perfil;
    }

    /**
     * Set rol_id
     * @param integer rol_id
     * @return perfilrol
     **/
    public function setRolId($rol_id){
        $this->rol_id = $rol_id;
        return $this;
    }

    /**
     * Get rol_id
     * @return integer
     **/
    public function getRolId(){
        return $this->rol_id;
    }

    /**
     * Set rol
     * @param \Core\SeguridadBundle\Entity\Rol $rol
     * @return perfilrol
     **/
    public function setRol(\Core\SeguridadBundle\Entity\Rol $rol){
        $this->rol = $rol;
        return $this;
    }

    /**
     * Get rol
     * @return \Core\SeguridadBundle\Entity\Rol
     **/
    public function getRol(){
        return $this->rol;
    }

    /**
     * Set estado
     * @param integer estado
     * @return perfilrol
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return perfilrol
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return perfilrol
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return perfilrol
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

}
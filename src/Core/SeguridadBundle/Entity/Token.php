<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.token")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\TokenRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Token{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.token_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $usuario_id
     * @ORM\Column(name="usuario_id", type="integer", nullable=false)
     **/
    private $usuario_id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     **/
    private $usuario;

    /**
     * @var integer $aplicacion_id
     * @ORM\Column(name="aplicacion_id", type="integer", nullable=false)
     **/
    private $aplicacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Aplicacion")
     * @ORM\JoinColumn(name="aplicacion_id", referencedColumnName="id")
     **/
    private $aplicacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fechacreacion")
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechaexpiracion
     * @ORM\Column(name="fechaexpiracion", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fechaexpiracion")
     **/
    private $fechaexpiracion;

    /**
     * @var string $token
     * @ORM\Column(name="token", type="string",length=256, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo token")
     **/
    private $token;

    /**
     * @var integer $tiemposeg
     * @ORM\Column(name="tiemposeg", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo tiemposeg")
     **/
    private $tiemposeg;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var string $uidsesion
     * @ORM\Column(name="uidsesion", type="string",length=256, nullable=true)
     **/
    private $uidsesion;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set usuario_id
     * @param integer usuario_id
     * @return token
     **/
    public function setUsuarioId($usuario_id){
        $this->usuario_id = $usuario_id;
        return $this;
    }

    /**
     * Get usuario_id
     * @return integer
     **/
    public function getUsuarioId(){
        return $this->usuario_id;
    }

    /**
     * Set usuario
     * @param \Core\SeguridadBundle\Entity\Usuario $usuario
     **/
    public function setUsuario(\Core\SeguridadBundle\Entity\Usuario $usuario){
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * Get usuario
     * @return \Core\SeguridadBundle\Entity\Usuario
     **/
    public function getUsuario(){
        return $this->usuario;
    }

    /**
     * Set aplicacion_id
     * @param integer aplicacion_id
     * @return token
     **/
    public function setAplicacionId($aplicacion_id){
        $this->aplicacion_id = $aplicacion_id;
        return $this;
    }

    /**
     * Get aplicacion_id
     * @return integer
     **/
    public function getAplicacionId(){
        return $this->aplicacion_id;
    }

    /**
     * Set aplicacion
     * @param \Core\SeguridadBundle\Entity\Aplicacion $aplicacion
     * @return token
     **/
    public function setAplicacion(Aplicacion $aplicacion){
        $this->aplicacion = $aplicacion;
        return $this;
    }

    /**
     * Get aplicacion
     * @return \Core\SeguridadBundle\Entity\Aplicacion
     **/
    public function getAplicacion(){
        return $this->aplicacion;
    }

    /**
     * Set fechacreacion
     * @param datetime fechacreacion
     * @return token
     **/
    public function setFechacreacion($fechacreacion){
        $this->fechacreacion = $fechacreacion;
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Set fechaexpiracion
     * @param datetime fechaexpiracion
     * @return token
     **/
    public function setFechaexpiracion($fechaexpiracion){
        $this->fechaexpiracion = $fechaexpiracion;
        return $this;
    }

    /**
     * Get fechaexpiracion
     * @return datetime
     **/
    public function getFechaexpiracion(){
        return $this->fechaexpiracion;
    }

    /**
     * Set token
     * @param string token
     * @return token
     **/
    public function setToken($token){
        $this->token = $token;
        return $this;
    }

    /**
     * Get token
     * @return string
     **/
    public function getToken(){
        return $this->token;
    }

    /**
     * Set tiemposeg
     * @param integer tiemposeg
     * @return token
     **/
    public function setTiemposeg($tiemposeg){
        $this->tiemposeg = $tiemposeg;
        return $this;
    }

    /**
     * Get tiemposeg
     * @return integer
     **/
    public function getTiemposeg(){
        return $this->tiemposeg;
    }

    /**
     * Set estado
     * @param integer estado
     * @return token
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set uidsesion
     * @param string $uidsesion
     * @return token
     **/
    public function setUidsesion($uidsesion){
        $this->uidsesion = $uidsesion;
        return $this;
    }

    /**
     * Get uidsesion
     * @return string
     **/
    public function getUidsesion(){
        return $this->uidsesion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->estado=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.sesion")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\SesionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Sesion{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.sesion_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $aplicacion_id
     * @ORM\Column(name="aplicacion_id", type="integer", nullable=true)
     **/
    private $aplicacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Aplicacion")
     * @ORM\JoinColumn(name="aplicacion_id", referencedColumnName="id")
     **/
    private $aplicacion;

    /**
     * @var integer $usuario_id
     * @ORM\Column(name="usuario_id", type="integer", nullable=false)
     **/
    private $usuario_id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     **/
    private $usuario;

    /**
     * @var string $host
     * @ORM\Column(name="host", type="string",length=200, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo host")
     **/
    private $host;

    /**
     * @var string $navegador
     * @ORM\Column(name="navegador", type="string",length=256, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo navegador")
     **/
    private $navegador;

    /**
     * @var string $ip
     * @ORM\Column(name="ip", type="string",length=25, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo ip")
     **/
    private $ip;

    /**
     * @var string $uidsesion
     * @ORM\Column(name="uidsesion", type="string",length=256, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo uidsesion")
     **/
    private $uidsesion;

    /**
     * @var datetime $fechalogin
     * @ORM\Column(name="fechalogin", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fechalogin")
     **/
    private $fechalogin;

    /**
     * @var datetime $fechalogout
     * @ORM\Column(name="fechalogout", type="datetime", nullable=true)
     **/
    private $fechalogout;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set aplicacion_id
     * @param integer $aplicacion_id
     * @return sesion
     **/
    public function setAplicacionId($aplicacion_id){
        $this->aplicacion_id = $aplicacion_id;
        return $this;
    }

    /**
     * Get aplicacion_id
     * @return integer
     **/
    public function getAplicacionId(){
        return $this->aplicacion_id;
    }

    /**
     * Set aplicacion
     * @param \Core\SeguridadBundle\Entity\Aplicacion $aplicacion
     * @return sesion
     **/
    public function setAplicacion(Aplicacion $aplicacion){
        $this->aplicacion = $aplicacion;
        return $this;
    }

    /**
     * Get aplicacion
     * @return \Core\SeguridadBundle\Entity\Aplicacion
     **/
    public function getAplicacion(){
        return $this->aplicacion;
    }

    /**
     * Set usuario_id
     * @param integer $usuario_id
     * @return sesion
     **/
    public function setUsuarioId($usuario_id){
        $this->usuario_id = $usuario_id;
        return $this;
    }

    /**
     * Get usuario_id
     * @return integer
     **/
    public function getUsuarioId(){
        return $this->usuario_id;
    }

    /**
     * Set usuario
     * @param \Core\SeguridadBundle\Entity\Usuario $usuario
     * @return sesion
     **/
    public function setUsuario(Usuario $usuario){
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * Get usuario
     * @return \Core\SeguridadBundle\Entity\Usuario
     **/
    public function getUsuario(){
        return $this->usuario;
    }

    /**
     * Set host
     * @param string $host
     * @return sesion
     **/
    public function setHost($host){
        $this->host = $host;
        return $this;
    }

    /**
     * Get host
     * @return string
     **/
    public function getHost(){
        return $this->host;
    }

    /**
     * Set navegador
     * @param string $navegador
     * @return sesion
     **/
    public function setNavegador($navegador){
        $this->navegador = $navegador;
        return $this;
    }

    /**
     * Get navegador
     * @return string
     **/
    public function getNavegador(){
        return $this->navegador;
    }

    /**
     * Set ip
     * @param string $ip
     * @return sesion
     **/
    public function setIp($ip){
        $this->ip = $ip;
        return $this;
    }

    /**
     * Get ip
     * @return string
     **/
    public function getIp(){
        return $this->ip;
    }

    /**
     * Set uidsesion
     * @param string $uidsesion
     * @return sesion
     **/
    public function setUidsesion($uidsesion){
        $this->uidsesion = $uidsesion;
        return $this;
    }

    /**
     * Get uidsesion
     * @return string
     **/
    public function getUidsesion(){
        return $this->uidsesion;
    }

    /**
     * Set fechalogin
     * @param datetime $fechalogin
     * @return sesion
     **/
    public function setFechalogin($fechalogin){
        $this->fechalogin = $fechalogin;
        return $this;
    }

    /**
     * Get fechalogin
     * @return datetime
     **/
    public function getFechalogin(){
        return $this->fechalogin;
    }

    /**
     * Set fechalogout
     * @param datetime $fechalogout
     * @return sesion
     **/
    public function setFechalogout($fechalogout){
        $this->fechalogout = $fechalogout;
        return $this;
    }

    /**
     * Get fechalogout
     * @return datetime
     **/
    public function getFechalogout(){
        return $this->fechalogout;
    }

    /**
     * Set estado
     * @param integer $estado
     * @return sesion
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->host = gethostname();
        $strip="unknown";
        if(isset($_SERVER['HTTP_CLIENT_IP'])){
            $strip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $strip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }elseif(isset($_SERVER['REMOTE_ADDR'])){
            $strip=$_SERVER['REMOTE_ADDR'];
        }
        $this->ip=$strip;
        $this->fechalogin = new \DateTime();
        $this->estado = 1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
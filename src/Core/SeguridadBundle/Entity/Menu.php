<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.menu")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\MenuRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Menu{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.menu_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $titulo
     * @ORM\Column(name="titulo", type="string",length=50, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo titulo")
     **/
    private $titulo;

    /**
     * @var integer $orden
     * @ORM\Column(name="orden", type="integer", nullable=true)
     **/
    private $orden;

    /**
     * @var string $icono
     * @ORM\Column(name="icono", type="string",length=100, nullable=true)
     **/
    private $icono;

    /**
     * @var string $url
     * @ORM\Column(name="url", type="string",length=200, nullable=true)
     **/
    private $url;

    /**
     * @var integer $menu_id
     * @ORM\Column(name="menu_id", type="integer", nullable=false)
     **/
    private $menu_id;

    /**
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", nullable=true)
     * */
    private $menu_padre;

    /**
     * @var integer $aplicacion_id
     * @ORM\Column(name="aplicacion_id", type="integer", nullable=true)
     **/
    private $aplicacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Aplicacion")
     * @ORM\JoinColumn(name="aplicacion_id", referencedColumnName="id", nullable=false)
     * */
    private $aplicacion;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=200, nullable=true)
     **/
    private $descripcion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var boolean $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set titulo
     * @param string titulo
     * @return menu
     **/
    public function setTitulo($titulo){
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * Get titulo
     * @return string
     **/
    public function getTitulo(){
        return $this->titulo;
    }

    /**
     * Set orden
     * @param integer orden
     * @return menu
     **/
    public function setOrden($orden){
        $this->orden = $orden;
        return $this;
    }

    /**
     * Get orden
     * @return integer
     **/
    public function getOrden(){
        return $this->orden;
    }

    /**
     * Set icono
     * @param string icono
     * @return menu
     **/
    public function setIcono($icono){
        $this->icono = $icono;
        return $this;
    }

    /**
     * Get icono
     * @return string
     **/
    public function getIcono(){
        return $this->icono;
    }

    /**
     * Set url
     * @param string url
     * @return menu
     **/
    public function setUrl($url){
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     * @return string
     **/
    public function getUrl(){
        return $this->url;
    }

    /**
     * Set menu_id
     * @param integer menu_id
     * @return menu
     **/
    public function setMenuId($menu_id){
        $this->menu_id = $menu_id;
        return $this;
    }

    /**
     * Get menu_id
     * @return integer
     **/
    public function getMenuId(){
        return $this->menu_id;
    }

    /**
     * Set menu
     * @param \Core\SeguridadBundle\Entity\Menu $menuPadre
     * */
    public function setMenuPadre(\Core\SeguridadBundle\Entity\Menu $menuPadre=null) {
        $this->menu_padre = $menuPadre;
        return $this;
    }

    /**
     * Get menu
     * @return \Core\SeguridadBundle\Entity\Menu
     * */
    public function getMenuPadre() {
        return $this->menu_padre;
    }

    /**
     * Set aplicacion_id
     * @param integer aplicacion_id
     * @return menu
     **/
    public function setAplicacionId($aplicacion_id){
        $this->aplicacion_id = $aplicacion_id;
        return $this;
    }

    /**
     * Get aplicacion_id
     * @return integer
     **/
    public function getAplicacionId(){
        return $this->aplicacion_id;
    }

    /**
     * Set aplicacion
     * @param \Core\SeguridadBundle\Entity\Aplicacion $aplicacion
     * */
    public function setAplicacion(\Core\SeguridadBundle\Entity\Aplicacion $aplicacion) {
        $this->aplicacion = $aplicacion;
        return $this;
    }

    /**
     * Get aplicacion
     * @return \Core\SeguridadBundle\Entity\Aplicacion
     * */
    public function getAplicacion() {
        return $this->aplicacion;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return menu
     **/
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return menu
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return menu
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set estado
     * @param integer estado
     * @return menu
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return menu
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

}

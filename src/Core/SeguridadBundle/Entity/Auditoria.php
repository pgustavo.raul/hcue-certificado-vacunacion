<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Table(name="hcue_sistema.auditoria")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\AuditoriaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Auditoria{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.auditoria_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $registro
     * @ORM\Column(name="registro", type="string", nullable=true)
     **/
    private $registro;

    /**
     * @var string $cambio
     * @ORM\Column(name="cambio", type="string", nullable=true)
     **/
    private $cambio;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=250, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo descripcion")
     **/
    private $descripcion;

    /**
     * @var integer $usuarioperfil_id
     * @ORM\Column(name="usuarioperfil_id", type="integer", nullable=false)
     **/
    private $usuarioperfil_id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuarioperfil")
     * @ORM\JoinColumn(name="usuarioperfil_id", referencedColumnName="id")
     **/
    private $usuarioperfil;

    /**
     * @var integer $menurol_id
     * @ORM\Column(name="menurol_id", type="integer", nullable=true)
     **/
    private $menurol_id;

    /**
     * @ORM\ManyToOne(targetEntity="MenuRol")
     * @ORM\JoinColumn(name="menurol_id", referencedColumnName="id")
     **/
    private $menurol;

    /**
     * @var integer $idregistro
     * @ORM\Column(name="idregistro", type="integer", nullable=true)
     **/
    private $idregistro;

    /**
     * @var string $ip
     * @ORM\Column(name="ip", type="string",length=20, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo ip")
     **/
    private $ip;

    /**
     * @var string $host
     * @ORM\Column(name="host", type="string",length=50, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo host")
     **/
    private $host;

    /**
     * @var string $tabla
     * @ORM\Column(name="tabla", type="string",length=50, nullable=true)
     **/
    private $tabla;

    /**
     * @var string $accion
     * @ORM\Column(name="accion", type="string",length=20, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo accion")
     **/
    private $accion;

    /**
     * @var integer $version
     * @ORM\Column(name="version", type="integer", nullable=true)
     **/
    private $version;

    /**
     * @var \DateTime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set registro
     * @param string $registro
     * @return auditoria
     **/
    public function setRegistro($registro){
        $this->registro = $registro;
        return $this;
    }

    /**
     * Get registro
     * @return string
     **/
    public function getRegistro(){
        return $this->registro;
    }

    /**
     * Set cambio
     * @param string $cambio
     * @return auditoria
     **/
    public function setCambio($cambio){
        $this->cambio = $cambio;
        return $this;
    }

    /**
     * Get cambio
     * @return string
     **/
    public function getCambio(){
        return $this->cambio;
    }

    /**
     * Set descripcion
     * @param string $descripcion
     * @return auditoria
     **/
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Set usuarioperfil_id
     * @param integer $usuarioperfil_id
     * @return auditoria
     **/
    public function setUsuarioperfilId($usuarioperfil_id){
        $this->usuarioperfil_id = $usuarioperfil_id;
        return $this;
    }

    /**
     * Get usuarioperfil_id
     * @return integer
     **/
    public function getUsuarioperfilId(){
        return $this->usuarioperfil_id;
    }

    /**
     * Set usuarioperfil
     * @param \Core\SeguridadBundle\Entity\Usuarioperfil $usuarioperfil
     * @return auditoria
     **/
    public function setUsuarioperfil(Usuarioperfil $usuarioperfil){
        $this->usuarioperfil = $usuarioperfil;
        return $this;
    }

    /**
     * Get usuarioperfil
     * @return \Core\SeguridadBundle\Entity\Usuarioperfil
     **/
    public function getUsuarioperfil(){
        return $this->usuarioperfil;
    }

    /**
     * Set menurol_id
     * @param integer $menurol_id
     * @return auditoria
     **/
    public function setMenurolId($menurol_id){
        $this->menurol_id = $menurol_id;
        return $this;
    }

    /**
     * Get menurol_id
     * @return integer
     **/
    public function getMenurolId(){
        return $this->menurol_id;
    }

    /**
     * Set menurol
     * @param \Core\SeguridadBundle\Entity\Menurol $menurol
     * @return auditoria
     **/
    public function setMenurol(Menurol $menurol){
        $this->menurol = $menurol;
        return $this;
    }

    /**
     * Get menurol
     * @return \Core\SeguridadBundle\Entity\Menurol
     **/
    public function getMenurol(){
        return $this->menurol;
    }

    /**
     * Set idregistro
     * @param integer $idregistro
     * @return auditoria
     **/
    public function setIdregistro($idregistro){
        $this->idregistro = $idregistro;
        return $this;
    }

    /**
     * Get idregistro
     * @return integer
     **/
    public function getIdregistro(){
        return $this->idregistro;
    }

    /**
     * Get ip
     * @return string
     **/
    public function getIp(){
        return $this->ip;
    }

    /**
     * Get host
     * @return string
     **/
    public function getHost(){
        return $this->host;
    }

    /**
     * Set tabla
     * @param string $tabla
     * @return auditoria
     **/
    public function setTabla($tabla){
        $this->tabla = $tabla;
        return $this;
    }

    /**
     * Get tabla
     * @return string
     **/
    public function getTabla(){
        return $this->tabla;
    }

    /**
     * Set accion
     * @param string $accion
     * @return auditoria
     **/
    public function setAccion($accion){
        $this->accion = $accion;
        return $this;
    }

    /**
     * Get accion
     * @return string
     **/
    public function getAccion(){
        return $this->accion;
    }

    /**
     * Set version
     * @param integer $version
     * @return auditoria
     **/
    public function setVersion($version){
        $this->version = $version;
        return $this;
    }

    /**
     * Get version
     * @return integer
     **/
    public function getVersion(){
        return $this->version;
    }

    /**
     * Get fechacreacion
     * @return \DateTime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->host = gethostname();
        $strip="unknown";
        if(isset($_SERVER['HTTP_CLIENT_IP'])){
            $strip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $strip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }elseif(isset($_SERVER['REMOTE_ADDR'])){
            $strip=$_SERVER['REMOTE_ADDR'];
        }
        $this->ip=$strip;

        $this->fechacreacion = new \DateTime();
    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Core\SeguridadBundle\Entity\Constantes;

/**
 * @ORM\Table(name="hcue_sistema.usuario")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\UsuarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Usuario implements AdvancedUserInterface, \Serializable {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.usuario_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $persona_id
     * @ORM\Column(name="persona_id", type="integer", nullable=false)
     * */
    private $persona_id;

    /**
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Persona",cascade={"persist"})
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     * */
    private $persona;

    /**
     * @var string $correo
     * @ORM\Column(name="correo", type="string",length=100, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo correo")
     * */
    private $correo;

    /**
     * @var string $clave
     * @ORM\Column(name="clave", type="string",length=256, nullable=false)
     * */
    private $clave;

    /**
     * @var datetime $fechaultacceso
     * @ORM\Column(name="fechaultacceso", type="datetime", nullable=true)
     * */
    private $fechaultacceso;

    /**
     * @var integer $ctestado_id
     * @ORM\Column(name="ctestado_id", type="integer", nullable=false)
     * */
    private $ctestado_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestado_id", referencedColumnName="id")
     * */
    private $ctestado;

    /**
     * @var datetime $fechaclave
     * @ORM\Column(name="fechaclave", type="datetime", nullable=true)
     * */
    private $fechaclave;

    /**
     * @var string $usuario
     * @ORM\Column(name="usuario", type="string",length=16, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo usuario")
     * */
    private $usuario;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var integer $cambiarclave
     * @ORM\Column(name="cambiarclave", type="integer", nullable=true)
     * */
    private $cambiarclave;


    private $usuarioperfil;

    /**
     * @ORM\OneToMany(targetEntity="Usuarioperfil", mappedBy="usuario")
     */
    private $usuarioperfiles;

    /**
     * Get usuario
     * @return \Core\SeguridadBundle\Entity\Usuarioperfil
     **/
    public function getUsuarioPerfiles(){
        return $this->usuarioperfiles;
    }

    /**
     * Set perfil
     * @param \Core\SeguridadBundle\Entity\Usuarioperfil $perfil
     **/
    public function setUsuarioPerfil(\Core\SeguridadBundle\Entity\Usuarioperfil $usuarioperfil){
        $this->usuarioperfil = $usuarioperfil;
        return $this;
    }

    /**
     * Get perfil
     * @return \Core\SeguridadBundle\Entity\Usuarioperfil
     **/
    public function getUsuarioPerfil(){
        return $this->usuarioperfil;
    }

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Set persona_id
     * @param integer persona_id
     * @return usuario
     * */
    public function setPersonaId($persona_id) {
        $this->persona_id = $persona_id;
        return $this;
    }

    /**
     * Get persona_id
     * @return integer
     * */
    public function getPersonaId() {
        return $this->persona_id;
    }

    /**
     * Set persona
     * @param \Core\AppBundle\Entity\Persona $persona
     * */
    public function setPersona(\Core\AppBundle\Entity\Persona $persona) {
        $this->persona = $persona;
        return $this;
    }

    /**
     * Get persona
     * @return \Core\AppBundle\Entity\Persona
     * */
    public function getPersona() {
        return $this->persona;
    }

    /**
     * Set correo
     * @param string correo
     * @return usuario
     * */
    public function setCorreo($correo) {
        $this->correo = $correo;
        return $this;
    }

    /**
     * Get correo
     * @return string
     * */
    public function getCorreo() {
        return $this->correo;
    }

    /**
     * Set clave
     * @param string clave
     * @return usuario
     * */
    public function setClave($clave) {
        $this->clave = $clave;
        return $this;
    }

    /**
     * Get clave
     * @return string
     * */
    public function getClave() {
        return $this->clave;
    }

    /**
     * Set fechaultacceso
     * @param datetime fechaultacceso
     * @return usuario
     * */
    public function setFechaultacceso($fechaultacceso) {
        $this->fechaultacceso = $fechaultacceso;
        return $this;
    }

    /**
     * Get fechaultacceso
     * @return datetime
     * */
    public function getFechaultacceso() {
        return $this->fechaultacceso;
    }

    /**
     * Set ctestado_id
     * @param integer ctestado_id
     * @return usuario
     * */
    public function setCtestadoId($ctestado_id) {
        $this->ctestado_id = $ctestado_id;
        return $this;
    }

    /**
     * Get ctestado_id
     * @return integer
     * */
    public function getCtestadoId() {
        return $this->ctestado_id;
    }

    /**
     * Set ctestado
     * @param \Core\AppBundle\Entity\Catalogo $ctestado
     * */
    public function setCtestado(\Core\AppBundle\Entity\Catalogo $ctestado=null) {
        $this->ctestado = $ctestado;
        return $this;
    }

    /**
     * Get ctestado
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtestado() {
        return $this->ctestado;
    }

    /**
     * Set fechaclave
     * @param datetime fechaclave
     * @return usuario
     * */
    public function setFechaclave($fechaclave) {
        $this->fechaclave = $fechaclave;
        return $this;
    }

    /**
     * Get fechaclave
     * @return datetime
     * */
    public function getFechaclave() {
        return $this->fechaclave;
    }

    /**
     * Set usuario
     * @param string usuario
     * @return usuario
     * */
    public function setUsuario($usuario) {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * Get usuario
     * @return string
     * */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Set activo
     * @param integer activo
     * @return usuario
     * */
    public function setActivo($activo) {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return usuario
     * */
    public function setUsuariocreacionId($usuariocreacion_id) {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId() {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return usuario
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id) {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId() {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set cambiarclave
     * @param integer cambiarclave
     * @return usuario
     * */
    public function setCambiarclave($cambiarclave) {
        $this->cambiarclave = $cambiarclave;
        return $this;
    }

    /**
     * Get cambiarclave
     * @return integer
     * */
    public function getCambiarclave() {
        return $this->cambiarclave;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist() {
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
        $this->activo = 1;
        $this->cambiarclave = 1;
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate() {
        $this->fechamodificacion = new \DateTime();
    }

    
    public function __construct() {
        $this->activo = 1;
    }

    public function eraseCredentials() {
        
    }

    public function getPassword() {
        return $this->clave;
    }

    public function getRoles() {
        return array("ROLE_USER");
    }

    public function getSalt() {

    }

    public function getUsername() {
        return $this->usuario;
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        if($this->ctestado_id == Constantes::CT_ESTADOUSUARIOBLOQUEADO){
            return false;
        }
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEnabled() {
        if($this->ctestado_id == Constantes::CT_ESTADOUSUARIOINACTIVO){
            return false;
        }
        return true;
    }

    public function serialize() {

        return serialize(array(
            $this->id,
            $this->usuario,
            $this->clave,
            $this->ctestado_id
        ));
    }

    public function unserialize($serialized) {

        list(
            $this->id,
            $this->usuario,
            $this->clave,
            $this->ctestado_id
        )= unserialize($serialized);
    }
}
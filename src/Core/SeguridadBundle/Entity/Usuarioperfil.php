<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.usuarioperfil")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\UsuarioperfilRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Usuarioperfil {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.usuarioperfil_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $usuario_id
     * @ORM\Column(name="usuario_id", type="integer", nullable=true)
     * */
    private $usuario_id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="usuarioperfiles")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * */
    private $usuario;

    /**
     * @var integer $perfilentidad_id
     * @ORM\Column(name="perfilentidad_id", type="integer", nullable=true)
     * */
    private $perfilentidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Perfilentidad")
     * @ORM\JoinColumn(name="perfilentidad_id", referencedColumnName="id")
     * */
    private $perfilentidad;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=true)
     * */
    private $estado;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     * */
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Set usuario_id
     * @param integer usuario_id
     * @return usuarioperfil
     * */
    public function setUsuarioId($usuario_id) {
        $this->usuario_id = $usuario_id;
        return $this;
    }

    /**
     * Get usuario_id
     * @return integer
     * */
    public function getUsuarioId() {
        return $this->usuario_id;
    }

    /**
     * Set usuario
     * @param \Core\SeguridadBundle\Entity\Usuario $usuario
     * @return usuarioperfil
     * */
    public function setUsuario(\Core\SeguridadBundle\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * Get usuario
     * @return \Core\SeguridadBundle\Entity\Usuario
     * */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Set perfilentidad_id
     * @param integer perfilentidad_id
     * @return usuarioperfil
     * */
    public function setPerfilentidadId($perfilentidad_id) {
        $this->perfilentidad_id = $perfilentidad_id;
        return $this;
    }

    /**
     * Get perfilentidad_id
     * @return integer
     * */
    public function getPerfilentidadId() {
        return $this->perfilentidad_id;
    }

    /**
     * Set perfilentidad
     * @param \Core\SeguridadBundle\Entity\Perfilentidad $perfilentidad
     * @return usuarioperfil
     * */
    public function setPerfilentidad(\Core\SeguridadBundle\Entity\Perfilentidad $perfilentidad) {
        $this->perfilentidad = $perfilentidad;
        return $this;
    }

    /**
     * Get perfilentidad
     * @return \Core\SeguridadBundle\Entity\Perfilentidad
     * */
    public function getPerfilentidad() {
        return $this->perfilentidad;
    }

    /**
     * Set estado
     * @param integer estado
     * @return usuarioperfil
     * */
    public function setEstado($estado) {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return usuarioperfil
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return usuarioperfil
     * */
    public function setUsuariocreacionId($usuariocreacion_id) {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId() {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return usuarioperfil
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id) {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId() {
        return $this->usuariomodificacion_id;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist() {
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
        $this->activo = 1;
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate() {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone() {
        $this->id = null;
    }

    public function copy() {
        return clone $this;
    }
}
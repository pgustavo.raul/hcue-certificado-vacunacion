<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.perfil")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\PerfilRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Perfil{

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=false)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.perfil_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=50, nullable=true)
     **/
    private $nombre;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=300, nullable=true)
     **/
    private $descripcion;

    /**
     * @var integer $perfil_id
     * @ORM\Column(name="perfil_id", type="integer", nullable=true)
     **/
    private $perfil_id;

    /**
     * @ORM\ManyToOne(targetEntity="Perfil")
     * @ORM\JoinColumn(name="perfil_id", referencedColumnName="id")
     **/
    private $perfil_padre;

    /**
     * @var boolean $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $organico_id
     * @ORM\Column(name="organico_id", type="integer", nullable=true)
     **/
    private $organico_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Organico")
     * @ORM\JoinColumn(name="organico_id", referencedColumnName="id")
     **/
    private $organico;


    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return perfil
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return perfil
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return perfil
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return perfil
     **/
    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     **/
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return perfil
     **/
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Set perfil_id
     * @param integer perfil_id
     * @return perfil
     **/
    public function setPerfilId($perfil_id){
        $this->perfil_id = $perfil_id;
        return $this;
    }

    /**
     * Get perfil_id
     * @return integer
     **/
    public function getPerfilId(){
        return $this->perfil_id;
    }

    /**
     * Set perfil
     * @param \Core\SeguridadBundle\Entity\Perfil $perfilPadre
     **/
    public function setPerfilPadre(\Core\SeguridadBundle\Entity\Perfil $perfilPadre=null){
        $this->perfil_padre = $perfilPadre;
        return $this;
    }

    /**
     * Get perfil
     * @return \Core\SeguridadBundle\Entity\Perfil
     **/
    public function getPerfilPadre(){
        return $this->perfil_padre;
    }

    /**
     * Set estado
     * @param integer estado
     * @return perfil
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set organico_id
     * @param integer organico_id
     * @return perfil
     **/
    public function setOrganicoId($organico_id){
        $this->organico_id = $organico_id;
        return $this;
    }

    /**
     * Get organico_id
     * @return integer
     **/
    public function getOrganicoId(){
        return $this->organico_id;
    }

    /**
     * Set organico
     * @param \Core\AppBundle\Entity\Organico $organico
     **/
    public function setOrganico(\Core\AppBundle\Entity\Organico $organico){
        $this->organico = $organico;
        return $this;
    }

    /**
     * Get organico
     * @return \Core\AppBundle\Entity\Organico
     **/
    public function getOrganico(){
        return $this->organico;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }


    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

}
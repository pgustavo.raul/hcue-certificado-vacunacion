<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.perfilentidad")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\PerfilentidadRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Perfilentidad{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.perfilentidad_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $perfil_id
     * @ORM\Column(name="perfil_id", type="integer", nullable=false)
     **/
    private $perfil_id;

    /**
     * @ORM\ManyToOne(targetEntity="Perfil")
     * @ORM\JoinColumn(name="perfil_id", referencedColumnName="id")
     **/
    private $perfil;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var integer $perfilentidad_id
     * @ORM\Column(name="perfilentidad_id", type="integer", nullable=true)
     **/
    private $perfilentidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Perfilentidad")
     * @ORM\JoinColumn(name="perfilentidad_id", referencedColumnName="id")
     **/
    private $perfilentidad_padre;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set perfil_id
     * @param integer perfil_id
     * @return perfilentidad
     **/
    public function setPerfilId($perfil_id){
        $this->perfil_id = $perfil_id;
        return $this;
    }

    /**
     * Get perfil_id
     * @return integer
     **/
    public function getPerfilId(){
        return $this->perfil_id;
    }

    /**
     * Set perfil
     * @param \Core\SeguridadBundle\Entity\Perfil $perfil
     **/
    public function setPerfil(\Core\SeguridadBundle\Entity\Perfil $perfil){
        $this->perfil = $perfil;
        return $this;
    }

    /**
     * Get perfil
     * @return \Core\SeguridadBundle\Entity\Perfil
     **/
    public function getPerfil(){
        return $this->perfil;
    }

    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return perfilentidad
     **/
    public function setEntidadId($entidad_id){
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId(){
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     **/
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad){
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidad(){
        return $this->entidad;
    }

    /**
     * Set perfilentidad_id
     * @param integer perfilentidad_id
     * @return perfilentidad
     **/
    public function setPerfilentidadId($perfilentidad_id){
        $this->perfilentidad_id = $perfilentidad_id;
        return $this;
    }

    /**
     * Get perfilentidad_id
     * @return integer
     **/
    public function getPerfilentidadId(){
        return $this->perfilentidad_id;
    }

    /**
     * Set perfilentidad
     * @param \Core\SeguridadBundle\Entity\Perfilentidad $perfilentidadPadre
     **/
    public function setPerfilentidadPadre(\Core\SeguridadBundle\Entity\Perfilentidad $perfilentidadPadre){
        $this->perfilentidad_padre = $perfilentidadPadre;
        return $this;
    }

    /**
     * Get perfilentidad
     * @return \Core\SeguridadBundle\Entity\Perfilentidad
     **/
    public function getPerfilentidadPadre(){
        return $this->perfilentidad_padre;
    }

    /**
     * Set estado
     * @param integer estado
     * @return perfilentidad
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return perfilentidad
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->activo=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){

    }


    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
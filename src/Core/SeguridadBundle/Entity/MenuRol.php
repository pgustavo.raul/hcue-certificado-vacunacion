<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.menurol")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\MenuRolRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MenuRol{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.menurol_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $menu_id
     * @ORM\Column(name="menu_id", type="integer", nullable=false)
     **/
    private $menu_id;

    /**
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     **/
    private $menu;

    /**
     * @var integer $rol_id
     * @ORM\Column(name="rol_id", type="integer", nullable=false)
     **/
    private $rol_id;

    /**
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     **/
    private $rol;

    /**
     * @var integer $ver
     * @ORM\Column(name="ver", type="integer", nullable=true)
     **/
    private $ver;

    /**
     * @var integer $agregar
     * @ORM\Column(name="agregar", type="integer", nullable=true)
     **/
    private $agregar;

    /**
     * @var integer $editar
     * @ORM\Column(name="editar", type="integer", nullable=true)
     **/
    private $editar;

    /**
     * @var integer $eliminar
     * @ORM\Column(name="eliminar", type="integer", nullable=true)
     **/
    private $eliminar;

    /**
     * @var integer $ctverinfo_id
     * @ORM\Column(name="ctverinfo_id", type="integer", nullable=true)
     **/
    private $ctverinfo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctverinfo_id", referencedColumnName="id")
     **/
    private $ctverinfo;

    /**
     * @var integer $exportar
     * @ORM\Column(name="exportar", type="integer", nullable=true)
     **/
    private $exportar;

    /**
     * @var boolean $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set menu_id
     * @param integer menu_id
     * @return menurol
     **/
    public function setMenuId($menu_id){
        $this->menu_id = $menu_id;
        return $this;
    }

    /**
     * Get menu_id
     * @return integer
     **/
    public function getMenuId(){
        return $this->menu_id;	}

    /**
     * Set menu
     * @param \Core\SeguridadBundle\Entity\Menu $menu
     **/
    public function setMenu(\Core\SeguridadBundle\Entity\Menu $menu){
        $this->menu = $menu;
        return $this;
    }

    /**
     * Get menu
     * @return \Core\SeguridadBundle\Entity\Menu
     **/
    public function getMenu(){
        return $this->menu;
    }

    /**
     * Set rol_id
     * @param integer rol_id
     * @return menurol
     **/
    public function setRolId($rol_id){
        $this->rol_id = $rol_id;
        return $this;
    }

    /**
     * Get rol_id
     * @return integer
     **/
    public function getRolId(){
        return $this->rol_id;
    }

    /**
     * Set rol
     * @param \Core\SeguridadBundle\Entity\Rol $rol
     **/
    public function setRol(\Core\SeguridadBundle\Entity\Rol $rol){
        $this->rol = $rol;
        return $this;
    }

    /**
     * Get rol
     * @return \Core\SeguridadBundle\Entity\Rol
     **/
    public function getRol(){
        return $this->rol;
    }

    /**
     * Set ver
     * @param integer ver
     * @return menurol
     **/
    public function setVer($ver){
        $this->ver = $ver;
        return $this;
    }

    /**
     * Get ver
     * @return integer
     **/
    public function getVer(){
        return $this->ver;
    }

    /**
     * Set agregar
     * @param integer agregar
     * @return menurol
     **/
    public function setAgregar($agregar){
        $this->agregar = $agregar;
        return $this;
    }

    /**
     * Get agregar
     * @return integer
     **/
    public function getAgregar(){
        return $this->agregar;
    }

    /**
     * Set editar
     * @param integer editar
     * @return menurol
     **/
    public function setEditar($editar){
        $this->editar = $editar;
        return $this;
    }

    /**
     * Get editar
     * @return integer
     **/
    public function getEditar(){
        return $this->editar;
    }

    /**
     * Set eliminar
     * @param integer eliminar
     * @return menurol
     **/
    public function setEliminar($eliminar){
        $this->eliminar = $eliminar;
        return $this;
    }

    /**
     * Get eliminar
     * @return integer
     **/
    public function getEliminar(){
        return $this->eliminar;
    }

    /**
     * Set ctverinfo_id
     * @param integer ctverinfo_id
     * @return menurol
     **/
    public function setCtverinfoId($ctverinfo_id){
        $this->ctverinfo_id = $ctverinfo_id;
        return $this;
    }

    /**
     * Get ctverinfo_id
     * @return integer
     **/
    public function getCtverinfoId(){
        return $this->ctverinfo_id;
    }

    /**
     * Set ctverinfo
     * @param \Core\AppBundle\Entity\Catalogo $ctverinfo
     **/
    public function setCtverinfo(\Core\AppBundle\Entity\Catalogo $ctverinfo=null){
        $this->ctverinfo = $ctverinfo;
        return $this;
    }

    /**
     * Get ctverinfo
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtverinfo(){
        return $this->ctverinfo;
    }

    /**
     * Set exportar
     * @param integer exportar
     * @return menurol
     **/
    public function setExportar($exportar){
        $this->exportar = $exportar;
        return $this;
    }

    /**
     * Get exportar
     * @return integer
     **/
    public function getExportar(){
        return $this->exportar;
    }

    /**
     * Set estado
     * @param integer estado
     * @return menurol
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return rol
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return menurol
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return menurol
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

}
<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

class UsuarioRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Usuario
     * @return QueryBuilder
     */
    public function getUsuarioQueryBuilder() {
        return $this->createQueryBuilder('t')
            ->select('t');
    }

    /**
     * Realiza la validación del usuario.
     * @param string $usuario Usuario del registro Usuario
     * @param string $clave Clave del registro Usuario
     * @return Array Core\SeguridadBundle\Entity\Usuario
     */
    public function validarUsuario($usuario, $clave){
        $qb= $this->createQueryBuilder('us')
            ->select('us')
            ->where('us.usuario = :usuario')
            ->andWhere('us.clave = :clave')
            ->andWhere('us.ctestado_id = :ctestado_id')
            ->setParameters(array(
                'usuario' => $usuario,
                'clave' => $clave,
                'ctestado_id' => Constantes::CT_ESTADOUSUARIOACTIVO
            ));
        $query = $qb->getQuery()->useQueryCache(true);
        return $query->getResult();
    }

    /**
     * Obtiene la lista de aplicaciones que le corresponden a un usuario
     * @param integer $usuario_id Id del Usuario Autenticado
     * @param integer $app_id Id de la Aplicación en la cual esta autenticado
     * @param integer $usuarioPerfil Id del perfil Activo en la Sesión
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\Aplicacion"
     */
    public function getAplicaciones($usuario_id, $app_id=0, $usuarioPerfil=0){
        $qb= $this->createQueryBuilder("us")
            ->from('CoreSeguridadBundle:Usuarioperfil','up')
            ->from('CoreSeguridadBundle:Perfilrol','pr')
            ->select("DISTINCT ap.id, ap.nombre, ap.descripcion, ap.icono, ap.url")
            ->join('up.perfilentidad','pe')
            ->join('pe.perfil','per')
            ->join('pr.rol','ro')
            ->join('ro.aplicacion','ap')
            ->where('us.id = :usuario_id')
            ->andWhere('up.usuario_id = us.id')
            ->andWhere('pr.perfil_id = per.id')
            ->andWhere('ap.estado = :estado_general')
            ->andWhere('ap.activo = :estado_general')
            ->andWhere('pr.estado = :estado_general')
            ->andWhere('up.estado = :estado_general')
            ->setParameters([
                'usuario_id' => $usuario_id,
                'estado_general' => Constantes::ESTADOGENERALHABILITADO
            ]);

        if(!empty($app_id) && is_numeric($app_id)){
            $qb->andWhere('ap.id != :aplicacion_id')
                ->setParameter('aplicacion_id', $app_id);
        }

        if(!empty($usuarioPerfil) && is_numeric($usuarioPerfil)){
            $qb->andWhere('up.id = :usuarioperfil_id')
                ->setParameter('usuarioperfil_id', $usuarioPerfil);
        }

        $query=$qb->getQuery();        
        
        $resultId="result_aplicaciones_by_usuario_or_not_app_or_usuarioPerfil_{$usuario_id}_{$app_id}_$usuarioPerfil";
        $query->useQueryCache(true)
              ->useResultCache(true,86400,$resultId);
        return $query->getResult();
    }

    /**
     * Realiza la verificación de la Persona registrada como Usuario.
     * @param integer $persona_id Clave Primaria de la Persona a Registrarse como Usuario
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objeto de tipo "Core\SeguridadBundle\Entity\Usuario"
     */
    public function getUsuarioPersona($persona_id, $arrayResult=false) {

        $qb= $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.persona_id = :persona_id')
            ->andWhere('u.activo = :activo')
            ->setParameters([
                'persona_id' => $persona_id,
                'activo' => Constantes::ESTADOGENERALHABILITADO
            ]);
        $query=$qb->getQuery();
        

        return ($arrayResult)?$query->getArrayResult():$query->getOneOrNullResult();

    }

    /**
     * Realiza la verificación de la Persona registrada como Usuario.
     * @param string $numeroidentificacion Identificacion del Usuario que se intenta ingresar
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objeto de tipo "HCUE\AdminBundle\Entity\Funcionario"
     */
    public function getUsuariobyIdentificacion($numeroidentificacion, $arrayResult=false) {

        $qb = $this->createQueryBuilder("u")
            ->select('u')
            ->innerJoin('u.persona', 'p', 'WITH', 'p.id = u.persona_id and p.activo = :estadogeneral')
            ->where('p.numeroidentificacion = :identificacion')
            ->andWhere('u.activo = :estadogeneral')
            ->setParameters([
                'estadogeneral' => Constantes::ESTADOGENERALHABILITADO,
                'identificacion' => $numeroidentificacion
            ]);

        $query = $qb->getQuery();
        
        $resultId=($arrayResult)?"result_array_usuario_by_identificacion_$numeroidentificacion":"result_usuario_by_identificacion_$numeroidentificacion";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId);         
        
       return  ($arrayResult)?$query->getArrayResult():$query->getOneOrNullResult();

    }

    /**
     * Realiza la validación del usuario cuando olvidó su contraseña.
     * @param string $username username del usuario
     * @return Array Lista de Objeto de tipo "Core\SeguridadBundle\Entity\Usuario"
     */
    public function getUsuarioByUsername($username){
        $qb= $this->createQueryBuilder('us')
            ->select('us')
            ->where('us.usuario = :username')
            ->andWhere('us.activo = :activo')
            ->andWhere('us.ctestado_id! = :ctestado_id')
            ->setParameters([
                'ctestado_id' => Constantes::CT_ESTADOUSUARIOINACTIVO,
                'username' => $username,
                'activo' => Constantes::ESTADOGENERALHABILITADO
            ])
        ;
        $query=$qb->getQuery();
        $query->useQueryCache(true);
        
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

    /**
     * Verificar el correo del usuaurio
     * @param String $correo
     * @param integer $id
     * @return Array Lista de Objeto de tipo "Core\SeguridadBundle\Entity\Usuario"
     */
    public function verificarCorreo($correo, $id=0) {
        $qb= $this->createQueryBuilder('us')
            ->select('us')
            ->where('us.correo = :correo')
            ->andWhere('us.activo = :activo')
            ->setParameters([
                'correo' => $correo,
                'activo' => Constantes::ESTADOGENERALHABILITADO
            ]);

        if((int) $id > 0){
            $qb->andWhere('us.id != :usuario_id')
                ->setParameter('usuario_id', $id);
        }

        $query = $qb->getQuery()->useQueryCache(true);
        return $query->setMaxResults(1)->getOneOrNullResult();
    }
}
<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class SesionRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Sesion
     * @return QueryBuilder
     */
    public function getSesionQueryBuilder() {
        return $this->createQueryBuilder('s')
            ->select('s');
    }

}
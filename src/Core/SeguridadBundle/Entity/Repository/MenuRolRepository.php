<?php

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

/**
 * Descripcion de RolRepository
 * @author Luis Mendoza
 */
class MenuRolRepository extends EntityRepository {

    /**
     * Devuelve el Objeto MenuRol solicitado
     * @param integer $menurol_id Id del MenuRol
     * @param integer $usuarioperfil_id
     * @return \Core\SeguridadBundle\Entity\MenuRol
     */
    public function obtenerMenuRol($menurol_id, $usuarioperfil_id) {
        $qb = $this->createQueryBuilder('mr')
            ->select('mr,m,r,ct')
            ->from('CoreSeguridadBundle:Perfilrol','pr')
            ->from('CoreSeguridadBundle:Usuarioperfil','up')
            ->join('up.perfilentidad','pe')
            ->join('mr.menu', 'm', 'WITH', 'm.estado=:activo and m.activo=:activo')
            ->join('mr.rol', 'r', 'WITH', 'r.estado=:activo and r.activo=:activo')
            ->leftJoin('mr.ctverinfo', 'ct')
            ->where('mr.id = :menurol_id')
            ->andWhere('pr.rol_id = mr.rol_id')
            ->andWhere('pe.perfil_id = pr.perfil_id')
            ->andWhere('up.id = :usuarioperfil_id')
            ->andWhere('mr.activo = :activo')
            ->andWhere('mr.estado = :activo')
            ->setParameters([
                'menurol_id' => $menurol_id,
                'usuarioperfil_id' => $usuarioperfil_id,
                'activo' => Constantes::ESTADOGENERALHABILITADO
            ]);
        $query = $qb->getQuery();
        $query->useQueryCache(true);
              
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

    /**
     * Obtiene la lista de menu_roles
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @param integer @estado para retornar la relacion de menu-roles de acuerdo a su estado (activo, inactivo)
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\MenuRol"
     */
    public function getMenuRoles($estado=0, $arrayResult=false) {
        $qb = $this->getMenuRolQueryBuilder($estado);
        $query = $qb->getQuery()->useQueryCache(true);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de menu-roles
     * @param integer @estado para retornar la relacion menu-roles de acuerdo a su estado (activo, inactivo)
     * @return QueryBuilder
     */
    public function getMenuRolQueryBuilder($estado) {
        $qb = $this->createQueryBuilder('mr')
            ->select('mr')
            ->andWhere('mr.activo = :activo')
            ->setParameter('activo', Constantes::ESTADOGENERALHABILITADO);

        if(!empty($estado) && is_numeric($estado)){
            $qb->andWhere('mr.estado = :estado')
                ->setParameter('estado', $estado);
        }
        return $qb;
    }

    /**
     * Obtiene la lista de menu_roles
     * @param integer $menurol_id Id delMenuRol
     * @param integer $usuarioperfil_id
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\MenuRol"
     */
    public function obtenerPermisos($menurol_id, $usuarioperfil_id) {
        $qb = $this->createQueryBuilder('mr')
            ->select('mr')
            ->from('CoreSeguridadBundle:Perfilrol','pr')
            ->from('CoreSeguridadBundle:Usuarioperfil','up')
            ->join('up.perfilentidad','pe')
            ->join('mr.menu','m')
            ->where('mr.id = :menurol_id')
            ->andWhere('mr.menu_id = m.id')
            ->andWhere('pr.rol_id = mr.rol_id')
            ->andWhere('pe.perfil_id = pr.perfil_id')
            ->andWhere('up.id = :usuarioperfil_id')
            ->andWhere('mr.activo = :activo')
            ->setParameters([
                'menurol_id' => $menurol_id,
                'usuarioperfil_id' => $usuarioperfil_id,
                'activo' => Constantes::ESTADOGENERALHABILITADO
            ]);
        $query = $qb->getQuery()->useQueryCache(true);
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

    /**
     * Obtiene la lista de objetos de tipo Menu que pertenecen a una aplicacion
     * @param integer $aplicacion_id Aplicación a la que pertenecen los menus
     * @param integer $rol_id Rol que se esta consultando
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getMenusRolByAplicacion($aplicacion_id, $rol_id, $arrayResult=false){
        $qb = $this->createQueryBuilder('mr')
            ->innerJoin('mr.rol', 'r', 'WITH', 'r.activo = :estado_general')
            ->where('mr.rol_id = :rol')
            ->andWhere('r.aplicacion_id = :aplicacion')
            ->andWhere('mr.activo = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'aplicacion' => $aplicacion_id,
                'rol' => $rol_id
            ]);

        if($arrayResult) {
            $qb->select('mr.id, mr.menu_id, mr.ver, mr.agregar, mr.editar, mr.eliminar, mr.exportar, mr.ctverinfo_id, mr.estado');
        }

        $query = $qb->getQuery()->useQueryCache(true);
        return ($arrayResult)?$query->getArrayResult():$query->getResult();
    }

}
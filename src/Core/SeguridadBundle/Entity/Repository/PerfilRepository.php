<?php
/**
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

class PerfilRepository extends EntityRepository{

    /**
     * Obtiene el objeto perfil deseado
     * @param integer $id
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\Perfil"
     */
    public function getPerfil($id, $arrayResult=false) {

        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.id = :id')
            ->andWhere('p.activo = :estado_general')
            ->andWhere('p.estado = :estado_general')
            ->setParameters([
                'id' => $id,
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO
            ]);

        $query = $qb->getQuery();
        $query->useQueryCache(true);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de la lista de hijos de un perfil
     * @param integer $parent_id
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getPerfilChildrens($parent_id, $arrayResult=false){

        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->Where('p.estado = :estado_general')
            ->andWhere('p.perfil_id = :perfil_padre')
            ->andWhere('p.activo = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'perfil_padre' => $parent_id
            ]);

        $query = $qb->getQuery();        
        $query->useQueryCache(true);
        
        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de la lista de hijos de un perfil
     * @param integer $id Id del perfil
     * @param integer $parent_id Id del perfil padre
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getPerfilAllChildrens($id, $parent_id=0, $arrayResult=false){
        $aResult=($id>0)?$this->getPerfil($id, $arrayResult):$this->getPerfilChildrens($parent_id, $arrayResult);
        $arrayReturn=array();
        foreach ($aResult as $obj){
            $arrayReturn[]=$obj;
            $aResult1=  $this->getPerfilAllChildrens(0, $obj->getId(), $arrayResult);
            if(!empty($aResult1)){
                $arrayReturn = array_merge($arrayReturn,$aResult1);
            }
        }
        return $arrayReturn;
    }

    /**
     * Retorna todos los registros de perfil ingresados en el sistema
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @param integer $estado
     * @return Array u objeto de tipo Perfil
     */
    public function getPerfiles($arrayResult=false, $estado = Constantes::ESTADOGENERALHABILITADO ) {

        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.estado = :estado_general')
            ->andWhere('p.activo = :estado_general')
            ->setParameters([
                'estado_general' => $estado,
            ]);

        $query = $qb->getQuery();

        $query->useQueryCache(true);
        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder
     * @param integer $id
     * @return QueryBuilder
     */
    public function isDelete($id){
        $qb = $this->createQueryBuilder('p');
        $qb1 = $this->createQueryBuilder('p1');

        $qb1->select('p1.perfil_id')
            ->where('p1.perfil_id = :id')
            ->andWhere('p1.activo = :estado_general')
            ->groupBy('p1.perfil_id');

        $qb->select('p')
            ->distinct(true)
            ->from('CoreSeguridadBundle:Perfilentidad','pe')
            ->from('CoreSeguridadBundle:Usuarioperfil','up')
            ->where('p.id = :id')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->andX('p.id = pe.perfil_id', 'pe.activo = :estado_general', 'pe.estado = :estado_general'),
                        $qb->expr()->andX('pe.id = up.perfilentidad_id', 'up.activo = :estado_general', 'up.estado = :estado_general')
                    ),
                    $qb->expr()->in(
                        'p.id',
                        $qb1->getDQL()
                    )
                )
            )
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'id' => $id
            ]);

        $query=$qb->getQuery();
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

}
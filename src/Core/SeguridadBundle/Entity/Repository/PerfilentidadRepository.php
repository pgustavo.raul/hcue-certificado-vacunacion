<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class PerfilentidadRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Perfilentidad
     * @return QueryBuilder
     */
    public function getPerfilentidadQueryBuilder() {
        return $this->createQueryBuilder('pe')
            ->select('pe');
    }

}
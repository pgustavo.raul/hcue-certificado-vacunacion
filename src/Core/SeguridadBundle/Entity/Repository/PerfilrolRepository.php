<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

class PerfilrolRepository extends EntityRepository{

    /**
     * Obtiene el las roles asignados a un perfil
     * @param integer $perfil_id Id del Perfil
     * @return Array u Object Perfilrol
     */
    public function getPerfilrol($perfil_id, $arrayResult=false) {

        $qb = $this->createQueryBuilder('pr')
            ->select('pr.id, pr.rol_id, pr.estado')
            ->where('pr.perfil_id = :perfil')
            ->andWhere('pr.estado = :estado_general')
            ->andWhere('pr.activo = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'perfil' => $perfil_id
            ]);
        $query = $qb->getQuery()->useQueryCache(true);
        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

}
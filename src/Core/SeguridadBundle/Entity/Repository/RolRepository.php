<?php

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

/**
 * Descripcion de RolRepository
 * @author Luis Malquin
 */
class RolRepository extends EntityRepository {

    /**
     * Obtiene la lista de roles con su respectiva relacion a la tabla aplicacion
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\Rol"
     */
    public function getRolesRelacion($arrayResult = false){

        $qb = $this->createQueryBuilder('r')
            ->select('r, a')
            ->innerJoin('r.aplicacion', 'a')
            ->where('r.estado = :estado_general')
            ->andWhere('r.activo = :estado_general')
            ->setParameter('estado_general', Constantes::ESTADOGENERALHABILITADO);

        $query = $qb->getQuery()->useQueryCache(true);
        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder
     * @param integer $id
     * @return QueryBuilder
     */
    public function isDelete($id){
        $qb = $this->createQueryBuilder('r');
        $qb->select('r')
            ->distinct(true)
//            ->from('CoreSeguridadBundle:MenuRol','mr')
            ->from('CoreSeguridadBundle:Perfilrol','pr')
            ->where('r.id = :id')
            ->andWhere(
                $qb->expr()->orX(
//                    $qb->expr()->andX('mr.rol_id = r.id', 'mr.activo = :estado_general'),
                    $qb->expr()->andX('pr.rol_id = r.id', 'pr.activo = :estado_general')
                )
            )
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'id' => $id
            ]);

        $query=$qb->getQuery();
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

}
<?php

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

/**
 * Descripcion de AplicacionRepository
 * @author Luis Mendoza
 */
class AplicacionRepository extends EntityRepository
{

    /**
     * Obtiene la lista de aplicaciones
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @param integer @estado para retornar las aplicaciones
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\Aplicacion"
     */
    public function getAplicaciones($estado = 0, $arrayResult=false) {

        $qb = $this->getAplicacionQueryBuilder($estado);
        $query = $qb->getQuery();
        
        $resultId=($arrayResult)?"result_array_aplicaciones_by_estado_$estado":"result_aplicaciones_by_estado_$estado";
        $query->useQueryCache(true)
              ->useResultCache(true,null,$resultId); 
        
        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de la lista de aplicaciones
     * @return QueryBuilder
     */
    public function getAplicacionQueryBuilder($estado = 0) {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.activo = :activo')
            ->orderBy('p.nombre', 'ASC')
            ->setParameter('activo', Constantes::ESTADOGENERALHABILITADO);

        if(!empty($estado) && is_numeric($estado)){
            $qb->andWhere('p.estado = :estado')
                ->setParameter('estado', $estado);
        }

        return $qb;
    }

    /**
     * Obtiene el QueryBuilder
     * @param integer $id
     * @return QueryBuilder
     */
    public function isDelete($id){
        $qb = $this->createQueryBuilder('ap');
        $qb->select('ap')
            ->distinct(true)
            ->from('CoreSeguridadBundle:Menu','me')
            ->from('CoreSeguridadBundle:Rol','ro')
            ->where('ap.id = :aplicacion')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX('me.aplicacion_id = ap.id', 'me.activo = :estado_general'),
                    $qb->expr()->andX('ro.aplicacion_id = ap.id', 'ro.activo = :estado_general')
                )
            )
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'aplicacion' => $id
            ]);

        $query=$qb->getQuery();
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

}
<?php

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;


/**
 * Descripcion de MenuRepository
 * @author Luis Malquin
 */
class MenuRepository extends EntityRepository{
    
    /**
     * Obtiene la lista de objetos de tipo Menu que pertenecen a una aplicacion
     * @param integer $aplicacion_id Aplicación a la que pertenecen los menus
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getMenusByAplicacion($aplicacion_id, $estado=true, $arrayResult=false){
        $qb = $this->createQueryBuilder('m')
            ->select('m.id, m.titulo, m.estado, m.menu_id')
            ->where('m.aplicacion_id = :aplicacion')
            ->andWhere('m.activo = :estado_general')
            ->orderBy('m.orden', 'ASC')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'aplicacion' => $aplicacion_id
            ]);

        if($estado){
            $qb->andWhere('m.estado = :estado_general');
        }

        $query = $qb->getQuery();
        $query->useQueryCache(true);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de menus a las cuales tiene permiso el usuario por aplicación
     * @param integer $app_id Aplicación a la que pertenecen los menus
     * @param integer $usuario_id Usuario autenticado en el Sistema
     * @param integer $usuarioPerfil_id Id Usuario perfil
     * @return QueryBuilder
     */
    public function getMenusAplicacionByUsuario($app_id, $usuario_id, $usuarioPerfil_id){
        $qb= $this->createQueryBuilder("me")
            ->select('me.id, mr.id as menurol_id, me.titulo, me.icono, me.url, me.menu_id, me.aplicacion_id')
            ->from('CoreSeguridadBundle:MenuRol','mr')
            ->from('CoreSeguridadBundle:Perfilrol','pr')
            ->from('CoreSeguridadBundle:Perfilentidad','pe')
            ->from('CoreSeguridadBundle:Usuarioperfil','up')
            ->innerJoin('me.aplicacion','ap')
            ->innerJoin('up.usuario','u')
            ->innerJoin('mr.rol','r')
            ->where('ap.id = :aplicacion_id')
            ->andWhere('r.aplicacion_id = ap.id')
            ->andWhere('u.id = :usuario_id')
            ->andWhere('up.id = :usuarioperfil_id')
            ->andWhere('mr.estado=:estado_general')
            ->andWhere('pr.estado=:estado_general')
            ->andWhere('ap.id=me.aplicacion_id')
            ->andWhere('mr.menu_id=me.id')
            ->andWhere('pr.rol_id=mr.rol_id')
            ->andWhere('pe.perfil_id=pr.perfil_id')
            ->andWhere('up.perfilentidad_id=pe.id')
            ->andWhere('u.id=up.usuario_id')
            ->andWhere('me.estado=:estado_general')
            ->andWhere('me.activo=:estado_general')
            ->andWhere('mr.activo=:estado_general')
            ->orderBy('me.orden', 'ASC')
            ->setParameters([
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO,
                'aplicacion_id' => $app_id,
                'usuario_id' => $usuario_id,
                'usuarioperfil_id' => $usuarioPerfil_id
            ]);

        $query = $qb->getQuery();
        $query->useQueryCache(true);

        return $query->getResult();
    }

    /**
     * Obtiene el QueryBuilder
     * @param integer $id
     * @return QueryBuilder
     */
    public function isDelete($id){
        $qb = $this->createQueryBuilder('me');
        $qb1 = $this->createQueryBuilder('me1');

        $qb1->select('me1.menu_id')
            ->where('me1.menu_id = :menu_id')
            ->andWhere('me1.activo = :estado_general')
            ->groupBy('me1.menu_id');

        $qb->select('me')
            ->distinct(true)
            ->from('CoreSeguridadBundle:MenuRol','mr')
            ->where('me.id = :menu_id')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX('mr.menu_id = me.id', 'mr.activo = :estado_general'),
                    $qb->expr()->in(
                        'me.id',
                        $qb1->getDQL()
                    )
                )
            )
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'menu_id' => $id
            ]);

        $query=$qb->getQuery();
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

}
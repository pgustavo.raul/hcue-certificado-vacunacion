<?php

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;

class AuditoriaRepository extends EntityRepository{

    /**
     * Obtiene el registro de auditoria deseado con todas sus relaciones
     * @param integer $id
     * @return object de tipo "Core\SeguridadBundle\Entity\Auditoria"
     */
    public function getAuditoria($id) {
        $qb = $this->createQueryBuilder('a')
            ->select('a,mr,up,us,p,m,ap')
            ->leftJoin('a.menurol', 'mr')
            ->leftJoin('mr.menu', 'm')
            ->leftJoin('m.aplicacion', 'ap')
            ->innerJoin('a.usuarioperfil', 'up')
            ->innerJoin('up.usuario', 'us')
            ->innerJoin('us.persona', 'p')
            ->where('a.id = :id')
            ->setParameter('id', $id);
        $query = $qb->getQuery()->useQueryCache(true);
        return $query->getOneOrNullResult();
    }

    /**
     * Obtiene el registro de una version con sus relaciones
     * @param integer $registro_id
     * @param integer $version
     * @param string $tabla
     * @return object de tipo "Core\SeguridadBundle\Entity\Auditoria"
     */
    public function getVersion($registro_id, $version, $tabla) {
        $qb = $this->createQueryBuilder('a')
            ->select('a,mr,m,ap')
            ->leftJoin('a.menurol', 'mr')
            ->leftJoin('mr.menu', 'm')
            ->leftJoin('m.aplicacion', 'ap')
            ->where('a.idregistro = :registro_id')
            ->andWhere('a.version = :version')
            ->andWhere('a.tabla = :tabla')
            ->setParameters([
                'registro_id' => $registro_id,
                'version' => $version,
                'tabla' => $tabla
            ]);
        $query = $qb->getQuery()->useQueryCache(true);
        return $query->getOneOrNullResult();
    }

    /**
     * Retorna el secuencial de la nueva version para el registro
     * @param int $registro_id
     * @return int Nueva version
     */
    public function getNextVersion($registro_id){
        $version=1;
        $maxversion=  $this->createQueryBuilder('a')
            ->select('MAX(a.version) as maxversion')
            ->where('a.idregistro = :registro_id')
            ->setParameter('registro_id',$registro_id)
            ->groupBy('a.idregistro')
            ->getQuery()->getOneOrNullResult();
        if($maxversion){
            $version=$maxversion['maxversion']+1;
        }
        return $version;

    }

    /**
     * Retorna las versiones de un determinado registro
     * @param integer $id Id de la auditoria
     * @param integer $registro_id Id del registro en auditoria
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return QueryBuilder
     */
    public function getVersiones($id, $registro_id, $tabla, $arrayResult=false){

        $qb= $this->createQueryBuilder('a')
            ->select('a.id, a.version')
            ->where('a.idregistro = :registro_id')
            ->andWhere('a.id! = :auditoria_id')
            ->andWhere('a.tabla = :tabla')
            ->orderBy('a.version', 'ASC')
            ->setParameters([
                'auditoria_id' => $id,
                'registro_id' => $registro_id,
                'tabla' => $tabla
            ]);

        $query = $qb->getQuery()->useQueryCache(true);
        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

}
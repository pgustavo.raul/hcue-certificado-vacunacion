<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

class UsuarioperfilRepository extends EntityRepository{

    /**
     * Obtiene la lista de las relaciones UsuarioPerfiles
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @param integer @estado para retornar los perfiles asignados a un usuario de acuerdo a su estado
     * @return Array Lista de Objetos de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function getUsuariosperfiles($estado=0, $arrayResult=false) {

        $qb = $this->getUsuarioperfilQueryBuilder($estado);
        $query = $qb->getQuery()->useQueryCache(true);

        return ($arrayResult)?$query->getArrayResult():$query->getResult();

    }

    /**
     * Obtiene el QueryBuilder de la lista de aplicaciones
     * @return QueryBuilder
     */
    public function getUsuarioperfilQueryBuilder($estado="") {
        $qb = $this->createQueryBuilder('up')
            ->select('up');

        if(!empty($estado) && is_numeric($estado)){
            $qb->andWhere('up.estado = :estado')
                ->setParameter('estado', $estado);
        }

        return $qb;
    }

    /**
     * Obtiene el QueryBuilder de los perfiles que tiene un usuario por aplicacion
     * @param integer $usuario_id Id del Usuario
     * @param integer $aplicacion_id Id de la aplicación
     * @return QueryBuilder
     */
    public function usuarioPerfilQueryBuilder($usuario_id, $aplicacion_id=0) {
        $qb = $this->createQueryBuilder('up')
            ->leftJoin('up.perfilentidad', 'pe', 'WITH', 'pe.estado=:estado_general and pe.activo=:estado_general')
            ->innerJoin('pe.perfil', 'p', 'WITH', 'p.estado=:estado_general and p.activo=:estado_general')
            ->innerJoin('pe.entidad', 'e', 'WITH', 'e.estado=:estado_general and e.activo=:estado_general')
            ->where('up.usuario_id = :usuario_id')
            ->andWhere('up.estado = :estado_general')
            ->andWhere('up.activo = :estado_general')
            ->orderBy('up.id', 'ASC')
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'usuario_id' => $usuario_id
            ]);

        if(!empty($aplicacion_id) && is_numeric($aplicacion_id)){
            $qb->from('CoreSeguridadBundle:Perfilrol','pr')
                ->leftJoin('pr.rol', 'r')
                ->andWhere('pr.perfil_id = p.id')
                ->andWhere('r.aplicacion_id = :aplicacion_id')
                ->andWhere('pr.activo = :estado_general')
                ->andWhere('pr.estado = :estado_general')
                ->setParameter('aplicacion_id', $aplicacion_id);
        }

        return $qb;
    }

    /**
     * Retorna todos los perfiles asociados a un usuario
     * @param integer $usuario_id Id del Usuario
     * @param integer $aplicacion_id Id de la aplicación
     * @return Array de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function getUsuarioPerfiles($usuario_id, $aplicacion_id=0) {

        $qb = $this->usuarioPerfilQueryBuilder($usuario_id, $aplicacion_id);
        $qb->select('up,pe,p,e');
        $query = $qb->getQuery()->useQueryCache(true);
        return $query->getResult();

    }

    /**
     * Verifica si el usuario tiene asignado al menos un perfil
     * @param integer $usuario_id Id del Usuario
     * @param integer $aplicacion_id Id de la aplicación
     * @return boolean <true> si tiene un perfil asignado
     */
    public function hasUsuarioPerfil($usuario_id, $aplicacion_id=0) {

        $qb = $this->usuarioPerfilQueryBuilder($usuario_id, $aplicacion_id);
        $qb->select('count(distinct up.id)');
        $query = $qb->getQuery()->useQueryCache(true);
        return $query->getSingleScalarResult();

    }

    /**
     * Retorna el primer registro como default
     * @param integer $usuario_id Id del Usuario
     * @param integer $aplicacion_id Id de la aplicación
     * @param boolean $all <true> para retornar el objeto con todas sus relaciones
     * @return Objeto de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function firstUsuarioPerfil($usuario_id, $aplicacion_id=0, $all=true) {

        $qb = $this->usuarioPerfilQueryBuilder($usuario_id, $aplicacion_id);

        if($all) {
            $qb->select('up,u,per,pe,e,p,c,d,z,pa,ca,pro,te')
                ->innerJoin('up.usuario', 'u', 'WITH', 'u.activo=:estado_general')
                ->innerJoin('u.persona', 'per', 'WITH', 'per.estado=:estado_general')
                ->innerJoin('e.circuito', 'c', 'WITH', 'c.estado=:estado_general')
                ->leftJoin('e.tipoestablecimiento', 'te', 'WITH', 'te.estado=:estado_general')
                ->innerJoin('c.distrito', 'd', 'WITH', 'd.estado=:estado_general')
                ->innerJoin('d.zona', 'z', 'WITH', 'z.estado=:estado_general')
                ->leftJoin('e.parroquia', 'pa', 'WITH', 'pa.estado=:estado_general')
                ->leftJoin('pa.canton', 'ca', 'WITH', 'ca.estado=:estado_general')
                ->leftJoin('ca.provincia', 'pro', 'WITH', 'pro.estado=:estado_general');
        }
        else {
            $qb->select('up');
        }

        $query = $qb->getQuery()->useQueryCache(true);

        return $query->setMaxResults(1)->getOneOrNullResult();

    }

    /**
     * Obtiene los Perfiles asociados a un Usuario
     * @param string $usuario_id Id del Usuario
     * @param boolean $arrayResult <true> para retornar el modelo o entidad como un array
     * @param boolean $perfilDefault <true> para retornar el primer perfil como default
     * @return Array u Objeto de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function getPerfilesporEntidad($usuario_id, $perfiles, $perfil_id, $estado=true) {
        $qb = $this->createQueryBuilder("up");

        $qb->leftJoin('up.perfilentidad','pe','WITH', 'pe.estado=:activo and pe.activo=:activo')
            ->Where('up.activo = :activo')
            ->andWhere('up.usuario_id = :usuario_id')
            ->andWhere(
                $qb->expr()->in(
                    'pe.perfil_id',
                    implode(",", $perfiles)
                )
            )
            ->andWhere(
                $qb->expr()->neq('pe.perfil_id', ':perfil_id')
            )
            ->orderBy('up.id', 'ASC')
            ->setParameters([
                'activo' => Constantes::ESTADOGENERALHABILITADO,
                'usuario_id' => $usuario_id,
                'perfil_id' => $perfil_id
            ]);

        if($estado){
            $qb->andWhere('up.estado = :estado')
                ->setParameter('estado', Constantes::ESTADOGENERALHABILITADO);
        }

        return $qb;

    }

    /**
     * Obtiene el registro usuarioperfilseleccionado
     * @param integer $id
     * @return Objeto de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function getUsuarioperfil($id) {

        $qb = $this->createQueryBuilder('up')
            ->select('up,u,per,pe,e,p,c,d,z,pa,ca,pr,te')
            ->innerJoin('up.usuario', 'u', 'WITH', 'u.activo=:estado_general')
            ->innerJoin('u.persona', 'per', 'WITH', 'per.estado=:estado_general')
            ->innerJoin('up.perfilentidad', 'pe', 'WITH', 'pe.estado=:estado_general')
            ->innerJoin('pe.entidad', 'e', 'WITH', 'e.estado=:estado_general')
            ->innerJoin('pe.perfil', 'p', 'WITH', 'p.estado=:estado_general and p.activo=:estado_general')
            ->innerJoin('e.circuito', 'c', 'WITH', 'c.estado=:estado_general')
            ->leftJoin('e.tipoestablecimiento', 'te', 'WITH', 'te.estado=:estado_general')
            ->innerJoin('c.distrito', 'd', 'WITH', 'd.estado=:estado_general')
            ->innerJoin('d.zona', 'z', 'WITH', 'z.estado=:estado_general')
            ->leftJoin('e.parroquia', 'pa', 'WITH', 'pa.estado=:estado_general')
            ->leftJoin('pa.canton', 'ca', 'WITH', 'ca.estado=:estado_general')
            ->leftJoin('ca.provincia', 'pr', 'WITH', 'pr.estado=:estado_general')
            ->Where('up.estado = :estado_general')
            ->andWhere('up.activo = :estado_general')
            ->andWhere('up.id = :id')
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'id' => $id
            ]);

        $query = $qb->getQuery()->useQueryCache(true);
        return $query->getOneOrNullResult();

    }

}
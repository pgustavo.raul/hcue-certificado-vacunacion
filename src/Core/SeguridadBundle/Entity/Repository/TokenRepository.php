<?php

/*
* @autor Luis Malquin
*/

namespace Core\SeguridadBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Core\SeguridadBundle\Entity\Constantes;

class TokenRepository extends EntityRepository{

    /**
     * Valida el token general
     * @param string $token
     * @return QueryBuilder
     */
    public function validToken($token){
        $tiempoactual=new \DateTime();
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->where("t.token=:token")
            ->andWhere(":fechaactual>=t.fechacreacion")
            ->andWhere(":fechaactual<=t.fechaexpiracion")
            ->andWhere("t.estado=:estado")
            ->setParameters(
                array(
                    "token" => $token,
                    "fechaactual" => $tiempoactual,
                    "estado"=>Constantes::ESTADOGENERALHABILITADO
                ));

        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    /**
     * Valida el token para una aplicacion
     * @param string $token
     * @param integer $app_id
     * @return QueryBuilder
     */
    public function validTokenAplicacion($token,$app_id) {
        $tiempoactual=new \DateTime();
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->where("t.token=:token")
            ->andWhere("t.aplicacion_id=:app_id")
            ->andWhere(":fechaactual>=t.fechacreacion")
            ->andWhere(":fechaactual<=t.fechaexpiracion")
            ->andWhere("t.estado=:estado")
            ->setParameters(
                array("token" => $token,
                    "app_id" => $app_id,
                    "fechaactual" => $tiempoactual,
                    "estado" => Constantes::ESTADOGENERALHABILITADO
                ));

        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

}
<?php

namespace Core\SeguridadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_sistema.rol")
 * @ORM\Entity(repositoryClass="Core\SeguridadBundle\Entity\Repository\RolRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Rol{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_sistema.rol_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $aplicacion_id
     * @ORM\Column(name="aplicacion_id", type="integer", nullable=true)
     **/
    private $aplicacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Aplicacion")
     * @ORM\JoinColumn(name="aplicacion_id", referencedColumnName="id", nullable=false)
     * */
    private $aplicacion;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=100, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo nombre")
     **/
    private $nombre;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=200, nullable=true)
     **/
    private $descripcion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var boolean $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set aplicacion_id
     * @param integer aplicacion_id
     * @return rol
     **/
    public function setAplicacionId($aplicacion_id){
        $this->aplicacion_id = $aplicacion_id;
        return $this;
    }

    /**
     * Get aplicacion_id
     * @return integer
     **/
    public function getAplicacionId(){
        return $this->aplicacion_id;
    }

    /**
     * Set aplicacion
     * @param \Core\SeguridadBundle\Entity\Aplicacion $aplicacion
     * @return rol
     * */
    public function setAplicacion(\Core\SeguridadBundle\Entity\Aplicacion $aplicacion) {
        $this->aplicacion = $aplicacion;
        return $this;
    }

    /**
     * Get aplicacion
     * @return \Core\SeguridadBundle\Entity\Aplicacion
     * */
    public function getAplicacion() {
        return $this->aplicacion;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return rol
     **/
    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     **/
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return rol
     **/
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return rol
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return aplicacion
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set estado
     * @param integer estado
     * @return rol
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return aplicacion
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get estado activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
        $this->usuariocreacion_id=1;
    }
    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
        $this->usuariomodificacion_id=1;
    }

}
var FANCY_GLYPH_OPTIONS={
    map: {
      //doc: "glyphicon glyphicon-file",
      //docOpen: "glyphicon glyphicon-file",
      checkbox: "glyphicon glyphicon-unchecked",
      checkboxSelected: "glyphicon glyphicon-check",
      checkboxUnknown: "glyphicon glyphicon-share",
      dragHelper: "glyphicon glyphicon-play",
      dropMarker: "glyphicon glyphicon-arrow-right",
      error: "glyphicon glyphicon-warning-sign",
      expanderClosed: "glyphicon glyphicon-menu-right",
      expanderLazy: "glyphicon glyphicon-menu-right",  
      expanderOpen: "glyphicon glyphicon-menu-down",  
      //expanderLazy: "glyphicon glyphicon-plus-sign",  // 
      //expanderOpen: "glyphicon glyphicon-collapse-down",  // 
      folder: "glyphicon glyphicon-folder-close",
      folderOpen: "glyphicon glyphicon-folder-open",
      loading: "glyphicon glyphicon-refresh glyphicon-spin"
    }

};


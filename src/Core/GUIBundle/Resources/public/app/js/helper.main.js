/**
 * Archivo de definición de ayudante de la aplicacion
 * Requiere:
 *      -jQuery
 *      -jquery.includeMany-1.2.2.js
 * Fecha de creacion: 04/08/2016
 * Fecha de modificación: 04/08/2016
 * Creado por: Luis Malquin (luisor_mlt@hotmail.com)
 * Modificado por: Luis Malquin
 **/

//Direccion base del app
var base_path = Routing.getBaseUrl().replace(/\w+\.php$/gi,'')+'/';

//alias para inclusion dinamica de archivos js, css   
if ($.configIncludeFile) {

    $.configIncludeFile.urls = {
        "jquery.datatables": [
            base_path + "bundles/coregui/datatables/js/datatables.bootstrap.min.js",
            base_path + "bundles/coregui/datatables/css/datatables.bootstrap.min.css"
        ],
        "jquery.jqGrid":[
            base_path + "bundles/coregui/jqGrid/css/ui.jqgrid.min.css",
            base_path + "bundles/coregui/jqGrid/js/jquery.jqGrid.min.js",
            base_path + "bundles/coregui/jqGrid/js/grid.locale-es.js"
        ],
        "jquery-ui.touch-punch":base_path + "bundles/coregui/jquery/js/jquery.ui.touch-punch.min.js",
        "jquery.easypiechart": base_path + "bundles/coregui/jquery/js/jquery.easypiechart.min.js",
        "jquery.sparkline":base_path + "bundles/coregui/jquery/js/jquery.sparkline.index.min.js",
        "jquery.flot":[
            base_path + "bundles/coregui/flot/js/jquery.flot.min.js",
            base_path + "bundles/coregui/flot/js/jquery.flot.pie.min.js",
            base_path + "bundles/coregui/flot/js/jquery.flot.resize.min.js",
            base_path + "bundles/coregui/flot/js/jquery.flot.categories.min.js"
        ],
        "prettify":[
            base_path + "bundles/coregui/jquery/css/prettify.min.css",
            base_path + "bundles/coregui/jquery/js/prettify.min.js"
        ],
        "jquery.gritter":[
            base_path + "bundles/coregui/jquery/css/jquery.gritter.min.css",
            base_path + "bundles/coregui/jquery/js/jquery.gritter.min.js"
        ],
        "jquery.knob":base_path + "bundles/coregui/jquery/js/jquery.knob.min.js",
        "jquery.inputlimiter":base_path + "bundles/coregui/jquery/js/jquery.inputlimiter.min.js",
        "jquery.maskedinput":base_path + "bundles/coregui/jquery/js/jquery.maskedinput.min.js",
        "jquery.chosen":[
            base_path + "bundles/coregui/jquery/css/chosen.min.css",
            base_path + "bundles/coregui/jquery/js/chosen.jquery.min.js"
        ],
        "autosize":base_path + "bundles/coregui/jquery/js/autosize.min.js",
        "spin":base_path + "bundles/coregui/jquery/js/spin.js",
        "spinbox":base_path + "bundles/coregui/jquery/js/spinbox.min.js",
        "moment":[
            base_path + "bundles/coregui/jquery/js/moment.min.js",
            base_path + "bundles/coregui/jquery/js/localization/moment.es.js"
        ],
        "bootbox":base_path + "bundles/coregui/jquery/js/bootbox.js",
        "treeview":base_path + "bundles/coregui/jquery/js/tree.min.js",
        "fancytree":[
            base_path + "bundles/coregui/fancytree/css/ui.fancytree-lion.min.css",
            base_path + "bundles/coregui/fancytree/js/jquery.fancytree-all.min.js"
        ],
        "fancytree-bootstrap":[
            base_path + "bundles/coregui/fancytree/css/ui.fancytree.min.css",
            base_path + "bundles/coregui/fancytree/js/jquery.fancytree-all.min.js",
            base_path + "bundles/coregui/fancytree/js/fancytree-options-bootstrap.js"
        ],
        "jquery.nestable":base_path + "bundles/coregui/jquery/js/jquery.nestable.min.js",
        "jquery.raty":base_path + "bundles/coregui/jquery/js/jquery.raty.min.js",
        "jquery.typeahead":base_path + "bundles/coregui/jquery/js/jquery-typeahead.js",
        "jquery.select2":[
            base_path + "bundles/coregui/jquery/css/select2.min.css",
            base_path + "bundles/coregui/jquery/js/select2.min.js"
        ],
        "jquery.wizard": base_path + "bundles/coregui/jquery/js/wizard.min.js",
        "jquery.validate":[
            base_path + "bundles/coregui/jquery/js/jvalidate/jquery.validate.min.js",
            base_path + "bundles/coregui/jquery/js/jvalidate/jquery-additional-methods.min.js",
            base_path + "bundles/coregui/app/js/jvalidate.additional-methods.app.min.js",
            base_path + "bundles/coregui/jquery/js/localization/validate.messages_es.js"
        ],
        "jquery.maskMoney":base_path + "bundles/coregui/jquery/js/jquery.maskMoney.js",
        "jquery.alphanum":base_path + "bundles/coregui/jquery/js/jquery.alphanum.js",

        "themecss":base_path + "bundles/coregui/themes/ace-admin/css/ace.min.css",
        "daterangepicker":[
            base_path + "bundles/coregui/jquery/css/daterangepicker.min.css",
            base_path + "bundles/coregui/jquery/js/daterangepicker.min.js"
        ],
        "bootstrap.datepicker":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-datepicker3.min.css",
            base_path + "bundles/coregui/bootstrap/js/bootstrap-datepicker.min.js",
            base_path + "bundles/coregui/bootstrap/js/locales/bootstrap-datepicker.es.js"
        ],
        "bootstrap.datetimepicker":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-datetimepicker.min.css",
            base_path + "bundles/coregui/bootstrap/js/bootstrap-datetimepicker.min.js"
        ],
        "bootstrap.timepicker":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-timepicker.min.css",
            base_path + "bundles/coregui/bootstrap/js/bootstrap-timepicker.min.js"
        ],

        "bootstrap.multiselect":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-multiselect.min.css",
            base_path + "bundles/coregui/bootstrap/js/bootstrap-multiselect.min.js"
        ],
        "bootstrap.colorpicker":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-colorpicker.min.css",
            base_path + "bundles/coregui/bootstrap/js/bootstrap-colorpicker.min.js"
        ],
        "bootstrap.editable":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-editable.min.css",
            base_path + "bundles/coregui/bootstrap/js/bootstrap-editable.min.js"
        ],
        "bootstrap.duallistbox":[
            base_path + "bundles/coregui/bootstrap/css/bootstrap-duallistbox.min.css",
            base_path + "bundles/coregui/bootstrap/js/jquery.bootstrap-duallistbox.min.js"
        ],

        "bootstrap.tag":[
            base_path + "bundles/coregui/bootstrap/js/bootstrap-tag.min.js"
        ],
        "modalForm":[
            base_path + "bundles/coregui/app/js/modal.form.min.js"
        ],
        "modalViewer":[
            base_path + "bundles/coregui/app/js/modal.viewer.min.js"
        ],
        "jquery.pschecker":[
            base_path + "bundles/coregui/jquery/css/pschecker.css",
            base_path + "bundles/coregui/jquery/js/pschecker.js"
        ],
        "jquery.jbox":[
            base_path + "bundles/coregui/jBox/css/jBox.min.css",
            base_path + "bundles/coregui/jBox/css/TooltipBorder.css",
            base_path + "bundles/coregui/jBox/js/jBox.min.js"
        ],
        "footable":[
            base_path + "bundles/coregui/footable/css/footable.bootstrap.css",
            base_path + "bundles/coregui/footable/js/footable.js"
        ],
        "table.navigation": base_path + "bundles/coregui/jquery/js/jquery.table_navigation.js",
        "echarts":[
            base_path + "bundles/coregui/echarts/css/echarts.css",
            base_path + "bundles/coregui/echarts/js/echarts-all.js",
            base_path + "bundles/coregui/echarts/js/utils.min.js"
        ],
        "echarts-3":[
            base_path + "bundles/coregui/echarts/js/echarts-all-3.js",
        ],
        "dropzone":[
            base_path + "bundles/coregui/jquery/css/dropzone.min.css",
            base_path + "bundles/coregui/jquery/js/dropzone.min.js"
        ]
    }
}

//Abre una nueva ventana 
$.openWindow = function(url, type, title, w, h) {
    var newwin = '';
    if (url != '') {
        if (type == 'popup') {

            if (!w)
                w = 570;
            if (!h)
                h = 440;
            var Xpos = (screen.width / 2) - (w / 2);
            var Ypos = (screen.height / 2) - (h / 2);
            newwin = window.open(url, title, 'width=' + w + ',height=' + h + ',scrollbars=yes,left=' + Xpos + ',top=' + Ypos);
            return newwin;
        } else if (type == 'blank') {
            newwin = window.open(url, '_blank');
            return newwin;
        } else {
            document.location.href = url;
        }
    }

};

//Abre una nueva ventana y envia datos por post
$.openWindowWithPost=function(url, type, title, w, h, params)
{
    var newwin = null;
    if (url != '') {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", url);
        form.setAttribute("target", (type=='blank')?'_blank':title);
        for (var i in params)
        {
            if (params.hasOwnProperty(i))
            {   var input=null;
                if ($.isPlainObject(params[i])){
                    var objects = params[i];
                    for (var j in objects) {
                        input = document.createElement('input');
                        input.type = 'hidden';
                        input.name = i+'['+j+']';
                        input.value = objects[j];
                        form.appendChild(input);
                    }
                }
                else {
                    input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }

            }
        }
        document.body.appendChild(form);

        if (type == 'popup') {

            if (!w)
                w = 570;
            if (!h)
                h = 440;
            var Xpos = (screen.width / 2) - (w / 2);
            var Ypos = (screen.height / 2) - (h / 2);
            newwin = window.open('about:blank', title, 'width=' + w + ',height=' + h + ',scrollbars=yes,left=' + Xpos + ',top=' + Ypos);

        }
        form.submit();
        document.body.removeChild(form);
    }
    return newwin;
};

//plugin que verifica si existe elementos segun el seletor
jQuery.fn.exists = function(callback) {
    if (this.length > 0 && callback != undefined)
        callback();
    return this.length;
};

//Verifica si existe elementos segun el selector e incluye archivos js y css por medio de un alias
$.fn.includeFileExistObjects = function(aliasname, callback) {
    $(this).exists(function() {
        $.include_once(aliasname, callback);
    });
};


//definimos la funcion para mostrar una ventana modal de confirmación
$.confirm=function(message,callback){
    $.include_once('bootbox',function(){

        bootbox.setDefaults({locale:'es'});
        bootbox.confirm(
            {
                title:'Confirmar',
                message:message,
                callback: callback
            });


    });
};

//definimos la funcion para mostrar una ventana modal de alerta
$.alert=function(message,type,callback){
    $.include_once('bootbox',function(){

        bootbox.setDefaults({locale:'es'});

        if(type !=undefined){
            if(type=='danger')message='<i class="fa fa-exclamation-triangle " aria-hidden="true"></i> '+message;
            if(type=='success')message='<i class="fa fa-check " aria-hidden="true"></i> '+message;
            if(type=='warning')message='<i class="fa fa-exclamation " aria-hidden="true"></i> '+message;
            if(type=='info')message='<i class="fa fa-info-circle " aria-hidden="true"></i> '+message;

            message='<div class="alert alert-'+type+'">'+message+'</div>';
        }

        bootbox.alert(
            {
                title:'Alerta!',
                message:message,
                callback: callback
            });


    });
};

//ventana modal de barra de progreso, de proceso en ejecución
var _processWait=null;
$.processWait=_processWait||(function(){
        var divWait=$("<div>",{'class':'modal fade','data-backdrop':'static','date-keyboard':'false','tabindex':'-1','role':'dialog','aria-hidden':"true"})
            .css({'overflow-y':'visible', 'z-index': 2000});
        var modaldialog=$('<div>',{'class':'modal-dialog'});
        var modalcontent=$('<div>',{'class':'modal-content'});
        var modalheader=$('<div>',{'class':'modal-header'});
        var modalbody=$('<div>',{'class':'modal-body'});
        var processbar=$('<div>',{'class':'progress progress-striped active'})
        //haader
        modalheader.append('<h4 class="blue bigger"><i class="fa fa-clock-o"></i> Procesando, por favor espere...</h4>');

        //body
        processbar.html('<div class="progress-bar progress-bar-yellow" style="width: 100%;"></div>');
        modalbody.html(processbar);

        //modal content

        modalcontent.html(modalheader).append(modalbody);
        //modal dialog
        modaldialog.html(modalcontent);
        divWait.html(modaldialog);
        divWait.keydown(function(e){
            //alert(e.keyCode)
            if (e.keyCode == 27) {
                return false;
            }
        });


        _processWait={
            show:function(){
                divWait.modal('show');
            },
            hide:function(){
                divWait.modal('hide');
            }
        };
        return _processWait;
    })();

//funcion para mostrar un modal con contenido por carga.
$.loadModalViewer=function(config){
    var defaults={
        id:'',
        url:'',
        title:'',
        autoOpen:true,
        //ancho del modal
        width:'',
        //alto del modal
        height:''
    };
    config=$.extend({},defaults,config);
    return $.include_once('modalViewer',function(){
        var dView=$('<div>',{id:(config.id)?config.id:''});
        var mView=$(dView).modalViewer({
            url:config.url,
            title:config.title,
            autoOpen:config.autoOpen,
            width:config.width,
            height:config.height
        });
        return mView;
    });
};
/**
 * Archivo de definición de utilidades
 * Requiere:
 *      -jQuery
 * Fecha de creacion: 04/08/2016
 * Fecha de modificación: 04/08/2016
 * Creado por: Luis Malquin (luisor_mlt@hotmail.com)
 * Modificado por: Luis Malquin
 **/

//Obtiene la ruta absoulta de la ubicacion actual en la app
$.getAbsolutePath = function () {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
};

//Elimina los espacios de los extremos de una cadena
$.trim = function (string)
{
    var str = '';
    if (string != null) {
        str = string.replace(/^\s+/g, '');
        str = str.replace(/\s+$/g, '');
    }
    return str;
};

//Elimina el etiquetas HTML de una cadena
$.stripHTML = function (string)
{
    return string.replace(/<[^>]+>/g, '');
};

//Obtiene la diferencia de dias entre una fecha inicial y final
$.getDiferenceDays = function (date1, date2) {
    var d1 = date1.split('-');
    var dat1 = new Date(d1[0], parseFloat(d1[1]) - 1, parseFloat(d1[2]));
    var d2 = date2.split('-');
    var dat2 = new Date(d2[0], parseFloat(d2[1]) - 1, parseFloat(d2[2]));
    // alert(dat2);
    var end = dat2.getTime() - dat1.getTime();
    var days = Math.floor(end / (1000 * 60 * 60 * 24))
    return days;
};

//Obtiene el ultimo dia del mes
$.lastDay = function dias(month, year) {
    month = parseInt(month);
    year = parseInt(year);
    switch (month) {
        case 1 :
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            return 31;
        case 2 :
            return (year % 4 == 0) ? 29 : 28;
    }
    return 30;
};

//Transforma un string a formato Date
$.stringToDate = function (date, format, delimiter)
{
    var formatLowerCase = format.toLowerCase();
    var formatItems = formatLowerCase.split(delimiter);
    var dateItems = date.split(delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]) - 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    return formatedDate;
};

//Valida si una fecha es válida
$.validDate = function (date, format) {
    if (date != undefined && date != "") {
        if (format == undefined || format == "") {
            format = "yyyy-mm-dd";
        }
        date = date.replace(new RegExp("/", "g"), '-');
        format = format.replace(new RegExp("/", "g"), '-');
        var d = format.split("-");
        var expresion = /^\d{x}\-\d{y}\-\d{z}$/;
        expresion = expresion.toString().replace("x", d[0].length);
        expresion = expresion.toString().replace("y", d[1].length);
        expresion = expresion.toString().replace("z", d[2].length);
        expresion = new RegExp(expresion.toString().replace(new RegExp("/", "g"), ""));
        if (!expresion.test(date)) {
            return false;
        }
        date = $.stringToDate(date, format, "-");
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getYear() + 1900;
        var numDays = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numDays = 30;
                break;
            case 2:
                if ($.leapYear(year)) {
                    numDays = 29
                } else {
                    numDays = 28
                }
                break;
            default:
                return false;
        }

        if (day > numDays || day == 0) {
            return false;
        }
        return true;
    }

    return false;
};

//Verifica si es año biciesto
$.leapYear = function (year) {
    var response=false;
    if ( (year%4==0) &&((year % 100 != 0) || (year % 400 == 0))) {
        response=true;
    }
    return response;
};

//Suma o resta dias a una fecha
$.operationDaysDate = function (date, format, nmonth, ndays, operation) {
    if (ndays == "")
        ndays = 0;
    if (nmonth == "")
        nmonth = 0;
    if (operation == "")
        operation = "+";
    var ndate = false;
    if (format == undefined || format == "") {
        format = "yyyy-mm-dd";
    }
    if ($.validDate(date, format)) {
        date = date.replace(new RegExp("/", "g"), '-');
        format = format.replace(new RegExp("/", "g"), '-');
        date = $.stringToDate(date, format, "-");
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getYear() + 1900;
        var nummonth = month + parseFloat(nmonth);
        var numdays = day + parseFloat(ndays);
        ndate = new Date(year, nummonth, numdays);
    }
    return ndate;
};

//da formato moneda a un número
$.formatMoney = function (number, prefix, decimals, decimal_sep, thousands_sep)
{
    var n = number,
        c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
        d = decimal_sep || '.', //if no decimal separetor is passed we use the comma as default decimal separator (we MUST use a decimal separator)

        p = (typeof prefix === 'undefined') ? '' : prefix,
        t = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, //if you don't want ot use a thousands separator you can pass empty string as thousands_sep value

        sign = (n < 0) ? '-' : '',
        //extracting the absolute value of the integer part of the number and converting to string
        i = parseInt(n = Math.abs(n).toFixed(c)) + '',
        j = ((j = i.length) > 3) ? j % 3 : 0;
    return p + sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
};

//Redondea un valor némerico
$.roundValue = function (value, decimals) {

    var nvalue = parseFloat(value);
    var tennumber = Math.pow(10, decimals);
    var result = Math.round(nvalue * tennumber) / tennumber;
    return result;
};

//Calcula el valor en base a un porcentaje
$.percentage = function (base, porc) {
    var resp = 0;
    if (!isNaN(parseFloat(base)) && !isNaN(parseFloat(porc)))
        resp = parseFloat(base) * parseFloat(porc) / 100;
    return Math.round(resp * 100) / 100;
};

//Busca y reemplaza un valor en una cadena
$.str_replace = function (search, replace, subject, count) {

   var j = 0,
        temp = '',
        repl = '',
        i=0,
        sl = 0, fl = 0,
        f = [].concat(search),
        r = [].concat(replace),
        s = subject,
        sa = s instanceof Array;
    s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }
    for (sl = s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j = 0, fl = f.length; j < fl; j++) {
            temp = s[i] + '';
            //repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            repl = (r[j] !== undefined) ? r[j] : '';
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length - s[i].length) / f[j].length;
            }
        }
    }
    return sa ? s : s[0];
};

//Obtiene la edad dando la fecha de nacimiento
$.getAge = function (date, format) {
    if (format == undefined || format == "") {
        format = "yyyy-mm-dd";
    }
    date = date.replace(new RegExp("/", "g"), '-');
    date = $.stringToDate(date, format, "-");
    var dia = date.getDate();
    var mes = date.getMonth() + 1;
    var anio = date.getFullYear();

    //calculo la ddate de hoy
    var fecha_hoy = new Date();
    var ahora_ano = fecha_hoy.getFullYear();
    var ahora_mes = fecha_hoy.getMonth() + 1;
    var ahora_dia = fecha_hoy.getDate();
    
   // realizamos el calculo
        var edad = (ahora_ano + 1900) - anio;
        if (ahora_mes < mes) {
            edad--;
        }
        if ((mes == ahora_mes) && (ahora_dia < dia)) {
            edad--;
        }
        if (edad >= 1900) {
            edad -= 1900;
        }

        // calculamos los meses
        var meses = 0;

        if (ahora_mes > mes && dia > ahora_dia)
            meses = ahora_mes - mes - 1;
        else if (ahora_mes > mes)
            meses = ahora_mes - mes
        if (ahora_mes < mes && dia < ahora_dia)
            meses = 12 - (mes - ahora_mes);
        else if (ahora_mes < mes)
            meses = 12 - (mes - ahora_mes + 1);
        if (ahora_mes == mes && dia > ahora_dia)
            meses = 11;

        // calculamos los dias
        var dias = 0;
        if (ahora_dia > dia)
            dias = ahora_dia - dia;
        if (ahora_dia < dia) {
            var ultimoDiaMes = new Date(ahora_ano, ahora_mes - 1, 0);
            dias = ultimoDiaMes.getDate() - (dia - ahora_dia);
        }
       return {anios: edad, meses:meses, dias:dias};  

};

//Retorna la fecha nacimiento dando la edad en años,meses y dias
$.getBirthDate = function (anios, meses, dias) {
    if (meses == '' || meses == undefined)
        meses = 0;
    if (dias == '' || dias == undefined)
        dias = 0;

    //obtenemos la ddate de hoy
    var fecha_hoy = new Date();
    var ahora_ano = fecha_hoy.getYear();
    var ahora_mes = fecha_hoy.getMonth() + 1;
    var ahora_dia = fecha_hoy.getDate();

    // calculamos la fecha del año
    var fechaanios = (ahora_ano + 1900) - anios;

    // calculamos la fecha del meses
    var fechameses = 0;

    if (ahora_mes > meses)
        fechameses = ahora_mes - meses;

    if (ahora_mes < meses)
        fechameses = 12 - (meses - ahora_mes);

    if (ahora_mes == meses && dias > ahora_dia)
        fechameses = 11;

    // calculamos la fecha del dia
    var fechadias = 0;

    if (ahora_dia > dias)
        fechadias = ahora_dia - dias;

    if (ahora_dia < dias)
        fechadias = dias - ahora_dia;

    return new Date(fechaanios, fechameses, fechadias);

};
/**
 * Archivo de definición de ayudante para el datatables 
 * Requiere:
 *      -jQuery       
 *      -jquery.includeMany-1.2.2.js
 *      -helper.main.js
 *      -helper.form.js 
 * Fecha de creacion: 04/08/2016
 * Fecha de modificación: 04/08/2016       
 * Creado por: Luis Malquin (luisor_mlt@hotmail.com)
 * Modificado por: Luis Malquin
 **/

//funcion para mostrar un formulario de edicion modal de un registro de datatables
$.loadModalFormDatatables=function(config){
           var dafaults={
               id:'',
               url:'',
               title:'',
               urlSubmit:'',
               grid:null,
               autoOpen:true,
               width:'',
               height:''
           };
           
           config=$.extend({},dafaults,config);
           
           return $.include_once('modalForm',function(){                           
                var divForm=$('<div>',{id:(config.id)?config.id:''});
                var mForm=$(divForm).modalForm({
                    url:config.url,                                
                    title:config.title,
                    urlSubmit:config.urlSubmit,
                    grid:config.grid,
                    autoOpen:config.autoOpen,
                    width:config.width,
                    height:config.height,
                });       
                return mForm;
           }); 
}


//funcion ajax para la peticion de eliminacion de un registro desde el datatables
$.ajaxDeleteRowDatatables=function(config){
    var defaults={
       url:'',
       data:{},
       async:false,
       grid:null              
    };
    config=$.extend({},defaults,config);
    $.ajax({
       url: config.url,
       async:config.async,
       beforeSend:function(){
           $.processWait.show();
       },
       success:function(response){
           $.processWait.hide();
           if(response.status){
                var objGrid=null;
                if(typeof(config.grid)=='string'){
                    objGrid=$(config.grid).dataTable();
                }else if(typeof(config.grid)=='object'){
                    objGrid=config.grid;
                }
                if(objGrid){
                    objGrid.ajax.reload();
                }    
                
                if(response.message){
                     $.gritter.add({
                            title: 'Información!',                                               
                            text: response.message,
                            time:3000,
                            class_name: 'gritter-success gritter-light'
                    });
                }
           }else{
              $.alert(response.message,'danger'); 
           }
       },
       /*error:function(){
            $.processWait.hide();
            $.alert("Se ha producido un error:</strong> No es posible eliminar el registro",'danger');
       }*/
    });
}




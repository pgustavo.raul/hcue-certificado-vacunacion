<?php

/**
 * @author Luis Malquin 
 */

namespace Core\GUIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CoreGUIBundle:Default:index.html.twig');
    }
}

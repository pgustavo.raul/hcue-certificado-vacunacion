<?php

/**
 * Clase controlador para la ejecución de acciones de visulizacion de pdf
 * @author Luis Malquin 
 */

namespace Core\GUIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class ObjectviewController extends Controller
{

    /**
     * @Route("/viewer/{ruta}/{parametros}", name="core_gui_objectview_viewer", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Template("CoreGUIBundle::object_view.html.twig")
     * @param string $ruta
     * @param string $parametros
     * @return text/html
     **/
    public function viewerAction($ruta="", $parametros="")
    {
        $params = "";

        if(!empty($parametros)) {
            $arrayParametros = explode(",", $parametros);
            foreach ($arrayParametros as $parametro) {
                $params = $params.'/'.$parametro;
            }
        }

        $pageUrl = $this->generateUrl($ruta, array(), false).$params;
        return array(
            'pathFile' => $pageUrl
        );
    }

}
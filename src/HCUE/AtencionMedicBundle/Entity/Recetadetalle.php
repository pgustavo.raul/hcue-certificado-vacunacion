<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Core\AppBundle\Entity\Catalogo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use HCUE\AtencionMedicBundle\Entity\Diagnosticoxrecetadetalle;
use Doctrine\Common\Collections\ArrayCollection;
use HCUE\AtencionMedicBundle\Entity\Horarioprescripcion;

/**
 * Descripcion de Recetadetalle
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.recetadetalle")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\RecetadetalleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Recetadetalle
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.recetadetalle_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $receta_id
     * @ORM\Column(name="receta_id", type="integer", nullable=false)
     * */
    private $receta_id;

    /**
     * @ORM\ManyToOne(targetEntity="Receta", inversedBy="detallereceta")
     * @ORM\JoinColumn(name="receta_id", referencedColumnName="id")
     * */
    private $receta;

    /**
     * @var integer $medicamento_id
     * @ORM\Column(name="medicamento_id", type="integer", nullable=false)
     * */
    private $medicamento_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Medicamento")
     * @ORM\JoinColumn(name="medicamento_id", referencedColumnName="id")
     * */
    private $medicamento;

    /**
     * @var integer $viaadmmed_id
     * @ORM\Column(name="viaadmmed_id", type="integer", nullable=false)
     * */
    private $viaadmmed_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="viaadmmed_id", referencedColumnName="id")
     * */
    private $viaadmmed;

    /**
     * @var integer $frecuenciadosis_id
     * @ORM\Column(name="frecuenciadosis_id", type="integer", nullable=false)
     * */
    private $frecuenciadosis_id;

    /**
     * @ORM\ManyToOne(targetEntity="Frecuenciadosis")
     * @ORM\JoinColumn(name="frecuenciadosis_id", referencedColumnName="id")
     * */
    private $frecuenciadosis;

    /**
     * @var integer $vecesxdia
     * @ORM\Column(name="vecesxdia", type="integer", nullable=true)
     * */
    private $vecesxdia;

    /**
     * @var integer $duracion
     * @ORM\Column(name="duracion", type="integer", nullable=true)
     * */
    private $duracion;

    /**
     * @var datetime $horafechainiciotrat
     * @ORM\Column(name="horafechainiciotrat", type="datetime", nullable=true)
     * */
    private $horafechainiciotrat;

    /**
     * @var datetime $horafechafintrat
     * @ORM\Column(name="horafechafintrat", type="datetime", nullable=true)
     * */
    private $horafechafintrat;

    /**
     * @var string $instrucciones
     * @ORM\Column(name="instrucciones", type="string",length=2000, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo instrucciones")
     * */
    private $instrucciones;

    /**
     * @var string $advertencias
     * @ORM\Column(name="advertencias", type="string",length=2000, nullable=true)
     * */
    private $advertencias;

    /**
     * @var float $dosis
     * @ORM\Column(name="dosis", type="float", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo dosis")
     * */
    private $dosis;

    /**
     * @var integer $unidadadministracion_id
     * @ORM\Column(name="unidadadministracion_id", type="integer", nullable=true)
     * */
    private $unidadadministracion_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="unidadadministracion_id", referencedColumnName="id")
     * */
    private $unidadadministracion;

    /**
     * @var integer $cantidad
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     * */
    private $cantidad;

    /**
     * @var string $cantidadtexto
     * @ORM\Column(name="cantidadtexto", type="string",length=256, nullable=true)
     * */
    private $cantidadtexto;

    /**
     * @var integer $ctestado_id
     * @ORM\Column(name="ctestado_id", type="integer",nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     * */
    private $ctestado_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestado_id", referencedColumnName="id")
     * */
    private $ctestado;

    /**
     * @var decimal $preciounitario
     * @ORM\Column(name="preciounitario", type="decimal", nullable=true)
     * */
    private $preciounitario;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     * */
    private $activo;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @ORM\OneToMany(targetEntity="Diagnosticoxrecetadetalle", mappedBy="recetadetalle",cascade={"persist"})
     * */
    private $diagnosticoxrecetadetalle;

    /**
     * @ORM\OneToMany(targetEntity="Horarioprescripcion", mappedBy="recetadetalle",cascade={"persist"})
     * */
    private $horarioprescripcion;

    public function __construct()
    {
        $this->diagnosticoxrecetadetalle = new ArrayCollection();
        $this->horarioprescripcion = new ArrayCollection();
    }

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set receta_id
     * @param integer receta_id
     * @return recetadetalle
     * */
    public function setRecetaId($receta_id)
    {
        $this->receta_id = $receta_id;
        return $this;
    }

    /**
     * Get receta_id
     * @return integer
     * */
    public function getRecetaId()
    {
        return $this->receta_id;
    }

    /**
     * Set receta
     * @param \HCUE\AtencionMedicBundle\Entity\Receta $receta
     * */
    public function setReceta(\HCUE\AtencionMedicBundle\Entity\Receta $receta)
    {
        $this->receta = $receta;
        return $this;
    }

    /**
     * Get receta
     * @return \HCUE\AtencionMedicBundle\Entity\Receta
     * */
    public function getReceta()
    {
        return $this->receta;
    }

    /**
     * Set medicamento_id
     * @param integer medicamento_id
     * @return recetadetalle
     * */
    public function setMedicamentoId($medicamento_id)
    {
        $this->medicamento_id = $medicamento_id;
        return $this;
    }

    /**
     * Get medicamento_id
     * @return integer
     * */
    public function getMedicamentoId()
    {
        return $this->medicamento_id;
    }

    /**
     * Set medicamento
     * @param \HCUE\AtencionMedicBundle\Entity\Medicamento $medicamento
     * */
    public function setMedicamento(
        \HCUE\AtencionMedicBundle\Entity\Medicamento $medicamento
    ) {
        $this->medicamento = $medicamento;
        return $this;
    }

    /**
     * Get medicamento
     * @return \HCUE\AtencionMedicBundle\Entity\Medicamento
     * */
    public function getMedicamento()
    {
        return $this->medicamento;
    }

    /**
     * Set viaadmmed_id
     * @param integer viaadmmed_id
     * @return recetadetalle
     * */
    public function setViaadmmedId($viaadmmed_id)
    {
        $this->viaadmmed_id = $viaadmmed_id;
        return $this;
    }

    /**
     * Get viaadmmed_id
     * @return integer
     * */
    public function getViaadmmedId()
    {
        return $this->viaadmmed_id;
    }

    /**
     * Set viaadmmed
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $viaadmmed
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function setViaadmmed(Detalleinfomedicamento $viaadmmed)
    {
        $this->viaadmmed = $viaadmmed;
        return $this;
    }

    /**
     * Get viaadmmed
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getViaadmmed()
    {
        return $this->viaadmmed;
    }

    /**
     * Set frecuenciadosis_id
     * @param decimal frecuenciadosis_id
     * @return recetadetalle
     * */
    public function setFrecuenciadosisId($frecuenciadosis_id)
    {
        $this->frecuenciadosis_id = $frecuenciadosis_id;
        return $this;
    }

    /**
     * Get frecuenciadosis_id
     * @return decimal
     * */
    public function getFrecuenciadosisId()
    {
        return $this->frecuenciadosis_id;
    }

    /**
     * Set frecuenciadosis
     * @param \HCUE\AtencionMedicBundle\Entity\Frecuenciadosis $frecuenciadosis
     * @return \HCUE\AtencionMedicBundle\Entity\Frecuenciadosis
     * */
    public function setFrecuenciadosis(\HCUE\AtencionMedicBundle\Entity\Frecuenciadosis $frecuenciadosis)
    {
        $this->frecuenciadosis = $frecuenciadosis;
        return $this;
    }

    /**
     * Get frecuenciadosis
     * @return \HCUE\AtencionMedicBundle\Entity\Frecuenciadosis
     * */
    public function getFrecuenciadosis()
    {
        return $this->frecuenciadosis;
    }

    /**
     * Set vecesxdia
     * @param integer vecesxdia
     * @return recetadetalle
     * */
    public function setVecesxdia($vecesxdia)
    {
        $this->vecesxdia = $vecesxdia;
        return $this;
    }

    /**
     * Get vecesxdia
     * @return integer
     * */
    public function getVecesxdia()
    {
        return $this->vecesxdia;
    }

    /**
     * Set duracion
     * @param integer duracion
     * @return recetadetalle
     * */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;
        return $this;
    }

    /**
     * Get duracion
     * @return integer
     * */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set horafechainiciotrat
     * @param datetime horafechainiciotrat
     * @return recetadetalle
     * */
    public function setHorafechainiciotrat($horafechainiciotrat)
    {
        $this->horafechainiciotrat = $horafechainiciotrat;
        return $this;
    }

    /**
     * Get horafechainiciotrat
     * @return datetime
     * */
    public function getHorafechainiciotrat()
    {
        return $this->horafechainiciotrat;
    }

    /**
     * Set horafechafintrat
     * @param datetime horafechafintrat
     * @return recetadetalle
     * */
    public function setHorafechafintrat($horafechafintrat)
    {
        $this->horafechafintrat = $horafechafintrat;
        return $this;
    }

    /**
     * Get horafechafintrat
     * @return datetime
     * */
    public function getHorafechafintrat()
    {
        return $this->horafechafintrat;
    }

    /**
     * Set instrucciones
     * @param string instrucciones
     * @return recetadetalle
     * */
    public function setInstrucciones($instrucciones)
    {
        $this->instrucciones = $instrucciones;
        return $this;
    }

    /**
     * Get instrucciones
     * @return string
     * */
    public function getInstrucciones()
    {
        return $this->instrucciones;
    }

    /**
     * Set advertencias
     * @param string advertencias
     * @return recetadetalle
     * */
    public function setAdvertencias($advertencias)
    {
        $this->advertencias = $advertencias;
        return $this;
    }

    /**
     * Get advertencias
     * @return string
     * */
    public function getAdvertencias()
    {
        return $this->advertencias;
    }

    /**
     * Set dosis
     * @param float dosis
     * @return recetadetalle
     * */
    public function setDosis($dosis)
    {
        $this->dosis = $dosis;
        return $this;
    }

    /**
     * Get dosis
     * @return float
     * */
    public function getDosis()
    {
        return $this->dosis;
    }

    /**
     * Set unidadadministracion_id
     * @param integer unidadadministracion_id
     * @return recetadetalle
     * */
    public function setUnidadadministracionId($unidadadministracion_id)
    {
        $this->unidadadministracion_id = $unidadadministracion_id;
        return $this;
    }

    /**
     * Get unidadadministracion_id
     * @return integer
     * */
    public function getUnidadadministracionId()
    {
        return $this->unidadadministracion_id;
    }

    /**
     * Set unidadadministracion
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $unidadadministracion
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function setUnidadadministracion(Detalleinfomedicamento $unidadadministracion)
    {
        $this->unidadadministracion = $unidadadministracion;
        return $this;
    }

    /**
     * Get unidadadministracion
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getUnidadadministracion()
    {
        return $this->unidadadministracion;
    }

    /**
     * Set cantidad
     * @param integer cantidad
     * @return recetadetalle
     * */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    /**
     * Get cantidad
     * @return integer
     * */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set cantidadtexto
     * @param string cantidadtexto
     * @return recetadetalle
     * */
    public function setCantidadtexto($cantidadtexto)
    {
        $this->cantidadtexto = $cantidadtexto;
        return $this;
    }

    /**
     * Get cantidadtexto
     * @return string
     * */
    public function getCantidadtexto()
    {
        return $this->cantidadtexto;
    }

    /**
     * Set ctestado_id
     * @param integer ctestado_id
     * @return receta
     * */
    public function setCtEstadoId($ctestado_id)
    {
        $this->ctestado_id = $ctestado_id;
        return $this;
    }

    /**
     * Get ctestado_id
     * @return integer
     * */
    public function getCtEstadoId()
    {
        return $this->ctestado_id;
    }


    /**
     * Set ctestado
     * @param \Core\AppBundle\Entity\Catalogo $ctestado
     * @return receta
     * */
    public function setCtEstado(Catalogo $ctestado)
    {
        $this->ctestado = $ctestado;
        return $this;
    }

    /**
     * Get ctestado
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtEstado()
    {
        return $this->ctestado;
    }

    /**
     * Set preciounitario
     * @param decimal preciounitario
     * @return recetadetalle
     * */
    public function setPreciounitario($preciounitario)
    {
        $this->preciounitario = $preciounitario;
        return $this;
    }

    /**
     * Get preciounitario
     * @return decimal
     * */
    public function getPreciounitario()
    {
        return $this->preciounitario;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return recetadetalle
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return recetadetalle
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return recetadetalle
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Add Diagnosticoxrecetadetalle
     * @param Diagnosticoxrecetadetalle $diagnosticoxrecetadetalle
     * */
    public function addDiagnosticoxrecetadetalle(Diagnosticoxrecetadetalle $diagnosticoxrecetadetalle)
    {
        $diagnosticoxrecetadetalle->setRecetadetalle($this);
        $this->diagnosticoxrecetadetalle->add($diagnosticoxrecetadetalle);
        return $this;
    }

    /**
     * Get Diagnosticoxrecetadetalle
     * @return Diagnosticoxrecetadetalle
     * */
    public function getDiagnosticoxrecetadetalle()
    {
        return $this->diagnosticoxrecetadetalle;
    }

    /**
     * Add Horarioprescripcion
     * @param Horarioprescripcion $horarioprescripcion
     * @return \HCUE\AtencionMedicBundle\Entity\Horarioprescripcion
     * */
    public function addHorarioprescripcion(Horarioprescripcion $horarioprescripcion)
    {
        $horarioprescripcion->setRecetadetalle($this);
        $this->horarioprescripcion->add($horarioprescripcion);
        return $this;
    }

    /**
     * Get Horarioprescripcion
     * @return Horarioprescripcion
     * */
    public function getHorarioprescripcion()
    {
        return $this->horarioprescripcion;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

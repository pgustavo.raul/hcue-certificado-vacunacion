<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de Frecuenciadosis
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.frecuenciadosis")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\FrecuenciadosisRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Frecuenciadosis
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.frecuenciadosis_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $etiqueta
     * @ORM\Column(name="etiqueta", type="string",length=30, nullable=true)
     * */
    private $etiqueta;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=100, nullable=true)
     * */
    private $descripcion;

    /**
     * @var integer $factor
     * @ORM\Column(name="factor", type="integer", nullable=true)
     * */
    private $factor;

    /**
     * @var integer $tipo
     * @ORM\Column(name="tipo", type="string", nullable=false)
     * */
    private $tipo;

    /**
     * @var integer $ingresomanual
     * @ORM\Column(name="ingresomanual", type="integer", nullable=true)
     * */
    private $ingresomanual;

    /**
     * @var integer $solohomeopatico
     * @ORM\Column(name="solohomeopatico", type="integer", nullable=true)
     * */
    private $solohomeopatico;

    /**
     * @var string $estado
     * @ORM\Column(name="estado", type="integer",nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     * */
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etiqueta
     * @param string etiqueta
     * @return frecuenciadosis
     * */
    public function setEtiqueta($etiqueta)
    {
        $this->etiqueta = $etiqueta;
        return $this;
    }

    /**
     * Get etiqueta
     * @return string
     * */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return frecuenciadosis
     * */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set factor
     * @param integer factor
     * @return frecuenciadosis
     * */
    public function setFactor($factor)
    {
        $this->factor = $factor;
        return $this;
    }

    /**
     * Get factor
     * @return integer
     * */
    public function getFactor()
    {
        return $this->factor;
    }

    /**
     * Set tipo
     * @param string tipo
     * @return frecuenciadosis
     * */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * Get tipo
     * @return integer
     * */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set ingresomanual
     * @param integer ingresomanual
     * @return frecuenciadosis
     * */
    public function setIngresomanual($ingresomanual)
    {
        $this->ingresomanual = $ingresomanual;
        return $this;
    }

    /**
     * Get ingresomanual
     * @return integer
     * */
    public function getIngresomanual()
    {
        return $this->ingresomanual;
    }

    /**
     * Set solohomeopatico
     * @param integer solohomeopatico
     * @return frecuenciadosis
     * */
    public function setSolohomeopatico($solohomeopatico)
    {
        $this->solohomeopatico = $solohomeopatico;
        return $this;
    }

    /**
     * Get solohomeopatico
     * @return integer
     * */
    public function getSolohomeopatico()
    {
        return $this->solohomeopatico;
    }

    /**
     * Set estado
     * @param integer estado
     * @return frecuenciadosis
     * */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return frecuenciadosis
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return frecuenciadosis
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return frecuenciadosis
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de Horarioprescripcion
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.horarioprescripcion")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\HorarioprescripcionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Horarioprescripcion
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.horarioprescripcion_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $horadia
     * @ORM\Column(name="horadia", type="string",length=20, nullable=true)
     * */
    private $horadia;

    /**
     * @var integer $recetadetalle_id
     * @ORM\Column(name="recetadetalle_id", type="integer", nullable=true)
     * */
    private $recetadetalle_id;

    /**
     * @ORM\ManyToOne(targetEntity="Recetadetalle", inversedBy="horarioprescripcion")
     * @ORM\JoinColumn(name="recetadetalle_id", referencedColumnName="id")
     * */
    private $recetadetalle;

    /**
     * @var integer $periododia_id
     * @ORM\Column(name="periododia_id", type="integer", nullable=true)
     * */
    private $periododia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Periododia")
     * @ORM\JoinColumn(name="periododia_id", referencedColumnName="id")
     * */
    private $periododia;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     * */
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     * */
    private $usuariocreacion_id;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set horadia
     * @param string horadia
     * @return horarioprescripcion
     * */
    public function setHoradia($horadia)
    {
        $this->horadia = $horadia;
        return $this;
    }

    /**
     * Get horadia
     * @return string
     * */
    public function getHoradia()
    {
        return $this->horadia;
    }

    /**
     * Set recetadetalle_id
     * @param integer recetadetalle_id
     * @return horarioprescripcion
     * */
    public function setRecetadetalleId($recetadetalle_id)
    {
        $this->recetadetalle_id = $recetadetalle_id;
        return $this;
    }

    /**
     * Get recetadetalle_id
     * @return integer
     * */
    public function getRecetadetalleId()
    {
        return $this->recetadetalle_id;
    }

    /**
     * Set recetadetalle
     * @param \HCUE\AtencionMedicBundle\Entity\Recetadetalle $recetadetalle
     * */
    public function setRecetadetalle(
        \HCUE\AtencionMedicBundle\Entity\Recetadetalle $recetadetalle
    ) {
        $this->recetadetalle = $recetadetalle;
        return $this;
    }

    /**
     * Get recetadetalle
     * @return \HCUE\AtencionMedicBundle\Entity\Recetadetalle
     * */
    public function getRecetadetalle()
    {
        return $this->recetadetalle;
    }

    /**
     * Set periododia_id
     * @param integer periododia_id
     * @return horarioprescripcion
     * */
    public function setPeriododiaId($periododia_id)
    {
        $this->periododia_id = $periododia_id;
        return $this;
    }

    /**
     * Get periododia_id
     * @return integer
     * */
    public function getPeriododiaId()
    {
        return $this->periododia_id;
    }

    /**
     * Set periododia
     * @param \HCUE\AtencionMedicBundle\Entity\Periododia $periododia
     * */
    public function setPeriododia(
        \HCUE\AtencionMedicBundle\Entity\Periododia $periododia
    ) {
        $this->periododia = $periododia;
        return $this;
    }

    /**
     * Get periododia
     * @return \HCUE\AtencionMedicBundle\Entity\Periododia
     * */
    public function getPeriododia()
    {
        return $this->periododia;
    }

    /**
     * Set activo
     * @param integer activo
     * @return horarioprescripcion
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return horarioprescripcion
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return horarioprescripcion
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

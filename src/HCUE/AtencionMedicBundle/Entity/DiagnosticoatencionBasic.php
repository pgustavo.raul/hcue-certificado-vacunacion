<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_amed.diagnosticoatencion")
 * @ORM\Entity()
 */
class DiagnosticoatencionBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Cie")
     * @ORM\JoinColumn(name="cie_id", referencedColumnName="id")
     * */
    private $cie;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     * */
    private $atencionmedica;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="ctcondiciondiagnostico_id", referencedColumnName="id")
     * */
    private $ctcondiciondiagnostico;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="ctcronologiadiagnostico_id", referencedColumnName="id")
     * */
    private $ctcronologiadiagnostico;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="cttipodiagnostico_id", referencedColumnName="id")
     * */
    private $cttipodiagnostico;

    /**
     * @var string $observacion
     * @ORM\Column(name="observacion", type="string")
     * */
    private $observacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Get cie
     * @return \HCUE\AtencionMedicBundle\Entity\Cie
     * */
    public function getCie() {
        return $this->cie;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     * */
    public function getAtencionmedica() {
        return $this->atencionmedica;
    }

    /**
     * Get ctcondiciondiagnostico
     * @return \Core\AppBundle\Entity\CatalogoBasic
     * */
    public function getCtcondiciondiagnostico() {
        return $this->ctcondiciondiagnostico;
    }

    /**
     * Get ctcronologiadiagnostico
     * @return \Core\AppBundle\Entity\CatalogoBasic
     * */
    public function getCtcronologiadiagnostico() {
        return $this->ctcronologiadiagnostico;
    }

    /**
     * Get cttipodiagnostico
     * @return \Core\AppBundle\Entity\CatalogoBasic
     * */
    public function getCttipodiagnostico() {
        return $this->cttipodiagnostico;
    }

    /**
     * Get observacion
     * @return string
     * */
    public function getObservacion() {
        return $this->observacion;
    }

}
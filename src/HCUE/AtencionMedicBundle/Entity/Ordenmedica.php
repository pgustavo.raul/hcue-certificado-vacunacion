<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.ordenmedica")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\OrdenmedicaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Ordenmedica{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.ordenmedica_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $ctprioridad_id
     * @ORM\Column(name="ctprioridad_id", type="integer", nullable=true)
     **/
    private $ctprioridad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctprioridad_id", referencedColumnName="id")
     **/
    private $ctprioridad;

    /**
     * @var string $muestraopieza
     * @ORM\Column(name="muestraopieza", type="string",length=2000, nullable=true)
     **/
    private $muestraopieza;

    /**
     * @var string $observacion
     * @ORM\Column(name="observacion", type="string",length=2000, nullable=true)
     **/
    private $observacion;

    /**
     * @var integer $esmovilizarse
     * @ORM\Column(name="esmovilizarse", type="boolean", nullable=true)
     **/
    private $esmovilizarse;

    /**
     * @var integer $esretvendas
     * @ORM\Column(name="esretvendas", type="boolean", nullable=true)
     **/
    private $esretvendas;

    /**
     * @var integer $esmedpresente
     * @ORM\Column(name="esmedpresente", type="boolean", nullable=true)
     **/
    private $esmedpresente;

    /**
     * @var integer $esrxcama
     * @ORM\Column(name="esrxcama", type="boolean", nullable=true)
     **/
    private $esrxcama;

    /**
     * @var integer $atencionmedica_id
     * @ORM\Column(name="atencionmedica_id", type="integer", nullable=true)
     **/
    private $atencionmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @var integer $cttipoordenmedica_id
     * @ORM\Column(name="cttipoordenmedica_id", type="integer", nullable=true)
     **/
    private $cttipoordenmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipoordenmedica_id", referencedColumnName="id")
     **/
    private $cttipoordenmedica;

    /**
     * @var string $tratamientoquerecibe
     * @ORM\Column(name="tratamientoquerecibe", type="string", nullable=true)
     **/
    private $tratamientoquerecibe;

    /**
     * @var string $resumenclinico
     * @ORM\Column(name="resumenclinico", type="string",length=2000, nullable=true)
     **/
    private $resumenclinico;

    /**
     * @var string $motivoconsulta
     * @ORM\Column(name="motivoconsulta", type="string",length=2000, nullable=true)
     **/
    private $motivoconsulta;

    /**
     * @ORM\ManyToOne(targetEntity="Core\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuariocreacion_id", referencedColumnName="id")
     **/
    private $usuario;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set ctprioridad_id
     * @param integer ctprioridad_id
     * @return ordenmedica
     **/
    public function setCtprioridadId($ctprioridad_id){
        $this->ctprioridad_id = $ctprioridad_id;
        return $this;
    }

    /**
     * Get ctprioridad_id
     * @return integer
     **/
    public function getCtprioridadId(){
        return $this->ctprioridad_id;
    }

    /**
     * Set ctprioridad
     * @param \Core\AppBundle\Entity\Catalogo $ctprioridad
     **/
    public function setCtprioridad(\Core\AppBundle\Entity\Catalogo $ctprioridad){
        $this->ctprioridad = $ctprioridad;
        return $this;
    }

    /**
     * Get ctprioridad
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtprioridad(){
        return $this->ctprioridad;
    }

    /**
     * Set muestraopieza
     * @param string muestraopieza
     * @return ordenmedica
     **/
    public function setMuestraopieza($muestraopieza){
        $this->muestraopieza = $muestraopieza;
        return $this;
    }

    /**
     * Get muestraopieza
     * @return string
     **/
    public function getMuestraopieza(){
        return $this->muestraopieza;
    }

    /**
     * Set observacion
     * @param string observacion
     * @return ordenmedica
     **/
    public function setObservacion($observacion){
        $this->observacion = $observacion;
        return $this;
    }

    /**
     * Get observacion
     * @return string
     **/
    public function getObservacion(){
        return $this->observacion;
    }

    /**
     * Set esmovilizarse
     * @param integer esmovilizarse
     * @return ordenmedica
     **/
    public function setEsmovilizarse($esmovilizarse){
        $this->esmovilizarse = $esmovilizarse;
        return $this;
    }

    /**
     * Get esmovilizarse
     * @return integer
     **/
    public function getEsmovilizarse(){
        return $this->esmovilizarse;
    }

    /**
     * Set esretvendas
     * @param integer esretvendas
     * @return ordenmedica
     **/
    public function setEsretvendas($esretvendas){
        $this->esretvendas = $esretvendas;
        return $this;
    }

    /**
     * Get esretvendas
     * @return integer
     **/
    public function getEsretvendas(){
        return $this->esretvendas;
    }

    /**
     * Set esmedpresente
     * @param integer esmedpresente
     * @return ordenmedica
     **/
    public function setEsmedpresente($esmedpresente){
        $this->esmedpresente = $esmedpresente;
        return $this;
    }

    /**
     * Get esmedpresente
     * @return integer
     **/
    public function getEsmedpresente(){
        return $this->esmedpresente;
    }

    /**
     * Set esrxcama
     * @param integer esrxcama
     * @return ordenmedica
     **/
    public function setEsrxcama($esrxcama){
        $this->esrxcama = $esrxcama;
        return $this;
    }

    /**
     * Get esrxcama
     * @return integer
     **/
    public function getEsrxcama(){
        return $this->esrxcama;
    }

    /**
     * Set atencionmedica_id
     * @param integer atencionmedica_id
     * @return ordenmedica
     **/
    public function setAtencionmedicaId($atencionmedica_id){
        $this->atencionmedica_id = $atencionmedica_id;
        return $this;
    }

    /**
     * Get atencionmedica_id
     * @return integer
     **/
    public function getAtencionmedicaId(){
        return $this->atencionmedica_id;
    }

    /**
     * Set atencionmedica
     * @param \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
     **/
    public function setAtencionmedica(\HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica){
        $this->atencionmedica = $atencionmedica;
        return $this;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     **/
    public function getAtencionmedica(){
        return $this->atencionmedica;
    }

    /**
     * Set cttipoordenmedica_id
     * @param integer cttipoordenmedica_id
     * @return ordenmedica
     **/
    public function setCttipoordenmedicaId($cttipoordenmedica_id){
        $this->cttipoordenmedica_id = $cttipoordenmedica_id;
        return $this;
    }

    /**
     * Get cttipoordenmedica_id
     * @return integer
     **/
    public function getCttipoordenmedicaId(){
        return $this->cttipoordenmedica_id;
    }

    /**
     * Set cttipoordenmedica
     * @param \Core\AppBundle\Entity\Catalogo $cttipoordenmedica
     **/
    public function setCttipoordenmedica(\Core\AppBundle\Entity\Catalogo $cttipoordenmedica){
        $this->cttipoordenmedica = $cttipoordenmedica;
        return $this;
    }

    /**
     * Get cttipoordenmedica
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCttipoordenmedica(){
        return $this->cttipoordenmedica;
    }

    /**
     * Set tratamientoquerecibe
     * @param string tratamientoquerecibe
     * @return ordenmedica
     **/
    public function setTratamientoquerecibe($tratamientoquerecibe){
        $this->tratamientoquerecibe = $tratamientoquerecibe;
        return $this;
    }

    /**
     * Get tratamientoquerecibe
     * @return string
     **/
    public function getTratamientoquerecibe(){
        return $this->tratamientoquerecibe;
    }

    /**
     * Set resumenclinico
     * @param string resumenclinico
     * @return ordenmedica
     **/
    public function setResumenclinico($resumenclinico){
        $this->resumenclinico = $resumenclinico;
        return $this;
    }

    /**
     * Get resumenclinico
     * @return string
     **/
    public function getResumenclinico(){
        return $this->resumenclinico;
    }

    /**
     * Set motivoconsulta
     * @param string motivoconsulta
     * @return ordenmedica
     **/
    public function setMotivoconsulta($motivoconsulta){
        $this->motivoconsulta = $motivoconsulta;
        return $this;
    }

    /**
     * Get motivoconsulta
     * @return string
     **/
    public function getMotivoconsulta(){
        return $this->motivoconsulta;
    }

    /**
     * Set usuario
     * @param \Core\SeguridadBundle\Entity\Usuario
     **/
    public function setUsuario(\Core\SeguridadBundle\Entity\Usuario $usuario){
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * Get usuario
     * @return \Core\SeguridadBundle\Entity\Usuario
     **/
    public function getUsuario(){
        return $this->usuario;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return ordenmedica
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return ordenmedica
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return ordenmedica
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return ordenesmedicas
     **/
    public function setFechacreacion() {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return \Datetime
     **/
    public function getFechacreacion() {
        return $this->fechacreacion;
    }

    /**
     * Set fechamodificacion
     * @ORM\PreUpdate
     * @return ordenesmedicas
     **/
    public function setFechamodificacion() {
        $this->fechamodificacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechamodificacion
     * @return \Datetime
     **/
    public function getFechamodificacion() {
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechamodificacion=new \DateTime();
        $this->fechacreacion=new \DateTime();
        $this->activo=1;

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

    }

    public function __toString() {

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
?>
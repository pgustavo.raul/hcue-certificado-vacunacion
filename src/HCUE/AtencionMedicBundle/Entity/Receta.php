<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Core\AppBundle\Entity\Catalogo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use HCUE\AtencionMedicBundle\Entity\Recetadetalle;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Descripcion de Receta
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.receta")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\RecetaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Receta
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.receta_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $paciente_id
     * @ORM\Column(name="paciente_id", type="integer", nullable=false)
     * */
    private $paciente_id;

    /**
     * @ORM\ManyToOne(targetEntity="\HCUE\AtencionMedicBundle\Entity\Paciente")
     * @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     * */
    private $paciente;

    /**
     * @var integer $atencionmedica_id
     * @ORM\Column(name="atencionmedica_id", type="integer", nullable=false)
     * */
    private $atencionmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="\HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     * */
    private $atencionmedica;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     * */
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     * */
    private $entidad;

    /**
     * @var integer $cttiporeceta_id
     * @ORM\Column(name="cttiporeceta_id", type="integer", nullable=false)
     * */
    private $cttiporeceta_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttiporeceta_id", referencedColumnName="id")
     * */
    private $cttiporeceta;

    /**
     * @var integer $ctestadopaciente_id
     * @ORM\Column(name="ctestadopaciente_id", type="integer", nullable=false)
     * */
    private $ctestadopaciente_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestadopaciente_id", referencedColumnName="id")
     * */
    private $ctestadopaciente;

    /**
     * @var integer $ctestado_id
     * @ORM\Column(name="ctestado_id", type="integer",nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     * */
    private $ctestado_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestado_id", referencedColumnName="id")
     * */
    private $ctestado;

    /**
     * @var datetime $fechaconfirmacion
     * @ORM\Column(name="fechaconfirmacion", type="datetime", nullable=true)
     * */
    private $fechaconfirmacion;

    /**
     * @var datetime $fechaentrega
     * @ORM\Column(name="fechaentrega", type="datetime", nullable=true)
     * */
    private $fechaentrega;

    /**
     * @var datetime $fechacaducidad
     * @ORM\Column(name="fechacaducidad", type="datetime", nullable=true)
     * */
    private $fechacaducidad;

    /**
     * @var datetime $fechacancelacion
     * @ORM\Column(name="fechacancelacion", type="datetime", nullable=true)
     * */
    private $fechacancelacion;

    /**
     * @var string $namereceta
     * @ORM\Column(name="namereceta", type="string",length=200, nullable=true)
     * */
    private $namereceta;

    /**
     * @var string $unicode
     * @ORM\Column(name="unicode", type="string",length=17, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo unicode")
     * */
    private $unicode;

    /**
     * @var string $tiempovigencia
     * @ORM\Column(name="tiempovigencia", type="integer",length=5, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo Tiempo de Vigencia")
     * */
    private $tiempovigencia;

    /**
     * @var string $motivocancelacion
     * @ORM\Column(name="motivocancelacion", type="string",length=2000, nullable=true)
     * */
    private $motivocancelacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @ORM\OneToMany(targetEntity="Recetadetalle", mappedBy="receta",cascade={"persist"})
     * */
    private $detallereceta;

    public function __construct()
    {
        $this->detallereceta = new ArrayCollection();
    }

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paciente_id
     * @param integer paciente_id
     * @return receta
     * */
    public function setPacienteId($paciente_id)
    {
        $this->paciente_id = $paciente_id;
        return $this;
    }

    /**
     * Get paciente_id
     * @return integer
     * */
    public function getPacienteId()
    {
        return $this->paciente_id;
    }

    /**
     * Set paciente
     * @param \HCUE\AtencionMedicBundle\Entity\Paciente $paciente
     * */
    public function setPaciente(
        \HCUE\AtencionMedicBundle\Entity\Paciente $paciente
    ) {
        $this->paciente = $paciente;
        return $this;
    }

    /**
     * Get paciente
     * @return \HCUE\AtencionMedicBundle\Entity\Paciente
     * */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set atencionmedica_id
     * @param integer atencionmedica_id
     * @return receta
     * */
    public function setAtencionmedicaId($atencionmedica_id)
    {
        $this->atencionmedica_id = $atencionmedica_id;
        return $this;
    }

    /**
     * Get atencionmedica_id
     * @return integer
     * */
    public function getAtencionmedicaId()
    {
        return $this->atencionmedica_id;
    }

    /**
     * Set atencionmedica
     * @param \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
     * */
    public function setAtencionmedica(
        \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
    ) {
        $this->atencionmedica = $atencionmedica;
        return $this;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     * */
    public function getAtencionmedica()
    {
        return $this->atencionmedica;
    }

    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return receta
     * */
    public function setEntidadId($entidad_id)
    {
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     * */
    public function getEntidadId()
    {
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     * */
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad)
    {
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     * */
    public function getEntidad()
    {
        return $this->entidad;
    }

    /**
     * Set cttiporeceta_id
     * @param integer cttiporeceta_id
     * @return receta
     * */
    public function setCttiporecetaId($cttiporeceta_id)
    {
        $this->cttiporeceta_id = $cttiporeceta_id;
        return $this;
    }

    /**
     * Get cttiporeceta_id
     * @return integer
     * */
    public function getCttiporecetaId()
    {
        return $this->cttiporeceta_id;
    }

    /**
     * Set cttiporeceta
     * @param \Core\AppBundle\Entity\Catalogo $cttiporeceta
     * */
    public function setCttiporeceta(Catalogo $cttiporeceta)
    {
        $this->cttiporeceta = $cttiporeceta;
        return $this;
    }

    /**
     * Get cttiporeceta
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCttiporeceta()
    {
        return $this->cttiporeceta;
    }

    /**
     * Set ctestadopaciente_id
     * @param integer ctestadopaciente_id
     * @return receta
     * */
    public function setCtEstadopacienteId($ctestadopaciente_id)
    {
        $this->ctestadopaciente_id = $ctestadopaciente_id;
        return $this;
    }

    /**
     * Get ctestadopaciente_id
     * @return integer
     * */
    public function getCtEstadopacienteId()
    {
        return $this->ctestadopaciente_id;
    }

    /**
     * Set ctestadopaciente
     * @param \Core\AppBundle\Entity\Catalogo $ctestadopaciente
     * @return receta
     * */
    public function setCtEstadopaciente(Catalogo $ctestadopaciente)
    {
        $this->ctestadopaciente = $ctestadopaciente;
        return $this;
    }

    /**
     * Get ctestadopaciente
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtEstadopaciente()
    {
        return $this->ctestadopaciente;
    }

    /**
     * Set ctestado_id
     * @param integer ctestado_id
     * @return receta
     * */
    public function setCtEstadoId($ctestado_id)
    {
        $this->ctestado_id = $ctestado_id;
        return $this;
    }

    /**
     * Get ctestado_id
     * @return integer
     * */
    public function getCtEstadoId()
    {
        return $this->ctestado_id;
    }


    /**
     * Set ctestado
     * @param \Core\AppBundle\Entity\Catalogo $ctestado
     * @return receta
     * */
    public function setCtEstado(Catalogo $ctestado)
    {
        $this->ctestado = $ctestado;
        return $this;
    }

    /**
     * Get ctestado
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtEstado()
    {
        return $this->ctestado;
    }

    /**
     * Set fechaconfirmacion
     * @param datetime fechaconfirmacion
     * @return receta
     * */
    public function setFechaconfirmacion($fechaconfirmacion)
    {
        $this->fechaconfirmacion = $fechaconfirmacion;
        return $this;
    }

    /**
     * Get fechaconfirmacion
     * @return datetime
     * */
    public function getFechaconfirmacion()
    {
        return $this->fechaconfirmacion;
    }

    /**
     * Set fechaentrega
     * @param datetime fechaentrega
     * @return receta
     * */
    public function setFechaentrega($fechaentrega)
    {
        $this->fechaentrega = $fechaentrega;
        return $this;
    }

    /**
     * Get fechaentrega
     * @return datetime
     * */
    public function getFechaentrega()
    {
        return $this->fechaentrega;
    }

    /**
     * Set fechacaducidad
     * @param datetime fechacaducidad
     * @return receta
     * */
    public function setFechacaducidad($fechacaducidad)
    {
        $this->fechacaducidad = $fechacaducidad;
        return $this;
    }

    /**
     * Get fechacaducidad
     * @return datetime
     * */
    public function getFechacaducidad()
    {
        return $this->fechacaducidad;
    }

    /**
     * Set fechacancelacion
     * @param datetime fechacancelacion
     * @return receta
     * */
    public function setFechacancelacion($fechacancelacion)
    {
        $this->fechacancelacion = $fechacancelacion;
        return $this;
    }

    /**
     * Get fechacancelacion
     * @return datetime
     * */
    public function getFechacancelacion()
    {
        return $this->fechacancelacion;
    }

    /**
     * Set namereceta
     * @param string namereceta
     * @return receta
     * */
    public function setNamereceta($namereceta)
    {
        $this->namereceta = $namereceta;
        return $this;
    }

    /**
     * Get namereceta
     * @return string
     * */
    public function getNamereceta()
    {
        return $this->namereceta;
    }

    /**
     * Set unicode
     * @param string unicode
     * @return receta
     * */
    public function setUnicode($unicode)
    {
        $this->unicode = $unicode;
        return $this;
    }

    /**
     * Get unicode
     * @return string
     * */
    public function getUnicode()
    {
        return $this->unicode;
    }

    /**
     * Set tiempovigencia
     * @param integer tiempovigencia
     * @return receta
     * */
    public function setTiempovigencia($tiempovigencia)
    {
        $this->tiempovigencia = $tiempovigencia;
        return $this;
    }

    /**
     * Get tiempovigencia
     * @return integer
     * */
    public function getTiempovigencia()
    {
        return $this->tiempovigencia;
    }

    /**
     * Set motivocancelacion
     * @param string motivocancelacion
     * @return receta
     * */
    public function setMotivocancelacion($motivocancelacion)
    {
        $this->motivocancelacion = $motivocancelacion;
        return $this;
    }

    /**
     * Get motivocancelacion
     * @return string
     * */
    public function getMotivocancelacion()
    {
        return $this->motivocancelacion;
    }

    /**
     * Set activo
     * @param integer activo
     * @return receta
     * */
    public function setActivo($activo)
    {
        $this->activo = $activo;
        return $this;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return receta
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return receta
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Add detallereceta
     * @param Recetadetalle $detallereceta
     * @return receta
     * */
    public function addDetallereceta(Recetadetalle $detallereceta)
    {
        $detallereceta->setReceta($this);
        $this->detallereceta->add($detallereceta);
        return $this;
    }

    /**
     * Get detallereceta
     * @return Recetadetalle
     * */
    public function getRecetadetalle()
    {
        return $this->detallereceta;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
        $this->fechaconfirmacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

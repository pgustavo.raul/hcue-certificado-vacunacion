<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de Diagnosticoxrecetadetalle
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.diagnosticoxrecetadetalle")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\DiagnosticoxrecetadetalleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Diagnosticoxrecetadetalle
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.diagnosticoxrecetadet_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $recetadetalle_id
     * @ORM\Column(name="recetadetalle_id", type="integer", nullable=false)
     * */
    private $recetadetalle_id;

    /**
     * @ORM\ManyToOne(targetEntity="Recetadetalle", inversedBy="diagnosticoxrecetadetalle")
     * @ORM\JoinColumn(name="recetadetalle_id", referencedColumnName="id")
     * */
    private $recetadetalle;

    /**
     * @var integer $diagnostico_id
     * @ORM\Column(name="diagnostico_id", type="integer", nullable=false)
     * */
    private $diagnostico_id;

    /**
     * @ORM\ManyToOne(targetEntity="\HCUE\AtencionMedicBundle\Entity\Diagnosticoatencion")
     * @ORM\JoinColumn(name="diagnostico_id", referencedColumnName="id")
     * */
    private $diagnostico;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recetadetalle_id
     * @param integer recetadetalle_id
     * @return diagnosticoxrecetadetalle
     * */
    public function setRecetadetalleId($recetadetalle_id)
    {
        $this->recetadetalle_id = $recetadetalle_id;
        return $this;
    }

    /**
     * Get recetadetalle_id
     * @return integer
     * */
    public function getRecetadetalleId()
    {
        return $this->recetadetalle_id;
    }

    /**
     * Set recetadetalle
     * @param \HCUE\AtencionMedicBundle\Entity\Recetadetalle $recetadetalle
     * */
    public function setRecetadetalle(
        \HCUE\AtencionMedicBundle\Entity\Recetadetalle $recetadetalle
    ) {
        $this->recetadetalle = $recetadetalle;
        return $this;
    }

    /**
     * Get recetadetalle
     * @return \HCUE\AtencionMedicBundle\Entity\Recetadetalle
     * */
    public function getRecetadetalle()
    {
        return $this->recetadetalle;
    }

    /**
     * Set diagnostico_id
     * @param integer diagnostico_id
     * @return recetadetalle
     * */
    public function setDiagnosticoId($diagnostico_id)
    {
        $this->diagnostico_id = $diagnostico_id;
        return $this;
    }

    /**
     * Get diagnostico_id
     * @return integer
     * */
    public function getDiagnosticoId()
    {
        return $this->diagnostico_id;
    }

    /**
     * Set diagnostico
     * @param \HCUE\AtencionMedicBundle\Entity\Diagnosticoatencion $diagnostico
     * */
    public function setDiagnostico(
        \HCUE\AtencionMedicBundle\Entity\Diagnosticoatencion $diagnostico
    ) {
        $this->diagnostico = $diagnostico;
        return $this;
    }

    /**
     * Get diagnostico
     * @return \HCUE\AtencionMedicBundle\Entity\Diagnosticoatencion
     * */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }

    /**
     * Set activo
     * @param integer activo
     * @return diagnosticoxrecetadetalle
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return diagnosticoxrecetadetalle
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return diagnosticoxrecetadetalle
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

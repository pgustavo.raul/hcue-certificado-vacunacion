<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.cargavacunacion")
* @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\CargavacunacionRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Cargavacunacion{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.cargavacunacion_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $vacunafecanio
	* @ORM\Column(name="vacunafecanio", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunafecanio")
	**/
	private $vacunafecanio;

	/**
	* @var integer $vacunafecmes
	* @ORM\Column(name="vacunafecmes", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunafecmes")
	**/
	private $vacunafecmes;

	/**
	* @var integer $vacunafecdia
	* @ORM\Column(name="vacunafecdia", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunafecdia")
	**/
	private $vacunafecdia;

	/**
	* @var string $puntovacunacion
	* @ORM\Column(name="puntovacunacion", type="string",length=200, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo puntovacunacion")
	**/
	private $puntovacunacion;

	/**
	* @var integer $unicodigo
	* @ORM\Column(name="unicodigo", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo unicodigo")
	**/
	private $unicodigo;

	/**
	* @var string $uninombre
	* @ORM\Column(name="uninombre", type="string",length=150, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo uninombre")
	**/
	private $uninombre;

	/**
	* @var integer $zona
	* @ORM\Column(name="zona", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo zona")
	**/
	private $zona;

	/**
	* @var string $distrito
	* @ORM\Column(name="distrito", type="string",length=150, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo distrito")
	**/
	private $distrito;

	/**
	* @var string $provincia
	* @ORM\Column(name="provincia", type="string",length=150, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo provincia")
	**/
	private $provincia;

	/**
	* @var string $canton
	* @ORM\Column(name="canton", type="string",length=150, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo canton")
	**/
	private $canton;

	/**
	* @var string $apellidos
	* @ORM\Column(name="apellidos", type="string",length=100, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo apellidos")
	**/
	private $apellidos;

	/**
	* @var string $nombres
	* @ORM\Column(name="nombres", type="string",length=100, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nombres")
	**/
	private $nombres;

	/**
	* @var string $tipoidentificacion
	* @ORM\Column(name="tipoidentificacion", type="string",length=100, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo tipoidentificacion")
	**/
	private $tipoidentificacion;

	/**
	* @var string $numidentificacion
	* @ORM\Column(name="numidentificacion", type="string",length=25, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo numidentificacion")
	**/
	private $numidentificacion;

	/**
	* @var string $sexo
	* @ORM\Column(name="sexo", type="string",length=15, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo sexo")
	**/
	private $sexo;

	/**
	* @var integer $nacimientoanio
	* @ORM\Column(name="nacimientoanio", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nacimientoanio")
	**/
	private $nacimientoanio;

	/**
	* @var integer $nacimientomes
	* @ORM\Column(name="nacimientomes", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nacimientomes")
	**/
	private $nacimientomes;

	/**
	* @var integer $nacimientodia
	* @ORM\Column(name="nacimientodia", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nacimientodia")
	**/
	private $nacimientodia;

	/**
	* @var string $nacionalidad
	* @ORM\Column(name="nacionalidad", type="string",length=50, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo nacionalidad")
	**/
	private $nacionalidad;

	/**
	* @var string $telefono
	* @ORM\Column(name="telefono", type="string",length=15, nullable=true)
	**/
	private $telefono;

	/**
	* @var string $correo
	* @ORM\Column(name="correo", type="string",length=100, nullable=true)
	**/
	private $correo;

	/**
	* @var string $poblacionobjetiva
	* @ORM\Column(name="poblacionobjetiva", type="string",length=100, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo poblacionobjetiva")
	**/
	private $poblacionobjetiva;

	/**
	* @var integer $vacunafase
	* @ORM\Column(name="vacunafase", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunafase")
	**/
	private $vacunafase;

	/**
	* @var string $vacunanombre
	* @ORM\Column(name="vacunanombre", type="string",length=200, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunanombre")
	**/
	private $vacunanombre;

	/**
	* @var string $vacunalote
	* @ORM\Column(name="vacunalote", type="string",length=50, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunalote")
	**/
	private $vacunalote;

	/**
	* @var integer $vacunadosis
	* @ORM\Column(name="vacunadosis", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo vacunadosis")
	**/
	private $vacunadosis;

	/**
	* @var string $agendado
	* @ORM\Column(name="agendado", type="string",length=50, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo agendado")
	**/
	private $agendado;

	/**
	* @var string $vacunadornombre
	* @ORM\Column(name="vacunadornombre", type="string",length=50, nullable=true)
	**/
	private $vacunadornombre;

	/**
	* @var string $vacunador
	* @ORM\Column(name="vacunador", type="string",length=20, nullable=true)
	**/
	private $vacunador;

	/**
	* @var string $registrador
	* @ORM\Column(name="registrador", type="string",length=50, nullable=true)
	**/
	private $registrador;

	/**
	* @var string $rcnombre
	* @ORM\Column(name="rcnombre", type="string",length=200, nullable=true)
	**/
	private $rcnombre;

	/**
	* @var string $rccalledomicilio
	* @ORM\Column(name="rccalledomicilio", type="string",length=200, nullable=true)
	**/
	private $rccalledomicilio;

	/**
	* @var string $rccondicioncedulado
	* @ORM\Column(name="rccondicioncedulado", type="string",length=150, nullable=true)
	**/
	private $rccondicioncedulado;

	/**
	* @var string $rcconyuge
	* @ORM\Column(name="rcconyuge", type="string",length=100, nullable=true)
	**/
	private $rcconyuge;

	/**
	* @var string $rcdomicilio
	* @ORM\Column(name="rcdomicilio", type="string",length=200, nullable=true)
	**/
	private $rcdomicilio;

	/**
	* @var string $rcestadocivil
	* @ORM\Column(name="rcestadocivil", type="string",length=20, nullable=true)
	**/
	private $rcestadocivil;

	/**
	* @var string $rcfechanacimiento
	* @ORM\Column(name="rcfechanacimiento", type="string",length=50, nullable=true)
	**/
	private $rcfechanacimiento;

	/**
	* @var string $rcinstruccion
	* @ORM\Column(name="rcinstruccion", type="string",length=50, nullable=true)
	**/
	private $rcinstruccion;

	/**
	* @var string $rclugarnacimiento
	* @ORM\Column(name="rclugarnacimiento", type="string",length=100, nullable=true)
	**/
	private $rclugarnacimiento;

	/**
	* @var string $rcnacionalidad
	* @ORM\Column(name="rcnacionalidad", type="string",length=50, nullable=true)
	**/
	private $rcnacionalidad;

	/**
	* @var string $rcnombremadre
	* @ORM\Column(name="rcnombremadre", type="string",length=100, nullable=true)
	**/
	private $rcnombremadre;

	/**
	* @var string $rcnombrepadre
	* @ORM\Column(name="rcnombrepadre", type="string",length=100, nullable=true)
	**/
	private $rcnombrepadre;

	/**
	* @var string $rcprofesion
	* @ORM\Column(name="rcprofesion", type="string",length=100, nullable=true)
	**/
	private $rcprofesion;

	/**
	* @var string $rcfechafallecimiento
	* @ORM\Column(name="rcfechafallecimiento", type="string",length=50, nullable=true)
	**/
	private $rcfechafallecimiento;

	/**
	* @var string $rcsexo
	* @ORM\Column(name="rcsexo", type="string",length=20, nullable=true)
	**/
	private $rcsexo;

	/**
	* @var string $rcerror
	* @ORM\Column(name="rcerror", type="string",length=500, nullable=true)
	**/
	private $rcerror;

	/**
	* @var string $prasfechanacimiento
	* @ORM\Column(name="prasfechanacimiento", type="string",length=100, nullable=true)
	**/
	private $prasfechanacimiento;

	/**
	* @var string $vacunaobligatoria
	* @ORM\Column(name="vacunaobligatoria", type="string",length=20, nullable=true)
	**/
	private $vacunaobligatoria;

	/**
	* @var integer $ctgruporiesgo_id
	* @ORM\Column(name="ctgruporiesgo_id", type="integer", nullable=true)
	**/
	private $ctgruporiesgo_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctgruporiesgo_id", referencedColumnName="id")
	**/
	private $ctgruporiesgo;

	/**
	* @var datetime $proximavacunacion
	* @ORM\Column(name="proximavacunacion", type="datetime", nullable=true)
	**/
	private $proximavacunacion;

	/**
	* @var integer $cttipocarga_id
	* @ORM\Column(name="cttipocarga_id", type="integer", nullable=true)
	**/
	private $cttipocarga_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="cttipocarga_id", referencedColumnName="id")
	**/
	private $cttipocarga;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var integer $ctestadocargavacuna_id
	* @ORM\Column(name="ctestadocargavacuna_id", type="integer", nullable=true)
	**/
	private $ctestadocargavacuna_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctestadocargavacuna_id", referencedColumnName="id")
	**/
	private $ctestadocargavacuna;

	/**
	* @var string $registroerror
	* @ORM\Column(name="registroerror", type="string",length=500, nullable=true)
	**/
	private $registroerror;

	/**
	* @var integer $paciente_id
	* @ORM\Column(name="paciente_id", type="integer", nullable=true)
	**/
	private $paciente_id;

	/**
	* @ORM\ManyToOne(targetEntity="Paciente")
	* @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
	**/
	private $paciente;

	/**
	* @var integer $persona_id
	* @ORM\Column(name="persona_id", type="integer", nullable=true)
	**/
	private $persona_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Persona")
	* @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
	**/
	private $persona;

	/**
	* @var integer $esquemavacunacion_id
	* @ORM\Column(name="esquemavacunacion_id", type="integer", nullable=true)
	**/
	private $esquemavacunacion_id;

	/**
	* @ORM\ManyToOne(targetEntity="Esquemavacunacion")
	* @ORM\JoinColumn(name="esquemavacunacion_id", referencedColumnName="id")
	**/
	private $esquemavacunacion;

	/**
	* @var integer $ctsexo_id
	* @ORM\Column(name="ctsexo_id", type="integer", nullable=true)
	**/
	private $ctsexo_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctsexo_id", referencedColumnName="id")
	**/
	private $ctsexo;

	/**
	* @var integer $ctestadocivil_id
	* @ORM\Column(name="ctestadocivil_id", type="integer", nullable=true)
	**/
	private $ctestadocivil_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctestadocivil_id", referencedColumnName="id")
	**/
	private $ctestadocivil;

	/**
	* @var integer $cttipoidentificacion_id
	* @ORM\Column(name="cttipoidentificacion_id", type="integer", nullable=true)
	**/
	private $cttipoidentificacion_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="cttipoidentificacion_id", referencedColumnName="id")
	**/
	private $cttipoidentificacion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=false)
	**/
	private $activo;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set vacunafecanio
	* @param integer vacunafecanio
	* @return cargavacunacion
	**/
	public function setVacunafecanio($vacunafecanio){
		 $this->vacunafecanio = $vacunafecanio;
		 return $this;
	}

	/**
	* Get vacunafecanio
	* @return integer
	**/
	public function getVacunafecanio(){
		 return $this->vacunafecanio;
	}

	/**
	* Set vacunafecmes
	* @param integer vacunafecmes
	* @return cargavacunacion
	**/
	public function setVacunafecmes($vacunafecmes){
		 $this->vacunafecmes = $vacunafecmes;
		 return $this;
	}

	/**
	* Get vacunafecmes
	* @return integer
	**/
	public function getVacunafecmes(){
		 return $this->vacunafecmes;
	}

	/**
	* Set vacunafecdia
	* @param integer vacunafecdia
	* @return cargavacunacion
	**/
	public function setVacunafecdia($vacunafecdia){
		 $this->vacunafecdia = $vacunafecdia;
		 return $this;
	}

	/**
	* Get vacunafecdia
	* @return integer
	**/
	public function getVacunafecdia(){
		 return $this->vacunafecdia;
	}

	/**
	* Set puntovacunacion
	* @param string puntovacunacion
	* @return cargavacunacion
	**/
	public function setPuntovacunacion($puntovacunacion){
		 $this->puntovacunacion = $puntovacunacion;
		 return $this;
	}

	/**
	* Get puntovacunacion
	* @return string
	**/
	public function getPuntovacunacion(){
		 return $this->puntovacunacion;
	}

	/**
	* Set unicodigo
	* @param integer unicodigo
	* @return cargavacunacion
	**/
	public function setUnicodigo($unicodigo){
		 $this->unicodigo = $unicodigo;
		 return $this;
	}

	/**
	* Get unicodigo
	* @return integer
	**/
	public function getUnicodigo(){
		 return $this->unicodigo;
	}

	/**
	* Set uninombre
	* @param string uninombre
	* @return cargavacunacion
	**/
	public function setUninombre($uninombre){
		 $this->uninombre = $uninombre;
		 return $this;
	}

	/**
	* Get uninombre
	* @return string
	**/
	public function getUninombre(){
		 return $this->uninombre;
	}

	/**
	* Set zona
	* @param integer zona
	* @return cargavacunacion
	**/
	public function setZona($zona){
		 $this->zona = $zona;
		 return $this;
	}

	/**
	* Get zona
	* @return integer
	**/
	public function getZona(){
		 return $this->zona;
	}

	/**
	* Set distrito
	* @param string distrito
	* @return cargavacunacion
	**/
	public function setDistrito($distrito){
		 $this->distrito = $distrito;
		 return $this;
	}

	/**
	* Get distrito
	* @return string
	**/
	public function getDistrito(){
		 return $this->distrito;
	}

	/**
	* Set provincia
	* @param string provincia
	* @return cargavacunacion
	**/
	public function setProvincia($provincia){
		 $this->provincia = $provincia;
		 return $this;
	}

	/**
	* Get provincia
	* @return string
	**/
	public function getProvincia(){
		 return $this->provincia;
	}

	/**
	* Set canton
	* @param string canton
	* @return cargavacunacion
	**/
	public function setCanton($canton){
		 $this->canton = $canton;
		 return $this;
	}

	/**
	* Get canton
	* @return string
	**/
	public function getCanton(){
		 return $this->canton;
	}

	/**
	* Set apellidos
	* @param string apellidos
	* @return cargavacunacion
	**/
	public function setApellidos($apellidos){
		 $this->apellidos = $apellidos;
		 return $this;
	}

	/**
	* Get apellidos
	* @return string
	**/
	public function getApellidos(){
		 return $this->apellidos;
	}

	/**
	* Set nombres
	* @param string nombres
	* @return cargavacunacion
	**/
	public function setNombres($nombres){
		 $this->nombres = $nombres;
		 return $this;
	}

	/**
	* Get nombres
	* @return string
	**/
	public function getNombres(){
		 return $this->nombres;
	}

	/**
	* Set tipoidentificacion
	* @param string tipoidentificacion
	* @return cargavacunacion
	**/
	public function setTipoidentificacion($tipoidentificacion){
		 $this->tipoidentificacion = $tipoidentificacion;
		 return $this;
	}

	/**
	* Get tipoidentificacion
	* @return string
	**/
	public function getTipoidentificacion(){
		 return $this->tipoidentificacion;
	}

	/**
	* Set numidentificacion
	* @param string numidentificacion
	* @return cargavacunacion
	**/
	public function setNumidentificacion($numidentificacion){
		 $this->numidentificacion = $numidentificacion;
		 return $this;
	}

	/**
	* Get numidentificacion
	* @return string
	**/
	public function getNumidentificacion(){
		 return $this->numidentificacion;
	}

	/**
	* Set sexo
	* @param string sexo
	* @return cargavacunacion
	**/
	public function setSexo($sexo){
		 $this->sexo = $sexo;
		 return $this;
	}

	/**
	* Get sexo
	* @return string
	**/
	public function getSexo(){
		 return $this->sexo;
	}

	/**
	* Set nacimientoanio
	* @param integer nacimientoanio
	* @return cargavacunacion
	**/
	public function setNacimientoanio($nacimientoanio){
		 $this->nacimientoanio = $nacimientoanio;
		 return $this;
	}

	/**
	* Get nacimientoanio
	* @return integer
	**/
	public function getNacimientoanio(){
		 return $this->nacimientoanio;
	}

	/**
	* Set nacimientomes
	* @param integer nacimientomes
	* @return cargavacunacion
	**/
	public function setNacimientomes($nacimientomes){
		 $this->nacimientomes = $nacimientomes;
		 return $this;
	}

	/**
	* Get nacimientomes
	* @return integer
	**/
	public function getNacimientomes(){
		 return $this->nacimientomes;
	}

	/**
	* Set nacimientodia
	* @param integer nacimientodia
	* @return cargavacunacion
	**/
	public function setNacimientodia($nacimientodia){
		 $this->nacimientodia = $nacimientodia;
		 return $this;
	}

	/**
	* Get nacimientodia
	* @return integer
	**/
	public function getNacimientodia(){
		 return $this->nacimientodia;
	}

	/**
	* Set nacionalidad
	* @param string nacionalidad
	* @return cargavacunacion
	**/
	public function setNacionalidad($nacionalidad){
		 $this->nacionalidad = $nacionalidad;
		 return $this;
	}

	/**
	* Get nacionalidad
	* @return string
	**/
	public function getNacionalidad(){
		 return $this->nacionalidad;
	}

	/**
	* Set telefono
	* @param string telefono
	* @return cargavacunacion
	**/
	public function setTelefono($telefono){
		 $this->telefono = $telefono;
		 return $this;
	}

	/**
	* Get telefono
	* @return string
	**/
	public function getTelefono(){
		 return $this->telefono;
	}

	/**
	* Set correo
	* @param string correo
	* @return cargavacunacion
	**/
	public function setCorreo($correo){
		 $this->correo = $correo;
		 return $this;
	}

	/**
	* Get correo
	* @return string
	**/
	public function getCorreo(){
		 return $this->correo;
	}

	/**
	* Set poblacionobjetiva
	* @param string poblacionobjetiva
	* @return cargavacunacion
	**/
	public function setPoblacionobjetiva($poblacionobjetiva){
		 $this->poblacionobjetiva = $poblacionobjetiva;
		 return $this;
	}

	/**
	* Get poblacionobjetiva
	* @return string
	**/
	public function getPoblacionobjetiva(){
		 return $this->poblacionobjetiva;
	}

	/**
	* Set vacunafase
	* @param integer vacunafase
	* @return cargavacunacion
	**/
	public function setVacunafase($vacunafase){
		 $this->vacunafase = $vacunafase;
		 return $this;
	}

	/**
	* Get vacunafase
	* @return integer
	**/
	public function getVacunafase(){
		 return $this->vacunafase;
	}

	/**
	* Set vacunanombre
	* @param string vacunanombre
	* @return cargavacunacion
	**/
	public function setVacunanombre($vacunanombre){
		 $this->vacunanombre = $vacunanombre;
		 return $this;
	}

	/**
	* Get vacunanombre
	* @return string
	**/
	public function getVacunanombre(){
		 return $this->vacunanombre;
	}

	/**
	* Set vacunalote
	* @param string vacunalote
	* @return cargavacunacion
	**/
	public function setVacunalote($vacunalote){
		 $this->vacunalote = $vacunalote;
		 return $this;
	}

	/**
	* Get vacunalote
	* @return string
	**/
	public function getVacunalote(){
		 return $this->vacunalote;
	}

	/**
	* Set vacunadosis
	* @param integer vacunadosis
	* @return cargavacunacion
	**/
	public function setVacunadosis($vacunadosis){
		 $this->vacunadosis = $vacunadosis;
		 return $this;
	}

	/**
	* Get vacunadosis
	* @return integer
	**/
	public function getVacunadosis(){
		 return $this->vacunadosis;
	}

	/**
	* Set agendado
	* @param string agendado
	* @return cargavacunacion
	**/
	public function setAgendado($agendado){
		 $this->agendado = $agendado;
		 return $this;
	}

	/**
	* Get agendado
	* @return string
	**/
	public function getAgendado(){
		 return $this->agendado;
	}

	/**
	* Set vacunadornombre
	* @param string vacunadornombre
	* @return cargavacunacion
	**/
	public function setVacunadornombre($vacunadornombre){
		 $this->vacunadornombre = $vacunadornombre;
		 return $this;
	}

	/**
	* Get vacunadornombre
	* @return string
	**/
	public function getVacunadornombre(){
		 return $this->vacunadornombre;
	}

	/**
	* Set vacunador
	* @param string vacunador
	* @return cargavacunacion
	**/
	public function setVacunador($vacunador){
		 $this->vacunador = $vacunador;
		 return $this;
	}

	/**
	* Get vacunador
	* @return string
	**/
	public function getVacunador(){
		 return $this->vacunador;
	}

	/**
	* Set registrador
	* @param string registrador
	* @return cargavacunacion
	**/
	public function setRegistrador($registrador){
		 $this->registrador = $registrador;
		 return $this;
	}

	/**
	* Get registrador
	* @return string
	**/
	public function getRegistrador(){
		 return $this->registrador;
	}

	/**
	* Set rcnombre
	* @param string rcnombre
	* @return cargavacunacion
	**/
	public function setRcnombre($rcnombre){
		 $this->rcnombre = $rcnombre;
		 return $this;
	}

	/**
	* Get rcnombre
	* @return string
	**/
	public function getRcnombre(){
		 return $this->rcnombre;
	}

	/**
	* Set rccalledomicilio
	* @param string rccalledomicilio
	* @return cargavacunacion
	**/
	public function setRccalledomicilio($rccalledomicilio){
		 $this->rccalledomicilio = $rccalledomicilio;
		 return $this;
	}

	/**
	* Get rccalledomicilio
	* @return string
	**/
	public function getRccalledomicilio(){
		 return $this->rccalledomicilio;
	}

	/**
	* Set rccondicioncedulado
	* @param string rccondicioncedulado
	* @return cargavacunacion
	**/
	public function setRccondicioncedulado($rccondicioncedulado){
		 $this->rccondicioncedulado = $rccondicioncedulado;
		 return $this;
	}

	/**
	* Get rccondicioncedulado
	* @return string
	**/
	public function getRccondicioncedulado(){
		 return $this->rccondicioncedulado;
	}

	/**
	* Set rcconyuge
	* @param string rcconyuge
	* @return cargavacunacion
	**/
	public function setRcconyuge($rcconyuge){
		 $this->rcconyuge = $rcconyuge;
		 return $this;
	}

	/**
	* Get rcconyuge
	* @return string
	**/
	public function getRcconyuge(){
		 return $this->rcconyuge;
	}

	/**
	* Set rcdomicilio
	* @param string rcdomicilio
	* @return cargavacunacion
	**/
	public function setRcdomicilio($rcdomicilio){
		 $this->rcdomicilio = $rcdomicilio;
		 return $this;
	}

	/**
	* Get rcdomicilio
	* @return string
	**/
	public function getRcdomicilio(){
		 return $this->rcdomicilio;
	}

	/**
	* Set rcestadocivil
	* @param string rcestadocivil
	* @return cargavacunacion
	**/
	public function setRcestadocivil($rcestadocivil){
		 $this->rcestadocivil = $rcestadocivil;
		 return $this;
	}

	/**
	* Get rcestadocivil
	* @return string
	**/
	public function getRcestadocivil(){
		 return $this->rcestadocivil;
	}

	/**
	* Set rcfechanacimiento
	* @param string rcfechanacimiento
	* @return cargavacunacion
	**/
	public function setRcfechanacimiento($rcfechanacimiento){
		 $this->rcfechanacimiento = $rcfechanacimiento;
		 return $this;
	}

	/**
	* Get rcfechanacimiento
	* @return string
	**/
	public function getRcfechanacimiento(){
		 return $this->rcfechanacimiento;
	}

	/**
	* Set rcinstruccion
	* @param string rcinstruccion
	* @return cargavacunacion
	**/
	public function setRcinstruccion($rcinstruccion){
		 $this->rcinstruccion = $rcinstruccion;
		 return $this;
	}

	/**
	* Get rcinstruccion
	* @return string
	**/
	public function getRcinstruccion(){
		 return $this->rcinstruccion;
	}

	/**
	* Set rclugarnacimiento
	* @param string rclugarnacimiento
	* @return cargavacunacion
	**/
	public function setRclugarnacimiento($rclugarnacimiento){
		 $this->rclugarnacimiento = $rclugarnacimiento;
		 return $this;
	}

	/**
	* Get rclugarnacimiento
	* @return string
	**/
	public function getRclugarnacimiento(){
		 return $this->rclugarnacimiento;
	}

	/**
	* Set rcnacionalidad
	* @param string rcnacionalidad
	* @return cargavacunacion
	**/
	public function setRcnacionalidad($rcnacionalidad){
		 $this->rcnacionalidad = $rcnacionalidad;
		 return $this;
	}

	/**
	* Get rcnacionalidad
	* @return string
	**/
	public function getRcnacionalidad(){
		 return $this->rcnacionalidad;
	}

	/**
	* Set rcnombremadre
	* @param string rcnombremadre
	* @return cargavacunacion
	**/
	public function setRcnombremadre($rcnombremadre){
		 $this->rcnombremadre = $rcnombremadre;
		 return $this;
	}

	/**
	* Get rcnombremadre
	* @return string
	**/
	public function getRcnombremadre(){
		 return $this->rcnombremadre;
	}

	/**
	* Set rcnombrepadre
	* @param string rcnombrepadre
	* @return cargavacunacion
	**/
	public function setRcnombrepadre($rcnombrepadre){
		 $this->rcnombrepadre = $rcnombrepadre;
		 return $this;
	}

	/**
	* Get rcnombrepadre
	* @return string
	**/
	public function getRcnombrepadre(){
		 return $this->rcnombrepadre;
	}

	/**
	* Set rcprofesion
	* @param string rcprofesion
	* @return cargavacunacion
	**/
	public function setRcprofesion($rcprofesion){
		 $this->rcprofesion = $rcprofesion;
		 return $this;
	}

	/**
	* Get rcprofesion
	* @return string
	**/
	public function getRcprofesion(){
		 return $this->rcprofesion;
	}

	/**
	* Set rcfechafallecimiento
	* @param string rcfechafallecimiento
	* @return cargavacunacion
	**/
	public function setRcfechafallecimiento($rcfechafallecimiento){
		 $this->rcfechafallecimiento = $rcfechafallecimiento;
		 return $this;
	}

	/**
	* Get rcfechafallecimiento
	* @return string
	**/
	public function getRcfechafallecimiento(){
		 return $this->rcfechafallecimiento;
	}

	/**
	* Set rcsexo
	* @param string rcsexo
	* @return cargavacunacion
	**/
	public function setRcsexo($rcsexo){
		 $this->rcsexo = $rcsexo;
		 return $this;
	}

	/**
	* Get rcsexo
	* @return string
	**/
	public function getRcsexo(){
		 return $this->rcsexo;
	}

	/**
	* Set rcerror
	* @param string rcerror
	* @return cargavacunacion
	**/
	public function setRcerror($rcerror){
		 $this->rcerror = $rcerror;
		 return $this;
	}

	/**
	* Get rcerror
	* @return string
	**/
	public function getRcerror(){
		 return $this->rcerror;
	}

	/**
	* Set prasfechanacimiento
	* @param string prasfechanacimiento
	* @return cargavacunacion
	**/
	public function setPrasfechanacimiento($prasfechanacimiento){
		 $this->prasfechanacimiento = $prasfechanacimiento;
		 return $this;
	}

	/**
	* Get prasfechanacimiento
	* @return string
	**/
	public function getPrasfechanacimiento(){
		 return $this->prasfechanacimiento;
	}

	/**
	* Set vacunaobligatoria
	* @param string vacunaobligatoria
	* @return cargavacunacion
	**/
	public function setVacunaobligatoria($vacunaobligatoria){
		 $this->vacunaobligatoria = $vacunaobligatoria;
		 return $this;
	}

	/**
	* Get vacunaobligatoria
	* @return string
	**/
	public function getVacunaobligatoria(){
		 return $this->vacunaobligatoria;
	}

	/**
	* Set ctgruporiesgo_id
	* @param integer ctgruporiesgo_id
	* @return cargavacunacion
	**/
	public function setCtgruporiesgoId($ctgruporiesgo_id){
		 $this->ctgruporiesgo_id = $ctgruporiesgo_id;
		 return $this;
	}

	/**
	* Get ctgruporiesgo_id
	* @return integer
	**/
	public function getCtgruporiesgoId(){
		 return $this->ctgruporiesgo_id;
	}

	/**
	* Set ctgruporiesgo
	* @param \Core\AppBundle\Entity\Catalogo $ctgruporiesgo
	**/
	public function setCtgruporiesgo(\Core\AppBundle\Entity\Catalogo $ctgruporiesgo){
		 $this->ctgruporiesgo = $ctgruporiesgo;
		 return $this;
	}

	/**
	* Get ctgruporiesgo
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtgruporiesgo(){
		 return $this->ctgruporiesgo;
	}

	/**
	* Set proximavacunacion
	* @param datetime proximavacunacion
	* @return cargavacunacion
	**/
	public function setProximavacunacion($proximavacunacion){
		 $this->proximavacunacion = $proximavacunacion;
		 return $this;
	}

	/**
	* Get proximavacunacion
	* @return datetime
	**/
	public function getProximavacunacion(){
		 return $this->proximavacunacion;
	}

	/**
	* Set cttipocarga_id
	* @param integer cttipocarga_id
	* @return cargavacunacion
	**/
	public function setCttipocargaId($cttipocarga_id){
		 $this->cttipocarga_id = $cttipocarga_id;
		 return $this;
	}

	/**
	* Get cttipocarga_id
	* @return integer
	**/
	public function getCttipocargaId(){
		 return $this->cttipocarga_id;
	}

	/**
	* Set cttipocarga
	* @param \Core\AppBundle\Entity\Catalogo $cttipocarga
	**/
	public function setCttipocarga(\Core\AppBundle\Entity\Catalogo $cttipocarga){
		 $this->cttipocarga = $cttipocarga;
		 return $this;
	}

	/**
	* Get cttipocarga
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCttipocarga(){
		 return $this->cttipocarga;
	}

	/**
	* Set estado
	* @param integer estado
	* @return cargavacunacion
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set ctestadocargavacuna_id
	* @param integer ctestadocargavacuna_id
	* @return cargavacunacion
	**/
	public function setCtestadocargavacunaId($ctestadocargavacuna_id){
		 $this->ctestadocargavacuna_id = $ctestadocargavacuna_id;
		 return $this;
	}

	/**
	* Get ctestadocargavacuna_id
	* @return integer
	**/
	public function getCtestadocargavacunaId(){
		 return $this->ctestadocargavacuna_id;
	}

	/**
	* Set ctestadocargavacuna
	* @param \Core\AppBundle\Entity\Catalogo $ctestadocargavacuna
	**/
	public function setCtestadocargavacuna(\Core\AppBundle\Entity\Catalogo $ctestadocargavacuna){
		 $this->ctestadocargavacuna = $ctestadocargavacuna;
		 return $this;
	}

	/**
	* Get ctestadocargavacuna
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtestadocargavacuna(){
		 return $this->ctestadocargavacuna;
	}

	/**
	* Set registroerror
	* @param string registroerror
	* @return cargavacunacion
	**/
	public function setRegistroerror($registroerror){
		 $this->registroerror = $registroerror;
		 return $this;
	}

	/**
	* Get registroerror
	* @return string
	**/
	public function getRegistroerror(){
		 return $this->registroerror;
	}

	/**
	* Set paciente_id
	* @param integer paciente_id
	* @return cargavacunacion
	**/
	public function setPacienteId($paciente_id){
		 $this->paciente_id = $paciente_id;
		 return $this;
	}

	/**
	* Get paciente_id
	* @return integer
	**/
	public function getPacienteId(){
		 return $this->paciente_id;
	}

	/**
	* Set paciente
	* @param \HCUE\PacienteBundle\Entity\Paciente $paciente
	**/
	public function setPaciente(\HCUE\PacienteBundle\Entity\Paciente $paciente){
		 $this->paciente = $paciente;
		 return $this;
	}

	/**
	* Get paciente
	* @return \HCUE\PacienteBundle\Entity\Paciente
	**/
	public function getPaciente(){
		 return $this->paciente;
	}

	/**
	* Set persona_id
	* @param integer persona_id
	* @return cargavacunacion
	**/
	public function setPersonaId($persona_id){
		 $this->persona_id = $persona_id;
		 return $this;
	}

	/**
	* Get persona_id
	* @return integer
	**/
	public function getPersonaId(){
		 return $this->persona_id;
	}

	/**
	* Set persona
	* @param \Core\AppBundle\Entity\Persona $persona
	**/
	public function setPersona(\Core\AppBundle\Entity\Persona $persona){
		 $this->persona = $persona;
		 return $this;
	}

	/**
	* Get persona
	* @return \Core\AppBundle\Entity\Persona
	**/
	public function getPersona(){
		 return $this->persona;
	}

	/**
	* Set esquemavacunacion_id
	* @param integer esquemavacunacion_id
	* @return cargavacunacion
	**/
	public function setEsquemavacunacionId($esquemavacunacion_id){
		 $this->esquemavacunacion_id = $esquemavacunacion_id;
		 return $this;
	}

	/**
	* Get esquemavacunacion_id
	* @return integer
	**/
	public function getEsquemavacunacionId(){
		 return $this->esquemavacunacion_id;
	}

	/**
	* Set esquemavacunacion
	* @param \HCUE\AtencionMedicBundle\Entity\Esquemavacunacion $esquemavacunacion
	**/
	public function setEsquemavacunacion(\HCUE\AtencionMedicBundle\Entity\Esquemavacunacion $esquemavacunacion){
		 $this->esquemavacunacion = $esquemavacunacion;
		 return $this;
	}

	/**
	* Get esquemavacunacion
	* @return \HCUE\AtencionMedicBundle\Entity\Esquemavacunacion
	**/
	public function getEsquemavacunacion(){
		 return $this->esquemavacunacion;
	}

	/**
	* Set ctsexo_id
	* @param integer ctsexo_id
	* @return cargavacunacion
	**/
	public function setCtsexoId($ctsexo_id){
		 $this->ctsexo_id = $ctsexo_id;
		 return $this;
	}

	/**
	* Get ctsexo_id
	* @return integer
	**/
	public function getCtsexoId(){
		 return $this->ctsexo_id;
	}

	/**
	* Set ctsexo
	* @param \Core\AppBundle\Entity\Catalogo $ctsexo
	**/
	public function setCtsexo(\Core\AppBundle\Entity\Catalogo $ctsexo){
		 $this->ctsexo = $ctsexo;
		 return $this;
	}

	/**
	* Get ctsexo
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtsexo(){
		 return $this->ctsexo;
	}

	/**
	* Set ctestadocivil_id
	* @param integer ctestadocivil_id
	* @return cargavacunacion
	**/
	public function setCtestadocivilId($ctestadocivil_id){
		 $this->ctestadocivil_id = $ctestadocivil_id;
		 return $this;
	}

	/**
	* Get ctestadocivil_id
	* @return integer
	**/
	public function getCtestadocivilId(){
		 return $this->ctestadocivil_id;
	}

	/**
	* Set ctestadocivil
	* @param \Core\AppBundle\Entity\Catalogo $ctestadocivil
	**/
	public function setCtestadocivil(\Core\AppBundle\Entity\Catalogo $ctestadocivil){
		 $this->ctestadocivil = $ctestadocivil;
		 return $this;
	}

	/**
	* Get ctestadocivil
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtestadocivil(){
		 return $this->ctestadocivil;
	}

	/**
	* Set cttipoidentificacion_id
	* @param integer cttipoidentificacion_id
	* @return cargavacunacion
	**/
	public function setCttipoidentificacionId($cttipoidentificacion_id){
		 $this->cttipoidentificacion_id = $cttipoidentificacion_id;
		 return $this;
	}

	/**
	* Get cttipoidentificacion_id
	* @return integer
	**/
	public function getCttipoidentificacionId(){
		 return $this->cttipoidentificacion_id;
	}

	/**
	* Set cttipoidentificacion
	* @param \Core\AppBundle\Entity\Catalogo $cttipoidentificacion
	**/
	public function setCttipoidentificacion(\Core\AppBundle\Entity\Catalogo $cttipoidentificacion){
		 $this->cttipoidentificacion = $cttipoidentificacion;
		 return $this;
	}

	/**
	* Get cttipoidentificacion
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCttipoidentificacion(){
		 return $this->cttipoidentificacion;
	}

	/**
	* Get fechacreacion
	* @return datetime
	**/
	public function getFechacreacion(){
		 return $this->fechacreacion;
	}

	/**
	* Get fechamodificacion
	* @return datetime
	**/
	public function getFechamodificacion(){
		 return $this->fechamodificacion;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return cargavacunacion
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return cargavacunacion
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set activo
	* @param integer activo
	* @return cargavacunacion
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* Get activo
	* @return integer
	**/
	public function getActivo(){
		 return $this->activo;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();
		$this->activo=1;

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
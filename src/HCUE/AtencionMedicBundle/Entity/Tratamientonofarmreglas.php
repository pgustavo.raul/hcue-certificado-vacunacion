<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.tratamientonofarmreglas")
* @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\TratamientonofarmreglasRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Tratamientonofarmreglas{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.tratamientonofarmreglas_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $cttratamientonf_id
	* @ORM\Column(name="cttratamientonf_id", type="integer", nullable=false)
	**/
	private $cttratamientonf_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="cttratamientonf_id", referencedColumnName="id")
	**/
	private $cttratamientonf;

	/**
	* @var string $edadminima
	* @ORM\Column(name="edadminima", type="string",length=10, nullable=true)
	**/
	private $edadminima;

	/**
	* @var string $edadmaxima
	* @ORM\Column(name="edadmaxima", type="string",length=10, nullable=true)
	**/
	private $edadmaxima;

	/**
	* @var integer $ctgrupoprioritario_id
	* @ORM\Column(name="ctgrupoprioritario_id", type="integer", nullable=true)
	**/
	private $ctgrupoprioritario_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctgrupoprioritario_id", referencedColumnName="id")
	**/
	private $ctgrupoprioritario;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set cttratamientonf_id
	* @param integer cttratamientonf_id
	* @return tratamientonofarmreglas
	**/
	public function setCttratamientonfId($cttratamientonf_id){
		 $this->cttratamientonf_id = $cttratamientonf_id;
		 return $this;
	}

	/**
	* Get cttratamientonf_id
	* @return integer
	**/
	public function getCttratamientonfId(){
		 return $this->cttratamientonf_id;
	}

	/**
	* Set cttratamientonf
	* @param \Core\AppBundle\Entity\Catalogo $cttratamientonf
	**/
	public function setCttratamientonf(\Core\AppBundle\Entity\Catalogo $cttratamientonf){
		 $this->cttratamientonf = $cttratamientonf;
		 return $this;
	}

	/**
	* Get cttratamientonf
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCttratamientonf(){
		 return $this->cttratamientonf;
	}

	/**
	* Set edadminima
	* @param string edadminima
	* @return tratamientonofarmreglas
	**/
	public function setEdadminima($edadminima){
		 $this->edadminima = $edadminima;
		 return $this;
	}

	/**
	* Get edadminima
	* @return string
	**/
	public function getEdadminima(){
		 return $this->edadminima;
	}

	/**
	* Set edadmaxima
	* @param string edadmaxima
	* @return tratamientonofarmreglas
	**/
	public function setEdadmaxima($edadmaxima){
		 $this->edadmaxima = $edadmaxima;
		 return $this;
	}

	/**
	* Get edadmaxima
	* @return string
	**/
	public function getEdadmaxima(){
		 return $this->edadmaxima;
	}

	/**
	* Set ctgrupoprioritario_id
	* @param integer ctgrupoprioritario_id
	* @return tratamientonofarmreglas
	**/
	public function setCtgrupoprioritarioId($ctgrupoprioritario_id){
		 $this->ctgrupoprioritario_id = $ctgrupoprioritario_id;
		 return $this;
	}

	/**
	* Get ctgrupoprioritario_id
	* @return integer
	**/
	public function getCtgrupoprioritarioId(){
		 return $this->ctgrupoprioritario_id;
	}

	/**
	* Set ctgrupoprioritario
	* @param \Core\AppBundle\Entity\Catalogo $ctgrupoprioritario
	**/
	public function setCtgrupoprioritario(\Core\AppBundle\Entity\Catalogo $ctgrupoprioritario){
		 $this->ctgrupoprioritario = $ctgrupoprioritario;
		 return $this;
	}

	/**
	* Get ctgrupoprioritario
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtgrupoprioritario(){
		 return $this->ctgrupoprioritario;
	}

	/**
	* Set estado
	* @param integer estado
	* @return tratamientonofarmreglas
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return tratamientonofarmreglas
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return tratamientonofarmreglas
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->fechamodificacion=new \DateTime();
$this->fechacreacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
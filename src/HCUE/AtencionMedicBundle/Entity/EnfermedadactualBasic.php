<?php 

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Table(name="hcue_amed.enfermedadactual")
* @ORM\Entity()
*/
class EnfermedadactualBasic
{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer")
	* @ORM\Id
	**/
	private $id;

	/**
	* @ORM\ManyToOne(targetEntity="AtencionMedicaBasic")
	* @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
	**/
	private $atencionmedica;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string")
	**/
	private $descripcion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Get atencionmedica
	* @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
	**/
	public function getAtencionmedica(){
		 return $this->atencionmedica;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

}
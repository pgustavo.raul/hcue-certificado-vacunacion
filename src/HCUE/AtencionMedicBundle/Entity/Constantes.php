<?php


namespace HCUE\AtencionMedicBundle\Entity;

use Core\AppBundle\Entity\Constantes as AppConstantes;

/**
 * Clase para definir las constantes utilizadas en los modulos de atención médica
 */
class Constantes extends AppConstantes
{
    /**
     * @const integer Id del tipo de catalogo para parentezcos
     */
    const TC_PARENTEZCOS = 8;

    /**
     * @const integer Id del tipo de catalogo para los estados de los niveles de instrucción
     */
    const TC_ESTADOSNIVELESINSTRUCCION = 14;

    /**
     * @const integer Id del tipo de catalogo para filtros de informacion por motivo de consulta
     */
    const TC_MOTIVOCONSULTA = 21;

    /**
     * @const integer Id del tipo de catalogo para los estados de la atención médica
     */
    const TC_ESTADOATENCIONMEDICA = 22;

    /**
     * @const integer Id del tipo catalogo para las condiciones de diagnosticos
     */
    const TC_CONDICIONDIAGNOSTICO = 23;

    /**
     * @const integer Id del  catalogo para los tipos de diagnosticos
     */
    const TC_TIPODIAGNOSTICO = 24;

    /**
     * @const integer Id del tipo catalogo para las cronologias de diagnosticos
     */
    const TC_CRONOLOGIADIAGNOSTICO = 25;

    /**
     * @const integer Id del tipo catalogo para los antecedentes del paciente en la atención
     */
    const TC_ANTECEDENTESATENCION = 28;

    /**
     * @const integer Id del tipo catalogo para definir los Organos y sistemas (Anatomía)
     */
    const TC_ANATOMIAORGANOSSISTEMAS = 29;

    /**
     * @const integer Id del tipo catalogo para definir las Regiones (Anatomía)
     */
    const TC_ANATOMIAREGIONES = 30;

    /**
     * @const integer Id del tipo catalogo para definir las Evidencia patológica (sin Evidencia, con evidencia)
     */
    const TC_EVIDENCIAPATOLOGICA = 31;

    /**
     * @const integer Id del tipo catalogo para los signos vitales
     */
    const TC_SIGNOSVITALES = 32;

    /**
     * @const integer Id del tipo catalogo para los grupos prioritarios
     */
    const TC_GRUPOPRIORITARIO = 35;

    /**
     * @const integer Id del tipo catalogo para la orientación sexual
     */
    const TC_ORIENTACIONSEXUAL = 36;

    /**
     * @const integer Id del tipo catalogo para la identidad de género
     */
    const TC_IDENTIDADGENERO = 37;

    /**
     * @const integer Id del tipo catalogo para los grupos sanguíneos
     */
    const TC_GRUPOSANGUINEO = 51;

    /**
     * @const integer Id del tipo catalogo para el estado general del paciente
     */
    const TC_ESTADOGENERALPACIENTE = 39;

    /**
     * @const integer Id del tipo catalogo para el tipo de datos de antecedentevalidacion
     */
    const TC_TIPOS_ANTECEDENTEVALIDACION = 33;

    /**
     * @const integer Id del tipo catalogo para identificar las secciones de los formularios
     */
    const TC_SECCIONESFORMULARIOSMEDICOS = 19;

    /**
     * @const integer Id del tipo catalogo para identificar las secciones de los formularios
     */
    const TC_TIPODOCUMENTOS = 38;

    /**
     * @const integer Id del tipo de catalogo para nacionalidades y pueblos indigenas
     */
    const TC_NACIONALIDADESPUEBLOS = 7;

    /**
     * @const integer Id del tipo de atencion de la tabla catalogo
     */
    const TC_TIPOATENCION = 41;

    /**
     * @const integer Id del tipo de atencion de la tabla catalogo
     */
    const TC_LUGARATENCION = 42;

    /**
     * @const integer Id del Odontograma simbología diagnóstico de la tabla catalogo
     */
    const TC_ODONTOGRAMA_SIMBOLOGIA_DIAGNOSTICO = 47;

    /**
     * @const integer Id del registro descripcion de las piezas dentales
     */
    const TC_PIEZAS_DENTALES_DESCRIPCION = 45;

    /**
     * @const integer Id del registro descripcion de las especiales medicas
     */
    const TC_ESPECIALIDADES_MEDICAS = 16;

    /**
     * @const integer Id del registro descripcion de los motivos de referencia
     */
    const TC_MOTIVO_REFERENCIA = 64;

    /**
     * @const integer Id del registro descripcion de las regiones de la boca
     */
    const TC_REGIONESBOCA = 62;

    /**
     * @const integer Id del registro descripcion de las regiones de la boca
     */
    const TC_ALTERACIONES_TEJIDOS_BLANDOS = 63;


    /*
     * Constantes de detalle catalogos
     * CT Abreviatura de catalogos
     */

    /**
     * @const integer Id del registro detalle catalogo etnia indígena
     */
    const CT_ETNIA_INDIGENA = 18;

    /**
     * @const integer Id del registro detalle de catalogo webservis sin respuesta
     */
    const CT_WS_SEGUROSALUD_SIN_RESPUESTA = 628;

    /**
     * @const integer Id del registro catalogo de sección Motivo de consulta
     */
    const CT_SECMOTIVOCONSULTA = 189;

    /**
     * @const integer Id del registro catalogo de sección Antecedentes
     */
    const CT_SECANTECEDENTES = 190;

    /**
     * @const integer Id del registro catalogo de sección Enfermedades o problema actual
     */
    const CT_SECENFERMEDADESPROBLEMAACTUAL = 191;

    /**
     * @const integer Id del registro catalogo de sección Revisión actual de órganos y sistemas
     */
    const CT_SECREVISIONORGANOSSISTEMAS = 192;

    /**
     * @const integer Id del registro catalogo de sección Signos vitales
     */
    const CT_SECSIGNOSVITALES = 193;

    /**
     * @const integer Id del registro catalogo de sección Examen físico
     */
    const CT_SECEXAMENFÍSICO = 194;

    /**
     * @const integer Id del registro catalogo de sección Diagnóstico
     */
    const CT_SECDIAGNOSTICO = 195;

    /**
     * @const integer Id del registro catalogo de sección Planes de tratamiento
     */
    const CT_SECPLANESTRATAMIENTO = 196;

    /**
     * @const integer Id del registro catalogo de sección Evolución
     */
    const CT_SECEVOLUCION = 197;

    /**
     * @const integer Id del registro catalogo de sección Prescripciones
     */
    const CT_SECPRESCRIPCIONES = 198;

    /**
     * @const integer Id del registro catalogo de sección Receta
     */
    const CT_SECRECETA = 630;

    /**
     * @const integer Id del registro catalogo de sección para información del paciente
     */
    const CT_SECINFORMACION_PACIENTE = 686;

    /**
     * @const integer Id del registro catalogo de sección para gestación actual
     */
    const CT_SECGESTACION_ACTUAL = 943;

    /**
     * @const integer Id del registro catalogo de sección para gestación actual
     */
    const CT_SECPERIODONTOGRAMA = 993;


    /**
     * @const integer Id del registro catalogo de motivos de consulta Trauma
     */
    const CT_TRAUMA = 216;

    /**
     * @const integer Id del registro catalogo de motivos de consulta Causa Clínica
     */
    const CT_CAUSACLINICA = 217;

    /**
     * @const integer Id del registro catalogo de motivos de consulta Causa Gíneco Obstetra
     */
    const CT_CAUSAGINECOOBSTETRA = 218;

    /**
     * @const integer Id del registro catalogo de motivos de consulta Causa Quirúrgica
     */
    const CT_CAUSAQUIRURGICA = 219;

    /**
     * @const integer Id del registro catalogo de motivos de consulta Otro motivo
     */
    const CT_OTROMOTIVO = 220;

    /**
     * @const integer Id del registro detalle catalogo estado de la atención Iniciada
     */
    const CT_ATENCIONINICIADA = 221;

    /**
     * @const integer Id del registro detalle catalogo estado de la atención Finalizada
     */
    const CT_ATENCIONFINALIZADA = 222;

    /**
     * @const integer Id del registro detalle catalogo diagnostico presuntivo
     */
    const CT_PRESUNTIVO = 223;

    /**
     * @const integer Id del registro detalle catalogo diagnostico definitivo inicial
     */
    const CT_DEFINITIVOINICIAL = 224;

    /**
     * @const integer Id del registro detalle catalogo diagnostico Definitivo Inicial Confirmado por Laboratorio
     */
    const CT_DEFINITIVOINICIALCONFIRMADO = 225;

    /**
     * @const integer Id del registro detalle catalogo diagnostico Definitivo Control
     */
    const CT_DEFINITIVOCONTROL = 226;

    /**
     * @const integer Id del registro detalle catalogo diagnostico Prevención
     */
    const CT_PREVENCION = 227;

    /**
     * @const integer Id del registro detalle catalogo diagnostico Morbilidad
     */
    const CT_MORBILIDAD = 228;

    /**
     * @const integer Id del registro detalle catalogo diagnostico Primera
     */
    const CT_PRIMERA = 229;

    /**
     * @const integer Id del registro detalle catalogo diagnostico Subsecuente
     */
    const CT_SUBSECUENTE = 230;


    /**
     * @const integer Id del registro detalle catalogo antecedentes personales
     */
    const CT_ANTECEDENTE_PERSONALES = 236;


    /**
     * @const integer Id del registro detalle catalogo antecedentes personales
     */
    const CT_ANTECEDENTE_FAMILIARES = 237;


    /**
     * @const integer Id del registro detalle catalogo antecedentes gineco-obstetricos
     */
    const CT_ANTECEDENTE_GINECO_OBSTETRICOS = 258;

    /**
     * @const integer Id del registro detalle catalogo antecedentes ginecologicos
     */
    const CT_ANTECEDENTE_GINECOLOGICOS = 1072;

    /**
     * @const integer Id del registro detalle catalogo antecedentes odontologicos
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS = 1085;

    /**
     * @const integer Id del registro detalle catalogo antecedentes durante las ultimas 24 horas recibio solo leche materna (0 a 2 años de edad)
     */
    const CT_ANTECEDENTE_RECIBIOSOLOLECHEMATERNA24H = 508;

    /**
     * @const integer Id del registro detalle catalogo antecedentes solo ha recibido leche materna (0 a 5 meses de edad)
     */
    const CT_ANTECEDENTE_RECIBIOSOLOLECHEMATERNA = 1458;

    /**
     * @const integer Id del registro detalle catalogo antecedentes durante las ultimas 24 horas consumio alimento solido, semisolido o suave(6 a 8 meses de edad)
     */
    const CT_ANTECEDENTE_CONSUMIOSOLIDOSEMISOLIDO = 509;

    /**
     * @const integer Id del registro detalle catalogo antecedentes Ha recibido Hierro, multivitaminas o minerales en polvo
     */
    const CT_ANTECEDENTE_HARECIBIDOHIERROMULTIVITAMINAS = 1452;

    /**
     * @const integer Id del registro detalle catalogo antecedentes Ha recibido Vitamina A
     */
    const CT_ANTECEDENTE_HARECIBIDOVITAMINA_A = 1453;

    /**
     * @const integer Id del registro detalle catalogo antecedentes postanatales
     */
    const CT_GRUPO_ANTECEDENTE_POSNATALES = 242;

    /**
     * @const integer Id del registro detalle catalogo temperatura de signos vitales
     */
    const CT_TEMPERATURA = 452;

    /**
     * @const integer Id del registro detalle catalogo presion sistolica
     */
    const CT_PRESION_SISTOLICA = 438;

    /**
     * @const integer Id del registro detalle catalogo presion asistolica
     */
    const CT_PRESION_ASISTOLICA = 439;

    /**
     * @const integer Id del registro detalle catalogo presion media
     */
    const CT_PRESION_MEDIA = 440;


    /**
     * @const integer Id del registro detalle catalogo frecuencia cardiaca
     */
    const CT_FRECUECIA_CARDIACA = 445;

    /**
     * @const integer Id del registro detalle catalogo frecuencia respiratoria
     */
    const CT_FRECUECIA_RESPIRATORIA = 446;

    /**
     * @const integer Id del registro detalle catalogo saturación
     */
    const CT_SATURACION = 460;

    /**
     * @const integer Id del registro detalle catalogo peso
     */
    const CT_PESO = 461;

    /**
     * @const integer Id del registro detalle catalogo talla
     */
    const CT_TALLA = 462;

    /**
     * @const integer Id del registro detalle catalogo talla corregida
     */
    const CT_TALLACORREGIDA = 1459;

    /**
     * @const integer Id del registro IMC
     */
    const CT_IMC = 467;

    /**
     * @const integer Id del registro de los indicadores de crecimiento
     */
    const CT_INDICADORCRECIMIENTO = 1172;

    /**
     * @const integer Id del registro de Talla para la edad (T/E)
     */
    const CT_TALLA_PARA_EDAD = 1173;

    /**
     * @const integer Id del registro Peso para la edad (P/E)
     */
    const CT_PESO_PARA_EDAD = 1174;

    /**
     * @const integer Id del registro Peso para la longitud talla (P/T)
     */
    const CT_PESO_PARA_LONGITUDTALLA = 1175;

    /**
     * @const integer Id del registro IMC para la edad (IMC/E)
     */
    const CT_IMC_PARA_EDAD = 1176;

    /**
     * @const integer Id del registro detalle catalogo perimetro cefalico
     */
    const CT_PERIMETRO_CEFALICO = 463;

    /**
     * @const integer Id del registro detalle catalogo perimetro abdominal
     */
    const CT_PERIMETRO_ABDOMINAL = 464;

    /**
     * @const integer Id del registro detalle catalogo glucosa capilar
     */
    const CT_GLUCOSA_CAPILAR = 996;

    /**
     * @const integer Id del registro detalle catalogo valor de hemoglobina
     */
    const CT_HEMOGLOBINA = 1157;

    /**
     * @const integer Id del registro detalle catalogo valor de hemoglobina corregido
     */
    const CT_HEMOGLOBINA_CORREGIDO = 1158;


    /**
     * @const array para dibujar las graficas de estado nutricional
     */
    const GRAFICAESTADONUTRICION = [
        self::CT_TALLA_PARA_EDAD => [
            "YAXISNAME" => "Longitud/estatura (cm)",
            "D" => [
                "COLUMNS" => "perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.valor",
                "LINEAS" => [0 => 0, 2 => 60, 6 => 182, 12 => 365, 60 => 1825],
                "MINX" => 0,
                "MINY" => 40,
                "MAXX" => 60,
                "SPLITNUMBERX" => 30,
                "SPLITNUMBERY" => 16,
                "subtitle" => 'Percentiles (Nacimiento a 5 años)',
            ],
            "M" => [
                "COLUMNS" => "perd.p999,perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.p01,perd.valor",
                "LINEAS" => [61 => 61, 72 => 73, 84 => 84, 96 => 96, 108 => 108, 120 => 120, 132 => 132, 144 => 144, 156 => 156, 168 => 168, 180 => 180, 192 => 192, 204 => 204, 228 => 228],
                "MINX" => 60,
                "MINY" => 90,
                "MAXX" => 228,
                "SPLITNUMBERX" => 57,
                "SPLITNUMBERY" => 12,
                "subtitle" => 'Percentiles (5 a 19 años)',
            ],
            "signo_id" => self::CT_TALLA,
            self::CT_SEXO_HOMBRE => [
                'title' => 'Longitud/estatura para la edad niños',
                'color' => '#0196DA'
            ],
            self::CT_SEXO_MUJER => [
                'title' => 'Longitud/estatura para la edad niñas',
                'color' => '#E57EB5'
            ],
        ],
        self::CT_PESO_PARA_EDAD => [
            "YAXISNAME" => "Peso (kg)",
            "D" => [
                "COLUMNS" => "perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.valor",
                "LINEAS" => [0 => 0, 2 => 60, 6 => 182, 12 => 365, 60 => 1825],
                "MINX" => 0,
                "MINY" => 0,
                "MAXX" => 60,
                "SPLITNUMBERX" => 30,
                "SPLITNUMBERY" => 13,
                "subtitle" => 'Percentiles (Nacimiento a 5 años)',
            ],
            "M" => [
                "COLUMNS" => "perd.p99,perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.p01,perd.valor",
                "LINEAS" => [61 => 61, 72 => 72, 84 => 84, 96 => 96, 108 => 108, 120 => 120],
                "MINX" => 60,
                "MINY" => 10,
                "MAXX" => 120,
                "SPLITNUMBERX" => 21,
                "SPLITNUMBERY" => 10,
                "subtitle" => 'Percentiles (5 a 10 años)',
            ],
            "signo_id" => self::CT_PESO,
            self::CT_SEXO_HOMBRE => [
                'title' => 'Peso para la edad niños',
                'color' => '#0196DA'
            ],
            self::CT_SEXO_MUJER => [
                'title' => 'Peso para la edad niñas',
                'color' => '#E57EB5'
            ],
        ],
        self::CT_IMC_PARA_EDAD => [
            "YAXISNAME" => "IMC (kg/m²)",
            "D" => [
                "COLUMNS" => "perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.valor",
                "LINEAS" => [0 => 0, 2 => 60, 4 => 120, 6 => 182, 8 => 240, 10 => 300, 12 => 365, 24 => 720, 36 => 1080, 48 => 1440, 60 => 1825],
                "MINX" => 0,
                "MINY" => 10,
                "MAXX" => 60,
                "SPLITNUMBERX" => 30,
                "SPLITNUMBERY" => 12,
                "subtitle" => 'Percentiles (Nacimiento a 5 años)',
            ],
            "M" => [
                "COLUMNS" => "perd.p99,perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.p01,perd.valor",
                "LINEAS" => [61 => 61, 72 => 73, 84 => 84, 96 => 96, 108 => 108, 120 => 120, 132 => 132, 144 => 144, 156 => 156, 168 => 168, 180 => 180, 192 => 192, 204 => 204, 216 => 216, 228 => 228],
                "MINX" => 60,
                "MINY" => 10,
                "MAXX" => 228,
                "SPLITNUMBERX" => 57,
                "SPLITNUMBERY" => 12,
                "subtitle" => 'Percentiles (5 a 19 años)',
            ],
            "signo_id" => self::CT_IMC,
            self::CT_SEXO_HOMBRE => [
                'title' => 'IMC para la edad niños',
                'color' => '#0196DA'
            ],
            self::CT_SEXO_MUJER => [
                'title' => 'IMC para la edad niñas',
                'color' => '#E57EB5'
            ],
        ],
        self::CT_PESO_PARA_LONGITUDTALLA => [
            "YAXISNAME" => "Peso (kg)",
            "COLUMNS" => "perd.p97,perd.p85,perd.p50,perd.p15,perd.p3,perd.valor",
            "signo_id" => self::CT_PESO,
            "T" => [
                "signo_id" => self::CT_TALLA,
                "subtitle" => 'Percentiles (2 a 5 años)',
                "XAXISNAME" => "Estatura (kg)",
                "LINEAS" => [45 => 45, 50 => 50, 55 => 55, 60 => 60, 65 => 65, 70 => 70, 75 => 75, 80 => 80, 85 => 85, 90 => 90, 95 => 95, 100 => 100, 105 => 105, 110 => 110, 115 => 115, 120 => 120],
                "MINX" => 65,
                "MINY" => 4,
                "MAXX" => 120,
                "SPLITNUMBERX" => 12,
                "SPLITNUMBERY" => 12,
                self::CT_SEXO_HOMBRE => [
                    'title' => 'Peso para la estatura niños',
                    'color' => '#0196DA'
                ],
                self::CT_SEXO_MUJER => [
                    'title' => 'Peso para la estatura niñas',
                    'color' => '#E57EB5'
                ],
            ],
            "L" => [
                "signo_id" => self::CT_TALLACORREGIDA,
                "subtitle" => 'Percentiles (Nacimiento a 2 años)',
                "XAXISNAME" => "Longitud (kg)",
                "LINEAS" => [45 => 45, 50 => 50, 55 => 55, 60 => 60, 65 => 65, 70 => 70, 75 => 75, 80 => 80, 85 => 85, 90 => 90, 95 => 95, 100 => 100, 105 => 105, 110 => 110],
                "MINX" => 45,
                "MINY" => 0,
                "MAXX" => 110,
                "SPLITNUMBERX" => 12,
                "SPLITNUMBERY" => 12,
                self::CT_SEXO_HOMBRE => [
                    'title' => 'Peso para la longitud niños',
                    'color' => '#0196DA'
                ],
                self::CT_SEXO_MUJER => [
                    'title' => 'Peso para la longitud niñas',
                    'color' => '#E57EB5'
                ],
            ]
        ]
    ];


    /**
     * @const integer Id del registro detalle catalogo con evidencia patológica
     */
    const CT_CONEVIDENCIAPATOLOGICA = 420;

    /**
     * @const integer Id del registro detalle catalogo Sin evidencia patológica
     */
    const CT_SINEVIDENCIAPATOLOGICA = 421;

    /**
     * @const integer Id del registro detalle catalogo de dispensarización del riesfo obstetrico
     */
    const CT_GINECO_DISPENSARIZACION = 499;
    /**
     * @const integer Id del registro detalle catalogo de plan de transporte
     */
    const CT_GINECO_PLAN_TRANSPORTE = 500;
    /**
     * @const integer Id del registro detalle catalogo de método para determinar las semanas de gestación
     */
    const CT_GINECO_METODO_CALCULO = 501;

    /**
     * @const integer Id del registro detalle catalogo de Fecha de última mestruación
     */
    const CT_GINECO_FUM = 505;

    /**
     * @const integer Id del registro detalle catalogo de semanas de gestación
     */
    const CT_GINECO_SEMANAS_GESTACION = 506;

    /**
     * @const integer Id del registro detalle catalogo de madre en periodo de lactancia
     */
    const CT_GINECO_PERIODO_LACTANCIA = 507;

    /**
     * @const integer Id del registro detalle catalogo de plan de parto
     */
    const CT_GINECO_PLAN_PARTO = 329;


    /**
     * @const integer Id del registro detalle catalogo gineco de otros
     */
    const CT_GINECO_OTROS = 379;


    /**
     * @const integer Id del registro detalle catalogo gineco de gestas previas
     */
    const CT_GINECO_GESTAS_PREVIAS = 796;


    /**
     * @const integer Id del registro detalle catalogo gineco de embarazo molar
     */
    const CT_GINECO_EMB_MOLAR = 797;

    /**
     * @const integer Id del registro detalle catalogo gineco de embarazo ectópico
     */
    const CT_GINECO_EMB_ECTOPICA = 798;

    /**
     * @const integer Id del registro detalle catalogo gineco de abortos
     */
    const CT_GINECO_ABORTOS = 799;

    /**
     * @const integer Id del registro detalle catalogo partos
     */
    const CT_GINECO_PARTOS = 800;

    /**
     * @const integer Id del registro detalle catalogo partos
     */
    const CT_GINECO_VAGINALES = 801;


    /**
     * @const integer Id del registro detalle catalogo partos
     */
    const CT_GINECO_CESAREAS = 802;

    /**
     * @const integer Id del registro detalle catalogo partos
     */
    const CT_GINECO_NACIDOS_VIVOS = 803;

    /**
     * @const integer Id del registro de nacidos muertos
     */
    const CT_GINECO_NACIDOS_MUERTOS = 804;


    /**
     * @const integer Id del registro viven
     */
    const CT_GINECO_VIVEN = 805;


    /**
     * @const integer Id del registro muerts 2da semana
     */
    const CT_GINECO_MUERTOS_1ERA_SEMANA = 806;


    /**
     * @const integer Id del registro muertos 2da semana
     */
    const CT_GINECO_MUERTOS_2DA_SEMANA = 807;


    /**
     * @const integer Id del fin del embarazo anterior
     */
    const CT_GINECO_FIN_EMBARAZO_ANTERIOR = 808;


    /**
     * @const integer Id del embarazo planeado
     */
    const CT_GINECO_EMBARAZO_PLANEADO = 809;


    /**
     * @const integer Id del fracaso metodo anticonceptivo
     */
    const CT_GINECO_FRACASO_METODO_ANTICONCEPTIVO = 810;


    /**
     * @const integer Id del ultimo embarazo
     */
    const CT_GINECO_ULTIMO_EMBARAZO = 811;

    /**
     * @const integer Id del ultimo embarazo
     */
    const CT_GINECO_EMBARAZO_MULTIPLE = 812;

    /**
     * @const integer Id de 3 espontaneos consecutivos
     */
    const CT_GINECO_3_ESPONTANEOS_CONSECUTIVOS = 813;


    /**
     * @const integer Id del registro detalle catalogo estado de la atención Cancelada
     */
    const CT_ATENCIONCANCELADA = 486;


    /**
     * @const integer Id del registro detalle catalogo input dinamico tipo boolean
     */
    const CT_DYNAMIC_INPUT_BOOLEAN = 527;

    /**
     * @const integer Id del registro detalle catalogo input dinamico tipo opciones
     */
    const CT_DYNAMIC_INPUT_OPTIONS = 528;

    /**
     * @const integer Id del registro detalle catalogo input dinamico tipo texto
     */
    const CT_DYNAMIC_INPUT_TEXT = 529;

    /**
     * @const integer Id del registro detalle catalogo input dinamico tipo seleccion
     */
    const CT_DYNAMIC_INPUT_SELECTION = 530;

    /**
     * @const integer Id del registro detalle catalogo input dinamico tipo numérico
     */
    const CT_DYNAMIC_INPUT_NUMERIC = 531;

    /**
     * @const integer Id del registro detalle catalogo input dinamico tipo fecha
     */
    const CT_DYNAMIC_INPUT_DATE = 532;


    /**
     * @const integer Id del registro detalle catalogo de durante las últimas 24 horas recibió alimentos solido, semisolido o suave
     */
    const CT_ANTECEDENTES_GINECO = [
        Constantes::CT_GINECO_GESTAS_PREVIAS,
        Constantes::CT_GINECO_EMB_MOLAR,
        Constantes::CT_GINECO_EMB_ECTOPICA,
        Constantes::CT_GINECO_ABORTOS,
        Constantes::CT_GINECO_PARTOS,
        Constantes::CT_GINECO_VAGINALES,
        Constantes::CT_GINECO_CESAREAS,
        Constantes::CT_GINECO_NACIDOS_VIVOS,
        Constantes::CT_GINECO_NACIDOS_MUERTOS,
        Constantes::CT_GINECO_VIVEN,
        Constantes::CT_GINECO_MUERTOS_1ERA_SEMANA,
        Constantes::CT_GINECO_MUERTOS_2DA_SEMANA,
        Constantes::CT_GINECO_FIN_EMBARAZO_ANTERIOR,
        Constantes::CT_GINECO_EMBARAZO_PLANEADO,
        Constantes::CT_GINECO_FRACASO_METODO_ANTICONCEPTIVO,
        Constantes::CT_GINECO_ULTIMO_EMBARAZO,
        Constantes::CT_GINECO_EMBARAZO_MULTIPLE,
        Constantes::CT_GINECO_3_ESPONTANEOS_CONSECUTIVOS
    ];

    /**
     * @const Arreglo de los antedentes de estado nutricional
     */
    const CT_ANTECEDENTES_ESTADONUTRICIONAL = [
        Constantes::CT_ANTECEDENTE_RECIBIOSOLOLECHEMATERNA24H,
        Constantes::CT_ANTECEDENTE_RECIBIOSOLOLECHEMATERNA,
        Constantes::CT_ANTECEDENTE_CONSUMIOSOLIDOSEMISOLIDO,
        Constantes::CT_ANTECEDENTE_HARECIBIDOHIERROMULTIVITAMINAS,
        Constantes::CT_ANTECEDENTE_HARECIBIDOVITAMINA_A,
    ];


    /**
     * @const integer arreglo de los signos vitales obligatorios
     */
    const CT_SIGNOS_OBLIGATORIOS = [
        Constantes::CT_TEMPERATURA,
        Constantes::CT_PRESION_SISTOLICA,
        Constantes::CT_PRESION_ASISTOLICA,
        Constantes::CT_FRECUECIA_CARDIACA,
        Constantes::CT_FRECUECIA_RESPIRATORIA,
        Constantes::CT_IMC,
        Constantes::CT_PESO,
        Constantes::CT_TALLA,
        Constantes::CT_TALLACORREGIDA,
        Constantes::CT_PERIMETRO_ABDOMINAL,
        Constantes::CT_PERIMETRO_CEFALICO,
        Constantes::CT_PRESION_MEDIA,
        Constantes::CT_SATURACION,
        Constantes::CT_GLUCOSA_CAPILAR,
        Constantes::CT_HEMOGLOBINA,
        Constantes::CT_HEMOGLOBINA_CORREGIDO,
    ];


    /**
     * @const array Id e imagen del registro detalle catalogo donde es encuentra los organos y sistemas
     */
    const IMG_ARRAY_ORGANOS_SISTEMAS_HOMBRE = array(
        '387' => 'hombre.jpg',
        '388' => 'hrespiratorio.jpg',
        '389' => 'hcirculatorio.jpg',
        '390' => 'hdigestivo.jpg',
        '391' => 'hreproductivo.jpg',
        '392' => 'hreproductivo.jpg',
        '393' => 'hesqueletico.jpg',
        '394' => 'hombre.jpg',
        '395' => 'hombre.jpg',
        '396' => 'hombre.jpg',
        '397' => 'hmuscular.jpg',
        '398' => 'hombre.jpg'
    );

    /**
     * @const array Id e imagen del registro detalle catalogo donde es encuentra los organos y sistemas
     */
    const IMG_ARRAY_ORGANOS_SISTEMAS_MUJER = array(
        '387' => 'mujer.jpg',
        '388' => 'mrespiratorio.jpg',
        '389' => 'mcirculatorio.jpg',
        '390' => 'mdigestivo.jpg',
        '391' => 'mreproductivo.jpg',
        '392' => 'mreproductivo.jpg',
        '393' => 'mesqueletico.jpg',
        '394' => 'mnervioso.jpg',
        '395' => 'mujer.jpg',
        '396' => 'mujer.jpg',
        '397' => 'mmuscular.jpg',
        '398' => 'mujer.jpg'
    );

    /**
     * @const integer Id del registro detalle catalogo para identificar la región anatómica de Piel Faneras
     */
    const CT_REGION_PIEL_FANERAS_PADRE = 411;

    /**
     * @const array  registra las regiones anatómicas que se muestran en la parte frontal del cuerpo humano
     */
    const CT_REGIONES_ANATOMICAS_FRONTALES = array(403, 404, 405, 406, 407, 409, 410);

    /**
     * @const array  registra las regiones anatómicas que se muestran en la parte posterior del cuerpo humano
     */
    const CT_REGIONES_ANATOMICAS_POSTERIORES = array(408);

    /**
     * @const array integer Id del registro detalle catalogo de antescedentes alergicos
     */
    const CT_ANTECEDENTE_PERSONAL_ALERGICO = 248;

    /**
     * @const array integer Id del registro detalle catalogo del Grupo Sanguíneo
     */
    const CT_ANTECEDENTE_PERSONAL_SANGUINEO = 533;

    /**
     * @const integer Id  Detalle Catalogo para registrar el grupo prioritario de embarazadas
     */
    const CT_GRUPO_PRIORITARIO_EMBARAZADA = 634;

    /**
     * @const integer Id  Detalle Catalogo para registrar el grupo prioritario de personas con discapacidad
     */
    const CT_GRUPO_PRIORITARIO_DISCAPACITADO = 1083;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de enfermeria
     */
    const CT_ESPECIALIDAD_ENFERMERIA = 665;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Enfermería Rural
     */
    const CT_ESPECIALIDAD_ENFERMERIA_RURAL = 667;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Auxiliar de Enfermería
     */
    const CT_ESPECIALIDAD_ENFERMERIA_AUXILIAR = 666;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Audiología / Foniatría
     */
    const CT_ESPECIALIDAD_AUDIOLOGIA = 134;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Biología Molecular
     */
    const CT_ESPECIALIDAD_BIOLOGIA = 135;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Bioquímica
     */
    const CT_ESPECIALIDAD_BIOQUIMICA = 136;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Epidemiología, Medicina Tropical
     */
    const CT_ESPECIALIDAD_EPIDEMIOLOGIA = 150;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Gerencia en Administración Hospitalaria
     */
    const CT_ESPECIALIDAD_GERENCIAHOSPITALARIA = 156;


    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Ginecologia y obstetricia
     */
    const CT_ESPECIALIDAD_GINECOLOGIA_OBSTETRICIA = 158;


    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Microbiología
     */
    const CT_ESPECIALIDAD_MICROBIOLOGIA = 177;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Psicología Clínica
     */
    const CT_ESPECIALIDAD_PSICOLOGIACLINICA = 655;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Psicorehabilitador
     */
    const CT_ESPECIALIDAD_PSICOREHABILITADOR = 656;


   /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Psiquiatria
     */
    const CT_ESPECIALIDAD_PSIQUIATRIA = 657;


    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Psiquiatría Infantil y del Adolescente
     */
    const CT_ESPECIALIDAD_PSIQUIATRIA_INFANTIL_Y_DEL_ADOLESCENTE = 658;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Salud Pública
     */
    const CT_ESPECIALIDAD_SALUDPUBLICA = 660;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Subespecialidad
     */
    const CT_ESPECIALIDAD_SUBESPECIALIDAD = 661;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Ultrasonido
     */
    const CT_ESPECIALIDAD_ULTRASONIDO = 663;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Imagenología y Diagnóstico por imagen
     */
    const CT_ESPECIALIDAD_IMAGEDIAGNIMAGEN = 161;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Laboratorio Clínico e Histopatológico
     */
    const CT_ESPECIALIDAD_LABORATORICLIHISTOPATOLOGICO = 166;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Endocrinología
     */
    const CT_ESPECIALIDAD_ENDOCRINOLOGIA = 149;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Medicina Familiar
     */
    const CT_ESPECIALIDAD_MEDICINAFAMILIAR = 173;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Atencion Primaria de salud
     */
    const CT_ESPECIALIDAD_ATENCIONPRIMARIASALUD = 133;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Pediatria
     */
    const CT_ESPECIALIDAD_PEDIATRIA = 653;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Medicina General Integral
     */
    const CT_ESPECIALIDAD_MEDICINAGENERALINTEGRAL = 174;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Medicina Rural
     */
    const CT_ESPECIALIDAD_MEDICINARURAL = 668;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de nutricion
     */
    const CT_ESPECIALIDAD_NUTRICION = 646;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de nutricion clinica
     */
    const CT_ESPECIALIDAD_NUTRICIONCLINICA = 647;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de genetica clinica
     */
    const CT_ESPECIALIDAD_GENETICACLINICA = 154;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Laboratorio Clínico e Histopatológico
     */
//    const CT_ESPECIALIDAD_MEDICINAGENETICA = 166;

    /**
     * @const integer catalogo de especialidad de enfermeria
     */
    const CT_ESPECIALIDADES_ESTADONUTRICIONAL = array(
        Constantes::CT_ESPECIALIDAD_ENDOCRINOLOGIA,
        Constantes::CT_ESPECIALIDAD_MEDICINAFAMILIAR,
        Constantes::CT_ESPECIALIDAD_ATENCIONPRIMARIASALUD,
        Constantes::CT_ESPECIALIDAD_PEDIATRIA,
        Constantes::CT_ESPECIALIDAD_MEDICINAGENERALINTEGRAL,
        Constantes::CT_ESPECIALIDAD_MEDICINARURAL,
        Constantes::CT_ESPECIALIDAD_GENETICACLINICA,
        Constantes::CT_ESPECIALIDAD_NUTRICION,
        Constantes::CT_ESPECIALIDAD_NUTRICIONCLINICA,
    );

    /**
     * @const integer catalogo de especialidad de enfermeria
     */
    const CT_ESPECIALIDADES_ENFERMERIA = array(
        Constantes::CT_ESPECIALIDAD_ENFERMERIA,
        Constantes::CT_ESPECIALIDAD_ENFERMERIA_RURAL,
        Constantes::CT_ESPECIALIDAD_ENFERMERIA_AUXILIAR
    );

    /**
     * @const integer Id del registro Detalle catalogo de sección intramural y extramural
     */
    const CT_SECINTRAEXTRAMURAL = 728;

    /**
     * @const integer Id del registro Detalle catalogo deL tipo de atención  intramural
     */
    const CT_INTRAMURAL = 710;

    /**
     * @const integer Id del registro Detalle catalogo del tipo de atención  extramural
     */
    const CT_EXTRAMURAL = 711;

    /**
     * @const integer Id del registro Detalle catalogo del lugar de atencion Establecimiento
     */
    const CT_LUGARATENCION_ESTABLECIMIENTO = 712;

    /**
     * @const integer Id del registro Detalle catalogo del órdenes médicas
     */
    const CT_ORDENES_MEDICAS = 764;
    /**
     * @const integer Id del registro catalogo de sección Odontogrma
     */
    const CT_SECODONTOGRAMA = 763;

    /**
     * @const integer Id del registro catalogo de sección Grupos prioritarios
     * */
    const CT_SECGRUPO_PRIORITARIO = 787;

    /**
     * @const integer Id del registro Detalle catalogo de sección vacunación
     */
    const CT_SECVACUNACION = 814;

    /**
     * @const integer Id del registro Detalle catalogo de sección referencia de atencion
     */
    const CT_REFERENCIA = 997;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de SUPERFICIE CARIADA
     * */
    const CT_SUPERFICIE_CARIADA = 751;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de SUPERFICIE OBTURADA
     */

    const CT_SUPERFICIE_OBTURADA = 752;
    /**
     * * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL QUE REQUIERE SELLANTE
     */

    const CT_PIEZA_DENTAL_QUE_REQUIERE_SELLANTE = 753;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL CON SELLANTE
     */

    const CT_PIEZA_DENTAL_CON_SELLANTE = 754;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL QUE REQUIERE EXTRACCION
     */

    const CT_PIEZA_DENTAL_QUE_REQUIERE_EXTRACCION = 755;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL PERDIDA POR CARIES
     */

    const CT_PIEZA_DENTAL_PERDIDA_POR_CARIES = 756;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL PERDIDA POR OTRA CAUSA
     */

    const CT_PIEZA_DENTAL_PERDIDA_POR_OTRA_CAUSA = 757;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL QUE REQUIERE ENDODONCIA
     */

    const CT_PIEZA_DENTAL_QUE_REQUIERE_ENDODONCIA = 758;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PIEZA DENTAL CON ENDODONCIA REALIZADA
     */

    const CT_PIEZA_DENTAL_CON_ENDODONCIA_REALIZADA = 759;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de CORONA POR REALIZAR
     */

    const CT_CORONA_POR_REALIZAR = 760;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de CORONA REALIZADA
     */

    const CT_CORONA_REALIZADA = 761;
    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PROTESIS FIJA POR REALIZAR
     */

    const CT_PROTESIS_FIJA_POR_REALIZAR = 762;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PROTESIS FIJA REALIZADA
     */

    const CT_PROTESIS_FIJA_REALIZADA = 815;

    /**
     * * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PROTESIS REMOVIBLE REALIZADA
     */
    const CT_PROTESIS_REMOVIBLE_REALIZADA = 816;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PROTESIS REMOVIBLE POR REALIZADA
     */
    const CT_PROTESIS_REMOVIBLE_POR_REALIZAR = 817;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PROTESIS TOTAL POR REALIZADAR
     */

    const CT_PROTESIS_TOTAL_POR_REALIZAR = 818;

    /** @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PROTESIS TOTAL REALIZADA */
    const CT_PROTESIS_TOTAL_REALIZADA = 819;

    /**
     * @const string edad minima para ingresar presión arterial del paciente //aammdd
     */
    const EDAD_MINIMA_PRESION_ARTERIAL = '150001';

    /**
     * @const string edad maxima para ingresar presión arterial del paciente //aammdd
     */
    const EDAD_MAXIMA_PRESION_ARTERIAL = '1201129';

    /*
    * Constantes de epidemiología
    */

    /**
     * @const integer indica si el modulo de epidemiología se encuentra activo
     */
    const MODULOEPIDEMIOLOGIACTIVO = false;

    /**
     * @const integer indica si el modulo de epidemiología se encuentra activo
     */
    const MODULOSIGNOSINTOMACTIVO = false;

    /**
     * @const integer indica el estado del registro de epidemiología como creado
     */
    const ESTADOEPINDIVIDUALCREADO = 0;

    /**
     * @const integer indica el identificador del catálogo de signos y síntomas
     */
    const CT_SINTOMAS_SIGNOS = 59;

    /**
     * @const integer indica el estado que determina si un caso a finalizado en el fallecimiento del paciente
     */
    const ESTADOEGRESOVIVO = 710;

    /**
     * @const integer indica el estado que determina si un caso a finalizado en el fallecimiento del paciente
     */
    const ESTADOEGRESOMUERTO = 711;

    /**
     * @const integer indica el estado del registro de ficha de investigación epidemiológica
     */
    const ESTADOFICHAINVESTIGACIONCREADO = 0;

    /**
     * @const integer indica el identificador del catálogo de evolución de la ficha de investigación
     */
    const CT_FICHA_EVOLUCION = 17;

    /**
     * @const integer indica el identificador del catálogo lugar de tratamiento de la ficha de investigación
     */
    const CT_FICHA_LUGARTRATAMIENTO = 27;

    /**
     * @const integer indica el identificador del catálogo servicios de la ficha de investigación
     */
    const CT_FICHA_SERVICIO = 28;

    /**
     * @const integer indica el identificador del catálogo entidades de la ficha de investigación
     */
    const CT_FICHA_ENTIDADES = 3836;

    /**
     * @const integer indica el identificador del catálogo condición egreso de la ficha de investigación
     */
    const CT_FICHA_CONDICIONEGRESO = 41;

    /**
     * @const array indica el mensaje y color de alerta de un diagnóstico de epidemiología
     */
    const ARRAY_CT_SEMAFOROEPIDEMIOLOGIA = array(
        1 => array(
            'clase' => 'danger',
            'dias' => 1,
            'mensaje' => 'Intervención e investigación inmediata dentro de <strong>24 horas</strong>'
        ),
        2 => array(
            'clase' => 'warning',
            'dias' => 2,
            'colorfondo' => '#fffde7',
            'mensaje' => 'Intervención e investigación dentro de <strong>48 horas</strong>'
        ),
        3 => array(
            'clase' => 'success',
            'dias' => 0,
            'colorfondo' => '#e8f5e9',
            'mensaje' => 'Notificación y análisis, tiempo indeterminado'
        )
    );

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PILAR_PROTESIS_FIJA_POR_REALIZAR
     */
    const CT_PILAR_PROTESIS_FIJA_POR_REALIZAR = 939;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PILAR_PROTESIS_FIJA_REALIZADA
     */
    const CT_PILAR_PROTESIS_FIJA_REALIZADA = 940;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PILAR_PROTESIS_REMOVIBLE_REALIZADA
     */
    const CT_PILAR_DERECHO_PROTESIS_REMOVIBLE_REALIZADA = 941;

    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PILAR_PROTESIS_REMOVIBLE_POR REALIZAR
     */
    const CT_PILAR_DERECHO_PROTESIS_REMOVIBLE_POR_REALIZAR = 942;


    /**
     * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PILAR_PROTESIS_REMOVIBLE_REALIZADA
     */
    const CT_PILAR_IZQUIERDO_PROTESIS_REMOVIBLE_REALIZADA = 982;

    /**      * @const integer Id del registro detalle catalogo para identificar la simbología del odontograma  de PILAR_PROTESIS_REMOVIBLE_POR REALIZAR */
    const CT_PILAR_IZQUIERDO_PROTESIS_REMOVIBLE_POR_REALIZAR = 983;

    /**      * @const integer Id del registro detalle catalogo para identificar la superficie dental  de VESTIBULAR */
    const CT_VESTIBULAR = 745;

    /**     * @const integer Id del registro detalle catalogo para identificar la superficie dental  de MESIAL */
    const CT_MESIAL = 746;

    /**      * @const integer Id del registro detalle catalogo para identificar la superficie dental  de OCLUSAL */
    const CT_OCLUSAL = 747;

    /**     * @const integer Id del registro detalle catalogo para identificar la superficie dental  de DISTAL */
    const CT_DISTAL = 748;

    /**    * @const integer Id del registro detalle catalogo para identificar la superficie dental  de LINGUAL */
    const CT_LINGUAL = 750;

    /**
     * @const integer Id del registro Detalle catalogo para describir la enfermedad periodental movilidad
     */
    const CT_MOVILIDAD = 995;

    /**
     * @const integer Id del registro Detalle catalogo para describir la enfermedad periodental recesion
     */
    const CT_RECESION = 994;

    /**
     * @const integer Id del registro catalogo de sección Odontogrma
     */
    const CT_SECPROCEDIMIENTOSREALIZADOS = 998;

    /**
     * @const integer de la edad mínima anios fertil
     */
    const EDADMINANIOS_FERTIL = 10;

    /**
     * @const integer de la edad mínima meses fertil
     */
    const EDADMINMESES_FERTIL = 0;

    /**
     * @const integer de la edad mínima dias fertil
     */
    const EDADMINDIAS_FERTIL = 0;

    /**
     * @const integer de la edad máxima anios fertil
     */
    const EDADMAXANIOS_FERTIL = 48;

    /**
     * @const integer de la edad máxima meses fertil
     */
    const EDADMAXMESES_FERTIL = 11;

    /**
     * @const integer de la edad máxima dias fertil
     */
    const EDADMAXDIAS_FERTIL = 29;

    /**
     * @const integer de la edad mínima de menospausia
     */
    const EDADMIN_MENOPAUSIA = 49;

    /**
     * @const integer Id del registro catalogo de sección Examen Físico Regional Estomatognático
     */
    const CT_SECEXAMENFISICOREGIONALESTOMATOGNATICO = 1007;

    /**
     * @const integer Id del registro Otros/Esquefique del catalogo catalogo motivo de referencia
     */
    const CT_MOTIVOREFERENCIA_OTROS = 1027;

    /**
     * @const integer de edad minima para signo vital perímetro cefálico
     */
    const CT_PERIMETRO_CEFALICO_EDAD_MINIMA = 0;

    /**
     * @const integer de edad maxima para signo vital perímetro cefálico
     */
    const CT_PERIMETRO_CEFALICO_EDAD_MAXIMA = 2;

    /**
     * @const integer Id del registro el tipo de odontograma inicial
     */
    const CT_ODONTOGRAMA_INICIAL = 1070;

    /**
     * @const integer Id del registro el tipo de odontograma procedimientos
     */
    const CT_ODONTOGRAMA_PROCEDIMIENTOS = 1071;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Odontologia
     */
    const CT_ESPECIALIDAD_ODONTOLOGIA = 669;

    /**
     * @const array con el id de las superficies
     */
    const ARRAY_CT_SUPERFICIES = array(
        Constantes::CT_VESTIBULAR => ' vestibular',
        Constantes::CT_MESIAL => ' mesial',
        Constantes::CT_OCLUSAL => ' oclusal',
        Constantes::CT_DISTAL => ' distal',
        Constantes::CT_LINGUAL => ' lingual o palatina',

    );

    /**
     * @const integer Id del registro catalogo del grupo de antecedente desarrollo
     */
    const CT_GRUPOANTECEDENTE_DESARROLLO = 1074;

    /**
     * @const integer Id del registro catalogo del grupo de antecedente generales
     */
    const CT_GRUPOANTECEDENTE_GENERALES = 1073;

    /**
     * @const integer Id del registro catalogo del grupo de antecedente metodo anticonceptivo
     */
    const CT_GRUPOANTECEDENTE_METODOANTICONCEPTIVO = 1076;

    /**
     * @const integer Id del registro catalogo del grupo de antecedente sexualidad
     */
    const CT_GRUPOANTECEDENTE_SEXUALIDAD = 1075;

    /**
     * @const integer Id del registro catalogo del grupo de antecedente sexualidad
     */
    const CT_GRUPOANTECEDENTE_HABITOSHIGUIENICOSBUCALES = 1089;

    /**
     * @const integer Id del registro catalogo del antecedente de menarquia
     */
    const CT_ANTECEDENTE_MENARQUIA = 1079;

    /**
     * @const integer Id del registro catalogo del antecedente de menopausia
     */
    const CT_ANTECEDENTE_MENOPAUSIA = 1080;

    /**
     * @const integer Id del registro catalogo del antecedente de fecha de última menstruación
     */
    const CT_ANTECEDENTE_FUM = 505;

    /**
     * @const integer Id del registro catalogo del antecedente de fecha de última citologia
     */
    const CT_ANTECEDENTE_FUC = 1077;

    /**
     * @const integer Id del registro catalogo del antecedente de terapia hormonal
     */
    const CT_ANTECEDENTE_TERAPIAHORMONAL = 1078;

    /**
     * @const integer Id del registro catalogo del Uso actual de metodos anticonceptivo
     */
    const CT_ANTECEDENTE_UAMA = 1082;

    /**
     * @const integer Id del registro catalogo del antecedente de relaciones sexuales
     */
    const CT_ANTECEDENTE_RELACIONESSEXUALES = 1081;

    /**
     * @const integer Id del registro catalogo casa hogar o acogimiento
     */
    const CT_LUGAR_ATENCIONCASAHOGAR = 1119;

    /**
     * @const integer Id del registro catalogo centro gerontológico
     */
    const CT_LUGAR_ATENCIONCENTROGER = 1120;


    /**
     * @const integer Id del registro catalogo cepillado dental 1 vez al día
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS_CEPILLADO_1_VEZ = 1090;

    /**
     * @const integer Id del registro catalogo cepillado dental 2 vez al día
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS_CEPILLADO_2_VEZ = 1091;

    /**
     * @const integer Id del registro catalogo cepillado dental 3 vez al día
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS_CEPILLADO_3_VEZ = 1092;


    /**
     * @const integer Id del registro catalogo cepillado dental mas de 3 vez al día
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS_CEPILLADO_MAS_3_VEZ = 1093;


    /**
     * @const integer Id del registro catalogo cepillado dental 3 vez al día
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS_APARATOS_INTRA = 1098;


    /**
     * @const integer Id del registro catalogo cepillado dental 3 vez al día
     */
    const CT_ANTECEDENTE_ODONTOLOGICOS_HiGIEN_APARATOS_INTRA = 1099;


    /**  * @const integer Id del registro detallecatalogo que permite registrar los niveles patológicos para los indicadores de salud bucal LEVE */
    const CT_SALUD_BUCAL_LEVE = 1121;
    /**  * @const integer Id del registro detallecatalogo que permite registrar los niveles patológicos para los indicadores de salud bucal MODERADA */
    const CT_SALUD_BUCAL_MODERADA = 1122;
    /**  * @const integer Id del registro detallecatalogo que permite registrar los niveles patológicos para los indicadores de salud bucal SEVERA */
    const CT_SALUD_BUCAL_SEVERA = 1123;
    /**  * @const integer Id del registro detallecatalogo que permite registrar los niveles patológicos para los indicadores de salud bucal ANGLE */
    const CT_SALUD_BUCAL_ANGLEI = 1124;
    /**  * @const integer Id del registro detallecatalogo que permite registrar los niveles patológicos para los indicadores de salud bucal ANGLEII */
    const CT_SALUD_BUCAL_ANGLEII = 1125;
    /**  * @const integer Id del registro detallecatalogo que permite registrar los niveles patológicos para los indicadores de salud bucal ANGLEIII */
    const CT_SALUD_BUCAL_ANGLEIII = 1126;


    const ARRAY_INDICE_OBTURADA = array(
        Constantes::CT_PIEZA_DENTAL_CON_ENDODONCIA_REALIZADA
    ,
        Constantes::CT_PILAR_DERECHO_PROTESIS_REMOVIBLE_REALIZADA
    ,
        Constantes::CT_CORONA_REALIZADA
    ,
        Constantes::CT_SUPERFICIE_OBTURADA
    );

    const ARRAY_INDICE_CARIES = array(
        Constantes::CT_PIEZA_DENTAL_QUE_REQUIERE_ENDODONCIA
    ,
        Constantes::CT_SUPERFICIE_CARIADA
    ,
        Constantes::CT_CORONA_POR_REALIZAR
    );

    const ARRAY_INDICE_PERDIDA = array(
        Constantes::CT_PIEZA_DENTAL_QUE_REQUIERE_EXTRACCION
    ,
        Constantes::CT_PIEZA_DENTAL_PERDIDA_POR_CARIES
    ,
        Constantes::CT_PROTESIS_FIJA_REALIZADA
    ,
        Constantes::CT_PROTESIS_FIJA_POR_REALIZAR
    ,
        Constantes::CT_PROTESIS_REMOVIBLE_REALIZADA
    ,
        Constantes::CT_PROTESIS_TOTAL_REALIZADA
    ,
        Constantes::CT_PROTESIS_TOTAL_POR_REALIZAR

    );

    /**
     * @const integer Id del registrode la tabla serviciotarifario para definir los servicios odontologicos
     */
    const ST_SERVICIOS_ODONTOLOGICOS = 89;

    /**
     * @constantes integer Id de detalle servicio tarifario
     */
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_SELLANTES = 1555;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_REIMPLANTES = 1573;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_RECROMIA = 1563;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_NECROPULPECTOMIA_UNIRADICULAR = 1560;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_NECROPULPECTOMIA_MULTIRADICULAR = 1561;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_LIMPIEZA_QUIRURGICA = 1574;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_FLUORIZACION = 1556;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_FERULIZACION = 1566;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_EXAMEN_HISTOPATOLOGICO = 1575;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_EMERGENCIA_CONTROL_DEL_DOLOR_Y_O_HEMORRAGIA = 1576;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_DESVITALIZACION_Y_MOMIFICACION = 1562;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_COMUNICACION_BUCONASAL = 1572;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_COMUNICACION_BUCOMAXILAR = 1571;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_CIRUGIIA_PERIRADICULAR = 1570;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_CIRUGIIA_DE_TEJIDOS_BLANDOS = 1568;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_CIRUGIA_PARA_BIOPSIA = 1567;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_CIRUGIA_APICAL = 1569;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_CEMENTACION_DE_CORONA = 1565;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_BIOPULPECTOMIA = 1557;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_BIOPULPECTOMIA_UNIRADICULAR = 1558;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_BIOPULPECTOMIA_MULTIRADICULAR = 1559;
    const TR_RESTAURACIONES_OTROS_TRATAMIENTOS_ADAPTACION_Y_CEMENTACION_DE_PERNO_PREFABRICADO = 1564;
    const TR_RESTAURACIONES_CON_RESINA_SIMPLE_UNA_SOLA_CARA = 1549;
    const TR_RESTAURACIONES_CON_RESINA_COMPUESTA_DOS_CARAS = 1550;
    const TR_RESTAURACIONES_CON_RESINA_COMPLEJA_MAS_DE_DOS_CARAS = 1551;
    const TR_RESTAURACIONES_CON_AMALGAMA_SIMPLE_UNA_SOLA_CARA = 1552;
    const TR_RESTAURACIONES_CON_AMALGAMA_COMPUESTA_DOS_CARAS = 1553;
    const TR_RESTAURACIONES_CON_AMALGAMA_COMPLEJA_MAS_DE_DOS_CARAS = 1554;
    const TR_PROTESIS_TOTAL_CON_PORCELANA_PROTESIS_SUPERIOR = 1580;
    const TR_PROTESIS_TOTAL_CON_PORCELANA_PROTESIS_SUPERIOR_E_INFERIOR = 1582;
    const TR_PROTESIS_TOTAL_CON_PORCELANA_PROTESIS_INFERIOR = 1581;
    const TR_PROTESIS_TOTAL_CON_ACRILICO_PROTESIS_SUPERIOR = 1577;
    const TR_PROTESIS_TOTAL_CON_ACRILICO_PROTESIS_SUPERIOR_E_INFERIOR = 1579;
    const TR_PROTESIS_TOTAL_CON_ACRILICO_PROTESIS_INFERIOR = 1578;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_6_O_MAS_PIEZAS = 1584;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_6_A_8_PIEZAS = 1590;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_5_PIEZAS = 1589;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_4_PIEZAS = 1588;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_3_PIEZAS = 1587;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_2_PIEZAS = 1586;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_1_PIEZA = 1585;
    const TR_PROTESIS_PARCIAL_SUPERIOR_O_INFERIOR_EN_ACRILICO_DE_1_A_5_PIEZAS = 1583;
    const TR_PERIDONCIA_PROFILAXIS = 1541;
    const TR_PERIDONCIA_PLACA_MIORRELAJANTE = 1548;
    const TR_PERIDONCIA_INJERTOS_POR_PIEZA = 1545;
    const TR_PERIDONCIA_GINGIVOPLASTIA_GINGIVECTOMIA = 1542;
    const TR_PERIDONCIA_CURETAJE = 1544;
    const TR_PERIDONCIA_COLGAJO = 1543;
    const TR_PERIDONCIA_ALAMBRADO = 1546;
    const TR_PERIDONCIA_AJUSTE_OCLUSAL = 1547;
    const TR_GENERAL_CONSULTA_ODONTOLOGICA_GENERAL = 1533;
    const TR_GENERAL_CONSULTA_ODONTOLOGICA_ESPECIALISTA = 1534;
    const TR_EXODONCIA_PIEZA_SEMIRETENIDA = 1539;
    const TR_EXODONCIA_PIEZA_RETENIDA = 1540;
    const TR_EXODONCIA_PIEZA_ERUPCIONADA = 1538;
    const TR_ENDODONCIA_UNIRADICULAR = 1535;
    const TR_ENDODONCIA_MULTIRADICULAR = 1537;
    const TR_ENDODONCIA_BIRADICULAR = 1536;
    const TR_CORONAS_CORONAS_JAKET_EN_ACRILICO = 1591;
    const TR_CORONAS_CORONA_METAL_ACRILICO = 1593;
    const TR_CORONAS_CORONA_DAVIS_INCLUIDO_PERNO_MUNON = 1592;
    const TR_CORONAS_CORONA_ACRILICO = 1594;

    const ALTITUD_HEMOGLOBINA = array(
        array('valor' => 0, 'minimo' => 0, 'maximo' => 999),
        array('valor' => 0.2, 'minimo' => 1000, 'maximo' => 1499),
        array('valor' => 0.5, 'minimo' => 1500, 'maximo' => 1999),
        array('valor' => 0.8, 'minimo' => 2000, 'maximo' => 2499),
        array('valor' => 1.3, 'minimo' => 2500, 'maximo' => 2999),
        array('valor' => 1.9, 'minimo' => 3000, 'maximo' => 3499),
        array('valor' => 2.7, 'minimo' => 3500, 'maximo' => 3999),
        array('valor' => 3.5, 'minimo' => 4000, 'maximo' => 4499),
        array('valor' => 4.5, 'minimo' => 4500, 'maximo' => 4999),
    );

    // Meses minimo para el valor de hemoglobina
    const EDAD_MINIMA_HEMOGLOBINA = 6;

    // Edad maxima para registar el peso para la edad 120 meses
    const EDAD_MAXIMA_PESO_PARA_EDAD = "120";

    // Edad maxima para registar el peso para la edad 1856 dias
    const EDAD_MAXIMA_PESO_PARA_LONGITUDTALLA = "1856";

    const COLORS_ALERTAS = array(
        '' => '',
        'verde' => '',
        'amarillo' => '#FEE074',
        'naranja' => '#f7a128',
        'rojo' => '#db6a44'
    );

    /**
     * @const integer Id del registrode Catalogo para determinar las indicaciones  del tratamiento no farmacológico
     */
    const CT_INDICACIONES_TRANOFARM = 1177;

    /**
     * @const integer Id del registrode Catalogo para determinar las Nutrición  del tratamiento no farmacológico
     */
    const CT_NUTRICION_TRANOFARM = 1427;

     /**
     * @const integer Id del registrode Catalogo para determinar las Recomendaciones  del tratamiento no farmacológico
     */
    const CT_RECOMENDACIONES_TRANOFARM = 1183;


    /**
     * @const integer Id del registro detalle catalogo Sin patológica para registrar las patologías de las regiones estomatogmaticas
     */
    const CT_SIN_PATOLOGIA_REGION_ESTOMATOGNATICO = 1159;


    /**
     * @const integer Numero minimo de caracteres de la identificacion de un extranjero
     */
    const MINIMO_CARACTERES_EXTRANJERO = 3;

    /**
     * @const integer Numero maximo de caracteres de la identificacion de un extranjero
     */
    const MAXIMO_CARACTERES_EXTRANJERO = 15;

    /**
     * @const integer Numero maximo de caracteres de la identificacion de un nacional
     */
    const MAXIMO_CARACTERES_NACIONAL = 10;


    /**
     * @const integer id del registro detalle catalogo para salud oral del catalogo de tratamiento no farmacológico
     */
    const  CT_TRATAMIENTONOFARM_SALUDORAL=1263 ;

    /**
     * @const integer id del registro detalle catalogo para salud oral del catalogo de tratamiento no farmacológico
     */
    const  CT_TRATAMIENTONOFARM_SALUDMENTAL=1317 ;

    /**
     * @const integer id del registro detalle catalogo para categorias generales  del catalogo de tratamiento no farmacológico
     */
    const  CT_TRATAMIENTONOFARM_GENERALES=1178 ;

    /**
     * @const integer Id del registro catalogo de laboratorio
     */
    const CT_LABORATORIO = 779;

    /**
     * @const integer Id del registro catalogo de imagenologia
     */
    const CT_IMAGENOLOGIA = 780;

    /**
     * @const integer Id del registro catalogo de histopatología
     */
    const CT_HISTOPATOLOGIA = 781;


    /**
     * @const integer Id del registro catalogo para identificar las solicitudes de certificado de vacunacion PENDIENTES 
     */
    const CT_CERTIFICADO_VACUNACION_PENDIENTE = 5138;

        /**
     * @const integer Id del registro catalogo para identificar las solicitudes de certificado de vacunacion ENVIADAS 
     */
    const CT_CERTIFICADO_VACUNACION_ENVIADA = 5139;


}

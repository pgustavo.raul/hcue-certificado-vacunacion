<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_catalogos.cieclasificacion")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\CieclasificacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Cieclasificacion
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_catalogos.cieclasificacion_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $versioncie
     * @ORM\Column(name="versioncie", type="string",length=10, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo versioncie")
     **/
    private $versioncie;

    /**
     * @var integer $orden
     * @ORM\Column(name="orden", type="integer", nullable=true)
     **/
    private $orden;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=300, nullable=true)
     **/
    private $nombre;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $cieclasificacion_id
     * @ORM\Column(name="cieclasificacion_id", type="integer", nullable=true)
     **/
    private $cieclasificacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Cieclasificacion")
     * @ORM\JoinColumn(name="cieclasificacion_id", referencedColumnName="id")
     **/
    private $cieclasificacion;


    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set versioncie
     * @param string versioncie
     * @return cieclasificacion
     **/
    public function setVersioncie($versioncie)
    {
        $this->versioncie = $versioncie;
        return $this;
    }

    /**
     * Get versioncie
     * @return string
     **/
    public function getVersioncie()
    {
        return $this->versioncie;
    }

    /**
     * Set orden
     * @param integer orden
     * @return cieclasificacion
     **/
    public function setOrden($orden)
    {
        $this->orden = $orden;
        return $this;
    }

    /**
     * Get orden
     * @return integer
     **/
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return cieclasificacion
     **/
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     **/
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return cieclasificacion
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return cieclasificacion
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Get estado
     * @return boolean
     */
    public function setEstado($estado = 1)
    {
        $this->estado = $estado;
    }

    /**
     * Set cieclasificacion_id
     * @param integer cieclasificacion_id
     * @return cieclasificacion
     **/
    public function setCieclasificacionId($cieclasificacion_id)
    {
        $this->cieclasificacion_id = $cieclasificacion_id;
        return $this;
    }

    /**
     * Get cieclasificacion_id
     * @return integer
     **/
    public function getCieclasificacionId()
    {
        return $this->cieclasificacion_id;
    }

    /**
     * Set cieclasificacion
     * @param \HCUE\AtencionMedicBundle\Entity\Cieclasificacion $cieclasificacion
     **/
    public function setCieclasificacion(\HCUE\AtencionMedicBundle\Entity\Cieclasificacion $cieclasificacion)
    {
        $this->cieclasificacion = $cieclasificacion;
        return $this;
    }

    /**
     * Get cieclasificacion
     * @return \HCUE\AtencionMedicBundle\Entity\Cieclasificacion
     **/
    public function getCieclasificacion()
    {
        return $this->cieclasificacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __toString()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

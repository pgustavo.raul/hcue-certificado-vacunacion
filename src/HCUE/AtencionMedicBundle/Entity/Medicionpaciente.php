<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.medicionpaciente")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\MedicionpacienteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Medicionpaciente
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.medicionpaciente_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $group_id
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     **/
    private $group_id;

    /**
     * @var integer $posiciontalla
     * @ORM\Column(name="posiciontalla", type="integer", nullable=true)
     **/
    private $posiciontalla;

    /**
     * @var string $uni
     * @ORM\Column(name="uni", type="string",length=20, nullable=true)
     **/
    private $uni;

    /**
     * @var integer $paciente_id
     * @ORM\Column(name="paciente_id", type="integer", nullable=false)
     **/
    private $paciente_id;

    /**
     * @ORM\ManyToOne(targetEntity="Paciente")
     * @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     **/
    private $paciente;

    /**
     * @var integer $atencionmedica_id
     * @ORM\Column(name="atencionmedica_id", type="integer", nullable=false)
     **/
    private $atencionmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @var integer $ctantropovital_id
     * @ORM\Column(name="ctantropovital_id", type="integer", nullable=false)
     **/
    private $ctantropovital_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctantropovital_id", referencedColumnName="id")
     **/
    private $ctantropovital;

    /**
     * @var string $valor
     * @ORM\Column(name="valor", type="string",length=30, nullable=true)
     **/
    private $valor;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var integer $zona_id
     * @ORM\Column(name="zona_id", type="integer", nullable=true)
     * */
    private $zona_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Zona")
     * @ORM\JoinColumn(name="zona_id", referencedColumnName="id")
     * */
    private $zona;

    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uni
     * @param string uni
     * @return $this
     **/
    public function setUni($uni)
    {
        $this->uni = $uni;
        return $this;
    }

    /**
     * Set paciente_id
     * @param integer paciente_id
     * @return $this
     **/
    public function setPacienteId($paciente_id)
    {
        $this->paciente_id = $paciente_id;
        return $this;
    }

    /**
     * Get paciente_id
     * @return integer
     **/
    public function getPacienteId()
    {
        return $this->paciente_id;
    }

    /**
     * Set paciente
     * @param \HCUE\AtencionMedicBundle\Entity\Paciente $paciente
     *
     * @return $this
     */
    public function setPaciente(\HCUE\AtencionMedicBundle\Entity\Paciente $paciente)
    {
        $this->paciente = $paciente;
        return $this;
    }

    /**
     * Get paciente
     * @return \HCUE\AtencionMedicBundle\Entity\Paciente
     **/
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Get uni
     * @return string
     **/
    public function getUni()
    {
        return $this->uni;
    }

    /**
     * Set group_id
     * @param integer
     * @return group
     **/
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
        return $this;
    }

    /**
     * Get group_id
     * @return integer
     **/
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Set posiciontalla
     * @param integer
     * @return $this
     **/
    public function setPosiciontalla($posiciontalla)
    {
        $this->posiciontalla = $posiciontalla;
        return $this;
    }

    /**
     * Get posiciontalla
     * @return integer
     **/
    public function getPosiciontalla()
    {
        return $this->posiciontalla;
    }

    /**
     * Set atencionmedica_id
     * @param integer atencionmedica_id
     * @return $this
     **/
    public function setAtencionmedicaId($atencionmedica_id)
    {
        $this->atencionmedica_id = $atencionmedica_id;
        return $this;
    }

    /**
     * Get atencionmedica_id
     * @return integer
     **/
    public function getAtencionmedicaId()
    {
        return $this->atencionmedica_id;
    }

    /**
     * Set atencionmedica
     * @param \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
     *
     * @return $this
     */
    public function setAtencionmedica(\HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica)
    {
        $this->atencionmedica = $atencionmedica;
        return $this;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     **/
    public function getAtencionmedica()
    {
        return $this->atencionmedica;
    }

    /**
     * Set ctantropovital_id
     * @param integer ctantropovital_id
     * @return $this
     **/
    public function setCtantropovitalId($ctantropovital_id)
    {
        $this->ctantropovital_id = $ctantropovital_id;
        return $this;
    }

    /**
     * Get ctantropovital_id
     * @return integer
     **/
    public function getCtantropovitalId()
    {
        return $this->ctantropovital_id;
    }

    /**
     * Set ctantropovital
     * @param \Core\AppBundle\Entity\Catalogo $ctantropovital
     * @return $this
     **/
    public function setCtantropovital(\Core\AppBundle\Entity\Catalogo $ctantropovital)
    {
        $this->ctantropovital = $ctantropovital;
        return $this;
    }

    /**
     * Get ctantropovital
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtantropovital()
    {
        return $this->ctantropovital;
    }

    /**
     * Set valor
     * @param string valor
     * @return $this
     **/
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * Get valor
     * @return string
     **/
    public function getValor()
    {
        return $this->valor;
    }


    /**
     * Set activo
     * @param integer activo
     * @return $this
     **/
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return $this
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return $this
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return $this
     * */
    public function setFechacreacion()
    {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Set zona
     * @param \Core\AppBundle\Entity\Zona $zona
     *
     * @return $this
     */
    public function setZona(\Core\AppBundle\Entity\Zona $zona)
    {
        $this->zona = $zona;
        return $this;
    }

    /**
     * Get zona
     * @return \Core\AppBundle\Entity\Zona
     * */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set zona_id
     * @param integer zona_id
     * @return atencionmedica
     * */
    public function setZonaId($zona_id)
    {
        $this->zona_id = $zona_id;
        return $this;
    }

    /**
     * Get zona_id
     * @return integer
     * */
    public function getZonaId()
    {
        return $this->zona_id;
    }


    /**
     * Get fechacreacion
     * @return datetime
     * */
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();

    }

    public function __toString()
    {

    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

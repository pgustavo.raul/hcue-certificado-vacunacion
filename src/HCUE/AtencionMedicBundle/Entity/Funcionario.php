<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.funcionario")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\FuncionarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Funcionario
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.funcionario_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $persona_id
     * @ORM\Column(name="persona_id", type="integer", nullable=false)
     **/
    private $persona_id;

    /**
     * @ORM\OneToOne(targetEntity="\Core\AppBundle\Entity\Persona",cascade={"persist"})
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     **/
    private $persona;

    /**
     * @var string $registronro
     * @ORM\Column(name="registronro", type="string",length=20, nullable=true)
     **/
    private $registronro;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     **/
    private $usuariocreacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     **/
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Funcionarioentidad", mappedBy="funcionario")
     */
    private $funcionarioentidad;

    /**
     * Get usuario
     * @return \HCUE\AtencionMedicBundle\Entity\Funcionarioentidad
     **/
    public function getFuncionarioEntidades()
    {
        return $this->funcionarioentidad;
    }


    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set persona_id
     * @param integer persona_id
     * @return funcionario
     **/
    public function setPersonaId($persona_id)
    {
        $this->persona_id = $persona_id;
        return $this;
    }

    /**
     * Get persona_id
     * @return integer
     **/
    public function getPersonaId()
    {
        return $this->persona_id;
    }

    /**
     * Set persona
     * @param \Core\AppBundle\Entity\Persona $persona
     **/
    public function setPersona(\Core\AppBundle\Entity\Persona $persona)
    {
        $this->persona = $persona;
        return $this;
    }

    /**
     * Get persona
     * @return \Core\AppBundle\Entity\Persona
     **/
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * Set registronro
     * @param string registronro
     * @return funcionario
     **/
    public function setRegistronro($registronro)
    {
        $this->registronro = $registronro;
        return $this;
    }

    /**
     * Get registronro
     * @return string
     **/
    public function getRegistronro()
    {
        return $this->registronro;
    }

    /**
     * Set estado
     * @param integer estado
     * @return funcionario
     **/
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return funcionario
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return funcionario
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return integer
     **/
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
        $this->activo = 1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

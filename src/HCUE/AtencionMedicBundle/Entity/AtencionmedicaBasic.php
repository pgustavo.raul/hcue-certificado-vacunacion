<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_amed.atencionmedica")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\AtencionmedicaBasicRepository")
 */
class AtencionmedicaBasic
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.atencionmedica_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var datetime $fechainicioatencion
     * @ORM\Column(name="fechainicioatencion", type="datetime", nullable=true)
     **/
    private $fechainicioatencion;

    /**
     * @var datetime $fechafinatencion
     * @ORM\Column(name="fechafinatencion", type="datetime", nullable=true)
     **/
    private $fechafinatencion;

    /**
     * @var integer $ctestado_id
     * @ORM\Column(name="ctestado_id", type="integer", nullable=true)
     **/
    private $ctestado_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestado_id", referencedColumnName="id")
     **/
    private $ctestado;



    /**
     * @var integer $ctespecialidadmedica_id
     * @ORM\Column(name="ctespecialidadmedica_id", type="integer", nullable=false)
     **/
    private $ctespecialidadmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctespecialidadmedica_id", referencedColumnName="id")
     **/
    private $ctespecialidadmedica;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var integer $paciente_id
     * @ORM\Column(name="paciente_id", type="integer", nullable=false)
     **/
    private $paciente_id;

    /**
     * @ORM\ManyToOne(targetEntity="Paciente")
     * @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     **/
    private $paciente;

    /**
     * @var integer $zona_id
     * @ORM\Column(name="zona_id", type="integer", nullable=true)
     **/
    private $zona_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Zona")
     * @ORM\JoinColumn(name="zona_id", referencedColumnName="id")
     **/
    private $zona;


    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @ORM\OneToOne(targetEntity="Core\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuariocreacion_id", referencedColumnName="id")
     * */
    private $usuariocreacion;


    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;


    /**
     * @var integer $ctsegurosalud_id
     * @ORM\Column(name="ctsegurosalud_id", type="integer", nullable=true)
     **/
    private $ctsegurosalud_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctsegurosalud_id", referencedColumnName="id")
     **/
    private $ctsegurosalud;

    /**
     * @ORM\OneToMany(targetEntity="MotivoatencionBasic", mappedBy="atencionmedica")
     */
    private $atencionmedicamotivos;

    /**
     * @ORM\OneToMany(targetEntity="AntecedentepacienteBasic", mappedBy="atencionmedica")
     */
    private $atencionmedicaantecedentes;

    /**
     * @ORM\OneToMany(targetEntity="EnfermedadactualBasic", mappedBy="atencionmedica")
     */
    private $atencionmedicaenfemedades;

    /**
     * @ORM\OneToMany(targetEntity="OrganosistemaBasic", mappedBy="atencionmedica")
     */
    private $atencionmedicaorganos;

    /**
     * @ORM\OneToMany(targetEntity="MedicionpacienteBasic", mappedBy="atencionmedica")
     */
    private $atencionmedicamediciones;

    /**
     * @ORM\OneToMany(targetEntity="ExamenfisicoBasic", mappedBy="atencionmedica")
     */
    private $atencionmedicaexamenes;

    /**
     ** @ORM\OneToMany(targetEntity="DiagnosticoatencionBasic", mappedBy="atencionmedica")
     **/
    private $atencionmedicadiagnosticos;

    /**
     ** @ORM\OneToMany(targetEntity="InterconsultaBasic", mappedBy="atencionmedica")
     **/
    private $atencionmedicainterconsultas;

    /**
     ** @ORM\OneToMany(targetEntity="EvolucionBasic", mappedBy="atencionmedica")
     **/
    private $atencionmedicaevoluciones;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get fechainicioatencion
     * @return datetime
     **/
    public function getFechainicioatencion(){
        return $this->fechainicioatencion;
    }

    /**
     * Set fechafinatencion
     * @param datetime fechafinatencion
     * @return atencionmedica
     **/
    public function setFechafinatencion($fechafinatencion){
        $this->fechafinatencion = $fechafinatencion;
        return $this;
    }

    /**
     * Get fechafinatencion
     * @return datetime
     **/
    public function getFechafinatencion(){
        return $this->fechafinatencion;
    }

    /**
     * Set ctestado_id
     * @param integer ctestado_id
     * @return atencionmedica
     **/
    public function setCtestadoId($ctestado_id){
        $this->ctestado_id = $ctestado_id;
        return $this;
    }

    /**
     * Get ctestado_id
     * @return integer
     **/
    public function getCtestadoId(){
        return $this->ctestado_id;
    }

    /**
     * Set ctestado
     * @param \Core\AppBundle\Entity\Catalogo $ctestado
     **/
    public function setCtestado(\Core\AppBundle\Entity\Catalogo $ctestado){
        $this->ctestado = $ctestado;
        return $this;
    }

    /**
     * Get ctestado
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtestado(){
        return $this->ctestado;
    }

    /**
     * Set ctespecialidadmedica_id
     * @param integer ctespecialidadmedica_id
     * @return atencionmedica
     **/
    public function setCtespecialidadmedicaId($ctespecialidadmedica_id){
        $this->ctespecialidadmedica_id = $ctespecialidadmedica_id;
        return $this;
    }

    /**
     * Get ctespecialidadmedica_id
     * @return integer
     **/
    public function getCtespecialidadmedicaId(){
        return $this->ctespecialidadmedica_id;
    }

    /**
     * Set ctespecialidadmedica
     * @param \Core\AppBundle\Entity\Catalogo $ctespecialidadmedica
     **/
    public function setCtespecialidadmedica(\Core\AppBundle\Entity\Catalogo $ctespecialidadmedica){
        $this->ctespecialidadmedica = $ctespecialidadmedica;
        return $this;
    }

    /**
     * Get ctespecialidadmedica
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtespecialidadmedica(){
        return $this->ctespecialidadmedica;
    }

    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return atencionmedica
     **/
    public function setEntidadId($entidad_id){
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId(){
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     **/
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad){
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidad(){
        return $this->entidad;
    }

    /**
     * Set paciente_id
     * @param integer paciente_id
     * @return atencionmedica
     **/
    public function setPacienteId($paciente_id){
        $this->paciente_id = $paciente_id;
        return $this;
    }

    /**
     * Get paciente_id
     * @return integer
     **/
    public function getPacienteId(){
        return $this->paciente_id;
    }

    /**
     * Set paciente
     * @param \HCUE\AtencionMedicBundle\Entity\Paciente $paciente
     **/
    public function setPaciente(\HCUE\AtencionMedicBundle\Entity\Paciente $paciente){
        $this->paciente = $paciente;
        return $this;
    }

    /**
     * Get paciente
     * @return \HCUE\AtencionMedicBundle\Entity\Paciente
     **/
    public function getPaciente(){
        return $this->paciente;
    }

    /**
     * Set zona_id
     * @param integer zona_id
     * @return atencionmedica
     **/
    public function setZonaId($zona_id){
        $this->zona_id = $zona_id;
        return $this;
    }

    /**
     * Get zona_id
     * @return integer
     **/
    public function getZonaId(){
        return $this->zona_id;
    }

    /**
     * Set zona
     * @param \Core\AppBundle\Entity\Zona $zona
     **/
    public function setZona(\Core\AppBundle\Entity\Zona $zona){
        $this->zona = $zona;
        return $this;
    }

    /**
     * Get zona
     * @return \Core\AppBundle\Entity\Zona
     **/
    public function getZona(){
        return $this->zona;
    }

    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return paciente
     * */
    public function setFechacreacion() {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     * */
    public function getFechacreacion() {
        return $this->fechacreacion;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return atencionmedica
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return atencionmedica
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Get usuariocreacion
     * @return \Core\SeguridadBundle\Entity\Usuario
     * */
    public function getUsuariocreacion() {
        return $this->usuariocreacion;
    }

    /**
     * Get usuariocreacion
     * @param \Core\SeguridadBundle\Entity\Usuario $usuario
     * @return \Core\SeguridadBundle\Entity\Usuario
     * */
    public function setUsuariocreacion(\Core\SeguridadBundle\Entity\Usuario $usuario) {
        $this->usuariocreacion=$usuario;
        return $this;
    }

    /**
     * Set activo
     * @param integer activo
     * @return atencionmedica
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }


    /**
     * Get ctsegurosalud_id
     * @return integer
     **/
    public function getCtsegurosaludId()
    {
        return $this->ctsegurosalud_id;
    }

    /**
     * Set ctsegurosalud_id
     * @return integer
     **/
    public function setCtsegurosaludId($ctsegurosalud_id)
    {
        $this->ctsegurosalud_id = $ctsegurosalud_id;
    }

    /**
     * Get ctsegurosalud
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtsegurosalud()
    {
        return $this->ctsegurosalud;
    }


    /**
     * Set ctestado
     * @param \Core\AppBundle\Entity\Catalogo $ctsegurosalud
     **/
    public function setCtsegurosalud(\Core\AppBundle\Entity\Catalogo $ctsegurosalud)
    {
        $this->ctsegurosalud = $ctsegurosalud;
    }

    /**
     * Get atencionmedicamotivos
     * @return \HCUE\AtencionMedicBundle\Entity\MotivoatencionBasic
     **/
    public function getAtencionmedicamotivos()
    {
        return $this->atencionmedicamotivos;
    }

    /**
     * Get atencionmedicaantecedentes
     * @return \HCUE\AtencionMedicBundle\Entity\AntecedentepacienteBasic
     **/
    public function getAtencionmedicaantecedentes()
    {
        return $this->atencionmedicaantecedentes;
    }

    /**
     * Get atencionmedicaenfemedades
     * @return \HCUE\AtencionMedicBundle\Entity\EnfermedadactualBasic
     **/
    public function getAtencionmedicaenfemedades()
    {
        return $this->atencionmedicaenfemedades;
    }

    /**
     * Get atencionmedicaorganos
     * @return \HCUE\AtencionMedicBundle\Entity\OrganosistemaBasic
     **/
    public function getAtencionmedicaorganos()
    {
        return $this->atencionmedicaorganos;
    }

    /**
     * Get atencionmedicamediciones
     * @return \HCUE\AtencionMedicBundle\Entity\MedicionpacienteBasic
     **/
    public function getAtencionmedicamediciones()
    {
        return $this->atencionmedicamediciones;
    }

    /**
     * Get atencionmedicaexamenes
     * @return \HCUE\AtencionMedicBundle\Entity\ExamenfisicoBasic
     **/
    public function getAtencionmedicaexamenes()
    {
        return $this->atencionmedicaexamenes;
    }

    /**
     * Get atencionmedicadiagnosticos
     * @return \HCUE\AtencionMedicBundle\Entity\DiagnosticoatencionBasic
     **/
    public function getAtencionmedicadiagnosticos()
    {
        return $this->atencionmedicadiagnosticos;
    }

    /**
     * Get atencionmedicainterconsultas
     * @return \HCUE\AtencionMedicBundle\Entity\InterconsultaBasic
     **/
    public function getAtencionmedicainterconsultas()
    {
        return $this->atencionmedicainterconsultas;
    }

    /**
     * Get atencionmedicaevoluciones
     * @return \HCUE\AtencionMedicBundle\Entity\EvolucionBasic
     **/
    public function getAtencionmedicaevoluciones()
    {
        return $this->atencionmedicaevoluciones;
    }

}
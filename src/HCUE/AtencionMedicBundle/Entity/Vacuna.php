<?php
/*
* @autor Lenin Vallejos
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.vacuna")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\VacunaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Vacuna
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.vacuna_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $medicamento_id
     * @ORM\Column(name="medicamento_id", type="integer", nullable=true)
     **/
    private $medicamento_id;

    /**
     * @ORM\ManyToOne(targetEntity="Medicamento")
     * @ORM\JoinColumn(name="medicamento_id", referencedColumnName="id")
     **/
    private $medicamento;

    /**
     * @var integer $hombre
     * @ORM\Column(name="hombre", type="integer", nullable=false)
     **/
    private $hombre;

    /**
     * @var integer $mujer
     * @ORM\Column(name="mujer", type="integer", nullable=false)
     **/
    private $mujer;

    /**
     * @var integer $intersexual
     * @ORM\Column(name="intersexual", type="integer", nullable=false)
     **/
    private $intersexual;

    /**
     * @var string $nombrevacuna
     * @ORM\Column(name="nombrevacuna", type="string",length=200, nullable=true)
     **/
    private $nombrevacuna;

    /**
     * @var string $descripcionvacuna
     * @ORM\Column(name="descripcionvacuna", type="string",length=500, nullable=true)
     **/
    private $descripcionvacuna;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $color
     * @ORM\Column(name="colorvacunacarnet", type="string", nullable=true)
     **/
    private $color;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     **/
    private $usuariocreacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;


    /**
     * @var integer $vacunador
     * @ORM\Column(name="vacunador", type="integer")
     **/
    private $vacunador;


    /**
     * @var integer $certificado
     * @ORM\Column(name="certificado", type="integer")
     **/
    private $certificado;

    /**
     * @var integer $aceptavacuna
     * @ORM\Column(name="aceptavacuna", type="integer")
     **/
    private $aceptavacuna;

    /**
     * @var datetime $fechainicia
     * @ORM\Column(name="fechainicia", type="datetime", nullable=true)
     **/
    private $fechainicia;

    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set medicamento_id
     * @param integer medicamento_id
     * @return vacuna
     **/
    public function setMedicamentoId($medicamento_id)
    {
        $this->medicamento_id = $medicamento_id;
        return $this;
    }

    /**
     * Get medicamento_id
     * @return integer
     **/
    public function getMedicamentoId()
    {
        return $this->medicamento_id;
    }

    /**
     * Set medicamento
     * @param Medicamento $medicamento
     **/
    public function setMedicamento(Medicamento $medicamento)
    {
        $this->medicamento = $medicamento;
        return $this;
    }

    /**
     * Get medicamento
     * @return Medicamento
     **/
    public function getMedicamento()
    {
        return $this->medicamento;
    }

    /**
     * Set hombre
     * @param integer $hombre
     * @return esquemavacunacion
     **/
    public function setHombre($hombre)
    {
        $this->hombre = $hombre;
        return $this;
    }

    /**
     * Get hombre
     * @return integer
     **/
    public function getHombre()
    {
        return $this->hombre;
    }

    /**
     * Set mujer
     * @param integer $mujer
     * @return esquemavacunacion
     **/
    public function setMujer($mujer)
    {
        $this->mujer = $mujer;
        return $this;
    }

    /**
     * Get mujer
     * @return integer
     **/
    public function getMujer()
    {
        return $this->mujer;
    }

    /**
     * Set intersexual
     * @param integer $intersexual
     * @return esquemavacunacion
     **/
    public function setIntersexual($intersexual)
    {
        $this->intersexual = $intersexual;
        return $this;
    }

    /**
     * Get intersexual
     * @return integer
     **/
    public function getIntersexual()
    {
        return $this->intersexual;
    }

    /**
     * Set nombrevacuna
     * @param string $nombrevacuna
     * @return vacuna
     **/
    public function setNombrevacuna($nombrevacuna)
    {
        $this->nombrevacuna = $nombrevacuna;
        return $this;
    }

    /**
     * Get nombrevacuna
     * @return string
     **/
    public function getNombrevacuna()
    {
        return $this->nombrevacuna;
    }



    /**
     * Set descripcionvacuna
     * @param string descripcionvacuna
     * @return vacuna
     **/
    public function setDescripcionvacuna($descripcionvacuna)
    {
        $this->descripcionvacuna = $descripcionvacuna;
        return $this;
    }

    /**
     * Get descripcionvacuna
     * @return string
     **/
    public function getDescripcionvacuna()
    {
        return $this->descripcionvacuna;
    }

    /**
     * Set estado
     * @param integer estado
     * @return vacuna
     **/
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set color
     * @param string color
     * @return vacuna
     **/
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Get color
     * @return string
     **/
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return vacuna
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return vacuna
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return vacuna
     **/
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }




    /**
     * Set certificado
     * @param integer $certificado
     * @return esquemavacunacion
     **/
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
        return $this;
    }

    /**
     * Get certificado
     * @return integer
     **/
    public function getCertificado()
    {
        return $this->certificado;
    }



    /**
     * Set vacunador
     * @param integer $vacunador
     * @return esquemavacunacion
     **/
    public function setVacunador($vacunador)
    {
        $this->vacunador = $vacunador;
        return $this;
    }

    /**
     * Get vacunador
     * @return integer
     **/
    public function getVacunador()
    {
        return $this->vacunador;
    }


    /**
     * Set aceptavacuna
     * @param integer $aceptavacuna
     * @return esquemavacunacion
     **/
    public function setAceptavacuna($aceptavacuna)
    {
        $this->aceptavacuna = $aceptavacuna;
        return $this;
    }

    /**
     * Get aceptavacuna
     * @return integer
     **/
    public function getAceptavacuna()
    {
        return $this->aceptavacuna;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
        $this->activo = 1;

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();

    }

    public function __toString()
    {

    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }

    /**
     * Set fechainicia
     * @param datetime fechainicia
     * @return vacuna
    **/
    public function setFechainicia($fechainicia){
        $this->fechainicia = $fechainicia;
        return $this;
    }

    /** 
     * Get fechainicia
     * @return datetime
    **/
    public function getFechainicia(){
        return $this->fechainicia;
    }

}

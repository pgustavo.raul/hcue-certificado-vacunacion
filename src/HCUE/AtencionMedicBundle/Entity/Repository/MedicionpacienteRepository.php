<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;


class MedicionpacienteRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Medicionpaciente
     * @return QueryBuilder
     */
    public function getMedicionpacienteQueryBuilder()
    {
        return $this->createQueryBuilder('mp')
            ->select('mp')
            ->Where('mp.activo = :activo')
            ->setParameter("activo", Constantes::ESTADOGENERALHABILITADO);
    }

    /**
     * Obtener mediciones por atencion medica
     * @param $atencionmedica_id
     * @return array
     */
    public function getMedicionesByAtencion($atencionmedica_id)
    {
        $mediciones = $this->getMedicionpacienteQueryBuilder()
            ->select("em.descripcion as especialidad, mp.id ,me.id as id_signo ,mp.valor, me.valor as medicion, mp.fechacreacion as fecha, mp.atencionmedica_id, mp.group_id, p.nombrecompleto, u.id as user_id, mp.posiciontalla")
            ->leftJoin("mp.ctantropovital", "me")
            ->leftJoin("mp.atencionmedica", "am")
            ->leftJoin("am.ctespecialidadmedica", "em")
            ->leftJoin("CoreSeguridadBundle:Usuario", "u", "WITH", "u.id = am.usuariocreacion_id")
            ->leftJoin("u.persona", "p")
            ->andWhere("mp.atencionmedica_id = :atencionmedica_id")
            ->setParameter('atencionmedica_id', $atencionmedica_id)
            ->orderBy('mp.group_id, me.orden ', 'DESC')
            ->getQuery()
            ->getArrayResult();
        return $mediciones;
    }

}

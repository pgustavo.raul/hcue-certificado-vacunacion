<?php
/*
* @autora Estefanía Gonzaga
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class MedicamentoRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Medicamento
     * @return QueryBuilder
     */
    public function getMedicamentoQueryBuilder()
    {
        return $this->createQueryBuilder('me')
            ->select('me')
            ->Where("me.estado=:estadogeneral")
            ->andWhere("me.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene un listado de medicamento filtrado por un criterio de busqueda
     * @param string $texto texto a buscar
     * @param integer $num Numero de registro a filtrar
     * @return arrayresult
     */
    public function getFilterMedicamento($texto, $num = 15)
    {
        $qb = $this->getMedicamentoQueryBuilder();
        $qbr = $qb->andWhere($qb->expr()->like("me.nombre", ":texto"))
            ->setParameter('texto', "%" . $texto . "%")
            ->setMaxResults($num)
            ->getQuery();

        return $qbr->getArrayResult();
    }

    /**
     * Retorna true o false si un medicamento es de tipo solido
     * @param integer $id Id del medicamento
     * @return boolean true o false
     */
    public function esSolido($id)
    {
        $response = false;
        $ids_solidos = Constantes::IFM_FFCNMB_SOLIDO_CUTANEO . ',' . Constantes::IFM_FFCNMB_SOLIDO_ORAL . ',' . Constantes::IFM_FFCNMB_SOLIDO_INHALACION . ',' . Constantes::IFM_FFCNMB_SOLIDO_PARENTAL . ',' . Constantes::IFM_FFCNMB_SOLIDO_RECTAL . ',' . Constantes::IFM_FFCNMB_SOLIDO_VAGINAL;
        $qb = $this->getMedicamentoQueryBuilder();
        $qbr = $qb->andWhere("me.id=:id")
            ->andWhere($qb->expr()->in("me.formfarmacnmb_id", $ids_solidos))
            ->setParameter('id', $id)
            ->getQuery();
        if (count($qbr->getArrayResult()) > 0) {
            $response = true;
        }

        return $response;
    }

    /**
     * Retorna la lista de todos los medicamentos
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\AtencionMedicBundle\Entity\Medicamento"
     */
    public function getMedicamento($arrayResult=false)
    {
        $qb = $this->getMedicamentoQueryBuilder();
        $qbr = $qb->getQuery();
        return ($arrayResult) ? $qbr->getArrayResult() : $qbr->getResult();
    }
}

<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class OrdenmedicaRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Ordenmedica
     * @return QueryBuilder
     */
    public function getOrdenmedicaQueryBuilder() {
        return $this->createQueryBuilder('o')
            ->select('o')
            ->where("o.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene la orden médica por id
     * @param integer $id Id de la orden médica
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\OrdenMedicaBundle\Entity\Ordenmedica"
     */
    public function getOrdenmedicaById($id, $arrayResult = false)
    {
        $queryBuilder = $this->getOrdenesmedicasQueryBuilder();
        $q = $queryBuilder
            ->andWhere("o.id=:id")
            ->setParameter("id", $id)
            ->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene la ordenes médicas por ID de atención
     * @param integer $atencionmedica_id Id de la atencion medica
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\OrdenMedicaBundle\Entity\Ordenmedica"
     */
    public function getOrdenesmedicasByAtencionId($atencionmedica_id, $tipoordenmedica_id)
    {
        $queryBuilder = $this->getOrdenmedicaQueryBuilder();
        $queryBuilder
            ->andWhere('o.atencionmedica_id=:atencionmedica_id')
            ->andWhere('o.cttipoordenmedica_id=:tipoordenmedica_id')
            ->setParameter("atencionmedica_id", $atencionmedica_id)
            ->setParameter("tipoordenmedica_id", $tipoordenmedica_id);
        if($tipoordenmedica_id== Constantes::CT_HISTOPATOLOGIA){
            $q=$queryBuilder->getQuery()
                ->getResult();
        }
        else{
            $q=$queryBuilder->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }
        return $q;
    }

    /**
     * Obtiene la ordenes médicas por ID de atención
     * @param integer $atencionmedica_id Id de la atencion medica
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\OrdenMedicaBundle\Entity\Ordenmedica"
     */
    public function getOrdenesmedicasAndGrupoByAtencionId($atencionmedica_id, $tipoordenmedica_id)
    {
        $qb= $this->createQueryBuilder('o')
            ->where("o.activo=:estadogeneral")
            ->andWhere('o.atencionmedica_id=:atencionmedica_id')
            ->andWhere('o.cttipoordenmedica_id=:tipoordenmedica_id')
            ->setParameter("atencionmedica_id", $atencionmedica_id)
            ->setParameter("tipoordenmedica_id", $tipoordenmedica_id)
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
        if($tipoordenmedica_id== Constantes::CT_HISTOPATOLOGIA){
            $q= $qb->select('o.id, o.resumenclinico, te.descripcion as grupo, o.fechacreacion, o.muestraopieza')
                ->distinct(true)
                ->leftJoin('HCUEOrdenMedicaBundle:Detalleordenmedica','do','WITH' ,'do.ordenmedica_id=o.id')
                ->leftJoin('do.tipoexamenvariable','te')
                ->getQuery()
                ->getResult();
        }
        else{
            $q= $qb->select('o')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }
        return $q;
    }

    /**
     * Obtiene los grupos de ordenes médicas registradas por atención medica y paciente
     * @param integer $atencionmedica_id Id de la atencion medica
     * @param integer $paciente_id Id del paciente
     * @param integer $cttipoordenmed_id Id del tipo de orden médica
     * @return Array Lista de Objetos de tipo "HCUE\OrdenMedicaBundle\Entity\Ordenmedica"
     */
    public function verifyOrdenmedicaByAtencionAndPaciente($atencionmedica_id, $paciente_id, $cttipoordenmed_id)
    {
        $qb=$this->createQueryBuilder('o')
            ->where("o.activo=:estadogeneral")
            ->innerJoin('o.atencionmedica','at')
            ->andWhere('o.cttipoordenmedica_id=:tipoordenmedica')
            ->andWhere('o.atencionmedica_id=:atencionmedica_id')
            ->andWhere('at.paciente_id=:paciente_id')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO)
            ->setParameter("atencionmedica_id", $atencionmedica_id)
            ->setParameter("paciente_id", $paciente_id)
            ->setParameter("tipoordenmedica", $cttipoordenmed_id);

        if($cttipoordenmed_id== Constantes::CT_HISTOPATOLOGIA){
            $q= $qb->select('te.tipoexamenvariable_id, te.descripcion')
                ->distinct(true)
                ->innerJoin('HCUEOrdenMedicaBundle:Detalleordenmedica','do','WITH' ,'do.ordenmedica_id=o.id')
                ->innerJoin('do.tipoexamenvariable','te')
                ->getQuery()
                ->getResult();
        }
        else{
            $q= $qb->select('o')
                ->getQuery()
                ->getResult();
        }
        return $q;
    }
}
?>
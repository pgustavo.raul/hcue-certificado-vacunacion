<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class CieRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Cie
     * @return QueryBuilder
     */
    public function getCieQueryBuilder()
    {
        return $this->createQueryBuilder('t')
            ->select('t');
    }

    /**
     * Obtiene el QueryBuilder de la lista de todos los padres activos del CIE
     * @return QueryBuilder
     */
    public function getCieAllparentsQueryBuilder()
    {
        $qb = $this->createQueryBuilder('cie')
            ->select("cie")
            ->Where('cie.cie_id is null')
            ->andWhere('cie.estado=:estado')
            ->setParameter("estado", Constantes::ESTADOGENERALHABILITADO);

        return $qb;
    }

    /**
     * Obtiene la lista de lista de todos de todos los grupos activos del CIE establecidos en la tabla Cieclasificacion
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\AtencionMedicBundle\Entity\Cieclasificacion"
     */
    public function getCieAllparents($arrayResult = false)
    {
        $queryBuilder = $this->getCieAllparentsQueryBuilder();
        $q = $queryBuilder->getQuery();

        $resultId = ($arrayResult) ? "result_array_cie_allparents" : "result_cie_allparents";
        $q->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de todos los capitulos activos del CIE establecidos en la tabla Cieclasificacion
     * @param string $sexo 16.- Hombre  17.- Mujer
     * @param string $textobusqueda texto a buscar
     * * @return QueryBuilder
     */
    public function getListBySexo($sexo, $textobusqueda)
    {
        $textobusqueda = trim($textobusqueda);
        $textobusqueda = strtoupper($textobusqueda);
//        echo  "<br>sexo: ".$sexo;
        $sexo = ($sexo == Constantes::CT_SEXO_HOMBRE) ? Constantes::CT_SEXO_MUJER : Constantes::CT_SEXO_HOMBRE; //esta condicion es porque ay algunos diagnosticos que aplican a ambos sexos
//        echo "<br>busqueda: ".$textobusqueda;


        $qb = $this->createQueryBuilder('cie')
            ->select("cie.id"
                . ",cie.codigo"
                . ",cie.nombre"
                . ",cie.edadmin"
                . ",cie.unidadedadmin"
                . ",cie.edadmax"
                . ",cie.unidadedadmax")
            ->Where('cie.ctsexo_id !=:sexo');
        $qb->andWhere("cie.estado=1");
        if ($textobusqueda) {
            $qb->andWhere("upper(cie.nombre) LIKE :textobusqueda");
            $qb->orWhere("upper(cie.codigo) LIKE :textobusqueda");
            $qb->orWhere("upper(cie.etiqueta) LIKE :textobusqueda");
            $qb->setParameter("textobusqueda", '%' . $textobusqueda . '%');
        }
        $qb->setParameter("sexo", $sexo)
            ->setFirstResult(0)
            ->setMaxResults(500);
        $qb->orderBy("cie.codigo");

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * Verifica que el codigo cie sea unico
     * @param string $codigo
     * @return integer
     */
    public function verifyCie($codigo)
    {
        $qb = $this->createQueryBuilder('cie')
            ->select('count(cie.id)')
            ->where('cie.codigo = :codigo')
            ->setParameter('codigo', $codigo);

        $query=$qb->getQuery();
        $query->useQueryCache(true);

        return $query->getSingleScalarResult();
    }

}

<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;

class AtencionmedicaBasicRepository extends EntityRepository
{

    /**
     * Obtiene datos de atencion medica para el pdf
     * @param $id
     * @return array
     */
    public function getAtencionmedica($id)
    {
        $qb = $this->createQueryBuilder('am')
            ->select('am')
            ->addSelect('an, av, gr, ca')
            ->addSelect('diag, ctcond, ctcron, cttip, cie')
            ->leftJoin('am.atencionmedicaantecedentes','an', 'WITH', 'an.activo = :estado')
            ->leftJoin('an.antecedente','av')
            ->leftJoin('av.ctgrupoantecedente','gr')
            ->leftJoin('av.ctantecedente','ca')
            ->leftJoin('am.atencionmedicadiagnosticos', 'diag', 'WITH', 'diag.activo = :estado')
            ->leftJoin('diag.cie', 'cie')
            ->leftJoin('diag.ctcondiciondiagnostico', 'ctcond')
            ->leftJoin('diag.ctcronologiadiagnostico', 'ctcron')
            ->leftJoin('diag.cttipodiagnostico', 'cttip')
            ->where('am.id = :id')
            ->setParameter('id', $id)
            ->setParameter('estado', Constantes::CT_ESTADOGENERALACTIVO);
        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Obener datos de atencion medica para el pdf
     * @param $atencionmedica_id
     * @return array
     */
    public function getAtencionmedicaPreviewData($atencionmedica_id){

        $qb = $this->createQueryBuilder('am')
            ->select('am.paciente_id, am.fechacreacion, am.fechafinatencion, am.entidad_id')
            ->addSelect('fo.archivo as foto, f.registronro, se.descripcion as sexo','pa.numerohistoriaclinica','pro.apellidopaterno as apellidopaterno_profesional','pro.primernombre as primernombre_profesional','pro.apellidomaterno as apellidomaterno_profesional','pro.segundonombre as segundonombre_profesional')
            ->addSelect('pe.id as persona_id, pe.apellidopaterno, pe.apellidomaterno, pe.primernombre, pe.segundonombre, pe.fechanacimiento, pe.numeroidentificacion')
            ->addSelect('ctesp.descripcion as especialidad, ctseg.descripcion as seguro, ctest.descripcion as estado_civil, ctniv.descripcion as instruccion, ctemp.descripcion as empresa')
            ->addSelect('e.nombreoficial as entidad')
            ->addSelect('parr.descripcion as parroquia, can.descripcion as canton, prov.descripcion as provincia')
            ->innerJoin('am.ctespecialidadmedica', 'ctesp')
            ->innerJoin('am.paciente','pa')
            ->innerJoin('pa.persona','pe')
            ->leftJoin('pe.foto','fo')
            ->innerJoin('pe.ctsexo','se')
            ->leftJoin('am.entidad','e')
            ->leftJoin('e.parroquia', 'parr')
            ->leftJoin('parr.canton', 'can')
            ->leftJoin('can.provincia', 'prov')
            ->innerJoin('CoreSeguridadBundle:Usuario','u','WITH','am.usuariocreacion_id = u.id')
            ->innerJoin('u.persona','pro')
            ->innerJoin('HCUEAtencionMedicBundle:Funcionario','f','WITH','pro.id = f.persona_id')
            ->leftJoin('am.ctsegurosalud', 'ctseg')
            ->leftJoin('pe.ctestadocivil','ctest')
            ->leftJoin('pe.ctniveleducacion','ctniv')
            ->leftJoin('pa.cttipoempresatrabajo', 'ctemp')
            ->where('am.id = :id')
            ->setParameter('id',$atencionmedica_id);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Obtiene las mediciones correspondientes a la atencion solicitada y a la ultima atencion de enfermeria
     * @param integer $paciente_id
     * @param \DateTime $fecha
     * @return array
     */
    public function getMedicionesByPaciente($paciente_id, $fecha)
    {
        $date = date('Y-m-d H:i:s', $fecha->getTimestamp());

        $qb = $this->createQueryBuilder('am');

        $qb->select('am')
            ->addSelect('ctesp, us, per')
            ->addSelect('med, ctant')
            ->leftJoin('am.ctespecialidadmedica', 'ctesp')
            ->innerJoin('am.atencionmedicamediciones','med')
            ->leftJoin('med.ctantropovital','ctant')
            ->innerJoin('am.usuariocreacion', 'us')
            ->innerJoin('us.persona', 'per')
            ->where(
                $qb->expr()->andX(
                    'am.paciente_id = :paciente',
                    'am.fechacreacion > :fecha',
                    'am.fechacreacion <= :fechacreacion',
                    'am.ctestado_id = :estado',
                    $qb->expr()->in(
                        'am.ctespecialidadmedica_id',
                        implode(',', Constantes::CT_ESPECIALIDADES_ENFERMERIA)
                    )
                )
            )
            ->orderBy('med.group_id', 'DESC')
            ->addOrderBy('ctant.orden', 'DESC')
            ->setParameter('paciente', $paciente_id)
            ->setParameter('fecha', $fecha->setTime(0, 0, 01))
            ->setParameter('fechacreacion', $date)
            ->setParameter('estado', Constantes::CT_ATENCIONFINALIZADA);

        $query = $qb->getQuery();

        return $query->getArrayResult();
    }

    /**
     * Obtiene datos de atencion médica (Historial de Atenciones) por IdPaciente
     * @param $idPaciente
     * @return array
     */
    public function getAtencionmedicaByPaciente($idPaciente, $maxResults)
    {
        $especilidades= array(Constantes::CT_ESPECIALIDAD_ENFERMERIA,Constantes::CT_ESPECIALIDAD_ENFERMERIA_AUXILIAR, Constantes::CT_ESPECIALIDAD_ENFERMERIA_RURAL);
        $qb = $this->createQueryBuilder('am');
        $qb->select('am.fechacreacion, am.fechafinatencion')
            ->addSelect('pro.nombrecompleto')
            ->addSelect('ctesp.descripcion as especialidad')
            ->addSelect('e.nombreoficial as entidad')
            ->innerJoin('am.ctespecialidadmedica', 'ctesp')
            ->leftJoin('am.entidad','e')
            ->innerJoin('CoreSeguridadBundle:Usuario','u','WITH','am.usuariocreacion_id = u.id')
            ->innerJoin('u.persona','pro')
            ->where('am.paciente_id = :paciente_id')
            ->andWhere('am.ctestado_id = :estado')
            ->andWhere(
                $qb->expr()->notIn(
                    'am.ctespecialidadmedica_id',
                    ':especilidades'
                )
            )
            ->setParameter('paciente_id', $idPaciente)
            ->setParameter('estado', Constantes::CT_ATENCIONFINALIZADA)
            ->setParameter('especilidades', $especilidades)
            ->orderBy('am.fechacreacion', 'DESC')
            ->setMaxResults($maxResults);

        $query = $qb->getQuery();

        return $query->getArrayResult();
    }
}
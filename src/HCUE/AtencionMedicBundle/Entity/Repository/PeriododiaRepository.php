<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class PeriododiaRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Periododia
     * @return QueryBuilder
     */
    public function getPeriododiaQueryBuilder()
    {
        return $this->createQueryBuilder('pd')
            ->select('pd')
            ->where("pd.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene la lista de periodos del dia
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Periododia"
     */
    public function getPeriododia($arrayResult = false)
    {
        $queryBuilder = $this->getPeriododiaQueryBuilder();
        $q = $queryBuilder->getQuery();

        $resultId = ($arrayResult) ? "result_array_periododia" : "result_periododia";
        $q->useQueryCache(true)->useResultCache(true,null,$resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }
}

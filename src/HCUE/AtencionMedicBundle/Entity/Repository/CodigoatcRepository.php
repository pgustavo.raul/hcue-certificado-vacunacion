<?php

/*
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class CodigoatcRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Codigoatc
     * @return QueryBuilder
     */
    public function getCodigoatcQueryBuilder()
    {
        return $this->createQueryBuilder('t')
            ->select('t');
    }

    /**
     * Obtiene el QueryBuilder de la lista de coincidencia del Codigoatc por descripción.
     * @param $texto Palabra a ser buscada en la descripción
     * @return QueryBuilder
     */
    public function getCodigoatc2($texto)
    {
        $texto = trim($texto);
        $texto = strtolower($texto);
        $wordsgroup = explode(" ", $texto);
        $arrayExceptionWord = array('de', 'del', 'la', 'los', 'las', 'el');
        $qb = $this->createQueryBuilder('c');

        $hits_words = '';
        $i = 0;

        $criteria = [];
        foreach ($wordsgroup as $word) {
            if (!in_array($word, $arrayExceptionWord)) {
                $add = $qb->expr()->like("replace(replace(replace(replace(replace(lower(c.nombre),'á','a'),'é','e'),'í','i'),'ó','o'),'ú','u')", $qb->expr()->literal('%' . $word . '%'));
                array_push($criteria, $add);
                if ($i != 0) {
                    $hits_words .= ' + ';
                }
                $hits_words .= "(length(c.nombre) - length(replace(c.nombre,'" . $word . "','')))";

                $i++;
            }
        }

        if ($i > 0) {
            $match_conditcion = ",( $hits_words ) as hits";
        } else {
            $match_conditcion = '';
        }
        $qb->select("c.id"
            . ",c.codigo"
            . ",CONCAT(c.codigo,' - ',c.nombre) as nombre"
            . $match_conditcion);

        if ($texto) {
            $qb->andWhere(
                call_user_func_array(array($qb->expr(), 'orx'), $criteria)
            );
        }
        $qb->setFirstResult(0)
            ->setMaxResults(20);
        if ($i > 0) {
            $qb->orderBy("hits", "DESC");
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de coincidencia del Codigoatc por código
     * @param $codigo a ser buscada en el código
     * @return QueryBuilder
     */
    public function getCodigoatc($codigo)
    {
        $qb = $this->createQueryBuilder('c')
            ->select("c.id, c.codigo,CONCAT(c.codigo,' - ',c.nombre) as nombre")
            ->where('c.codigo like :codigo')
            //->andWhere("length(c.nombre) = 4")
            //->setParameter('word', '%'.strtoupper($word).'%')
            ->setParameter('codigo', '%' . strtoupper($codigo) . '%')
            ->setMaxResults(20)
            ->getQuery()
            ->getArrayResult();

        return $qb;
    }
}

<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class CargavacunacionRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Cargavacunacion
        * @return QueryBuilder
        */
	public function getCargavacunacionQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');
        }

}
?>
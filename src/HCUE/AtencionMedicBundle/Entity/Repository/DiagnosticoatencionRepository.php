<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class DiagnosticoatencionRepository extends EntityRepository {

    /**
     * Obtiene el QueryBuilder de la lista de Diagnosticoatencion
     * @return QueryBuilder
     */
    public function getDiagnosticoQueryBuilder() {
        return $this->createQueryBuilder('t')
            ->select('t');
    }

    /**
     * Obtiene el QueryBuilder de los diagnositcos por una atencion
     * @param integer $atencion_id Id de la Atencion Medica
     * @return QueryBuilder
     */
    public function getDiagnosticosQueryBuilderByAtencion($atencion_id) {
        $qb = $this->createQueryBuilder('da')
            ->Where("da.atencionmedica_id=:atencion_id and da.activo=1")
            ->setParameter('atencion_id', $atencion_id);
        return $qb;
    }

    /**
     * Obtener todos los diagnositcos de una atencion
     * @param integer $atencion_id Id de la Atencion Medica
     * @param boolean $all
     * @return array
     */
    public function getDiagnosticosByAtencion($atencion_id, $all=false) {

        $qb = $this->createQueryBuilder('da')
            ->InnerJoin('da.cie', 'cie')
            ->leftJoin("da.ctcondiciondiagnostico", "cc")
            ->leftJoin("da.cttipodiagnostico", "ct")
            ->leftJoin("da.ctcronologiadiagnostico", "cr")
            ->Where('da.atencionmedica_id = :atencion_id')
            ->andWhere('da.activo = :estado_general')
            ->setParameters([
                'atencion_id' => $atencion_id,
                'estado_general' => Constantes::CT_ESTADOGENERALACTIVO
            ]);

        if($all) {
            $qb->select('da, cie');
        }
        else {
            $qb->select("da.id ,da.observacion, cie.nombre as nombre_cie, cie.id as id_cie, cie.codigo as codigo_cie ,cc.descripcion as condicion, ct.descripcion as tipo, cr.descripcion as cronologico");
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Obtener el historial de diagnosticos par atencion o atencioens
     * @param integer $paciente_id
     * @return array
     */
    public function getDiagnosticosRecordByAtenciones($atenciones){

        $diagnosticos = $this->createQueryBuilder('da')
            ->select("da.fechacreacion as fecha,da.id ,da.observacion, cie.nombre as nombre_cie, cie.id as id_cie, cie.codigo as codigo_cie ,cc.descripcion as condicion, ct.descripcion as tipo, cr.descripcion as cronologico")
            ->InnerJoin("da.cie", "cie")
            ->leftJoin("da.ctcondiciondiagnostico", "cc")
            ->leftJoin("da.cttipodiagnostico", "ct")
            ->leftJoin("da.ctcronologiadiagnostico", "cr")
            ->Where("da.atencionmedica_id in (:atenciones) and da.activo=1")
            ->setParameter('atenciones', $atenciones)
            ->getQuery()
            ->getArrayResult();
        return $diagnosticos;
    }

    /**
     * Obtener los diagnosticos mas usados
     * @param $especialidadid
     * @return Array
     */
    public function getMostUsedDiagnosticos($especialidadid){

        $diagnosticos = $this->createQueryBuilder('da')
            ->select('count(da.id) as ammount, da.cie_id')
            ->addSelect('(SELECT at.codigo 
            FROM HCUEAtencionMedicBundle:Cie at
            WHERE at.id = da.cie_id)')
            ->addSelect('(SELECT at1.nombre 
            FROM HCUEAtencionMedicBundle:Cie at1
            WHERE at1.id = da.cie_id)')
            ->addSelect('(SELECT at2.prevencion 
            FROM HCUEAtencionMedicBundle:Cie at2
            WHERE at2.id = da.cie_id)')            
            ->leftJoin("HCUEAtencionMedicBundle:Atencionmedica", "am", "WITH", "am.id=da.atencionmedica_id")
            ->leftJoin("HCUEAtencionMedicBundle:Cie", "cie", "WITH", "cie.id=da.cie_id")
            ->Where('am.ctespecialidadmedica_id = :ctespecialidadmedica_id and length(cie.codigo) = 4 ')
            ->setParameter('ctespecialidadmedica_id',$especialidadid)
            ->groupBy('da.cie_id')
            ->orderBy('ammount','DESC')
            ->setMaxResults(12)
            ->getQuery()
            ->getArrayResult();

        return $diagnosticos;
    }

    /**
     * Obtiene el QueryBuilder de la lista de diagnosticos que ha tenido un paciente
     * @param integer $paciente_id Id del paciente
     * @param integer $num Número de diagnosticos a mostrar
     * @return QueryBuilder
     */
    public function getDiagnosticos($paciente_id, $num = 0) {
        $qb = $this->createQueryBuilder('da')
            ->select('da')
            ->InnerJoin('da.atencionmedica', 'am', 'WITH', 'am.id = da.atencionmedica_id and am.activo = :estado_general and am.ctestado_id = :estado')
            ->Where("am.paciente_id = :paciente_id")
            ->andWhere('da.activo = :estado_general')
            ->orderBy('da.fechacreacion', 'DESC')
            ->setParameters([
                'estado' => Constantes::CT_ATENCIONFINALIZADA,
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'paciente_id' => $paciente_id
            ]);

        $query = $qb->getQuery();
        $result = $query->getResult();
        if($num > 0) {
            $result = $query->setMaxResults($num)->getResult();
        }
        return $result;
    }

    /**
     * Obtiene el QueryBuilder si el diagnostico esta asociado a una receta
     * @param integer $atencionmedica_id Id de atencion médica
     * @param integer $diagnostico_id Id Id del diagnóstico
     * @return QueryBuilder
     */
    public function VerifyDiagnosticoInRecetaQueryBuilder($atencionmedica_id, $diagnostico_id){
        $qb = $this->createQueryBuilder("da")
            ->select('distinct(me.id), me.nombre')
            ->InnerJoin('da.atencionmedica','am')
            ->InnerJoin('HCUERecetaBundle:Receta','r','WITH','r.atencionmedica_id = am.id')
            ->InnerJoin('HCUERecetaBundle:Recetadetalle','rd','WITH','rd.receta_id = r.id')
            ->InnerJoin('rd.medicamento','me')
            ->InnerJoin('rd.diagnosticoxrecetadetalle','rdd','WITH','rdd.recetadetalle_id = rd.id')
            ->Where('r.atencionmedica_id = :atencionmedica_id')
            ->andWhere('rdd.diagnostico_id = :diagnostico_id')
            ->andWhere('rdd.activo = :activo')
            ->setParameter('activo',Constantes::ESTADOGENERALHABILITADO)
            ->setParameter('atencionmedica_id',$atencionmedica_id)
            ->setParameter('diagnostico_id',$diagnostico_id)
            ->getQuery();

        return $qb;
    }

    /**
     * Obtener todos los diagnositcos de una atencion para el pdf
     * @param integer $atencion_id Id de la Atencion Medica
     * @return Array
     */
    public function getDiagnosticosPreviewByAtencion($atencion_id) {

        $diagnosticos = $this->createQueryBuilder('da')
            ->select("da.observacion, cie.nombre as nombre_cie,cie.codigo as codigo_cie ,cc.descripcion as ctcondiciondiagnostico, ct.descripcion as cttipodiagnostico, cr.descripcion as ctcronologiadiagnostico")
            ->innerJoin("da.cie", "cie")
            ->leftJoin("da.ctcondiciondiagnostico", "cc")
            ->leftJoin("da.cttipodiagnostico", "ct")
            ->leftJoin("da.ctcronologiadiagnostico", "cr")
            ->Where("da.atencionmedica_id=:atencion_id")
            ->andWhere('da.activo = :activo')
            ->setParameter('atencion_id', $atencion_id)
            ->setParameter('activo',Constantes::ESTADOGENERALHABILITADO)
            ->getQuery()
            ->getArrayResult();

        return $diagnosticos;

    }

    public function VerifyDiagnosticoInImagenQueryBuilder($diagnostico_id){
        $qb = $this->createQueryBuilder("da")
            ->select('da')
            ->InnerJoin('HCUEOrdenMedicaBundle:Diagnosticoxordendetalle','dd','WITH','dd.diagnosticoatencion_id = da.id')
            ->Where('da.id = :diagnostico_id')
            ->andWhere('da.activo = :activo')
            ->andWhere('dd.activo = :activo')
            ->setParameter('activo',Constantes::ESTADOGENERALHABILITADO)
            ->setParameter('diagnostico_id',$diagnostico_id)
            ->getQuery();

        return $qb;
    }

    /**
     * Permite consultar los diagnosticos mediante un array o un string separado por ","
     * @param string u array $ids
     * @return array u list de tipo HCUE/AtencionMedicBundle/Entity/Diagnosticoatencion
     **/
    public function getDiagnosticosIds($ids)
    {
        $qb = $this->createQueryBuilder('da');

        $qb->select('da, c, tipo, cond, cron')
            ->innerJoin('da.cie', 'c')
            ->leftJoin('da.cttipodiagnostico', 'tipo')
            ->leftJoin('da.ctcondiciondiagnostico', 'cond')
            ->leftJoin('da.ctcronologiadiagnostico', 'cron')
            ->where('da.activo = :estado_general')
            ->andWhere(
                $qb->expr()->in('da.id', $ids)
            )
            ->setParameter('estado_general', Constantes::ESTADOGENERALHABILITADO);

        $query = $qb->getQuery();

        return $query->getResult();
    }

    //funciones de epidemiología
    /**
     * Obtiene el QueryBuilder de la lista de diagnosticos que ha tenido un paciente
     * @param integer $paciente_id Id del paciente
     * @return QueryBuilder
     */
    public function getDiagnosticosEpidemiologiaByPaciente($paciente_id) {
        $qb = $this->createQueryBuilder('da')
            ->select('da')
//            ->InnerJoin('da.atencionmedica', 'am', 'WITH', 'am.id = da.atencionmedica_id and am.activo = :estado_general and am.ctestado_id = :estado')
            ->InnerJoin('da.atencionmedica', 'am', 'WITH', 'am.id = da.atencionmedica_id and am.activo = :estado_general')
            ->InnerJoin('HCUEAtencionMedicBundle:Epindividual', 'epi', 'WITH', 'epi.diagnosticoatencion_id = da.id')
            ->Where("am.paciente_id = :paciente_id")
            ->andWhere('da.activo = :estado_general')
            ->orderBy('da.fechacreacion', 'DESC')
            ->setParameters([
//                'estado' => Constantes::CT_ATENCIONFINALIZADA,
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'paciente_id' => $paciente_id
            ]);

        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }
}
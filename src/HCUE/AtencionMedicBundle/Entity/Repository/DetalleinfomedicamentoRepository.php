<?php

/*
 * @autor Estefanía Gonzaga
 */

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class DetalleinfomedicamentoRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Detalleinfomedicamento
     * @return QueryBuilder
     */
    public function getDetalleinfomedicamentoQueryBuilder()
    {
        return $this->createQueryBuilder('di')
            ->select('di')
            ->Where("di.estado=:estadogeneral")
            ->andWhere('di.activo=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene el QueryBuilder de un detalle infomedicamento por Id
     * @param integer $id Id del Detalle info medicamento
     * @return QueryBuilder
     */
    public function findDetalleinfomedicamentoQueryBuilder($id)
    {
        $qb = $this->getDetalleinfomedicamentoQueryBuilder();
        return $qb->andWhere("di.id=:id")->setParameter("id", $id);
    }

    /**
     * Obtiene el QueryBuilder de la lista de informedicamento identificados por un tipo de infomedicamento
     * @param integer $infomedicamento_id identifica el tipo de infomedicamento
     * @return QueryBuilder
     */
    public function getDetalleInfoMedQueryBuilder($infomedicamento_id)
    {
        $qb = $this->getDetalleinfomedicamentoQueryBuilder();
        return $qb->andWhere('di.infomedicamento_id=:infomedicamento_id')
            ->andWhere('di.infomedicamento_id is null')
            ->orderBy('di.valor', 'ASC')
            ->setParameter('infomedicamento_id', $infomedicamento_id);
    }

    /**
     * Obtiene la lista de infomedicamentos identificados por un tipo de infomedicamento
     * @param integer $infomedicamento_id identifica el tipo de infomedicamento
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento"
     */
    public function getDetalleinfomedicamento($infomedicamento_id, $arrayResult = false)
    {
        $queryBuilder = $this->getDetalleInfoMedQueryBuilder($infomedicamento_id);
        $q = $queryBuilder->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de infomedicamento identificados por el infomedicamento padre sin la recursividad
     * @param integer $infomedicamento_id identifica el infomedicamento padre
     * @return QueryBuilder
     */
    public function getDetalleinfomedicamentoChildrensQueryBuilder($infomedicamento_id)
    {
        $qb = $this->getDetalleinfomedicamentoQueryBuilder();
        return $qb->andWhere('di.infomedicamento_id=:infomedicamento_id')
            ->andWhere('di.detalleinfomedicamento_id is null')
            ->setParameter('infomedicamento_id', $infomedicamento_id);
    }

    /**
     * Obtiene la lista de detalles infomedicamento identificados por un infomedicamento padre
     * @param integer $infomedicamento_id identifica el infomedicamento padre
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento"
     */
    public function getDetalleinfomedicamentoChildrens($infomedicamento_id, $arrayResult = false)
    {
        $queryBuilder = $this->getDetalleinfomedicamentoChildrensQueryBuilder($infomedicamento_id);
        $q = $queryBuilder->getQuery();

        $resultId = ($arrayResult) ? "result_array_detalleinfomedicamento_by_infomed_$infomedicamento_id" : "result_detalleinfomedicamento_by_infomed_$infomedicamento_id";
        $q->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }
}

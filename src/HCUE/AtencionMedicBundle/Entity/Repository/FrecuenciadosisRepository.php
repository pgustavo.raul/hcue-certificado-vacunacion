<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class FrecuenciadosisRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Frecuenciadosis
     * @return QueryBuilder
     */
    public function getFrecuenciadosisQueryBuilder()
    {
        return $this->createQueryBuilder('fr')
            ->select('fr')
            ->where("fr.estado=:estadogeneral")
            ->andWhere("fr.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene la lista de las frecuencias
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\AtencionMedicBundle\Entity\Frecuenciasdosis"
     */
    public function getFrecuenciadosis($arrayResult = false)
    {
        $queryBuilder = $this->getFrecuenciadosisQueryBuilder();
        $q = $queryBuilder->getQuery();
        $resultId = ($arrayResult) ? "result_array_frecuenciadosis" : "result_frecuenciadosis";
        $q->useQueryCache(true)->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }
}

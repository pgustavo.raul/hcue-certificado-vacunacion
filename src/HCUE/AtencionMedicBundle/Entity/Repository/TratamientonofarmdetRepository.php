<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Lenin Vallejos
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class TratamientonofarmdetRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Tratamientonofarmdet            
        * @return QueryBuilder  
        */
	public function getTratamientonofarmdetQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');                                                 
        }


    /**
     * Eliminar todos los registros filtrando por tratamiento
     * @param integer $tratamientonofarm_id
     */
    public function deleteAllByTratamientoId($tratamientonofarm_id)
    {

        $em = $this->getEntityManager();
        $q = $em->createQueryBuilder();
        $q->delete('HCUEAtencionMedicBundle:tratamientonofarmdet', 'tnf')
            ->where('tnf.tratamientonofarm_id=:tratamientonofarm_id')
            ->setParameter('tratamientonofarm_id', $tratamientonofarm_id)
            ->getQuery()
            ->execute();


    }


}
?>
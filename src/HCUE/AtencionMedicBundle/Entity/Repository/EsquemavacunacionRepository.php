<?php
/*
* @autor Richard Veliz
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class EsquemavacunacionRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Esquemavacunacion
     * @return QueryBuilder
     */
    public function getEsquemavacunacionQueryBuilder()
    {
        return $this->createQueryBuilder('eqi')
            ->select('eqi')
            ->where('eqi.estado=:activo')
            ->andWhere('eqi.activo=:activo')
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene el listado de vacunas relacionas al esquema de vacunacion
     * @return Array
     */
    public function getEsquemavacunacion()
    {
        $qb = $this->getEsquemavacunacionQueryBuilder();
        $qbr = $qb
            ->select("vc.nombrevacuna,vc.color,eqi.cttipoesquema_id,eqi.dosis,eqi.edadoptimamaxanio,
            eqi.edadoptimamaxmes,eqi.edadoptimamaxdia,eqi.id")
            ->innerJoin('eqi.vacuna', 'vc')
            ->orderBy("eqi.dosis,eqi.cttipoesquema_id,eqi.edadoptimamaxanio,eqi.edadoptimamaxmes,eqi.edadoptimamaxdia,vc.nombrevacuna")
            ->getQuery();

        return ($qbr->getResult() > 0) ? $qbr->getResult() : array();
    }

    /**
     * Obtiene array distinct de dosis
     * @return Array
     */
    public function getDosis()
    {
        $qb = $this->getEsquemavacunacionQueryBuilder();
        $qbr = $qb
            ->select("eqi.dosis")
            ->distinct(true)
            ->orderBy("eqi.dosis")
            ->getQuery();

        return ($qbr->getResult() > 0) ? $qbr->getResult() : array();
    }

    /**
     * Obtiene el listado de esquema del paciente por la edad y el sexo
     * @param $view nombre de la vista
     * @param $edadanios anios del paciente
     * @param $edadmeses meses del paciente
     * @param $edaddias dias del paciente
     * @param $sexo Sexo del Paciente (Hombre,Mujer,Intersexual)
     * @return array
     */
    public function getEsquemavacunacionByPaciente($edadanios, $edadmeses, $edaddias, $sexo)
    {
        $edad = str_pad($edadanios, 3, 0, STR_PAD_LEFT)
            . str_pad($edadmeses, 2, 0, STR_PAD_LEFT)
            . str_pad($edaddias, 2, 0, STR_PAD_LEFT);

        $sql = "SELECT eqi.id,eqi.dosis,eqi.vacunanoobligatoria,eqi.vacunaobligatoria,
                eqi.cttipoesquema_id,te.valor nombreesquema,vc.nombrevacuna, vc.colorvacunacarnet COLOR,
                eqi.edadoptimamaxdia OPTMAXDIA,eqi.edadoptimamaxmes OPTMAXMES,eqi.edadoptimamaxanio OPTMAXANIO
                FROM HCUE_AMED.ESQUEMAVACUNACION eqi
                INNER JOIN HCUE_AMED.VACUNA vc ON vc.ID = eqi.VACUNA_ID
                INNER JOIN HCUE_CATALOGOS.DETALLECATALOGO te ON te.ID = eqi.CTTIPOESQUEMA_ID
                WHERE eqi.estado = :activo AND eqi.activo = :activo AND vc.ACTIVO = :activo AND vc.ESTADO = :activo
                AND CONCAT(CONCAT(LPAD(eqi.edadminanio, 3, 0), LPAD(eqi.edadminmes, 2, 0)), LPAD(eqi.edadmindia, 2, 0)) <= :edad
                AND vc.$sexo = :valorsexo 
                ORDER BY eqi.DOSIS,eqi.CTTIPOESQUEMA_ID,eqi.edadoptimamaxanio,eqi.edadoptimamaxmes,
                eqi.edadoptimamaxdia,vc.NOMBREVACUNA";
        $params['edad'] = $edad;
        $params['valorsexo'] = 1;
        $params['activo'] = Constantes::CT_ESTADOGENERALACTIVO;
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute($params);

        return $stmt->fetchAll();
    }


    /**
     * Obtiene si se le puede aplicar la vacuna o no al paciente
     * @param $id Id del Esquemavacunacion
     * @param $edadanios anios del paciente
     * @param $edadmeses meses del paciente
     * @param $edaddias dias del paciente
     * @return array
     */
    public function getAplicar($id, $edadanios, $edadmeses, $edaddias)
    {
        $edad = str_pad($edadanios, 3, 0, STR_PAD_LEFT)
            . str_pad($edadmeses, 2, 0, STR_PAD_LEFT)
            . str_pad($edaddias, 2, 0, STR_PAD_LEFT);
        $sql = "SELECT id FROM HCUE_AMED.ESQUEMAVACUNACION eqi WHERE eqi.estado= :activo AND eqi.activo= :activo
                AND eqi.id=:id
                AND CONCAT(CONCAT(LPAD(eqi.edadminanio,3,0), LPAD(eqi.edadminmes,2,0)), LPAD(eqi.edadmindia,2,0))<= :edad 
                AND CONCAT(CONCAT(LPAD(eqi.edadmaxanio,3,0), LPAD(eqi.edadmaxmes,2,0)), LPAD(eqi.edadmaxdia,2,0))>= :edad";
        $params['id'] = $id;
        $params['edad'] = $edad;
        $params['activo'] = Constantes::CT_ESTADOGENERALACTIVO;
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute($params);

        return ($stmt->fetchColumn()) ? true : false;
    }


    /**
     * Obtiene si el esquema esta en la edad optima de vacunacion para el paciente
     * @param $id Id del Esquemavacunacion
     * @param $edadanios anios del paciente
     * @param $edadmeses meses del paciente
     * @param $edaddias dias del paciente
     * @return array
     */
    public function getOptima($id, $edadanios, $edadmeses, $edaddias)
    {
        $edad = str_pad($edadanios, 3, 0, STR_PAD_LEFT)
            . str_pad($edadmeses, 2, 0, STR_PAD_LEFT)
            . str_pad($edaddias, 2, 0, STR_PAD_LEFT);
        $sql = "SELECT id FROM HCUE_AMED.ESQUEMAVACUNACION eqi WHERE eqi.estado= :activo AND eqi.activo= :activo
                AND eqi.id=:id
                AND CONCAT(CONCAT(LPAD(eqi.edadoptimaminanio,3,0), LPAD(eqi.edadoptimaminmes,2,0)), LPAD(eqi.edadoptimamindia,2,0))<= :edad 
                AND CONCAT(CONCAT(LPAD(eqi.edadoptimamaxanio,3,0), LPAD(eqi.edadoptimamaxmes,2,0)), LPAD(eqi.edadoptimamaxdia,2,0))>= :edad";
        $params['id'] = $id;
        $params['edad'] = $edad;
        $params['activo'] = Constantes::CT_ESTADOGENERALACTIVO;
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute($params);

        return ($stmt->fetchColumn()) ? true : false;
    }

    /**
     * Obtiene el listado de esquema del paciente por la edad y el sexo
     * @param $edadanios anios del paciente
     * @param $edadmeses meses del paciente
     * @param $edaddias dias del paciente
     * @param $sexo Sexo del Paciente (Hombre,Mujer,Intersexual)
     * @return array
     */
    public function getDosisByPaciente($edadanios, $edadmeses, $edaddias, $sexo)
    {
        $edad = str_pad($edadanios, 3, 0, STR_PAD_LEFT)
            . str_pad($edadmeses, 2, 0, STR_PAD_LEFT)
            . str_pad($edaddias, 2, 0, STR_PAD_LEFT);
        $sql = "SELECT DISTINCT eqi.dosis dosis, eqi.cttipoesquema_id
                FROM HCUE_AMED.ESQUEMAVACUNACION eqi
                INNER JOIN HCUE_AMED.VACUNA vc ON vc.ID = eqi.VACUNA_ID
                WHERE eqi.estado = :activo AND eqi.activo = :activo AND vc.ACTIVO = :activo AND vc.ESTADO = :activo
                AND CONCAT(CONCAT(LPAD(eqi.edadminanio, 3, 0), LPAD(eqi.edadminmes, 2, 0)), LPAD(eqi.edadmindia, 2, 0)) <= :edad
                AND vc.$sexo = :valorsexo
                ORDER BY eqi.dosis ASC";
        $params['edad'] = $edad;
        $params['valorsexo'] = 1;
        $params['activo'] = Constantes::CT_ESTADOGENERALACTIVO;
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute($params);

        return $stmt->fetchAll();
    }


    /**
     * Obtiene el listado de dosis por tipo de esquema
     * @param string $tipoesquema_id Ids separados por coma del catalogo de Tipo esquema
     * @return Object
     */
    public function getDosisByTipoesquema($tipoesquema_id)
    {
        $qb = $this->getEsquemavacunacionQueryBuilder();
        $qbr = $qb
            ->select("eqi.dosis DOSIS,eqi.cttipoesquema_id CTTIPOESQUEMA_ID")
            ->distinct(true)
            ->andWhere($qb->expr()->in("eqi.cttipoesquema_id", $tipoesquema_id))
            ->orderBy("eqi.dosis")
            ->getQuery();

        return ($qbr->getResult() > 0) ? $qbr->getResult() : array();
    }

    /**
     * Obtiene el listado de esquemas por ids separados por coma
     * @param String $ids Id de los esquemas separados por coma
     * @return Object
     */
    public function getEsquemavacunacionByIds($ids)
    {
        $qb = $this->getEsquemavacunacionQueryBuilder();
        $qbr = $qb
            ->innerJoin('eqi.vacuna', 'vc')
            ->andWhere($qb->expr()->in("eqi.id", $ids))
            ->getQuery();

        return ($qbr->getResult() > 0) ? $qbr->getResult() : array();
    }

    /**
     * Obtiene el listado de esquemas de vacunas por tipo de esquema
     * @param string $tipoesquema_id Ids separados por coma del catalogo de Tipo esquema
     * @return Object
     */
    public function getEsquemavacunacionByTipo($tipoesquema_id)
    {
        $qb = $this->getEsquemavacunacionQueryBuilder();
        $qbr = $qb
            ->select("eqi.id ID,eqi.dosis DOSIS,eqi.vacunanoobligatoria VACUNANOOBLIGATORIA,
            eqi.vacunaobligatoria VACUNAOBLIGATORIA,eqi.cttipoesquema_id CTTIPOESQUEMA_ID,
            te.valor NOMBREESQUEMA,vc.nombrevacuna NOMBREVACUNA, vc.color COLOR,
            eqi.edadoptimamaxdia OPTMAXDIA,eqi.edadoptimamaxmes OPTMAXMES,eqi.edadoptimamaxanio OPTMAXANIO")
            ->innerJoin('eqi.vacuna', 'vc')
            ->innerJoin('eqi.cttipoesquema', 'te')
            ->andWhere($qb->expr()->in("eqi.cttipoesquema_id", $tipoesquema_id))
            ->orderBy("eqi.dosis,eqi.cttipoesquema_id,eqi.edadoptimamaxanio,
            eqi.edadoptimamaxmes,eqi.edadoptimamaxdia,vc.nombrevacuna")
            ->getQuery();

        return ($qbr->getResult() > 0) ? $qbr->getResult() : array();
    }

    /**
     * Obtiene el listado de esquemas por ids separados por coma
     * @param String $ids Id de los esquemas separados por coma
     * @return Object
     */
    public function esUltimadosis($id)
    {
        $qb = $this->getEsquemavacunacionQueryBuilder();
        $qbr = $qb
            ->innerJoin('eqi.vacuna', 'vc')
            ->andWhere($qb->expr()->in("eqi.id", $id))
            ->getQuery();

        return ($qbr->getResult() > 0) ? $qbr->getResult() : array();
    }
}

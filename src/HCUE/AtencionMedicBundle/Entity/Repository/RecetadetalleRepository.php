<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class RecetadetalleRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Recetadetalle
     * @return QueryBuilder
     */
    public function getRecetadetalleQueryBuilder()
    {
        return $this->createQueryBuilder('rd')
            ->select('rd')
            ->where("rd.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene el detalle de la receta por id
     * @param integer $receta_id Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getRecetadetalleByRecetaId($receta_id, $arrayResult = false)
    {
        $queryBuilder = $this->getRecetadetalleQueryBuilder();
        $q = $queryBuilder
            ->innerJoin("rd.medicamento", "md")
            ->andWhere("rd.receta_id=:receta_id")
            ->andWhere("md.estupefaciente=:nopsicotropicooestupefaciente")
            ->andWhere("md.psicotropico=:nopsicotropicooestupefaciente")
            ->setParameter("receta_id", $receta_id)
            ->setParameter("nopsicotropicooestupefaciente", 0)
            ->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }
//
//    /**
//     * Obtiene el detalle de la receta por id
//     * @param integer $atencion_id Id de la atencion medica
//     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
//     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
//     */
//    public function getRecetadetalleByAtencionId($atencion_id, $arrayResult = false)
//    {
//        $queryBuilder = $this->getRecetadetalleQueryBuilder();
//        $q = $queryBuilder->select("rd,md,viad,fr,un")
//            ->innerJoin("rd.receta", "rc")
//            ->innerJoin("rd.medicamento", "md")
//            ->innerJoin("rd.viaadmmed", "viad")
//            ->innerJoin("rd.frecuenciadosis", "fr")
//            ->innerJoin("rd.unidadadministracion", "un")
//            ->andWhere("rc.atencionmedica_id=:atencion_id")
//            ->setParameter("atencion_id", $atencion_id)
//            ->getQuery();
//
//        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
//    }

    /**
     * Obtiene el detalle de la receta por id
     * @param integer $atencion_id Id de la atencion medica
     * @return array Lista de Objetos de tipo "HCUE\AtencionMedicBundle\Entity\Recetadetalle"
     */
    public function getRecetadetalleBasic($atencion_id)
    {
        $qb = $this->createQueryBuilder('rd')
            ->select('rd.id, rd.receta_id, rd.fechacreacion, rd.instrucciones, rd.duracion, rd.advertencias, rd.cantidad, rd.dosis')
            ->addSelect('md.nombre as medicamento, viad.valor as via, fr.descripcion as frecuencia')
            ->innerJoin("rd.receta", "rc")
            ->innerJoin("rd.medicamento", "md")
            ->innerJoin("rd.viaadmmed", "viad")
            ->innerJoin("rd.frecuenciadosis", "fr")
//            ->innerJoin("rd.unidadadministracion", "un")
            ->where("rc.atencionmedica_id = :atencion_id")
            ->andWhere('rd.activo = :estado')
            ->setParameters([
                'atencion_id' => $atencion_id,
                'estado' => Constantes::CT_ESTADOGENERALACTIVO
            ])
            ->getQuery();

        return $qb->getScalarResult();
    }

//    /**
//     * Verifica que un medicamento no se repita para una misma atención
//     * @param integer $medicamento_id Id del medicamento
//     * @param integer $atencion_id Id de la atencion medica
//     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
//     */
//    public function verifyRecetadetalleByMedicamentoId($medicamento_id, $atencion_id)
//    {
//        $queryBuilder = $this->getRecetadetalleQueryBuilder();
//        $q = $queryBuilder
//            ->innerJoin('HCUERecetaBundle:Receta','r','WITH','r.id = rd.receta_id')
//            ->andWhere("r.atencionmedica_id=:atencion_id")
//            ->andWhere("rd.medicamento_id=:medicamento_id")
//            ->setParameter("atencion_id", $atencion_id)
//            ->setParameter("medicamento_id", $medicamento_id)
//            ->getQuery()
//            ->getResult();
//
//        return $q;
//    }
}

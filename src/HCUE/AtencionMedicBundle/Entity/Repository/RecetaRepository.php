<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class RecetaRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Receta
     * @return QueryBuilder
     */
    public function getRecetaQueryBuilder()
    {
        return $this->createQueryBuilder('rc')
            ->select('rc')
            ->where('rc.activo=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene la receta por id
     * @param integer $id Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Receta"
     */
    public function getRecetaById($id, $arrayResult = false)
    {
        $queryBuilder = $this->getRecetaQueryBuilder();
        $q = $queryBuilder->andWhere("rc.id=:id")
            ->setParameter("id", $id)
            ->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene la receta por id
     * @param integer $atencion_id Id de la atencion medica
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Receta"
     */
    public function getRecetasByAtencion($atencion_id)
    {
        $queryBuilder = $this->getRecetaQueryBuilder();
        $q = $queryBuilder
            ->select('rc,p,ps')
            ->innerJoin("HCUERecetaBundle:Recetadetalle", "rd", "with", "rd.receta_id=rc.id")
            ->innerJoin("rd.medicamento", "md")
            ->innerJoin("rc.paciente", "p")
            ->innerJoin("p.persona", "ps")
            ->andWhere("rc.atencionmedica_id=:atencion_id")
            ->andWhere("md.estupefaciente=:nopsicotropicooestupefaciente")
            ->andWhere("md.psicotropico=:nopsicotropicooestupefaciente")
            ->setParameter("nopsicotropicooestupefaciente", 0)
            ->setParameter("atencion_id", $atencion_id)
            ->getQuery();

        return $q->getResult();
    }
}

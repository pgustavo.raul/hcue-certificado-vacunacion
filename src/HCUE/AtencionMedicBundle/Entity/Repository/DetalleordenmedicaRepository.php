<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class DetalleordenmedicaRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Detalleordenmedica
     * @return QueryBuilder
     */
    public function getDetalleordenmedicaQueryBuilder() {
        return $this->createQueryBuilder('do')
            ->select('do')
            ->where("do.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtener todos los detalles de ordenes médicas con el grupo
     * @param integer $ordenmedica_id
     * @return Array
     */
    public function getDetordenmedicaGrupoByOrdenmedicaId($ordenmedica_id) {
        $q = $this->createQueryBuilder('do')
            ->select("ta.codigo, do.tipoexamenvariable_id,te.nombre,te.tipoexamenvariable_id as id, te.descripcion, ca.descripcion as prioridad, ca.id as prioridad_id")
            ->innerJoin("do.tipoexamenvariable", "te")
            ->innerJoin('te.detalleserviciotarifario','ta')
            ->innerJoin('do.ctprioridad','ca')
            ->where("do.ordenmedica_id=:ordenmedica_id")
            ->andWhere("do.activo=:estadogeneral")
            ->setParameter('ordenmedica_id', $ordenmedica_id)
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO)
            ->getQuery()
            ->getArrayResult();
        return $q;
    }

    /**
     * Eliminar el detalle orden médica
     * @param integer $ordenmedica_id
     * @return boolean
     */
    public function deleteDetordenmedicaByOrdenmedicaId($ordenmedica_id)
    {
        $em = $this->getEntityManager();
        $q = $em->createQueryBuilder();
        $q->delete('HCUEOrdenMedicaBundle:Detalleordenmedica','do')
            ->where('do.ordenmedica_id=:ordenmedica_id')
            ->setParameter('ordenmedica_id', $ordenmedica_id)
            ->getQuery()
            ->execute();
        return true;
    }

    /**
     * Obtener todos los detalles de ordenes médicas
     * @param integer $atencionmedica_id
     * @return Array
     */
    public function getOrdenmedicadetalleByAtencionmedicaId($atencionmedica_id)  {
        $q = $this->createQueryBuilder('do')
            ->select("do.id, do.ordenmedica_id,t.codigo, te.nombre, te.descripcion as grupo, ca.descripcion as prioridad, ca.id as prioridad_id, o.cttipoordenmedica_id")
            ->innerJoin("do.ordenmedica", "o")
            ->innerJoin("do.tipoexamenvariable","te")
            ->innerJoin("te.detalleserviciotarifario", "t")
            ->innerJoin('do.ctprioridad','ca')
            ->where("o.atencionmedica_id=:atencionmedica_id")
            ->orderBy('do.fechamodificacion','DESC')
            ->setParameter('atencionmedica_id', $atencionmedica_id)
            ->getQuery()
            ->getArrayResult();
        return $q;
    }

    /**
     * Obtener todos los detalles de ordenes médicas
     * @param integer $atencionmedica_id
     * @return Array
     */
    public function getOrdenmedicadetalleByOrdenmedica($ordenmedica_id)  {
        $q = $this->createQueryBuilder('do')
            ->select("do.id, do.ordenmedica_id,t.codigo, te.nombre, te.descripcion as grupo, ca.descripcion as prioridad, ca.id as prioridad_id")
            ->innerJoin("do.ordenmedica", "o")
            ->innerJoin("do.tipoexamenvariable","te")
            ->innerJoin("te.detalleserviciotarifario", "t")
            ->innerJoin('do.ctprioridad','ca')
            ->where("do.ordenmedica_id=:ordenmedica_id")
            ->orderBy('do.fechamodificacion','DESC')
            ->setParameter('ordenmedica_id', $ordenmedica_id)
            ->getQuery()
            ->getArrayResult();
        return $q;
    }
}

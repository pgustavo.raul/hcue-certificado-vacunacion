<?php
/*
* @autor Richard Veliz
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class RegistrovacunacionRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Vacunacion
     * @return QueryBuilder
     */
    public function getVacunacionesQueryBuilder()
    {
        return $this->createQueryBuilder('inm')
            ->select('inm')
            ->where("inm.activo=:estadogeneral")
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }



    /**
     * Obtiene las vacunaciones por paciente
     * @param integer $paciente_id Id del paciente
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\VacunacionBundle\Entity\Vacunacion"
     */
    public function getVacunacionByPacienteId($paciente_id, $arrayResult = false)
    {
        $queryBuilder = $this->getVacunacionesQueryBuilder();
        $query = $queryBuilder
            ->select('inm,am,esq,ent')
            ->leftJoin('inm.atencionmedica', 'am')
            ->innerJoin('inm.esquemavacunacion', 'esq')
            ->innerJoin('esq.vacuna', 'vc')
            ->innerJoin('inm.entidad', 'ent')
            ->andWhere("inm.paciente_id=:paciente_id")
            ->setParameter("paciente_id", $paciente_id)
            ->orderBy('esq.dosis, vc.nombrevacuna, inm.fechavacunacion');

        return ($arrayResult) ? $query->getQuery()->getArrayResult() : $query->getQuery()->getResult();
    }



    /**
     * Obtiene el total de vacunaciones del paciente
     * @param integer $paciente_id Id del paciente
     * @return integer
     */
    public function getCountVacunacionByPacienteidCertificado($paciente_id, $requierecertificado=1)
    {
        $queryBuilder = $this->getVacunacionesQueryBuilder();
        $query = $queryBuilder
            ->select('count(inm.id)')
            ->leftJoin('inm.esquemavacunacion', 'esv')
            ->leftJoin('esv.vacuna', 'vac')
            ->andWhere("inm.paciente_id=:paciente_id")            
            ->setParameter("paciente_id", $paciente_id)
            ->andWhere('inm.activo=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);;
            if($requierecertificado){
                $query->andWhere("vac.certificado=:requierecertificado")
                ->setParameter("requierecertificado", $requierecertificado);
            }
            
            

        return (integer)$query->getQuery()->getSingleScalarResult();
    }

    /**
     * Obtiene datos de las vacunas del paciente
     * @param integer $paciente_id Id del paciente
     * @return array
     */
    public function getVacunacionByPacienteidCertificado($paciente_id, $requierecertificado=1,$arrayResult = false)
    {
        $queryBuilder = $this->getVacunacionesQueryBuilder();
        $query = $queryBuilder
            ->select('vac.nombrevacuna,esv.dosis,inm.fechavacunacion')
            ->leftJoin('inm.esquemavacunacion', 'esv')
            ->leftJoin('esv.vacuna', 'vac')
            ->andWhere("inm.paciente_id=:paciente_id")            
            ->setParameter("paciente_id", $paciente_id)
            ->andWhere('inm.activo=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);;
            if($requierecertificado){
                $query->andWhere("vac.certificado=:requierecertificado")
                ->setParameter("requierecertificado", $requierecertificado);
            }
        return ($arrayResult) ? $query->getQuery()->getArrayResult() : $query->getQuery()->getResult();
        //return (integer)$query->getQuery()->getSingleScalarResult();
    }

    /**
     *Obtiene las vacunaciones por paciente
     * @param array $paciente_id Id pacientes
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\VacunacionBundle\Entity\Vacunacion"
     */
    public function getVacunacionByArrayPacientes($arrayPaciente_id, $arrayResult = false) {
        $query=$this->createQueryBuilder('rv')
            ->select("rv")
            ->Where("rv.paciente_id in(:arrayPaciente_id)")
            ->setParameter('arrayPaciente_id', $arrayPaciente_id)
        ;

        return ($arrayResult) ? $query->getQuery()->getArrayResult() : $query->getQuery()->getResult();
    }
}
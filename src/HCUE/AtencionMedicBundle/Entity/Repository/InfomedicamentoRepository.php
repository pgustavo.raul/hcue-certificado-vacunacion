<?php
/*
* @autora Estefanía Gonzaga
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class InfomedicamentoRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Infomedicamento
     * @return QueryBuilder
     */
    public function getInfomedicamentoQueryBuilder()
    {
        return $this->createQueryBuilder('t')
            ->select('t');
    }
}

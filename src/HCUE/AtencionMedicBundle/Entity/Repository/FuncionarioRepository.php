<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class FuncionarioRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Funcionario
     * @return QueryBuilder
     */
    public function getFuncionarioQueryBuilder()
    {
        return $this->createQueryBuilder('t')
            ->select('t');
    }

    /**
     * Realiza la verificación de la Persona registrada como Funcionario.
     * @param integer $persona_id ID de la Persona a Registrarse como Funcionario
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objeto de tipo "HCUE\AtencionMedicBundle\Entity\Funcionario"
     */
    public function getFuncionarioPersona($persona_id, $arrayResult = false)
    {
        $qb = $this->createQueryBuilder("f")
            ->select('f')
            ->where('f.persona_id = :persona')
            ->andWhere('f.activo = :estado_general')
            ->setParameters([
                'estado_general' => Constantes::ESTADOGENERALHABILITADO,
                'persona' => $persona_id
            ]);

        $query = $qb->getQuery();
        return ($arrayResult) ? $query->getArrayResult() : $query->getOneOrNullResult();
    }

    /**
     * Realiza la verificación de la Persona registrada como Funcionario.
     * @param string $numeroidentificacion Identificacion del Funcionario que se intenta ingresar
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objeto de tipo "HCUE\AtencionMedicBundle\Entity\Funcionario"
     */
    public function getFuncionariobyIdentificacion($numeroidentificacion, $arrayResult = false)
    {
        $qb = $this->createQueryBuilder("f")
            ->select('f')
            ->innerJoin('f.persona', 'p', 'WITH', 'p.id = f.persona_id and p.activo = :estadogeneral')
            ->where('p.numeroidentificacion = :identificacion')
            ->andWhere('f.activo = :estadogeneral')
            ->setParameters([
                'estadogeneral' => Constantes::ESTADOGENERALHABILITADO,
                'identificacion' => $numeroidentificacion
            ]);

        $query = $qb->getQuery();
        return ($arrayResult) ? $query->getArrayResult() : $query->getOneOrNullResult();
    }
}

<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class TratamientonofarmreglasRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Tratamientonofarmreglas            
        * @return QueryBuilder  
        */
	public function getTratamientonofarmreglasQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');                                                 
        }

}
?>
<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class HorarioprescripcionRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Horarioprescripcion
     * @return QueryBuilder
     */
    public function getHorarioprescripcionQueryBuilder()
    {
        return $this->createQueryBuilder('h')
            ->select('h')
            ->where('h.activo=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene el horario de preescripcion relacionados a la receta
     * @param integer $receta_id Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getHorarioByRecetaId($receta_id, $arrayResult = false)
    {
        $queryBuilder = $this->getHorarioprescripcionQueryBuilder();
        $q = $queryBuilder->innerJoin('h.recetadetalle', 'rd')
            ->andWhere("rd.receta_id=:receta_id")
            ->setParameter("receta_id", $receta_id)
            ->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

//    /**
//     * Obtiene el horario de preescripcion por el detalle de la receta
//     * @param integer $recetadetalle_id Id del detalle de la receta
//     * @param boolean $arrayResult      <true> para retornar cada modelo o entidad de la lista como un array
//     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Horarioprescripcion"
//     */
//    public function getHorarioByRecetadetaleId(
//        $recetadetalle_id,
//        $arrayResult = false
//    ) {
//        $queryBuilder = $this->getHorarioprescripcionQueryBuilder();
//        $q = $queryBuilder
//            ->andWhere("h.recetadetalle_id=:recetadetalle_id")
//            ->setParameter("recetadetalle_id", $recetadetalle_id)
//            ->getQuery();
//
//        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
//    }
}

<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class TipoexamenvariableRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Tipoexamenvariable
     * @return QueryBuilder
     */
    public function getTipoexamenvariableQueryBuilder(){
        return $this->createQueryBuilder('t')
            ->select('t')
            ->where('t.activo=:estadogeneral')
            ->andWhere('t.estado=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Retorna todos los padres de registros de examenes ingresados en el sistema
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @param integer $catalogo
     * @return Array u objeto de tipo Tipoexamenvariable
     */
    public function getExamenesParentsByTipoAndNivel($arrayResult=false, $catalogo, $nivelatencion){
        $qb =$this->createQueryBuilder('t');
        $qb->select('DISTINCT (t.tipoexamenvariable_id) AS id, t.descripcion as nombre')
            ->innerJoin('t.detalleserviciotarifario', 'ta')
            ->where('t.activo=:estadogeneral')
            ->andWhere('t.estado=:estadogeneral')
            ->andWhere('t.cttipoordenmedica_id=:catalogo')
            ->setParameters([
                'estadogeneral' => Constantes::CT_ESTADOGENERALACTIVO,
                'catalogo' => $catalogo,
            ]);
        if($nivelatencion==Constantes::TE_PRIMERNIVELATENCION){
            $qb->andWhere('ta.primernivelvalor<>:cero')
                ->setParameter('cero' , 0);
        }
        else if($nivelatencion==Constantes::TE_SEGUNDONIVELATENCION){
            $qb->andWhere('ta.segundonivelvalor<>:cero')
                ->setParameter('cero' , 0);
        }
        else if($nivelatencion==Constantes::TE_TERCERNIVELATENCION){
            $qb->andWhere('ta.tercernivelvalor<>:cero')
                ->setParameter('cero' , 0);
        }

        $q = $qb->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Retorna todos los registros de examenes hijos ingresados en el sistema
     * @param integer $catalogo
     * @param integer $nivelatencion
     * @param integer $parent_id
     * @return Array u objeto de tipo Tipoexamenvariable
     */
    public function getProcedimientosByTipoAndNivelAndParentId($catalogo, $nivelatencion, $parent_id) {
        $qb =$this->createQueryBuilder('te');

        $qb->select('te.id, te.descripcion as grupo, ta.codigo, te.nombre')
            ->innerJoin('te.detalleserviciotarifario', 'ta')
            ->where('te.activo=:estadogeneral')
            ->andWhere('te.estado=:estadogeneral')
            ->andWhere('te.cttipoordenmedica_id=:catalogo')
            ->andWhere('te.tipoexamenvariable_id=:parent_id')
            ->setParameters([
                'estadogeneral' => Constantes::CT_ESTADOGENERALACTIVO,
                'catalogo' => $catalogo,
                'parent_id'=>$parent_id
            ]);

//        if($parent_id!=0){
//            $qb->andWhere('te.tipoexamenvariable_id=:parent_id')
//                ->setParameter('parent_id' , $parent_id);
//        }

        if($nivelatencion==Constantes::TE_PRIMERNIVELATENCION){
            $qb->andWhere('ta.primernivelvalor<>:cero')
                ->setParameter('cero' , 0);
        }
        else if($nivelatencion==Constantes::TE_SEGUNDONIVELATENCION){
            $qb->andWhere('ta.segundonivelvalor<>:cero')
                ->setParameter('cero' , 0);
        }
        else if($nivelatencion==Constantes::TE_TERCERNIVELATENCION){
            $qb->andWhere('ta.tercernivelvalor<>:cero')
                ->setParameter('cero' , 0);
        }

        $query = $qb->getQuery()
            ->getResult();

        return $query;
    }

    /**
     * Retorna todos los registros de examenes hijos ingresados en el sistema
     * @param integer $servicio_id
     * @param integer $catalogo
     * @return Array u objeto de tipo Tipoexamenvariable
     */
    public function getProcedimientosSelected($servicio_id, $catalogo) {
        $qb =$this->createQueryBuilder('te');
        $qb->select('te.id, te.descripcion as grupo, ta.codigo, te.nombre')
            ->innerJoin('te.detalleserviciotarifario', 'ta')
            ->where('te.activo=:estadogeneral')
            ->andWhere('te.estado=:estadogeneral')
            ->andWhere('te.cttipoordenmedica_id=:catalogo')
            ->andWhere($qb->expr()->in('te.id',$servicio_id))
            ->setParameters([
                'estadogeneral' => Constantes::CT_ESTADOGENERALACTIVO,
                'catalogo' => $catalogo,
            ]);

        $query = $qb->getQuery()
            ->getResult();

        return $query;
    }
}
?>
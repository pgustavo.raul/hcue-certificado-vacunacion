<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class FuncionarioentidadRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Funcionarioentidad
     * @return QueryBuilder
     */
    public function getFuncionarioentidadQueryBuilder()
    {
        return $this->createQueryBuilder('fe')
            ->select('fe');
    }

    /**
     * Obtiene las entidades asociadas a un funcionario
     * @param integer $funcionario_id Id del funcionario
     * @return Array u Objeto de tipo "Core\SeguridadBundle\Entity\Usuarioperfil"
     */
    public function obtenerFuncionarioEntidad($funcionario_id, $estado = true)
    {
        $qb = $this->createQueryBuilder("fe");
        $qb->Where('fe.activo = :activo')
            ->andWhere('fe.funcionario_id = :funcionario_id')
            ->orderBy('fe.id', 'ASC')
            ->setParameters([
                'activo' => Constantes::ESTADOGENERALHABILITADO,
                'funcionario_id' => $funcionario_id,
            ]);
        if ($estado) {
            $qb->andWhere('fe.estado = :estado')
                ->setParameter('estado', Constantes::ESTADOGENERALHABILITADO);
        }

        return $qb;
    }
}

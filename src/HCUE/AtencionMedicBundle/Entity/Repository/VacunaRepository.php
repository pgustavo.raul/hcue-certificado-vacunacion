<?php
/*
* @autor Lenin Vallejos
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class VacunaRepository extends EntityRepository
{


    public function getVacunas($certificado=1,$vacunador=1,$aceptavacuna=1,$arrayResult = false){

        $qb = $this->createQueryBuilder('p')
            ->select("p")
            ->Where("p.activo=:activo")
            ->andWhere("p.estado=:estado")
            ->andWhere("p.certificado=:certificado")
            ->andWhere("p.vacunador=:vacunador")
            ->andWhere("p.aceptavacuna=:aceptavacuna")
            ->setParameter('certificado', $certificado)
            ->setParameter('vacunador', $vacunador)
            ->setParameter('aceptavacuna', $aceptavacuna)
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO)
            ->setParameter('estado', Constantes::CT_ESTADOGENERALACTIVO)
        ;
        $q = $qb->getQuery();
        $q->useQueryCache(true)
            ->useResultCache(true, 3600, "vacunas_".$certificado.$vacunador.$aceptavacuna);


        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    public function getAllVacunas($state=Constantes::CT_ESTADOGENERALACTIVO,$arrayResult = false){

        $qb = $this->createQueryBuilder('p')
            ->select("p")
            ->Where("p.activo=:activo")
            ->andWhere("p.estado=:estado")
            ->setParameter('activo', $state)
            ->setParameter('estado', $state)
        ;
        $q = $qb->getQuery();
        $q->useQueryCache(true)
            ->useResultCache(true, 3600, "vacunas_all");
        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

}
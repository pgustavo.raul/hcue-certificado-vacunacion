<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class TratamientonofarmRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Tratamientonofarm            
        * @return QueryBuilder  
        */
	public function getTratamientonofarmQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');                                                 
        }


    /**
     * Obtiene las indicaciones a enviar tomando en cuenta los tratamientos
     * @param $atencionmedica_id
     * @param $cttratamiento_id
     * @return mixed
     */
    public function selectByTratamientoId($atencionmedica_id,$cttratamiento_id){

        $qb=$this->createQueryBuilder('t')
            ->select('t')
            ->innerJoin("t.ctcategoria","cat")
            ->where("t.activo=:activo")
            ->andWhere("cat.catalogo_id=:cttratamiento_id ")
            ->andWhere("t.atencionmedica_id=:atencionmedica_id")
            ->setParameter('cttratamiento_id', $cttratamiento_id)
            ->setParameter('atencionmedica_id', $atencionmedica_id)
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO)
        ;

        $query = $qb->getQuery();

        return $query->getResult();
    }
}
?>
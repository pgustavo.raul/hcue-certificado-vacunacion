<?php

namespace HCUE\AtencionMedicBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\AtencionMedicBundle\Entity\Constantes;

class DiagnosticoxrecetadetalleRepository extends EntityRepository
{
    /**
     * Obtiene el QueryBuilder de la lista de Diagnosticoxrecetadetalle
     * @return QueryBuilder
     */
    public function getDiagnosticoxrecetadetalleQueryBuilder()
    {
        return $this->createQueryBuilder('drd')
            ->select('drd')
            ->where('drd.activo=:estadogeneral')
            ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Obtiene los diagnosticos relacionados a la receta
     * @param integer $receta_id Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getDiagnosticosByRecetaId($receta_id, $arrayResult = false)
    {
        $queryBuilder = $this->getDiagnosticoxrecetadetalleQueryBuilder();
        $q = $queryBuilder->innerJoin('drd.recetadetalle', 'rd')
            ->andWhere("rd.receta_id=:receta_id")
            ->setParameter("receta_id", $receta_id)
            ->getQuery();

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Obtiene los diagnosticos relacionados a la receta
     * @param integer $recetadetalle_id Id del detalle de la receta
     * @param boolean $arrayResult      <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getDiagnosticosByRecetadetalleId(
        $recetadetalle_id,
        $arrayResult = false
    ) {
        $queryBuilder = $this->getDiagnosticoxrecetadetalleQueryBuilder();
        $q = $queryBuilder
            ->andWhere("drd.recetadetalle_id=:recetadetalle_id")
            ->setParameter("recetadetalle_id", $recetadetalle_id)
            ->getQuery();
        $result = null;

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }
}

<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.detalleordenmedica")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\DetalleordenmedicaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Detalleordenmedica{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.detalleordenmedica_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $ctprioridad_id
     * @ORM\Column(name="ctprioridad_id", type="integer", nullable=false)
     **/
    private $ctprioridad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctprioridad_id", referencedColumnName="id")
     **/
    private $ctprioridad;

    /**
     * @var integer $tipoexamenvariable_id
     * @ORM\Column(name="tipoexamenvariable_id", type="integer", nullable=true)
     **/
    private $tipoexamenvariable_id;

    /**
     * @ORM\ManyToOne(targetEntity="Tipoexamenvariable")
     * @ORM\JoinColumn(name="tipoexamenvariable_id", referencedColumnName="id")
     **/
    private $tipoexamenvariable;

//    /**
//     * @var integer $tarifarioestablecimiento_id
//     * @ORM\Column(name="tarifarioestablecimiento_id", type="integer", nullable=true)
//     **/
//    private $tarifarioestablecimiento_id;

//    /**
//     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Tarifarioestablecimiento")
//     * @ORM\JoinColumn(name="tarifarioestablecimiento_id", referencedColumnName="id")
//     **/
//    private $tarifarioestablecimiento;

    /**
     * @var integer $ordenmedica_id
     * @ORM\Column(name="ordenmedica_id", type="integer", nullable=true)
     **/
    private $ordenmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="Ordenmedica")
     * @ORM\JoinColumn(name="ordenmedica_id", referencedColumnName="id", onDelete="CASCADE")
     **/
    private $ordenmedica;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set ctprioridad_id
     * @param integer ctprioridad_id
     * @return detalleordenmedica
     **/
    public function setCtprioridadId($ctprioridad_id){
        $this->ctprioridad_id = $ctprioridad_id;
        return $this;
    }

    /**
     * Get ctprioridad_id
     * @return integer
     **/
    public function getCtprioridadId(){
        return $this->ctprioridad_id;
    }

    /**
     * Set ctprioridad
     * @param \Core\AppBundle\Entity\Catalogo $ctprioridad
     **/
    public function setCtprioridad(\Core\AppBundle\Entity\Catalogo $ctprioridad){
        $this->ctprioridad = $ctprioridad;
        return $this;
    }

    /**
     * Get ctprioridad
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtprioridad(){
        return $this->ctprioridad;
    }

    /**
     * Set tipoexamenvariable_id
     * @param integer tipoexamenvariable_id
     * @return detalleordenmedica
     **/
    public function setTipoexamenvariableId($tipoexamenvariable_id){
        $this->tipoexamenvariable_id = $tipoexamenvariable_id;
        return $this;
    }

    /**
     * Get tipoexamenvariable_id
     * @return integer
     **/
    public function getTipoexamenvariableId(){
        return $this->tipoexamenvariable_id;
    }

    /**
     * Set tipoexamenvariable
     * @param \HCUE\AtencionMedicBundle\Entity\Tipoexamenvariable $tipoexamenvariable
     **/
    public function setTipoexamenvariable(\HCUE\AtencionMedicBundle\Entity\Tipoexamenvariable $tipoexamenvariable){
        $this->tipoexamenvariable = $tipoexamenvariable;
        return $this;
    }

    /**
     * Get tipoexamenvariable
     * @return \HCUE\AtencionMedicBundle\Entity\Tipoexamenvariable
     **/
    public function getTipoexamenvariable(){
        return $this->tipoexamenvariable;
    }

//    /**
//     * Set tarifarioestablecimiento_id
//     * @param integer tarifarioestablecimiento_id
//     * @return detalleordenmedica
//     **/
//    public function setTarifarioestablecimientoId($tarifarioestablecimiento_id){
//        $this->tarifarioestablecimiento_id = $tarifarioestablecimiento_id;
//        return $this;
//    }
//
//    /**
//     * Get tarifarioestablecimiento_id
//     * @return integer
//     **/
//    public function getTarifarioestablecimientoId(){
//        return $this->tarifarioestablecimiento_id;
//    }
//
//    /**
//     * Set tarifarioestablecimiento
//     * @param \Core\AppBundle\Entity\Tarifarioestablecimiento $tarifarioestablecimiento
//     **/
//    public function setTarifarioestablecimiento(\Core\AppBundle\Entity\Tarifarioestablecimiento $tarifarioestablecimiento){
//        $this->tarifarioestablecimiento = $tarifarioestablecimiento;
//        return $this;
//    }
//
//    /**
//     * Get tarifarioestablecimiento
//     * @return \Core\AppBundle\Entity\Tarifarioestablecimiento
//     **/
//    public function getTarifarioestablecimiento(){
//        return $this->tarifarioestablecimiento;
//    }

    /**
     * Set ordenmedica_id
     * @param integer ordenmedica_id
     * @return detalleordenmedica
     **/
    public function setOrdenmedicaId($ordenmedica_id){
        $this->ordenmedica_id = $ordenmedica_id;
        return $this;
    }

    /**
     * Get ordenmedica_id
     * @return integer
     **/
    public function getOrdenmedicaId(){
        return $this->ordenmedica_id;
    }

    /**
     * Set ordenmedica
     * @param \HCUE\AtencionMedicBundle\Entity\Ordenmedica $ordenmedica
     **/
    public function setOrdenmedica(\HCUE\AtencionMedicBundle\Entity\Ordenmedica $ordenmedica){
        $this->ordenmedica = $ordenmedica;
        return $this;
    }

    /**
     * Get ordenmedica
     * @return \HCUE\AtencionMedicBundle\Entity\Ordenmedica
     **/
    public function getOrdenmedica(){
        return $this->ordenmedica;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return detalleordenmedica
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return detalleordenmedica
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return detalleordenmedica
     **/
    public function setActivo($activo){
        $this->activo = $activo;
        return $this;
    }

    /**
     * Get activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->activo=1;
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

    }

    public function __toString() {

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
?>
<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.detalleinfomedicamento")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\DetalleinfomedicamentoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Detalleinfomedicamento
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.detalleinfomedicamento_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $codigo
     * @ORM\Column(name="codigo", type="string",length=30, nullable=true)
     * */
    private $codigo;

    /**
     * @var string $valor
     * @ORM\Column(name="valor", type="string",length=256, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo valor")
     * */
    private $valor;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=2000, nullable=true)
     * */
    private $descripcion;

    /**
     * @var string $estado
     * @ORM\Column(name="estado", type="integer",nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     * */
    private $estado;

    /**
     * @var integer $infomedicamento_id
     * @ORM\Column(name="infomedicamento_id", type="integer", nullable=true)
     * */
    private $infomedicamento_id;

    /**
     * @var integer $detalleinfomedicamento_id
     * @ORM\Column(name="detalleinfomedicamento_id", type="integer", nullable=true)
     * */
    private $detalleinfomedicamento_id;

    /**
     * @ORM\ManyToOne(targetEntity="Infomedicamento")
     * @ORM\JoinColumn(name="infomedicamento_id", referencedColumnName="id")
     * */
    private $infomedicamento;

    /**
     * @ORM\ManyToOne(targetEntity="Detalleinfomedicamento")
     * @ORM\JoinColumn(name="detalleinfomedicamento_id", referencedColumnName="id")
     * */
    private $detalleinfomedicamento;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     * @param string codigo
     * @return detalleinfomedicamento
     * */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * Get codigo
     * @return string
     * */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set valor
     * @param string valor
     * @return detalleinfomedicamento
     * */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * Get valor
     * @return string
     * */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return detalleinfomedicamento
     * */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     * @param integer estado
     * @return detalleinfomedicamento
     * */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set infomedicamento_id
     * @param integer infomedicamento_id
     * @return detalleinfomedicamento
     * */
    public function setInfomedicamentoId($infomedicamento_id)
    {
        $this->infomedicamento_id = $infomedicamento_id;
        return $this;
    }

    /**
     * Get infomedicamento_id
     * @return integer
     * */
    public function getInfomedicamentoId()
    {
        return $this->infomedicamento_id;
    }

    /**
     * Set detalleinfomedicamento_id
     * @param integer detalleinfomedicamento_id
     * @return detalleinfomedicamento
     * */
    public function setDetalleinfomedicamentoId($detalleinfomedicamento_id)
    {
        $this->detalleinfomedicamento_id = $detalleinfomedicamento_id;
        return $this;
    }

    /**
     * Get detalleinfomedicamento_id
     * @return integer
     * */
    public function getDetalleinfomedicamentoId()
    {
        return $this->detalleinfomedicamento_id;
    }

    /**
     * Set infomedicamento
     * @param \HCUE\AtencionMedicBundle\Entity\Infomedicamento $infomedicamento
     * */
    public function setInfomedicamento(\HCUE\AtencionMedicBundle\Entity\Infomedicamento $infomedicamento)
    {
        $this->infomedicamento = $infomedicamento;
        return $this;
    }

    /**
     * Get infomedicamento
     * @return \HCUE\AtencionMedicBundle\Entity\Infomedicamento
     * */
    public function getInfomedicamento()
    {
        return $this->infomedicamento;
    }

    /**
     * Set detalleinfomedicamento
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $detalleinfomedicamento
     * @return detalleinfomedicamento
     * */
    public function setDetalleinfomedicamento(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $detalleinfomedicamento = null)
    {
        $this->detalleinfomedicamento = $detalleinfomedicamento;
        return $this;
    }

    /**
     * Get detalleinfomedicamento
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getDetalleinfomedicamento()
    {
        return $this->detalleinfomedicamento;
    }

    /**
     * Set activo
     * @param integer activo
     * @return detalleinfomedicamento
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return detalleinfomedicamento
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return detalleinfomedicamento
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __toString()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

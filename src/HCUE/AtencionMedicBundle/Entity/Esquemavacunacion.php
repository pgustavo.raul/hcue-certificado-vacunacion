<?php
/*
* @autor Richard Veliz
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.esquemavacunacion")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\EsquemavacunacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Esquemavacunacion
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.esquemavacunacion_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $vacuna_id
     * @ORM\Column(name="vacuna_id", type="integer", nullable=true)
     **/
    private $vacuna_id;

    /**
     * @ORM\ManyToOne(targetEntity="Vacuna")
     * @ORM\JoinColumn(name="vacuna_id", referencedColumnName="id")
     **/
    private $vacuna;


    /**
     * @var integer $cttipoesquema_id
     * @ORM\Column(name="cttipoesquema_id", type="integer", nullable=true)
     **/
    private $cttipoesquema_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipoesquema_id", referencedColumnName="id")
     **/
    private $cttipoesquema;

    /**
     * @var integer $dosis
     * @ORM\Column(name="dosis", type="integer", nullable=true)
     **/
    private $dosis;

    /**
     * @var string $vacunaobligatoria
     * @ORM\Column(name="vacunaobligatoria", type="string",length=500, nullable=true)
     **/
    private $vacunaobligatoria;

    /**
     * @var string $vacunanoobligatoria
     * @ORM\Column(name="vacunanoobligatoria", type="string",length=500, nullable=true)
     **/
    private $vacunanoobligatoria;

    /**
     * @var string $edadoptimavacuna
     * @ORM\Column(name="edadoptimavacuna", type="string",length=500, nullable=true)
     **/
    private $edadoptimavacuna;


    /**
     * @var integer $ctgprioritario_id
     * @ORM\Column(name="ctgprioritario_id", type="integer", nullable=false)
     **/
    private $ctgprioritario_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctgprioritario_id", referencedColumnName="id")
     **/
    private $ctgprioritario;

    /**
     * @var integer $tiempoesperadosis
     * @ORM\Column(name="tiempoesperadosis", type="integer", nullable=false)
     **/
    private $tiempoesperadosis;

    /**
     * @var integer $tiempoesperadosisdias
     * @ORM\Column(name="tiempoesperadosisdias", type="integer", nullable=false)
     **/
    private $tiempoesperadosisdias;

    /**
     * @var integer $validagruporiesgo
     * @ORM\Column(name="validagruporiesgo", type="integer", nullable=true)
     **/
    private $validagruporiesgo;

    /**
     * @var integer $edadmaxdia
     * @ORM\Column(name="edadmaxdia", type="integer", nullable=true)
     **/
    private $edadmaxdia;

    /**
     * @var integer $edadmaxmes
     * @ORM\Column(name="edadmaxmes", type="integer", nullable=true)
     **/
    private $edadmaxmes;

    /**
     * @var integer $edadmaxanio
     * @ORM\Column(name="edadmaxanio", type="integer", nullable=true)
     **/
    private $edadmaxanio;

    /**
     * @var integer $edadmindia
     * @ORM\Column(name="edadmindia", type="integer", nullable=true)
     **/
    private $edadmindia;

    /**
     * @var integer $edadminmes
     * @ORM\Column(name="edadminmes", type="integer", nullable=true)
     **/
    private $edadminmes;

    /**
     * @var integer $edadminanio
     * @ORM\Column(name="edadminanio", type="integer", nullable=true)
     **/
    private $edadminanio;

    /**
     * @var integer $edadoptimamaxdia
     * @ORM\Column(name="edadoptimamaxdia", type="integer", nullable=true)
     **/
    private $edadoptimamaxdia;

    /**
     * @var integer $edadoptimamaxmes
     * @ORM\Column(name="edadoptimamaxmes", type="integer", nullable=true)
     **/
    private $edadoptimamaxmes;

    /**
     * @var integer $edadoptimamaxanio
     * @ORM\Column(name="edadoptimamaxanio", type="integer", nullable=true)
     **/
    private $edadoptimamaxanio;

    /**
     * @var integer $edadoptimamindia
     * @ORM\Column(name="edadoptimamindia", type="integer", nullable=true)
     **/
    private $edadoptimamindia;

    /**
     * @var integer $edadoptimaminmes
     * @ORM\Column(name="edadoptimaminmes", type="integer", nullable=true)
     **/
    private $edadoptimaminmes;

    /**
     * @var integer $edadoptimaminanio
     * @ORM\Column(name="edadoptimaminanio", type="integer", nullable=true)
     **/
    private $edadoptimaminanio;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=true)
     **/
    private $estado;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;


    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTiempoesperadosisdias()
    {
        return $this->tiempoesperadosisdias;
    }

    /**
     * @param int $tiempoesperadosisdias
     */
    public function setTiempoesperadosisdias($tiempoesperadosisdias)
    {
        $this->tiempoesperadosisdias = $tiempoesperadosisdias;
    }

    /**
     * Set vacuna_id
     * @param integer vacuna_id
     * @return esquemavacunacion
     **/
    public function setVacunaId($vacuna_id)
    {
        $this->vacuna_id = $vacuna_id;
        return $this;
    }

    /**
     * Get vacuna_id
     * @return integer
     **/
    public function getVacunaId()
    {
        return $this->vacuna_id;
    }

    /**
     * Set vacuna
     * @param Vacuna $vacuna
     **/
    public function setVacuna(Vacuna $vacuna)
    {
        $this->vacuna = $vacuna;
        return $this;
    }

    /**
     * Get vacuna
     * @return Vacuna
     **/
    public function getVacuna()
    {
        return $this->vacuna;
    }

    /**
     * Set cttipoesquema_id
     * @param integer cttipoesquema_id
     * @return vacuna
     **/
    public function setCttipoesquemaId($cttipoesquema_id)
    {
        $this->cttipoesquema_id = $cttipoesquema_id;
        return $this;
    }

    /**
     * Get cttipoesquema_id
     * @return integer
     **/
    public function getCttipoesquemaId()
    {
        return $this->cttipoesquema_id;
    }

    /**
     * Set cttipoesquema
     * @param \Core\AppBundle\Entity\Catalogo $cttipoesquema
     **/
    public function setCttipoesquema(\Core\AppBundle\Entity\Catalogo $cttipoesquema)
    {
        $this->cttipoesquema = $cttipoesquema;
        return $this;
    }

    /**
     * Get cttipoesquema
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCttipoesquema()
    {
        return $this->cttipoesquema;
    }

    /**
     * Get nombre
     * @return string
     **/
    public function getNombre()
    {
        return $this->getNombrevacuna() . " " . $this->getDosis();
    }

    /**
     * Get nombrevacuna
     * @return string
     **/
    public function getNombrevacuna()
    {
        return $this->getVacuna()->getNombrevacuna();
    }


    /**
     * Get color
     * @return string
     **/
    public function getColor()
    {
        return $this->getVacuna()->getColor();
    }

    /**
     * Get nombreesquema
     * @return string
     **/
    public function getNombreesquema()
    {
        return $this->getCttipoesquema()->getValor();
    }

    /**
     * Set dosis
     * @param integer dosis
     * @return esquemavacunacion
     **/
    public function setDosis($dosis)
    {
        $this->dosis = $dosis;
        return $this;
    }

    /**
     * Get dosis
     * @return integer
     **/
    public function getDosis()
    {
        return $this->dosis;
    }

    /**
     * Set vacunaobligatoria
     * @param string vacunaobligatoria
     * @return esquemavacunacion
     **/
    public function setVacunaobligatoria($vacunaobligatoria)
    {
        $this->vacunaobligatoria = $vacunaobligatoria;
        return $this;
    }

    /**
     * Get vacunaobligatoria
     * @return string
     **/
    public function getVacunaobligatoria()
    {
        return $this->vacunaobligatoria;
    }

    /**
     * Set vacunanoobligatoria
     * @param string vacunanoobligatoria
     * @return esquemavacunacion
     **/
    public function setVacunanoobligatoria($vacunanoobligatoria)
    {
        $this->vacunanoobligatoria = $vacunanoobligatoria;
        return $this;
    }

    /**
     * Get vacunanoobligatoria
     * @return string
     **/
    public function getVacunanoobligatoria()
    {
        return $this->vacunanoobligatoria;
    }

    /**
     * Set edadoptimavacuna
     * @param string edadoptimavacuna
     * @return esquemavacunacion
     **/
    public function setEdadoptimavacuna($edadoptimavacuna)
    {
        $this->edadoptimavacuna = $edadoptimavacuna;
        return $this;
    }

    /**
     * Get edadoptimavacuna
     * @return string
     **/
    public function getEdadoptimavacuna()
    {
        return $this->edadoptimavacuna;
    }


    /**
     * Set ctgprioritario_id
     * @param integer ctgprioritario_id
     * @return atenciongprioritario
     **/
    public function setCtgprioritarioId($ctgprioritario_id)
    {
        $this->ctgprioritario_id = $ctgprioritario_id;
        return $this;
    }

    /**
     * Get ctgprioritario_id
     * @return integer
     **/
    public function getCtgprioritarioId()
    {
        return $this->ctgprioritario_id;
    }

    /**
     * Set ctgprioritario
     * @param \Core\AppBundle\Entity\Catalogo $ctgprioritario
     **/
    public function setCtgprioritario(\Core\AppBundle\Entity\Catalogo $ctgprioritario)
    {
        $this->ctgprioritario = $ctgprioritario;
        return $this;
    }

    /**
     * Get ctgprioritario
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtgprioritario()
    {
        return $this->ctgprioritario;
    }

    /**
     * Set tiempoesperadosis
     * @param integer tiempoesperadosis
     * @return esquemavacunacion
     **/
    public function setTiempoesperadosis($tiempoesperadosis){
        $this->tiempoesperadosis = $tiempoesperadosis;
        return $this;
    }

    /**
     * Get tiempoesperadosis
     * @return integer
     **/
    public function getTiempoesperadosis(){
        return $this->tiempoesperadosis;
    }

    /**
     * Set validagruporiesgo
     * @param integer validagruporiesgo
     * @return esquemavacunacion
     **/
    public function setValidagruporiesgo($validagruporiesgo)
    {
        $this->validagruporiesgo = $validagruporiesgo;
        return $this;
    }

    /**
     * Get validagruporiesgo
     * @return integer
     **/
    public function getValidagruporiesgo()
    {
        return $this->validagruporiesgo;
    }

    /**
     * Set edadmaxdia
     * @param integer edadmaxdia
     * @return esquemavacunacion
     **/
    public function setEdadmaxdia($edadmaxdia)
    {
        $this->edadmaxdia = $edadmaxdia;
        return $this;
    }

    /**
     * Get edadmaxdia
     * @return integer
     **/
    public function getEdadmaxdia()
    {
        return $this->edadmaxdia;
    }

    /**
     * Set edadmaxmes
     * @param integer edadmaxmes
     * @return esquemavacunacion
     **/
    public function setEdadmaxmes($edadmaxmes)
    {
        $this->edadmaxmes = $edadmaxmes;
        return $this;
    }

    /**
     * Get edadmaxmes
     * @return integer
     **/
    public function getEdadmaxmes()
    {
        return $this->edadmaxmes;
    }

    /**
     * Set edadmaxanio
     * @param integer edadmaxanio
     * @return esquemavacunacion
     **/
    public function setEdadmaxanio($edadmaxanio)
    {
        $this->edadmaxanio = $edadmaxanio;
        return $this;
    }

    /**
     * Get edadmaxanio
     * @return integer
     **/
    public function getEdadmaxanio()
    {
        return $this->edadmaxanio;
    }

    /**
     * Set edadmindia
     * @param integer edadmindia
     * @return esquemavacunacion
     **/
    public function setEdadmindia($edadmindia)
    {
        $this->edadmindia = $edadmindia;
        return $this;
    }

    /**
     * Get edadmindia
     * @return integer
     **/
    public function getEdadmindia()
    {
        return $this->edadmindia;
    }

    /**
     * Set edadminmes
     * @param integer edadminmes
     * @return esquemavacunacion
     **/
    public function setEdadminmes($edadminmes)
    {
        $this->edadminmes = $edadminmes;
        return $this;
    }

    /**
     * Get edadminmes
     * @return integer
     **/
    public function getEdadminmes()
    {
        return $this->edadminmes;
    }

    /**
     * Set edadminanio
     * @param integer edadminanio
     * @return esquemavacunacion
     **/
    public function setEdadminanio($edadminanio)
    {
        $this->edadminanio = $edadminanio;
        return $this;
    }

    /**
     * Get edadminanio
     * @return integer
     **/
    public function getEdadminanio()
    {
        return $this->edadminanio;
    }

    /**
     * Set edadoptimamaxdia
     * @param integer edadoptimamaxdia
     * @return esquemavacunacion
     **/
    public function setEdadoptimamaxdia($edadoptimamaxdia)
    {
        $this->edadoptimamaxdia = $edadoptimamaxdia;
        return $this;
    }

    /**
     * Get edadoptimamaxdia
     * @return integer
     **/
    public function getEdadoptimamaxdia()
    {
        return $this->edadoptimamaxdia;
    }

    /**
     * Set edadoptimamaxmes
     * @param integer edadoptimamaxmes
     * @return esquemavacunacion
     **/
    public function setEdadoptimamaxmes($edadoptimamaxmes)
    {
        $this->edadoptimamaxmes = $edadoptimamaxmes;
        return $this;
    }

    /**
     * Get edadoptimamaxmes
     * @return integer
     **/
    public function getEdadoptimamaxmes()
    {
        return $this->edadoptimamaxmes;
    }

    /**
     * Set edadoptimamaxanio
     * @param integer edadoptimamaxanio
     * @return esquemavacunacion
     **/
    public function setEdadoptimamaxanio($edadoptimamaxanio)
    {
        $this->edadoptimamaxanio = $edadoptimamaxanio;
        return $this;
    }

    /**
     * Get edadoptimamaxanio
     * @return integer
     **/
    public function getEdadoptimamaxanio()
    {
        return $this->edadoptimamaxanio;
    }

    /**
     * Set edadoptimamindia
     * @param integer edadoptimamindia
     * @return esquemavacunacion
     **/
    public function setEdadoptimamindia($edadoptimamindia)
    {
        $this->edadoptimamindia = $edadoptimamindia;
        return $this;
    }

    /**
     * Get edadoptimamindia
     * @return integer
     **/
    public function getEdadoptimamindia()
    {
        return $this->edadoptimamindia;
    }

    /**
     * Set edadoptimaminmes
     * @param integer edadoptimaminmes
     * @return esquemavacunacion
     **/
    public function setEdadoptimaminmes($edadoptimaminmes)
    {
        $this->edadoptimaminmes = $edadoptimaminmes;
        return $this;
    }

    /**
     * Get edadoptimaminmes
     * @return integer
     **/
    public function getEdadoptimaminmes()
    {
        return $this->edadoptimaminmes;
    }

    /**
     * Set edadoptimaminanio
     * @param integer edadoptimaminanio
     * @return esquemavacunacion
     **/
    public function setEdadoptimaminanio($edadoptimaminanio)
    {
        $this->edadoptimaminanio = $edadoptimaminanio;
        return $this;
    }

    /**
     * Get edadoptimaminanio
     * @return integer
     **/
    public function getEdadoptimaminanio()
    {
        return $this->edadoptimaminanio;
    }

    /**
     * Set estado
     * @param integer estado
     * @return esquemavacunacion
     **/
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return esquemavacunacion
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return esquemavacunacion
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return esquemavacunacion
     **/
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechacreacion = new \DateTime();
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();

    }

    public function __toString()
    {

    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

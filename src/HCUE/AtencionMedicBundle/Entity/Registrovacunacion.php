<?php
/*
* @autor Lenin Vallejos
*/
namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Core\AppBundle\Entity\Catalogo;

/**
 * @ORM\Table(name="hcue_amed.registrovacunacion")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\RegistrovacunacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Registrovacunacion
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.registrovacunacion_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $atencionmedica_id
     * @ORM\Column(name="atencionmedica_id", type="integer", nullable=true)
     **/
    private $atencionmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @var integer $esquemavacunacion_id
     * @ORM\Column(name="esquemavacunacion_id", type="integer", nullable=true)
     **/
    private $esquemavacunacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Esquemavacunacion")
     * @ORM\JoinColumn(name="esquemavacunacion_id", referencedColumnName="id")
     **/
    private $esquemavacunacion;

    /**
     * @var integer $paciente_id
     * @ORM\Column(name="paciente_id", type="integer", nullable=false)
     **/
    private $paciente_id;

    /**
     * @ORM\ManyToOne(targetEntity="\HCUE\PacienteBundle\Entity\Paciente")
     * @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     **/
    private $paciente;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     * */
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     * */
    private $entidad;

    /**
     * @var integer $ctespecialidadmedica_id
     * @ORM\Column(name="ctespecialidadmedica_id", type="integer", nullable=false)
     **/
    private $ctespecialidadmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctespecialidadmedica_id", referencedColumnName="id")
     **/
    private $ctespecialidadmedica;

    /**
     * @var integer $internaexterna
     * @ORM\Column(name="internaexterna", type="integer", nullable=false)
     * */
    private $internaexterna;

    /**
     * @var \Datetime $fechavacunacion
     * @ORM\Column(name="fechavacunacion", type="datetime", nullable=false)
     **/
    private $fechavacunacion;

    /**
     * @var \Datetime $proximavacunacion
     * @ORM\Column(name="proximavacunacion", type="datetime", nullable=false)
     **/
    private $proximavacunacion;

    /**
     * @var string $lote
     * @ORM\Column(name="lote", type="string", length=200, nullable=true)
     * */
    private $lote;

    /**
     * @var integer $embarazada
     * @ORM\Column(name="embarazada", type="integer", nullable=true)
     **/
    private $embarazada;

    /**
     * @var integer $estadovacunacion
     * @ORM\Column(name="estadovacunacion", type="integer", nullable=false)
     **/
    private $estadovacunacion;

    /**
     * @var string $observacion
     * @ORM\Column(name="observacion", type="string", length=2000, nullable=false)
     * */
    private $observacion;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     **/
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * @var \Datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var \Datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @ORM\OneToOne(targetEntity="Core\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuariocreacion_id", referencedColumnName="id")
     * */
    private $usuariocreacion;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var string $vacunadorid
     * @ORM\Column(name="vacunadorid", type="string", length=20, nullable=true)
     * */
    private $vacunadorid;

    /**
     * @var string $vacunadornombre
     * @ORM\Column(name="vacunadornombre", type="string", length=50, nullable=true)
     * */
    private $vacunadornombre;

    /**
     * @var integer $cttiporegistro_id
     * @ORM\Column(name="cttiporegistro_id", type="integer", nullable=false)
     **/
    private $cttiporegistro_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttiporegistro_id", referencedColumnName="id")
     **/
    private $cttiporegistro;

    /**
     * @var string $puntovacunaciondesc
     * @ORM\Column(name="puntovacunaciondesc", type="string", length=150, nullable=true)
     * */
    private $puntovacunaciondesc;

    /**
     * @var string $puntovacunacionunicod
     * @ORM\Column(name="puntovacunacionunicod", type="string", length=10, nullable=true)
     * */
    private $puntovacunacionunicod;

    /**
     * @var string $registrador
     * @ORM\Column(name="registrador", type="string", length=50, nullable=true)
     * */
    private $registrador;

    /**
     * @var integer $fasevacunacion
     * @ORM\Column(name="fasevacunacion", type="integer", nullable=false)
     **/
    private $fasevacunacion;

    /**
     * @var string $agendado
     * @ORM\Column(name="agendado", type="string", length=1, nullable=true)
     * */
    private $agendado;

    /**
     * @var integer $exteriordosisprevia
     * @ORM\Column(name="exteriordosisprevia", type="integer", nullable=true)
     **/
    private $exteriordosisprevia;

    /**
     * @var \Datetime $exteriordosisfecha
     * @ORM\Column(name="exteriordosisfecha", type="datetime", nullable=true)
     **/
    private $exteriordosisfecha;

    /**
     * @var integer $exteriordosispais_id
     * @ORM\Column(name="exteriordosispais_id", type="integer", nullable=true)
     **/
    private $exteriordosispais_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Pais")
     * @ORM\JoinColumn(name="exteriordosispais_id", referencedColumnName="id")
     * */
    private $exteriordosispais;

    /**
     * @var string $exteriordosislote
     * @ORM\Column(name="exteriordosislote", type="string", length=200, nullable=true)
     * */
    private $exteriordosislote;

    public function __construct()
    {

    }


    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set atencionmedica_id
     * @param integer atencionmedica_id
     * @return vacunacion
     **/
    public function setAtencionmedicaId($atencionmedica_id)
    {
        $this->atencionmedica_id = $atencionmedica_id;
        return $this;
    }

    /**
     * Get atencionmedica_id
     * @return integer
     **/
    public function getAtencionmedicaId()
    {
        return $this->atencionmedica_id;
    }

    /**
     * Set atencionmedica
     * @param AtencionmedicaBasic $atencionmedica
     * @return vacunacion
     **/
    public function setAtencionmedica(AtencionmedicaBasic $atencionmedica)
    {
        $this->atencionmedica = $atencionmedica;
        return $this;
    }

    /**
     * Get atencionmedica
     * @return AtencionmedicaBasic
     **/
    public function getAtencionmedica()
    {
        return $this->atencionmedica;
    }

    /**
     * Set esquemavacunacion_id
     * @param integer esquemavacunacion_id
     * @return vacunacion
     **/
    public function setEsquemavacunacionId($esquemavacunacion_id)
    {
        $this->esquemavacunacion_id = $esquemavacunacion_id;
        return $this;
    }

    /**
     * Get esquemavacunacion_id
     * @return integer
     **/
    public function getEsquemavacunacionId()
    {
        return $this->esquemavacunacion_id;
    }

    /**
     * Set esquemavacunacion
     * @param \HCUE\VacunacionBundle\Entity\Esquemavacunacion $esquemavacunacion
     * @return vacunacion
     **/
    public function setEsquemavacunacion(Esquemavacunacion $esquemavacunacion)
    {
        $this->esquemavacunacion = $esquemavacunacion;
        return $this;
    }

    /**
     * Get esquemavacunacion
     * @return \HCUE\VacunacionBundle\Entity\Esquemavacunacion
     **/
    public function getEsquemavacunacion()
    {
        return $this->esquemavacunacion;
    }

    /**
     * Set paciente_id
     * @param integer paciente_id
     * @return vacunacion
     **/
    public function setPacienteId($paciente_id)
    {
        $this->paciente_id = $paciente_id;
        return $this;
    }

    /**
     * Get paciente_id
     * @return integer
     **/
    public function getPacienteId()
    {
        return $this->paciente_id;
    }

    /**
	* Set paciente
	* @param \HCUE\PacienteBundle\Entity\Paciente $paciente
	**/
	public function setPaciente(\HCUE\PacienteBundle\Entity\Paciente $paciente){
        $this->paciente = $paciente;
        return $this;
   }

    /**
     * Get paciente
     * @return Paciente
     **/
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return vacunacion
     * */
    public function setEntidadId($entidad_id)
    {
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     * */
    public function getEntidadId()
    {
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     * @return vacunacion
     * */
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad)
    {
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     * */
    public function getEntidad()
    {
        return $this->entidad;
    }

    /**
     * Set ctespecialidadmedica_id
     * @param integer ctespecialidadmedica_id
     * @return atencionmedica
     **/
    public function setCtespecialidadmedicaId($ctespecialidadmedica_id)
    {
        $this->ctespecialidadmedica_id = $ctespecialidadmedica_id;
        return $this;
    }

    /**
     * Get ctespecialidadmedica_id
     * @return integer
     **/
    public function getCtespecialidadmedicaId()
    {
        return $this->ctespecialidadmedica_id;
    }

    /**
     * Set ctespecialidadmedica
     * @param \Core\AppBundle\Entity\Catalogo $ctespecialidadmedica
     **/
    public function setCtespecialidadmedica(\Core\AppBundle\Entity\Catalogo $ctespecialidadmedica)
    {
        $this->ctespecialidadmedica = $ctespecialidadmedica;
        return $this;
    }

    /**
     * Get ctespecialidadmedica
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtespecialidadmedica()
    {
        return $this->ctespecialidadmedica;
    }

    /**
     * Set internaexterna
     * @param integer internaexterna
     * @return vacunacion
     * */
    public function setInternaexterna($internaexterna)
    {
        $this->internaexterna = $internaexterna;
        return $this;
    }

    /**
     * Get internaexterna
     * @return integer
     * */
    public function getInternaexterna()
    {
        return $this->internaexterna;
    }

    /**
     * Set fechavacunacion
     * @param \DateTime fechavacunacion
     * @return vacunacion
     * */
    public function setFechavacunacion(\DateTime $fechavacunacion)
    {
        $this->fechavacunacion = $fechavacunacion;
        return $this;
    }

    /**
     * Get fechavacunacion
     * @return \DateTime
     * */
    public function getFechavacunacion()
    {
        return $this->fechavacunacion;
    }


    /**
     * Set proximavacunacion
     * @param \DateTime proximavacunacion
     * @return vacunacion
     * */
    public function setProximavacunacion(\DateTime $proximavacunacion = null)
    {
        $this->proximavacunacion = $proximavacunacion;
        return $this;
    }

    /**
     * Get proximavacunacion
     * @return \DateTime
     * */
    public function getProximavacunacion()
    {
        return $this->proximavacunacion;
    }

    /**
     * Set lote
     * @param string lote
     * @return vacunacion
     * */
    public function setLote($lote)
    {
        $this->lote = $lote;
        return $this;
    }

    /**
     * Get lote
     * @return string
     * */
    public function getLote()
    {
        return $this->lote;
    }

    /**
     * Set lote
     * @param string observacion
     * @return vacunacion
     * */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
        return $this;
    }

    /**
     * Set embarazada
     * @param integer embarazada
     * @return vacunacion
     **/
    public function setEmbarazada($embarazada)
    {
        $this->embarazada = $embarazada;
        return $this;
    }

    /**
     * Get embarazada
     * @return integer
     **/
    public function getEmbarazada()
    {
        return $this->embarazada;
    }

    /**
     * Get observacion
     * @return string
     * */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set estadovacunacion
     * @param integer estadovacunacion
     * @return vacunacion
     * */
    public function setEstadovacunacion($estadovacunacion)
    {
        $this->estadovacunacion = $estadovacunacion;
        return $this;
    }

    /**
     * Get estadovacunacion
     * @return integer
     * */
    public function getEstadovacunacion()
    {
        return $this->estadovacunacion;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return vacunacion
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return vacunacion
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Get usuariocreacion
     * @return \Core\SeguridadBundle\Entity\Usuario
     * */
    public function getUsuariocreacion()
    {
        return $this->usuariocreacion;
    }

    /**
     * Get usuariocreacion
     * @param \Core\SeguridadBundle\Entity\Usuario $usuario
     * @return \Core\SeguridadBundle\Entity\Usuario
     * */
    public function setUsuariocreacion(\Core\SeguridadBundle\Entity\Usuario $usuario)
    {
        $this->usuariocreacion = $usuario;
        return $this;
    }

    /**
     * Get fechacreacion
     * @return \DateTime
     * */
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Set estado
     * @param integer estado
     * @return vacunacion
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer activo
     * @return vacunacion
     **/
    public function setActivo($activo)
    {
        $this->activo = $activo;
        return $this;
    }

    /**
     * Set vacunadorid
     * @param string vacunadorid
     * @return vacunacion
     * */
    public function setVacunadorid($vacunadorid)
    {
        $this->vacunadorid = $vacunadorid;
        return $this;
    }

    /**
     * Get vacunadorid
     * @return string
     * */
    public function getVacunadorid()
    {
        return $this->vacunadorid;
    }

    /**
     * @return string
     */
    public function getVacunadornombre()
    {
        return $this->vacunadornombre;
    }

    /**
     * @param string $vacunadornombre
     * @return string
     */
    public function setVacunadornombre($vacunadornombre)
    {
        $this->vacunadornombre = $vacunadornombre;
        return $this;
    }

    /**
	* Set cttiporegistro_id
	* @param integer cttiporegistro_id
	* @return paciente
	**/
	public function setCttiporegistroId($cttiporegistro_id){
        $this->cttiporegistro_id = $cttiporegistro_id;
        return $this;
    }

   /**
   * Get cttiporegistro_id
   * @return integer
   **/
   public function getCttiporegistroId(){
        return $this->cttiporegistro_id;
    }

    /**
     * Set cttiporegistro
     * @param Catalogo $cttiporegistro
     **/
    public function setCttiporegistro(Catalogo $cttiporegistro)
    {
        $this->cttiporegistro = $cttiporegistro;
        return $this;
    }

    /**
     * Get cttiporegistro
     * @return Catalogo
     **/
    public function getCttiporegistro()
    {
        return $this->cttiporegistro;
    }

    /**
     * Get $puntovacunaciondesc
     * @return string
     */
    public function getPuntovacunaciondesc()
    {
        return $this->puntovacunaciondesc;
    }

    /**
     * Set $puntovacunaciondesc
     * @param  string  $puntovacunaciondesc
     */
    public function setPuntovacunaciondesc(string $puntovacunaciondesc)
    {
        $this->puntovacunaciondesc = $puntovacunaciondesc;
        return $this;
    }

    /**
     * Get $puntovacunacionunicod
     * @return string
     */
    public function getPuntovacunacionunicod()
    {
        return $this->puntovacunacionunicod;
    }

    /**
     * Set $puntovacunacionunicod
     * @param  string  $puntovacunacionunicod 
     */
    public function setPuntovacunacionunicod(string $puntovacunacionunicod)
    {
        $this->puntovacunacionunicod = $puntovacunacionunicod;
        return $this;
    }

    /**
     * Get $registrador
     * @return  string
     */
    public function getRegistrador()
    {
        return $this->registrador;
    }

    /**
     * Set $registrador
     * @param string  $registrador
     */
    public function setRegistrador(string $registrador)
    {
        $this->registrador = $registrador;
        return $this;
    }

    /**
     * Get $fasevacunacion
     * @return integer
     */
    public function getFasevacunacion()
    {
        return $this->fasevacunacion;
    }

    /**
     * Set $fasevacunacion
     * @param integer $fasevacunacion
     */
    public function setFasevacunacion($fasevacunacion)
    {
        $this->fasevacunacion = $fasevacunacion;
        return $this;
    }

    /**
     * Get $agendado
     * @return string
     */
    public function getAgendado()
    {
        return $this->agendado;
    }

    /**
     * Set $agendado
     * @param string $agendado
     */
    public function setAgendado(string $agendado)
    {
        $this->agendado = $agendado;
        return $this;
    }

    /**
     * Get $exteriordosisprevia
     * @return integer
     */
    public function getExteriordosisprevia()
    {
        return $this->exteriordosisprevia;
    }

    /**
     * Set $exteriordosisprevia
     * @param integer $exteriordosisprevia
     */
    public function setExteriordosisprevia($exteriordosisprevia)
    {
        $this->exteriordosisprevia = $exteriordosisprevia;
        return $this;
    }

    /**
     * Set exteriordosisfecha
     * @param \DateTime exteriordosisfecha
     * @return vacunacion
     * */
    public function setExteriordosisfecha(\DateTime $exteriordosisfecha)
    {
        $this->exteriordosisfecha = $exteriordosisfecha;
        return $this;
    }

    /**
     * Get exteriordosisfecha
     * @return \DateTime
     * */
    public function getExteriordosisfecha()
    {
        return $this->exteriordosisfecha;
    }

    /**
     * Get $exteriordosispais_id
     * @return  integer
     */
    public function getExteriordosispais_id()
    {
        return $this->exteriordosispais_id;
    }

    /**
     * Set $exteriordosispais_id
     * @param integer $exteriordosispais_id
     */
    public function setExteriordosispais_id($exteriordosispais_id)
    {
        $this->exteriordosispais_id = $exteriordosispais_id;
        return $this;
    }

    /**
     * Get the value of pais
     * @return \Core\AppBundle\Entity\Pais
     */
    public function getExteriordosispais()
    {
        return $this->exteriordosispais;
    }

    /**
     * Set exteriordosispais
     * @param \Core\AppBundle\Entity\Pais $exteriordosispais
     * */
    public function setExteriordosispais(\Core\AppBundle\Entity\Pais $exteriordosispais)
    {
        $this->exteriordosispais = $exteriordosispais;
        return $this;
    }

    /**
     * Get $exteriordosislote
     * @return string
     */
    public function getExteriordosislote()
    {
        return $this->exteriordosislote;
    }

    /**
     * Set $exteriordosislote
     * @param string $exteriordosislote
     */
    public function setExteriordosislote(string $exteriordosislote)
    {
        $this->exteriordosislote = $exteriordosislote;
        return $this;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();

    }

    public function __toString()
    {

    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

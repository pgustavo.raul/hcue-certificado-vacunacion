<?php 
/*
* @autor Lenin VAllejos
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.certificadovacuna")
* @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\CertificadovacunaRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Certificadovacuna{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.certificadovacuna_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $identificacion
	* @ORM\Column(name="identificacion", type="string",length=25, nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo identificacion")
	**/
	private $identificacion;

	/**
	* @var datetime $fechanacimiento
	* @ORM\Column(name="fechanacimiento", type="datetime", nullable=true)
	**/
	private $fechanacimiento;

	/**
	* @var string $correo
	* @ORM\Column(name="correo", type="string",length=100, nullable=true)
	**/
	private $correo;

	/**
	* @var integer $cttipoidentificacion_id
	* @ORM\Column(name="cttipoidentificacion_id", type="integer", nullable=false)
	**/
	private $cttipoidentificacion_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="cttipoidentificacion_id", referencedColumnName="id")
	**/
	private $cttipoidentificacion;

	
	/**
	* @var integer $ctestadoverificacion_id
	* @ORM\Column(name="ctestadoverificacion_id", type="integer", nullable=false)
	**/
	private $ctestadoverificacion_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctestadoverificacion_id", referencedColumnName="id")
	**/
	private $ctestadoverificacion;

	/**
	* @var integer $ctsexo_id
	* @ORM\Column(name="ctsexo_id", type="integer", nullable=true)
	**/
	private $ctsexo_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctsexo_id", referencedColumnName="id")
	**/
	private $ctsexo;

	/**
	 * @var integer $paciente_id
	 * @ORM\Column(name="paciente_id", type="integer", nullable=false)
	 **/
	private $paciente_id;

	/**
	* @ORM\ManyToOne(targetEntity="HCUE\PacienteBundle\Entity\Paciente")
	* @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
	**/
	private $paciente;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=true)
	**/
	private $estado;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;


	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;



	/**
	 * @var string $nombres
	* @ORM\Column(name="nombres", type="string",length=100, nullable=false)
	**/
	private $nombres;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=true)
	**/
	private $activo;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set identificacion
	* @param string identificacion
	* @return certificadovacuna
	**/
	public function setIdentificacion($identificacion){
		 $this->identificacion = $identificacion;
		 return $this;
	}

	/**
	* Get identificacion
	* @return string
	**/
	public function getIdentificacion(){
		 return $this->identificacion;
	}

	/**
	* Set fechanacimiento
	* @param datetime fechanacimiento
	* @return certificadovacuna
	**/
	public function setFechanacimiento($fechanacimiento){
		//$this->fechanacimiento = new \DateTime($fechanacimiento);
		$this->fechanacimiento = $fechanacimiento;
        return $this;
	}

	/**
	* Get fechanacimiento
	* @return date
	**/
	public function getFechanacimiento(){
		 return $this->fechanacimiento;
	}

	/**
	* Set correo
	* @param string correo
	* @return certificadovacuna
	**/
	public function setCorreo($correo){
		 $this->correo = $correo;
		 return $this;
	}

	/**
	* Get correo
	* @return string
	**/
	public function getCorreo(){
		 return $this->correo;
	}

	/**
	* Set ctestadoverificacion_id
	* @param integer ctestadoverificacion_id
	* @return certificadovacuna
	**/
	public function setCtestadoverificacionId($ctestadoverificacion_id){
		 $this->ctestadoverificacion_id = $ctestadoverificacion_id;
		 return $this;
	}

	/**
	* Get ctestadoverificacion_id
	* @return integer
	**/
	public function getCtestadoverificacionId(){
		 return $this->ctestadoverificacion_id;
	}

	/**
	* Set ctestadoverificacion
	* @param \Core\AppBundle\Entity\Catalogo $ctestadoverificacion
	**/
	public function setCtestadoverificacion(\Core\AppBundle\Entity\Catalogo $ctestadoverificacion){
		 $this->ctestadoverificacion = $ctestadoverificacion;
		 return $this;
	}

	/**
	* Get ctestadoverificacion
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtestadoverificacion(){
		 return $this->ctestadoverificacion;
	}

	

	/**
	* Set cttipoidentificacion_id
	* @param integer cttipoidentificacion_id
	* @return certificadovacuna
	**/
	public function setCttipoidentificacionId($cttipoidentificacion_id){
		$this->cttipoidentificacion_id = $cttipoidentificacion_id;
		return $this;
   }

   /**
   * Get cttipoidentificacion_id
   * @return integer
   **/
   public function getCttipoidentificacionId(){
		return $this->cttipoidentificacion_id;
   }

   	/**
	* Set cttipoidentificacion
	* @param \Core\AppBundle\Entity\Catalogo $cttipoidentificacion
	**/
	public function setCttipoidentificacion(\Core\AppBundle\Entity\Catalogo $cttipoidentificacion){
		$this->cttipoidentificacion = $cttipoidentificacion;
		return $this;
   }

   	/**
	* Get cttipoidentificacion
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCttipoidentificacion(){
		return $this->cttipoidentificacion;
   }


	/**
	* Set ctsexo_id
	* @param integer ctsexo_id
	* @return certificadovacuna
	**/
	public function setCtsexoId($ctsexo_id){
		 $this->ctsexo_id = $ctsexo_id;
		 return $this;
	}

	/**
	* Get ctsexo_id
	* @return integer
	**/
	public function getCtsexoId(){
		 return $this->ctsexo_id;
	}

	/**
	* Set ctsexo
	* @param \Core\AppBundle\Entity\Catalogo $ctsexo
	**/
	public function setCtsexo(\Core\AppBundle\Entity\Catalogo $ctsexo){
		 $this->ctsexo = $ctsexo;
		 return $this;
	}

	/**
	* Get ctsexo
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtsexo(){
		 return $this->ctsexo;
	}


	/**
	* Set $nombres
	* @param string $nombres
	*/ 
	public function setNombres(string $nombres)
	{
	$this->nombres = $nombres;
	}
 
  /**
   * Get $nombres
   * @return  string
   */ 
  public function getNombres()
  {
   return $this->nombres;
  }

	/**
	* Set estado
	* @param integer estado
	* @return certificadovacuna
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Get fechacreacion
	* @return datetime
	**/
	public function getFechacreacion(){
		 return $this->fechacreacion;
	}

	/**
	* Get fechamodificacion
	* @return datetime
	**/
	public function getFechamodificacion(){
		 return $this->fechamodificacion;
	}



	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return certificadovacuna
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}


	/**
	 * Set $paciente_id 
	* @param integer  $paciente_id 
	*/ 
	public function setPacienteId($paciente_id)
	{
		$this->paciente_id = $paciente_id;
	}
	
	/**
	 * Get $paciente_id
	 * @return  integer
	 */ 
	public function getPacienteId()
	{
		return $this->paciente_id;
	}
	
	/**
	 * Set $paciente
	* @param \HCUE\PacienteBundle\Entity\Paciente  $paciente
	**/
	public function setPaciente(\HCUE\PacienteBundle\Entity\Paciente $paciente){
		$this->paciente = $paciente;
	}
 
	/**
	 * Get $paciente
	 * @return \HCUE\PacienteBundle\Entity\Paciente 
	 **/
	public function getPaciente(){
		return $this->paciente;
	}


	/**
	* Get activo
	* @return integer
	**/
	public function getActivo(){
		 return $this->activo;
	}

	
	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();
		$this->activo=1;
		$this->estado=1;

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
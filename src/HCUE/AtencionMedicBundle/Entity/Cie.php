<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_catalogos.cie")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\CieRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Cie
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_catalogos.cie_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $versioncie
     * @ORM\Column(name="versioncie", type="string",length=10, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo versioncie")
     * */
    private $versioncie;

    /**
     * @var string $codigo
     * @ORM\Column(name="codigo", type="string",length=6, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo codigo")
     * */
    private $codigo;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=300, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo nombre")
     * */
    private $nombre;

    /**
     * @var integer $ctsexo_id
     * @ORM\Column(name="ctsexo_id", type="integer", nullable=true)
     * */
    private $ctsexo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctsexo_id", referencedColumnName="id")
     * */
    private $ctsexo;

    /**
     * @var integer $edadmin
     * @ORM\Column(name="edadmin", type="integer", nullable=true)
     * */
    private $edadmin;

    /**
     * @var string $unidadedadmin
     * @ORM\Column(name="unidadedadmin", type="string",length=1, nullable=true)
     * */
    private $unidadedadmin;

    /**
     * @var integer $edadmax
     * @ORM\Column(name="edadmax", type="integer", nullable=true)
     * */
    private $edadmax;

    /**
     * @var string $unidadedadmax
     * @ORM\Column(name="unidadedadmax", type="string",length=1, nullable=true)
     * */
    private $unidadedadmax;

    /**
     * @var string $trivial
     * @ORM\Column(name="trivial", type="string",length=1, nullable=true)
     * */
    private $trivial;

    /**
     * @var string $nocbd
     * @ORM\Column(name="nocbd", type="string",length=1, nullable=true)
     * */
    private $nocbd;

    /**
     * @var string $fetal
     * @ORM\Column(name="fetal", type="string",length=1, nullable=true)
     * */
    private $fetal;

    /**
     * @var integer $cie_id
     * @ORM\Column(name="cie_id", type="integer", nullable=true)
     * */
    private $cie_id;

    /**
     * @ORM\ManyToOne(targetEntity="Cie")
     * @ORM\JoinColumn(name="cie_id", referencedColumnName="id")
     * */
    private $ciepadre;

    /**
     * @var integer $cieclacapitulo_id
     * @ORM\Column(name="cieclacapitulo_id", type="integer", nullable=true)
     * */
    private $cieclacapitulo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Cieclasificacion")
     * @ORM\JoinColumn(name="cieclacapitulo_id", referencedColumnName="id")
     * */
    private $cieclacapitulo;

    /**
     * @var integer $cieclagrupo_id
     * @ORM\Column(name="cieclagrupo_id", type="integer", nullable=true)
     * */
    private $cieclagrupo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Cieclasificacion")
     * @ORM\JoinColumn(name="cieclagrupo_id", referencedColumnName="id")
     * */
    private $cieclagrupo;

    /**
     * @var string $tipoactualizacion
     * @ORM\Column(name="tipoactualizacion", type="string",length=1, nullable=true)
     * */
    private $tipoactualizacion;

    /**
     * @var string $descactualizacion
     * @ORM\Column(name="descactualizacion", type="string",length=200, nullable=true)
     * */
    private $descactualizacion;

    /**
     * @var integer $notificacionobligatoria
     * @ORM\Column(name="notificacionobligatoria", type="boolean", nullable=true)
     * */
    private $notificacionobligatoria;

    /**
     * @var integer $altocosto
     * @ORM\Column(name="altocosto", type="boolean", nullable=true)
     * */
    private $altocosto;

    /**
     * @var integer $ayudasrapidas
     * @ORM\Column(name="ayudasrapidas", type="boolean", nullable=true)
     * */
    private $ayudasrapidas;

    /**
     * @var string $etiqueta
     * @ORM\Column(name="etiqueta", type="string",length=200, nullable=true)
     * */
    private $etiqueta;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     * */
    private $estado;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacions
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     * */
    private $fechacreacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $prevencion
     * @ORM\Column(name="prevencion", type="boolean", nullable=true)
     * */
    private $prevencion;


    /**
     * @var integer $morbilidad
     * @ORM\Column(name="morbilidad", type="boolean", nullable=true)
     * */
    private $morbilidad;

    public function __construct()
    {
    }

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set versioncie
     * @param string versioncie
     * @return cie
     * */
    public function setVersioncie($versioncie)
    {
        $this->versioncie = $versioncie;
        return $this;
    }

    /**
     * Get versioncie
     * @return string
     * */
    public function getVersioncie()
    {
        return $this->versioncie;
    }

    /**
     * Set codigo
     * @param string codigo
     * @return cie
     * */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * Get codigo
     * @return string
     * */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return cie
     * */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     * */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ctsexo_id
     * @param integer ctsexo_id
     * @return cie
     * */
    public function setCtsexoId($ctsexo_id)
    {
        $this->ctsexo_id = $ctsexo_id;
        return $this;
    }

    /**
     * Get ctsexo_id
     * @return integer
     * */
    public function getCtsexoId()
    {
        return $this->ctsexo_id;
    }

    /**
     * Set ctsexo
     * @param \Core\AppBundle\Entity\Catalogo $ctsexo
     * */
    public function setCtsexo(\Core\AppBundle\Entity\Catalogo $ctsexo = null)
    {
        $this->ctsexo = $ctsexo;
        return $this;
    }

    /**
     * Get ctsexo
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtsexo()
    {
        return $this->ctsexo;
    }

    /**
     * Set edadmin
     * @param integer edadmin
     * @return cie
     * */
    public function setEdadmin($edadmin)
    {
        $this->edadmin = $edadmin;
        return $this;
    }

    /**
     * Get edadmin
     * @return integer
     * */
    public function getEdadmin()
    {
        return $this->edadmin;
    }

    /**
     * Set unidadedadmin
     * @param string unidadedadmin
     * @return cie
     * */
    public function setUnidadedadmin($unidadedadmin)
    {
        $this->unidadedadmin = $unidadedadmin;
        return $this;
    }

    /**
     * Get unidadedadmin
     * @return string
     * */
    public function getUnidadedadmin()
    {
        return $this->unidadedadmin;
    }

    /**
     * Set edadmax
     * @param integer edadmax
     * @return cie
     * */
    public function setEdadmax($edadmax)
    {
        $this->edadmax = $edadmax;
        return $this;
    }

    /**
     * Get edadmax
     * @return integer
     * */
    public function getEdadmax()
    {
        return $this->edadmax;
    }

    /**
     * Set unidadedadmax
     * @param string unidadedadmax
     * @return cie
     * */
    public function setUnidadedadmax($unidadedadmax)
    {
        $this->unidadedadmax = $unidadedadmax;
        return $this;
    }

    /**
     * Get unidadedadmax
     * @return string
     * */
    public function getUnidadedadmax()
    {
        return $this->unidadedadmax;
    }

    /**
     * Set trivial
     * @param string trivial
     * @return cie
     * */
    public function setTrivial($trivial)
    {
        $this->trivial = $trivial;
        return $this;
    }

    /**
     * Get trivial
     * @return string
     * */
    public function getTrivial()
    {
        return $this->trivial;
    }

    /**
     * Set nocbd
     * @param string nocbd
     * @return cie
     * */
    public function setNocbd($nocbd)
    {
        $this->nocbd = $nocbd;
        return $this;
    }

    /**
     * Get nocbd
     * @return string
     * */
    public function getNocbd()
    {
        return $this->nocbd;
    }

    /**
     * Set fetal
     * @param string fetal
     * @return cie
     * */
    public function setFetal($fetal)
    {
        $this->fetal = $fetal;
        return $this;
    }

    /**
     * Get fetal
     * @return string
     * */
    public function getFetal()
    {
        return $this->fetal;
    }

    /**
     * Set cie_id
     * @param integer cie_id
     * @return cie
     * */
    public function setCieId($cie_id)
    {
        $this->cie_id = $cie_id;
        return $this;
    }

    /**
     * Get cie_id
     * @return integer
     * */
    public function getCieId()
    {
        return $this->cie_id;
    }

    /**
     * Set cie
     * @param \HCUE\AtencionBundle\Entity\Cie $ciepadre
     * */
    public function setCiepadre(\HCUE\AtencionMedicBundle\Entity\Cie $ciepadre)
    {
        $this->ciepadre = $ciepadre;
        return $this;
    }

    /**
     * Get cie
     * @return \HCUE\AtencionMedicBundle\Entity\Cie
     * */
    public function getCiepadre()
    {
        return $this->ciepadre;
    }

    /**
     * Set cieclacapitulo_id
     * @param integer cieclacapitulo_id
     * @return cie
     * */
    public function setCieclacapituloId($cieclacapitulo_id)
    {
        $this->cieclacapitulo_id = $cieclacapitulo_id;
        return $this;
    }

    /**
     * Get cieclacapitulo_id
     * @return integer
     * */
    public function getCieclacapituloId()
    {
        return $this->cieclacapitulo_id;
    }

    /**
     * Set cieclacapitulo
     * @param \HCUE\AtencionMedicBundle\Entity\Cieclasificacion $cieclacapitulo
     * */
    public function setCieclacapitulo(\HCUE\AtencionMedicBundle\Entity\Cieclasificacion $cieclacapitulo)
    {
        $this->cieclacapitulo = $cieclacapitulo;
        return $this;
    }

    /**
     * Get cieclacapitulo
     * @return \HCUE\AtencionMedicBundle\Entity\Cieclacapitulo
     * */
    public function getCieclacapitulo()
    {
        return $this->cieclacapitulo;
    }

    /**
     * Set cieclagrupo_id
     * @param integer cieclagrupo_id
     * @return cie
     * */
    public function setCieclagrupoId($cieclagrupo_id)
    {
        $this->cieclagrupo_id = $cieclagrupo_id;
        return $this;
    }

    /**
     * Get cieclagrupo_id
     * @return integer
     * */
    public function getCieclagrupoId()
    {
        return $this->cieclagrupo_id;
    }

    /**
     * Set cieclagrupo
     * @param \HCUE\AtencionMedicBundle\Entity\Cieclasificacion $cieclagrupo
     * */
    public function setCieclagrupo(\HCUE\AtencionMedicBundle\Entity\Cieclasificacion $cieclagrupo)
    {
        $this->cieclagrupo = $cieclagrupo;
        return $this;
    }

    /**
     * Get cieclagrupo
     * @return \HCUE\AtencionMedicBundle\Entity\Cieclagrupo
     * */
    public function getCieclagrupo()
    {
        return $this->cieclagrupo;
    }

    /**
     * Set tipoactualizacion
     * @param string tipoactualizacion
     * @return cie
     * */
    public function setTipoactualizacion($tipoactualizacion)
    {
        $this->tipoactualizacion = $tipoactualizacion;
        return $this;
    }

    /**
     * Get tipoactualizacion
     * @return string
     * */
    public function getTipoactualizacion()
    {
        return $this->tipoactualizacion;
    }

    /**
     * Set descactualizacion
     * @param string descactualizacion
     * @return cie
     * */
    public function setDescactualizacion($descactualizacion)
    {
        $this->descactualizacion = $descactualizacion;
        return $this;
    }

    /**
     * Get descactualizacion
     * @return string
     * */
    public function getDescactualizacion()
    {
        return $this->descactualizacion;
    }

    /**
     * Set notificacionobligatoria
     * @param integer notificacionobligatoria
     * @return cie
     * */
    public function setNotificacionobligatoria($notificacionobligatoria)
    {
        $this->notificacionobligatoria = $notificacionobligatoria;
        return $this;
    }

    /**
     * Get notificacionobligatoria
     * @return integer
     * */
    public function getNotificacionobligatoria()
    {
        return $this->notificacionobligatoria;
    }

    /**
     * Set altocosto
     * @param integer altocosto
     * @return cie
     * */
    public function setAltocosto($altocosto)
    {
        $this->altocosto = $altocosto;
        return $this;
    }

    /**
     * Get altocosto
     * @return integer
     * */
    public function getAltocosto()
    {
        return $this->altocosto;
    }

    /**
     * Set ayudasrapidas
     * @param integer ayudasrapidas
     * @return cie
     * */
    public function setAyudasrapidas($ayudasrapidas)
    {
        $this->ayudasrapidas = $ayudasrapidas;
        return $this;
    }

    /**
     * Get ayudasrapidas
     * @return integer
     * */
    public function getAyudasrapidas()
    {
        return $this->ayudasrapidas;
    }

    /**
     * Set etiqueta
     * @param string etiqueta
     * @return cie
     * */
    public function setEtiqueta($etiqueta)
    {
        $this->etiqueta = $etiqueta;
        return $this;
    }

    /**
     * Get etiqueta
     * @return string
     * */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }

    /**
     * Set estado
     * @param integer estado
     * @return cie
     * */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return cie
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return cie
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }


    /**
     * Set prevencion
     * @param integer prevencion
     * @return cie
     * */
    public function setPrevencion($prevencion)
    {
        $this->prevencion = $prevencion;
        return $this;
    }

    /**
     * Get prevencion
     * @return integer
     * */
    public function getPrevencion()
    {
        return $this->prevencion;
    }

    /**
     * Set morbilidad
     * @param integer morbilidad
     * @return cie
     * */
    public function setMorbilidad($morbilidad)
    {
        $this->morbilidad = $morbilidad;
        return $this;
    }

    /**
     * Get morbilidad
     * @return integer
     * */
    public function getMorbilidad()
    {
        return $this->morbilidad;
    }


    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __toString()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

<?php

/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.diagnosticoatencion")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\DiagnosticoatencionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Diagnosticoatencion {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.diagnosticoatencion_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $cie_id
     * @ORM\Column(name="cie_id", type="integer", nullable=false)
     * */
    private $cie_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Cie")
     * @ORM\JoinColumn(name="cie_id", referencedColumnName="id")
     * */
    private $cie;

    /**
     * @var integer $atencionmedica_id
     * @ORM\Column(name="atencionmedica_id", type="integer", nullable=false)
     * */
    private $atencionmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     * */
    private $atencionmedica;

    /**
     * @var integer $ctcondiciondiagnostico_id
     * @ORM\Column(name="ctcondiciondiagnostico_id", type="integer", nullable=true)
     * */
    private $ctcondiciondiagnostico_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctcondiciondiagnostico_id", referencedColumnName="id")
     * */
    private $ctcondiciondiagnostico;

    /**
     * @var integer $ctcronologiadiagnostico_id
     * @ORM\Column(name="ctcronologiadiagnostico_id", type="integer", nullable=true)
     * */
    private $ctcronologiadiagnostico_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctcronologiadiagnostico_id", referencedColumnName="id")
     * */
    private $ctcronologiadiagnostico;

    /**
     * @var integer $cttipodiagnostico_id
     * @ORM\Column(name="cttipodiagnostico_id", type="integer", nullable=true)
     * */
    private $cttipodiagnostico_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipodiagnostico_id", referencedColumnName="id")
     * */
    private $cttipodiagnostico;

    /**
     * @var string $observacion
     * @ORM\Column(name="observacion", type="string",length=4000, nullable=true)
     * */
    private $observacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     * */
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $zona_id
     * @ORM\Column(name="zona_id", type="integer", nullable=true)
     * */
    private $zona_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Zona")
     * @ORM\JoinColumn(name="zona_id", referencedColumnName="id")
     * */
    private $zona;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cie_id
     * @param integer cie_id
     * @return diagnosticoatencion
     * */
    public function setCieId($cie_id) {
        $this->cie_id = $cie_id;
        return $this;
    }

    /**
     * Get cie_id
     * @return integer
     * */
    public function getCieId() {
        return $this->cie_id;
    }

    /**
     * Set cie
     * @param \HCUE\AtencionMedicBundle\Entity\Cie $cie
     * */
    public function setCie(\HCUE\AtencionMedicBundle\Entity\Cie $cie) {
        $this->cie = $cie;
        return $this;
    }

    /**
     * Get cie
     * @return \HCUE\AtencionMedicBundle\Entity\Cie
     * */
    public function getCie() {
        return $this->cie;
    }

    /**
     * Get ciedescripcion
     * @return string
     * */
    public function getCiedescripcion() {
        return $this->cie->getCodigoNombre();
    }

    /**
     * Set atencionmedica_id
     * @param integer atencionmedica_id
     * @return diagnosticoatencion
     * */
    public function setAtencionmedicaId($atencionmedica_id) {
        $this->atencionmedica_id = $atencionmedica_id;
        return $this;
    }

    /**
     * Get atencionmedica_id
     * @return integer
     * */
    public function getAtencionmedicaId() {
        return $this->atencionmedica_id;
    }

    /**
     * Set atencionmedica
     * @param \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
     * */
    public function setAtencionmedica(\HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica) {
        $this->atencionmedica = $atencionmedica;
        return $this;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     * */
    public function getAtencionmedica() {
        return $this->atencionmedica;
    }

    /**
     * Set ctcondiciondiagnostico_id
     * @param integer ctcondiciondiagnostico_id
     * @return diagnosticoatencion
     * */
    public function setCtcondiciondiagnosticoId($ctcondiciondiagnostico_id) {
        $this->ctcondiciondiagnostico_id = $ctcondiciondiagnostico_id;
        return $this;
    }

    /**
     * Get ctcondiciondiagnostico_id
     * @return integer
     * */
    public function getCtcondiciondiagnosticoId() {
        return $this->ctcondiciondiagnostico_id;
    }

    /**
     * Set ctcondiciondiagnostico
     * @param \Core\AppBundle\Entity\Catalogo $ctcondiciondiagnostico
     * */
    public function setCtcondiciondiagnostico(\Core\AppBundle\Entity\Catalogo $ctcondiciondiagnostico) {
        $this->ctcondiciondiagnostico = $ctcondiciondiagnostico;
        return $this;
    }

    /**
     * Get ctcondiciondiagnostico
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtcondiciondiagnostico() {
        return $this->ctcondiciondiagnostico;
    }

    /**
     * Set ctcronologiadiagnostico_id
     * @param integer ctcronologiadiagnostico_id
     * @return diagnosticoatencion
     * */
    public function setCtcronologiadiagnosticoId($ctcronologiadiagnostico_id) {
        $this->ctcronologiadiagnostico_id = $ctcronologiadiagnostico_id;
        return $this;
    }

    /**
     * Get ctcronologiadiagnostico_id
     * @return integer
     * */
    public function getCtcronologiadiagnosticoId() {
        return $this->ctcronologiadiagnostico_id;
    }

    /**
     * Set ctcronologiadiagnostico
     * @param \Core\AppBundle\Entity\Catalogo $ctcronologiadiagnostico
     * */
    public function setCtcronologiadiagnostico(\Core\AppBundle\Entity\Catalogo $ctcronologiadiagnostico) {
        $this->ctcronologiadiagnostico = $ctcronologiadiagnostico;
        return $this;
    }

    /**
     * Get ctcronologiadiagnostico
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCtcronologiadiagnostico() {
        return $this->ctcronologiadiagnostico;
    }

    /**
     * Set cttipodiagnostico_id
     * @param integer cttipodiagnostico_id
     * @return diagnosticoatencion
     * */
    public function setCttipodiagnosticoId($cttipodiagnostico_id) {
        $this->cttipodiagnostico_id = $cttipodiagnostico_id;
        return $this;
    }

    /**
     * Get cttipodiagnostico_id
     * @return integer
     * */
    public function getCttipodiagnosticoId() {
        return $this->cttipodiagnostico_id;
    }

    /**
     * Set cttipodiagnostico
     * @param \Core\AppBundle\Entity\Catalogo $cttipodiagnostico
     * */
    public function setCttipodiagnostico(\Core\AppBundle\Entity\Catalogo $cttipodiagnostico = null) {
        $this->cttipodiagnostico = $cttipodiagnostico;
        return $this;
    }

    /**
     * Get cttipodiagnostico
     * @return \Core\AppBundle\Entity\Catalogo
     * */
    public function getCttipodiagnostico() {
        return $this->cttipodiagnostico;
    }

    /**
     * Set observacion
     * @param string observacion
     * @return diagnosticoatencion
     * */
    public function setObservacion($observacion) {
        $this->observacion = $observacion;
        return $this;
    }

    /**
     * Get observacion
     * @return string
     * */
    public function getObservacion() {
        return $this->observacion;
    }

    /**
     * Set activo
     * @param integer activo
     * @return diagnosticoatencion
     * */
    public function setActivo($activo) {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return diagnosticoatencion
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id) {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId() {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return diagnosticoatencion
     * */
    public function setUsuariocreacionId($usuariocreacion_id) {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId() {
        return $this->usuariocreacion_id;
    }

    /**
     * Set zona
     * @param \Core\AppBundle\Entity\Zona $zona
     * */
    public function setZona(\Core\AppBundle\Entity\Zona $zona) {
        $this->zona = $zona;
        return $this;
    }

    /**
     * Get zona
     * @return \Core\AppBundle\Entity\Zona
     * */
    public function getZona() {
        return $this->zona;
    }

    /**
     * Set zona_id
     * @param integer zona_id
     * @return atencionmedica
     * */
    public function setZonaId($zona_id) {
        $this->zona_id = $zona_id;
        return $this;
    }

    /**
     * Get zona_id
     * @return integer
     * */
    public function getZonaId() {
        return $this->zona_id;
    }

    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return paciente
     * */
    public function setFechacreacion() {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     * */
    public function getFechacreacion() {
        return $this->fechacreacion;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist() {
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate() {
        $this->fechamodificacion = new \DateTime();
    }

    public function __toString() {
        
    }

    public function __clone() {
        $this->id = null;
    }

    public function copy() {
        return clone $this;
    }

}

?>
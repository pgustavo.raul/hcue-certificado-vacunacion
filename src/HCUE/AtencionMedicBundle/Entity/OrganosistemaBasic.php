<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_amed.organosistemarevision")
 * @ORM\Entity()
 */
class OrganosistemaBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="ctorgasistema_id", referencedColumnName="id")
     **/
    private $ctorgasistema;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="ctevidenciapatologica_id", referencedColumnName="id")
     **/
    private $ctevidenciapatologica;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     **/
    private $descripcion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     **/
    public function getAtencionmedica(){
        return $this->atencionmedica;
    }

    /**
     * Get ctorgasistema
     * @return \Core\AppBundle\Entity\CatalogoBasic
     **/
    public function getCtorgasistema(){
        return $this->ctorgasistema;
    }

    /**
     * Get ctevidenciapatologica
     * @return \Core\AppBundle\Entity\CatalogoBasic
     **/
    public function getCtevidenciapatologica(){
        return $this->ctevidenciapatologica;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion(){
        return $this->descripcion;
    }

}
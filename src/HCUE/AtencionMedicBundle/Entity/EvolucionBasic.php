<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_amed.evolucion")
 * @ORM\Entity()
 */
class EvolucionBasic {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     * */
    private $atencionmedica;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     * */
    private $descripcion;

    /**
     * Get id
     * @return integer
     * */
    public function getId() {
        return $this->id;
    }


    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     * */
    public function getAtencionmedica() {
        return $this->atencionmedica;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion() {
        return $this->descripcion;
    }

}
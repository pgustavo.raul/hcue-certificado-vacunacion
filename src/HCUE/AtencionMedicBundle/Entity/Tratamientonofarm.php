<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
* @ORM\Table(name="hcue_amed.tratamientonofarm")
* @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\TratamientonofarmRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Tratamientonofarm{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.tratamientonofarm_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $atencionmedica_id
	* @ORM\Column(name="atencionmedica_id", type="integer", nullable=false)
	**/
	private $atencionmedica_id;

	/**
	* @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
	* @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
	**/
	private $atencionmedica;

	/**
	* @var integer $ctcategoria_id
	* @ORM\Column(name="ctcategoria_id", type="integer", nullable=false)
	**/
	private $ctcategoria_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctcategoria_id", referencedColumnName="id")
	**/
	private $ctcategoria;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=4000, nullable=true)
	**/
	private $descripcion;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=false)
	**/
	private $activo;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Tratamientonofarmdet", mappedBy="tratamientonofarm", cascade={"persist", "remove"})
     */
    private $tratamientonofarmdets;



    public function __construct() {
        $this->tratamientonofarmdets = new ArrayCollection();
    }

    /**
     * get Tratamientonofarmdets
     * \Doctrine\Common\Collections\ArrayCollection
     * */
    public function getTratamientonofarmdets() {
        return $this->tratamientonofarmdets;
    }


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set atencionmedica_id
	* @param integer atencionmedica_id
	* @return tratamientonofarm
	**/
	public function setAtencionmedicaId($atencionmedica_id){
		 $this->atencionmedica_id = $atencionmedica_id;
		 return $this;
	}

	/**
	* Get atencionmedica_id
	* @return integer
	**/
	public function getAtencionmedicaId(){
		 return $this->atencionmedica_id;
	}

	/**
	* Set atencionmedica
	* @param \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
	**/
	public function setAtencionmedica(\HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica){
		 $this->atencionmedica = $atencionmedica;
		 return $this;
	}

	/**
	* Get atencionmedica
	* @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
	**/
	public function getAtencionmedica(){
		 return $this->atencionmedica;
	}

	/**
	* Set ctcategoria_id
	* @param integer ctcategoria_id
	* @return tratamientonofarm
	**/
	public function setCtcategoriaId($ctcategoria_id){
		 $this->ctcategoria_id = $ctcategoria_id;
		 return $this;
	}

	/**
	* Get ctcategoria_id
	* @return integer
	**/
	public function getCtcategoriaId(){
		 return $this->ctcategoria_id;
	}

	/**
	* Set ctcategoria
	* @param \Core\AppBundle\Entity\Catalogo $ctcategoria
	**/
	public function setCtcategoria(\Core\AppBundle\Entity\Catalogo $ctcategoria){
		 $this->ctcategoria = $ctcategoria;
		 return $this;
	}

	/**
	* Get ctcategoria
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtcategoria(){
		 return $this->ctcategoria;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return tratamientonofarm
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return tratamientonofarm
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return tratamientonofarm
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

    /**
     * Get fechacreacion
     * @return \Datetime
     **/
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

	/**
	* Set activo
	* @param integer activo
	* @return tratamientonofarm
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}



	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->activo=1;
$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
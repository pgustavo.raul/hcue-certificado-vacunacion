<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Lenin VAllejos
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.tratamientonofarmdet")
* @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\TratamientonofarmdetRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Tratamientonofarmdet{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.tratamientonofarmdet_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $tratamientonofarm_id
	* @ORM\Column(name="tratamientonofarm_id", type="integer", nullable=false)
	**/
	private $tratamientonofarm_id;

	/**
	* @ORM\ManyToOne(targetEntity="Tratamientonofarm")
	* @ORM\JoinColumn(name="tratamientonofarm_id", referencedColumnName="id")
	**/
	private $tratamientonofarm;

	/**
	* @var integer $ctindicaciones_id
	* @ORM\Column(name="ctindicaciones_id", type="integer", nullable=false)
	**/
	private $ctindicaciones_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctindicaciones_id", referencedColumnName="id")
	**/
	private $ctindicaciones;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=false)
	**/
	private $activo;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set tratamientonofarm_id
	* @param integer tratamientonofarm_id
	* @return tratamientonofarmdet
	**/
	public function setTratamientonofarmId($tratamientonofarm_id){
		 $this->tratamientonofarm_id = $tratamientonofarm_id;
		 return $this;
	}

	/**
	* Get tratamientonofarm_id
	* @return integer
	**/
	public function getTratamientonofarmId(){
		 return $this->tratamientonofarm_id;
	}

	/**
	* Set tratamientonofarm
	* @param \HCUE\AtencionMedicBundle\Entity\Tratamientonofarm $tratamientonofarm
	**/
	public function setTratamientonofarm(\HCUE\AtencionMedicBundle\Entity\Tratamientonofarm $tratamientonofarm){
		 $this->tratamientonofarm = $tratamientonofarm;
		 return $this;
	}

	/**
	* Get tratamientonofarm
	* @return \HCUE\AtencionMedicBundle\Entity\Tratamientonofarm
	**/
	public function getTratamientonofarm(){
		 return $this->tratamientonofarm;
	}

	/**
	* Set ctindicaciones_id
	* @param integer ctindicaciones_id
	* @return tratamientonofarmdet
	**/
	public function setCtindicacionesId($ctindicaciones_id){
		 $this->ctindicaciones_id = $ctindicaciones_id;
		 return $this;
	}

	/**
	* Get ctindicaciones_id
	* @return integer
	**/
	public function getCtindicacionesId(){
		 return $this->ctindicaciones_id;
	}

	/**
	* Set ctindicaciones
	* @param \Core\AppBundle\Entity\Catalogo $ctindicaciones
	**/
	public function setCtindicaciones(\Core\AppBundle\Entity\Catalogo $ctindicaciones){
		 $this->ctindicaciones = $ctindicaciones;
		 return $this;
	}

	/**
	* Get ctindicaciones
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtindicaciones(){
		 return $this->ctindicaciones;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return tratamientonofarmdet
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return tratamientonofarmdet
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set activo
	* @param integer activo
	* @return tratamientonofarmdet
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->activo=1;
$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
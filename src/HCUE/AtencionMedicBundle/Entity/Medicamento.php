<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.medicamento")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\MedicamentoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Medicamento
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.medicamento_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $codigoatc_id
     * @ORM\Column(name="codigoatc_id", type="integer", nullable=false)
     * */
    private $codigoatc_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Codigoatc")
     * @ORM\JoinColumn(name="codigoatc_id", referencedColumnName="id")
     * */
    private $codigoatc;

    /**
     * @var integer $formfarmacnmb_id
     * @ORM\Column(name="formfarmacnmb_id", type="integer", nullable=false)
     * */
    private $formfarmacnmb_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="formfarmacnmb_id", referencedColumnName="id")
     * */
    private $formfarmacnmb;

    /**
     * @var integer $formfarma_id
     * @ORM\Column(name="formfarma_id", type="integer", nullable=false)
     * */
    private $formfarma_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="formfarma_id", referencedColumnName="id")
     * */
    private $formfarma;

    /**
     * @var integer $presentacion_id
     * @ORM\Column(name="presentacion_id", type="integer", nullable=false)
     * */
    private $presentacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="presentacion_id", referencedColumnName="id")
     * */
    private $presentacion;

    /**
     * @var string $codigomedicamento
     * @ORM\Column(name="codigomedicamento", type="string",length=64, nullable=true)
     * */
    private $codigomedicamento;

    /**
     * @var string $codigomedicamentocnmb
     * @ORM\Column(name="codigomedicamentocnmb", type="string",length=64, nullable=true)
     * */
    private $codigomedicamentocnmb;

    /**
     * @var string $estado
     * @ORM\Column(name="estado", type="boolean",length=30, nullable=true)
     * */
    private $estado;

    /**
     * @var integer $concentracion_id
     * @ORM\Column(name="concentracion_id", type="integer", nullable=true)
     * */
    private $concentracion_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="concentracion_id", referencedColumnName="id")
     * */
    private $concentracion;

    /**
     * @var integer $estupefaciente
     * @ORM\Column(name="estupefaciente", type="boolean", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estupefaciente")
     * */
    private $estupefaciente;

    /**
     * @var integer $psicotropico
     * @ORM\Column(name="psicotropico", type="boolean", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo psicotropico")
     * */
    private $psicotropico;

    /**
     * @var integer $niveluno
     * @ORM\Column(name="niveluno", type="boolean", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo niveluno")
     * */
    private $niveluno;

    /**
     * @var integer $niveldos
     * @ORM\Column(name="niveldos", type="boolean", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo niveldos")
     * */
    private $niveldos;

    /**
     * @var integer $niveltres
     * @ORM\Column(name="niveltres", type="boolean", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo niveltres")
     * */
    private $niveltres;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=256, nullable=true)
     * */
    private $nombre;

    /**
     * @var string $sinonimo
     * @ORM\Column(name="sinonimo", type="string",length=128, nullable=true)
     * */
    private $sinonimo;

    /**
     * @var string $precaucionesgenerales
     * @ORM\Column(name="precaucionesgenerales", type="string",length=2000, nullable=true)
     * */
    private $precaucionesgenerales;

    /**
     * @var integer $advertenciaembarazo
     * @ORM\Column(name="advertenciaembarazo", type="boolean", nullable=true)
     * */
    private $advertenciaembarazo;

    /**
     * @var integer $alertariesgoembarazo_id
     * @ORM\Column(name="alertariesgoembarazo_id", type="integer", nullable=true)
     * */
    private $alertariesgoembarazo_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="alertariesgoembarazo_id", referencedColumnName="id")
     * */
    private $alertariesgoembarazo;

    /**
     * @var integer $alertariesgolactancia_id
     * @ORM\Column(name="alertariesgolactancia_id", type="integer", nullable=true)
     * */
    private $alertariesgolactancia_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="alertariesgolactancia_id", referencedColumnName="id")
     * */
    private $alertariesgolactancia;

    /**
     * @var integer $advertenciaedad
     * @ORM\Column(name="advertenciaedad", type="boolean", nullable=true)
     * */
    private $advertenciaedad;

    /**
     * @var integer $edadminima
     * @ORM\Column(name="edadminima", type="integer", nullable=true)
     * */
    private $edadminima;

    /**
     * @var integer $edadmaxima
     * @ORM\Column(name="edadmaxima", type="integer", nullable=true)
     * */
    private $edadmaxima;

    /**
     * @var string $reaccionesadversas
     * @ORM\Column(name="reaccionesadversas", type="string",length=2000, nullable=true)
     * */
    private $reaccionesadversas;

    /**
     * @var string $condicionesalmacenamiento
     * @ORM\Column(name="condicionesalmacenamiento", type="string",length=2000, nullable=true)
     * */
    private $condicionesalmacenamiento;

    /**
     * @var string $interacciones
     * @ORM\Column(name="interacciones", type="string",length=2000, nullable=true)
     * */
    private $interacciones;

    /**
     * @var string $infoadicional
     * @ORM\Column(name="infoadicional", type="string",length=2000, nullable=true)
     * */
    private $infoadicional;

    /**
     * @var decimal $preciounitario
     * @ORM\Column(name="preciounitario", type="decimal", nullable=true)
     * */
    private $preciounitario;

    /**
     * @var integer $externalizable
     * @ORM\Column(name="externalizable", type="boolean", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo externalizable")
     * */
    private $externalizable;

    /**
     * @var integer $unidadadmin_id
     * @ORM\Column(name="unidadadmin_id", type="integer", nullable=true)
     * */
    private $unidadadmin_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento")
     * @ORM\JoinColumn(name="unidadadmin_id", referencedColumnName="id")
     * */
    private $unidadadmin;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoatc_id
     * @param integer codigoatc_id
     * @return medicamento
     * */
    public function setCodigoatcId($codigoatc_id)
    {
        $this->codigoatc_id = $codigoatc_id;
        return $this;
    }

    /**
     * Get codigoatc_id
     * @return integer
     * */
    public function getCodigoatcId()
    {
        return $this->codigoatc_id;
    }

    /**
     * Set codigoatc
     * @param \HCUE\AtencionMedicBundle\Entity\Codigoatc $codigoatc
     * */
    public function setCodigoatc(\HCUE\AtencionMedicBundle\Entity\Codigoatc $codigoatc)
    {
        $this->codigoatc = $codigoatc;
        return $this;
    }

    /**
     * Get codigoatc
     * @return \HCUE\AtencionMedicBundle\Entity\Codigoatc
     * */
    public function getCodigoatc()
    {
        return $this->codigoatc;
    }

    /**
     * Set formfarmacnmb_id
     * @param integer formfarmacnmb_id
     * @return medicamento
     * */
    public function setFormfarmacnmbId($formfarmacnmb_id)
    {
        $this->formfarmacnmb_id = $formfarmacnmb_id;
        return $this;
    }

    /**
     * Get formfarmacnmb_id
     * @return integer
     * */
    public function getFormfarmacnmbId()
    {
        return $this->formfarmacnmb_id;
    }

    /**
     * Set formfarmacnmb
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $formfarmacnmb
     * */
    public function setFormfarmacnmb(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $formfarmacnmb)
    {
        $this->formfarmacnmb = $formfarmacnmb;
        return $this;
    }

    /**
     * Get formfarmacnmb
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getFormfarmacnmb()
    {
        return $this->formfarmacnmb;
    }

    /**
     * Set formfarma_id
     * @param integer formfarma_id
     * @return medicamento
     * */
    public function setFormfarmaId($formfarma_id)
    {
        $this->formfarma_id = $formfarma_id;
        return $this;
    }

    /**
     * Get formfarma_id
     * @return integer
     * */
    public function getFormfarmaId()
    {
        return $this->formfarma_id;
    }

    /**
     * Set formfarma
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $formfarma
     * */
    public function setFormfarma(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $formfarma)
    {
        $this->formfarma = $formfarma;
        return $this;
    }

    /**
     * Get formfarma
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getFormfarma()
    {
        return $this->formfarma;
    }

    /**
     * Set presentacion_id
     * @param integer presentacion_id
     * @return medicamento
     * */
    public function setPresentacionId($presentacion_id)
    {
        $this->presentacion_id = $presentacion_id;
        return $this;
    }

    /**
     * Get presentacion_id
     * @return integer
     * */
    public function getPresentacionId()
    {
        return $this->presentacion_id;
    }

    /**
     * Set presentacion
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $presentacion
     * */
    public function setPresentacion(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $presentacion)
    {
        $this->presentacion = $presentacion;
        return $this;
    }

    /**
     * Get presentacion
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getPresentacion()
    {
        return $this->presentacion;
    }

    /**
     * Set codigomedicamento
     * @param string codigomedicamento
     * @return medicamento
     * */
    public function setCodigomedicamento($codigomedicamento)
    {
        $this->codigomedicamento = $codigomedicamento;
        return $this;
    }

    /**
     * Get codigomedicamento
     * @return string
     * */
    public function getCodigomedicamento()
    {
        return $this->codigomedicamento;
    }

    /**
     * Set codigomedicamentocnmb
     * @param string codigomedicamentocnmb
     * @return medicamento
     * */
    public function setCodigomedicamentocnmb($codigomedicamentocnmb)
    {
        $this->codigomedicamentocnmb = $codigomedicamentocnmb;
        return $this;
    }

    /**
     * Get codigomedicamentocnmb
     * @return string
     * */
    public function getCodigomedicamentocnmb()
    {
        return $this->codigomedicamentocnmb;
    }

    /**
     * Set estado
     * @param string estado
     * @return medicamento
     * */
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return string
     * */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set concentracion_id
     * @param integer concentracion_id
     * @return medicamento
     * */
    public function setConcentracionId($concentracion_id)
    {
        $this->concentracion_id = $concentracion_id;
        return $this;
    }

    /**
     * Get concentracion_id
     * @return integer
     * */
    public function getConcentracionId()
    {
        return $this->concentracion_id;
    }

    /**
     * Set concentracion
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $concentracion
     * */
    public function setConcentracion(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $concentracion)
    {
        $this->concentracion = $concentracion;
        return $this;
    }

    /**
     * Get concentracion
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getConcentracion()
    {
        return $this->concentracion;
    }

    /**
     * Set estupefaciente
     * @param integer estupefaciente
     * @return medicamento
     * */
    public function setEstupefaciente($estupefaciente)
    {
        $this->estupefaciente = $estupefaciente;
        return $this;
    }

    /**
     * Get estupefaciente
     * @return integer
     * */
    public function getEstupefaciente()
    {
        return $this->estupefaciente;
    }

    /**
     * Set psicotropico
     * @param integer psicotropico
     * @return medicamento
     * */
    public function setPsicotropico($psicotropico)
    {
        $this->psicotropico = $psicotropico;
        return $this;
    }

    /**
     * Get psicotropico
     * @return integer
     * */
    public function getPsicotropico()
    {
        return $this->psicotropico;
    }

    /**
     * Set niveluno
     * @param integer niveluno
     * @return medicamento
     * */
    public function setNiveluno($niveluno)
    {
        $this->niveluno = $niveluno;
        return $this;
    }

    /**
     * Get niveluno
     * @return integer
     * */
    public function getNiveluno()
    {
        return $this->niveluno;
    }

    /**
     * Set niveldos
     * @param integer niveldos
     * @return medicamento
     * */
    public function setNiveldos($niveldos)
    {
        $this->niveldos = $niveldos;
        return $this;
    }

    /**
     * Get niveldos
     * @return integer
     * */
    public function getNiveldos()
    {
        return $this->niveldos;
    }

    /**
     * Set niveltres
     * @param integer niveltres
     * @return medicamento
     * */
    public function setNiveltres($niveltres)
    {
        $this->niveltres = $niveltres;
        return $this;
    }

    /**
     * Get niveltres
     * @return integer
     * */
    public function getNiveltres()
    {
        return $this->niveltres;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return medicamento
     * */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     * */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set sinonimo
     * @param string sinonimo
     * @return medicamento
     * */
    public function setSinonimo($sinonimo)
    {
        $this->sinonimo = $sinonimo;
        return $this;
    }

    /**
     * Get sinonimo
     * @return string
     * */
    public function getSinonimo()
    {
        return $this->sinonimo;
    }

    /**
     * Set precaucionesgenerales
     * @param string precaucionesgenerales
     * @return medicamento
     * */
    public function setPrecaucionesgenerales($precaucionesgenerales)
    {
        $this->precaucionesgenerales = $precaucionesgenerales;
        return $this;
    }

    /**
     * Get precaucionesgenerales
     * @return string
     * */
    public function getPrecaucionesgenerales()
    {
        return $this->precaucionesgenerales;
    }

    /**
     * Set advertenciaembarazo
     * @param integer advertenciaembarazo
     * @return medicamento
     * */
    public function setAdvertenciaembarazo($advertenciaembarazo)
    {
        $this->advertenciaembarazo = $advertenciaembarazo;
        return $this;
    }

    /**
     * Get advertenciaembarazo
     * @return integer
     * */
    public function getAdvertenciaembarazo()
    {
        return $this->advertenciaembarazo;
    }

    /**
     * Set alertariesgoembarazo_id
     * @param integer alertariesgoembarazo_id
     * @return medicamento
     * */
    public function setAlertariesgoembarazoId($alertariesgoembarazo_id)
    {
        $this->alertariesgoembarazo_id = $alertariesgoembarazo_id;
        return $this;
    }

    /**
     * Get alertariesgoembarazo_id
     * @return integer
     * */
    public function getAlertariesgoembarazoId()
    {
        return $this->alertariesgoembarazo_id;
    }

    /**
     * Set alertariesgoembarazo
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $alertariesgoembarazo
     * */
    public function setAlertariesgoembarazo(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $alertariesgoembarazo = null)
    {
        $this->alertariesgoembarazo = $alertariesgoembarazo;
        return $this;
    }

    /**
     * Get alertariesgoembarazo
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getAlertariesgoembarazo()
    {
        return $this->alertariesgoembarazo;
    }

    /**
     * Set alertariesgolactancia_id
     * @param integer alertariesgolactancia_id
     * @return medicamento
     * */
    public function setAlertariesgolactanciaId($alertariesgolactancia_id)
    {
        $this->alertariesgolactancia_id = $alertariesgolactancia_id;
        return $this;
    }

    /**
     * Get alertariesgolactancia_id
     * @return integer
     * */
    public function getAlertariesgolactanciaId()
    {
        return $this->alertariesgolactancia_id;
    }

    /**
     * Set alertariesgolactancia
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $alertariesgolactancia
     * */
    public function setAlertariesgolactancia(
        \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $alertariesgolactancia = null
    ) {
        $this->alertariesgolactancia = $alertariesgolactancia;
        return $this;
    }

    /**
     * Get alertariesgolactancia
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getAlertariesgolactancia()
    {
        return $this->alertariesgolactancia;
    }

    /**
     * Set advertenciaedad
     * @param integer advertenciaedad
     * @return medicamento
     * */
    public function setAdvertenciaedad($advertenciaedad)
    {
        $this->advertenciaedad = $advertenciaedad;
        return $this;
    }

    /**
     * Get advertenciaedad
     * @return integer
     * */
    public function getAdvertenciaedad()
    {
        return $this->advertenciaedad;
    }

    /**
     * Set edadminima
     * @param integer edadminima
     * @return medicamento
     * */
    public function setEdadminima($edadminima)
    {
        $this->edadminima = $edadminima;
        return $this;
    }

    /**
     * Get edadminima
     * @return integer
     * */
    public function getEdadminima()
    {
        return $this->edadminima;
    }

    /**
     * Set edadmaxima
     * @param integer edadmaxima
     * @return medicamento
     * */
    public function setEdadmaxima($edadmaxima)
    {
        $this->edadmaxima = $edadmaxima;
        return $this;
    }

    /**
     * Get edadmaxima
     * @return integer
     * */
    public function getEdadmaxima()
    {
        return $this->edadmaxima;
    }

    /**
     * Set reaccionesadversas
     * @param string reaccionesadversas
     * @return medicamento
     * */
    public function setReaccionesadversas($reaccionesadversas)
    {
        $this->reaccionesadversas = $reaccionesadversas;
        return $this;
    }

    /**
     * Get reaccionesadversas
     * @return string
     * */
    public function getReaccionesadversas()
    {
        return $this->reaccionesadversas;
    }

    /**
     * Set condicionesalmacenamiento
     * @param string condicionesalmacenamiento
     * @return medicamento
     * */
    public function setCondicionesalmacenamiento($condicionesalmacenamiento)
    {
        $this->condicionesalmacenamiento = $condicionesalmacenamiento;
        return $this;
    }

    /**
     * Get condicionesalmacenamiento
     * @return string
     * */
    public function getCondicionesalmacenamiento()
    {
        return $this->condicionesalmacenamiento;
    }

    /**
     * Set interacciones
     * @param string interacciones
     * @return medicamento
     * */
    public function setInteracciones($interacciones)
    {
        $this->interacciones = $interacciones;
        return $this;
    }

    /**
     * Get interacciones
     * @return string
     * */
    public function getInteracciones()
    {
        return $this->interacciones;
    }

    /**
     * Set infoadicional
     * @param string infoadicional
     * @return medicamento
     * */
    public function setInfoadicional($infoadicional)
    {
        $this->infoadicional = $infoadicional;
        return $this;
    }

    /**
     * Get infoadicional
     * @return string
     * */
    public function getInfoadicional()
    {
        return $this->infoadicional;
    }

    /**
     * Set preciounitario
     * @param decimal preciounitario
     * @return medicamento
     * */
    public function setPreciounitario($preciounitario)
    {
        $this->preciounitario = $preciounitario;
        return $this;
    }

    /**
     * Get preciounitario
     * @return decimal
     * */
    public function getPreciounitario()
    {
        return $this->preciounitario;
    }

    /**
     * Set externalizable
     * @param integer externalizable
     * @return medicamento
     * */
    public function setExternalizable($externalizable)
    {
        $this->externalizable = $externalizable;
        return $this;
    }

    /**
     * Get externalizable
     * @return integer
     * */
    public function getExternalizable()
    {
        return $this->externalizable;
    }

    /**
     * Set unidadadmin_id
     * @param integer unidadadmin_id
     * @return medicamento
     * */
    public function setUnidadadminId($unidadadmin_id)
    {
        $this->unidadadmin_id = $unidadadmin_id;
        return $this;
    }

    /**
     * Get unidadadmin_id
     * @return integer
     * */
    public function getUnidadadminId()
    {
        return $this->unidadadmin_id;
    }

    /**
     * Set unidadadmin
     * @param \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $unidadadmin
     * */
    public function setUnidadadmin(\HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento $unidadadmin)
    {
        $this->unidadadmin = $unidadadmin;
        return $this;
    }

    /**
     * Get unidadadmin
     * @return \HCUE\AtencionMedicBundle\Entity\Detalleinfomedicamento
     * */
    public function getUnidadadmin()
    {
        return $this->unidadadmin;
    }

    /**
     * Set activo
     * @param integer activo
     * @return medicamento
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return medicamento
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return medicamento
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __toString()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

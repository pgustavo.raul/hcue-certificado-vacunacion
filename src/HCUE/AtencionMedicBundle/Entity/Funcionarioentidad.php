<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.funcionarioentidad")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\FuncionarioentidadRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Funcionarioentidad
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.funcionarioentidad_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $funcionario_id
     * @ORM\Column(name="funcionario_id", type="integer", nullable=false)
     **/
    private $funcionario_id;

    /**
     * @ORM\ManyToOne(targetEntity="Funcionario")
     * @ORM\JoinColumn(name="funcionario_id", referencedColumnName="id")
     **/
    private $funcionario;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var \Datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var \Datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     **/
    private $activo;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;


    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set funcionario_id
     * @param integer $funcionario_id
     * @return funcionarioentidad
     **/
    public function setFuncionarioId($funcionario_id)
    {
        $this->funcionario_id = $funcionario_id;
        return $this;
    }

    /**
     * Get funcionario_id
     * @return integer
     **/
    public function getFuncionarioId()
    {
        return $this->funcionario_id;
    }

    /**
     * Set funcionario
     * @param \HCUE\AtencionMedicBundle\Entity\Funcionario $funcionario
     * @return funcionarioentidad
     **/
    public function setFuncionario(\HCUE\AtencionMedicBundle\Entity\Funcionario $funcionario)
    {
        $this->funcionario = $funcionario;
        return $this;
    }

    /**
     * Get funcionario
     * @return \HCUE\AtencionMedicBundle\Entity\Funcionario
     **/
    public function getFuncionario()
    {
        return $this->funcionario;
    }

    /**
     * Set entidad_id
     * @param integer $entidad_id
     * @return funcionarioentidad
     **/
    public function setEntidadId($entidad_id)
    {
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId()
    {
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     * @return funcionarioentidad
     **/
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad)
    {
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidad()
    {
        return $this->entidad;
    }

    /**
     * Set estado
     * @param integer $estado
     * @return funcionarioentidad
     **/
    public function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set activo
     * @param integer $activo
     * @return funcionarioentidad
     **/
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariocreacion_id
     * @param integer $usuariocreacion_id
     * @return funcionarioentidad
     **/
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer $usuariomodificacion_id
     * @return funcionarioentidad
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechamodificacion = new \DateTime();
        $this->fechacreacion = new \DateTime();
        $this->activo = 1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

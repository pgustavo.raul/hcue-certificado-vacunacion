<?php 

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Table(name="hcue_amed.antecedentepaciente")
* @ORM\Entity()
*/
class AntecedentepacienteBasic
{
	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer")
	* @ORM\Id
	**/
	private $id;

	/**
	* @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
	* @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
	**/
	private $atencionmedica;

	/**
	* @ORM\ManyToOne(targetEntity="AntecedentevalidacionBasic")
	* @ORM\JoinColumn(name="antecedentevalidacion_id", referencedColumnName="id")
	**/
	private $antecedente;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string")
	**/
	private $descripcion;

    /**
     * @var string $valor
     * @ORM\Column(name="valor", type="string")
     **/
    private $valor;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Get atencionmedica
	* @return AtencionmedicaBasic
	**/
	public function getAtencionmedica(){
		 return $this->atencionmedica;
	}

	/**
	* Get antecedente
	* @return AntecedentevalidacionBasic
	**/
	public function getAntecedente(){
		 return $this->antecedente;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

    /**
     * Get valor
     * @return string
     **/
    public function getValor(){
        return $this->valor;
    }

}
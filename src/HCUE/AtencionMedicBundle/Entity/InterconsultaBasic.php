<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_amed.interconsulta")
 * @ORM\Entity()
 */
class InterconsultaBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     **/
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="ctespecialidadconsulta_id", referencedColumnName="id")
     **/
    private $ctespecialidadconsulta;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Funcionario")
     * @ORM\JoinColumn(name="funcionario_id", referencedColumnName="id")
     **/
    private $funcionario;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string")
     **/
    private $descripcion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     **/
    public function getAtencionmedica()
    {
        return $this->atencionmedica;
    }

    /**
     * Get ctespecialidadconsulta
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtespecialidadconsulta()
    {
        return $this->ctespecialidadconsulta;
    }

    /**
     * Get funcionario
     * @return \HCUE\AtencionMedicBundle\Entity\Funcionario
     **/
    public function getFuncionario()
    {
        return $this->funcionario;
    }

    /**
     * Get descripcion
     * @return string
     **/
    public function getDescripcion()
    {
        return $this->descripcion;
    }

}
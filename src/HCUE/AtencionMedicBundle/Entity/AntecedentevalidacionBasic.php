<?php 

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Table(name="hcue_amed.antecedentevalidacion")
* @ORM\Entity()
*/
class AntecedentevalidacionBasic
{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	**/
	private $id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
	* @ORM\JoinColumn(name="ctgrupoantecedente_id", referencedColumnName="id")
	**/
	private $ctgrupoantecedente;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
	* @ORM\JoinColumn(name="ctantecedente_id", referencedColumnName="id")
	**/
	private $ctantecedente;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Get ctgrupoantecedente
	* @return \Core\AppBundle\Entity\CatalogoBasic
	**/
	public function getCtgrupoantecedente(){
		 return $this->ctgrupoantecedente;
	}

	/**
	* Get ctantecedente
	* @return \Core\AppBundle\Entity\CatalogoBasic
	**/
	public function getCtantecedente(){
		 return $this->ctantecedente;
	}

}
<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.codigoatc")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\CodigoatcRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Codigoatc
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.codigoatc_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $codigo
     * @ORM\Column(name="codigo", type="string",length=20, nullable=true)
     * */
    private $codigo;

    /**
     * @var integer $nivel
     * @ORM\Column(name="nivel", type="integer", nullable=true)
     * */
    private $nivel;

    /**
     * @var integer $codigoatc_id
     * @ORM\Column(name="codigoatc_id", type="integer", nullable=true)
     * */
    private $codigoatc_id;

    /**
     * @ORM\ManyToOne(targetEntity="Codigoatc")
     * @ORM\JoinColumn(name="codigoatc_id", referencedColumnName="id")
     * */
    private $codigoatc;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=128, nullable=true)
     * */
    private $nombre;

    /**
     * @var integer $ddd
     * @ORM\Column(name="ddd", type="integer", nullable=true)
     * */
    private $ddd;

    /**
     * @var string $unidadddd
     * @ORM\Column(name="unidadddd", type="string",length=20, nullable=true)
     * */
    private $unidadddd;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     * @param string codigo
     * @return codigoatc
     * */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * Get codigo
     * @return string
     * */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nivel
     * @param integer nivel
     * @return codigoatc
     * */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
        return $this;
    }

    /**
     * Get nivel
     * @return integer
     * */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set codigoatc_id
     * @param integer codigoatc_id
     * @return codigoatc
     * */
    public function setCodigoatcId($codigoatc_id)
    {
        $this->codigoatc_id = $codigoatc_id;
        return $this;
    }

    /**
     * Get codigoatc_id
     * @return integer
     * */
    public function getCodigoatcId()
    {
        return $this->codigoatc_id;
    }

    /**
     * Set codigoatc
     * @param \HCUE\AtencionMedicBundle\Entity\Codigoatc $codigoatc
     * */
    public function setCodigoatc(\HCUE\AtencionMedicBundle\Entity\Codigoatc $codigoatc)
    {
        $this->codigoatc = $codigoatc;
        return $this;
    }

    /**
     * Get codigoatc
     * @return \HCUE\AtencionMedicBundle\Entity\Codigoatc
     * */
    public function getCodigoatc()
    {
        return $this->codigoatc;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return codigoatc
     * */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     * */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ddd
     * @param integer ddd
     * @return codigoatc
     * */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;
        return $this;
    }

    /**
     * Get ddd
     * @return integer
     * */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * Set unidadddd
     * @param string unidadddd
     * @return codigoatc
     * */
    public function setUnidadddd($unidadddd)
    {
        $this->unidadddd = $unidadddd;
        return $this;
    }

    /**
     * Get unidadddd
     * @return string
     * */
    public function getUnidadddd()
    {
        return $this->unidadddd;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
    }

    public function __toString()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

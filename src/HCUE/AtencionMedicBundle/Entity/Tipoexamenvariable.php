<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.tipoexamenvariable")
* @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\TipoexamenvariableRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Tipoexamenvariable{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.tipoexamenvariable_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $cttipoordenmedica_id
	* @ORM\Column(name="cttipoordenmedica_id", type="integer", nullable=true)
	**/
	private $cttipoordenmedica_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="cttipoordenmedica_id", referencedColumnName="id")
	**/
	private $cttipoordenmedica;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=true)
	**/
	private $estado;

	/**
	* @var integer $detalleserviciotarifario_id
	* @ORM\Column(name="detalleserviciotarifario_id", type="integer", nullable=true)
	**/
	private $detalleserviciotarifario_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Detalleserviciotarifario")
	* @ORM\JoinColumn(name="detalleserviciotarifario_id", referencedColumnName="id")
	**/
	private $detalleserviciotarifario;

	/**
	* @var integer $tipoexamenvariable_id
	* @ORM\Column(name="tipoexamenvariable_id", type="integer", nullable=true)
	**/
	private $tipoexamenvariable_id;

	/**
	* @ORM\ManyToOne(targetEntity="Tipoexamenvariable")
	* @ORM\JoinColumn(name="tipoexamenvariable_id", referencedColumnName="id")
	**/
	private $tipoexamenvariable;

	/**
	* @var string $descripcion
	* @ORM\Column(name="descripcion", type="string",length=250, nullable=true)
	**/
	private $descripcion;

	/**
	* @var string $nombre
	* @ORM\Column(name="nombre", type="string",length=250, nullable=true)
	**/
	private $nombre;

	/**
	* @var string $formulario
	* @ORM\Column(name="formulario", type="string",length=100, nullable=true)
	**/
	private $formulario;

	/**
	* @var integer $datosbasico
	* @ORM\Column(name="datosbasico", type="integer", nullable=true)
	**/
	private $datosbasico;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=true)
	**/
	private $activo;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set cttipoordenmedica_id
	* @param integer cttipoordenmedica_id
	* @return tipoexamenvariable
	**/
	public function setCttipoordenmedicaId($cttipoordenmedica_id){
		 $this->cttipoordenmedica_id = $cttipoordenmedica_id;
		 return $this;
	}

	/**
	* Get cttipoordenmedica_id
	* @return integer
	**/
	public function getCttipoordenmedicaId(){
		 return $this->cttipoordenmedica_id;
	}

	/**
	* Set cttipoordenmedica
	* @param \Core\AppBundle\Entity\Catalogo $cttipoordenmedica
	**/
	public function setCttipoordenmedica(\Core\AppBundle\Entity\Catalogo $cttipoordenmedica){
		 $this->cttipoordenmedica = $cttipoordenmedica;
		 return $this;
	}

	/**
	* Get cttipoordenmedica
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCttipoordenmedica(){
		 return $this->cttipoordenmedica;
	}

	/**
	* Set estado
	* @param integer estado
	* @return tipoexamenvariable
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set detalleserviciotarifario_id
	* @param integer detalleserviciotarifario_id
	* @return tipoexamenvariable
	**/
	public function setDetalleserviciotarifarioId($detalleserviciotarifario_id){
		 $this->detalleserviciotarifario_id = $detalleserviciotarifario_id;
		 return $this;
	}

	/**
	* Get detalleserviciotarifario_id
	* @return integer
	**/
	public function getDetalleserviciotarifarioId(){
		 return $this->detalleserviciotarifario_id;
	}

	/**
	* Set detalleserviciotarifario
	* @param \Core\AppBundle\Entity\Detalleserviciotarifario $detalleserviciotarifario
	**/
	public function setDetalleserviciotarifario(\Core\AppBundle\Entity\Detalleserviciotarifario $detalleserviciotarifario){
		 $this->detalleserviciotarifario = $detalleserviciotarifario;
		 return $this;
	}

	/**
	* Get detalleserviciotarifario
	* @return \Core\AppBundle\Entity\Detalleserviciotarifario
	**/
	public function getDetalleserviciotarifario(){
		 return $this->detalleserviciotarifario;
	}

	/**
	* Set tipoexamenvariable_id
	* @param integer tipoexamenvariable_id
	* @return tipoexamenvariable
	**/
	public function setTipoexamenvariableId($tipoexamenvariable_id){
		 $this->tipoexamenvariable_id = $tipoexamenvariable_id;
		 return $this;
	}

	/**
	* Get tipoexamenvariable_id
	* @return integer
	**/
	public function getTipoexamenvariableId(){
		 return $this->tipoexamenvariable_id;
	}

	/**
	* Set tipoexamenvariable
	* @param \HCUE\AtencionMedicBundle\Entity\Tipoexamenvariable $tipoexamenvariable
	**/
	public function setTipoexamenvariable(\HCUE\AtencionMedicBundle\Entity\Tipoexamenvariable $tipoexamenvariable){
		 $this->tipoexamenvariable = $tipoexamenvariable;
		 return $this;
	}

	/**
	* Get tipoexamenvariable
	* @return \HCUE\AtencionMedicBundle\Entity\Tipoexamenvariable
	**/
	public function getTipoexamenvariable(){
		 return $this->tipoexamenvariable;
	}

	/**
	* Set descripcion
	* @param string descripcion
	* @return tipoexamenvariable
	**/
	public function setDescripcion($descripcion){
		 $this->descripcion = $descripcion;
		 return $this;
	}

	/**
	* Get descripcion
	* @return string
	**/
	public function getDescripcion(){
		 return $this->descripcion;
	}

	/**
	* Set nombre
	* @param string nombre
	* @return tipoexamenvariable
	**/
	public function setNombre($nombre){
		 $this->nombre = $nombre;
		 return $this;
	}

	/**
	* Get nombre
	* @return string
	**/
	public function getNombre(){
		 return $this->nombre;
	}

	/**
	* Set formulario
	* @param string formulario
	* @return tipoexamenvariable
	**/
	public function setFormulario($formulario){
		 $this->formulario = $formulario;
		 return $this;
	}

	/**
	* Get formulario
	* @return string
	**/
	public function getFormulario(){
		 return $this->formulario;
	}

	/**
	* Set datosbasico
	* @param integer datosbasico
	* @return tipoexamenvariable
	**/
	public function setDatosbasico($datosbasico){
		 $this->datosbasico = $datosbasico;
		 return $this;
	}

	/**
	* Get datosbasico
	* @return integer
	**/
	public function getDatosbasico(){
		 return $this->datosbasico;
	}

	/**
	* Set activo
	* @param integer activo
	* @return tipoexamenvariable
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return tipoexamenvariable
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return tipoexamenvariable
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
				$this->activo=1;
$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
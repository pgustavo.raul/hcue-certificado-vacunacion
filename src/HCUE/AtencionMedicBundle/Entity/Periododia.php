<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Descripcion de Periododia
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.periododia")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\PeriododiaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Periododia
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.periododia_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=30, nullable=true)
     * */
    private $nombre;

    /**
     * @var string $horamin
     * @ORM\Column(name="horamin", type="string",length=20, nullable=true)
     * */
    private $horamin;

    /**
     * @var string $horamax
     * @ORM\Column(name="horamax", type="string",length=20, nullable=true)
     * */
    private $horamax;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     * */
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=true)
     * */
    private $usuariocreacion_id;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return periododia
     * */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     * */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set horamin
     * @param string horamin
     * @return periododia
     * */
    public function setHoramin($horamin)
    {
        $this->horamin = $horamin;
        return $this;
    }

    /**
     * Get horamin
     * @return string
     * */
    public function getHoramin()
    {
        return $this->horamin;
    }

    /**
     * Set horamax
     * @param string horamax
     * @return periododia
     * */
    public function setHoramax($horamax)
    {
        $this->horamax = $horamax;
        return $this;
    }

    /**
     * Get horamax
     * @return string
     * */
    public function getHoramax()
    {
        return $this->horamax;
    }

    /**
     * Set activo
     * @param integer activo
     * @return periododia
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return periododia
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return periododia
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

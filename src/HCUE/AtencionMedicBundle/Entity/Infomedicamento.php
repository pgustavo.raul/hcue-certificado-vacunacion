<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.infomedicamento")
 * @ORM\Entity(repositoryClass="HCUE\AtencionMedicBundle\Entity\Repository\InfomedicamentoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Infomedicamento
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.infomedicamento_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=100, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo nombre")
     * */
    private $nombre;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string",length=250, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo descripcion")
     * */
    private $descripcion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * */
    private $activo;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * */
    private $usuariocreacion_id;

    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return infomedicamento
     * */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get nombre
     * @return string
     * */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     * @param string descripcion
     * @return infomedicamento
     * */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * Get descripcion
     * @return string
     * */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     * @param integer activo
     * @return infomedicamento
     * */
    public function setActivo($activo)
    {
        if ($this->activo) {
            $this->activo = $activo;
        }
        return $this;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return infomedicamento
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return infomedicamento
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * @ORM\PrePersist
     * */
    public function prePersist()
    {
        $this->activo = 1;
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * */
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }

    public function __toString()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        return clone $this;
    }
}

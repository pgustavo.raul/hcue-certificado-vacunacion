<?php

namespace HCUE\AtencionMedicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hcue_amed.medicionpaciente")
 * @ORM\Entity()
 */
class MedicionpacienteBasic
{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     **/
    private $id;

    /**
     * @var integer $group_id
     * @ORM\Column(name="group_id", type="integer")
     **/
    private $group_id;

    /**
     * @ORM\ManyToOne(targetEntity="AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\CatalogoBasic")
     * @ORM\JoinColumn(name="ctantropovital_id", referencedColumnName="id")
     **/
    private $ctantropovital;

    /**
     * @var string $valor
     * @ORM\Column(name="valor", type="string")
     **/
    private $valor;

    /**
     * @var \Datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime")
     **/
    private $fechacreacion;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer")
     **/
    private $activo;

    /**
     * Get id
     * @return integer
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get group_id
     * @return integer
     **/
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     **/
    public function getAtencionmedica()
    {
        return $this->atencionmedica;
    }

    /**
     * Get ctantropovital
     * @return \Core\AppBundle\Entity\CatalogoBasic
     **/
    public function getCtantropovital()
    {
        return $this->ctantropovital;
    }

    /**
     * Get valor
     * @return string
     **/
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get fechacreacion
     * @return \Datetime
     * */
    public function getFechacreacion() {
        return $this->fechacreacion;
    }

}
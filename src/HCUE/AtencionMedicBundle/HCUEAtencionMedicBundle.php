<?php

namespace HCUE\AtencionMedicBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

//Se modifico el nombre de AtencionMedicaBundle porque daba conflicto con AtencionMedicaBundle de hcue-admin-amed,
//dos bundles no pueden llevar el mismo nombre así no pertenezcan al mismo módulo

class HCUEAtencionMedicBundle extends Bundle
{
}

<?php

namespace HCUE\AtencionMedicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AtencionMedicBundle:Default:index.html.twig');
    }
}

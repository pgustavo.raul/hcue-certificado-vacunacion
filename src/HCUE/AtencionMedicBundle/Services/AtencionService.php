<?php


namespace HCUE\AtencionMedicBundle\Services;

use HCUE\AtencionMedicBundle\Entity\Constantes;

/**
 * Description of AtencionService
 *
 * @author danny
 */
class AtencionService
{

    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Prepara los datos para desplegar en la vista
     * @param int $atencionmedica_id
     * @return array
     */
    public function preparePdfReport($atencionmedica_id)
    {
        $medicionManager = $this->container->get('hcue.amed.manager.medicionpaciente');
        $atencionmedicaManager = $this->container->get('hcue.amed.manager.atencionmedica');
        $detalleordenManager = $this->container->get('hcue.amed.manager.detalleordenmedica');
        $timeManager = $this->container->get('core.app.util.datetime');
        $atencionmedica = $atencionmedicaManager->getFormulario002($atencionmedica_id);
        $data = $atencionmedicaManager->getAtencionmedicaPreviewData($atencionmedica_id);
        $ordenes = $detalleordenManager->getOrdenmedicadetalleByAtencionmedicaId($atencionmedica_id);
        /*Inclusion tratamiento no farmacologico*/
        $tratamientonofarmManager =  $this->container->get("hcue.amed.manager.tratamientonofarm");
        $objindicaciones = $tratamientonofarmManager->selectByTratamientoId($atencionmedica_id,Constantes::CT_INDICACIONES_TRANOFARM);
        $objnutricion = $tratamientonofarmManager->selectByTratamientoId($atencionmedica_id,Constantes::CT_NUTRICION_TRANOFARM);
        $objrecomendaciones = $tratamientonofarmManager->selectByTratamientoId($atencionmedica_id,Constantes::CT_RECOMENDACIONES_TRANOFARM);
        $objTratamientonofarm = array(
            "objindicaciones" => $objindicaciones,
            "objnutricion" => $objnutricion,
            "objrecomendaciones" => $objrecomendaciones
        );

        $resultEnfermeria = $atencionmedicaManager->getMedicionesByPaciente($data['paciente_id'], $data['fechafinatencion']);
        $resultAmed = $medicionManager->getMedicionesByAtencion($atencionmedica_id);
        $medicionAmed = null;

        if (count($resultAmed) > 0) {
            $medicionAmed = $resultAmed;
            $medicionAmed = from($medicionAmed)
                ->groupBy(
                    '$med ==> $med["group_id"]',
                    '($med) ==> array(
                        "valor" => $med["valor"],
                        "medicion" => $med["medicion"]
                    )'
                )->toArray();
        }

        $enfermeria = null;
        $medicionEnfermeria = null;

        if (isset($resultEnfermeria[0])) {
            $enfermeria = $resultEnfermeria[0];
            $medicionEnfermeria = from($enfermeria['atencionmedicamediciones'])
                ->groupBy(
                    '$med ==> $med["group_id"]',
                    '($med) ==> array(
                        "valor" => $med["valor"],
                        "ctantropovital" => $med["ctantropovital"]
                    )'
            )->toArray();
        }

        $data['edad'] = '';
        $fechaNacimiento = $data['fechanacimiento'];
        $edad = ($fechaNacimiento) ? $timeManager->tiempoEntreFechas(date_format($fechaNacimiento, 'Y-m-d')) : '';
        if ($edad) {
            $data['edad'] = $edad['anios'] . ' años,  ' . $edad['meses'] . ' meses,  ' . $edad['dias'] . ' días';
        }

        $recetaDetalleManager = $this->container->get("hcue.amed.manager.recetadetalle");
        $horarioRecetaDetalleManager = $this->container->get("hcue.amed.manager.horarioprescripcion");
        $periodoDiaManager = $this->container->get("hcue.amed.manager.periododia");
        $recetaMedica = $recetaDetalleManager->getRecetadetalleBasic($atencionmedica_id);
        $periodos = $periodoDiaManager->getPeriododia();

        $MedicamentoHorario = [];
        if (!empty($recetaMedica)) {
            if (!empty($recetaMedica)) {
                $horarios = $horarioRecetaDetalleManager->getHorarioByRecetaId($recetaMedica[0]['receta_id'], true);
                foreach ($horarios as $horario) {
                    if (isset($MedicamentoHorario[$horario['recetadetalle_id']][$horario['periododia_id']])) {
                        $medicamentoHorarioAlone = $MedicamentoHorario[$horario['recetadetalle_id']][$horario['periododia_id']];
                        $MedicamentoHorario[$horario['recetadetalle_id']][$horario['periododia_id']] = $medicamentoHorarioAlone . ';' . $horario['horadia'];
                    } else {
                        $MedicamentoHorario[$horario['recetadetalle_id']][$horario['periododia_id']] = $horario['horadia'];
                    }
                }
            }
        }

        return array(
            'atencionmedica' => $atencionmedica,
            'data' => $data,
            'recetadetalle' => $recetaMedica,
            'medicamentoHorario' => $MedicamentoHorario,
            'periodos' => $periodos,
            'ordenes' => $ordenes,
            'enfermeria' => $enfermeria,
            'medicionenfermeria' => $medicionEnfermeria,
            'medicionesamed' => $medicionAmed,
            'tratamientonofarm' => $objTratamientonofarm
        );
    }

}

<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use HCUE\AtencionMedicBundle\Entity\Detalleordenmedica;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Clase de manipulación del repositorio Detalleordenmedica
 */
class DetalleordenmedicaManager extends BaseManager {

    /**
     * Obtener todos los detalles de ordenes médicas con el grupo
     * @param integer $ordenmedica_id
     * @return Array
     */
    public function getDetordenmedicaGrupoByOrdenmedicaId($ordenmedica_id)
    {
        return $this->getRepository()->getDetordenmedicaGrupoByOrdenmedicaId($ordenmedica_id);
    }

    /**
     * Eliminar el detalle orden médica
     * @param integer $ordenmedica_id
     * @return boolean
     */
    public function deleteDetordenmedicaByOrdenmedicaId($ordenmedica_id)
    {
        return $this->getRepository()->deleteDetordenmedicaByOrdenmedicaId($ordenmedica_id);
    }

    /**
     * Obtener todos los detalles de ordenes médicas
     * @param integer $atencionmedica_id
     * @return Array
     */
    public function getOrdenmedicadetalleByAtencionmedicaId($atencionmedica_id)  {
        return $this->getRepository()->getOrdenmedicadetalleByAtencionmedicaId($atencionmedica_id);
    }

    /**
     * Obtener todos los detalles de ordenes médicas
     * @param integer $ordenmedica_id
     * @return Array
     */
    public function getOrdenmedicadetalleByOrdenmedica($ordenmedica_id)  {
        return $this->getRepository()->getOrdenmedicadetalleByOrdenmedica($ordenmedica_id);
    }
}

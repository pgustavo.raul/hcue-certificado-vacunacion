<?php

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Clase de manipulación del repositorio Recetadetalle
 */
class RecetadetalleManager extends BaseManager
{
    /**
     * Obtiene el detalle de la receta por de la atencion medica
     * @param integer $atencion_id Id de la atencion medica
     * @return array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getRecetadetalleBasic($atencion_id)
    {
        return $this->getRepository()->getRecetadetalleBasic($atencion_id);
    }

    /**
     * Obtiene el detalle de la receta por id
     * @param integer $receta_id   Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getRecetadetalleByRecetaId($receta_id, $arrayResult = false)
    {
        return $this->getRepository()->getRecetadetalleByRecetaId($receta_id, $arrayResult);
    }

    /**
     * Verifica que un medicamento no se repita para una misma atención
     * @param integer $medicamento_id Id del medicamento
     * @param integer $atencion_id Id de la atencion medica
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function verifyRecetadetalleByMedicamentoId($medicamento_id, $atencion_id)
    {
        return $this->getRepository()->verifyRecetadetalleByMedicamentoId($medicamento_id, $atencion_id);
    }
}

<?php

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\AtencionMedicBundle\Entity\Receta;
use HCUE\AtencionMedicBundle\Entity\Constantes;

/**
 * Clase de manipulación del repositorio Receta
 */
class RecetaManager extends BaseManager
{

    /**
     * Obtiene la receta por id
     * @param integer $id          Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Receta"
     */
    public function getRecetaById($id, $arrayResult = false)
    {
        return $this->getRepository()->getRecetaById($id, $arrayResult);
    }

    /**
     * Obtiene la receta por la atención
     * @param integer $atencion_id Id de la Atención Medica
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Receta"
     */
    public function getRecetasByAtencion($atencion_id)
    {
        return $this->getRepository()->getRecetasByAtencion($atencion_id);
    }

    /**
     * Obtiene la receta por la atención
     * @param integer $atencion_id Id de la Atención Medica
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Receta"
     */
    public function getRecetaByAtencion($atencion_id)
    {
        return $this->findOneBy(array("atencionmedica_id" => $atencion_id));
    }

    /**
     * Obtiene el id de la receta por la atencion y el tipo de la receta
     * @param integer $atencion_id   Id de la Atención Medica
     * @param integer $tiporeceta_id Id del tipo de receta
     * @return integer
     */
    public function getIdByAtencionandTiporeceta($atencion_id, $tiporeceta_id)
    {
        $receta = $this->findOneBy(array(
            "atencionmedica_id" => $atencion_id,
            "cttiporeceta_id"   => $tiporeceta_id
        ));
        $id = ($receta) ? $receta->getId() : 0;

        return $id;
    }

    /**
     * Inactiva o elimina un registro basado en el modelo Receta
     * @param integer $id    Id del registro Receta
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id, $flush = true)
    {
        $receta = $this->find($id);
        if (!$receta) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }
        $catalogoManager = $this->getContainer()->get("core.app.entity.manager.catalogo");
        $recetadetalleManager = $this->getContainer()->get("hcue.amed.manager.recetadetalle");
        $detalle = $recetadetalleManager->getRecetadetalleByRecetaId($id, true);
        if (count($detalle) == 1) {
            $receta->setActivo(0);
            $receta->setCtEstado($catalogoManager->find(Constantes::CT_ESTADO_RECETA_ANULADA));
        } else {
            $receta->setActivo(1);
            $receta->setCtEstado($catalogoManager->find(Constantes::CT_ESTADO_RECETA_CONFIRMADA));
        }
        $this->save($receta, $flush);

        return true;
    }
}

<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\AtencionMedicBundle\Entity\Constantes;

/**
 * Clase de manipulación del repositorio Atencionmedica
 */
class AtencionmedicaManager extends BaseManager{

    /**
     * Retorna las atenciones medicas inicadas del medico por especialidad
     * @param integer $especialidad_id
     * @param integer $paciente_id
     * @return object Atencionmedica
     */
    public function getAllatenciones($especialidad_id, $paciente_id, $anio) {
        $arrayMeses = array(
            1 => array('label' => 'ENE', 'valor' => 0),
            2 => array('label' => 'FEB', 'valor' => 0),
            3 => array('label' => 'MAR', 'valor' => 0),
            4 => array('label' => 'ABR', 'valor' => 0),
            5 => array('label' => 'MAY', 'valor' => 0),
            6 => array('label' => 'JUN', 'valor' => 0),
            7 => array('label' => 'JUL', 'valor' => 0),
            8 => array('label' => 'AGO', 'valor' => 0),
            9 => array('label' => 'SEP', 'valor' => 0),
            10 => array('label' => 'OCT', 'valor' => 0),
            11 => array('label' => 'NOV', 'valor' => 0),
            12 => array('label' => 'DIC', 'valor' => 0)
        );

        $arrayReturn = array();
        $result = $this->getRepository()->getAllatenciones($especialidad_id, $paciente_id, $anio);

        if ( $result ) {
            foreach ($result as $item) {
                $mes = (int) $item['MES'];
                if(array_key_exists($mes, $arrayMeses)) {
                    $arrayMeses[$mes]['valor'] = (int) $item['CANTIDAD'];
                }
            }
        }

        if ( $result ) {
            $arrayReturn[0]["label"] = "Atenciones realizadas $anio";
            foreach ($arrayMeses as $mes) {
                $arrayReturn[0]["data"][] = array($mes['label'], $mes['valor']);
            }
        }

        return $arrayReturn;
    }

    /**
     * Retorna las atenciones medicas inicadas del medico por especialidad
     * @param integer $especialidad_id
     * @param boolean $sexo
     * @param integer $estado
     * @param boolean $count
     * @return object Atencionmedica
     */
    public function getAtencionesBySexoEspecialidad($especialidad_id=0, $sexo=0, $estado=Constantes::CT_ATENCIONFINALIZADA, $count=true)
    {
        $security = $this->getContainer()->get('core.seguridad.service.security_token');
        $entidad_id = $security->getEntidad()->getId();
        $usuario_id = $security->getUser()->getId();
        $result = $this->getRepository()->getAtencionesBySexoEspecialidad( $usuario_id, $entidad_id, $especialidad_id, $sexo, $count,$estado);

        return $result;
    }


    /**
     * Retorna la ultima atención activa por paciente
     *  @param integer $especialidad_id
     *  @return object Atencionmedica
     */
    public function getLastByPaciente($paciente_id, $especialidad_id=0)
    {
        $arraycondicion=array("paciente_id"=>$paciente_id,"ctestado_id"=>Constantes::CT_ATENCIONFINALIZADA,"activo"=>1);
        if($especialidad_id){
            $arraycondicion=array("paciente_id"=>$paciente_id,"ctestado_id"=>Constantes::CT_ATENCIONFINALIZADA,"ctespecialidadmedica_id"=>$especialidad_id,"activo"=>1);
        }


        return $this->findOneBy(
            $arraycondicion
            ,array("id"=>'DESC')
        );

    }


    /**
     * Retorna la ultima atención activa por paciente
     *  @param integer $especialidad_id
     *  @return object Atencionmedica
     */
    public function getLastByPacienteLessEnfermeria($paciente_id)
    {


        $result=$this->getRepository()->getLastAtencionByEspecialidades($paciente_id, Constantes::CT_ESPECIALIDADES_ENFERMERIA, $is_in_arrayespecialidades=false);
        return $result;


    }

    /**
     * Obener datos de atencion medica para el pdf
     * @param $atencionmedica_id
     * @return array
     */
    public function getAtencionmedicaPreviewData($atencionmedica_id){

        $atencionmedicaData = $this->getRepository()->getAtencionmedicaPreviewData($atencionmedica_id);
        return $atencionmedicaData;
    }

    /**
     * Obtiene datos de atencion medica para el pdf del formulario 002
     * @param $id
     * @return array
     */
    public function getFormulario002($id)
    {
        $reporteRepository = $this->getEntityManager()->getRepository('HCUEAtencionMedicBundle:AtencionmedicaBasic');
        $result = $reporteRepository->getAtencionmedica($id);

        return $result;
    }

    /**
     * Obtiene las mediciones correspondientes a la atencion solicitada y a la ultima atencion de enfermeria
     * @param integer $paciente_id
     * @param string $fecha
     * @return array
     */
    public function getMedicionesByPaciente($paciente_id, $fecha)
    {
        $reporteRepository = $this->getEntityManager()->getRepository('HCUEAtencionMedicBundle:AtencionmedicaBasic');
        $result = $reporteRepository->getMedicionesByPaciente($paciente_id, $fecha);

        return $result;
    }

}
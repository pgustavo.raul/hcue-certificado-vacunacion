<?php

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Clase de manipulación del repositorio Periododia
 */
class PeriododiaManager extends BaseManager
{

    /**
     * Obtiene los periodos
     * @return id Id del Periodo dia
     */

    public function getPeriododia()
    {
        $peridosdias = $this->getRepository()->getPeriododia($array = true);

        return $peridosdias;
    }
}

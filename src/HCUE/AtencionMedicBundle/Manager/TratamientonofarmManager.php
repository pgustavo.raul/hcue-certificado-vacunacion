<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
* requiere el bundle <AppBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
* Clase de manipulación del repositorio Tratamientonofarm
*/
class TratamientonofarmManager extends BaseManager{

    /**
     * Obtiene las indicaciones a enviar tomando en cuenta los tratamientos
     * @param $atencionmedica_id
     * @param $cttratamiento_id
     * @return mixed
     */
    public function selectByTratamientoId($atencionmedica_id,$cttratamiento_id)
    {
        $result= $this->getRepository()->selectByTratamientoId($atencionmedica_id,$cttratamiento_id);
        return $result;
    }

}
?>
<?php

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Clase de manipulación del repositorio Horarioprescripcion
 */
class HorarioprescripcionManager extends BaseManager
{

    /**
     * Obtiene el horario de preescripcion relacionados a la receta
     * @param integer $receta_id   Id de la receta
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\RecetaBundle\Entity\Recetadetalle"
     */
    public function getHorarioByRecetaId($receta_id, $arrayResult = false)
    {
        return $this->getRepository()->getHorarioByRecetaId($receta_id, $arrayResult);
    }

}

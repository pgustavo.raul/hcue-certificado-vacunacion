<?php
/*
 * Esta clase ha sido creada por el generador de código fuente AppGen v2.0,
 * requiere el bundle <AppBundle> para su correcto funcionamiento
 * @autor Luis Malquin
 */

namespace HCUE\AtencionMedicBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\AtencionMedicBundle\Entity\Constantes;

/**
 * Clase de manipulación del repositorio Medicionpaciente
 */
class MedicionpacienteManager extends BaseManager
{

    /**
     * Obtiene la medicion del signo vital
     * @param integer $paciente_id Id del Paciente
     * @param integer $atencion_id Id de la Atencion Medica
     * @param integer $group_id
     * @param integer $signo_id
     * @return integer
     */
    public function getMedicionPacienteBySigno($paciente_id, $atencion_id, $group_id, $signo_id)
    {
        return $this->getRepository()->getMedicionPacienteBySigno($paciente_id, $atencion_id, $group_id, $signo_id);
    }

    /**
     * Obtener historial de las mediciones
     * @param $paciente_id
     * @param $estadonutricion Indica si se muestra en historial los signos de estado de nutricion
     * @return array
     */
    public function getMedicionesRecordByPaciente($paciente_id, $estadonutricion = 0)
    {
        return $this->getRepository()->getMedicionesRecordByPaciente($paciente_id, $estadonutricion);
    }

    /**
     * Obtener las mediciones de una atención activa
     * @param $atencionmedica_id
     * @param $estadonutricion Indica si se consulta los signos de estado de nutricion
     * @return array
     */
    public function getMedicionesByAtencion($atencionmedica_id)
    {
        return $this->getRepository()->getMedicionesByAtencion($atencionmedica_id);
    }

}

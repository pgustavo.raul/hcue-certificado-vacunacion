<?php

/**
 * Servicio para la validación de token de google para recaptcha v3
 * RecaptchaService
 * @author Jipson Montalbán
 */
namespace HCUE\PacienteBundle\Services;

use Symfony\Component\DependencyInjection\Container;


class RecaptchaService
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Consume el webservis de discapacidad
     * @param string $token clave enviada desde el servicio de google
     * @return array con los resultados del consumo
     */
    public function validarTokenGoogle($token)
    {
        //dump($token);die;
        $app_service = $this->container->get('core.app.service.app');
        $url_verify = $app_service->getParameter('recaptcha_url.url_verifi');
        $data=[
            'secret'=> $app_service->getParameter('key_globals.secret_key'),
            'response'=> $token
        ];

        try {
            $cu = curl_init();
            curl_setopt($cu,CURLOPT_URL,$url_verify);
            curl_setopt($cu,CURLOPT_POST,true);
            curl_setopt($cu,CURLOPT_POSTFIELDS,http_build_query($data));
            curl_setopt($cu,CURLOPT_RETURNTRANSFER,true);
            $response = curl_exec($cu);
            curl_close($cu);
            return json_decode($response,true);
        } catch (\Exception $exc) {
            $response = array(
                "success" => false,
                "error-codes" => "Error en el token de recapcha v3"
            );
        } 
        return $response;
    }
}

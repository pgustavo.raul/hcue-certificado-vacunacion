<?php
/**
 * Created by PhpStorm.
 * User: egonzaga
 * Date: 29/11/18
 * Time: 11:37
 */

namespace HCUE\PacienteBundle\Services;

use Core\AppBundle\Entity\Persona;
use HCUE\PacienteBundle\Entity\Constantes;
use HCUE\PacienteBundle\Form\Type\MadrepacienteType;
use HCUE\PacienteBundle\Form\Type\PacienteType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\DateTime;

class MadrepacienteService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Método para guardar la información de los datos de la madre del paciente
     * @param $rmadrepcte
     */
    public function saveMadrepcte($objPaciente, $request){


        $securityToken = $this->container->get('core.seguridad.service.security_token');
        $managerPersona = $this->container->get("core.app.entity.manager.persona");

        $usuario = $securityToken->getPersona();
        $em = $this->container->get('doctrine.orm.entity_manager');
        $servicePersona = $this->container->get("hcue.paciente.service.persona");
        $serviceutil = $this->container->get("core.app.util.tipoidentificacion");
        $rmadrepcte = $request->get('paciente')['madrepcte'];
        $rpersona = $rmadrepcte["persona"];



        $objrequestmadrepcte=$objPaciente->getMadrepcte();
        $persona=$objrequestmadrepcte->getPersona();
        $numeroidentificacion=$persona->getNumeroidentificacion();
        $cttipoidentificacion=$persona->getCttipoidentificacion();
        $cttipoidentificacion_id = $cttipoidentificacion->getId() ;

        if($objrequestmadrepcte->getId()){
            $objrequestmadrepcte->setUsuariomodificacionId($usuario->getId());
        }

        $parroquianacimiento_id = $provincianacimiento_id = 0;
        if (array_key_exists("parroquia", $rpersona)) {
            $provincianacimiento_id = $rpersona['provincia'];
            $parroquianacimiento_id = $rpersona['parroquia'];
        }
        //si no encuentra persona busca si existe caso contrario lo crea,  el objeto viene seteado con null desde el controlador

        if(!$persona->getId()){
            $id_representante_noiden=(array_key_exists('persona_madre_paciente_id', $rmadrepcte)) ? $rmadrepcte["persona_madre_paciente_id"] : '';
            if($id_representante_noiden){
                $persona = $managerPersona->find($id_representante_noiden);
            }
            else{
                if($numeroidentificacion && $cttipoidentificacion_id){
                    $objpersona = $managerPersona->findOneBy(array('numeroidentificacion' => $numeroidentificacion,'cttipoidentificacion_id'=>$cttipoidentificacion_id,'activo'=>Constantes::CT_ESTADOGENERALACTIVO));
                    if($objpersona){
                        $persona= $objpersona;
                    }
                }
            }
        }

        if ($parroquianacimiento_id > 0) {
            $persona->setProvinciaId($provincianacimiento_id);
            $persona->setParroquiaId($parroquianacimiento_id);
            $persona->setParroquia($em->getReference('CoreAppBundle:Parroquia', $parroquianacimiento_id));
        }


        $fechanacimiento=$persona->getFechanacimiento();
        if(is_string($fechanacimiento)){
            $fechanacimiento=new \DateTime($fechanacimiento);
            $fechanacimiento->setTime(0, 0, 0);
            $persona->setFechanacimiento($fechanacimiento);
        }
        if ($cttipoidentificacion_id == Constantes::CT_TIPOIDENTNOIDENTIFICADO) {
            $numeroidentificacion = $servicePersona->GenerarSecuencial($persona);
            $persona->setNoidentificado($numeroidentificacion);
            $persona->setNumeroidentificacion($numeroidentificacion);
        }

        if ($cttipoidentificacion_id == Constantes::CT_TIPOIDENTCEDULA) {
            $validCedula = $serviceutil->validarCedula($numeroidentificacion);
            if (!$validCedula["validacion"]) {
                throw new \Exception($validCedula["mensaje"], -1);
            }
            $persona = $servicePersona->setFotoPersona($cttipoidentificacion_id, $numeroidentificacion, $persona);
        }
        $persona->setEstado(Constantes::CT_ESTADOGENERALACTIVO);
        $objrequestmadrepcte->setPersona($persona);
        $objrequestmadrepcte->setEstado(Constantes::CT_ESTADOGENERALACTIVO);
        $objPaciente->setMadrepcte($objrequestmadrepcte);

        return $objPaciente;


    }





}
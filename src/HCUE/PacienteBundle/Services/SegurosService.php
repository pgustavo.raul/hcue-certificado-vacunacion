<?php

/**
 * Servicio con metodos para manjear los webservis de los seguros
 * Descripcion de SegurosService
 * @author Richard Veliz
 */

namespace HCUE\PacienteBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use HCUE\PacienteBundle\Entity\Constantes;

class SegurosService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Consume los webservis del iess,issfa,isspol
     * @param string $cedula cedula de la persona
     * @return array con los resultados del consumo
     */
    public function consumir($cedula)
    {
        $hoy = date("Y/m/d");
        try {
            $service = $this->container->get("core.wsc.rpis");
            $serviceCatalogo = $this->container->get("core.app.entity.manager.catalogo");
            $resultservice = $service->getCoberturasAtencionMedica($cedula, $hoy);
            $idSeguro = 0;
            if (is_array($resultservice)) {
                $iess = $resultservice["iess"];
                $issfa = $resultservice["issfa"];
                $isspol = $resultservice["isspol"];
                if ($issfa["estado_operacion"] && $issfa["cobertura"]) {
                    $idSeguro = Constantes::CT_SEGURO_ISSFA;
                }
                if ($isspol["estado_operacion"] && $isspol["cobertura"]) {
                    $idSeguro = Constantes::CT_SEGURO_ISSPOL;
                }
                if ($iess["estado_operacion"] && $iess["cobertura"]) {
                    $seguro = ($iess["tipo_seguro"]);
                    if (!empty($seguro)) {
                        $idSeguro = $serviceCatalogo->searchCatalogo($seguro);
                        $idSeguro = $idSeguro ? $idSeguro->getId() : 0;
                    }
                } elseif (!$idSeguro) {
                    $idSeguro = Constantes::CT_SEGURO_NO_APORTA;
                }
            }

        } catch (\Exception $exc) {
            $idSeguro = 0;
        }

        return $idSeguro;
    }
}

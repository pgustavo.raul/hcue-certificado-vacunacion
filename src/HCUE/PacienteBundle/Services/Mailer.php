<?php

namespace HCUE\PacienteBundle\Services;

use Core\AppBundle\Services\Mailer as AppMailer;
use Symfony\Bridge\Twig\TwigEngine;
use Core\SeguridadBundle\Entity\Token as EntityToken;
use Core\SeguridadBundle\Entity\Usuario as EntityUsuario;
use Core\AppBundle\Utils\UtilDateTime;

class Mailer {

    protected $appmailer;
    protected $engine;

    function __construct(AppMailer $appMailer,  TwigEngine $engine) {
        $this->appmailer=$appMailer;
        $this->engine=$engine;
    }

    /**
     * Envía al email del paciente el certificado de vacunación
     * @param $datos
     * @param $html
     */
    public function sendNotificacionAceptacion($html,$datos){
        $to = array("{$datos['correo']}" => "{$datos['nombre']}");
        $body=  $html;
        $this->appmailer->setMessage($to, $body,'Notificación de aceptación de vacuna');
        return $this->appmailer->send();
    }
}

?>

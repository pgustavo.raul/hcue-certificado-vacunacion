<?php

/**
 * Servicio con metodos para manjear el webservis de discapacidad
 * Descripcion de DiscapacidadService
 * @author Richard Veliz
 */
namespace HCUE\PacienteBundle\Services;

use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Component\DependencyInjection\Container;

class DiscapacidadService
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Consume el webservis de discapacidad
     * @param string $cedula cedula de la persona
     * @return array con los resultados del consumo
     */
    public function consumir($cedula)
    {
        $dispacidad = Constantes::CT_WS_DISCAPACIDAD_RESPUESTA_NO;
        $porcentaje = $tipodiscapacidad = "";
        $tipodiscapacidad_id = 0;
        $webservis = false;
        try {
            $service = $this->container->get("core.wsc.discapacidades");
            $resultservice = $service->BuscarPersonaConDiscapacidad($cedula);
            $resultservice = get_object_vars($resultservice);
            if (is_array($resultservice)) {
                $webservis = true;
                if (array_key_exists("CodigoConadis", $resultservice)) {
                    $dispacidad = Constantes::CT_WS_DISCAPACIDAD_RESPUESTA_SI;
                    $porcentaje = $resultservice["PorcentajeDiscapacidad"];
                    $catalogoManager = $this->container->get("core.app.entity.manager.catalogo");
                    $tipodiscapacidad = $resultservice["DeficienciaPredomina"];
                    $serviceUtilstring = $this->container->get('core.app.util.string');
                    $tipodiscapacidad = $serviceUtilstring->removeAccent($tipodiscapacidad);
                    $catalogo = $catalogoManager->searchCatalogo($tipodiscapacidad, Constantes::TC_TIPODISCAPACIDAD);
                    $tipodiscapacidad_id = ($catalogo) ? $catalogo->getId() : 0;
                }
            }

            $result = array(
                "discapacidad" => $dispacidad,
                "porcentaje" => $porcentaje,
                "tipodiscapacidad_id" => $tipodiscapacidad_id,
                "tipodiscapacidad" => $tipodiscapacidad,
                "webservis" => $webservis
            );

        } catch (\Exception $exc) {
            $result = array(
                "webservis" => false,
                "discapacidad" => Constantes::CT_WS_DISCAPACIDAD_SIN_RESPUESTA
            );
        }

        return $result;
    }
}

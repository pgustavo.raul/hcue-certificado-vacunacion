<?php

/**
 * Servicio con metodos para manejar la lógica de las personas
 * Descripcion de PersonaService
 * @author Richard Veliz
 */

namespace HCUE\PacienteBundle\Services;

use Core\AppBundle\Entity\Persona;
use Symfony\Component\DependencyInjection\Container;
use HCUE\PacienteBundle\Entity\Constantes;

class PersonaService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Genera el número secuencial de los pacientes que no tienen cédula
     * @param Persona $dataPersona información del paciente necesaria para generar el secuencial
     * @return $secuencial string
     */
    public function GenerarSecuencial(Persona $dataPersona)
    {
        $primerNombre = mb_substr(trim($dataPersona->getPrimernombre()), 0, 2, 'UTF-8');
        $segundoNombre = trim($dataPersona->getSegundonombre())
            ? mb_substr(trim($dataPersona->getSegundonombre()), 0, 1, 'UTF-8') : 0;
        $apellidoPaterno = mb_substr(trim($dataPersona->getApellidopaterno()), 0, 2, 'UTF-8');
        $apellidoMaterno = trim($dataPersona->getApellidomaterno())
            ? mb_substr(trim($dataPersona->getApellidomaterno()), 0, 1, 'UTF-8') : 0;
        $fechaNacimiento = $dataPersona->getFechanacimiento();

        (is_string($fechaNacimiento)) ? $fechaNacimiento : $fechaNacimiento= $fechaNacimiento->format('Y-m-d');

        $utilDate = $this->container->get('core.app.util.datetime');

        if ($utilDate->validarFecha($fechaNacimiento)) {
            $fechaNacimiento = $fechaNacimiento->format('Y-m-d');
        }

        if (!$fechaNacimiento) {
            throw new \Exception('Se ha producido un error en el sistema, no es posible generar el secuencial', -1);
        }
        $arrayfecha = explode("-", $fechaNacimiento);

        if (!is_array($arrayfecha)) {
            $arrayfecha = explode("/", $fechaNacimiento);
        }
        //Formato YYYY-mm-dd
        $anio = $arrayfecha[2];
        $mes = $arrayfecha[1];
        $dia = $arrayfecha[0];

        if (strlen($anio) != 4) {
            //Formato dd-mm-YYYY
            $anio = $arrayfecha[0];
            $dia = $arrayfecha[2];
        }

        $control = mb_substr($anio, 2, 1, 'UTF-8');
        $idNacionalidad = $dataPersona->getPais()->getId();
        if ($idNacionalidad == Constantes::CT_NACIONALIDAD_ECUATORIANA) {
            $idProvincia = $dataPersona->getProvinciaId();
            $provinciaService = $this->container->get("core.app.entity.manager.provincia");
            $provincia = $provinciaService->find($idProvincia);
            $codigoProvincia = $provincia->getCodprovincia();
        } else {
            $codigoProvincia = "99";
        }
        $secuencialName = $primerNombre . $segundoNombre . $apellidoPaterno . $apellidoMaterno;
        $secuencialFecha = $anio . $mes . $dia;
        $secuencial = $secuencialName . $codigoProvincia . $secuencialFecha . $control;

        if (mb_strlen($secuencial) != 17) {
            throw new \Exception('Se ha producido un error en el sistema, no es posible generar los 17 dígitos del secuencial',
                -1);
        }


        return $secuencial;
    }

    public function setFotoPersona($tipoidentificacion, $numeroidentificacion, $objPersona)
    {
        if ($tipoidentificacion == Constantes::CT_TIPOIDENTCEDULA) {
            $managerArchivo = $this->container->get("core.app.entity.manager.archivo");
            $archivo = $managerArchivo->getArchivoByIdentificacion($numeroidentificacion);
            if ($archivo) {
                $objPersona->setFoto($archivo);
            }
        }
        return $objPersona;
    }
}

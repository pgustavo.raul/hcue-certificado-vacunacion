<?php

/**
 * Servicio con metodos para manjear la lógica de Pacientes
 * Descripcion de PacienteService
 * @author Richard Veliz
 */
namespace HCUE\PacienteBundle\Services;

use HCUE\PacienteBundle\Entity\Paciente;
use Symfony\Component\DependencyInjection\Container;
use Core\AppBundle\Entity\Persona;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Constantes;

class PacienteService
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Valida en el servidor el paciente
     * @param integer $tipoidentificacion tipo de identificación
     * @param integer $numeroidentificacionanterior numero de identificación del anterior registro
     * @param Persona $persona modelo de la Persona
     */
    public function validarPaciente(Persona $persona, $numeroidentificacionanterior = "")
    {
        $numeroidentificacion = $persona->getNumeroidentificacion();
        if (!empty($numeroidentificacion) && $numeroidentificacion != $numeroidentificacionanterior) {
            $serviceutilti = $this->container->get("core.app.util.tipoidentificacion");
            $servicepaciente = $this->container->get("hcue.paciente.entity.manager.paciente");
            if ($persona->getCttipoidentificacionId() == Constantes::CT_TIPOIDENTCEDULA) {
                $validCedula = $serviceutilti->validarCedula($numeroidentificacion);
                if (!$validCedula["validacion"]) {
                    throw new \Exception($validCedula["mensaje"], -1);
                }
            }
            $existe = $servicepaciente->existePaciente($numeroidentificacion);
            if ($existe["existe_paciente"]) {
                throw new \Exception("Existe un paciente ingresado con el mismo Número de Identicación, por favor revisar.", -1);
            }
        }
    }

    /**
     * Obtiene el base64 de una imagen
     * @param      $image   Nombre de la imagen con su extension
     * @param bool $coregui si es true busca en coregui/app/images/ caso contrario en /hcuereceta/images/
     * @return string
     **/
    public function getImagebase64($image, $coregui = false)
    {
        $rootDir = $this->container->get('kernel')->getRootDir();
        $url = ($coregui) ? '/../web/bundles/coregui/app/images/' : '/../web/bundles/hcuepaciente/images/';
        $img = base64_encode(file_get_contents($rootDir . $url . $image));

        return $img;
    }
}

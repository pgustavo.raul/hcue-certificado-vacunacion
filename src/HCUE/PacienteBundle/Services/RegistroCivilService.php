<?php

/**
 * Servicio con metodos para manjear el webservis del registro civil
 * Descripcion de RegistroCivilService
 * @author Richard Veliz
 */
namespace HCUE\PacienteBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use HCUE\PacienteBundle\Entity\Constantes;

class RegistroCivilService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Consume el servicio del webservis del Registro Civil
     * @param string $cedula cedula de la persona
     * @return array con los resultados del consumo
     */
    public function consumir($cedula)
    {
        $result = array();
        $webservis = false;
        $anormal = 0;
        try {
            $service = $this->container->get("core.wsc.registrocivil");
            $serviceCatalogo = $this->container->get("core.app.entity.manager.catalogo");
            $serviceProvincia = $this->container->get("core.app.entity.manager.provincia");
            $serviceCanton = $this->container->get("core.app.entity.manager.canton");
            $serviceParroquia = $this->container->get("core.app.entity.manager.parroquia");
            $servicePais = $this->container->get("core.app.entity.manager.pais");
            $serviceresult = $service->getInformacionPersonaPorNUI($cedula);
            $serviceresult = get_object_vars($serviceresult);
            $serviceresult = get_object_vars($serviceresult["return"]);

            if (empty($serviceresult)) {
                return array("webservis" => $webservis, "anormal" => 1);
            }

            $codigoMessage = $serviceresult["CodigoMensaje"];

            if ($codigoMessage == "000") {
                $serviceresult = get_object_vars($serviceresult["Ciudadano"]);
                $nombre = explode(" ", $serviceresult["Nombre"]);
                $nombre = $this->normalizarNombreCompleto($nombre);
                if (count($nombre) != 4) {
                    $anormal = 1;
                }
                $apellidopaterno = array_key_exists(0, $nombre) ? $nombre[0] : "";
                $apellidomaterno = array_key_exists(1, $nombre) ? $nombre[1] : "";
                $primernombre = array_key_exists(2, $nombre) ? $nombre[2] : "";
                $segundonombre = array_key_exists(3, $nombre) ? $nombre[3] : "";
                $nombrecompleto = $serviceresult["Nombre"];
                $webservis = true;
                $lugarnacimiento = explode("/", $serviceresult["LugarNacimiento"]);
                $fechanacimiento = $serviceresult["FechaNacimiento"];
                if (!empty($fechanacimiento)) {
                    $fechanacimiento = explode("/", $fechanacimiento);
                    $fechanacimiento = date_create($fechanacimiento[0] . '-' . $fechanacimiento[1] . '-' . $fechanacimiento[2]);
                    $fechanacimiento = date_format($fechanacimiento, "d-m-Y");
                }
                $provincia = $canton = $canton_id = $provincia_id = $parroquia = $sexo = $nacinalidad = $estadocivil = "";
                $extranjero = false;
                $estadocivil = $serviceCatalogo->searchCatalogo(ucfirst(strtolower($serviceresult["EstadoCivil"])));
                $sexo = $serviceCatalogo->searchCatalogo(ucfirst(strtolower($serviceresult["Sexo"])));
                $nacionalidad = $serviceresult["Nacionalidad"];
                $nacionalidad = mb_substr($nacionalidad, 0, mb_strlen($nacionalidad, 'UTF-8') - 2);
                $nacinalidad = $servicePais->searchNacionalidad($nacionalidad);
                if (is_array($lugarnacimiento) && count($lugarnacimiento) >= 3) {
                    $provincia = $serviceProvincia->searchProvincia($lugarnacimiento[0]);
                    $provincia_id = $provincia ? $provincia->getId() : "";
                    $canton = $serviceCanton->searchCanton($provincia_id, $lugarnacimiento[1]);
                    $canton_id = $canton ? $canton->getId() : "";
                    $parroquia = $serviceParroquia->searchParroquia($canton_id, $lugarnacimiento[2]);
                } elseif (is_array($lugarnacimiento) && count($lugarnacimiento) == 1) {
                    $extranjero = true;
                }
                $nombrepadre = $serviceresult["NombrePadre"];
                $nombremadre = $serviceresult["NombreMadre"];
                $result = array(
                    'anormal'             => $anormal,
                    "apellidopaterno"     => $apellidopaterno,
                    "apellidomaterno"     => $apellidomaterno,
                    "primernombre"        => $primernombre,
                    "segundonombre"       => $segundonombre,
                    "nombrecompleto"      => $nombrecompleto,
                    "nombrepadre"         => $nombrepadre,
                    "nombremadre"         => $nombremadre,
                    "fechanacimiento"     => $fechanacimiento,
                    "estadocivil"         => $estadocivil ? $estadocivil->getId() : "",
                    "sexo"                => $sexo ? $sexo->getId() : "",
                    "lugarnacimiento"     => $serviceresult["LugarNacimiento"],
                    "provincianacimiento" => $provincia ? $provincia->getId() : "",
                    "cantonnacimiento"    => $canton_id,
                    "parroquianacimiento" => $parroquia ? $parroquia->getId() : "",
                    "nacionalidad"        => $nacinalidad ? $nacinalidad->getId() : Constantes::CT_NACIONALIDAD_ECUATORIANA,
                    "disablednacionalidad"=> $nacinalidad ? true : false,
                    "extranjero"          => $extranjero,
                    "fechaCedulacion"     => $serviceresult["FechaCedulacion"],
                    "condicioncedulado"   => $serviceresult["CondicionCedulado"],
                    "webservis"           => $webservis,
                );
            } else {
                $result["webservis"] = $webservis;
                $result["anormal"] = $anormal;
            }

        } catch (\Exception $exc) {
            $result["webservis"] = $webservis;
            $result["anormal"] = $anormal;
        }

        return $result;
    }

    /**
     * Normaliza los nombres y apellidos que contengan determinativos "de","la","los",etc.
     * @param array $nombrecompleto array con los nombres y apellidos
     * @return array $nombrecompleto normalizado con los nombres y apellidos
     */
    public function normalizarNombreCompleto($nombrecompleto)
    {
        $determinativos = array(
            "DE",
            "DEL",
            "LA",
            "LAS",
            "LO",
            "LOS",
            "DE LOS",
            "DE LA"
        );
        $c = 0;
        $tamaño = count($nombrecompleto) - 1;
        foreach ($nombrecompleto as $k => $valor) {
            $valor = $nombrecompleto[$k];
            $bandera = false;
            foreach ($determinativos as $de) {
                if ($valor == $de) {
                    $nombrecompleto[$k + 1] = array_key_exists($k + 1, $nombrecompleto) ? $de . " " . $nombrecompleto[$k + 1] : $de;
                    $bandera = true;
                    unset($nombrecompleto[$k]);
                }
            }
            if ($tamaño == $k) {
                unset($nombrecompleto[$k]);
            }
            if ($bandera == false) {
                $nombrecompleto[$c] = $valor;
                $c++;
            }
        }

        return $nombrecompleto;
    }
}

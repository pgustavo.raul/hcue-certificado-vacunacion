<?php

namespace HCUE\PacienteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ArchivoType extends AbstractType
{

    private $container;

    /*
     * Set contenedor
     */

    public function setContainer($container)
    {
        $this->container = $container;
    }

    /*
     * Get contenedor
     */

    protected function getContainer()
    {
        return $this->container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $archivo = $options["data"];
        $security_service = $this->getContainer()->get('core.seguridad.service.security_token');
        $entidad_id = $security_service->getEntidad()->getId();
        $nombrepaciente = null;
        if ($archivo->getPacienteId()) {
            $service = $this->getContainer()->get('hcue.paciente.entity.manager.paciente');
            $paciente = $service->getPacienteById($archivo->getPacienteId());
            $nombrepaciente = $paciente->getPersona()->getNumeroIdentificacion() . " " . $paciente->getPersona()->getNombrecompleto();
        }

        $builder
            ->add('entidad_id', HiddenType::class, array('data' => $entidad_id))
            ->add('paciente_id', HiddenType::class)
            ->add('nombrepaciente', TextType::class, array(
                'label'  => 'Paciente',
                'mapped' => false,
                'data'   => $nombrepaciente,
                'attr'   => array(
                    'class'       => 'required',
                    'placeholder' => 'Ingrese el Paciente',
                    'disabled'    => true
                )
            ))
            ->add('numeroarchivo', TextType::class, array(
                'label' => 'Número de Archivo',
                'attr'  => array(
                    'class'       => 'numeric existe_archivo',
                    'placeholder' => 'Ingrese el Número de Archivo',
                    'maxlength'   => '10',
                    'min'         => '1'
                )
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HCUE\PacienteBundle\Entity\Archivo',
            "modo"       => null
        ));
    }

    public function getName()
    {
        return "archivo";
    }
}

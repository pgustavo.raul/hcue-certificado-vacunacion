<?php

namespace HCUE\PacienteBundle\Form\Type;

use Doctrine\Bundle\DoctrineBundle\Tests\DependencyInjection\TestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;

class PacienteType extends AbstractType
{

    private $em;
    private $container;

    public function __construct(EntityManager $entityManager,Container $container)
    {
        $this->em = $entityManager;
        $this->container=$container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $modo = $options["modo"];
        $paciente = $options["data"];

        $etnia = $paciente->getCtetnia();
        $pueblo = $paciente->getCtpueblo();
        $nacionalidadetnica = $paciente->getCtnacionalidadetnica();
        $parroquia_id = ($paciente->getParroquiaId()) ? $paciente->getParroquiaId() : 0;
        $canton_id = $provincia_id = 0;
        $nacionalidadetnica_id = $nacionalidadpueblos_id = 0;
        $discapacidad = ($paciente->getCtdiscapacidad()) ? $paciente->getCtdiscapacidad() : $this->em->getReference('CoreAppBundle:Catalogo',
            Constantes::CT_WS_DISCAPACIDAD_NO_APLICA);
        $tiporepresentante = ($paciente->getTiporepresentante() === null) ? 0 : $paciente->getTiporepresentante();
        $classrepresentante = ($paciente->getTiporepresentante() === null) ? "numeric cedula" : "upper alphanumeric";
        $maxlenghtrepresentante = ($paciente->getTiporepresentante() === null) ? "10" : "15";
        $nacionalidadetnicaDisabled = true;
        $parroquia = $canton = $provincia = $entidad = null;
        $persona = ($paciente->getPersona()) ? $paciente->getPersona() : null;
        $madrepcte = ($paciente->getMadrepcte()) ? $paciente->getMadrepcte() : $this->container->get('hcue.paciente.entity.manager.madrepaciente')->create();

        $repoCatalogo = $this->em->getRepository("CoreAppBundle:Catalogo");
        $repoCatalogoDescripcion = $this->container->get('hcue.paciente.entity.manager.paciente');

        $repoIncripcion =  $this->em->getRepository("HCUEInscripcionBundle:Inscripcion");
        $inscripcionManager= $this->container->get('hcue.paciente.entity.manager.inscripcion');
        if ($paciente->getCtnacionalidadetnicaId()) {
            $nacionalidadetnica_id = $paciente->getCtnacionalidadetnicaId();
            $nacionalidadpueblos_id = Constantes::TC_NACIONALIDADESPUEBLOS;
            $nacionalidadetnicaDisabled = false;
        }
        $puebloDisabled = ($paciente->getCtpuebloId() || $nacionalidadpueblos_id == Constantes::TC_NACIONALIDADESPUEBLOS) ? false : true;
        if ($parroquia_id > 0) {
            $repoparroquia = $this->em->getRepository('CoreAppBundle:Parroquia');
            $repoentidad = $this->em->getRepository('CoreAppBundle:Entidad');
            $parroquia = $repoparroquia->getUbicacionById($parroquia_id);
            $paciente->setParroquia($parroquia);
            $canton = $parroquia->getCanton();
            $canton_id = $canton->getId();
            $provincia = $canton->getProvincia();
            $provincia_id = $provincia->getId();
            if($paciente->getId()){
                $inscripcionPaciente = $repoIncripcion->getInscripcionByPacienteId($paciente->getId());
                if($inscripcionPaciente){
                    $entidad_id = $inscripcionPaciente->getEntidadId();
                    if($entidad_id){
                        $entidad = $repoentidad->getEntidadById($entidad_id);
                    }
                }
            }
        }

        $builder
            ->add('persona', PersonaType::class, array("data" => $persona, "modo" => $modo))
            ->add('madrepcte', MadrepacienteType::class, array("data"=>$madrepcte))
            ->add('numerohistoriaclinica', TextType::class, array(
                'label' => 'N. Historia Clínica',
                'attr' => array(
                    'class' => 'input-sm',
                    'disabled' => true,
                    'placeholder' => 'Nuevo'
                ),
                'required' => false
            ))
            ->add('tiporepresentante', ChoiceType::class, array(
                'choices' => array('Nacional' => 0, 'Extranjero' => 1),
                'expanded' => true,
                'multiple' => false,
                'label' => false,
                'data' => $tiporepresentante,
                'placeholder' => false
            ))
            ->add('numerorepresentante', TextType::class, array(
                'label' => 'N° de Identificación',
                'attr' => array(
                    'class' => "input-sm $classrepresentante",
                    'placeholder' => 'Número de Identificación',
                    'maxlength' => "$maxlenghtrepresentante"
                )
            ))
            ->add('celular', TextType::class, array(
                'label' => 'Celular',
                'attr' => array(
                    'class' => 'input-sm',
                    'placeholder' => 'Ingrese celular',
                    'mask' => '999-999-9999',
                    'disabled' => true
                ),
                'required' => false
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Teléfono',
                'attr' => array(
                    'class' => 'input-sm',
                    'placeholder' => 'Ingrese teléfono',
                    'mask' => '(99)-999-9999',
                    'disabled' => true
                ),
                'required' => false
            ))
            ->add('ctetnia', EntityType::class, array(
                'label' => 'Autoidentificación Étnica',
                'class' => 'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_AUTOIDENTIFICACIONETNICA),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $etnia,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('ctnacionalidadetnica', EntityType::class, array(
                'label' => 'Nacionalidad Étnica/Pueblos',
                'class' => 'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'choices' => $repoCatalogoDescripcion->getCatalogo($nacionalidadpueblos_id),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $nacionalidadetnica,
                'attr' => array(
                    'class' => 'input-sm',
                    'disabled' => $nacionalidadetnicaDisabled
                ),
                'required' => false
            ))
            ->add('ctpueblo', EntityType::class, array(
                'label' => 'Pueblos Kichwa',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogoDescripcion->getCatalogoChildrens($nacionalidadetnica_id),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $pueblo,
                'attr' => array(
                    'class' => 'input-sm require',
                    'disabled' => $puebloDisabled
                ),
                'required' => false
            ))
            ->add('ctestadoniveleducacion', EntityType::class, array(
                'label' => 'Estado de Nivel de Educación',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_ESTADOSNIVELESINSTRUCCION),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('pais', EntityType::class, array(
                'label'       => 'País de Residencia',
                'class'       => 'CoreAppBundle:Pais',
                'choices'     => $this->em->getRepository("CoreAppBundle:Pais")->getPaises(),
                'choice_label'    => 'nombre',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'attr'        => array(
                    'class'    => 'chosen-select input-sm required',
                    'disabled' => true
                )
            ))
            ->add('provincia', EntityType::class, array(
                'label' => 'Provincia',
                'mapped' => false,
                'required'    => false,
                'class' => 'CoreAppBundle:Provincia',
                'choices' => $this->em->getRepository("CoreAppBundle:Provincia")
                    ->getProvincias(false,1),
                'choice_label' => 'getCodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $provincia,
                'attr' => array(
                    'class' => 'chosen-select input-sm',
                    'disabled' => true
                )
            ))
            ->add('canton', EntityType::class, array(
                'label' => 'Cantón',
                'mapped' => false,
                'required'    => false,
                'class' => 'CoreAppBundle:Canton',
                'choice_label' => 'getCodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $canton,
                'choices' => $this->em->getRepository("CoreAppBundle:Canton")
                    ->getCantones($provincia_id),
                'attr' => array(
                    'class' => 'chosen-select input-sm',
                    'disabled' => true
                )
            ))
            ->add('parroquia', EntityType::class, array(
                'label' => 'Parroquia',
                'required'    => false,
                'class' => 'CoreAppBundle:Parroquia',
                'choice_label' => 'getCodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $parroquia,
                'choices' => $this->em->getRepository("CoreAppBundle:Parroquia")
                    ->getParroquias($canton_id),
                'attr' => array(
                    'class' => 'chosen-select input-sm',
                    'disabled' => true
                )
            ))
            ->add('correo', TextType::class, array(
                'label' => 'Correo Electrónico',
                'attr' => array(
                    "class" => 'lower email',
                    "placeholder" => 'Ingrese el Correo Electrónico',
                    'maxlength' => '60',
                    'disabled' => true
                ),
                'required' => false
            ))
            ->add('calleprincipal', TextType::class, array(
                'label' => 'Calle Principal',
                'attr' => array(
                    "class" => 'input-sm required upper alphanumericspace',
                    "placeholder" => 'Calle Principal',
                    'maxlength' => '150',
                    'disabled' => true
                )
            ))
            ->add('numero', TextType::class, array(
                'label' => 'Número',
                'attr' => array(
                    "class" => 'upper alphanumeric',
                    "placeholder" => 'Número',
                    'maxlength' => '7',
                    'disabled' => true
                ),
                'required' => false
            ))
            ->add('callesecundaria', TextType::class, array(
                'label' => 'Calle Secundaria',
                'attr' => array(
                    "class" => 'input-sm upper alphanumericspace',
                    "placeholder" => 'Calle Secundaria',
                    'maxlength' => '150',
                    'disabled' => true
                )
            ,
                'required' => false
            ))
            ->add('referenciaresidencia', TextType::class, array(
                'label' => 'Referencia de Residencia',
                'attr' => array(
                    "style" => 'resize:none; min-height: 100%;',
                    "class" => 'input-sm required upper alphanumericspace',
                    'placeholder' => 'Referencia del lugar donde vive',
                    'maxlength' => '250',
                    'disabled' => true
                )
            ))
            ->add('cttiposegurosalud', EntityType::class, array(
                'label' => 'Seguro de Salud Principal',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_TIPOSSEGUROSALUD),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('segurosaludsecundario', TextType::class, array(
                'label' => 'Seguro de Salud Secundario',
                'attr' => array(
                    'class' => 'upper alphaspace',
                    "placeholder" => 'Seguro de Salud Secundario',
                    'maxlength' => '50',
                    'disabled' => true
                ),
                'required' => false
            ))
            ->add('barrio', TextType::class, array(
                'label' => 'Barrio',
                'attr' => array(
                    'class' => 'input-sm required upper alphanumericspace',
                    "placeholder" => 'Barrio',
                    'maxlength' => '150',
                    'disabled' => true
                )
            ))
            ->add('cttipoempresatrabajo', EntityType::class, array(
                'label' => 'Tipo de Empresa de Trabajo',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_TIPOSEMPRESA),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('profesion', EntityType::class, array(
                'label' => 'Ocupación/Profesión Principal',
                'class' => 'CoreAppBundle:Profesion',
                'choices' => $this->em->getRepository("CoreAppBundle:Profesion")
                    ->getProfesiones(),
                'choice_label' => 'nombre',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'chosen-select input-sm required',
                    'disabled' => true
                )
            ))
            ->add('porcentajediscapacidad', HiddenType::class, array(
                'label' => 'Porcentaje de Discapacidad',
                'required' => false
            ))
            ->add('cttipodiscapacidad_id', HiddenType::class, array(
                'label' => 'Tipo de Discapacidad',
                'required' => false
            ))
            ->add('cttipobono', EntityType::class, array(
                'label' => 'Tipo de Bono que recibe',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_TIPOSBONOESTADO),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('ctdiscapacidad', EntityType::class, array(
                'label' => 'Tiene discapacidad?',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_DISCAPACIDAD),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $discapacidad,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('nombrecontacto', TextType::class, array(
                'label' => 'En caso necesario llamar a?',
                'attr' => array(
                    'class' => 'input-sm required upper alphaspace',
                    'placeholder' => 'Nombre y Apellido',
                    'maxlength' => '150',
                    'disabled' => true
                )
            ))
            ->add('telefonocontacto', TextType::class, array(
                'label' => 'Teléfono',
                'attr' => array(
                    'class' => 'input-sm numeric',
                    'placeholder' => 'Números',
                    'disabled' => true,
                    'maxlength' => '10',
                ),
                'required' => false
            ))
            ->add('direccioncontacto', TextareaType::class, array(
                'label' => 'Dirección',
                'attr' => array(
                    "style" => 'resize:none; min-height: 100%;',
                    'class' => 'input-sm upper alphanumericspace',
                    'placeholder' => 'Domiciliaria',
                    'maxlength' => '250',
                    'disabled' => true
                ),
                'required' => false
            ))
            ->add('ctparentezcocontacto', EntityType::class, array(
                'label' => 'Parentesco',
                'class' => 'CoreAppBundle:Catalogo',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_PARENTEZCOS),
                'choice_label' => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('fecharegistro', TextType::class, array(
                'label' => 'Fecha de Registro:',
                'required' => false,
                'mapped' => false,
                'attr' => array(
                    'class' => 'input-sm',
                    'disabled' => true,
                    'value' => date("Y-m-d")
                )
            ))
            ->add('ctparentescomadre',EntityType::class,array('label'=>'Parentesco',
                'class'=>'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('ct')
                        ->where('ct.id IN (:ids)')
                        ->setParameter('ids',Constantes::LIST_PARENTESCO_REPRESENTANTE)
                        ->orderBy('ct.descripcion', 'ASC');
                },
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('persona_paciente_id', HiddenType::class, array(
                'label' => 'Id de persona paciente:',
                'required' => false,
                'mapped' => false,
            ))
            ->add('entidad', EntityType::class, array(
                'label' => 'Establecimiento de Adscripción Territorial',
                'label_attr' => ['title'=>'Dato Importante ya que se realiza seguimiento de niños y mujeres embarazadas'],
                'mapped' => false,
                'class' => 'CoreAppBundle:Entidad',
                'choice_label' => 'nombreoficial',
                'choices' => $inscripcionManager->getEntidadesCercanasQueryBuilder($parroquia_id,$canton_id)->getQuery()->getResult(),
                'placeholder' => '--Seleccione--',
                'data' => $entidad,
                'attr' => array(
                    'class' => 'input-sm required',
                )
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HCUE\PacienteBundle\Entity\Paciente',
            "modo" => null
        ));
    }

    public function getName()
    {
        return "paciente";
    }
}

<?php

namespace HCUE\PacienteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ImpresreporthistclinicaType extends AbstractType {


    public function __construct($options = array()) {

    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('motivoreimpresion',  TextareaType::class,array(
                'label'=>'Motivo de Reimpresión',
                'required' => true,
                'attr'=>array(
                    'class'=>'upper required alphaspace',
                    "cols" => 40,
                    "rows" => 4,
                    'maxlength' => 200,
                    'placeholder' => 'Ingrese el motivo de reimpresión'
                )
            ));
    }

    public function getName() {
        return 'impresreporthistclinica';
    }



}
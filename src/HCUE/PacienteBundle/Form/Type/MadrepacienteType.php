<?php

namespace HCUE\PacienteBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Core\AppBundle\Utils\UtilDateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class MadrepacienteType extends AbstractType{

    private $em;
    private $util;

    public function __construct(EntityManager $entityManager, UtilDateTime $util) {
        $this->em = $entityManager;
        $this->util = $util;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $repoCatalogo = $this->em->getRepository("CoreAppBundle:Catalogo");
        $madrepcte = $options["data"];
        $persona = ($madrepcte->getPersona()) ? $madrepcte->getPersona() : null;

        $builder
            ->add('persona', PersonaType::class, array("data"=>$persona))
            ->add('direcciondomicilio',TextType::class, array(
                'label'=>'Dirección domicilio',
                'attr' => array(
                    "style" => 'resize:none; min-height: 100%;',
                    'class' => 'input-sm upper alphanumericspace required',
                    'placeholder' => 'Domicilio',
                    'maxlength' => '250',
                    'disabled' => true
                ),
                'required' => true
            ))
            ->add('numerotelefonico',TextType::class, array(
                'label'=>'Número telefónico',
                'attr' => array(
                    'class' => 'input-sm numeric required',
                    'placeholder' => 'Ingrese teléfono',
                    'maxlength' => 10,
                    'minlength' => 9,
                    'disabled' => true
                ),
                'required' => true
            ))

            ->add('ctetnia',EntityType::class,array('label'=>'Autoidentificación Étnica',
                'class'=>'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_AUTOIDENTIFICACIONETNICA),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
//                'data' => $etnia,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))

            ->add('recibebono', ChoiceType::class, array(
                'label' => 'Recibe Bono',
                'choices' => array("Si"=>1, "No"=>0),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                    'disabled' => true
                )
            ))

            ->add('estado',TextType::class,array('label'=>'Estado',
                'attr'=>array('class'=>'numeric')))

            ->add('persona_madre_paciente_id', HiddenType::class, array(
                'label' => 'Id de persona del representante paciente:',
                'required' => false,
                'mapped' => false,
            ))

        ;
    }

    public function getName() {
        return 'madrepaciente';
    }
}

<?php

namespace HCUE\PacienteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Core\AppBundle\Utils\UtilDateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PersonaType extends AbstractType
{

    private $em;
    private $util;

    public function __construct(
        EntityManager $entityManager,
        UtilDateTime $util
    ) {
        $this->em = $entityManager;
        $this->util = $util;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $repoParroquia = $this->em->getRepository("CoreAppBundle:Parroquia");

        $persona = $options["data"];
        $canton = $parroquia = $provincia = null;
        $edadanios = $edadmeses = $edaddias = "";
        $canton_id = $provincia_id = 0;
        $disabledNacionalidad = true;
        $disabledCedula = false;
        $fechaminima = date('Y-m-d', (strtotime('-120 year', strtotime(date('Y-m-d')))));
        $repoCatalogo = $this->em->getRepository("CoreAppBundle:Catalogo");
        $strFechaNac='';
        if (!empty($persona)) {
            $parroquia_id = ($persona->getParroquiaId()) ? $persona->getParroquiaId() : 0;
            $objParroquia=$repoParroquia->findOneBy(array("id"=>$parroquia_id,"estado"=>1));
            $fechaNacimiento = $persona->getFechanacimiento();
            if ($fechaNacimiento) {
                $strFechaNac=$fechaNacimiento->format('d-m-Y');
                $edad = $this->util->tiempoEntreFechas($fechaNacimiento->format('Y-m-d'));
                $edadanios = ($edad) ? $edad["anios"]: '';
                $edadmeses = ($edad) ? $edad["meses"] : '';
                $edaddias = ($edad) ? $edad["dias"] : '';
            }
            if($objParroquia){
                if ($parroquia_id > 0) {
                    $repoparroquia = $this->em->getRepository('CoreAppBundle:Parroquia');
                    $parroquia = $repoparroquia->getUbicacionById($parroquia_id);
                    $persona->setParroquia($parroquia);
                    $canton = $parroquia->getCanton();
                    $canton_id = $canton->getId();
                    $provincia = $canton->getProvincia();
                    $provincia_id = $provincia->getId();
                }
            }

            if ($persona->getPaisId()) {
                if ($persona->getPaisId() == 4) {
                    $disabledNacionalidad = false;
                }
            }
            if ($persona->getCttipoidentificacionId()) {
                if ($persona->getCttipoidentificacionId() == 6) {
                    $disabledCedula = true;
                }
            }
        }
        $builder
            ->add('cttipoidentificacion', EntityType::class, array(
                'label'       => 'Tipo de Identificación',
                'class'       => 'CoreAppBundle:Catalogo',
                'choices'     => $repoCatalogo->getCatalogo(Constantes::TC_TIPOIDENTIFICACION),
                'choice_label'    => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'attr'        => array(
                    'class'    => 'input-sm required',
                    'disabled' => $disabledCedula
                )
            ))
            ->add('numeroidentificacion', TextType::class, array(
                'label' => 'Número de Identificación',
                'attr'  => array(
                    'class'       => 'input-sm existe_paciente required upper ',
                    'disabled'    => true,
                    'placeholder' => 'Número Identificación',
                    'maxlength'   => '20'
                )
            ))
            ->add('apellidopaterno', TextType::class, array(
                'label' => 'Primer Apellido',
                'attr'  => array(
                    'class'       => 'input-sm required upper alphaspace',
                    'placeholder' => 'Ingrese primer apellido',
                    'maxlength'   => '50',
                    'minlength'   => '2',
                    'disabled'    => true
                )
            ))
            ->add('apellidomaterno', TextType::class, array(
                'label'    => 'Segundo Apellido',
                'attr'     => array(
                    'class'       => 'input-sm upper alphaspace',
                    'placeholder' => 'Ingrese segundo apellido',
                    'maxlength'   => '50',
                    'disabled'    => true
                ),
                'required' => false
            ))
            ->add('primernombre', TextType::class, array(
                'label' => 'Primer Nombre',
                'attr'  => array(
                    'class'       => 'input-sm required upper alphaspace',
                    'placeholder' => 'Ingrese primer nombre',
                    'maxlength'   => '50',
                    'minlength'   => '2',
                    'disabled'    => true
                )
            ))
            ->add('segundonombre', TextType::class, array(
                'label'    => 'Segundo Nombre',
                'attr'     => array(
                    'class'       => 'input-sm upper alphaspace',
                    'placeholder' => 'Ingrese segundo nombre',
                    'maxlength'   => '50',
                    'disabled'    => true
                ),
                'required' => false
            ))
            ->add('pais', EntityType::class, array(
                'label'       => 'Nacionalidad',
                'class'       => 'CoreAppBundle:Pais',
                'choices'     => $this->em->getRepository("CoreAppBundle:Pais")->getPaises(),
                'choice_label'    => 'nacionalidad',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'attr'        => array(
                    'class'    => 'chosen-select input-sm required',
                    'disabled' => true
                )
            ))
            ->add('provincia', EntityType::class, array(
                'label'       => 'Provincia',
                'mapped'      => false,
                'required'    => false,
                'class'       => 'CoreAppBundle:Provincia',
                'choices'     => $this->em->getRepository("CoreAppBundle:Provincia")->getProvincias(false,1),
                'choice_label'    => 'CodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'data'        => $provincia,
                'attr'        => array(
                    'class'    => 'chosen-select input-sm',
                    'disabled' => $disabledNacionalidad
                )
            ))
            ->add('canton', EntityType::class, array(
                'label'       => 'Cantón',
                'mapped'      => false,
                'class'       => 'CoreAppBundle:Canton',
                'choice_label'    => 'CodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'data'        => $canton,
                'required'    => false,
                'choices'     => $this->em->getRepository("CoreAppBundle:Canton")->getCantones($provincia_id),
                'attr'        => array(
                    'class'    => 'chosen-select input-sm',
                    'disabled' => $disabledNacionalidad
                )
            ))
            ->add('parroquia', EntityType::class, array(
                'label'       => 'Parroquia',
                'class'       => 'CoreAppBundle:Parroquia',
                'choice_label'    => 'CodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'data'        => $parroquia,
                'choices'     => $this->em->getRepository("CoreAppBundle:Parroquia")->getParroquias($canton_id),
                'required'    => false,
                'attr'        => array(
                    'class'    => 'chosen-select input-sm',
                    'disabled' => $disabledNacionalidad
                )
            ))
            ->add('lugarnacimiento', TextType::class, array(
                'label' => 'Lugar de Nacimiento',
                'attr'  => array(
                    'class'       => 'input-sm  required upper ',
                    'placeholder' => 'Ingrese el lugar de nacimiento',
                    'maxlength'   => '150',
                    'disabled'    => true
                )
            ))
            ->add('fechanacimiento', TextType::class, array(
                'label'  => 'Fecha de Nacimiento',
//                'widget' => 'single_text',
//                'format' => 'dd-MM-yyyy',
                'data'             => $strFechaNac,
                'attr'   => array(
                    'class'            => 'input-sm required input-inline date-picker',
                    'maxDate'          => date('d-m-Y'),
                    'minDate'          => $fechaminima,
                    'data-provide'     => 'date-picker',
                    'data-date-format' => 'dd-mm-yyyy',
                    'disabled'         => true,
                )
            ))
            ->add('edadanio', TextType::class, array(
                'label'    => 'Años',
                'mapped'   => false,
                'data'     => $edadanios,
                'attr'     => array(
                    'class'       => 'input-sm numeric',
                    'placeholder' => 'Años',
                    'maxlength'   => '3',
                    'disabled'    => true,
                    'min'    => 0
                ),
            ))
            ->add('edadmes', TextType::class, array(
                'label'    => 'Meses',
                'mapped'   => false,
                'data'     => $edadmeses,
                'attr'     => array(
                    'class'       => 'input-sm numeric',
                    'placeholder' => 'Meses',
                    'maxlength'   => '2',
                    'disabled'    => true
                ),
                'required' => false
            ))
            ->add('edaddia', TextType::class, array(
                'label'    => 'Días',
                'mapped'   => false,
                'data'     => $edaddias,
                'attr'     => array(
                    'class'       => 'input-sm numeric',
                    'placeholder' => 'Días',
                    'maxlength'   => '2',
                    'disabled'    => true
                ),
                'required' => false
            ))
            ->add('ctsexo', EntityType::class, array(
                'label'       => 'Sexo',
                'class'       => 'CoreAppBundle:Catalogo',
                'choices'     => $repoCatalogo->getCatalogo(Constantes::TC_SEXO),
                'choice_label'    => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'attr'        => array(
                    'class'    => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('ctestadocivil', EntityType::class, array(
                'label'       => 'Estado Civil',
                'class'       => 'CoreAppBundle:Catalogo',
                'choices'     => $repoCatalogo->getCatalogo(Constantes::TC_ESTADOCIVIL),
                'choice_label'    => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'attr'        => array(
                    'class'    => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('ctniveleducacion', EntityType::class, array(
                'label'       => 'Nivel de Educación',
                'class'       => 'CoreAppBundle:Catalogo',
                'choices'     => $repoCatalogo->getCatalogo(Constantes::TC_NIVELESINSTRUCCION),
                'choice_label'    => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                'attr'        => array(
                    'class'    => 'input-sm required',
                    'disabled' => true
                )
            ))
            ->add('foto_id', HiddenType::class, array(
                'label'    => 'Foto',
                'required' => false,
                "disabled" => true,
                'attr'     => array('class' => 'input-sm')
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\AppBundle\Entity\Persona',
            'data'       => null,
            'modo'       => null,
        ));
    }

    public function getName()
    {
        return "persona";
    }
}

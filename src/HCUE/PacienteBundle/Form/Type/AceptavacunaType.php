<?php

/**
 * Vista de registro de formulario
 * @author Luis Nuñez
 */

namespace HCUE\PacienteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AceptavacunaType extends AbstractType
{

    private $em;
    private $container;

    public function __construct(EntityManager $entityManager,Container $container)
    {
        $this->em = $entityManager;
        $this->container=$container;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aceptavacuna = $builder->getData();
        $repoCatalogo = $this->em->getRepository("CoreAppBundle:Catalogo");
        $repoParroquia = $this->em->getRepository("CoreAppBundle:Parroquia");
        $parroquia = $canton = $provincia =  $canton_id = $provincia_id= 0;
        $parroquia_id=$options['parroquia_id'];
        if($parroquia_id){
            $parroquia=$repoParroquia->find($parroquia_id);
            $canton=$parroquia->getCanton();
            $canton_id=$canton->getId();
            $provincia=$canton->getProvincia();
            $provincia_id=$provincia->getId();
        }
        $fechanacimiento="";
        if($aceptavacuna->getFechanacimiento()){
            $fechanacimiento=$aceptavacuna->getFechanacimiento()->format('d-m-Y');
        }
        $builder
            ->add('cttipoidentificacion', EntityType::class, array(
                'label' => 'Tipo identificación',
                'class' => 'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_TIPOIDENTIFICACION),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                )
            ))
            ->add('identificacion',TextType::class, array(
                'label' => 'Identificación',
                'attr' => array(
                    'class' => 'required',
                    'placeholder' => '',
                )
            ))
            ->add('nombre',TextType::class, array(
                'label' => 'Nombre Completo',
                'attr' => array(
                    'class' => 'required',
                    'placeholder' => 'Apellidos y Nombres',
                )
            ))
            ->add('fechanacimiento',TextType::class, array(
                'label' => 'Fecha de nacimiento',
                'mapped'=>false,
                'data' =>$fechanacimiento,
                'attr' => array(
                    'class' => 'required',
                    'placeholder' => '',
                )
            ))
            ->add('ctsexo', EntityType::class, array(
                'label' => 'Sexo',
                'class' => 'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_SEXO),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                )
            ))
            ->add('ctestadocivil', EntityType::class, array(
                'label' => 'Estado civil',
                'class' => 'CoreAppBundle:Catalogo',
                'choice_label' => 'descripcion',
                'choices' => $repoCatalogo->getCatalogo(Constantes::TC_ESTADOCIVIL),
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'input-sm required',
                )
            ))
            ->add('correo',TextType::class, array(
                'label' => 'Correo electrónico para notificaciones',
                //'required'    => false,
                'attr' => array(
                    'placeholder' => '',
                )
            ))
            ->add('provincia', EntityType::class, array(
                'label' => 'Provincia',
                'mapped' => false,
                'class' => 'CoreAppBundle:Provincia',
                'choices' => $this->em->getRepository("CoreAppBundle:Provincia")
                    ->getProvincias(false,1),
                'choice_label' => 'getCodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $provincia,
                'attr' => array(
                    'class' => 'chosen-select input-sm'
                    //, 'disabled' => true
                )
            ))
            ->add('canton', EntityType::class, array(
                'label' => 'Cantón',
                'mapped' => false,
                'class' => 'CoreAppBundle:Canton',
                'choice_label' => 'getCodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $canton,
                'choices' => $this->em->getRepository("CoreAppBundle:Canton")
                    ->getCantones($provincia_id),
                'attr' => array(
                    'class' => 'chosen-select input-sm'
                    //,'disabled' => true
                )
            ))
            ->add('parroquia', EntityType::class, array(
                'label' => 'Parroquia',
                'mapped' => false,
                'class' => 'CoreAppBundle:Parroquia',
                'choice_label' => 'getCodigoAndDescripcion',
                'placeholder' => '--Seleccione--',
                'empty_data' => null,
                'data' => $parroquia,
                'choices' => $this->em->getRepository("CoreAppBundle:Parroquia")
                    ->getParroquias($canton_id),
                'attr' => array(
                    'class' => 'chosen-select input-sm'
                    //, 'disabled' => true
                )
            ))


        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HCUE\PacienteBundle\Entity\Aceptavacuna',
            "modo"       => null,
            'parroquia_id' => null
        ));
    }

    public function getName()
    {
        return "aceptavacuna";
    }
}

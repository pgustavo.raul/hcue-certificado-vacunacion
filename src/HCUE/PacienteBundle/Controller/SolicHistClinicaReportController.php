<?php

namespace HCUE\PacienteBundle\Controller;

use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SolicHistClinicaReportController extends Controller
{
    /**
     * Obtiene la vista del grid (datatable) de la solicitud de Historia Clínica
     * @Route("/index/{menurol_id}",name="hcue_paciente_solic_hist_clinica_report_index",requirements={"menurol_id" = "\d+"},defaults={"menurol_id"=0})
     * @param int $menurol_id Id del menu del rol
     * @Template()
     * @return text/html HCUEPacienteBundle:SolicHistClinicaReport:index.html.twig
     * */
    public function indexAction($menurol_id = 0)
    {
        $service_security = $this->get('core.seguridad.service.security_authorization');
        if ($menurol_id > 0) {
            $service_security->setActiveMenuRolId($menurol_id);
        }
        $datatable = '';
        //Verificamos los permisos
        if ($service_security->hasPermission()) {
            $datatable = $this->get('hcue.paciente.datatable.solichistclinicareport');
            $datatable->buildDatatable();
        } else {
            $this->get("session")->getFlashBag()->add("danger", 'Acceso denegado !');
        }
        return array('datatable' => $datatable);
    }

    /**
     * Obtiene los datos del grid (datatable) de la solicitud de Historia Clínica
     * @Route("/results",name="hcue_paciente_solic_hist_clinica_report_results")
     * @param Request $request Variable de obtencion de datos
     * @return json
     * */
    public function resultsAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            try {
                $datatable = $this->get('hcue.paciente.datatable.solichistclinicareport');
                return $datatable->getResponse($request);
            } catch (\Exception $ex) {
                $this->get('core.app.logger')->addRecord($ex);
                $message = ($ex->getCode() >= 0)
                    ? "Se ha producido un error en el sistema, no es posible mostrar la información."
                    : $ex->getMessage();

                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }
        return new Response('Solicitud incorrecta.', 400);
    }

    /** Imprime las atenciones medicas finalizadas
     * @Route("/imprimir/{idsolicitud}/{atencionmedica_id}/{ctestadosolicitud_id}", name="hcue_paciente_atencion_imprimir",requirements={"atencionmedica_id" = "\d+", "ctestadosolicitud_id" = "\d+"}, defaults={"atencionmedica_id" = 0, "ctestadosolicitud_id" = 0}, options={"expose"=true})
     * @Method("GET")
     * @param int $atencionmedica_id
     * @param int $ctestadosolicitud_id
     * @return Response file pdf inline
     */
    public function imprimirAction($idsolicitud = 0, $atencionmedica_id = 0, $ctestadosolicitud_id = 0)
    {
        $service_security=$this->get('core.seguridad.service.security_authorization');
        $usuario_id = $service_security->getSecurityToken()->getUser()->getId();

        $atencionmedicaService = $this->get('hcue.amed.service.atencion');
        $datos = $atencionmedicaService->preparePdfReport($atencionmedica_id);

        if($datos) {
            $solicreporthistclinManager =  $this->get('hcue.paciente.manager.solicreporthistoriaclinica');
            $solicreporthistclinManager->updateStateSolicitudReport($idsolicitud, array('usuario_id' => $usuario_id, 'ctestadosolicitud_id' => $ctestadosolicitud_id));
            if($ctestadosolicitud_id == Constantes::CT_SOLICITUD_IMPRESA) {
                $impresreporthistclinicaManager = $this->get('hcue.Paciente.manager.impresreporthistclinica');
                $impresreporthistclinicaManager->saveImpresReportHistClinica(array(), $idsolicitud);
            }
        }

        $html = $this->renderView('HCUEAtencionMedicBundle:Dashboard/Preview:preview.html.twig', $datos);

        $header = $this->renderView('CoreGUIBundle:pdf:header.html.twig');

        $footer = $this->renderView('CoreGUIBundle:pdf:footer.html.twig', array(
            'footer_left' => 'MSP / HCU-form.002 / 2017',
            'footer_right' => 'CONSULTA EXTERNA - ANAMNESIS Y EXAMEN FÍSICO'
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, [
                'header-html' => $header,
                'footer-html' => $footer,
                'header-spacing' => '3',
                'footer-spacing' => '3',
                'margin-top' => 20,
                'margin-right' => 10,
                'margin-left' => 10,
                'margin-bottom' => 18,
                'enable-javascript' => true,
                'images' => true
            ]),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename=MSP_HCU-form.002_2017.pdf'
            )
        );
    }

    /**
     * Guarda los datos de un formulario basado en un modelo Impresreporthistclinica
     * @Route("/savereimpresion",name="hcue_paciente_atencion_reimprimir_save")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param integer $id
     * @return JsonResponse  {"status","message"}
     **/
    public function saveReimprimirAction(Request $request, $id = 0){

        $status = 0;
        $message = "";
        try {
            //Verficamos los permisos del usuario
            $service_security=$this->get('core.seguridad.service.security_authorization');
            if (($service_security->canAdd() && !$id) || ($service_security->canEdit() && $id)) {

                $impresreporthistclinicaManager =  $this->get('hcue.paciente.manager.impresreporthistclinica');
                $impresreporthistclinica = $request->get('impresreporthistclinica');
                $solicitudid = $request->get('impresreporthistclinica_idsolicitud');
                if($impresreporthistclinica) {
                    $impresreporthistclinicaManager->saveImpresReportHistClinica($impresreporthistclinica, $solicitudid);
                } else {
                    throw new \Exception('Se ha producido un error al realizar esta operación');
                }

                $status = 1;
                $message=($id)?"La solicitud ha sido actualizada correctamente":"La solicitud ha sido reimpresa correctamente";
            } else {
                throw new \Exception('No tiene permiso para realizar esta operación');
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode())?"Se ha producido un error en el sistema, no es posible guardar el registro de solicitud.":$ex->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));
    }

    /** Reimprime las atenciones medicas finalizadas
     * @Route("/reimprimir/{atencionmedica_id}/{idsolicitud}", name="hcue_paciente_atencion_reimprimir_add",requirements={"atencionmedica_id" = "\d+"}, defaults={"atencionmedica_id" = 0}, options={"expose"=true})
     * @Method({"GET","POST"})
     * @Template()
     * @return text/html HCUEAtencionMedicBundle:HistorialAtencion:reimprimiradd.html.twig
     */
    public function reimprimiraddAction($atencionmedica_id = 0, $idsolicitud = 0)
    {
        $formView = "";
        try {
            $service_security=$this->get('core.seguridad.service.security_authorization');
            if($service_security->canAdd()){
                $impresreporthistclinicaManager = $this->get('hcue.paciente.manager.impresreporthistclinica');
                $form = $impresreporthistclinicaManager->createForm();
                $formView = $form->createView();
            } else {
                throw new \Exception('Acceso denegado');
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode())?"Se ha producido un error en el sistema, no es posible mostrar la interfaz.":$ex->getMessage();
            $this->get("session")->getFlashBag()->add("danger", $message);
        }

        return array(
            'form' => $formView,
            'atencionmedica_id' => $atencionmedica_id,
            'idsolicitud' => $idsolicitud
        );
    }

}
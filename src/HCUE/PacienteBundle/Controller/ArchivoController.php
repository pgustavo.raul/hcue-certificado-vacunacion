<?php

namespace HCUE\PacienteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use HCUE\PacienteBundle\Form\Type\ArchivoType;
use Symfony\Component\HttpFoundation\Response;
use HCUE\PacienteBundle\Entity\Constantes;

class ArchivoController extends Controller
{
    /**
     * Obtiene la vista del grid (datatable) de los archivos de los pacientes
     * @Route("/index/{menurol_id}",name="hcue_paciente_archivo_datatable",defaults={"menurol_id" =0},requirements={"menurol_id" = "\d+"})
     * @param $menurol_id Id del menu del Rol
     * @Template()
     * @return text/html
     **/
    public function indexAction($menurol_id = 0)
    {
        $serviceSecurity = $this->get('core.seguridad.service.security_authorization');
        $serviceSecurity->setActiveMenuRolId($menurol_id);
        $datatable = '';
        //Verificamos los permisos
        if ($serviceSecurity->hasPermission()) {
            $datatable = $this->get('hcue.paciente.datatable.archivo');
            $datatable->buildDatatable();
        } else {
            $this->get("session")->getFlashBag()->add(
                "danger",
                'Acceso denegado !'
            );
        }
        return $this->render(
            'HCUEPacienteBundle:Archivo:index.html.twig',
            array('datatable' => $datatable)
        );
    }

    /**
     * Obtiene los datos del grid (datatable) de los pacientes
     * @Route("/results",name="hcue_paciente_archivo_results")
     * @param Request $request Variable de obtencion de datos
     * @return json
     **/
    public function indexResultsAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            try {
                $datatable = $this->get('hcue.paciente.datatable.archivo');
                return $datatable->getResponse($request);
            } catch (\Exception $ex) {
                $this->get('core.app.logger')->addRecord($ex);
                $message = ($ex->getCode()>=0)
                    ? "Se ha producido un error en el sistema, no es posible mostrar la información."
                    : $ex->getMessage();
                    $message= $ex->getMessage();
                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }
        return new Response('Solicitud incorrecta.', 400);
    }

    /**
     * Obtiene la vista de edicion de un registro de numero de archivo
     * @Route("/edit/{id}/{paciente_id}",name="hcue_paciente_archivo_edit",requirements={"id" = "\d+","paciente_id" = "\d+"}, defaults={"id" = 0,"paciente_id"=0}, options={"expose"=true})
     * @param integer $id         Id
     * @param integer $paciente_id Id del paciente
     * @param Request $request    Variable de obtencion de datos
     * @return text/html
     **/
    public function editAction(Request $request, $id = 0, $paciente_id = 0)
    {
        $modo = "EDITAR";
        $formView = $entidad_id = $archivo = "";
        try {
            $serviceSecurity = $this->get('core.seguridad.service.security_authorization');
            if ($serviceSecurity->canEdit()) {
                $managerArchivo
                    = $this->get("hcue.paciente.entity.manager.archivo");
                $modelo = $managerArchivo->create();
                $options = array("modo" => $modo);
                if ($id) {
                    $modelo = $this->getDoctrine()->getManager()
                        ->getReference("HCUEPacienteBundle:Archivo", $id);
                    if (empty($modelo)) {
                        throw $this->createNotFoundException("No se encontró un registro con id $id");
                    }
                } else {
                    $modelo->setPacienteId($paciente_id);
                }
                $form = $this->createForm(ArchivoType::class, $modelo, $options);
                $form->handleRequest($request);
                $formView = $form->createView();
                $archivo = $form->getData();
                $security_service = $this->get('core.seguridad.service.security_token');
                $entidad_id = $security_service->getEntidad()->getId();
            } else {
                throw new \Exception('Acceso denegado', -1);
            }
        } catch (\Exception $exc) {
            $message = $exc->getMessage();
            $this->get("session")->getFlashBag()->add(
                "danger",
                $message
            );
        }

        return $this->render(
            'HCUEPacienteBundle:Archivo:edit.html.twig',
            array(
                'form'      => $formView,
                'archivo'   => $archivo,
                'entidad_id' => $entidad_id,
                'modo'      => $modo
            )
        );
    }

    /**
     * Controlador de que obtiene el listado de los paciente filtrado por texto
     * @Route("/filter/{texto}", name="hcue_paciente_archivo_filtrar", defaults={"texto" = ""} )
     * @param integer $texto para filtrar los pacintes
     * @return json
     */
    public function filterPacienteAction($texto = "")
    {
        $result = array();
        $managerArchvio = $this->get("hcue.paciente.entity.manager.paciente");
        try {
            $paciente = $managerArchvio->getFilterPaciente($texto);
            foreach ($paciente as $p) {
                $identificacion = $p["numeroidentificacion"];
                $nombre = $p["nombrecompleto"];
                $result[] = array(
                    "id"    => $p["id"],
                    "value" => $identificacion . " " . $nombre
                );
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode()>=0)
                ? "Se ha producido un error en el sistema, no es posible filtar el paciente."
                : $ex->getMessage();
            $this->addFlash("danger", $message);
        }

        return new JsonResponse($result);
    }

    /**
     * Guarda los datos del paciente en la base de datos
     * @Route("/save",name="hcue_paciente_archivo_save")
     * @param Request $request Variable de obtencion de datos
     * @return text/html
     **/
    public function saveAction(Request $request)
    {
        $status = 0;
        $message = "";
        try {
            $serviceSecurity
                = $this->get('core.seguridad.service.security_authorization');
            if ($serviceSecurity->canAdd()) {
                $managerArchvio
                    = $this->get("hcue.paciente.entity.manager.archivo");
                $rarchivo = $request->get("archivo");
                $existe = $managerArchvio->existeArchivo(
                    $rarchivo["numeroarchivo"],
                    $rarchivo["entidad_id"]
                );
                if ($existe["existe_archivo"]) {
                    throw $this->createNotFoundException(
                        "El número de archivo ya existe..."
                    );
                }
                $form = $this->createForm(ArchivoType::class, $managerArchvio->create());
                $form->handleRequest($request);
                $result = $managerArchvio->saveArchivo($form->getData());
                if ($result) {
                    $status = 1;
                    $message = "Número de Archivo registrado correctamente..!";
                }
            } else {
                throw new \Exception('No tiene permiso para realizar esta operación', -1);
            }
        } catch (\Exception $ex) {
            $status = 0;
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode()>=0)
                ? "Se ha producido un error en el sistema, no es posible guardar la información."
                : $ex->getMessage();
        }

        return new JsonResponse(
            array(
                "status"  => $status,
                "message" => $message
            )
        );
    }

    /**
     * Actualiza los datos del paciente en la base de datos
     * @Route("/update/{id}",name="hcue_paciente_archivo_update",requirements={"id" = "\d+"}, defaults={"id" = 0}, options={"expose"=true})
     * @param interger $id      Id del Archivo paciente
     * @param Request  $request Variable de obtencion de datos
     * @return text/html
     **/
    public function updateAction(Request $request, $id = 0)
    {
        if (!$id) {
            return $this->saveAction($request);
        }
        $status = 0;
        $message = "";
        try {
            $serviceSecurity
                = $this->get('core.seguridad.service.security_authorization');
            if ($serviceSecurity->canEdit() && $id) {
                $managerArchvio
                    = $this->get("hcue.paciente.entity.manager.archivo");
                $archivo = $this->getDoctrine()->getManager()
                    ->getReference("HCUEPacienteBundle:Archivo", $id);
                if (!$archivo) {
                    throw $this->createNotFoundException(
                        "No se encontro el registro con id: $id"
                    );
                }
                $rarchivo = $request->get("archivo");
                $numeroarchivo = $rarchivo["numeroarchivo"];
                $entidad_id = $rarchivo["entidad_id"];
                if ($numeroarchivo != $archivo->getNumeroarchivo()) {
                    $existe = $managerArchvio->existeArchivo(
                        $numeroarchivo,
                        $entidad_id
                    );
                    if ($existe["existe_archivo"]) {
                        throw $this->createNotFoundException(
                            "El número de archivo ya existe..."
                        );
                    }
                }
                $form = $this->createForm(ArchivoType::class, $archivo);
                $form->handleRequest($request);
                $result = $managerArchvio->saveArchivo($form->getData());
                if ($result) {
                    $status = 1;
                    $message = "Número de Archivo actualizado correctamente..!";
                }
            } else {
                throw new \Exception('No tiene permiso para realizar esta operación', -1);
            }
        } catch (\Exception $ex) {
            $status = 0;
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode()>=0)
                ? "Se ha producido un error en el sistema, no es posible actualizar la información."
                : $ex->getMessage();
        }

        return new JsonResponse(
            array(
                "status"  => $status,
                "message" => $message
            )
        );
    }

    /**
     * Controlador llamdo por ajax para verifica si existe creado un numero de archivo para la misma entidad
     * @Route("/existearchivo/{numeroarchivo}/{entidad_id}/{id}", name="hcue_paciente_archivo_existe", defaults={"numeroarchivo" = 0,"entidad_id"=0,"id" = 0})
     * @param integer $numeroarchivo numero de archivo
     * @param integer $entidad_id     Id de la entidad
     * @param integer $id            Id del Archivo
     * @return json
     */
    public function existeAction($numeroarchivo = 0, $entidad_id = 0, $id = 0)
    {
        $status = 0;
        $existe = false;
        $numeroarchivoAnterior = "";
        $managerArchvio = $this->get("hcue.paciente.entity.manager.archivo");
        try {
            if ($id > 0) {
                $archivo = $this->getDoctrine()->getManager()
                    ->getReference("HCUEPacienteBundle:Archivo", $id);
                $numeroarchivoAnterior = $archivo->getNumeroarchivo();
            }
            if ($numeroarchivoAnterior != $numeroarchivo) {
                $existe = $managerArchvio->existeArchivo($numeroarchivo, $entidad_id);
                if ($existe) {
                    $status = $existe["existe_archivo"];
                }
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode()>=0)
                ? "Se ha producido un error en el sistema, no es posible verificar existencia."
                : $ex->getMessage();
            $this->addFlash("danger", $message);
        }

        return new JsonResponse($status);
    }

    /**
     * Obtiene la vista del busqueda avanzada de funcionarios
     * @Route("/filteravanzadaArchivo",name="hcue_paciente_archivo_filteravanzada")
     * @Template()
     * @return text/html CoreAdminBundle:Usuario:filterAvanzada.html.twig
     * */
    public function filterAvanzadaAction()
    {
        $catalogoManager = $this->get('core.app.entity.manager.catalogo');
        $tipoIdentificacion = $catalogoManager->getCatalogo(Constantes::TC_TIPOIDENTIFICACION);

        return ["tipoidentificacion"=>$tipoIdentificacion];
    }
}

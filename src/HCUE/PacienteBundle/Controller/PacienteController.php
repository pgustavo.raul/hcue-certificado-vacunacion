<?php

namespace HCUE\PacienteBundle\Controller;

use HCUE\PacienteBundle\Entity\Constantes;
use HCUE\PacienteBundle\Form\Type\PacienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Core\AppBundle\Entity\Constantes as ConstantesCore;
use Core\AppBundle\Entity\Combobox;


class PacienteController extends Controller
{
    /**
     * Obtiene la vista del grid (datatable) de los pacientes
     * @Route("/index/{menurol_id}",name="hcue_paciente_paciente_index",requirements={"menurol_id" = "\d+"},defaults={"menurol_id"=0})
     * @param int $menurol_id Id del menu del rol
     * @Template()
     * @return text/html HCUEPacienteBundle:Paciente:index.html.twig
     * */
    public function indexAction($menurol_id = 0)
    {
        $service_security = $this->get('core.seguridad.service.security_authorization');
        if ($menurol_id > 0) {
            $service_security->setActiveMenuRolId($menurol_id);
        }
        $datatable = '';
        //Verificamos los permisos
        if ($service_security->hasPermission()) {
            $datatable = $this->get('hcue.paciente.datatable.paciente');
            $datatable->buildDatatable();
        } else {
            $this->get("session")->getFlashBag()->add("danger", 'Acceso denegado !');
        }
        return array('datatable' => $datatable);
    }

    /**
     * Obtiene los datos del grid (datatable) de los pacientes
     * @Route("/results",name="hcue_paciente_paciente_results")
     * @param Request $request Variable de obtencion de datos
     * @return json
     * */
    public function resultsAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            try {
                $datatable = $this->get('hcue.paciente.datatable.paciente');
                return $datatable->getResponse($request);
            } catch (\Exception $ex) {
                $this->get('core.app.logger')->addRecord($ex);
                $message = ($ex->getCode() >= 0)
                    ? "Se ha producido un error en el sistema, no es posible mostrar la información."
                    : $ex->getMessage();
                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }
        return new Response('Solicitud incorrecta.', 400);
    }

    /**
     * Obtiene la lista de cantones identificados por el id de la provincia
     * @Route("/canton/combobox/{provincia_id}", name="hcue_paciente_paciente_cantoncombobox" , requirements={"provincia_id" = "\d+"}, defaults={"provincia_id"="0"})
     * @param integer $provincia_id id de la provincia
     * @return json lista de cantones en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function cantonComboboxAction($provincia_id = 0)
    {
        $managerCanton = $this->get("core.app.entity.manager.canton");
        $cantones = $managerCanton->getCantonesCombobox($provincia_id, true);
        $cantones = $this->get('serializer')->serialize($cantones, 'json');

        return new Response($cantones);
    }

    /**
     * Obtiene la lista de parroquias identificados por el id del canton
     * @Route("/parroquia/combobox/{canton_id}", name="hcue_paciente_paciente_parroquiacombobox", requirements={"canton_id" = "\d+"}, defaults={"canton_id"="0"})
     * @param integer $canton_id id del canton
     * @return json lista de parroquias en formato json {{Id:"..",CodDescripcion:".."},...}
     */
    public function parroquiaComboboxAction($canton_id = 0)
    {
        $managerProvincia = $this->get("core.app.entity.manager.parroquia");
        $parroquias = $managerProvincia->getParroquiasCombobox($canton_id, true);
        $parroquias = $this->get('serializer')->serialize($parroquias, 'json');

        return new Response($parroquias);
    }

    /**
     * Obtiene la lista de enitdades identificados por el id de la parroquia
     * @Route("/entidad/combobox/{parroquia_id}", name="hcue_paciente_paciente_entidadcombobox", requirements={"parroquia_id" = "\d+"}, defaults={"parroquia_id"="0"})
     * @param integer $parroquia_id id de la parroquia
     * @return json lista de entidades en formato json {{Id:"..",CodDescripcion:".."},...}
     */
    public function entidadComboboxAction($parroquia_id = 0)
    {
        $canton_id=0;
        if($parroquia_id){
            $canton_id=$this->get('core.app.entity.manager.parroquia')->find($parroquia_id)->getCantonId();
        }
        $managerInscripcion = $this->get("hcue.paciente.entity.manager.inscripcion");
        $entidades_data = $managerInscripcion->getEntidadesCercanasQueryBuilder($parroquia_id,$canton_id)->getQuery()->getResult();
        $arrayResult=array();
        foreach ($entidades_data as $obj){
            $row=new Combobox();
            $descripcion=$obj->getCodigoAndNombreoficial();
            $row->setId($obj->getId());
            $row->setDescripcion($descripcion);
            $arrayResult[]=$row;
        }
        $entidades = $this->get('serializer')->serialize($arrayResult, 'json');

        return new Response($entidades);
    }

    /**
     * Obtiene la lista de nacionalidades etnicas
     * @Route("/nacindigena/combobox/{ctcatalogoetnia_id}", name="hcue_paciente_paciente_nacindigenacombobox" , requirements={"ctcatalogoetnia_id" = "\d+"}, defaults={"ctcatalogoetnia_id"="0"})
     * @param integer $ctcatalogoetnia_id id del catalogo de etnia
     * @return json lista de nacionalidad Etnica en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function nacionalidadEtnicaComboboxAction($ctcatalogoetnia_id = 0)
    {
        if ($ctcatalogoetnia_id != Constantes::CT_ETNIA_INDIGENA) {
            return new JsonResponse("");
        }
        $managerCatalogo = $this->get("core.app.entity.manager.catalogo");
        $result = $managerCatalogo->getCatalogoCombobox(Constantes::TC_NACIONALIDADESPUEBLOS);
        $result = $this->get('serializer')->serialize($result, 'json');

        return new Response($result);
    }

    /**
     * Obtiene la lista de los pueblos
     * @Route("/pueblos/combobox/{ctnacionalidadetnica_id}", name="hcue_paciente_paciente_puebloscombobox" , requirements={"ctnacionalidadetnica_id" = "\d+"}, defaults={"ctnacionalidadetnica_id"="0"})
     * @param integer $ctnacionalidadetnica_id id del catalogo de Nacionalidad Etnicas
     * @return json lista de pueblos en formato json {{Id:"..",Descripcion:".."},...}
     */
    public function puebloComboboxAction($ctnacionalidadetnica_id = 0)
    {
        $managerCatalogo = $this->get("hcue.paciente.entity.manager.paciente");
        $result = $managerCatalogo->getCatalogoChildrens($ctnacionalidadetnica_id);
        $result = $this->get('serializer')->serialize($result, 'json');

        return new Response($result);
    }

    /**
     * Obtiene la vista de registro de pacientes
     * @Route("/add",name="hcue_paciente_paciente_add")
     * @param Request $request Variable de obtencion de datos
     * @return text
     * @throws \Exception
     * @Template()
     */
    public function addAction(Request $request)
    {
        $modo = "AGREGAR";
        $formView = "";
        $service_security = $this->get('core.seguridad.service.security_authorization');
        if ($service_security->canAdd()) {
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $options = array("modo" => $modo);
            $form = $pacienteManager->createForm(0, $options);
            $form->handleRequest($request);
            $formView = $form->createView();
        } else {
            throw new \Exception('Acceso denegado', -1);
        }
        return $this->render(
            'HCUEPacienteBundle:Paciente:add.html.twig',
            array(
                "form" => $formView,
                "modo" => $modo
            )
        );
    }

    /**
     * Guarda los datos del paciente en la base de datos
     * @Route("/save",name="hcue_paciente_paciente_save")
     * @param Request $request Variable de obtencion de datos
     * @return text/html
     * */
    public function saveAction(Request $request)
    {
        $url = ($request->get('btnconfirmarnuevo') == "true") ? 'hcue_paciente_paciente_add' : 'hcue_paciente_paciente_index';


        try {

            $service_security = $this->get('core.seguridad.service.security_authorization');
            if ($service_security->canAdd()) {
                $madrepacienteManager = $this->get("hcue.paciente.entity.manager.madrepaciente");
                $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
                $managerPersona = $this->get("core.app.entity.manager.persona");
                $paciente = $pacienteManager->create();
                $rpaciente = $request->get('paciente');
                $rpersona = $rpaciente["persona"];
                $parroquianacimiento_id = $provincianacimiento_id = 0;
                $tipoidentificacion = $numeroidentificacion = "";
                $id_paciente_noiden=(array_key_exists('persona_paciente_id', $rpaciente)) ? $rpaciente["persona_paciente_id"] : '';
                if (is_array($rpersona)) {
                    $tipoidentificacion = (array_key_exists('cttipoidentificacion', $rpersona)) ? $rpersona["cttipoidentificacion"] : '';
                    $numeroidentificacion = (array_key_exists('numeroidentificacion', $rpersona)) ? $rpersona["numeroidentificacion"] : '';
                    if (array_key_exists("parroquia", $rpersona)) {
                        $parroquianacimiento_id = $rpersona['parroquia'];
                        $provincianacimiento_id = $rpersona['provincia'];
                    }
                }
                if($id_paciente_noiden){
                    $persona = $managerPersona->find($id_paciente_noiden);
                }
                else{
                    $existePersona = $managerPersona->findOneBy(array('numeroidentificacion' => $numeroidentificacion));
                    $persona = ($existePersona) ? $managerPersona->find($existePersona->getId()) : $managerPersona->create();
                }
                if (is_array($rpaciente)) {
                    if (array_key_exists("tiporepresentante", $rpaciente)) {
                        $paciente->setTiporepresentante($rpaciente["tiporepresentante"]);
                    }
                    if (array_key_exists("parroquia", $rpaciente)) {
                        $paciente->setParroquiaId($rpaciente['parroquia']);
                    }
                    if (array_key_exists("ctnacionalidadetnica", $rpaciente)) {
                        $paciente->setCtnacionalidadetnicaId($rpaciente['ctnacionalidadetnica']);
                    }
                    if (array_key_exists("ctpueblo", $rpaciente)) {
                        $paciente->setCtpuebloId($rpaciente['ctpueblo']);
                    }
                    if (!empty($rpaciente['cttipodiscapacidad_id'])) {
                        $catalogoManager = $this->get("core.app.entity.manager.catalogo");
                        $tipodiscapacidad = $catalogoManager->find($rpaciente['cttipodiscapacidad_id']);
                        $paciente->setCttipodiscapacidad($tipodiscapacidad);
                    }
                }
                $servicePersona = $this->get("hcue.paciente.service.persona");
                $persona = $servicePersona->setFotoPersona($tipoidentificacion, $numeroidentificacion, $persona);
                if ($parroquianacimiento_id > 0) {
                    $persona->setProvinciaId($provincianacimiento_id);
                    $persona->setParroquiaId($parroquianacimiento_id);
                }
                $paciente->setPersona($persona);
                if(isset($request->get('paciente')['madrepcte'])) {
                    $madrepcte=$madrepacienteManager->getMadrepcte($request);
                    $paciente->setMadrepcte($madrepcte);
                }


                $form = $this->createForm(PacienteType::class, $paciente);
                $form->handleRequest($request);

                $result = $pacienteManager->savePaciente($form->getData(), $request);
                if ($result) {
                    $this->addFlash("success", "Paciente registrado correctamente..!");
                }
            }else {
                throw new \Exception('No tiene permiso para realizar esta operación', -1);
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() >= 0)
                ? "Se ha producido un error en el sistema, no es posible guardar el registro de paciente."
                : $ex->getMessage();
            $this->addFlash("danger", $message);
        }
        return $this->redirect($this->generateUrl($url));
    }

    /**
     * Actualiza los datos del paciente en la base de datos
     * @Route("/update/{id}",name="hcue_paciente_paciente_update",requirements={"id" = "\d+"}, defaults={"id" = 0})
     * @param integer $Id Id del paciente
     * @param Request $request Variable de obtencion de datos
     * @return text/html
     * */
    public function updateAction(Request $request, $id = 0)
    {
        $url = ($request->get('btnconfirmarnuevo') == "true")
            ? 'hcue_paciente_paciente_add' : 'hcue_paciente_paciente_index';

        try {

            $service_security = $this->get('core.seguridad.service.security_authorization');
            if ($service_security->canEdit() && $id) {
                $madrepacienteManager = $this->get("hcue.paciente.entity.manager.madrepaciente");

                $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
                $paciente = $this->getDoctrine()->getManager()->getReference("HCUEPacienteBundle:Paciente", $id);
                if (!$paciente) {
                    throw $this->createNotFoundException("No se encontro el registro con id: $id");
                }
                $rpaciente = $request->get('paciente');
                $rpersona = $rpaciente["persona"];
                $parroquianacimiento_id = $provincianacimiento_id = 0;
                $tipoidentificacion = $numeroidentificacion = "";
                $persona = $paciente->getPersona();
                $numeroidentificacionanterior = $persona->getNumeroidentificacion();
                if (is_array($rpersona)) {
                    $tipoidentificacion = (array_key_exists('cttipoidentificacion', $rpersona)) ? $rpersona["cttipoidentificacion"] : '';
                    $numeroidentificacion = (array_key_exists('numeroidentificacion', $rpersona)) ? $rpersona["numeroidentificacion"] : '';
                    if (array_key_exists("parroquia", $rpersona)) {
                        $provincianacimiento_id = $rpersona['provincia'];
                        $parroquianacimiento_id = $rpersona['parroquia'];
                    }

                }
                if (is_array($rpaciente)) {
                    if (array_key_exists("tiporepresentante", $rpaciente)) {
                        $paciente->setTiporepresentante($rpaciente["tiporepresentante"]);
                    }
                    if (array_key_exists("parroquia", $rpaciente)) {
                        $paciente->setParroquiaId($rpaciente['parroquia']);
                    }
                    if (array_key_exists("ctnacionalidadetnica", $rpaciente)) {
                        $paciente->setCtnacionalidadetnicaId($rpaciente['ctnacionalidadetnica']);
                    }
                    if (array_key_exists("ctpueblo", $rpaciente)) {
                        $paciente->setCtpuebloId($rpaciente['ctpueblo']);
                    }
                    if (!empty($rpaciente['cttipodiscapacidad_id'])) {
                        $catalogoManager = $this->get("core.app.entity.manager.catalogo");
                        $tipodiscapacidad = $catalogoManager->find($rpaciente['cttipodiscapacidad_id']);
                        $paciente->setCttipodiscapacidad($tipodiscapacidad);
                    }

                }
                $servicePersona = $this->get("hcue.paciente.service.persona");
                $persona = $servicePersona->setFotoPersona($tipoidentificacion, $numeroidentificacion, $persona);
                if ($parroquianacimiento_id > 0) {
                    $persona->setProvinciaId($provincianacimiento_id);
                    $persona->setParroquiaId($parroquianacimiento_id);
                }

                //busca la madrepcte , si no encuentra setea como null
                if(isset($request->get('paciente')['madrepcte'])) {
                    if(!$request->get('paciente')['madrepcte']['persona']['cttipoidentificacion']){
                        throw new \Exception('Se ha producido un error en el sistema, no es posible guardar el registro de paciente. Debe registrar los datos del representante ',-1);
                    }
                    $madrepcte = $madrepacienteManager->getMadrepcte($request);
                    $paciente->setMadrepcte($madrepcte);
                }

                $form = $this->createForm(PacienteType::class, $paciente);
                $form->handleRequest($request);

                $result = $pacienteManager->savePaciente($form->getData(), $request, $numeroidentificacionanterior);
                if ($result) {
                    $this->addFlash("success", "Paciente actualizado correctamente..!");
                }
            }
            else {
                throw new \Exception('No tiene permiso para realizar esta operación', -1);
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() >= 0)
                ? "Se ha producido un error en el sistema, no es posible guardar el registro de paciente. Verifique que exista información en los campos obligatorios (*) "
                : $ex->getMessage();
            $this->addFlash("danger", $message);
        }

        return $this->redirect($this->generateUrl($url));


    }

    /**
     * Obtiene la vista de consulta de un registro de paciente por id o por cedula
     * @Route("/show/{valor}/{escedula}", name="hcue_paciente_paciente_show", defaults={"valor" = 0,"escedula"=false}, options={"expose"=true})
     * @param integer $valor id o cédula del paciente
     * @param boolean $escedula Determina si el valor es cedula o el id
     * @throws \Exception
     * @Template()
     * @return text/html HCUEPacienteBundle:Paciente:show.html.twig
     * */
    public function showAction($valor = "", $escedula = false)
    {
        $modo = "CONSULTAR";
        $formView = $paciente = $anios = $estrategiapaciente = "";
        $service_security = $this->get('core.seguridad.service.security_authorization');

        if ($service_security->canView()) {
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $id = ($escedula) ? $pacienteManager->getIdporCedula($valor) : $valor;

            $options = array("disabled" => true, "modo" => $modo);
            $form = $pacienteManager->createForm($id, $options);
            $paciente = $form->getData();
            $formView = $form->createView();
            $fechaNac = $paciente->getPersona()->getFechanacimiento();
            $timeManager = $this->get('core.app.util.datetime');
            $edad = ($fechaNac) ? $timeManager->tiempoEntreFechas($fechaNac->format('Y-m-d')) : '';
            if ($edad) {
                $anios = $edad['anios'];
            }

            $estrategiapacienteManager = $this->get("hcue.paciente.manager.estrategiapaciente");
            $estrategiapaciente = $estrategiapacienteManager->getEstrategiapacienteByIdPaciente($id);

        } else {
            throw new \Exception('Acceso denegado', -1);
        }
        return array (
            'form' => $formView,
            'paciente' => $paciente,
            'modo' => $modo,
            'anios' => $anios,
            'estrategiapaciente' => $estrategiapaciente
        );
    }

    /**
     * Obtiene la vista de edicion de un registro de paciente
     * @Route("/edit/{id}",name="hcue_paciente_paciente_edit",requirements={"id" = "\d+"}, options={"expose"=true})
     * @param interger $id Id del Paciente
     * @param Request $request Variable de obtencion de datos
     * @throws \Exception
     * @Template()
     * @return text/html HCUEPacienteBundle:Paciente:edit.html.twig
     * */
    public function editAction(Request $request, $id = 0)
    {
        $modo = "EDITAR";
        $formView = $paciente = $estrategiapaciente = "";
        $service_security = $this->get('core.seguridad.service.security_authorization');

        if ($service_security->canEdit()) {
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $personaManager = $this->get('core.app.entity.manager.persona');
            $options = array("modo" => $modo);
            $form = $pacienteManager->createForm($id, $options);
            $form->handleRequest($request);
            $paciente = $form->getData();
            $formView = $form->createView();
            $fechaNac = $paciente->getPersona()->getFechanacimiento();
            $timeManager = $this->get('core.app.util.datetime');
            $edad = ($fechaNac) ? $timeManager->tiempoEntreFechas($fechaNac->format('Y-m-d')) : '';
            if ($edad) {
                $anios = $edad['anios'];
            }

            $estrategiapacienteManager = $this->get("hcue.paciente.manager.estrategiapaciente");
            $estrategiapaciente = $estrategiapacienteManager->getEstrategiapacienteByIdPaciente($id);
            $identificacion=$paciente->getPersona()->getNumeroidentificacion();
            if($paciente->getPersona()->getEstado()==1 && $paciente->getPersona()->getCttipoidentificacionId()==ConstantesCore::CT_TIPOIDENTCEDULA){
                $estadoPersona=$this->get('core.app.service.persona')->esFallecido($identificacion);
                if($estadoPersona){
                    if($personaManager->updateEstadoById($paciente->getPersona()->getId())){
                        $paciente->setEstado(0);
                    }
                }
            }

        } else {
            throw new \Exception('Acceso denegado', -1);
        }
        return $this->render(
            'HCUEPacienteBundle:Paciente:edit.html.twig',
            array(
                'form' => $formView,
                'paciente' => $paciente,
                'anios' => $anios,
                'modo' => $modo,
                'estrategiapaciente' => $estrategiapaciente
            )
        );
    }

    /**
     * Anula los datos del paciente en la base de datos
     * @Route("/eliminar/{id}",name="hcue_paciente_paciente_eliminar",requirements={"id" = "\d+"}, defaults={"id" = 0}, options={"expose"=true})
     * @param integer $id Id del Paciente
     * @return text/html
     * */
    public function eliminarAction($id)
    {
        $status = 0;
        $message = "";
        try {
            $service_security = $this->get('core.seguridad.service.security_authorization');
            if ($service_security->canDelete()) {
                if ($id > 0) {
                    $paciente = $this->getDoctrine()->getManager()
                        ->getReference("HCUEPacienteBundle:Paciente", $id);
                    if (!$paciente) {
                        throw $this->createNotFoundException("No se encontro el registro con id: $id");
                    }
                }
                $servicepaciente = $this->get("hcue.paciente.entity.manager.paciente");
                $result = $servicepaciente->eliminarPaciente($paciente);
                if ($result) {
                    $status = 1;
                    $message = "Paciente eliminado correctamente..!";
                }
            } else {
                throw new \Exception('No tiene permiso para realizar esta operación', -1);
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() >= 0)
                ? "Se ha producido un error en el sistema, no es posible eliminar el registro de paciente."
                : $ex->getMessage();
        }

        return new JsonResponse(array(
            "status" => $status,
            "message" => $message
        ));
    }

    /**
     * Consume el webservis del Registro Civil
     * @Route("/wsregistrocivil/{cedula}", name="hcue_paciente_paciente_wsregistrocivil" , requirements={"cedula" = "\d+"} , defaults={"cedula" = 0})
     * @param integer $cedula cédula de ciudadania del paciente
     * @return json con la respuesta del webservice
     */
    public function WSRegistroCivilAction($cedula)
    {
        $status = 0;
        $type = "noexiste";
        try {
            $serviceRegistrocivil = $this->get("hcue.paciente.service.registrocivil");
            $arrayResult = $serviceRegistrocivil->consumir($cedula);
            if ($arrayResult["webservis"]) {
                $type = "webservice";
                $status = 1;
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $arrayResult = "";
        }
        return new JsonResponse(array(
            'status' => $status,
            'type' => $type,
            'data' => $arrayResult
        ));
    }

    /**
     * Consume los webservis del iess,issfa,isspol
     * @Route("/wsseguros/{cedula}", name="hcue_paciente_paciente_wsseguros" , requirements={"cedula" = "\d+"} , defaults={"cedula" = 0})
     * @param integer $cedula cédula de ciudadania del paciente
     * @return json con la respuesta de los webservis
     */
    public function WSSegurosAction($cedula)
    {
        $status = 0;
        $seguro_id = 0;
        try {
            $serviceSeguros = $this->get("hcue.paciente.service.seguros");
            $seguro_id = $serviceSeguros->consumir($cedula);
            if ($seguro_id > 0) {
                $status = 1;
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
        }
        return new JsonResponse(array(
            'status' => $status,
            'seguro_id' => $seguro_id
        ));
    }

    /**
     * Consume el webservis de discapacidades
     * @Route("/wsdiscapacidad/{cedula}", name="hcue_paciente_paciente_wsdiscapacidad" , requirements={"cedula" = "\d+"} , defaults={"cedula" = 0})
     * @param integer $cedula cédula de ciudadania del paciente
     * @return json con la respuesta del webservis
     */
    public function WSDiscapacidadAction($cedula)
    {
        $status = 0;
        $result = array();
        try {
            $serviceDiscapacidad = $this->get("hcue.paciente.service.discapacidad");
            $result = $serviceDiscapacidad->consumir($cedula);
            $status = 1;
        } catch (\Exception $ex) {
            $result = array("discapacidad" => Constantes::CT_WS_DISCAPACIDAD_SIN_RESPUESTA);
            $this->get('core.app.logger')->addRecord($ex);
        }
        return new JsonResponse(array('status' => $status, 'data' => $result));
    }

    /**
     * Controlador llamdo por ajax para verifica si existe creado un Paciente
     * @Route("/existepaciente/{numeroidentificacion}/{id}", name="hcue_paciente_paciente_existepaciente", defaults={"numeroidentificacion" = 0,"id" = 0})
     * @param integer $numeroidentificacion numero de identificación del paciente
     * @param integer $id id del paciente
     * @return json
     */
    public function existePacienteAction($numeroidentificacion, $id)
    {
        $status = 0;
        $identificacionAnterior = "";
        try {
            if ($id > 0) {
                $persona = $this->getDoctrine()->getManager()->getReference("CoreAppBundle:Persona", $id);
                $identificacionAnterior = $persona->getNumeroidentificacion();
            }
            if ($numeroidentificacion != $identificacionAnterior) {
                $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
                $existe = $pacienteManager->existePaciente($numeroidentificacion);
                $status = $existe["existe_paciente"];
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
        }

        return new JsonResponse($status);
    }

    /**
     * Controlador llamdao por ajax para verifica si existe creado una Madrepaciente
     * @Route("/existmadrepcte/{numeroidentificacion}/{id}", name="hcue_paciente_paciente_existmadrepcte", defaults={"numeroidentificacion" = 0,"id" = 0})
     * @param integer $numeroidentificacion numero de identificación de madrepaciente
     * @param integer $id id de madrepaciente
     * @return json
     */
    public function existMadrepcteAction($numeroidentificacion, $id)
    {
        try {
            $persona=array();
            $personaManager = $this->get("core.app.entity.manager.persona");
            $parroquiaManager = $this->get("core.app.entity.manager.parroquia");
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $paciente=$pacienteManager->findOneBy(array('numerohistoriaclinica'=>$numeroidentificacion,'estado'=>Constantes::CT_ESTADOGENERALACTIVO,'activo'=>Constantes::CT_ESTADOGENERALACTIVO));
            if($paciente){
                $persona=$personaManager->obtenerPersonaPorIdentificacion($numeroidentificacion,true);
                if($persona){
                    $fechanacimiento=$persona[0]['fechanacimiento'];
                    $persona[0]['fechanacimiento']=$fechanacimiento->format('d-m-Y');
                    $parroquia_id=$persona[0]['parroquia_id'];
                    if($parroquia_id){
                        $objParroquia= $parroquiaManager->find($parroquia_id);
                        $objCanton=$objParroquia->getCanton();
                        $canton_id=$objCanton->getId();
                        $objProvincia=$objCanton->getProvincia();
                        $provincia_id=$objProvincia->getId();
                        $persona[0]['canton_id']=$canton_id;
                        $persona[0]['provincia_id']=$provincia_id;
                    }
                }
            }

        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
        }


        return new JsonResponse($persona);
    }

    /**
     * Accion que retorna los datos de un paciente por id
     * @Route("/llenar/{id}", name="hcue_paciente_paciente_llenar", defaults={"id" = 0} )
     * @param integer $id Id del paciente
     * @return json
     */
    public function llenarAction($id)
    {
        $result = array();
        try {
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $result = $pacienteManager->getArrayPaciente($id);
        } catch (\Exception $exc) {
            $this->get('core.app.logger')->addRecord($exc);
        }
        return new JsonResponse($result);
    }

    /**
     * Muestra la vista de los datos del representante
     * @Route("/addrepresentante/{id}/{modo}",name="hcue_paciente_paciente_addrepresentante",defaults={"id"=0,"modo"=0}, options={"expose"=true})
     * @param integer $id Id del Paciente
     * @param integer $modo modo de la vista
     * @Template("HCUEPacienteBundle:Paciente:divRepresentante.html.twig")
     * @return text/html HCUEPacienteBundle:Paciente:divRepresentante.html.twig
     * */
    public function addRepresentanteAction($id, $modo)
    {
        $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
        $disabled = ($modo == "CONSULTAR") ? true : false;
        $options = array("modo" => $modo, "disabled" => $disabled);
        $form = $pacienteManager->createForm($id, $options);
        $formView = $form->createView();
        return array(
            "form" => $formView,
        );
    }

    /** Imprime el formulario del paciente
     * @Route("/print/{id}", name="hcue_paciente_paciente_print", defaults={"id" = 0}, options={"expose"=true})
     * @Method("GET")
     * @param integer $id
     * @return application/pdf
     */
    public function printAction($id)
    {
        $pdf = $html = "";
        try {
            $formView = $paciente = "";
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $servicePaciente = $this->get("hcue.paciente.service.paciente");
            $securityToken = $this->get('core.seguridad.service.security_token');
            $entidad = $securityToken->getEntidad();
            $usuario = $securityToken->getPersona();
            $paciente = $pacienteManager->getPacienteById($id);
            $persona = $paciente->getPersona();
            $serviceUtildatetime = $this->get('core.app.util.datetime');
            $fechaNacimiento = $persona->getFechanacimiento();
            $edad = ($fechaNacimiento) ? $serviceUtildatetime->tiempoEntreFechas($fechaNacimiento->format('Y-m-d')) : '';
            if ($persona->getFotoId() > 0) {
                $managerArchivo = $this->get("core.app.entity.manager.archivo");
                $archivo = $managerArchivo->getArchivoByIdentificacion($paciente->getPersona()->getNumeroidentificacion());
                if ($archivo) {
                    $base64Foto = $archivo->getArchivo();
                }
            } else {
                $base64Foto = $servicePaciente->getImagebase64('fotodefault.png', true);
            }

            $html = $this->renderView(
                'HCUEPacienteBundle:Paciente:print.html.twig',
                array(
                    'paciente' => $paciente,
                    'edad' => $edad,
                    'usuario' => $usuario,
                    'entidad' => $entidad,
                    'base64Foto' => $base64Foto
                )
            );

        } catch (\Exception $exc) {
            $this->get('core.app.logger')->addRecord($exc);
            $html = ($exc->getCode() >= 0)
                ? "Se ha producido un error en el sistema, no es posible mostrar el formulario 001."
                : $exc->getMessage();
        }

        $header = $this->renderView('CoreGUIBundle:pdf:header.html.twig');

        $footer = $this->renderView('CoreGUIBundle:pdf:footer.html.twig', array(
            'footer_left' => 'MSP / HCU-form.001 / 2017',
            'footer_right' => 'ADMISIÓN'
        ));

        $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html, [
            'header-html' => $header,
            'footer-html' => $footer,
            'header-spacing' => '3',
            'footer-spacing' => '3',
            'margin-top' => 20,
            'margin-right' => 10,
            'margin-left' => 10,
            'margin-bottom' => 18,
            'enable-javascript' => true,
            'images' => true
        ]);

        return new Response($pdf,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="MSP_HCU-form.001_2017.pdf"'
            )
        );
    }

    /**
     * Obtiene la vista del grid (datatable) de los pacientes
     * @Route("/filteravanzada",name="hcue_paciente_paciente_filteravanzada")
     * @Template()
     * @return text/html HCUEAtencionMedicBundle:Paciente:index.html.twig
     * */
    public function filterAvanzadaAction()
    {
        $catalogoManager = $this->get('core.app.entity.manager.catalogo');
        $tipoIdentificacion = $catalogoManager->getCatalogo(Constantes::TC_TIPOIDENTIFICACION);

        return ["tipoidentificacion"=>$tipoIdentificacion];
    }

    private function validateSameRepresentante($paciente,$request){
        $result=false;
        $objmadrepcte=$paciente->getMadrepcte();
        $identipaciente=trim($objmadrepcte->getPersona()->getNumeroidentificacion());
        $numeroidentificacion=trim($request->get('paciente')['madrepcte']['persona']['numeroidentificacion']);
        if($numeroidentificacion==$identipaciente){
            $result=true;
        }
        return $result;

    }

    /**
     * Controlador para generar el numero de tipo No Identificado
     * @Route("/buscarnoidentificado/{tipo_iden}/{primer_ape}/{segundo_ape}/{primer_nom}/{segundo_nom}/{fecha_nac}/{pais_nac}/{prov_nac}", name="hcue_paciente_paciente_buscarnoidentificado", defaults={"tipo_iden"=0, "primer_ape"="", "segundo_ape"="0", "primer_nom"="", "segundo_nom"="0", "fecha_nac"="", "pais_nac"="", "prov_nac"="0"})
     * @param integer $tipo_iden tipo de identificación
     * @param string $primer_ape primer apellido
     * @param string $segundo_ape segundo apellido
     * @param string $primer_nom primer nombre
     * @param string $segundo_nom segundo nombre
     * @param string $fecha_nac fecha de nacimiento
     * @param string $pais_nac pais de nacimiento
     * @param string $prov_nac provincia de nacimiento
     * @return json
     */
    public function buscarNoIdentificado($tipo_iden, $primer_ape, $segundo_ape, $primer_nom, $segundo_nom, $fecha_nac, $pais_nac, $prov_nac)
    {
        $madrecpte = "";
        try {
            $secuencial = $this->generaNoIdentificado($tipo_iden, $primer_ape, $segundo_ape, $primer_nom, $segundo_nom, $fecha_nac, $pais_nac, $prov_nac);
            if ($secuencial != "") {
                $managerPersona = $this->get("core.app.entity.manager.persona");
                $managerPaciente = $this->get("hcue.paciente.entity.manager.paciente");
                $persona = $managerPersona->getPersonaByIdentificacion($secuencial);

                if ($persona) {
                    $managerPais = $this->get("core.app.entity.manager.pais");

                    $madrecpte = array(
                        'id' => $persona->getId(),
                        'numeroidentificacion' => $persona->getNumeroidentificacion(),
                        'nombrecompleto' => $persona->getNombrecompleto(),
                        'fechanacimiento' => $persona->getFechanacimiento()->format('d-m-Y'),
                        'nacionalidad' => $managerPais->find($persona->getPaisId())->getNacionalidad(),
                        'ispaciente' => $managerPaciente->findOneBy(array('persona_id'=>$persona->getId(),'activo'=>Constantes::CT_ESTADOGENERALACTIVO,'estado'=>Constantes::CT_ESTADOGENERALACTIVO))
                    );
                }
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
        }
        return new JsonResponse($madrecpte);
    }

    public function generaNoIdentificado($tipo_iden, $primer_ape, $segundo_ape, $primer_nom, $segundo_nom, $fecha_nac, $pais_nac, $prov_nac)
    {
        $secuencial = "";

        try {
            if ($tipo_iden == Constantes::CT_TIPOIDENTNOIDENTIFICADO) {
                $managerPais = $this->get("core.app.entity.manager.pais");
                $pais = $managerPais->find($pais_nac);

                $managerPersona = $this->get("core.app.entity.manager.persona");
                $persona = $managerPersona->create();
                $persona->setCttipoidentificacionId($tipo_iden);
                $persona->setApellidopaterno($primer_ape);
                $persona->setApellidomaterno($segundo_ape=="0" ? null : $segundo_ape);
                $persona->setPrimernombre($primer_nom);
                $persona->setSegundonombre($segundo_nom=="0" ? null : $segundo_nom);
                $persona->setFechanacimiento($fecha_nac);
                $persona->setPais($pais);

                if ($pais_nac == Constantes::CT_NACIONALIDAD_ECUATORIANA) {
                    $persona->setProvinciaId($prov_nac);
                }

                $servicePersona = $this->container->get("hcue.paciente.service.persona");
                $secuencial = $servicePersona->GenerarSecuencial($persona);
            }

        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
        }

        return $secuencial;
    }

    /**
     * Ingreso al formulario de vacunacion a partir de id de paciente
     * @Route("/public/aceptacionvacuna/",name="hcue_paciente_aceptacion_vacunacion_covid")
     * @Template()
     */
    public function publicaAceptacionvacunaAction()
    {
        $aceptavacunaManager=$this->get("hcue.paciente.entity.manager.aceptavacuna");
        $formView = "";
        $html = "";
        try {
            $form = $aceptavacunaManager->createForm();
            $formView = $form->createView();
            $html = '<label class="control-label required" for="aceptavacuna_entidad">Entidad donde desea vacunarse<i style="color:red" class="asterisk">*</i></label>';
            $html .='<br><select id="aceptavacuna_entidad" name="aceptavacuna[entidad]" required="required" class="form-control  form-control" style="width:100%">';
            $entidadManager = $this->get("core.app.entity.manager.entidad");
            $arrayEntidades = array(101,104,231,38666,944,941,41116,1781,1785
            ,1638,1793,1805,3463,1779,940);
            $datos = $entidadManager->getEntidadByArray($arrayEntidades);
            if($datos){
                $html .= '<option value="" selected="selected">--Seleccione--</option>';
                foreach ($datos as $data){
                    $nombre = $data->getParroquia()->getCanton()->getProvincia()->getDescripcion().' / '.$data->getParroquia()->getCanton()->getDescripcion().' / '.$data->getParroquia()->getDescripcion().' / '.$data->getNombreoficial();
                    $html .= '<option value="'.$data->getId().'">'.$nombre.'</option>';
                }
                $html .= '</select>';
            }
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible mostrar la interfaz." : $ex->getMessage();
            $this->get("session")->getFlashBag()->add("danger", $message);
        }
        return $this->render('HCUEPacienteBundle:Default:aceptvacunacovid.html.twig',array( "form" => $formView, "selection" =>$html));

    }
    /**
     * Guarda los datos de un formulario basado en un modelo aceptavacuna
     * @Route("/public/saveaceptavacuna",name="hcue_paciente_paciente_save_acepta_vacuna")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse  {"status","message"}
     * */
    public function saveaceptavacunaAction(Request $request)
    {
        $status = false;
        try {
            $aceptavacunaManager = $this->get("hcue.paciente.entity.manager.aceptavacuna");
            $pacienteCovidManager = $this->get("hcue.paciente.entity.manager.pacientecovid");
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $respuesta=$aceptavacunaManager->saveAceptavacuna($request,$aceptavacunaManager,$pacienteCovidManager);
            $status = $respuesta['status'];
            $message = $respuesta['message'];
            if($status==true){
                $result= $pacienteManager->savePersonaPacienteCovid($request->get('aceptavacuna'));
                $status = $result['status'];
                if($status == true){
                    $message = 'Su registro de aceptación y su creación de paciente en el sistema se realizó de manera exitosa';
                }else{
                    $status = false;
                    $message = 'No se ha podido crear el paciente  por favor verifique';
                }
            }

        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $status = false;
            $message = ($ex->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $ex->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));
    }
    /** envia el correo con el certificado
     * @Route("/public/sendNotificacion", name="hcue_paciente_paciente_send_notificacion")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse {"status","message"}
     */
    public function sendNotificacionAction(Request $request)
    {
        try {
            $datos = array('correo'=>$request->get('correo'),'nombre'=>$request->get('nombre'));
            $html = $this->renderView('HCUEPacienteBundle:Default:mailerNotificacionAceptVacuna.html.twig', array('datos'=>$datos));
            $mailerService=  $this->get('hcue.paciente.mailer');
            $mailerService->sendNotificacionAceptacion($html,$datos);
            return new JsonResponse(array('data'=>'','status'>true));
        } catch (\Exception $exc) {
            $this->get('core.app.logger')->addRecord($exc);
            $message = ($exc->getCode() >= 0)
                ? "Se ha producido un error en el sistema, no es posible enviar la notificacion."
                : $exc->getMessage();
            return new Response(
                $message,
                Response::HTTP_BAD_REQUEST,
                array('content-type' => 'text/plain')
            );
        }
    }

    /**
     * Guarda los datos de un formulario basado en un modelo aceptavacuna
     * @Route("/public/validapacienteacept",name="hcue_paciente_paciente_valida_paciente_acept")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse  {"status","message"}
     * */
    public function validapacienteaceptAction(Request $request)
    {
        $status = false;
        $message = "Se ha producido un error en el sistema, no es posible validar la identificación";
        $correo = "";
        try {
            $aceptavacunaManager = $this->get("hcue.paciente.entity.manager.aceptavacuna");
            $pacienteCovidManager = $this->get("hcue.paciente.entity.manager.pacientecovid");
            $respuesta=$aceptavacunaManager->validRegistro($request,$aceptavacunaManager,$pacienteCovidManager);
            $status = $respuesta['status'];
            $message = $respuesta['message'];
            $correo = $respuesta['correo'];
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible validar la identificación." : $ex->getMessage();
            $status = false;
        }

        return new JsonResponse(array("status" => $status, "message" => $message,"correo" => $correo));
    }

    /**
     * Ingreso al formulario de vacunacion a partir de id de paciente
     * @Route("/public/consultaAgenda/",name="hcue_paciente_consulta_agenda_vacuna")
     * @Template()
     */
    public function publicaconsultaAgendaAction()
    {
        return $this->render('HCUEPacienteBundle:Default:consultagenda.html.twig');
    }

    /**
     * Guarda los datos de un formulario basado en un modelo aceptavacuna
     * @Route("/public/consultaAgengapaciente",name="hcue_paciente_consulta_agenda_paciente")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse  {"status","message"}
     * */
    public function consultaAgengavacunaAction(Request $request)
    {
        $status = false;
        $html ='';
        try {

            $agendaVacunaManager = $this->get("hcue.paciente.entity.manager.agendavacuna");
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $pacientecovidManager = $this->get("hcue.paciente.entity.manager.pacientecovid");
            $respuesta=$agendaVacunaManager->consultaAgenda($request,$pacienteManager,$pacientecovidManager);
            $status = $respuesta['status'];
            if($status==true){
                $data = $respuesta['data'];
                $html .='<div class="row">';
                    $html .='<div class="col-sm-6">';
                        $html .= '<label><b>Nombre: </b> '.$data->getNombre().'</label>';
                    $html .='</div>';
                    $html .='<div class="col-sm-6">';
                        $html .= '<label><b>Denominación: </b> '.$data->getDenominacion().'</label>';
                    $html .='</div>';
                $html .='</div>';

                $html .='<div class="row">';
                    $html .='<div class="col-sm-6">';
                        $html .= '<label><b>Punto vacunación: </b> '.$data->getCrp().'</label>';
                    $html .='</div>';
                    $html .='<div class="col-sm-6">';
                        $html .= '<label><b>Ciudad: </b> '.$data->getCiudad().'</label>';
                    $html .='</div>';
                $html .='</div>';
                $html .='<div class="row">';
                    $html .='<div class="col-sm-6">';
                        $html .= '<label><b>Fecha: </b> '.$data->getFecha().'</label>';
                    $html .='</div>';
                    $html .='<div class="col-sm-6">';
                        $html .= '<label><b>Hora: </b> '.$data->getHora().'</label>';
                    $html .='</div>';
                $html .='</div>';
                $html .= '<div class="row">';
                    $html .= '<div class="col-sm-6">';
                        $html .= '<label><b>Vacunador: </b> ' . $data->getCodvacunador() . '</label>';
                    $html .= '</div>';
                $html .= '</div>';
                $html .= '<div class="row">';
                    $html .='<div class="col-sm-8 form-group">';
                        $html .= '<label class="control-label required" for="correo" aria-required="true"><b>¿Desea actualizar su correo para notificaciones?</b><i style="color:red" class="asterisk">*</i></label><br>';
                        $html .=' <div class="input-group">';
                        $html .='<input type="text" id="correo" name="correo" required="required" class="form-control lower email form-control" placeholder="" style="width:100%" aria-required="true" value="'.$respuesta['correo'].'">';
                        $html .= '<span class="input-group-btn" id="actualizacCorreo">
                                    <button class="btn btn-sm btn-primary input-sm" type="button" onclick="actualizaCorreo();" >
                                        <i class="fa fa-refresh bigger-110 "> Actualizar correo</i>
                                    </button>
                                </span>
                            </div>';
                    $html .='</div>';
                $html .='</div>';
            }
            $message = $respuesta['message'];
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $status = false;
            $message = ($ex->getCode() >= 0) ? "Se ha producido un error en el sistema, no se puede consultar la información." : $ex->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message,'html' =>$html));
    }

    /**
     * Guarda los datos de un formulario basado en un modelo aceptavacuna
     * @Route("/public/actualizacorreopaciente",name="hcue_paciente_actualiza_correo")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse  {"status","message"}
     * */
    public function actualizacorreopacienteAction(Request $request)
    {
        $status = false;
        try {
            $pacienteManager = $this->get("hcue.paciente.entity.manager.paciente");
            $result = $pacienteManager->actualizacorreoPaciente($request);
            $status = $result['status'];
            $message = $result['message'];
        } catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $status = false;
            $message = ($ex->getCode() >= 0) ? "Se ha producido un error en el sistema, no se puede consultar la información." : $ex->getMessage();
        }

        return new JsonResponse(array("status" => $status, "message" => $message));


    }
}
<?php 
/*
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Jipson Montalbán
*/

namespace HCUE\PacienteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use HCUE\PacienteBundle\Entity\Dbvacuancion;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use HCUE\PacienteBundle\Entity\Constantes;


class DbvacunacionController extends Controller {
 
        
    /**
     * Retorna la vista de la lista de datos datatables tipo Certificadovacuna
     * @Route("/",name="hcue_paciente_certificadovacuna_public_certificadomsp")
     * @Method({"GET"})
     * @Template()
     * @return text/html HCUEPacienteBundle:Dbvacunacion:Public:certificadomsp.html.twig
     **/

    public function certificadomspAction()
    {
        
    }
        /**        
        * Retorna la lista de datos tipo  Dbvacunacion para el datatables    
        * @Route("/result",name="hcue_paciente_db_vacunacion_result")
        * @Method({"GET"})    
        * @param Request $request  
        * @return JsonResponse
        **/
        public function resultAction(Request $request){
                if ($request->isXmlHttpRequest()) {
                   try{
                      $datatable = $this->get("hcue.paciente.datatable.dbvacunacion");                                                         
                      return $datatable->getResponse();  
                   } catch (\Exception $exc) {                           
                       $this->get("core.app.logger")->addRecord($exc);
                       $message = ($exc->getCode()>=0)?"Se ha producido un error en el sistema, no es posible mostrar la información.":$exc->getMessage();
                       return new Response($message, Response::HTTP_BAD_REQUEST, array("content-type" => "text/plain"));
                   }
                }
                return new Response("Solicitud incorrecta.", 400);
        }
                 
        /**
         * Verificar paciente en tabla dbvacunacion
         * @Route("/tomadatopacientemsp",name="hcue_paciente_public_datospacientemsp")
         * @Method({"GET","POST"})
         * @param Request $request
         * @return Array
         **/
        public function consultadatoPacientemsp(Request $request)
        {
            try{
                $dbcontadorcertificadoManager=$this->get("hcue.paciente.manager.Dbcontadorcertificado");
                $google_service = $this->get('hcue.paciente.service.recaptcha');
                $googleToken=$request->get('h-captcha-response');
                $resposeToken=$google_service->validarTokenGoogle($googleToken);
                if($resposeToken["success"]){
                    $dbvacunacionManager=$this->get("hcue.paciente.manager.dbvacunacion");
                    $paciente=$dbvacunacionManager->getPacienteVacunacionVacunometro($request->get('form')['identificacion'],$request->get('form')['fechanacimiento']);
                    $cont=0;
                    $dataVacuna=array();
                    $dataPaciente=array();
                    $fecha_nacimiento='';
                    $idencrypt='';
                    if($paciente){
                            foreach($paciente as $vacunaPaciente){
                                $dataVacuna[$cont]['nomvacuna']=$vacunaPaciente['NOM_VACUNA'];
                                $dataVacuna[$cont]['dosisaplicada']=$vacunaPaciente['DOSIS_APLICADA'];
                                $dataVacuna[$cont]['fechavacuna']= date_format(date_create($vacunaPaciente['FECHA_VACUNA']),'Y-m-d');
                                $cont++;
                                $idencrypt= str_replace('/','@@@',openssl_encrypt($vacunaPaciente['NUM_IDEN'], Constantes::CT_METODO_ENCRIPT, Constantes::CT_CLAVE_DECODE, false, base64_decode(Constantes::CT_BASE_DECODE)));
                                $fechanaciminto=date_create($vacunaPaciente['ANIO_NACIMIENTO'].'-'.$vacunaPaciente['MES_NACIMIENTO'].'-'.$vacunaPaciente['DIA_NACIMIENTO']);
                                $dataPaciente[0]['fechanacimiento']=date_format($fechanaciminto,'Y-m-d');
                                $dataPaciente[0]['nombres']=$vacunaPaciente['APELLIDOS_NOMBRES'];
                                $dataPaciente[0]['idencrypt']=$idencrypt;
                                $dataPaciente[0]['iden']=$vacunaPaciente['NUM_IDEN'];
                            }
                        $contadorArchivo=$dbcontadorcertificadoManager->create();
                        $contadorArchivo->setNumidentificacion($vacunaPaciente['NUM_IDEN']);
                        if(isset($resposeToken["credit"])){
                            $contadorArchivo->setCredit($resposeToken["credit"]);
                        }else{
                            $contadorArchivo->setCredit(0);
                        }
                        $dbcontadorcertificadoManager->saveContadorcertificado($contadorArchivo);
                        if($dataVacuna){
                            $data = array("status"=>true,"message"=>"Paciente se encuentra vacunado","datavacuna"=>$dataVacuna, "datapersona"=>$dataPaciente);
                        }else{
                            $data = array("status"=>false,"message"=>"Certificado no encontrado, estamos trabajando en la carga de su información, vuelva a intentarlo en 24 horas");
                        }
                    }else{
                        $data = array("status"=>false,"message"=>"Certificado no encontrado, cédula o fecha de nacimiento ingresados de manera incorrecta");
                    }
                }else{
                    $contadorArchivo=$dbcontadorcertificadoManager->create();
                    $contadorArchivo->setNumidentificacion($request->get('form')['identificacion']);
                    if(!isset($resposeToken["credit"])){
                    $contadorArchivo->setCredit($resposeToken["credit"]);
                    }else{
                        $contadorArchivo->setCredit(0);
                    }
                    $dbcontadorcertificadoManager->saveContadorcertificado($contadorArchivo);
                    $data = array("status"=>false,"message"=>"Información de certificado no generado, vuelva a cargar la página");
                }
                return new JsonResponse(array("data"=>$data));
            }catch (\Exception $ex) {
                $this->get('core.app.logger')->addRecord($ex);
                $message = ($ex->getCode() >= 0)
                    ? "Se ha producido un error en el sistema, no es posible mostrar la información."
                    : $ex->getMessage();
                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }

        /**
         * Impresion de certificados de vacunacion de la base del vacunometro
         * @Route("/mostrarcertificadomsp/{cedula}",name="hcue_vacunacion_mostrarcertificadomsp",defaults={"cedula"=0,"fecha"=null} ,options={"expose"=true})
         * @Template()
         */
        public function publicaViewpdfcertificadomasivoAction($cedula)
        {
            try {
                $dbvacunacionManager=$this->get("hcue.paciente.manager.dbvacunacion");
                $cedulapaciente=str_replace('@@@','/',$cedula);
                $cedulapaciente = openssl_decrypt($cedulapaciente, Constantes::CT_METODO_ENCRIPT, Constantes::CT_CLAVE_DECODE, false, base64_decode(Constantes::CT_BASE_DECODE));
                $datos = $dbvacunacionManager->getPacienteVacunacionVacunometroBycedula($cedulapaciente);
                $dataVacuna=array();
                $dataPaciente=array();
                $inmonudeprimido=false;
                $data=array();
                $cont=0;
                $i=0;
                $fechaEmision=date('Y-m-d H:i:s');
                if($datos){
                    foreach($datos as $vacunaPaciente){
                        $dataVacuna[$i]['nomvacuna']=$vacunaPaciente['NOM_VACUNA'];
                        $dataVacuna[$i]['dosisaplicada']=$vacunaPaciente['DOSIS_APLICADA'];
                        $dataVacuna[$i]['fechavacuna']=$vacunaPaciente['FECHA_VACUNA'];
                        //$dataVacuna[$i]['fechavacuna']=$vacunaPaciente['FECHA_VACUNA']->format("Y-m-d");
                        $dataVacuna[$i]['lote']=$vacunaPaciente['LOTE_VACUNA'];
                        if($vacunaPaciente['DOSIS_APLICADA'] !=999 && $vacunaPaciente['DOSIS_APLICADA'] !=4){
                            $cont++;
                        }
                        if($vacunaPaciente['NOM_VACUNA']=='BNT162b2 PFIZER'){
                            $dataPaciente[0]['medicinavacuna']='Vacuna de ARNm frente a COVID-19 (con nucleósidos modificados)';
                        }else if($vacunaPaciente['NOM_VACUNA']=='CoronaVac SINOVAC'){
                            $dataPaciente[0]['medicinavacuna']='COVID-19 Vaccine (Vero Cell), Inactivated.';
                        }else if($vacunaPaciente['NOM_VACUNA']=='CHADOX1S RECOMBINANTE ASTRAZENECA'){
                            $dataPaciente[0]['medicinavacuna']='CHADOX1-S [recombinant] vaccine againt COVID-19';
                        }else if($vacunaPaciente['NOM_VACUNA']=='CANSINO'){
                            $dataPaciente[0]['medicinavacuna']='Recombinante de vector de Adenovirus tipo 5 (Convidecia)';
                        }else if($vacunaPaciente['NOM_VACUNA']=='ZHIFIVAX'){
                            $dataPaciente[0]['medicinavacuna']='Vacuna recombinante (CHO) para la prevención del COVID 19';
                        }else {
                            $dataPaciente[0]['medicinavacuna']='N/A';
                        }
                        if($vacunaPaciente['DOSIS_APLICADA'] ==9){
                            $inmonudeprimido=true;
                        }
                        $idencrypt= str_replace('/','@@@',openssl_encrypt($vacunaPaciente['NUM_IDEN'], Constantes::CT_METODO_ENCRIPT, Constantes::CT_CLAVE_DECODE, false, base64_decode(Constantes::CT_BASE_DECODE)));
                        $fechanaciminto=date_create($vacunaPaciente['FECHANACIMIENTO']);
                        $dataPaciente[0]['fechanacimiento']=date_format($fechanaciminto,'Y-m-d');
                        $dataPaciente[0]['nombres']=$vacunaPaciente['APELLIDOS_NOMBRES'];
                        $dataPaciente[0]['identificacion']=$vacunaPaciente['NUM_IDEN'];
                        $dataPaciente[0]['numdosisaplicadas']=$cont;
                        $dataPaciente[0]['fechaemitida']=$fechaEmision;
                        $dataPaciente[0]['identeificacioncifrada']=$idencrypt;
                        $i++;
                    }
                }
                if($dataVacuna){

                    $html = $this->renderView('HCUEPacienteBundle:Dbvacunacion:print_certificado.html.twig', array('datavacuna'=>$dataVacuna,'datapaciente'=>$dataPaciente,'inmonudeprimido'=>$inmonudeprimido));

                    $header = $this->renderView('CoreGUIBundle:pdf:headercertvacunas.html.twig');

                    $footer = $this->renderView('CoreGUIBundle:pdf:footercertificado.html.twig');
                    $this->get('knp_snappy.pdf')->getInternalGenerator()->setTimeout(300);
                    return new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml($html, [
                            'header-html' => $header,
                            'footer-html' => $footer,
                            'header-spacing' => '3',
                            'footer-spacing' => '3',
                            'page-size' => "A4",
                            'margin-top' => 20,
                            'margin-right' => 10,
                            'margin-left' => 10,
                            'margin-bottom' => 18,
                            'enable-javascript' => true,
                            'images' => true
                        ],'certificado_covid'.'-'.$cedulapaciente),
                        200,
                        array(
                            'Content-Type' => 'application/pdf',
                            'Content-Disposition' => 'inline; filename=MSP_HCU_certificadovacunacion'.$cedulapaciente.'.pdf'
                        )
                    );
                }
                else{
                    return new Response(
                        "Usted no tiene vacunas para el día indicado",
                        Response::HTTP_BAD_REQUEST,
                        array('content-type' => 'text/plain')
                    );
                }

            } catch (\Exception $exc) {
                $this->get('core.app.logger')->addRecord($exc);
                $message = ($exc->getCode() >= 0)
                    ? "Se ha producido un error en el sistema, no es posible cargar el certificado."
                    : $exc->getMessage();
                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }

        /**
         * Impresion de certificados de vacunacion de la base del vacunometro
         * @Route("/consultarqr/{cedula}",name="hcue_vacunacion_consultarqr",defaults={"cedula"=0} ,options={"expose"=true})
         * @Template()
         */
        public function publicaConstaqrAction($cedula)
        {
            try {
                $dbvacunacionManager=$this->get("hcue.paciente.manager.dbvacunacion");
                $cedulapaciente=str_replace('@@@','/',$cedula);
                $cedulapaciente = openssl_decrypt($cedulapaciente, Constantes::CT_METODO_ENCRIPT, Constantes::CT_CLAVE_DECODE, false, base64_decode(Constantes::CT_BASE_DECODE));
                $datos = $dbvacunacionManager->getPacienteVacunacionVacunometroBycedula($cedulapaciente);
                $dataVacuna=array();
                $dataPaciente=array();
                $inmonudeprimido=false;
                $data=array();
                $cont=0;
                $i=0;
                $fechaEmision=date('Y-m-d H:i:s');
                if($datos){
                    foreach($datos as $vacunaPaciente){
                        $dataVacuna[$i]['nomvacuna']=$vacunaPaciente['NOM_VACUNA'];
                        $dataVacuna[$i]['dosisaplicada']=$vacunaPaciente['DOSIS_APLICADA'];
                        $dataVacuna[$i]['fechavacuna']=$vacunaPaciente['FECHA_VACUNA'];
                        //$dataVacuna[$i]['fechavacuna']=$vacunaPaciente['FECHA_VACUNA']->format("Y-m-d");
                        $dataVacuna[$i]['lote']=$vacunaPaciente['LOTE_VACUNA'];
                        if($vacunaPaciente['DOSIS_APLICADA'] !=999 && $vacunaPaciente['DOSIS_APLICADA'] !=4){
                            $cont++;
                        }
                        if($vacunaPaciente['NOM_VACUNA']=='BNT162b2 PFIZER'){
                            $dataPaciente[0]['medicinavacuna']='Vacuna de ARNm frente a COVID-19 (con nucleósidos modificados)';
                        }else if($vacunaPaciente['NOM_VACUNA']=='CoronaVac SINOVAC'){
                            $dataPaciente[0]['medicinavacuna']='COVID-19 Vaccine (Vero Cell), Inactivated.';
                        }else if($vacunaPaciente['NOM_VACUNA']=='CHADOX1S RECOMBINANTE ASTRAZENECA'){
                            $dataPaciente[0]['medicinavacuna']='CHADOX1-S [recombinant] vaccine againt COVID-19';
                        }else if($vacunaPaciente['NOM_VACUNA']=='CANSINO'){
                            $dataPaciente[0]['medicinavacuna']='Recombinante de vector de Adenovirus tipo 5 (Convidecia)';
                        }else if($vacunaPaciente['NOM_VACUNA']=='ZHIFIVAX'){
                            $dataPaciente[0]['medicinavacuna']='Vacuna recombinante (CHO) para la prevención del COVID 19';
                        }else {
                            $dataPaciente[0]['medicinavacuna']='N/A';
                        }
                        if($vacunaPaciente['DOSIS_APLICADA']==9){
                            $inmonudeprimido=true;
                        }
                        $idencrypt= str_replace('/','@@@',openssl_encrypt($vacunaPaciente['NUM_IDEN'], Constantes::CT_METODO_ENCRIPT, Constantes::CT_CLAVE_DECODE, false, base64_decode(Constantes::CT_BASE_DECODE)));
                        $fechanaciminto=date_create($vacunaPaciente['FECHANACIMIENTO']);
                        $dataPaciente[0]['fechanacimiento']=date_format($fechanaciminto,'Y-m-d');
                        $dataPaciente[0]['nombres']=$vacunaPaciente['APELLIDOS_NOMBRES'];
                        $dataPaciente[0]['identificacion']=$vacunaPaciente['NUM_IDEN'];
                        $dataPaciente[0]['numdosisaplicadas']=$cont;
                        $dataPaciente[0]['fechaemitida']=$fechaEmision;
                        $dataPaciente[0]['identeificacioncifrada']=$idencrypt;
                        $i++;
                    }
                }
                if($dataVacuna){
                return $this->render('HCUEPacienteBundle:Dbvacunacion:viewqr_certificado.html.twig',array('datavacuna'=>$dataVacuna,'datapaciente'=>$dataPaciente,'inmonudeprimido'=>$inmonudeprimido));
                }
                else{
                    return new Response(
                        "Usted no tiene vacunas para el día indicado",
                        Response::HTTP_BAD_REQUEST,
                        array('content-type' => 'text/plain')
                    );
                }

            } catch (\Exception $exc) {
                $this->get('core.app.logger')->addRecord($exc);
                $message = ($exc->getCode() >= 0)
                    ? "Se ha producido un error en el sistema, no es posible cargar el certificado."
                    : $exc->getMessage();
                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }
        
}
 ?>
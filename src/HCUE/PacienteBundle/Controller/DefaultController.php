<?php

namespace HCUE\PacienteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * Obtiene la vista del dashboard del app regitro de pacientes
     * @Route("/{menurol_id}",name="hcue_paciente_default_index",requirements={"menurol_id" = "\d+"},defaults={"menurol_id"=0})
     * @param insteger $menurol_id Id del menu rol
     * @return text/html
     * */
    public function indexAction($menurol_id = 0)
    {
//        $datapaciente = array();
        $provincia = $canton = $parroquia = "";
        $security_service = $this->get('core.seguridad.service.security_token');
//        $managerPaciente = $this->get("hcue.paciente.entity.manager.paciente");
//        $managerArchivo = $this->get("hcue.paciente.entity.manager.archivo");
//        $managerInscripcion = $this->get("hcue.paciente.entity.manager.inscripcion");
        $entidad = $security_service->getEntidad();
//        $entidad_id = $entidad->getId();
        if ($entidad->getParroquia()) {
            $objparroquia = $entidad->getParroquia();
            $objcanton = $objparroquia->getCanton();
            $parroquia = $objparroquia->getDescripcion();
            $canton = $objcanton->getDescripcion();
            $provincia = $objcanton->getProvincia()->getDescripcion();
        }
//        $conarchivo = $managerArchivo->getCountArchivo($entidad_id);
//        $sinarchivo = $managerArchivo->getCountArchivo($entidad_id, true);
//        $coninscripcion = $managerInscripcion->getCountInscripcion();
//        $sininscripcion = $managerInscripcion->getCountInscripcion(true);
//        $historyPaciente = $managerPaciente->getHistoryPaciente();
//        $color = array("#89A54E", "#4572A7", "#7ac70c", "#AA4643", "#1cb0f6");
//        foreach ($historyPaciente as $k => $history) {
//            $datapaciente[$k]["data"][] = array(
//                $history["DESCRIPCION"],
//                $history["CANTIDAD"]
//            );
//            $datapaciente[$k]["color"] = array_key_exists($k, $color)
//                ? $color[$k] : $color[0];
//        }
        return $this->render(
            'HCUEPacienteBundle:Default:index.html.twig',
            array(
                "entidad"        => $entidad,
                "provincia"      => $provincia,
                "canton"         => $canton,
                "parroquia"      => $parroquia,
//                "datapaciente"   => $datapaciente,
//                "sinarchivo"     => $sinarchivo,
//                "conarchivo"     => $conarchivo,
//                "coninscripcion" => $coninscripcion,
//                "sininscripcion" => $sininscripcion,
            )
        );
    }
}

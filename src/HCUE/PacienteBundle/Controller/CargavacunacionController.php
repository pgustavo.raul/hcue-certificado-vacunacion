<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Diego Orellana, Armando Guerrero
*/

namespace HCUE\PacienteBundle\Controller;

use Core\AppBundle\Entity\Persona;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use HCUE\PacienteBundle\Entity\Constantes;

class CargavacunacionController extends Controller {

        /**
        * Retorna la vista de la lista de datos datatables tipo Cargavacunacion
        * @Route("/{menurol_id}",name="hcue_paciente_cargavacunacion_index",requirements={"menurol_id" = "\d+"},defaults={"menurol_id"=0})
        * @Method({"GET"})
        * @Template()
        * @param int $menurol_id Id del menu del rol
        * @return text/html HCUEPacienteBundle:Cargavacunacion:index.html.twig
        **/
        public function indexAction($menurol_id){

                $service_security=$this->get("core.seguridad.service.security_authorization");
                $service_security->setActiveMenuRolId($menurol_id);
                $em = $this->getDoctrine()->getManager();
                $repoCatalogo = $em->getRepository("CoreAppBundle:Catalogo");
                $repoVacuna = $em->getRepository("HCUEAtencionMedicBundle:Vacuna");
                $vacunas = "";
                $riesgo = "";
                $tipo_ident = "";
                $sexo = "";
                //Verificacion de permiso de acceso
                if($service_security->hasPermission()){
                    $vacunas = $repoVacuna->getAllVacunas();
                    $riesgo = $repoCatalogo->getCatalogo(Constantes::TC_GRUPORIESGO_VACUNACION);
                    $tipo_ident = $repoCatalogo->getCatalogo(Constantes::TC_TIPOIDENTIFICACION);
                    $sexo = $repoCatalogo->getCatalogo(Constantes::TC_SEXO);
                    return $this->render("HCUEPacienteBundle:Cargavacunacion:index.html.twig",array('vacunas'=>$vacunas,'riesgos'=>$riesgo, 'tipo_idents'=>$tipo_ident,'sexos'=>$sexo));
                }else{
                    $this->get("session")->getFlashBag()->add(
                       "danger", "Acceso denegado !"
                    );
                }
        }

        /**
        * Retorna la vista de un formulario tipo CargavacunacionType
        * @Route("/add",name="hcue_paciente_cargavacunacion_add")
        * @Method({"GET","POST"})
        * @Template()
        * @return text/html HCUEPacienteBundle:Cargavacunacion:add.html.twig
        **/
        public function addAction(){
                $formView = "";
                try {
                    //verificamos permisos del usuario
                    $service_security=$this->get("core.seguridad.service.security_authorization");
                    if($service_security->canAdd()){
                        $cargavacunacionManager=  $this->get("hcue.paciente.manager.cargavacunacion");
                        die("viene");

                    }else{
                        throw new \Exception("Acceso denegado");
                    }
                } catch (\Exception $exc) {

                    $this->get("core.app.logger")->addRecord($exc);
                    $message = ($exc->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible mostrar la interfaz." : $exc->getMessage();
                    $this->get("session")->getFlashBag()->add(
                            "danger", $message
                    );
                }
                return array(
                     "form" => $formView
                );
        }

        /**
        * Guarda los datos de un formulario basado en un modelo Cargavacunacion
        * @Route("/save",name="hcue_paciente_cargavacunacion_save")
        * @Method("POST")
        * @Template()
        * @param Request $request
        **/
        public function saveAction(Request $request){
            try {
                $msjerror = "";
                $msjupload = "";
                $errorsfile = "";
                $arrayresult=array();
                $errorsdata=array();
                $uploadsdata=array();
                $registrosInsertados = 0;
                $cargavacunacionManager=  $this->get("hcue.paciente.manager.cargavacunacion");
                $padronninosService=$this->get("hcue.seguimiento.service.padronninos");
                $managerPaciente=$this->get("hcue.paciente.entity.manager.paciente");
                $managerPersona = $this->get("core.app.entity.manager.persona");
                $em = $this->getDoctrine()->getManager();
                $repoVacuna = $em->getRepository("HCUEAtencionMedicBundle:Vacuna");
                $repoEsquemaVacunacion = $em->getRepository("HCUEAtencionMedicBundle:Esquemavacunacion");
                $repoRegistroVacunacion = $em->getRepository("HCUEAtencionMedicBundle:Registrovacunacion");
                $repoEntidad = $em->getRepository("CoreAppBundle:Entidad");
                $managerCatalogo = $this->container->get("core.app.entity.manager.catalogo");
                $serviceRegistrocivil = $this->get("hcue.paciente.service.registrocivil");
                $security=$this->container->get('core.seguridad.service.security_token');
                $userId = $security->getUser()->getId();
                //Verficamos los permisos del usuario
                $service_security=$this->get("core.seguridad.service.security_authorization");
                if(($service_security->canAdd()) || ($service_security->canEdit())){
                    $file = $request->files->get('carga_vacuna_file');
                    $errorsfile=$padronninosService->verifyFileCsv($file);
                    if(!$errorsfile){
                        $delimiter=$this->getFileDelimiter($file);
                        $arraycsv=$this->getArrayCsv($file,$delimiter);
                        $arrayVacunas = $repoVacuna->getAllVacunas(1,true);
                        $arrayEntidades=$repoEntidad->getEntidadbyNivel(true);
                        $arrayEsquemas=$repoEsquemaVacunacion->getEsquemavacunacionQueryBuilder()->getQuery()->getArrayResult();
                        $validateArrayCsv=$cargavacunacionManager->validateArrayCsv($arraycsv, $arrayVacunas, $managerCatalogo, $arrayEntidades, $arrayEsquemas);
                        $errorsdata = $validateArrayCsv['errorsdata'];
                        $arraycsv = $validateArrayCsv['arraycsv'];

                        if(count($arraycsv)>Constantes::CT_REGISTROS_PERMITIDOS){
                            $errorsfile[]="Solo puede ingresar hasta " . strval(Constantes::CT_REGISTROS_PERMITIDOS - 1) . " pacientes por archivo";
                        }else{
                            if(!$errorsdata){
                                if(count($arraycsv)){
                                    $arrayIdentificacion  = array_column($arraycsv, 'num_iden');
                                    $arrayPersonas = $managerPersona->getPersonaByArrayIdentificacion($arrayIdentificacion);
                                    $arrayPersona_id  = array_column($arrayPersonas, 'id');
                                    $arrayPacientes = $managerPaciente->getPacienteByArray($arrayPersona_id, true);

                                    for($x=1;$x<count($arraycsv);$x++){
                                        $arraycsv[$x]['index'] = $x+1;
                                        $msjerror = "";
                                        $identificacion=$arraycsv[$x]['num_iden'];
                                        $auxPersona["correo"]=$arraycsv[$x]['correo_electronico'];
                                        $fechanacimiento = new \Datetime($arraycsv[$x]['año_nacimiento'] . str_pad($arraycsv[$x]['mes_nacimiento'],2,'0',STR_PAD_LEFT) . str_pad($arraycsv[$x]['dia_nacimiento'],2,'0',STR_PAD_LEFT));
                                        $sexo = $arraycsv[$x]['sexo'];
                                        $existePersona = array_search($identificacion, array_column($arrayPersonas, 'numeroidentificacion'));

                                        if (!($existePersona === false)){
                                            $arrayPersona = $arrayPersonas[$existePersona];
                                            $existePaciente = array_search($arrayPersona['id'], array_column($arrayPacientes, 'persona_id'));
                                            if($existePaciente === false){
                                                $persona = $managerPersona->getPersonaByIdentificacion($identificacion);
                                                if ($arraycsv[$x]['tipo_iden_id'] <> Constantes::CT_TIPOIDENTCEDULA){
                                                    if ($persona->getPais()){
                                                        $persona->setPais($em->getReference("CoreAppBundle:Pais",$persona->getPais()->getId()));
                                                    }
                                                    elseif ($arraycsv[$x]['nacionalidad_id']){
                                                        $persona->setPais($em->getReference("CoreAppBundle:Pais",$arraycsv[$x]['nacionalidad_id']));
                                                    }
                                                    else{
                                                        $msjerror .= ' No es posible generar el número de historia clínica, Nacionalidad es obligatoria,';
                                                    }
                                                    if ($persona->getParroquia()){
                                                        $persona->setProvinciaId($persona->getParroquia()->getCanton()->getProvincia()->getId());
                                                    }
                                                    elseif ($arraycsv[$x]['provincia_id']){
                                                        $persona->setProvinciaId($arraycsv[$x]['provincia_id']);
                                                    }
                                                    else{
                                                        $msjerror .= ' No es posible generar el número de historia clínica, Provincia es obligatoria,';
                                                    }
                                                }
                                                if ($msjerror == ''){
                                                    try {
                                                        $paciente = $managerPaciente->savePacienteCarga($persona, $auxPersona);
                                                        if(!$paciente){
                                                            $msjerror .= ' No se pudo crear el paciente,';
                                                        }
                                                        else{
                                                            $arrayPaciente = $cargavacunacionManager->renombraKeyArray($paciente);
                                                            array_push($arrayPacientes, $arrayPaciente);
                                                            $arraycsv[$x]['paciente_id'] = $arrayPaciente['id'];
                                                        }
                                                    } catch (\Exception $exc) {
                                                        $msjerror .= ' No es posible generar el número de historia clínica, por favor verifique nombres y apellidos,';
                                                    }
                                                }
                                            }
                                            else{
                                                $paciente_id = $arrayPacientes[$existePaciente]['id'];
                                                $arraycsv[$x]['paciente_id'] = $paciente_id;
                                            }
                                        }
                                        else{
                                            $objPersona = new Persona;

                                            if ($arraycsv[$x]['tipo_iden_id'] == Constantes::CT_TIPOIDENTCEDULA){
                                                $arrayPersona = $serviceRegistrocivil->consumir($identificacion);
                                                if ($arrayPersona['webservis']){
                                                    $objPersona->setApellidopaterno($arrayPersona['apellidopaterno']);
                                                    $objPersona->setApellidomaterno($arrayPersona['apellidomaterno']);
                                                    $objPersona->setPrimernombre($arrayPersona['primernombre']);
                                                    $objPersona->setSegundonombre($arrayPersona['segundonombre']);
                                                    $objPersona->setNombrecompleto($arrayPersona['nombrecompleto']);
                                                    $objPersona->setPais($em->getReference("CoreAppBundle:Pais",$arrayPersona['nacionalidad']));
                                                    $objPersona->setLugarnacimiento($arrayPersona['lugarnacimiento']);
                                                    $objPersona->setFechanacimiento(new \Datetime($arrayPersona['fechanacimiento']));
                                                    $objPersona->setCtsexo($em->getReference("CoreAppBundle:Catalogo",$arrayPersona['sexo']));
                                                    $objPersona->setCtestadocivil($em->getReference("CoreAppBundle:Catalogo",$arrayPersona['estadocivil']));
                                                    $objPersona->setNombrepadre($arrayPersona['nombrepadre']);
                                                    $objPersona->setNombremadre($arrayPersona['nombremadre']);
                                                    $objPersona->setCttipoidentificacion($em->getReference('CoreAppBundle:Catalogo', Constantes::CT_TIPOIDENTCEDULA));
                                                }
                                                else{
                                                    $msjerror .= ' No se pudo crear la persona, problemas al consultar con el registro civil,';
                                                }
                                            }
                                            else{
                                                $arrayApellidos = explode(" ", $arraycsv[$x]['apellidos']);
                                                $arrayNombres = explode(" ", $arraycsv[$x]['nombres']);
                                                $apellidopaterno = array_key_exists(0, $arrayApellidos) ? $arrayApellidos[0] : "";
                                                $apellidomaterno = array_key_exists(1, $arrayApellidos) ? $arrayApellidos[1] : "";
                                                $primernombre = array_key_exists(0, $arrayNombres) ? $arrayNombres[0] : "";
                                                $segundonombre = array_key_exists(1, $arrayNombres) ? $arrayNombres[1] : "";
                                                $nombrecompleto = $apellidopaterno . ' ' . $apellidomaterno . ' ' . $primernombre . ' ' . $segundonombre;
                                                $objPersona->setApellidopaterno($apellidopaterno);
                                                $objPersona->setApellidomaterno($apellidomaterno);
                                                $objPersona->setPrimernombre($primernombre);
                                                $objPersona->setSegundonombre($segundonombre);
                                                $objPersona->setNombrecompleto($nombrecompleto);
                                                $objPersona->setFechanacimiento($fechanacimiento);
                                                if ($arraycsv[$x]['provincia_id'] && $arraycsv[$x]['nacionalidad_id']){
                                                    $objPersona->setPais($em->getReference("CoreAppBundle:Pais",$arraycsv[$x]['nacionalidad_id']));
                                                    $objPersona->setProvinciaId($arraycsv[$x]['provincia_id']);
                                                    if ($arraycsv[$x]['tipo_iden_id'] == Constantes::CT_TIPOIDENTNOIDENTIFICADO){
                                                        try {
                                                            $servicePersona = $this->container->get("hcue.paciente.service.persona");
                                                            $identificacion = $servicePersona->GenerarSecuencial($objPersona);
                                                            $objPersona->setNoidentificado($identificacion);
                                                        } catch (\Exception $exc) {
                                                            $msjerror .= ' No es posible generar el número de historia clínica, por favor verifique nombres y apellidos,';
                                                        }
                                                    }
                                                }
                                                else{
                                                    $msjerror .= ' No es posible generar el número de historia clínica, Nacionalidad y Provincia son obligatorios,';
                                                }
                                                $objPersona->setCtsexo($em->getReference("CoreAppBundle:Catalogo",$arraycsv[$x]['sexo_id']));
                                                $objPersona->setCttipoidentificacion($em->getReference('CoreAppBundle:Catalogo', $arraycsv[$x]['tipo_iden_id']));
                                            }

                                            if ($msjerror == ''){
                                                $objPersona->setNumeroidentificacion($identificacion);
                                                $objPersona->setEstado(Constantes::CT_ESTADOGENERALACTIVO);
                                                $objPersona->setActivo(Constantes::CT_ESTADOGENERALACTIVO);

                                                $persona = (array) $managerPersona->savePersona($objPersona, true);
                                                $arrayPersona = $cargavacunacionManager->renombraKeyArray($persona);

                                                if(!$persona){
                                                    $msjerror .= ' No se pudo crear la persona,';
                                                }
                                                else{
                                                    $arraycsv[$x]['ctsexo_id'] = $arrayPersona['ctsexo']->getId();
                                                    $arrayPersona['ctsexo_id'] = $arraycsv[$x]['ctsexo_id'];
                                                    $paciente = $managerPaciente->savePacienteCarga($objPersona, $auxPersona);
                                                    if(!$paciente){
                                                        $msjerror .= ' No se pudo crear el paciente,';
                                                    }
                                                    else{
                                                        $arrayPaciente = $cargavacunacionManager->renombraKeyArray($paciente);
                                                        array_push($arrayPacientes, $arrayPaciente);
                                                        $arraycsv[$x]['paciente_id'] = $arrayPaciente['id'];
                                                    }
                                                }
                                            }
                                        }
                                        if($msjerror <> ''){
                                            $errorsdata[]=array("linea"=>$x+1,"identificacion"=>$identificacion,"error"=>$msjerror);
                                        }
                                    }
                                    if(!$errorsdata){
                                        $cabecera = $arraycsv[0];
                                        unset($arraycsv[0]);
                                        $dosis_aplicada  = array_column($arraycsv, 'dosis_aplicada');
                                        $año_aplicacion  = array_column($arraycsv, 'año_aplicacion');
                                        $mes_aplicacion  = array_column($arraycsv, 'mes_aplicacion');
                                        $dia_aplicacion  = array_column($arraycsv, 'dia_aplicacion');
                                        array_multisort($dosis_aplicada, SORT_ASC, $año_aplicacion, SORT_ASC, $mes_aplicacion, SORT_ASC, $dia_aplicacion, SORT_ASC, $arraycsv);
                                        array_unshift($arraycsv, $cabecera);
                                        $arrayPaciente_id = array_column($arraycsv, 'paciente_id');
                                        $registroVacunacion=$repoRegistroVacunacion->getVacunacionByArrayPacientes($arrayPaciente_id,true);
                                        $insertaVacuna = false;
                                        $registrosInsertados = 0;
                                        for($x=1;$x<count($arraycsv);$x++){
                                            $msjupload = "";
                                            $msjerror = "";
                                            $identificacion=$arraycsv[$x]['num_iden'];
                                            $paciente_id = $arraycsv[$x]['paciente_id'];
                                            $esquemavacunacion_id = $arraycsv[$x]['esquemavacunacion_id'];
                                            $vacunaObligatoria = $arraycsv[$x]['vacunaobligatoria'];
                                            $insertaVacuna = false;
                                            $exteriordosisprevia = $arraycsv[$x]['exteriordosisprevia'];
                                            if (count($registroVacunacion)){
                                                foreach ($registroVacunacion as $vacuna) {
                                                    if ($vacunaObligatoria <> null){
                                                        if ($vacuna['paciente_id'] == $paciente_id && $vacuna['esquemavacunacion_id'] == $esquemavacunacion_id){
                                                            $msjupload .= " Registro no insertado, Vacuna previamente registrada en PRAS.";
                                                            $insertaVacuna = false;
                                                            break;
                                                        }
                                                        elseif($vacuna['paciente_id'] == $paciente_id && !($vacuna['esquemavacunacion_id'] == $vacunaObligatoria)) {
                                                            $msjerror .= " Vacuna Inválida, no se ha registrado dosis previa.";
                                                            $insertaVacuna = false;
                                                            break;
                                                        }
                                                        elseif($vacuna['paciente_id'] == $paciente_id && $vacuna['esquemavacunacion_id'] == $vacunaObligatoria){
                                                            $insertaVacuna = true;
                                                            break;
                                                        }
                                                    }
                                                    else{
                                                        if ($vacuna['paciente_id'] == $paciente_id && $vacuna['esquemavacunacion_id'] == $esquemavacunacion_id){
                                                            $msjupload .= " Registro no insertado, Vacuna previamente registrada en PRAS.";
                                                            $insertaVacuna = false;
                                                            break;
                                                        }
                                                        elseif($vacuna['paciente_id'] == $paciente_id) {
                                                            $insertaVacuna = true;
                                                        }
                                                        else{
                                                            $insertaVacuna = true;
                                                        }
                                                    }
                                                }
                                            }
                                            else{
                                                if ($vacunaObligatoria == null || $exteriordosisprevia){
                                                    $insertaVacuna = true;
                                                }
                                            }
                                            if ($insertaVacuna && $vacunaObligatoria == null){
                                                $status = $cargavacunacionManager->saveVacunaCarga($arraycsv[$x], $userId);
                                                if ($status){
                                                    $msjupload .= " Vacuna registrada con éxito";
                                                    $registrosInsertados += 1;
                                                }
                                                else{
                                                    $msjerror .= " No se pudo registrar la vacuna.";
                                                }
                                            }
                                            elseif($insertaVacuna){
                                                $status = $cargavacunacionManager->saveVacunaCarga($arraycsv[$x], $userId);
                                                if ($status){
                                                    $msjupload .= " Vacuna registrada con éxito";
                                                    $registrosInsertados += 1;
                                                }
                                                else{
                                                    $msjerror .= " No se pudo registrar la vacuna.";
                                                }
                                            }
                                            elseif($msjupload == ""){
                                                $msjerror .= " Vacuna Inválida, no se ha registrado dosis previa.";
                                            }
                                            if($msjerror <> ''){
                                                $errorsdata[]=array("linea"=>$arraycsv[$x]['index'],"identificacion"=>$identificacion,"error"=>$msjerror);
                                            }
                                            $uploadsdata[]=array("linea"=>$arraycsv[$x]['index'], "identificacion"=>$identificacion,"mensaje"=>$msjupload);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    throw new \Exception("No tiene permiso para realizar esta operación");
                }
            } catch (\Exception $exc) {
                $this->get("core.app.logger")->addRecord($exc);
                $msjerror = ($exc->getCode())?"Se ha producido un error en el sistema, no es posible guardar el registro." : $exc->getMessage();
                $errorsdata[]=array("linea"=>'',"identificacion"=>'',"error"=>$msjerror);
            }
            if ($errorsdata){
                $sort  = array_column($errorsdata, 'linea');
                array_multisort($sort, SORT_ASC, $errorsdata);
            }
            if ($uploadsdata){
                $sort  = array_column($uploadsdata, 'linea');
                array_multisort($sort, SORT_ASC, $uploadsdata);
            }
            return array(
                "errorsfile"=>$errorsfile,
                "errorsdata"=>$errorsdata,
                "uploadsdata"=>$uploadsdata,
                "registrosinsertados"=>$registrosInsertados,
                "arrayresult"=>$arrayresult
            );
        }

    private function getFileDelimiter($file, $checkLines = 2){
        $file = new \SplFileObject($file);
        $delimiters = array(
            ',',
            //'\t',
            ';',
            '|',
            ':'
        );
        $results = array();
        $i = 0;
        while($file->valid() && $i <= $checkLines){
            $line = $file->fgets();
            foreach ($delimiters as $delimiter){
                $regExp = '/['.$delimiter.']/';
                $fields = preg_split($regExp, $line);
                if(count($fields) > 1){
                    if(!empty($results[$delimiter])){
                        $results[$delimiter]++;
                    } else {
                        $results[$delimiter] = 1;
                    }
                }
            }
            $i++;
        }
        $results = array_keys($results, max($results));
        return $results[0];
    }

    private function getArrayCsv($csv,$delimiter){
        $fila = 1;
        $arrayresult=array();
        if (($gestor = fopen($csv, "r")) !== FALSE) {
            while (($datos = fgetcsv($gestor, 2000, $delimiter)) !== FALSE) {
                $numero = count($datos);
                $fila++;
                for ($c=0; $c < $numero; $c++) {
                    if($c==0) $key='año_aplicacion';
                    if($c==1) $key='mes_aplicacion';
                    if($c==2) $key='dia_aplicacion';
                    if($c==3) $key='punto_vacunacion';
                    if($c==4) $key='unicodigo_entidad';
                    if($c==5) $key='nombre_entidad';
                    if($c==6) $key='zona';
                    if($c==7) $key='distrito';
                    if($c==8) $key='provincia';
                    if($c==9) $key='canton';
                    if($c==10) $key='apellidos';
                    if($c==11) $key='nombres';
                    if($c==12) $key='tipo_iden';
                    if($c==13) $key='num_iden';
                    if($c==14) $key='sexo';
                    if($c==15) $key='año_nacimiento';
                    if($c==16) $key='mes_nacimiento';
                    if($c==17) $key='dia_nacimiento';
                    if($c==18) $key='nacionalidad';
                    if($c==19) $key='tel_contacto';
                    if($c==20) $key='correo_electronico';
                    if($c==21) $key='pobla_vacuna';
                    if($c==22) $key='fase_vacuna';
                    if($c==23) $key='nom_vacuna';
                    if($c==24) $key='lote_vacuna';
                    if($c==25) $key='dosis_aplicada';
                    if($c==26) $key='paciente_agendado';
                    if($c==27) $key='nom_profesional_aplica';
                    if($c==28) $key='iden_profesional_aplica';
                    if($c==29) $key='nom_profesional_registra';
                    if($c==30) $key='exteriordosisprevia';
                    if($c==31) $key='exteriordosisfechaanio';
                    if($c==32) $key='exteriordosisfechames';
                    if($c==33) $key='exteriordosisfechadia';
                    if($c==34) $key='exteriordosispais';
                    if($c==35) $key='exteriordosislote';
                    $arraytmp[$key]= trim($datos[$c]);
                }
                if($arraytmp){
                    $arrayresult[]=$arraytmp;
                }
            }
            fclose($gestor);
        }
        return $arrayresult;
    }
}
 ?>
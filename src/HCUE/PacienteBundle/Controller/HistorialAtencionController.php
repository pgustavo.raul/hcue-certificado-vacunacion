<?php

namespace HCUE\PacienteBundle\Controller;

use HCUE\PacienteBundle\Entity\Constantes;
use HCUE\PacienteBundle\Form\Type\PacienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class HistorialAtencionController extends Controller
{
    /**
     * Obtiene la vista del grid (datatable) del historial de atención médica
     * @Route("/index/{menurol_id}",name="hcue_paciente_historial_atencion_index",requirements={"menurol_id" = "\d+"},defaults={"menurol_id"=0})
     * @param int $menurol_id Id del menu del rol
     * @Template()
     * @return text/html HCUEPacienteBundle:HistorialAtencion:index.html.twig
     * */
    public function indexAction($menurol_id = 0)
    {
        $service_security = $this->get('core.seguridad.service.security_authorization');
        if ($menurol_id > 0) {
            $service_security->setActiveMenuRolId($menurol_id);
        }
        $datatable = '';
        //Verificamos los permisos
        if ($service_security->hasPermission()) {
            $datatable = $this->get('hcue.paciente.datatable.historialatencion');
            $datatable->buildDatatable();
        } else {
            $this->get("session")->getFlashBag()->add("danger", 'Acceso denegado !');
        }
        return array('datatable' => $datatable);
    }

    /**
     * Obtiene los datos del grid (datatable) del historial de atención médica
     * @Route("/results",name="hcue_paciente_historial_atencion_results")
     * @param Request $request Variable de obtencion de datos
     * @return json
     * */
    public function resultsAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            try {
                $datatable = $this->get('hcue.paciente.datatable.historialatencion');
                return $datatable->getResponse($request);
            } catch (\Exception $ex) {
                $this->get('core.app.logger')->addRecord($ex);
                $message = ($ex->getCode() >= 0)
                    ? "Se ha producido un error en el sistema, no es posible mostrar la información."
                    : $ex->getMessage();
                return new Response(
                    $message,
                    Response::HTTP_BAD_REQUEST,
                    array('content-type' => 'text/plain')
                );
            }
        }
        return new Response('Solicitud incorrecta.', 400);
    }

    /**
     * Obtiene la vista del grid (datatable) de los pacientes
     * @Route("/filteravanzada",name="hcue_paciente_historial_atencion_filteravanzada")
     * @Template()
     * @return text/html HCUEAtencionMedicBundle:HistorialAtencion:index.html.twig
     * */
    public function filterAvanzadaAction()
    {
        $catalogoManager = $this->get('core.app.entity.manager.catalogo');
        $tipoIdentificacion = $catalogoManager->getCatalogo(Constantes::TC_TIPOIDENTIFICACION);

        return ["tipoidentificacion"=>$tipoIdentificacion];
    }

    /**
     * Obtiene la vista del grid (datatable) de las ultimas 5 atenciones médicas del paciente
     * @Route("/show/{paciente_id}",name="hcue_paciente_historial_atencion_show",requirements={"paciente_id" = "\d+"},defaults={"paciente_id"=0}, options={"expose"=true})
     * @param int $paciente_id Id del paciente
     * @Template()
     * @return text/html HCUEPacienteBundle:HistorialAtencion:showHistoryMedicalAttention.html.twig
     * */
    public function showHistoryMedicalAttentionAction($paciente_id = 0)
    {
        $pacienteManager = $this->get('hcue.paciente.entity.manager.paciente');
        $historialAtencion = $pacienteManager->getAtencionesMedicasByPaciente($paciente_id);

        return array (
            'historialatenciones' => $historialAtencion
        );
    }

}
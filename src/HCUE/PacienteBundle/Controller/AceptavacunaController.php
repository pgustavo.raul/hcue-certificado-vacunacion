<?php
/**
 * Controller para aceptacion de vacuna
 *
 * @author Lenin Vallejos
 */
namespace HCUE\PacienteBundle\Controller;

use HCUE\PacienteBundle\Entity\Aceptavacuna;
use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;



class AceptavacunaController extends Controller
{
    /**
     * Lists all aceptavacuna entities.
     * @Route("/{menurol_id}",name="hcue_paciente_acepta_vacuna",defaults={"menurol_id" =0},requirements={"menurol_id" = "\d+"})
     * @param $menurol_id Id del menu del Rol
     * @Template()
     * @return text/html HCUEPacienteBundle:Aceptavacuna:index.html.twig
     */
    public function indexAction($menurol_id = 0)
    {


        $em = $this->getDoctrine()->getManager();
        $serviceSecurity = $this->get('core.seguridad.service.security_authorization');
        $serviceSecurity->setActiveMenuRolId($menurol_id);
        $repoCatalogo = $em->getRepository("CoreAppBundle:Catalogo");
        $repoVacuna = $em->getRepository("HCUEAtencionMedicBundle:Vacuna");
        if ($menurol_id > 0) {
            $serviceSecurity->setActiveMenuRolId($menurol_id);
        }
        //Verificamos los permisos

        $form='';
        if ($serviceSecurity->hasPermission()) {
            $aceptavacuna = new Aceptavacuna();
 
            $form = $this->createFormBuilder($aceptavacuna)
              ->add('vacuna', EntityType::class, array(
                    'label' => 'Vacuna',
                    'class' => 'HCUEAtencionMedicBundle:Vacuna',
                    'choice_label' => 'nombrevacuna',
                    'choices' => $repoVacuna->getVacunas(1,1,1),
                    'placeholder' => '--Seleccione--',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'input-sm required',
                    )
                ))
                ->add('cttipoidentificacion', EntityType::class, array(
                    'label' => 'Tipo identificación',
                    'class' => 'CoreAppBundle:Catalogo',
                    'choice_label' => 'descripcion',
                    'choices' => $repoCatalogo->getCatalogo(Constantes::TC_TIPOIDENTIFICACION),
                    'placeholder' => '--Seleccione--',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'input-sm required',
                    )
                ))

                ->add('identificacion',TextType::class, array(
                    'label' => 'Identificación',
                    'attr' => array(
                        'class' => 'required',
                        'placeholder' => '',
                    )
                ))

                ->getForm();
 
        } else {
            $this->get("session")->getFlashBag()->add("danger", 'Acceso denegado !');
        }
    

        return array(
            'form' => $form->createView()
        );

    }

    /**
     * Creates a new aceptavacuna entity.
     * @Route("/createorupdate",name="hcue_paciente_aceptavacuna_createorupdate")
     * @Method({"GET", "POST"})
     * @param Request $request
     */
    public function createOrUpdate(Request $request){
        $cttipoidentificacion=$request->get('form')['cttipoidentificacion'];
        $identificacion=$request->get('form')['identificacion'];
        $vacuna=$request->get('form')['vacuna'];
        $em = $this->getDoctrine()->getManager();
        return $this->redirectToRoute('hcue_paciente_aceptavacuna_new',
        array("vacuna"=>$vacuna,"cttipoidentificacion"=>$cttipoidentificacion,"identificacion"=>$identificacion));
    }




    /**
     * Creates a new aceptavacuna entity.
     *
     * @Route("/new/{vacuna}/{cttipoidentificacion}/{identificacion}", name="hcue_paciente_aceptavacuna_new", requirements={"vacuna" = "\d+","cttipoidentificacion" = "[^/]++","identificacion" = "[^/]++"})
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $vacuna,$cttipoidentificacion,$identificacion)
    {
        $serviceSecurity = $this->get('core.seguridad.service.security_authorization');
        $personaManager = $this->get('core.app.entity.manager.persona');
        $pacienteManager = $this->get('hcue.paciente.entity.manager.paciente');

        $menuactivo=$serviceSecurity->getActiveMenuRolId();
        $serviceRegistrocivil = $this->get("hcue.paciente.service.registrocivil");
        $aceptavacunaManager = $this->get('hcue.paciente.entity.manager.aceptavacuna');
        $catalogoManager = $this->get('core.app.entity.manager.catalogo');

        $ctsexo="";
        $ctestadocivil="";
        $correo="";
        $nombrecompleto="";
        $fechanacimiento="";
        $aceptavacuna=$aceptavacunaManager->findOneBy(array("cttipoidentificacion_id"=>$cttipoidentificacion,"identificacion"=>$identificacion,"vacuna"=>$vacuna));
        $parroquia_id= null;
        if(!$aceptavacuna){
            $aceptavacuna = new Aceptavacuna();
            $aceptavacuna->setCttipoidentificacionId($cttipoidentificacion);
            $persona=$personaManager->findOneBy(array("cttipoidentificacion_id"=>$cttipoidentificacion,"numeroidentificacion"=>$identificacion,"estado"=>1));

            if($persona){
                    $ctsexo=$persona->getCtsexo();
                    $ctestadocivil=$persona->getCtestadocivil();
                    $nombrecompleto=$persona->getNombrecompleto();
                    $fechanacimiento=$persona->getFechanacimiento()->format('d-m-Y');
                    $cttipoidentificacion=$persona->getCttipoidentificacion();
                    $paciente=$pacienteManager->findOneBy(array("persona_id"=>$persona->getId(),"estado"=>1,"activo"=>1));
                    if($paciente){
                        $correo=$paciente->getCorreo();
                        $parroquia=$paciente->getParroquia();
                        if($parroquia){
                            $parroquia_id=$parroquia->getId();
                        }
                    }
            }else{
                if($cttipoidentificacion==Constantes::CT_TIPOIDENTCEDULA){

                    $arraypersona = $serviceRegistrocivil->consumir($identificacion);
                    if($arraypersona['webservis']){
                        $ctsexo=($arraypersona['sexo'])?$catalogoManager->find($arraypersona['sexo']):null;
                        $nombrecompleto=$arraypersona["nombrecompleto"];
                        $fechanacimiento=$arraypersona["fechanacimiento"];
                        $ctestadocivil=$catalogoManager->find($arraypersona["estadocivil"]);
                        if ($arraypersona["webservis"]) {
                            if ($arraypersona['condicioncedulado'] == "FALLECIDO" || $arraypersona['condicioncedulado'] == "EXTRANJERO FALLECIDO") {
                                die("La persona se encuentra fallecida no es posible realizar esta acción  ");
                            }
                        }else{
                            die("Cédula incorrecta o servicio web del Registro Civil no disponible,  intente más tarde ");
                        }
                    }
                }

            }

            $aceptavacuna->setIdentificacion($identificacion);
            $cttipoidentificacion=$catalogoManager->find($cttipoidentificacion);

            if($ctsexo){
                $aceptavacuna->setCtsexo($ctsexo);
            }
            $aceptavacuna->setCttipoidentificacion($cttipoidentificacion);
            if($ctestadocivil){
                $aceptavacuna->setCtestadocivil($ctestadocivil);
            }
            $aceptavacuna->setNombre($nombrecompleto);
            if($fechanacimiento){
                $aceptavacuna->setFechanacimiento($fechanacimiento);
            }
            $aceptavacuna->setCorreo($correo);

        }

        //die($aceptavacuna->getNombre());



        $form = $this->createForm('HCUE\PacienteBundle\Form\Type\AceptavacunaType', $aceptavacuna , ['parroquia_id'=>$parroquia_id]);
        $form->handleRequest($request);

        return $this->render('HCUEPacienteBundle:Aceptavacuna:new.html.twig',array( "form" => $form->createView(),
            "cttipoidentificacion"=>$cttipoidentificacion,"identificacion"=>$identificacion, "vacuna"=>$vacuna, "menuactivo"=>$menuactivo
             ));
    }

    /**
     * Finds and displays a aceptavacuna entity.
     *
     * @Route("/{id}", name="aceptavacuna_show")
     * @Method("GET")
     */
    public function showAction(Aceptavacuna $aceptavacuna)
    {
        $deleteForm = $this->createDeleteForm($aceptavacuna);

        return $this->render('aceptavacuna/show.html.twig', array(
            'aceptavacuna' => $aceptavacuna,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing aceptavacuna entity.
     *
     * @Route("/edit/{id}", name="hcue_paciente_aceptavacuna_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        $serviceSecurity = $this->get('core.seguridad.service.security_authorization');

        $menuactivo=$serviceSecurity->getActiveMenuRolId();
        $aceptavacunaManager = $this->get('hcue.paciente.entity.manager.aceptavacuna');
        $aceptavacuna=$aceptavacunaManager->find($id);
        $editForm = $this->createForm('HCUE\PacienteBundle\Form\Type\AceptavacunaType', $aceptavacuna);
        $editForm->handleRequest($request);


        return $this->render('HCUEPacienteBundle:Aceptavacuna:edit.html.twig', array(
            'aceptavacuna' => $aceptavacuna,
            "menuactivo"=>$menuactivo,
            'form' => $editForm->createView(),

        ));
    }


    /**
     * Valida el estado del registro
     * @Route("/validate",name="hcue_paciente_aceptavacuna_validate")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse  {"status","message"}
     * */
    public function validapacienteaceptAction(Request $request)
    {

        $aceptavacunaManager = $this->get("hcue.paciente.entity.manager.aceptavacuna");
        $personaManager = $this->get('core.app.entity.manager.persona');

        $cttipoidentificacion=$request->get('form')['cttipoidentificacion'];
        $identificacion=$request->get('form')['identificacion'];
        $vacuna=$request->get('form')['vacuna'];
        $status = false;

        if($cttipoidentificacion==5){
            $persona=$personaManager->findOneBy(array("cttipoidentificacion_id"=>$cttipoidentificacion,"numeroidentificacion"=>$identificacion,"estado"=>1));
            if(!$persona){
                return new JsonResponse(array("status" => false, "message" => "El paciente no existe, debe registrar a la persona en el módulo de pacientes "));
            }

        }
        $vacuna=$aceptavacunaManager->findOneBy(array("cttipoidentificacion_id"=>$cttipoidentificacion,"identificacion"=>$identificacion,"vacuna_id"=>$vacuna));
        $status=true;
        $message='OK';
        if($vacuna){
            $estado=$vacuna->getEstado();    
            if($estado==1){
                $status=false;
                $message="El paciente con identificación (".$identificacion.") ya aceptó la vacuna";                
            }
        }
        return new JsonResponse(array("status" => $status, "message" => $message));


    }









}

<?php
/*
* @autor Lenin Vallejos
*/

namespace HCUE\PacienteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use HCUE\AtencionMedicBundle\Entity\Certificadovacuna;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Validator\Constraints\Length;

class CertificadovacunaController extends Controller
{



    /**
     * Retorna la vista de la lista de datos datatables tipo Certificadovacuna
     * @Route("/public/index",name="hcue_paciente_certificadovacuna_public_index",requirements={"menurol_id" = "\d+"},defaults={"menurol_id"=0})
     * @Method({"GET"})
     * @Template()
     * @return text/html HCUEPacienteBundle:Certificadovacuna:Public:index.html.twig
     **/

    public function publicindexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repoCatalogo = $em->getRepository("CoreAppBundle:Catalogo");


        $certificadoVacuna = new Certificadovacuna();
        $form = $this->createFormBuilder($certificadoVacuna)

              ->add('cttipoidentificacion', EntityType::class, array(
                'label'       => 'Tipo de Identificación',
                'class'       => 'CoreAppBundle:Catalogo',
                'choices'     => $repoCatalogo->getCatalogo(Constantes::TC_TIPOIDENTIFICACION),
                'choice_label'    => 'descripcion',
                'placeholder' => '--Seleccione--',
                'empty_data'  => null,
                    'required'=>true
            ))

            ->add('identificacion', TextType::class, array(
                'label' => 'Identificación',
                //, 'placeholder' => 'Ingrese el Nro de identificación '
                 'required'=>true
            ))
            ->add('fechanacimiento', TextType::class, array(
                'label' => 'Fecha de Nacimiento',
                'required'=>true
            ))
            ->add('correo', TextType::class, array(
                'label' => 'Correo Electrónico',
                'required'=>true,
                'attr' => array(
                    'placeholder' => '',
                    'class'=> 'col-sm-10 control-label',
                    'style'=> 'text-align:left',
                )
            ))

            ->add('fechaexpedicion', TextType::class, array(
                'label' => 'Fecha de Expedición de Cédula',
                'required'=>false,
                'mapped' =>false,
                'attr' => array(
                    'placeholder' => '',
                    'class'=> 'col-sm-10 control-label',
                    'style'=> 'text-align:left',
                )
            ))
            ->getForm();


        return array(
            'form' => $form->createView()
        );
    }

    /**
     * Valida si existe vacuna en la tabla registrovacuna
     * @Route("/public/validatevacuna",name="hcue_paciente_certificadovacuna_public_validatevacuna")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse  {"status","message"}
     **/
    public function validatevacunaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $emcertificado = $em->getRepository("HCUEAtencionMedicBundle:Certificadovacuna");
        $emregistrovacunacion = $em->getRepository("HCUEAtencionMedicBundle:Registrovacunacion");
        $pacienteManager=$this->get("hcue.paciente.entity.manager.paciente");
        $managerPersona = $this->get("core.app.entity.manager.persona");
        $RegistroCService = $this->container->get("hcue.paciente.service.registrocivil");
        $cttipoidentificacion=$request->get('form')['cttipoidentificacion'];
        $identificacion=$request->get('form')['identificacion'];
        $fechanacimientoform=$request->get('form')['fechanacimiento'];
        $correoform=$request->get('form')['correo'];
        $fechanacimientoform = new \Datetime($fechanacimientoform);
        $certificado=$emcertificado->findOneBy(array('cttipoidentificacion_id'=>$cttipoidentificacion,"identificacion"=>$identificacion,'fechanacimiento'=>$fechanacimientoform,'estado'=>1, 'activo'=>1));
        $fechaexpedicion=$request->get('form')['fechaexpedicion'];
        if(!empty($fechaexpedicion)){
            $serviceresult=$RegistroCService->consumir($identificacion);
            if($serviceresult['webservis']){
                $fechaemisionRc=($serviceresult['fechaCedulacion']);
                $fechaemisionRc = explode('/',$fechaemisionRc);
                $fechaemisionRc = $fechaemisionRc[2].'-'.$fechaemisionRc[1].'-'.$fechaemisionRc[0];
                if($fechaemisionRc != $fechaexpedicion){
                    return new JsonResponse(array('status'=>false,'message'=>'La información de fecha de expedición es incorrecta, por favor vuelva a intentar'));
                }
                if($fechaemisionRc != $fechaexpedicion){
                    return new JsonResponse(array('status'=>false,'message'=>'La información de fecha de expedición es incorrecta, por favor vuelva a intentar'));
                }
            }else{
                return new JsonResponse(array('status'=>false,'message'=>'Cédula incorrecta o servicio web de registro civil no disponible por el momento, consulte mas tarde'));
            }
                        
        }
        if(!$certificado){
            $persona=$managerPersona->findOneBy(array('cttipoidentificacion_id'=>$cttipoidentificacion,"numeroidentificacion"=>$identificacion,'fechanacimiento'=>$fechanacimientoform, "activo"=>1));
            if(!$persona){
                return new JsonResponse(array('status'=>false,'message'=>'Certificado no encontrado, estamos trabajando en la carga de su información, vuelva a intentarlo en 72 horas, o revise la fecha de nacimiento.'));
            }else{
                $paciente=$pacienteManager->findOneBy(array("persona_id"=>$persona->getId(),"estado"=>1,"activo"=>1));
                if(!$paciente){
                    return new JsonResponse(array('status'=>false,'message'=>'Certificado no encontrado, estamos trabajando en la carga de su información, vuelva a intentarlo en 72 horas.'));
                }                       
            }
        }else{
            $paciente=$certificado->getPaciente();
            $fechamodificacion=$certificado->getFechamodificacion()->format('Ymd');
            $today = date("Ymd");
            if($fechamodificacion==$today){
                return new JsonResponse(array('status'=>false,'message'=>'El paciente ya tiene registrada una solicitud que se enviará al correo: <b>'.$certificado->getCorreo().'</b>'));
            }

        }
        $validarcorreo=$this->validarCorreo($correoform);
        if(!$validarcorreo){
            return new JsonResponse(array('status'=>false,'message'=>"Estimado paciente su correo electrónico es: <b>".$correoform."</b> es incorrecto.<br> Por favor digite un correo válido</a>"));
        }
        $returnnumvacunas=$emregistrovacunacion->getVacunacionByPacienteidCertificado($paciente->getId());
        if(sizeof($returnnumvacunas)!=0){
            $pacientedata=array('idpacinete'=>$paciente->getId(),'correo'=>$correoform);
            return new JsonResponse(array('status'=>true,'message'=>"Estimado paciente usted consta en la Plataforma de Atenciones de Salud con:",'vacunadata'=>$returnnumvacunas,'pacientedata'=>$pacientedata));
        }else{
            return new JsonResponse(array('status'=>false,'message'=>'Certificado no encontrado, estamos trabajando en la carga de su información, vuelva a intentarlo en 72 horas'));
        }
    }

    private function validarCorreo($correo){
        $pos = strpos($correo, '.');
        if($pos===false){
            return false;
        }

        $arraycorreosinvalidos=array('sincorreo@','nosabe@','notiene@', 'noconoce@',
        '@sincorreo','@nosabe','@notiene','@notienecorreo','mm@sss','mm@wsss','aaa@sss');
        foreach ($arraycorreosinvalidos as $cadena_buscada) {
            $pos = strpos($correo, $cadena_buscada);
            if($pos!== false){
                return false;
                break;
            }
        }
        return true;
    }

    /**
     * Creates a new aceptavacuna entity.
     * @Route("/public/createorupdate",name="hcue_paciente_certificadovacuna_public_createorupdate")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @Template()
     */
    public function createOrUpdate(Request $request){
        $em = $this->getDoctrine()->getManager();
        $emcertificado = $em->getRepository("HCUEAtencionMedicBundle:Certificadovacuna");
        $managerPersona = $this->get("core.app.entity.manager.persona");
        $pacienteManager=$this->get("hcue.paciente.entity.manager.paciente");
        $cttipoidentificacion=$request->get('form')['cttipoidentificacion'];
        $identificacion=$request->get('form')['identificacion'];
        $fechanacimiento=$request->get('form')['fechanacimiento'];
        $correo=$request->get('form')['correo'];
        $fechanacimiento = new \Datetime($fechanacimiento);
        try {
            $persona=$managerPersona->findOneBy(array('cttipoidentificacion_id'=>$cttipoidentificacion,"numeroidentificacion"=>$identificacion,'fechanacimiento'=>$fechanacimiento, "activo"=>1));
            $paciente=$pacienteManager->findOneBy(array("persona_id"=>$persona->getId(),"estado"=>1,"activo"=>1));
            if($paciente){
                $correo=$correo;
                $certificadovacuna=$emcertificado->findOneBy(array('paciente'=>$paciente,'estado'=>1, 'activo'=>1));
                if(!$certificadovacuna){
                    $certificadovacuna = new Certificadovacuna();
                }
                $certificadovacuna->setPaciente($paciente);
                if($correo){
                    $certificadovacuna->setCorreo($correo);
                }
                $paciente->setCorreo($correo);
                $certificadovacuna->setIdentificacion($identificacion);
                $certificadovacuna->setNombres($persona->getNombres());
                $certificadovacuna->setFechanacimiento($fechanacimiento);
                $certificadovacuna->setCtsexo($persona->getCtSexo());
                $certificadovacuna->setCtestadoverificacion($em->getReference('CoreAppBundle:Catalogo', Constantes::CT_CERTIFICADO_VACUNACION_PENDIENTE));
                $certificadovacuna->setCttipoidentificacion($persona->getCttipoidentificacion());
                $em->persist($certificadovacuna);
                $em->flush();

                return $this->render('HCUEPacienteBundle:Certificadovacuna:publicresponse.html.twig',array("certificadovacuna"=>$certificadovacuna));
            }
        }catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $status = false;
            $message = ($ex->getCode() >= 0) ? "Se ha producido un error en el sistema, no es posible guardar el registro." : $ex->getMessage();
        }
    }

/**
     * Obtener correo del paciente
     * @Route("/public/tomapaciente",name="hcue_paciente_public_datos")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return Array
     **/
    public function consultadatoPaciente(Request $request)
    {
        try{
            $pacienteManager=$this->get("hcue.paciente.entity.manager.paciente");
                $paciente=$pacienteManager->findOneBy(array('numerohistoriaclinica'=>$request->get('form')['identificacion'],"activo"=>1,"estado"=>1));
                $data=array("status"=>false,"message"=>"Certificado no encontrado, estamos trabajando en la carga de su información, vuelva a intentarlo en 72 horas","correo"=>'',"pacienteid"=>'');
                if($paciente){
                    $data = array("status"=>true,"message"=>"Paciente se encuentra vacunado","correo"=>$paciente->getCorreo(),"pacienteid"=>$paciente->getId());
                }
                return new JsonResponse(array("data"=>$data));
        }catch (\Exception $ex) {
            $this->get('core.app.logger')->addRecord($ex);
            $message = ($ex->getCode() >= 0)
                ? "Se ha producido un error en el sistema, no es posible mostrar la información."
                : $ex->getMessage();
            return new Response(
                $message,
                Response::HTTP_BAD_REQUEST,
                array('content-type' => 'text/plain')
            );
        }
    }

}

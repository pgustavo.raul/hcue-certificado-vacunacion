<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.madrepaciente")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\MadrepacienteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Madrepaciente{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.madrepaciente_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var integer $persona_id
     * @ORM\Column(name="persona_id", type="integer", nullable=false)
     **/
    private $persona_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Persona", cascade={"persist"})
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     **/
    private $persona;

    /**
     * @var string $direcciondomicilio
     * @ORM\Column(name="direcciondomicilio", type="string",length=250, nullable=true)
     **/
    private $direcciondomicilio;

    /**
     * @var string $numerotelefonico
     * @ORM\Column(name="numerotelefonico", type="string",length=15, nullable=true)
     **/
    private $numerotelefonico;

    /**
     * @var integer $ctetnia_id
     * @ORM\Column(name="ctetnia_id", type="integer", nullable=true)
     **/
    private $ctetnia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctetnia_id", referencedColumnName="id")
     **/
    private $ctetnia;

    /**
     * @var integer $recibebono
     * @ORM\Column(name="recibebono", type="integer", nullable=true)
     **/
    private $recibebono;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=true)
     **/
    private $estado;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=true)
     **/
    private $activo;



    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set persona_id
     * @param integer persona_id
     * @return madrepaciente
     **/
    public function setPersonaId($persona_id){
        $this->persona_id = $persona_id;
        return $this;
    }

    /**
     * Get persona_id
     * @return integer
     **/
    public function getPersonaId(){
        return $this->persona_id;
    }

    /**
     * Set persona
     * @param \Core\AppBundle\Entity\Persona $persona
     **/
    public function setPersona(\Core\AppBundle\Entity\Persona $persona){
        $this->persona = $persona;
        return $this;
    }

    /**
     * Get persona
     * @return \Core\AppBundle\Entity\Persona
     **/
    public function getPersona(){
        return $this->persona;
    }

    /**
     * Set direcciondomicilio
     * @param string direcciondomicilio
     * @return madrepaciente
     **/
    public function setDirecciondomicilio($direcciondomicilio){
        $this->direcciondomicilio = $direcciondomicilio;
        return $this;
    }

    /**
     * Get direcciondomicilio
     * @return string
     **/
    public function getDirecciondomicilio(){
        return $this->direcciondomicilio;
    }

    /**
     * Set numerotelefonico
     * @param string numerotelefonico
     * @return madrepaciente
     **/
    public function setNumerotelefonico($numerotelefonico){
        $this->numerotelefonico = $numerotelefonico;
        return $this;
    }

    /**
     * Get numerotelefonico
     * @return string
     **/
    public function getNumerotelefonico(){
        return $this->numerotelefonico;
    }

    /**
     * Set ctetnia_id
     * @param integer ctetnia_id
     * @return madrepaciente
     **/
    public function setCtetniaId($ctetnia_id){
        $this->ctetnia_id = $ctetnia_id;
        return $this;
    }

    /**
     * Get ctetnia_id
     * @return integer
     **/
    public function getCtetniaId(){
        return $this->ctetnia_id;
    }

    /**
     * Set ctetnia
     * @param \Core\AppBundle\Entity\Catalogo $ctetnia
     **/
    public function setCtetnia(\Core\AppBundle\Entity\Catalogo $ctetnia){
        $this->ctetnia = $ctetnia;
        return $this;
    }

    /**
     * Get ctetnia
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtetnia(){
        return $this->ctetnia;
    }

    /**
     * Set recibebono
     * @param integer recibebono
     * @return madrepaciente
     **/
    public function setRecibebono($recibebono){
        $this->recibebono = $recibebono;
        return $this;
    }

    /**
     * Get recibebono
     * @return integer
     **/
    public function getRecibebono(){
        return $this->recibebono;
    }

    /**
     * Set estado
     * @param integer estado
     * @return madrepaciente
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }


    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechamodificacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return madrepaciente
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return madrepaciente
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Set activo
     * @param integer activo
     * @return madrepaciente
     **/
    public function setActivo($activo){
        if($this->activo){
            $this->activo=$activo;
        }
        return $this;
    }

    /**
     * Get activo
     * @return integer
     **/
    public function getActivo(){
        return $this->activo;
    }




    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->activo=1;
        $this->estado=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }
}

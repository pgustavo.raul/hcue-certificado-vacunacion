<?php

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.solicreporthistoriaclinica")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\SolicreporthistoriaclinicaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Solicreporthistoriaclinica {

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.solicreporthistoriacli_id_seq", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var string $numerodocumento
     * @ORM\Column(name="numerodocumento", type="string",length=50, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo numerodocumento")
     * @Assert\Regex("/^\MSP+[A-Z0-9]+[\-M$\-E$\-O$]/")
     **/
    private $numerodocumento;

    /**
     * @var string $motivo
     * @ORM\Column(name="motivo", type="string",length=256, nullable=true)
     * @Assert\NotNull(message="Debe ingresar el campo motivo")
     **/
    private $motivo;

    /**
     * @var string $profesionalunidad
     * @ORM\Column(name="profesionalunidad", type="string",length=100, nullable=true)
     **/
    private $profesionalunidad;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var integer $atencionmedica_id
     * @ORM\Column(name="atencionmedica_id", type="integer", nullable=false)
     **/
    private $atencionmedica_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic")
     * @ORM\JoinColumn(name="atencionmedica_id", referencedColumnName="id")
     **/
    private $atencionmedica;

    /**
     * @var integer $usuarioresponsable_id
     * @ORM\Column(name="usuarioresponsable_id", type="integer", nullable=false)
     **/
    private $usuarioresponsable_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuarioresponsable_id", referencedColumnName="id")
     **/
    private $usuarioresponsable;

    /**
     * @var integer $ctestadoimpresion_id
     * @ORM\Column(name="ctestadoimpresion_id", type="integer", nullable=false)
     **/
    private $ctestadoimpresion_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestadoimpresion_id", referencedColumnName="id")
     **/
    private $ctestadoimpresion;

    /**
     * @var integer $usuarioactual_id
     * @ORM\Column(name="usuarioactual_id", type="integer", nullable=false)
     **/
    private $usuarioactual_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\SeguridadBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="usuarioactual_id", referencedColumnName="id")
     **/
    private $usuarioactual;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Set numerodocumento
     * @param string numerodocumento
     * @return solicreporthistoriaclinica
     **/
    public function setNumerodocumento($numerodocumento){
        $this->numerodocumento = $numerodocumento;
        return $this;
    }

    /**
     * Get numerodocumento
     * @return string
     **/
    public function getNumerodocumento(){
        return $this->numerodocumento;
    }

    /**
     * Set motivo
     * @param string motivo
     * @return solicreporthistoriaclinica
     **/
    public function setMotivo($motivo){
        $this->motivo = $motivo;
        return $this;
    }

    /**
     * Get motivo
     * @return string
     **/
    public function getMotivo(){
        return $this->motivo;
    }

    /**
     * Set profesionalunidad
     * @param string profesionalunidad
     * @return solicreporthistoriaclinica
     **/
    public function setProfesionalunidad($profesionalunidad){
        $this->profesionalunidad = $profesionalunidad;
        return $this;
    }

    /**
     * Get profesionalunidad
     * @return string
     **/
    public function getProfesionalunidad(){
        return $this->profesionalunidad;
    }

    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return solicreporthistoriaclinica
     **/
    public function setEntidadId($entidad_id){
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId(){
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     **/
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad){
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidad(){
        return $this->entidad;
    }

    /**
     * Set atencionmedica_id
     * @param integer atencionmedica_id
     * @return atenciongprioritario
     **/
    public function setAtencionmedicaId($atencionmedica_id){
        $this->atencionmedica_id = $atencionmedica_id;
        return $this;
    }

    /**
     * Get atencionmedica_id
     * @return integer
     **/
    public function getAtencionmedicaId(){
        return $this->atencionmedica_id;
    }

    /**
     * Set atencionmedica
     * @param \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica
     **/
    public function setAtencionmedica(\HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic $atencionmedica){
        $this->atencionmedica = $atencionmedica;
        return $this;
    }

    /**
     * Get atencionmedica
     * @return \HCUE\AtencionMedicBundle\Entity\AtencionmedicaBasic
     **/
    public function getAtencionmedica(){
        return $this->atencionmedica;
    }

    /**
     * Set usuarioresponsable_id
     * @param integer usuarioresponsable_id
     * @return solicreporthistoriaclinica
     **/
    public function setUsuarioresponsableId($usuarioresponsable_id){
        $this->usuarioresponsable_id = $usuarioresponsable_id;
        return $this;
    }

    /**
     * Get usuarioresponsable_id
     * @return integer
     **/
    public function getUsuarioresponsableId(){
        return $this->usuarioresponsable_id;
    }

    /**
     * Set usuarioresponsable
     * @param \Core\SeguridadBundle\Entity\Usuario $usuarioresponsable
     **/
    public function setUsuarioresponsable(\Core\SeguridadBundle\Entity\Usuario $usuarioresponsable){
        $this->usuarioresponsable = $usuarioresponsable;
        return $this;
    }

    /**
     * Get usuarioresponsable
     * @return \Core\SeguridadBundle\Entity\Usuario
     **/
    public function getUsuarioresponsable(){
        return $this->usuarioresponsable;
    }

    /**
     * Set ctestadoimpresion_id
     * @param integer ctestadoimpresion_id
     * @return solicreporthistoriaclinica
     **/
    public function setCtestadoimpresionId($ctestadoimpresion_id){
        $this->ctestadoimpresion_id = $ctestadoimpresion_id;
        return $this;
    }

    /**
     * Get ctestadoimpresion_id
     * @return integer
     **/
    public function getCtestadoimpresionId(){
        return $this->ctestadoimpresion_id;
    }

    /**
     * Set ctestadoimpresion
     * @param \Core\AppBundle\Entity\Catalogo $ctestadoimpresion
     **/
    public function setCtestadoimpresion(\Core\AppBundle\Entity\Catalogo $ctestadoimpresion){
        $this->ctestadoimpresion = $ctestadoimpresion;
        return $this;
    }

    /**
     * Get ctestadoimpresion
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtestadoimpresion(){
        return $this->ctestadoimpresion;
    }

    /**
     * Set usuarioactual_id
     * @param integer usuarioactual_id
     * @return solicreporthistoriaclinica
     **/
    public function setUsuarioactualId($usuarioactual_id){
        $this->usuarioactual_id = $usuarioactual_id;
        return $this;
    }

    /**
     * Get usuarioactual_id
     * @return integer
     **/
    public function getUsuarioactualId(){
        return $this->usuarioactual_id;
    }

    /**
     * Set usuarioactual
     * @param \HCUE\AtencionMedicBundle\Entity\Usuarioactual $usuarioactual
     **/
    public function setUsuarioactual(\Core\SeguridadBundle\Entity\Usuario $usuarioactual){
        $this->usuarioactual = $usuarioactual;
        return $this;
    }

    /**
     * Get usuarioactual
     * @return \Core\SeguridadBundle\Entity\Usuario
     **/
    public function getUsuarioactual(){
        return $this->usuarioactual;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return aplicacion
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return aplicacion
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion = new \DateTime();
    }
    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();
    }

}
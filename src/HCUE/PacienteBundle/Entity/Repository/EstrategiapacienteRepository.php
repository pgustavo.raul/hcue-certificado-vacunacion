<?php

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;

/**
 * 
 *
 * @author Fausto Suarez
 */
class EstrategiapacienteRepository extends EntityRepository
{

    /**
    * Obtiene el QueryBuilder de la lista de Estrategiapaciente
    * @return QueryBuilder  
    */
    public function getEstrategiapacienteQueryBuilder() {
        return $this->createQueryBuilder('ep')
                    ->select('ep')
                    ->where("ep.estado=:estadogeneral")
                    ->setParameter("estadogeneral", Constantes::CT_ESTADOGENERALACTIVO);
    }

    /**
     * Retorna el modelo de toda la lista de Estrategiapaciente
     * @param integer $paciente_id Id del paciente
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "HCUE\PacienteBundle\Entity\Estrategiapaciente"
     */
    public function getEstrategiapacienteByIdPaciente($id, $arrayResult=false)
    {
        $sql = "SELECT c.descripcion AS estrategia, d.descripcion AS pacienteclas, a.ctprioridadatencion_id, e.descripcion AS prioridadatencion, a.ctmotivoestrategia_id
                FROM HCUE_AMED.ESTRATEGIAPACIENTE a
                INNER JOIN (SELECT MAX(id) AS id FROM HCUE_AMED.ESTRATEGIAPACIENTE WHERE paciente_id=:paciente GROUP BY ctestrategia_id, ctpacienteclas_id,paciente_id) b
                ON a.id=b.id AND a.estado=:estadogeneral AND a.ctmotivoestrategia_id!=:salidaestrategia
                LEFT JOIN HCUE_CATALOGOS.DETALLECATALOGO c ON a.ctestrategia_id=c.id
                LEFT JOIN HCUE_CATALOGOS.DETALLECATALOGO d ON a.ctpacienteclas_id=d.id
                LEFT JOIN HCUE_CATALOGOS.DETALLECATALOGO e ON a.ctprioridadatencion_id=e.id
                ORDER BY ctmotivoestrategia_id DESC";
         $params['paciente'] = $id;
         $params['estadogeneral'] = Constantes::CT_ESTADOGENERALACTIVO;
         $params['salidaestrategia'] = Constantes::CT_SALIDA_ESTRATEGIA;

         $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
         $stmt->execute($params);
         return $stmt->fetchAll();
    }
}
<?php 
/*
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Jipson Montalban
*/

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;

class dbvvacunacionRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Db_vacunacion            
        * @return QueryBuilder  
        */
	public function getDb_vacunacionvariosQueryBuilder() {
                return $this->createQueryBuilder('dbvv')
                            ->select('dbvv');                                                 
        }

        /**
        * Obtiene la informaciòn bàsica del paciente del vacunòmetro           
        * @param integer $dni del paciente  
        * @param string $fechanacimiento del paciente  
        * @return json  Lista de objetos del pacinete vacunado
        */
        public function getPacienteVacunacionVacunometro($dni,$fechanacimiento){
                $fechadata=explode('-',$fechanacimiento);
                $qbuilder=$this->getDb_vacunacionQueryBuilder();
                $query=$qbuilder
                ->select('dbvv.num_iden,dbvv.apellidos_nombres,dbvv.fecha_vacuna, dbvv.dosis_aplicada, dbvv.nom_vacuna,dbvv.anio_nacimiento,dbvv.mes_nacimiento,dbvv.dia_nacimiento')
                //->andwhere('dbvv.estado=:estado')
                ->andwhere('dbvv.num_iden=:dni')
                ->andwhere('dbvv.anio_nacimiento=:anionacimiento')
                ->andwhere('dbvv.mes_nacimiento=:mesnacimiento')
                ->andwhere('dbvv.dia_nacimiento=:dianacimiento')
                ->setParameter("dni:",$dni)
                ->setParameter("anionacimiento:", $fechadata[0])
                ->setParameter("mesnacimiento:", $fechadata[1])
                ->setParameter("dianacimiento:", $fechadata[2]);
                //->setParameter("estado",Constantes::CT_ESTADOACTIVOVACUNOMETRO);
                return $query->getQuery()->getResult();
        }

        /**
        * Obtiene la informaciòn bàsica del paciente del vacunòmetro           
        * @param integer $dni del paciente  
        * @return json  Lista de objetos del pacinete vacunado
        */
        public function getPacienteVacunacionVacunometroBycedula($dni){
                $qbuilder=$this->getDb_vacunacionQueryBuilder();
                $query=$qbuilder
                ->select("dbvv.num_iden,dbvv.apellidos_nombres,dbvv.fecha_vacuna, dbvv.dosis_aplicada, dbvv.nom_vacuna,concat(dbvv.anio_nacimiento,'-',dbvv.mes_nacimiento,'-',dbvv.dia_nacimiento) AS fechanacimiento,dbvv.lote_vacuna")
                ->andwhere('dbvv.num_iden=:dni')
                ->orderBy('dbvv.dosis_aplicada', 'ASC')
                ->setParameter("dni:",$dni);
                return $query->getQuery()->getResult();
        }
}
?>
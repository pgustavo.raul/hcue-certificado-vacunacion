<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;

class MadrepacienteRepository extends EntityRepository{

    /**
     * Obtiene el QueryBuilder de la lista de Madrepaciente
     * @return QueryBuilder
     */
    public function getMadrepacienteQueryBuilder() {
        return $this->createQueryBuilder('t')
            ->select('t');
    }

    /**
     * Obtiene el Paciente por el numero de identificacion
     * @param integer $identificacion numero de identificacion
     * @return interger $id
     */
    public function getMadrepcteByIdentificacion($identificacion)
    {
        $queryBuilder = $this->createQueryBuilder('mp')
            ->select("mp,p")
            ->innerJoin('mp.persona', 'p')
            ->Where("p.numeroidentificacion=:identificacion")
            ->andWhere("mp.activo=:activo")
            ->setParameter('identificacion', $identificacion)
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO)
            ->setMaxResults(1);
        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}

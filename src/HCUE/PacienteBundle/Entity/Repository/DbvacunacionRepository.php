<?php 
/*
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Jipson Montalban
*/

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;

class DbvacunacionRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Db_vacunacion            
        * @return QueryBuilder  
        */
	public function getDb_vacunacionQueryBuilder() {
                return $this->createQueryBuilder('dbv')
                            ->select('dbv');                                                 
        }

        /**
        * Obtiene la informaciòn bàsica del paciente del vacunòmetro           
        * @param integer $dni del paciente  
        * @param string $fechanacimiento del paciente  
        * @return json  Lista de objetos del pacinete vacunado
        */
        public function getPacienteVacunacionVacunometro($dni,$fechanacimiento){
                $fechadata=explode('-',$fechanacimiento);
                //cambio de consulta con vacuna alternativa para generar certificado
                /* $qbuilder=$this->getDb_vacunacionQueryBuilder();
                $query=$qbuilder
                ->select('dbv.num_iden,dbv.apellidos_nombres,dbv.fecha_vacuna, dbv.dosis_aplicada, dbv.nom_vacuna,dbv.anio_nacimiento,dbv.mes_nacimiento,dbv.dia_nacimiento')
                //->andwhere('dbv.estado=:estado')
                ->andwhere('dbv.num_iden=:dni')
                ->andwhere('dbv.anio_nacimiento=:anionacimiento')
                ->andwhere('dbv.mes_nacimiento=:mesnacimiento')
                ->andwhere('dbv.dia_nacimiento=:dianacimiento')
                ->setParameter("dni:",$dni)
                ->setParameter("anionacimiento:", $fechadata[0])
                ->setParameter("mesnacimiento:", $fechadata[1])
                ->setParameter("dianacimiento:", $fechadata[2]);
                //->setParameter("estado",Constantes::CT_ESTADOACTIVOVACUNOMETRO);
                return $query->getQuery()->getResult(); */
                $sql="SELECT  dbvacunacion.num_iden,dbvacunacion.apellidos_nombres,dbvacunacion.fecha_vacuna, dbvacunacion.dosis_aplicada, dbvacunacion.nom_vacuna,dbvacunacion.anio_nacimiento,dbvacunacion.mes_nacimiento,dbvacunacion.dia_nacimiento      
                FROM (SELECT dbv.num_iden,
                                (dbv.apellidos||' '|| dbv.nombres)apellidos_nombres,
                                dbv.fecha_vacuna, 
                                dbv.dosis_aplicada, 
                                dbv.nom_vacuna,
                                dbv.anio_nacimiento,
                                dbv.mes_nacimiento,
                                dbv.dia_nacimiento,
                                dbv.lote_vacuna
                                FROM HCUE_AMED.DB_VACUNACION dbv
                                WHERE dbv.NUM_IDEN =:dni
                                AND dbv.anio_nacimiento =:anionacimiento
                                AND dbv.mes_nacimiento =:mesnacimiento
                                AND dbv.dia_nacimiento =:dianacimiento
                                ORDER BY dbv.fecha_vacuna) dbvacunacion
                UNION ALL 
                SELECT  dbvacunacionvarios.num_iden,dbvacunacionvarios.apellidos_nombres,dbvacunacionvarios.fecha_vacuna, dbvacunacionvarios.dosis_aplicada, dbvacunacionvarios.nom_vacuna,dbvacunacionvarios.anio_nacimiento,dbvacunacionvarios.mes_nacimiento,dbvacunacionvarios.dia_nacimiento      
                FROM (SELECT dbvv.num_iden,
                                (dbvv.apellidos||' '|| dbvv.nombres)apellidos_nombres,
                                dbvv.fecha_vacuna, 
                                dbvv.dosis_aplicada, 
                                dbvv.nom_vacuna,
                                dbvv.anio_nacimiento,
                                dbvv.mes_nacimiento,
                                dbvv.dia_nacimiento,
                                dbvv.lote_vacuna
                                FROM HCUE_AMED.DB_VACUNACIONVARIOS dbvv
                                WHERE dbvv.NUM_IDEN =:dni
                                AND dbvv.anio_nacimiento =:anionacimiento
                                AND dbvv.mes_nacimiento =:mesnacimiento
                                AND dbvv.dia_nacimiento =:dianacimiento
                                ORDER BY dbvv.fecha_vacuna) dbvacunacionvarios";
                  $params['dni'] = $dni;
                  $params['anionacimiento'] = $fechadata[0];
                  $params['mesnacimiento'] = $fechadata[1];
                  $params['dianacimiento'] = $fechadata[2];
                  $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
                  $stmt->execute($params);
                  return $stmt->fetchAll();
        }

        /**
        * Obtiene la informaciòn bàsica del paciente del vacunòmetro           
        * @param integer $dni del paciente  
        * @return json  Lista de objetos del pacinete vacunado
        */
        public function getPacienteVacunacionVacunometroBycedula($dni){
                //cambio de consulta con vacuna alternativa para generar certificado

                /* $qbuilder=$this->getDb_vacunacionQueryBuilder();
                $query=$qbuilder
                ->select("dbv.num_iden,dbv.apellidos_nombres,dbv.fecha_vacuna, dbv.dosis_aplicada, dbv.nom_vacuna,concat(dbv.anio_nacimiento,'-',dbv.mes_nacimiento,'-',dbv.dia_nacimiento) AS fechanacimiento,dbv.lote_vacuna")
                ->andwhere('dbv.num_iden=:dni')
                ->orderBy('dbv.dosis_aplicada', 'ASC')
                ->setParameter("dni:",$dni);
                return $query->getQuery()->getResult(); */
                
                $sql = "SELECT dbvacunacion.num_iden,
                dbvacunacion.apellidos_nombres,
                dbvacunacion.fecha_vacuna, 
                dbvacunacion.dosis_aplicada, 
                dbvacunacion.nom_vacuna,
                dbvacunacion.fechanacimiento,
                dbvacunacion.lote_vacuna 
                FROM (SELECT dbv.num_iden,
                (dbv.apellidos||' '|| dbv.nombres)apellidos_nombres,
                dbv.fecha_vacuna, 
                dbv.dosis_aplicada, 
                dbv.nom_vacuna,
                (dbv.anio_nacimiento||'-'||dbv.mes_nacimiento||'-'||dbv.dia_nacimiento) AS fechanacimiento,
                dbv.lote_vacuna
                FROM HCUE_AMED.DB_VACUNACION dbv
                WHERE dbv.NUM_IDEN =: dni
                ORDER BY dbv.fecha_vacuna) dbvacunacion
                UNION ALL
                SELECT dbvacunacionvarios.num_iden,
                dbvacunacionvarios.apellidos_nombres,
                dbvacunacionvarios.fecha_vacuna, 
                dbvacunacionvarios.dosis_aplicada, 
                dbvacunacionvarios.nom_vacuna,
                dbvacunacionvarios.fechanacimiento,
                dbvacunacionvarios.lote_vacuna
                FROM (SELECT dbvv.num_iden,
                (dbvv.apellidos||' '|| dbvv.nombres)apellidos_nombres,
                dbvv.fecha_vacuna, 
                dbvv.dosis_aplicada, 
                CASE WHEN dbvv.nom_vacuna='ZF2001' THEN 'ZHIFIVAX' ELSE nom_vacuna END AS nom_vacuna,
                (dbvv.anio_nacimiento||'-'||dbvv.mes_nacimiento||'-'||dbvv.dia_nacimiento) AS fechanacimiento,
                dbvv.lote_vacuna
                FROM HCUE_AMED.DB_VACUNACIONVARIOS dbvv  
                WHERE dbvv.NUM_IDEN =: dni
                ORDER BY dbvv.fecha_vacuna) dbvacunacionvarios";

                $params['dni'] = $dni;
                $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
                $stmt->execute($params);
                return $stmt->fetchAll();
        }
}
?>
<?php


namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PersonaArchivoRepository
 *
 * @author rv-pc
 */
class ArchivoRepository extends EntityRepository
{
    /**
     * Obtiene el id del Archivo por el número de archivo y el id de la entidad
     * @param integer $valor         valor a buscar
     * @param integer $entidad_id    id de la entidad
     * @param boolean $porpacienteid true si se quiere consultar por el id del paciente
     * @return interger $id
     */
    public function getIdArchivo($valor, $entidad_id, $porpacienteid = false)
    {
        $id = 0;
        $campo = ($porpacienteid) ? "pa.paciente_id" : "pa.numeroarchivo";
        $archivo = $this->createQueryBuilder('pa')
            ->select("pa.id")
            ->Where($campo . "=:valor")
            ->andWhere("pa.entidad_id=:entidad_id")
            ->setParameter('valor', $valor)
            ->setParameter('entidad_id', $entidad_id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if (array_key_exists("id", $archivo)) {
            $id = $archivo["id"];
        }
        return $id;
    }

    /**
     * Obtiene el total de paciente con o sin archivo
     * @param boolean $entidad_id Id de la Entidad
     * @param boolean $sin_archivo
     * @return interger
     */
    public function getCountArchivo($entidad_id, $sin_archivo = false)
    {
        $nots = $this->createQueryBuilder("arc")
            ->select("arc.paciente_id")
            ->where("arc.entidad_id=$entidad_id")
            ->getDQL();
        $qb = $this->createQueryBuilder("arc");
        $qb->select("COUNT(pt.id)")
            ->add("from", "HCUEPacienteBundle:Paciente pt")
            ->setMaxResults(1);
        ($sin_archivo) ? $qb->where($qb->expr()->notIn("pt.id", $nots))
            : $qb->where($qb->expr()->in("pt.id", $nots));

        return $qb->getQuery()->getOneOrNullResult()[1];
    }
}

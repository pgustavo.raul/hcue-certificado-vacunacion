<?php

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;
/**
 * Descripcion de SolicreporthistoriaclinicaRepository
 * @author Miguel Faubla
 */
class SolicreporthistoriaclinicaRepository extends EntityRepository
{

    /**
     * Obtiene el QueryBuilder de la lista de Solicreporthistoriaclinica
     * @return QueryBuilder
     */
    public function getSolicreporthistoriaclinicaQueryBuilder() {
        return $this->createQueryBuilder('solrephis')
            ->select('solrephis');
    }

    /**
     * Obtiene si ya se ha registrado la solicitud para la impresión del reporte 002
     * @param integer $atencionmedica_id Id de la Atencion
     * @return boolean
     */
    public function isExistsSolicReportHistory($atencionmedica_id)
    {
        $queryBuilder = $this->getSolicreporthistoriaclinicaQueryBuilder();
        $q = $queryBuilder
            ->Select('count(solrephis.id)')
            ->andWhere("solrephis.atencionmedica_id = :atencionmedica_id")
            ->setParameter("atencionmedica_id", $atencionmedica_id)
            ->setMaxResults(1)
            ->getQuery();

        return ($q->getSingleScalarResult() > 0) ? 1 : 0;
    }

    /**
     * Obtiene si la solicitud se encuentra aprobada para la impresión del reporte 002
     * @param integer $atencionmedica_id Id de la Atencion
     * @param integer $estadosolicitud Id del catalogo de estados para la impresion de la historia clinica
     * @return boolean
     */
    public function isExistsApprovedSolicReportHistory($atencionmedica_id, $estadosolicitud)
    {
        $queryBuilder = $this->getSolicreporthistoriaclinicaQueryBuilder();
        $q = $queryBuilder
            ->Select('count(solrephis.id)')
            ->andWhere("solrephis.atencionmedica_id = :atencionmedica_id")
            ->andWhere("solrephis.ctestadoimpresion_id = :ctestadoimpresion_id")
            ->setParameter("atencionmedica_id", $atencionmedica_id)
            ->setParameter("ctestadoimpresion_id", $estadosolicitud)
            ->setMaxResults(1)
            ->getQuery();

        return ($q->getSingleScalarResult() > 0) ? 1 : 0;
    }

    /**
     * Obtiene si la solicitud ya fue impresa
     * @param integer $id Id de la Solicitud
     * @param integer $estadosolicitud Id del catalogo de estados para la impresion de la historia clinica
     * @return boolean
     */
    public function isExistsImpSolicReportHistory($id, $estadosolicitud)
    {
        $queryBuilder = $this->getSolicreporthistoriaclinicaQueryBuilder();
        $q = $queryBuilder
            ->Select('count(solrephis.id)')
            ->andWhere("solrephis.id = :id")
            ->andWhere("solrephis.ctestadoimpresion_id = :ctestadoimpresion_id")
            ->setParameter("id", $id)
            ->setParameter("ctestadoimpresion_id", $estadosolicitud)
            ->setMaxResults(1)
            ->getQuery();

        return ($q->getSingleScalarResult() > 0) ? 1 : 0;
    }


}
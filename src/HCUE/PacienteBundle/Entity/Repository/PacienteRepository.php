<?php

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use HCUE\PacienteBundle\Entity\Constantes;

/**
 * Descripcion de PacienteRepository
 *
 * @author Richard Veliz
 */
class PacienteRepository extends EntityRepository
{

    /**
     * Obtiene el Paciente por el numero de identificacion
     * @param integer $identificacion numero de identificacion
     * @param date $fechanacimiento fecha de nacimiento (opcional)
     * @return interger $id
     */
    public function getPacienteByIdentificacion($identificacion,$fechanacimiento=null)
    {

        $qb = $this->createQueryBuilder('p')
            ->select("p")
            ->innerJoin('p.persona', 'ps')
            ->Where("ps.numeroidentificacion=:identificacion")
            ->andWhere("p.activo=:activo")
            ->setParameter('identificacion', $identificacion)
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO);
        if($fechanacimiento){
            $qb->andWhere("ps.fechanacimiento=:fechanacimiento")
                ->setParameter('fechanacimiento', $fechanacimiento);
        }
        $qb->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene el Paciente por Id
     * @param integer $id Id del Paciente
     * @return interger $id
     */
    public function getPacienteById($id)
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->select("p,ps,ti,pa,et,ene,ts,disp,te,pr,tb,ptzco,sexo,etcivil,nedu,pq,ct,pv,ppq,pct,ppv,tdis")
            ->leftJoin('p.ctetnia', 'et')
            ->leftJoin('p.ctestadoniveleducacion', 'ene')
            ->leftJoin('p.cttiposegurosalud', 'ts')
            ->leftJoin('p.ctdiscapacidad', 'disp')
            ->leftJoin('p.cttipoempresatrabajo', 'te')
            ->leftJoin('p.cttipobono', 'tb')
            ->leftJoin('p.ctparentezcocontacto', 'ptzco')
            ->leftJoin('p.profesion', 'pr')
            ->leftJoin('p.parroquia', 'pq')
            ->leftJoin('pq.canton', 'ct')
            ->leftJoin('ct.provincia', 'pv')
            ->leftJoin('p.persona', 'ps')
            ->leftJoin('ps.cttipoidentificacion', 'ti')
            ->leftJoin('ps.pais', 'pa')
            ->leftJoin('ps.ctsexo', 'sexo')
            ->leftJoin('ps.ctestadocivil', 'etcivil')
            ->leftJoin('ps.ctniveleducacion', 'nedu')
            ->leftJoin('ps.parroquia', 'ppq')
            ->leftJoin('ppq.canton', 'pct')
            ->leftJoin('pct.provincia', 'ppv')
            ->leftJoin('p.cttipodiscapacidad', 'tdis')
            ->Where("p.id=:id")
            ->andWhere("p.activo=:activo")
            ->setParameter('id', $id)
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO)
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene el id del paciente por el numero de cedula de la persona
     * @param integer $cedula cedula de la persona
     * @return string $cedula
     */
    public function getIdporCedula($cedula)
    {
        $id = 0;
        $paciente = $this->createQueryBuilder('p')
            ->select("p.id")
            ->innerJoin('p.persona', 'ps')
            ->Where("ps.numeroidentificacion=:cedula")
            ->setParameter('cedula', $cedula)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if (array_key_exists("id", $paciente)) {
            $id = $paciente["id"];
        }

        return $id;
    }

    /**
     * Obtiene un listado de paciente filtrado por un creiterio de busqueda
     * @param string $texto texto a buscar
     * @return arrayresult
     */
    public function getFilterPaciente($texto)
    {
        $qb = $this->createQueryBuilder('pt');
        $qbr = $qb->select("pt.id,ps.numeroidentificacion,ps.nombrecompleto")
            ->leftJoin("CoreAppBundle:Persona", "ps", "WITH", "ps.id=pt.persona_id")
            ->Where($qb->expr()->like("ps.numeroidentificacion", ":texto"))
            ->orWhere($qb->expr()->like("ps.nombrecompleto", ":texto"))
            ->setParameter('texto', "%" . $texto . "%")
            ->setMaxResults(15)
            ->getQuery();

        return $qbr->getArrayResult();
    }

    /**
     * Obtiene el QueryBuilder de la lista de paciente que tienen y no tienen numero de archivo por entidad
     * @param integer $entidad_id Id de la entidad
     * @return QueryBuilder
     */
    public function getQueryBuilderArchivo($entidad_id)
    {
        $qb = $this->createQueryBuilder('paciente');
        $qbr = $qb
            ->select("case when pacientearchivo.id is null then 0 else pacientearchivo.id end id,pacientearchivo.entidad_id,paciente.id as paciente_id,pacientearchivo.numeroarchivo,pacientearchivo.estado,pp.nombrecompleto,pp.numeroidentificacion,ct.descripcion tipoidentificacion,paciente.numerohistoriaclinica,pp.cttipoidentificacion_id")
            ->innerJoin("paciente.persona", 'pp', 'WITH', 'paciente.persona_id=pp.id')
            ->innerJoin("pp.cttipoidentificacion", 'ct', 'WITH', 'pp.cttipoidentificacion_id=ct.id')
            ->leftJoin("HCUEPacienteBundle:Archivo", "pacientearchivo", "WITH",
                "paciente.id=pacientearchivo.paciente_id and pacientearchivo.estado=:activo and pacientearchivo.entidad_id='$entidad_id'")
            ->where("pp.activo=:activo and paciente.activo=:activo")
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO);

        return $qbr;
    }


    /**
     * Obtiene el QueryBuilder de la lista de los paciente que tienen y no tienen inscripcion territorial
     * @return QueryBuilder
     */
    public function getQueryBuilderInscripcion()
    {
        $qb = $this->createQueryBuilder('paciente');
        $qbr
            = $qb->select("case when inscripcion.id is null then 0 else inscripcion.id end id,inscripcion.suministroelectrico,paciente.id as paciente_id,ent.nombreoficial,pp.nombrecompleto,pp.numeroidentificacion,paciente.numerohistoriaclinica,pp.fechanacimiento,ct.descripcion,inscripcion.estado, inscripcion.estado estadoicono,pp.cttipoidentificacion_id")
            ->innerJoin("paciente.persona", 'pp', 'WITH', 'paciente.persona_id=pp.id')
            ->innerJoin("pp.cttipoidentificacion", 'ct', 'WITH', 'pp.cttipoidentificacion_id=ct.id')
            ->leftJoin("HCUEInscripcionBundle:Inscripcion", "inscripcion", "WITH",
                "paciente.id=inscripcion.paciente_id and inscripcion.activo=:activo")
            ->leftJoin("inscripcion.entidad", "ent", "WITH", "ent.id=inscripcion.entidad_id")
            ->where("pp.activo=:activo and paciente.activo=:activo")
            ->orderBy("inscripcion.id","desc")
            ->setParameter('activo', Constantes::CT_ESTADOGENERALACTIVO);

        return $qbr;
    }

    /**
     * Obtiene los pacientes registrados el dia de hoy
     * @return arrayresult
     */
    public function getPacienteRegisteredToday()
    {
        $hoy = date("Y-m-d");
        $qb = $this->createQueryBuilder('pt');
        $qb->select("COUNT (pt.id) CANTIDAD,'Hoy' DESCRIPCION")
            ->where("TO_CHAR(pt.fechacreacion, 'YYYY-MM-DD')='$hoy'")
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene los pacientes registrados el dia de ayer
     * @return arrayresult
     */
    public function getPacienteRegisteredYesterday()
    {
        $hoy = date("Y-m-d");
        $ayer = date("Y-m-d", strtotime("-1 day", strtotime($hoy)));
        $qb = $this->createQueryBuilder('pt');
        $qb->select("COUNT (pt.id) CANTIDAD,'Ayer' DESCRIPCION")
            ->where("TO_CHAR(pt.fechacreacion, 'YYYY-MM-DD')='$ayer'")
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene los pacientes registrados la semana pasada
     * @return arrayresult
     */
    public function getPacienteRegisteredLastWeek()
    {
        $hoy = date("Y-m-d");
        $ayer = date("Y-m-d", strtotime("-1 day", strtotime($hoy)));
        $anteriorsemana = date("Y-m-d", strtotime("-7 day", strtotime($hoy)));
        $qb = $this->createQueryBuilder('pt');
        $qb->select("COUNT (pt.id) CANTIDAD,'Hace siete dias' DESCRIPCION")
            ->where("TO_CHAR(pt.fechacreacion, 'YYYY-MM-DD') BETWEEN '$anteriorsemana' AND '$ayer'")
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene los pacientes registrados la semana pasada
     * @return arrayresult
     */
    public function getPacienteRegisteredLastMonth()
    {
        $mes = intval(date("m"));
        $anteriormes = ($mes == 1) ? '12' : str_pad($mes - 1, 2, '0', STR_PAD_LEFT);
        $qb = $this->createQueryBuilder('pt');
        $qb->select("COUNT (pt.id) CANTIDAD,'Último mes' DESCRIPCION")
            ->where("TO_CHAR(pt.fechacreacion, 'MM') = '$anteriormes'")
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene todos los pacientes registrados
     * @return arrayresult
     */
    public function getPacienteAllRegistered()
    {
        $qb = $this->createQueryBuilder('pt');
        $qb->select("COUNT (pt.id) CANTIDAD,'Todos' DESCRIPCION")
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Obtiene la lista de pacientes por medio del arreglo de identificaciones
     * @return Array Lista de Objetos de tipo "\HCUE\PacienteBundle\Entity\Paciente"
     */
    public function getPacienteByArray($arraypersona_id, $arrayResult = false) {
        $query=$this->createQueryBuilder('pt')
            ->select("pt")
            ->Where("pt.persona_id in(:arraypersona_id)")
            ->setParameter('arraypersona_id', $arraypersona_id)
        ;

        return ($arrayResult) ? $query->getQuery()->getArrayResult() : $query->getQuery()->getResult();
    }
}

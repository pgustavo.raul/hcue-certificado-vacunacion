<?php 
/*
* @autor Jipson Montalbán
*/

namespace HCUE\PacienteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class DbcontadorcertificadoRepository extends EntityRepository{

	/**
        * Obtiene el QueryBuilder de la lista de Db_contadorcertificado            
        * @return QueryBuilder  
        */
	public function getDb_contadorcertificadoQueryBuilder() {
                return $this->createQueryBuilder('t')
                            ->select('t');                                                 
        }

}
?>
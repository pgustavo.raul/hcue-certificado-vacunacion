<?php

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Core\AppBundle\Entity\Catalogo;
use Core\AppBundle\Entity\Profesion;
use Core\AppBundle\Entity\Persona;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Descripcion de paciente
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.paciente")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\PacienteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Paciente
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.paciente_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $madrepcte_id
     * @ORM\Column(name="madrepcte_id", type="integer", nullable=true)
     **/
    private $madrepcte_id;

    /**
     * @ORM\ManyToOne(targetEntity="Madrepaciente", cascade={"persist"})
     * @ORM\JoinColumn(name="madrepcte_id", referencedColumnName="id")
     **/
    private $madrepcte;

    /**
     * @var integer $persona_id
     * @ORM\Column(name="persona_id", type="integer", nullable=false)
     * */
    private $persona_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Persona" )
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     * */
    private $persona;

    /**
     * @var string $numerohistoriaclinica
     * @ORM\Column(name="numerohistoriaclinica", type="string", nullable=true)
     * */
    private $numerohistoriaclinica;

    /**
     * @var string $tipopresentante
     * @ORM\Column(name="tiporepresentante", type="integer", nullable=true)
     * */
    private $tiporepresentante;

    /**
     * @var string $numeropresentante
     * @ORM\Column(name="numerorepresentante", type="string", nullable=true)
     * */
    private $numerorepresentante;

    /**
     * @var string $celular
     * @ORM\Column(name="celular", type="string", nullable=true)
     * */
    private $celular;

    /**
     * @var string $telefono
     * @ORM\Column(name="telefono", type="string",length=15, nullable=true)
     * */
    private $telefono;

    /**
     * @var string $ctetnia_id
     * @ORM\Column(name="ctetnia_id", type="integer", nullable=true)
     * */
    private $ctetnia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctetnia_id", referencedColumnName="id")
     * */
    private $ctetnia;

    /**
     * @var string $ctnacionalidadetnica_id
     * @ORM\Column(name="ctnacionalidadetnica_id", type="integer", nullable=true)
     * */
    private $ctnacionalidadetnica_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctnacionalidadetnica_id", referencedColumnName="id")
     * */
    private $ctnacionalidadetnica;

    /**
     * @var string $ctpueblo_id
     * @ORM\Column(name="ctpueblo_id", type="integer", nullable=true)
     * */
    private $ctpueblo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctpueblo_id", referencedColumnName="id")
     * */
    private $ctpueblo;

    /**
     * @var string $ctestadoniveleducacion_id
     * @ORM\Column(name="ctestadoniveleducacion_id", type="integer", nullable=false)
     * */
    private $ctestadoniveleducacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestadoniveleducacion_id", referencedColumnName="id")
     * */
    private $ctestadoniveleducacion;

    /**
     * @var string $pais_id
     * @ORM\Column(name="pais_id", type="integer", nullable=false)
     * */
    private $pais_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Pais")
     * @ORM\JoinColumn(name="pais_id", referencedColumnName="id")
     * */
    private $pais;

    /**
     * @var string $parroquia_id
     * @ORM\Column(name="parroquia_id", type="integer", nullable=true)
     * */
    private $parroquia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Parroquia")
     * @ORM\JoinColumn(name="parroquia_id", referencedColumnName="id")
     * */
    private $parroquia;

    /**
     * @var string $calleprincipal
     * @ORM\Column(name="calleprincipal", type="string",length=500, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo calleprincipal")
     * */
    private $calleprincipal;

    /**
     * @var string numero
     * @ORM\Column(name="numeroresidencia", type="string",length=5, nullable=true)
     * */
    private $numero;

    /**
     * @var string $callesecundaria
     * @ORM\Column(name="callesecundaria", type="string",length=500, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo callesecundaria")
     * */
    private $callesecundaria;

    /**
     * @var string $referenciaresidencia
     * @ORM\Column(name="referenciaresidencia", type="string",length=500, nullable=true)
     * */
    private $referenciaresidencia;

    /**
     * @var string $cttiposegurosalud_id
     * @ORM\Column(name="cttiposegurosalud_id", type="integer", nullable=false)
     * */
    private $cttiposegurosalud_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttiposegurosalud_id", referencedColumnName="id")
     * */
    private $cttiposegurosalud;

    /**
     * @var string $segurosaludsecundario
     * @ORM\Column(name="segurosaludsecundario", type="string",length=50, nullable=true)
     * */
    private $segurosaludsecundario;

    /**
     * @var string $barrio
     * @ORM\Column(name="barrio", type="string",length=200, nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo barrio")
     * */
    private $barrio;

    /**
     * @var int $cttipoempresatrabajo_id
     * @ORM\Column(name="cttipoempresatrabajo_id", type="integer", nullable=false)
     * */
    private $cttipoempresatrabajo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipoempresatrabajo_id", referencedColumnName="id")
     * */
    private $cttipoempresatrabajo;

    /**
     * @var string $profesion_id
     * @ORM\Column(name="profesion_id", type="integer", nullable=false)
     * */
    private $profesion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Profesion")
     * @ORM\JoinColumn(name="profesion_id", referencedColumnName="id")
     * */
    private $profesion;

    /**
     * @var string $ctdiscapacidad_id
     * @ORM\Column(name="ctdiscapacidad_id", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo discapacidad")
     * */
    private $ctdiscapacidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctdiscapacidad_id", referencedColumnName="id")
     * */
    private $ctdiscapacidad;

    /**
     * @var string $cttipobono_id
     * @ORM\Column(name="cttipobono_id", type="integer", nullable=false)
     * */
    private $cttipobono_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipobono_id", referencedColumnName="id")
     * */
    private $cttipobono;

    /**
     * @var string $nombrecontacto
     * @ORM\Column(name="nombrecontacto", type="string",length=150, nullable=true)
     * */
    private $nombrecontacto;

    /**
     * @var string $telefonocontacto
     * @ORM\Column(name="telefonocontacto", type="string",length=15, nullable=true)
     * */
    private $telefonocontacto;

    /**
     * @var string $direccioncontacto
     * @ORM\Column(name="direccioncontacto", type="string",length=500, nullable=true)
     * */
    private $direccioncontacto;

    /**
     * @var string $ctparentezcocontacto_id
     * @ORM\Column(name="ctparentezcocontacto_id", type="integer", nullable=true)
     * */
    private $ctparentezcocontacto_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctparentezcocontacto_id", referencedColumnName="id")
     * */
    private $ctparentezcocontacto;

    /**
     * @var \Datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fecha de creacion")
     * */
    private $fechacreacion;

    /**
     * @var \Datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var string $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo usuario de creacion")
     * */
    private $usuariocreacion_id;

    /**
     * @var string $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     * */
    private $estado;

    /**
     * @var integer $activo
     * @ORM\Column(name="activo", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo activo")
     * */
    private $activo;

    /**
     * @var string $correo
     * @ORM\Column(name="correo", type="string",length=100, nullable=true)
     * */
    private $correo;

    /**
     * @var integer $porcentajediscapacidad
     * @ORM\Column(name="porcentajediscapacidad", type="integer", nullable=true)
     * */
    private $porcentajediscapacidad;


    /**
     * @var string $cttipodiscapacidad_id
     * @ORM\Column(name="cttipodiscapacidad_id", type="integer", nullable=true)
     * */
    private $cttipodiscapacidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipodiscapacidad_id", referencedColumnName="id")
     * */
    private $cttipodiscapacidad;

    /**
     * @var int $ctgruposanguineo_id
     * @ORM\Column(name="ctgruposanguineo_id", type="integer", nullable=true)
     * */
    private $ctgruposanguineo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctgruposanguineo_id", referencedColumnName="id", nullable=true)
     * */
    private $ctgruposanguineo;

    /**
     * @ORM\OneToMany(targetEntity="Archivo", mappedBy="paciente",cascade={"persist"})
     * */
    private $archivos;


    /**
     * @var string $ctparentescomadre_id
     * @ORM\Column(name="ctparentescomadre_id", type="integer", nullable=true)
     * */
    private $ctparentescomadre_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctparentescomadre_id", referencedColumnName="id")
     * */
    private $ctparentescomadre;


    /**
     * @var integer $poractualizar
     * @ORM\Column(name="poractualizar", type="integer", nullable=true)
     * */
    private $poractualizar;

    /**
	* @var integer $ctorigen_id
	* @ORM\Column(name="ctorigen_id", type="integer", nullable=true)
	**/
	private $ctorigen_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctorigen_id", referencedColumnName="id")
     **/
    private $ctorigen;

    public function __construct()
    {
        $this->archivos = new ArrayCollection();
    }


    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set madrepcte_id
     * @param integer madrepcte_id
     * @return paciente
     **/
    public function setMadrepcteId($madrepcte_id){
        $this->madrepcte_id = $madrepcte_id;
    }

    /**
     * Get madrepcte_id
     * @return integer
     **/
    public function getMadrepcteId(){
        return $this->madrepcte_id;
    }

    /**
     * Set madrepcte
     * @param \HCUE\PacienteBundle\Entity\Madrepaciente $madrepcte
     **/
    public function setMadrepcte(\HCUE\PacienteBundle\Entity\Madrepaciente $madrepcte = null){
        $this->madrepcte = $madrepcte;
        return $this;
    }

    /**
     * Get madrepcte
     * @return \HCUE\PacienteBundle\Entity\Madrepaciente
     **/
    public function getMadrepcte(){
        return $this->madrepcte;
    }

    /**
     * Set persona_id
     * @param integer $persona_id
     * @return paciente
     * */
    public function setPersonaId($persona_id)
    {
        $this->persona_id = $persona_id;
        return $this;
    }

    /**
     * Get persona_id
     * @return integer
     * */
    public function getPersonaId()
    {
        return $this->persona_id;
    }

    /**
     * Set persona
     * @param Persona $persona
     * @return paciente
     * */
    public function setPersona(Persona $persona)
    {
        $this->persona = $persona;
        return $this;
    }

    /**
     * Get persona
     * @return Persona
     * */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * Set numerohistoriaclinica
     * @param string $numerohistoriaclinica
     * @return paciente
     * */
    public function setNumerohistoriaclinica($numerohistoriaclinica)
    {
        $this->numerohistoriaclinica = $numerohistoriaclinica;
        return $this;
    }

    /**
     * Get numerohistoriaclinica
     * @return string
     * */
    public function getNumerohistoriaclinica()
    {
        return $this->numerohistoriaclinica;
    }

    /**
     * Set tiporepresentante
     * @param integer $tiporepresentante
     * @return paciente
     * */
    public function setTiporepresentante($tiporepresentante)
    {
        $this->tiporepresentante = $tiporepresentante;
        return $this;
    }

    /**
     * Get tiporepresentante
     * @return integer
     * */
    public function getTiporepresentante()
    {
        return $this->tiporepresentante;
    }

    /**
     * Set numerorepresentante
     * @param string $numerorepresentante
     * @return paciente
     * */
    public function setNumerorepresentante($numerorepresentante)
    {
        $this->numerorepresentante = $numerorepresentante;
        return $this;
    }

    /**
     * Get numerorepresentante
     * @return string
     * */
    public function getNumerorepresentante()
    {
        return $this->numerorepresentante;
    }

    /**
     * Set celular
     * @param string $celular
     * @return paciente
     * */
    public function setCelular($celular)
    {
        $this->celular = $celular;
        return $this;
    }

    /**
     * Get celular
     * @return string
     * */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set telefono
     * @param string $telefono
     * @return paciente
     * */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
        return $this;
    }

    /**
     * Get telefono
     * @return string
     * */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set ctetnia_id
     * @param integer $ctetnia_id
     * @return paciente
     * */
    public function setCtetniaId($ctetnia_id)
    {
        $this->ctetnia_id = $ctetnia_id;
        return $this;
    }

    /**
     * Get ctetnia_id
     * @return integer
     * */
    public function getCtetniaId()
    {
        return $this->ctetnia_id;
    }

    /**
     * Set ctetnia
     * @param Catalogo $ctetnia
     * @return paciente
     * */
    public function setCtetnia(Catalogo $ctetnia)
    {
        $this->ctetnia = $ctetnia;
        return $this;
    }

    /**
     * Get ctetnia
     * @return Catalogo
     * */
    public function getCtetnia()
    {
        return $this->ctetnia;
    }

    /**
     * Set ctnacionalidadetnica_id
     * @param integer $ctnacionalidadetnica_id
     * @return paciente
     * */
    public function setCtnacionalidadetnicaId($ctnacionalidadetnica_id)
    {
        $this->ctnacionalidadetnica_id = $ctnacionalidadetnica_id;
        return $this;
    }

    /**
     * Get ctnacionalidadetnica_id
     * @return integer
     * */
    public function getCtnacionalidadetnicaId()
    {
        return $this->ctnacionalidadetnica_id;
    }

    /**
     * Set ctnacionalidadetnica
     * @param Catalogo $ctnacionalidadetnica
     * @return paciente
     * */
    public function setCtnacionalidadetnica(
        Catalogo $ctnacionalidadetnica = null
    ) {
        $this->ctnacionalidadetnica = $ctnacionalidadetnica;
        return $this;
    }

    /**
     * Get ctnacionalidadetnica
     * @return Catalogo
     * */
    public function getCtnacionalidadetnica()
    {
        return $this->ctnacionalidadetnica;
    }

    /**
     * Set ctpueblo_id
     * @param integer $ctpueblo_id
     * @return paciente
     * */
    public function setCtpuebloId($ctpueblo_id)
    {
        $this->ctpueblo_id = $ctpueblo_id;
        return $this;
    }

    /**
     * Get ctpueblo_id
     * @return integer
     * */
    public function getCtpuebloId()
    {
        return $this->ctpueblo_id;
    }

    /**
     * Set ctpueblo
     * @param Catalogo $ctpueblo
     * @return paciente
     * */
    public function setCtpueblo(Catalogo $ctpueblo = null)
    {
        $this->ctpueblo = $ctpueblo;
        return $this;
    }

    /**
     * Get ctpueblo
     * @return Catalogo
     * */
    public function getCtpueblo()
    {
        return $this->ctpueblo;
    }

    /**
     * Set ctestadoniveleducacion_id
     * @param integer $ctestadoniveleducacion_id
     * @return paciente
     * */
    public function setCtestadoniveleducacionId($ctestadoniveleducacion_id)
    {
        $this->ctestadoniveleducacion_id = $ctestadoniveleducacion_id;
        return $this;
    }

    /**
     * Get ctestadoniveleducacion_id
     * @return integer
     * */
    public function getCtestadoniveleducacionId()
    {
        return $this->ctestadoniveleducacion_id;
    }

    /**
     * Set ctestadoniveleducacion
     * @param Catalogo $ctestadoniveleducacion
     * @return paciente
     * */
    public function setCtestadoniveleducacion(Catalogo $ctestadoniveleducacion)
    {
        $this->ctestadoniveleducacion = $ctestadoniveleducacion;
        return $this;
    }

    /**
     * Get ctestadoniveleducacion
     * @return Catalogo
     * */
    public function getCtestadoniveleducacion()
    {
        return $this->ctestadoniveleducacion;
    }

    /**
     * Set pais_id
     * @param integer $pais_id
     * @return paciente
     * */
    public function setPaisId($pais_id)
    {
        $this->pais_id = $pais_id;
        return $this;
    }

    /**
     * Get pais_id
     * @return integer
     * */
    public function getPaisId()
    {
        return $this->pais_id;
    }

    /**
     * Set pais
     * @param \Core\AppBundle\Entity\Pais $pais
     * @return paciente
     * */
    public function setPais(\Core\AppBundle\Entity\Pais $pais)
    {
        $this->pais = $pais;
        return $this;
    }

    /**
     * Get pais
     * @return \Core\AppBundle\Entity\Pais
     * */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set parroquia_id
     * @param integer $parroquia_id
     * @return paciente
     * */
    public function setParroquiaId($parroquia_id)
    {
        $this->parroquia_id = $parroquia_id;
        return $this;
    }

    /**
     * Get parroquia_id
     * @return integer
     * */
    public function getParroquiaId()
    {
        return $this->parroquia_id;
    }

    /**
     * Set parroquia
     * @param \Core\AppBundle\Entity\Parroquia $parroquia
     * @return paciente
     * */
    public function setParroquia(\Core\AppBundle\Entity\Parroquia $parroquia=null)
    {
        $this->parroquia = $parroquia;
        return $this;
    }

    /**
     * Get parroquia
     * @return \Core\AppBundle\Entity\Parroquia
     * */
    public function getParroquia()
    {
        return $this->parroquia;
    }

    /**
     * Set calleprincipal
     * @param string $calleprincipal
     * @return paciente
     * */
    public function setCalleprincipal($calleprincipal)
    {
        $this->calleprincipal = $calleprincipal;
        return $this;
    }

    /**
     * Get calleprincipal
     * @return string
     * */
    public function getCalleprincipal()
    {
        return $this->calleprincipal;
    }

    /**
     * Set numero
     * @param string $numero
     * @return paciente
     * */
    public function setNumero($numero)
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * Get numero
     * @return string
     * */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set callesecundaria
     * @param string $callesecundaria
     * @return paciente
     * */
    public function setCallesecundaria($callesecundaria)
    {
        $this->callesecundaria = $callesecundaria;
        return $this;
    }

    /**
     * Get callesecundaria
     * @return string
     * */
    public function getCallesecundaria()
    {
        return $this->callesecundaria;
    }

    /**
     * Set referenciaresidencia
     * @param string $referenciaresidencia
     * @return paciente
     * */
    public function setReferenciaresidencia($referenciaresidencia)
    {
        $this->referenciaresidencia = $referenciaresidencia;
        return $this;
    }

    /**
     * Get referenciaresidencia
     * @return string
     * */
    public function getReferenciaresidencia()
    {
        return $this->referenciaresidencia;
    }

    /**
     * Set cttiposegurosalud_id
     * @param integer $cttiposegurosalud_id
     * @return paciente
     * */
    public function setCttiposegurosaludId($cttiposegurosalud_id)
    {
        $this->cttiposegurosalud_id = $cttiposegurosalud_id;
        return $this;
    }

    /**
     * Get cttiposegurosalud_id
     * @return integer
     * */
    public function getCttiposegurosaludId()
    {
        return $this->cttiposegurosalud_id;
    }

    /**
     * Set cttiposegurosalud
     * @param Catalogo $cttiposegurosalud
     * @return paciente
     * */
    public function setCttiposegurosalud(Catalogo $cttiposegurosalud)
    {
        $this->cttiposegurosalud = $cttiposegurosalud;
        return $this;
    }

    /**
     * Get cttiposegurosalud
     * @return Catalogo
     * */
    public function getCttiposegurosalud()
    {
        return $this->cttiposegurosalud;
    }

    /**
     * Set barrio
     * @param string $barrio
     * @return paciente
     * */
    public function setBarrio($barrio)
    {
        $this->barrio = $barrio;
        return $this;
    }

    /**
     * Get barrio
     * @return string
     * */
    public function getBarrio()
    {
        return $this->barrio;
    }

    /**
     * Set segurosaludsecundario
     * @param string $segurosaludsecundario
     * @return paciente
     * */
    public function setSegurosaludsecundario($segurosaludsecundario)
    {
        $this->segurosaludsecundario = $segurosaludsecundario;
        return $this;
    }

    /**
     * Get segurosaludsecundario
     * @return string
     * */
    public function getSegurosaludsecundario()
    {
        return $this->segurosaludsecundario;
    }

    /**
     * Set cttipoempresatrabajo_id
     * @param integer $cttipoempresatrabajo_id
     * @return paciente
     * */
    public function setCttipoempresatrabajoId($cttipoempresatrabajo_id)
    {
        $this->cttipoempresatrabajo_id = $cttipoempresatrabajo_id;
        return $this;
    }

    /**
     * Get cttipoempresatrabajo_id
     * @return integer
     * */
    public function getCttipoempresatrabajoId()
    {
        return $this->cttipoempresatrabajo_id;
    }

    /**
     * Set cttipoempresatrabajo
     * @param Catalogo $cttipoempresatrabajo
     * @return paciente
     * */
    public function setCttipoempresatrabajo(Catalogo $cttipoempresatrabajo)
    {
        $this->cttipoempresatrabajo = $cttipoempresatrabajo;
        return $this;
    }

    /**
     * Get Catalogo
     * @return Catalogo
     * */
    public function getCttipoempresatrabajo()
    {
        return $this->cttipoempresatrabajo;
    }

    /**
     * Set profesion_id
     * @param integer $profesion_id
     * @return paciente
     * */
    public function setProfesionId($profesion_id)
    {
        $this->profesion_id = $profesion_id;
        return $this;
    }

    /**
     * Get profesion_id
     * @return integer
     * */
    public function getProfesionId()
    {
        return $this->profesion_id;
    }

    /**
     * Set profesion
     * @param Profesion $profesion
     * @return paciente
     * */
    public function setProfesion(Profesion $profesion)
    {
        $this->profesion = $profesion;
        return $this;
    }

    /**
     * Get profesion
     * @return Profesion
     * */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set ctdiscapacidad_id
     * @param integer $discapacidad_id
     * @return paciente
     * */
    public function setCtdiscapacidadId($ctdiscapacidad_id)
    {
        $this->cttipodiscapacidad_id = $ctdiscapacidad_id;
        return $this;
    }

    /**
     * Get ctdiscapacidad_id
     * @return integer
     * */
    public function getCtdiscapacidadId()
    {
        return $this->ctdiscapacidad_id;
    }

    /**
     * Set ctdiscapacidad
     * @param Catalogo $ctdiscapacidad
     * @return paciente
     * */
    public function setCtdiscapacidad(Catalogo $ctdiscapacidad)
    {
        $this->ctdiscapacidad = $ctdiscapacidad;
        return $this;
    }

    /**
     * Get ctdiscapacidad
     * @return Catalogo
     * */
    public function getCtdiscapacidad()
    {
        return $this->ctdiscapacidad;
    }

    /**
     * Set cttipobono_id
     * @param integer $cttipobono_id
     * @return paciente
     * */
    public function setCttipobonoId($cttipobono_id)
    {
        $this->cttipobono_id = $cttipobono_id;
        return $this;
    }

    /**
     * Get cttipobono_id
     * @return integer
     * */
    public function getCttipobonoId()
    {
        return $this->cttipobono_id;
    }

    /**
     * Set cttipobono
     * @param Catalogo $cttipobono
     * @return paciente
     * */
    public function setCttipobono(Catalogo $cttipobono)
    {
        $this->cttipobono = $cttipobono;
        return $this;
    }

    /**
     * Get cttipobono
     * @return Catalogo
     * */
    public function getCttipobono()
    {
        return $this->cttipobono;
    }

    /**
     * Set nombrecontacto
     * @param string $nombrecontacto
     * @return paciente
     * */
    public function setNombrecontacto($nombrecontacto)
    {
        $this->nombrecontacto = $nombrecontacto;
        return $this;
    }

    /**
     * Get nombrecontacto
     * @return string
     * */
    public function getNombrecontacto()
    {
        return $this->nombrecontacto;
    }

    /**
     * Set telefonocontacto
     * @param string $telefonocontacto
     * @return paciente
     * */
    public function setTelefonocontacto($telefonocontacto)
    {
        $this->telefonocontacto = $telefonocontacto;
        return $this;
    }

    /**
     * Get telefonocontacto
     * @return string
     * */
    public function getTelefonocontacto()
    {
        return $this->telefonocontacto;
    }

    /**
     * Set direccioncontacto
     * @param string $direccioncontacto
     * @return paciente
     * */
    public function setDireccioncontacto($direccioncontacto)
    {
        $this->direccioncontacto = $direccioncontacto;
        return $this;
    }

    /**
     * Get direccioncontacto
     * @return string
     * */
    public function getDireccioncontacto()
    {
        return $this->direccioncontacto;
    }

    /**
     * Set ctparentezcocontacto_id
     * @param integer $ctparentezcocontacto_id
     * @return paciente
     * */
    public function setCtparentezcocontactoId($ctparentezcocontacto_id)
    {
        $this->ctparentezcocontacto_id = $ctparentezcocontacto_id;
        return $this;
    }

    /**
     * Get ctparentezcocontacto_id
     * @return integer
     * */
    public function getCtparentezcocontactoId()
    {
        return $this->ctparentezcocontacto_id;
    }

    /**
     * Set ctparentezcocontacto
     * @param Catalogo $ctparentezcocontacto
     * @return paciente
     * */
    public function setCtparentezcocontacto(Catalogo $ctparentezcocontacto)
    {
        $this->ctparentezcocontacto = $ctparentezcocontacto;
        return $this;
    }

    /**
     * Get ctparentezcocontacto
     * @return Catalogo
     * */
    public function getCtparentezcocontacto()
    {
        return $this->ctparentezcocontacto;
    }

    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return paciente
     * */
    public function setFechacreacion()
    {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return \Datetime
     * */
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Set fechamodificacion
     * @ORM\PreUpdate
     * @return paciente
     * */
    public function setFechamodificacion()
    {
        $this->fechamodificacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechamodificacion
     * @return \Datetime
     * */
    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer $usuariocreacion_id
     * @return paciente
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer $usuariomodificacion_id
     * @return paciente
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }


    /**
     * Set estado
     * @param integer $estado
     * @return paciente
     **/
    public function setEstado($estado = 1)
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set Activo
     * @param integer $activo
     * @return paciente
     **/
    public function setActivo($activo = 1)
    {
        $this->activo = $activo;
        return $this;
    }

    /**
     * Get activo
     * @return integer
     * */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set Correo
     * @param string $correo
     * @return paciente
     **/
    public function setCorreo($correo)
    {
        $this->correo = $correo;
        return $this;
    }

    /**
     * Get Correo
     * @return string
     * */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set PorcentajeDiscapacidad
     * @param integer $porcentajediscapacidad
     * @return paciente
     **/
    public function setPorcentajeDiscapacidad($porcentajediscapacidad)
    {
        $this->porcentajediscapacidad = $porcentajediscapacidad;
        return $this;
    }

    /**
     * Get PorcentajeDiscapacidad
     * @return integer
     * */
    public function getPorcentajeDiscapacidad()
    {
        return $this->porcentajediscapacidad;
    }


    /**
     * Set cttipodiscapaidad_id
     * @param integer $cttipodiscapaidad_id
     * @return paciente
     * */
    public function setCttipodiscapacidadId($cttipodiscapaidad_id)
    {
        $this->cttipodiscapacidad_id = $cttipodiscapaidad_id;
        return $this;
    }

    /**
     * Get cttipodiscapacidad_id
     * @return integer
     * */
    public function getCttipodiscapacidadId()
    {
        return $this->cttipodiscapacidad_id;
    }

    /**
     * Set cttipodiscapacidad_id
     * @param Catalogo $cttipodiscapacidad
     * @return paciente
     * */
    public function setCttipodiscapacidad(Catalogo $cttipodiscapacidad)
    {
        $this->cttipodiscapacidad = $cttipodiscapacidad;
        return $this;
    }

    /**
     * Get cttipodiscapacidad
     * @return Catalogo
     * */
    public function getCttipodiscapacidad()
    {
        return $this->cttipodiscapacidad;
    }

        /**
     * Set poractualizar
     * @param integer $poractualizar
     * @return persona
     **/
    public function setPoractualizar($poractualizar = 1)
    {
        $this->poractualizar = $poractualizar;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getPoractualizar()
    {
        return $this->poractualizar;
    }

    /**
     * Set ctgruposanguineo_id
     * @param integer $ctgruposanguineo_id
     * @return paciente
     * */
    public function setCtgruposanguineoId($ctgruposanguineo_id)
    {
        $this->ctgruposanguineo_id = $ctgruposanguineo_id;
        return $this;
    }

    /**
     * Get ctgruposanguineo_id
     * @return integer
     * */
    public function getCtgruposanguineoId()
    {
        return $this->ctgruposanguineo_id;
    }

    /**
     * Set ctgruposanguineo
     * @param Catalogo $ctgruposanguineo
     * @return paciente
     * */
    public function setCtgruposanguineo(Catalogo $ctgruposanguineo = null)
    {
        $this->ctgruposanguineo = $ctgruposanguineo;
        return $this;
    }

    /**
     * Get ctgruposanguineo
     * @return Catalogo
     * */
    public function getCtgruposanguineo()
    {
        return $this->ctgruposanguineo;
    }


    /**
     * Set ctparentescomadre_id
     * @param integer $ctparentescomadre_id
     * @return paciente
     * */
    public function setCtparentescomadreId($ctparentescomadre_id)
    {
        $this->ctparentescomadre_id = $ctparentescomadre_id;
        return $this;
    }

    /**
     * Get ctparentescomadre_id
     * @return integer
     * */
    public function getCtparentescomadreId()
    {
        return $this->ctparentescomadre_id;
    }

    /**
     * Set ctparentescomadre
     * @param Catalogo $ctparentescomadre
     * @return paciente
     * */
    public function setCtparentescomadre(
        Catalogo $ctparentescomadre = null
    ) {
        $this->ctparentescomadre = $ctparentescomadre;
        return $this;
    }

    /**
     * Get ctparentescomadre
     * @return Catalogo
     * */
    public function getCtparentescomadre()
    {
        return $this->ctparentescomadre;
    }

    /**
     * Set unicpacienteid
     * @param string $unicpacienteid
     * @return paciente
     * */
    public function setUnicpacienteid($unicpacienteid)
    {
        $this->unicpacienteid = $unicpacienteid;
        return $this;
    }

    /**
     * Get unicpacienteid
     * @return string
     * */
    public function getUnicpacienteid()
    {
        return $this->unicpacienteid;
    }

    /**
	* Set ctorigen_id
	* @param integer ctorigen_id
	* @return paciente
	**/
	public function setCtorigenId($ctorigen_id){
        $this->ctorigen_id = $ctorigen_id;
        return $this;
    }

   /**
   * Get ctorigen_id
   * @return integer
   **/
   public function getCtorigenId(){
        return $this->ctorigen_id;
    }

    /**
     * Set ctorigen
     * @param Catalogo $ctorigen
     **/
    public function setCtorigen(Catalogo $ctorigen)
    {
        $this->ctorigen = $ctorigen;
        return $this;
    }

    /**
     * Get ctorigen
     * @return Catalogo
     **/
    public function getCtorigen()
    {
        return $this->ctorigen;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
        $this->estado = 1;
        $this->activo = 1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
        $this->poractualizar = null;
    }
}

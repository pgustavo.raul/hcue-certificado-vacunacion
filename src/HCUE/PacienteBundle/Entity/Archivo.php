<?php

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use HCUE\PacienteBundle\Entity\Paciente;

/**
 * Descripcion de paciente
 * @author Richard Veliz
 * @ORM\Table(name="hcue_amed.pacientearchivo")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\ArchivoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Archivo
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.pacientearchivo_id_seq", allocationSize=1, initialValue=1)
     * */
    private $id;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     * */
    private $entidad_id;

    /**
     * @var integer $paciente_id
     * @ORM\Column(name="paciente_id", type="integer", nullable=false)
     * */
    private $paciente_id;

    /**
     * @ORM\ManyToOne(targetEntity="Paciente", inversedBy="archivos")
     * @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     * */
    private $paciente;

    /**
     * @var string $numeroarchivo
     * @ORM\Column(name="numeroarchivo", type="string", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo Número de Archivo")
     * * */
    private $numeroarchivo;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo fecha de creacion")
     * */
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     * */
    private $fechamodificacion;

    /**
     * @var string $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo usuario de creacion")
     * */
    private $usuariocreacion_id;

    /**
     * @var string $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     * */
    private $usuariomodificacion_id;

    /**
     * @var string $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     * */
    private $estado;


    /**
     * Get id
     * @return integer
     * */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return archivo
     * */
    public function setEntidadId($entidad_id)
    {
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     * */
    public function getEntidadId()
    {
        return $this->entidad_id;
    }

    /**
     * Set paciente_id
     * @param integer paciente_id
     * @return archivo
     * */
    public function setPacienteId($paciente_id)
    {
        $this->paciente_id = $paciente_id;
        return $this;
    }

    /**
     * Get paciente_id
     * @return integer
     * */
    public function getPacienteId()
    {
        return $this->paciente_id;
    }

    /**
     * Set paciente
     * @param Paciente $paciente
     * */
    public function setPaciente(Paciente $paciente)
    {
        $this->paciente = $paciente;
        return $this;
    }

    /**
     * Get paciente
     * @return Paciente
     * */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set numeroarchivo
     * @param string numeroarchivo
     * @return archivo
     * */
    public function setNumeroArchivo($numeroarchivo)
    {
        $this->numeroarchivo = $numeroarchivo;
        return $this;
    }

    /**
     * Get numeroarchivo
     * @return string
     * */
    public function getNumeroArchivo()
    {
        return $this->numeroarchivo;
    }


    /**
     * Set fechacreacion
     * @ORM\PrePersist
     * @return archivo
     * */
    public function setFechacreacion()
    {
        $this->fechacreacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechacreacion
     * @return datetime
     * */
    public function getFechacreacion()
    {
        return $this->fechacreacion;
    }

    /**
     * Set fechamodificacion
     * @ORM\PreUpdate
     * @return archivo
     * */
    public function setFechamodificacion()
    {
        $this->fechamodificacion = new \DateTime();
        return $this;
    }

    /**
     * Get fechamodificacion
     * @return datetime
     * */
    public function getFechamodificacion()
    {
        return $this->fechamodificacion;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return archivo
     * */
    public function setUsuariocreacionId($usuariocreacion_id)
    {
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     * */
    public function getUsuariocreacionId()
    {
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return archivo
     * */
    public function setUsuariomodificacionId($usuariomodificacion_id)
    {
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     * */
    public function getUsuariomodificacionId()
    {
        return $this->usuariomodificacion_id;
    }


    /**
     * Set estado
     * @param integer $estado Estado
     * @return archivo
     **/
    public function setEstado($estado)
    {
        if ($this->estado) {
            $this->estado = $estado;
        }
        return $this;
    }

    /**
     * Get estado
     * @return integer
     * */
    public function getEstado()
    {
        return $this->estado;
    }


    /**
     * @ORM\PrePersist
     **/
    public function prePersist()
    {
        $this->fechacreacion = new \DateTime();
        $this->fechamodificacion = new \DateTime();
        $this->estado = 1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate()
    {
        $this->fechamodificacion = new \DateTime();
    }
}

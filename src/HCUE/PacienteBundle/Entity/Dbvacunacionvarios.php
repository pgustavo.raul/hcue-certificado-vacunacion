<?php 
/*
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Jipson Montalban
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.db_vacunacionvarios")
* @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\DbvacunacionvariosRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Dbvacunacionvarios{

	/**
	* @var string $grupo_riesgo
	* @ORM\Column(name="grupo_riesgo", type="string",length=255, nullable=true)
	**/
	private $grupo_riesgo;

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.certificadovacuna_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $gedad2
	* @ORM\Column(name="gedad2", type="string",length=255, nullable=true)
	**/
	private $gedad2;

	/**
	* @var string $fecha_segunda_dosis
	* @ORM\Column(name="fecha_segunda_dosis", type="string",length=255, nullable=true)
	**/
	private $fecha_segunda_dosis;

	/**
	* @var string $prv_cod
	* @ORM\Column(name="prv_cod", type="string",length=2, nullable=true)
	**/
	private $prv_cod;

	/**
	* @var string $semanas
	* @ORM\Column(name="semanas", type="string",length=255, nullable=true)
	**/
	private $semanas;

	/**
	* @var integer $anio_aplicacion
	* @ORM\Column(name="anio_aplicacion", type="integer", nullable=true)
	**/
	private $anio_aplicacion;

	/**
	* @var integer $mes_aplicacion
	* @ORM\Column(name="mes_aplicacion", type="integer", nullable=true)
	**/
	private $mes_aplicacion;

	/**
	* @var integer $dia_aplicacion
	* @ORM\Column(name="dia_aplicacion", type="integer", nullable=true)
	**/
	private $dia_aplicacion;

	/**
	* @var string $punto_vacunacion
	* @ORM\Column(name="punto_vacunacion", type="string",length=255, nullable=true)
	**/
	private $punto_vacunacion;

	/**
	* @var string $unicodigo
	* @ORM\Column(name="unicodigo", type="string",length=6, nullable=true)
	**/
	private $unicodigo;

	/**
	* @var string $uni_nombre
	* @ORM\Column(name="uni_nombre", type="string",length=255, nullable=true)
	**/
	private $uni_nombre;

	/**
	* @var string $zona
	* @ORM\Column(name="zona", type="string",length=7, nullable=true)
	**/
	private $zona;

	/**
	* @var string $distrito
	* @ORM\Column(name="distrito", type="string",length=6, nullable=true)
	**/
	private $distrito;

	/**
	* @var string $provincia
	* @ORM\Column(name="provincia", type="string",length=255, nullable=true)
	**/
	private $provincia;

	/**
	* @var string $canton
	* @ORM\Column(name="canton", type="string",length=255, nullable=true)
	**/
	private $canton;

	/**
	* @var string $apellidos
	* @ORM\Column(name="apellidos", type="string",length=500, nullable=true)
	**/
	private $apellidos;

	/**
	* @var string $nombres
	* @ORM\Column(name="nombres", type="string",length=500, nullable=true)
	**/
	private $nombres;

	/**
	* @var string $tipo_iden
	* @ORM\Column(name="tipo_iden", type="string",length=255, nullable=true)
	**/
	private $tipo_iden;

	/**
	* @var string $num_iden
	* @ORM\Column(name="num_iden", type="string",length=255, nullable=true)
	**/
	private $num_iden;

	/**
	* @var string $sexo
	* @ORM\Column(name="sexo", type="string",length=7, nullable=true)
	**/
	private $sexo;

	/**
	* @var integer $anio_nacimiento
	* @ORM\Column(name="anio_nacimiento", type="integer", nullable=true)
	**/
	private $anio_nacimiento;

	/**
	* @var integer $mes_nacimiento
	* @ORM\Column(name="mes_nacimiento", type="integer", nullable=true)
	**/
	private $mes_nacimiento;

	/**
	* @var integer $dia_nacimiento
	* @ORM\Column(name="dia_nacimiento", type="integer", nullable=true)
	**/
	private $dia_nacimiento;

	/**
	* @var string $etnia
	* @ORM\Column(name="etnia", type="string",length=255, nullable=true)
	**/
	private $etnia;

	/**
	* @var string $nacionalidad_pueblos
	* @ORM\Column(name="nacionalidad_pueblos", type="string",length=255, nullable=true)
	**/
	private $nacionalidad_pueblos;

	/**
	* @var string $pueblos
	* @ORM\Column(name="pueblos", type="string",length=255, nullable=true)
	**/
	private $pueblos;

	/**
	* @var string $tel_contacto
	* @ORM\Column(name="tel_contacto", type="string",length=255, nullable=true)
	**/
	private $tel_contacto;

	/**
	* @var string $correo_electronico
	* @ORM\Column(name="correo_electronico", type="string",length=500, nullable=true)
	**/
	private $correo_electronico;

	/**
	* @var string $pobla_vacuna_1
	* @ORM\Column(name="pobla_vacuna_1", type="string",length=255, nullable=true)
	**/
	private $pobla_vacuna_1;

	/**
	* @var string $grupo_riesgo_1
	* @ORM\Column(name="grupo_riesgo_1", type="string",length=255, nullable=true)
	**/
	private $grupo_riesgo_1;

	/**
	* @var string $tuvo_covid
	* @ORM\Column(name="tuvo_covid", type="string",length=255, nullable=true)
	**/
	private $tuvo_covid;

	/**
	* @var string $nom_vacuna
	* @ORM\Column(name="nom_vacuna", type="string",length=255, nullable=true)
	**/
	private $nom_vacuna;

	/**
	* @var string $lote_vacuna
	* @ORM\Column(name="lote_vacuna", type="string",length=255, nullable=true)
	**/
	private $lote_vacuna;

	/**
	* @var integer $dosis_aplicada
	* @ORM\Column(name="dosis_aplicada", type="integer", nullable=true)
	**/
	private $dosis_aplicada;

	/**
	* @var string $nom_profesional_aplica
	* @ORM\Column(name="nom_profesional_aplica", type="string",length=255, nullable=true)
	**/
	private $nom_profesional_aplica;

	/**
	* @var string $iden_profesional_aplica
	* @ORM\Column(name="iden_profesional_aplica", type="string",length=255, nullable=true)
	**/
	private $iden_profesional_aplica;

	/**
	* @var string $nom_profesional_registra
	* @ORM\Column(name="nom_profesional_registra", type="string",length=255, nullable=true)
	**/
	private $nom_profesional_registra;

	/**
	* @var string $iden_profesional_registra
	* @ORM\Column(name="iden_profesional_registra", type="string",length=255, nullable=true)
	**/
	private $iden_profesional_registra;

	/**
	* @var string $paciente_agendado
	* @ORM\Column(name="paciente_agendado", type="string",length=255, nullable=true)
	**/
	private $paciente_agendado;

	/**
	* @var integer $fase_vacuna
	* @ORM\Column(name="fase_vacuna", type="integer", nullable=true)
	**/
	private $fase_vacuna;

	/**
	* @var integer $edad_anios
	* @ORM\Column(name="edad_anios", type="integer", nullable=true)
	**/
	private $edad_anios;

	/**
	* @var date $fecha_vacuna
	* @ORM\Column(name="fecha_vacuna", type="datetime", nullable=true)
	**/
	private $fecha_vacuna;

	/**
	* @var string $fuente
	* @ORM\Column(name="fuente", type="string",length=255, nullable=true)
	**/
	private $fuente;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=true)
	**/
	private $estado;

	/**
	* @var string $apellidos_nombres
	* @ORM\Column(name="apellidos_nombres", type="string",length=500, nullable=true)
	**/
	private $apellidos_nombres;

	/**
	* @var string $pobla_vacuna
	* @ORM\Column(name="pobla_vacuna", type="string",length=255, nullable=true)
	**/
	private $pobla_vacuna;

	/**
	* Set grupo_riesgo
	* @param string grupo_riesgo
	* @return db_vacunacion
	**/
	public function setGrupoRiesgo($grupo_riesgo){
		 $this->grupo_riesgo = $grupo_riesgo;
		 return $this;
	}

	/**
	* Get grupo_riesgo
	* @return string
	**/
	public function getGrupoRiesgo(){
		 return $this->grupo_riesgo;
	}

	/**
	* Get grupo
	* @return string
	**/
	public function getGrupo(){
		 return $this->grupo;
	}

	/**
	* Set gedad2
	* @param string gedad2
	* @return db_vacunacion
	**/
	public function setGedad2($gedad2){
		 $this->gedad2 = $gedad2;
		 return $this;
	}

	/**
	* Get gedad2
	* @return string
	**/
	public function getGedad2(){
		 return $this->gedad2;
	}

	/**
	* Set fecha_segunda_dosis
	* @param string fecha_segunda_dosis
	* @return db_vacunacion
	**/
	public function setFecha_segunda_dosis($fecha_segunda_dosis){
		 $this->fecha_segunda_dosis = $fecha_segunda_dosis;
		 return $this;
	}

	/**
	* Get fecha_segunda_dosis
	* @return string
	**/
	public function getFecha_segunda_dosis(){
		 return $this->fecha_segunda_dosis;
	}

	/**
	* Set prv_cod
	* @param string prv_cod
	* @return db_vacunacion
	**/
	public function setPrvCod($prv_cod){
		 $this->prv_cod = $prv_cod;
		 return $this;
	}

	/**
	* Get prv_cod
	* @return string
	**/
	public function getPrvCod(){
		 return $this->prv_cod;
	}

	/**
	* Get prv
	* @return \HCUE\PacienteBundle\Entity\Prv
	**/
	public function getPrv(){
		 return $this->prv;
	}

	/**
	* Set semanas
	* @param string semanas
	* @return db_vacunacion
	**/
	public function setSemanas($semanas){
		 $this->semanas = $semanas;
		 return $this;
	}

	/**
	* Get semanas
	* @return string
	**/
	public function getSemanas(){
		 return $this->semanas;
	}

	/**
	* Set anio_aplicacion
	* @param integer anio_aplicacion
	* @return db_vacunacion
	**/
	public function setAnioAplicacion($anio_aplicacion){
		 $this->anio_aplicacion = $anio_aplicacion;
		 return $this;
	}

	/**
	* Get anio_aplicacion
	* @return integer
	**/
	public function getAnioAplicacion(){
		 return $this->anio_aplicacion;
	}

	/**
	* Get anio
	* @return string
	**/
	public function getAnio(){
		 return $this->anio;
	}

	/**
	* Set mes_aplicacion
	* @param integer mes_aplicacion
	* @return db_vacunacion
	**/
	public function setMesAplicacion($mes_aplicacion){
		 $this->mes_aplicacion = $mes_aplicacion;
		 return $this;
	}

	/**
	* Get mes_aplicacion
	* @return integer
	**/
	public function getMesAplicacion(){
		 return $this->mes_aplicacion;
	}

	/**
	* Set dia_aplicacion
	* @param integer dia_aplicacion
	* @return db_vacunacion
	**/
	public function setDiaAplicacion($dia_aplicacion){
		 $this->dia_aplicacion = $dia_aplicacion;
		 return $this;
	}

	/**
	* Get dia_aplicacion
	* @return integer
	**/
	public function getDiaAplicacion(){
		 return $this->dia_aplicacion;
	}

	/**
	* Get dia
	* @return string
	**/
	public function getDia(){
		 return $this->dia;
	}

	/**
	* Set punto_vacunacion
	* @param string punto_vacunacion
	* @return db_vacunacion
	**/
	public function setPuntoVacunacion($punto_vacunacion){
		 $this->punto_vacunacion = $punto_vacunacion;
		 return $this;
	}

	/**
	* Get punto_vacunacion
	* @return string
	**/
	public function getPuntoVacunacion(){
		 return $this->punto_vacunacion;
	}

	/**
	* Get punto
	* @return string
	**/
	public function getPunto(){
		 return $this->punto;
	}

	/**
	* Set unicodigo
	* @param string unicodigo
	* @return db_vacunacion
	**/
	public function setUnicodigo($unicodigo){
		 $this->unicodigo = $unicodigo;
		 return $this;
	}

	/**
	* Get unicodigo
	* @return string
	**/
	public function getUnicodigo(){
		 return $this->unicodigo;
	}

	/**
	* Set uni_nombre
	* @param string uni_nombre
	* @return db_vacunacion
	**/
	public function setUniNombre($uni_nombre){
		 $this->uni_nombre = $uni_nombre;
		 return $this;
	}

	/**
	* Get uni_nombre
	* @return string
	**/
	public function getUniNombre(){
		 return $this->uni_nombre;
	}

	/**
	* Get uni
	* @return string
	**/
	public function getUni(){
		 return $this->uni;
	}

	/**
	* Set zona
	* @param string zona
	* @return db_vacunacion
	**/
	public function setZona($zona){
		 $this->zona = $zona;
		 return $this;
	}

	/**
	* Get zona
	* @return string
	**/
	public function getZona(){
		 return $this->zona;
	}

	/**
	* Set distrito
	* @param string distrito
	* @return db_vacunacion
	**/
	public function setDistrito($distrito){
		 $this->distrito = $distrito;
		 return $this;
	}

	/**
	* Get distrito
	* @return string
	**/
	public function getDistrito(){
		 return $this->distrito;
	}

	/**
	* Set provincia
	* @param string provincia
	* @return db_vacunacion
	**/
	public function setProvincia($provincia){
		 $this->provincia = $provincia;
		 return $this;
	}

	/**
	* Get provincia
	* @return string
	**/
	public function getProvincia(){
		 return $this->provincia;
	}

	/**
	* Set canton
	* @param string canton
	* @return db_vacunacion
	**/
	public function setCanton($canton){
		 $this->canton = $canton;
		 return $this;
	}

	/**
	* Get canton
	* @return string
	**/
	public function getCanton(){
		 return $this->canton;
	}

	/**
	* Set apellidos
	* @param string apellidos
	* @return db_vacunacion
	**/
	public function setApellidos($apellidos){
		 $this->apellidos = $apellidos;
		 return $this;
	}

	/**
	* Get apellidos
	* @return string
	**/
	public function getApellidos(){
		 return $this->apellidos;
	}

	/**
	* Set nombres
	* @param string nombres
	* @return db_vacunacion
	**/
	public function setNombres($nombres){
		 $this->nombres = $nombres;
		 return $this;
	}

	/**
	* Get nombres
	* @return string
	**/
	public function getNombres(){
		 return $this->nombres;
	}

	/**
	* Set tipo_iden
	* @param string tipo_iden
	* @return db_vacunacion
	**/
	public function setTipoIden($tipo_iden){
		 $this->tipo_iden = $tipo_iden;
		 return $this;
	}

	/**
	* Get tipo_iden
	* @return string
	**/
	public function getTipoIden(){
		 return $this->tipo_iden;
	}

	/**
	* Get tipo
	* @return string
	**/
	public function getTipo(){
		 return $this->tipo;
	}

	/**
	* Set num_iden
	* @param string num_iden
	* @return db_vacunacion
	**/
	public function setNumIden($num_iden){
		 $this->num_iden = $num_iden;
		 return $this;
	}

	/**
	* Get num_iden
	* @return string
	**/
	public function getNumIden(){
		 return $this->num_iden;
	}

	/**
	* Get num
	* @return string
	**/
	public function getNum(){
		 return $this->num;
	}

	/**
	* Set sexo
	* @param string sexo
	* @return db_vacunacion
	**/
	public function setSexo($sexo){
		 $this->sexo = $sexo;
		 return $this;
	}

	/**
	* Get sexo
	* @return string
	**/
	public function getSexo(){
		 return $this->sexo;
	}

	/**
	* Set anio_nacimiento
	* @param integer anio_nacimiento
	* @return db_vacunacion
	**/
	public function setAnioNacimiento($anio_nacimiento){
		 $this->anio_nacimiento = $anio_nacimiento;
		 return $this;
	}

	/**
	* Get anio_nacimiento
	* @return integer
	**/
	public function getAnioNacimiento(){
		 return $this->anio_nacimiento;
	}

	/**
	* Set mes_nacimiento
	* @param integer mes_nacimiento
	* @return db_vacunacion
	**/
	public function setMesNacimiento($mes_nacimiento){
		 $this->mes_nacimiento = $mes_nacimiento;
		 return $this;
	}

	/**
	* Get mes_nacimiento
	* @return integer
	**/
	public function getMesNacimiento(){
		 return $this->mes_nacimiento;
	}

	/**
	* Get mes
	* @return string
	**/
	public function getMes(){
		 return $this->mes;
	}

	/**
	* Set dia_nacimiento
	* @param integer dia_nacimiento
	* @return db_vacunacion
	**/
	public function setDiaNacimiento($dia_nacimiento){
		 $this->dia_nacimiento = $dia_nacimiento;
		 return $this;
	}

	/**
	* Get dia_nacimiento
	* @return integer
	**/
	public function getDiaNacimiento(){
		 return $this->dia_nacimiento;
	}

	/**
	* Set etnia
	* @param string etnia
	* @return db_vacunacion
	**/
	public function setEtnia($etnia){
		 $this->etnia = $etnia;
		 return $this;
	}

	/**
	* Get etnia
	* @return string
	**/
	public function getEtnia(){
		 return $this->etnia;
	}

	/**
	* Set nacionalidad_pueblos
	* @param string nacionalidad_pueblos
	* @return db_vacunacion
	**/
	public function setNacionalidadPueblos($nacionalidad_pueblos){
		 $this->nacionalidad_pueblos = $nacionalidad_pueblos;
		 return $this;
	}

	/**
	* Get nacionalidad_pueblos
	* @return string
	**/
	public function getNacionalidadPueblos(){
		 return $this->nacionalidad_pueblos;
	}

	/**
	* Get nacionalidad
	* @return string
	**/
	public function getNacionalidad(){
		 return $this->nacionalidad;
	}

	/**
	* Set pueblos
	* @param string pueblos
	* @return db_vacunacion
	**/
	public function setPueblos($pueblos){
		 $this->pueblos = $pueblos;
		 return $this;
	}

	/**
	* Get pueblos
	* @return string
	**/
	public function getPueblos(){
		 return $this->pueblos;
	}

	/**
	* Set tel_contacto
	* @param string tel_contacto
	* @return db_vacunacion
	**/
	public function setTelContacto($tel_contacto){
		 $this->tel_contacto = $tel_contacto;
		 return $this;
	}

	/**
	* Get tel_contacto
	* @return string
	**/
	public function getTelContacto(){
		 return $this->tel_contacto;
	}

	/**
	* Get tel
	* @return \HCUE\PacienteBundle\Entity\Tel
	**/
	public function getTel(){
		 return $this->tel;
	}

	/**
	* Set correo_electronico
	* @param string correo_electronico
	* @return db_vacunacion
	**/
	public function setCorreoElectronico($correo_electronico){
		 $this->correo_electronico = $correo_electronico;
		 return $this;
	}

	/**
	* Get correo_electronico
	* @return string
	**/
	public function getCorreoElectronico(){
		 return $this->correo_electronico;
	}

	/**
	* Get correo
	* @return string
	**/
	public function getCorreo(){
		 return $this->correo;
	}

	/**
	* Set pobla_vacuna_1
	* @param string pobla_vacuna_1
	* @return db_vacunacion
	**/
	public function setPobla_vacuna_1($pobla_vacuna_1){
		 $this->pobla_vacuna_1 = $pobla_vacuna_1;
		 return $this;
	}

	/**
	* Get pobla_vacuna_1
	* @return string
	**/
	public function getPobla_vacuna_1(){
		 return $this->pobla_vacuna_1;
	}

	/**
	* Set grupo_riesgo_1
	* @param string grupo_riesgo_1
	* @return db_vacunacion
	**/
	public function setGrupo_riesgo_1($grupo_riesgo_1){
		 $this->grupo_riesgo_1 = $grupo_riesgo_1;
		 return $this;
	}

	/**
	* Get grupo_riesgo_1
	* @return string
	**/
	public function getGrupo_riesgo_1(){
		 return $this->grupo_riesgo_1;
	}

	/**
	* Set tuvo_covid
	* @param string tuvo_covid
	* @return db_vacunacion
	**/
	public function setTuvoCovid($tuvo_covid){
		 $this->tuvo_covid = $tuvo_covid;
		 return $this;
	}

	/**
	* Get tuvo_covid
	* @return string
	**/
	public function getTuvoCovid(){
		 return $this->tuvo_covid;
	}

	/**
	* Get tuvo
	* @return string
	**/
	public function getTuvo(){
		 return $this->tuvo;
	}

	/**
	* Set nom_vacuna
	* @param string nom_vacuna
	* @return db_vacunacion
	**/
	public function setNomVacuna($nom_vacuna){
		 $this->nom_vacuna = $nom_vacuna;
		 return $this;
	}

	/**
	* Get nom_vacuna
	* @return string
	**/
	public function getNomVacuna(){
		 return $this->nom_vacuna;
	}

	/**
	* Get nom
	* @return string
	**/
	public function getNom(){
		 return $this->nom;
	}

	/**
	* Set lote_vacuna
	* @param string lote_vacuna
	* @return db_vacunacion
	**/
	public function setLoteVacuna($lote_vacuna){
		 $this->lote_vacuna = $lote_vacuna;
		 return $this;
	}

	/**
	* Get lote_vacuna
	* @return string
	**/
	public function getLoteVacuna(){
		 return $this->lote_vacuna;
	}

	/**
	* Get lote
	* @return string
	**/
	public function getLote(){
		 return $this->lote;
	}

	/**
	* Set dosis_aplicada
	* @param integer dosis_aplicada
	* @return db_vacunacion
	**/
	public function setDosisAplicada($dosis_aplicada){
		 $this->dosis_aplicada = $dosis_aplicada;
		 return $this;
	}

	/**
	* Get dosis_aplicada
	* @return integer
	**/
	public function getDosisAplicada(){
		 return $this->dosis_aplicada;
	}

	/**
	* Get dosis
	* @return string
	**/
	public function getDosis(){
		 return $this->dosis;
	}

	/**
	* Set nom_profesional_aplica
	* @param string nom_profesional_aplica
	* @return db_vacunacion
	**/
	public function setNom_profesional_aplica($nom_profesional_aplica){
		 $this->nom_profesional_aplica = $nom_profesional_aplica;
		 return $this;
	}

	/**
	* Get nom_profesional_aplica
	* @return string
	**/
	public function getNom_profesional_aplica(){
		 return $this->nom_profesional_aplica;
	}

	/**
	* Set iden_profesional_aplica
	* @param string iden_profesional_aplica
	* @return db_vacunacion
	**/
	public function setIden_profesional_aplica($iden_profesional_aplica){
		 $this->iden_profesional_aplica = $iden_profesional_aplica;
		 return $this;
	}

	/**
	* Get iden_profesional_aplica
	* @return string
	**/
	public function getIden_profesional_aplica(){
		 return $this->iden_profesional_aplica;
	}

	/**
	* Set nom_profesional_registra
	* @param string nom_profesional_registra
	* @return db_vacunacion
	**/
	public function setNom_profesional_registra($nom_profesional_registra){
		 $this->nom_profesional_registra = $nom_profesional_registra;
		 return $this;
	}

	/**
	* Get nom_profesional_registra
	* @return string
	**/
	public function getNom_profesional_registra(){
		 return $this->nom_profesional_registra;
	}

	/**
	* Set iden_profesional_registra
	* @param string iden_profesional_registra
	* @return db_vacunacion
	**/
	public function setIden_profesional_registra($iden_profesional_registra){
		 $this->iden_profesional_registra = $iden_profesional_registra;
		 return $this;
	}

	/**
	* Get iden_profesional_registra
	* @return string
	**/
	public function getIden_profesional_registra(){
		 return $this->iden_profesional_registra;
	}

	/**
	* Set paciente_agendado
	* @param string paciente_agendado
	* @return db_vacunacion
	**/
	public function setPacienteAgendado($paciente_agendado){
		 $this->paciente_agendado = $paciente_agendado;
		 return $this;
	}

	/**
	* Get paciente_agendado
	* @return string
	**/
	public function getPacienteAgendado(){
		 return $this->paciente_agendado;
	}

	/**
	* Get paciente
	* @return string
	**/
	public function getPaciente(){
		 return $this->paciente;
	}

	/**
	* Set fase_vacuna
	* @param integer fase_vacuna
	* @return db_vacunacion
	**/
	public function setFaseVacuna($fase_vacuna){
		 $this->fase_vacuna = $fase_vacuna;
		 return $this;
	}

	/**
	* Get fase_vacuna
	* @return integer
	**/
	public function getFaseVacuna(){
		 return $this->fase_vacuna;
	}

	/**
	* Get fase
	* @return string
	**/
	public function getFase(){
		 return $this->fase;
	}

	/**
	* Set edad_anios
	* @param integer edad_anios
	* @return db_vacunacion
	**/
	public function setEdadAnios($edad_anios){
		 $this->edad_anios = $edad_anios;
		 return $this;
	}

	/**
	* Get edad_anios
	* @return integer
	**/
	public function getEdadAnios(){
		 return $this->edad_anios;
	}

	/**
	* Get edad
	* @return string
	**/
	public function getEdad(){
		 return $this->edad;
	}

	/**
	* Set fecha_vacuna
	* @param date fecha_vacuna
	* @return db_vacunacion
	**/
	public function setFechaVacuna($fecha_vacuna){
		 $this->fecha_vacuna = $fecha_vacuna;
		 return $this;
	}

	/**
	* Get fecha_vacuna
	* @return date
	**/
	public function getFechaVacuna(){
		 return $this->fecha_vacuna;
	}

	/**
	* Get fecha
	* @return string
	**/
	public function getFecha(){
		 return $this->fecha;
	}

	/**
	* Set fuente
	* @param string fuente
	* @return db_vacunacion
	**/
	public function setFuente($fuente){
		 $this->fuente = $fuente;
		 return $this;
	}

	/**
	* Get fuente
	* @return string
	**/
	public function getFuente(){
		 return $this->fuente;
	}

	/**
	* Set estado
	* @param integer estado
	* @return db_vacunacion
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Set apellidos_nombres
	* @param string apellidos_nombres
	* @return db_vacunacion
	**/
	public function setApellidosNombres($apellidos_nombres){
		 $this->apellidos_nombres = $apellidos_nombres;
		 return $this;
	}

	/**
	* Get apellidos_nombres
	* @return string
	**/
	public function getApellidosNombres(){
		 return $this->apellidos_nombres;
	}

	/**
	* Set pobla_vacuna
	* @param string pobla_vacuna
	* @return db_vacunacion
	**/
	public function setPoblaVacuna($pobla_vacuna){
		 $this->pobla_vacuna = $pobla_vacuna;
		 return $this;
	}

	/**
	* Get pobla_vacuna
	* @return string
	**/
	public function getPoblaVacuna(){
		 return $this->pobla_vacuna;
	}

	/**
	* Get pobla
	* @return string
	**/
	public function getPobla(){
		 return $this->pobla;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		
	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
		
	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
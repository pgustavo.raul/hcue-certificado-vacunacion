<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Fausto Suarez
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.estrategiapaciente")
* @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\EstrategiapacienteRepository")
* @ORM\HasLifecycleCallbacks()
*/


class Estrategiapaciente{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.estrategiapaciente_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var integer $ctestrategia_id
	* @ORM\Column(name="ctestrategia_id", type="integer", nullable=false)
	**/
	private $ctestrategia_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctestrategia_id", referencedColumnName="id")
	**/
	private $ctestrategia;

	/**
	* @var integer $ctpacienteclas_id
	* @ORM\Column(name="ctpacienteclas_id", type="integer", nullable=true)
	**/
	private $ctpacienteclas_id;

	/**
	* @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
	* @ORM\JoinColumn(name="ctpacienteclas_id", referencedColumnName="id")
	**/
	private $ctpacienteclas;

	/**
	* @var integer $paciente_id
	* @ORM\Column(name="paciente_id", type="integer", nullable=false)
	**/
	private $paciente_id;

	/**
	* @ORM\ManyToOne(targetEntity="HCUE\PacienteBundle\Entity\Paciente")
	* @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
	**/
	private $paciente;

	/**
	* @var datetime $fechamotivo
	* @ORM\Column(name="fechamotivo", type="datetime", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo fechamotivo")
	**/
	private $fechamotivo;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo estado")
	**/
	private $estado;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;

    /**
     * @var integer  $ctprioridadatencion_id
     * @ORM\Column(name="ctprioridadatencion_id", type="integer", nullable=false)
     **/
	private $ctprioridadatencion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctprioridadatencion_id", referencedColumnName="id")
     **/
	private $ctprioridadatencion;

    /**
     * @var integer  $ctmotivoestrategia_id
     * @ORM\Column(name="ctmotivoestrategia_id", type="integer", nullable=false)
     **/
	private $ctmotivoestrategia_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctmotivoestrategia_id", referencedColumnName="id")
     **/
	private $ctmotivoestrategia;


    



	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set ctestrategia_id
	* @param integer ctestrategia_id
	* @return estrategiapaciente
	**/
	public function setCtestrategiaId($ctestrategia_id){
		 $this->ctestrategia_id = $ctestrategia_id;
		 return $this;
	}

	/**
	* Get ctestrategia_id
	* @return integer
	**/
	public function getCtestrategiaId(){
		 return $this->ctestrategia_id;
	}

	/**
	* Set ctestrategia
	* @param \Core\AppBundle\Entity\Catalogo $ctestrategia
	**/
	public function setCtestrategia(\Core\AppBundle\Entity\Catalogo $ctestrategia){
		 $this->ctestrategia = $ctestrategia;
		 return $this;
	}

	/**
	* Get ctestrategia
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtestrategia(){
		 return $this->ctestrategia;
	}

	/**
	* Set ctpacienteclas_id
	* @param integer ctpacienteclas_id
	* @return estrategiapaciente
	**/
	public function setCtpacienteclasId($ctpacienteclas_id){
		 $this->ctpacienteclas_id = $ctpacienteclas_id;
		 return $this;
	}

	/**
	* Get ctpacienteclas_id
	* @return integer
	**/
	public function getCtpacienteclasId(){
		 return $this->ctpacienteclas_id;
	}

	/**
	* Set ctpacienteclas
	* @param \Core\AppBundle\Entity\Catalogo $ctpacienteclas
	**/
	public function setCtpacienteclas(\Core\AppBundle\Entity\Catalogo $ctpacienteclas){
		 $this->ctpacienteclas = $ctpacienteclas;
		 return $this;
	}

	/**
	* Get ctpacienteclas
	* @return \Core\AppBundle\Entity\Catalogo
	**/
	public function getCtpacienteclas(){
		 return $this->ctpacienteclas;
	}

	/**
	* Set paciente_id
	* @param integer paciente_id
	* @return estrategiapaciente
	**/
	public function setPacienteId($paciente_id){
		 $this->paciente_id = $paciente_id;
		 return $this;
	}

	/**
	* Get paciente_id
	* @return integer
	**/
	public function getPacienteId(){
		 return $this->paciente_id;
	}

	/**
	* Set paciente
	* @param \HCUE\PacienteBundle\Entity\Paciente $paciente
	**/
	public function setPaciente(\HCUE\PacienteBundle\Entity\Paciente $paciente){
		 $this->paciente = $paciente;
		 return $this;
	}

	/**
	* Get paciente
	* @return \HCUE\PacienteBundle\Entity\Paciente
	**/
	public function getPaciente(){
		 return $this->paciente;
	}

	/**
	* Set fechamotivo
	* @param datetime fechamotivo
	* @return estrategiapaciente
	**/
	public function setFechamotivo($fechamotivo){
		 $this->fechamotivo = $fechamotivo;
		 return $this;
	}

	/**
	* Get fechamotivo
	* @return datetime
	**/
	public function getFechamotivo(){
		 return $this->fechamotivo;
	}

	/**
	* Set estado
	* @param integer estado
	* @return estrategiapaciente
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}

	/**
	* Get fechacreacion
	* @return datetime
	**/
	public function getFechacreacion(){
		 return $this->fechacreacion;
	}

	/**
	* Get fechamodificacion
	* @return datetime
	**/
	public function getFechamodificacion(){
		 return $this->fechamodificacion;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return estrategiapaciente
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return estrategiapaciente
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
     * @return int
     */
    public function getCtprioridadatencionId()
    {
        return $this->ctprioridadatencion_id;
    }

    /**
     * @param int $ctprioridadatencion_id
     */
    public function setCtprioridadatencionId($ctprioridadatencion_id)
    {
        $this->ctprioridadatencion_id = $ctprioridadatencion_id;
    }

    /**
     * @return mixed
     */
    public function getCtprioridadatencion()
    {
        return $this->ctprioridadatencion;
    }

    /**
     * @param \Core\AppBundle\Entity\Catalogo $ctprioridadatencion
     */
    public function setCtprioridadatencion(\Core\AppBundle\Entity\Catalogo $ctprioridadatencion)
    {
        $this->ctprioridadatencion = $ctprioridadatencion;
    }




	/**
     * @return int
     */
    public function getCtmotivoestrategiaId()
    {
        return $this->ctmotivoestrategia_id;
    }

    /**
     * @param int $ctmotivoestrategia_id
     */
    public function setCtmotivoestrategiaId($ctmotivoestrategia_id)
    {
        $this->ctmotivoestrategia_id = $ctmotivoestrategia_id;
    }

    /**
     * @return mixed
     */
    public function getCtmotivoestrategia()
    {
        return $this->ctmotivoestrategia;
    }

    /**
     * @param \Core\AppBundle\Entity\Catalogo $ctmotivoestrategia
     */
    public function setCtmotivoestrategia(\Core\AppBundle\Entity\Catalogo $ctmotivoestrategia)
    {
        $this->ctmotivoestrategia = $ctmotivoestrategia;
    }


	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		$this->fechacreacion=new \DateTime();
		$this->fechamodificacion=new \DateTime();

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
				$this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>
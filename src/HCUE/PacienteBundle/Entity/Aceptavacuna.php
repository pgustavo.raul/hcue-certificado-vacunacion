<?php
/*
* @autor Luis Núñez
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.aceptavacuna")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\AceptavacunaRepository")
 * @ORM\HasLifecycleCallbacks()
 */


class Aceptavacuna{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.seq_id_aceptavacuna", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var integer $cttipoidentificacion_id
     * @ORM\Column(name="cttipoidentificacion_id", type="integer", nullable=false)
     **/
    private $cttipoidentificacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttipoidentificacion_id", referencedColumnName="id")
     **/
    private $cttipoidentificacion;

    /**
     * @var string $identificacion
     * @ORM\Column(name="identificacion", type="string",length=17)
     **/
    private $identificacion;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=17)
     **/
    private $nombre;

    /**
     * @var string $correo
     * @ORM\Column(name="correo", type="string",length=60)
     **/
    private $correo;

    /**
     * @var integer $entidad_id
     * @ORM\Column(name="entidad_id", type="integer", nullable=false)
     **/
    private $entidad_id;

    /**
     * @ORM\ManyToOne(targetEntity="\Core\AppBundle\Entity\Entidad")
     * @ORM\JoinColumn(name="entidad_id", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var datetime $fechanacimiento
     * @ORM\Column(name="fechanacimiento", type="datetime", nullable=false)
     **/
    private $fechanacimiento;

    /**
     * @var integer $ctsexo_id
     * @ORM\Column(name="ctsexo_id", type="integer", nullable=false)
     **/
    private $ctsexo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctsexo_id", referencedColumnName="id")
     **/
    private $ctsexo;

    /**
     * @var integer $ctestadocivil_id
     * @ORM\Column(name="ctestadocivil_id", type="integer", nullable=false)
     **/
    private $ctestadocivil_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctestadocivil_id", referencedColumnName="id")
     **/
    private $ctestadocivil;

    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var integer $vacuna_id
     * @ORM\Column(name="vacuna_id", type="integer", nullable=false)
     **/
    private $vacuna_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\AtencionMedicBundle\Entity\Vacuna")
     * @ORM\JoinColumn(name="vacuna_id", referencedColumnName="id")
     **/
    private $vacuna;

    /**
     * @var integer $alergia
     * @ORM\Column(name="alergia", type="integer", nullable=false)
     **/
    private $alergia;

    /**
     * @var integer $respiratoria
     * @ORM\Column(name="respiratoria", type="integer", nullable=false)
     **/
    private $respiratoria;

    /**
     * @var integer $hemorragia
     * @ORM\Column(name="hemorragia", type="integer", nullable=false)
     **/
    private $hemorragia;

    /**
     * @var integer $inmuno
     * @ORM\Column(name="inmuno", type="integer", nullable=false)
     **/
    private $inmuno;

    /**
     * @var integer $enfermedad
     * @ORM\Column(name="enfermedad", type="integer", nullable=false)
     **/
    private $enfermedad;

    /**
     * @var integer $embarazo
     * @ORM\Column(name="embarazo", type="integer", nullable=false)
     **/
    private $embarazo;

    /**
     * @var integer $lactancia
     * @ORM\Column(name="lactancia", type="integer", nullable=false)
     **/
    private $lactancia;

    /**
     * @var integer $covid
     * @ORM\Column(name="covid", type="integer", nullable=false)
     **/
    private $covid;

    /**
     * @var integer $usuariocreacion_id
     * @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
     **/
    private $usuariocreacion_id;

    /**
     * @var integer $usuariomodificacion_id
     * @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
     **/
    private $usuariomodificacion_id;

    /**
     * @var integer  $cttiporegistro_id
     * @ORM\Column(name="cttiporegistro_id", type="integer", nullable=false)
     **/
    private $cttiporegistro_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="cttiporegistro_id", referencedColumnName="id")
     **/
    private $cttiporegistro;


    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechamodificacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }

    /**
     * Set cttipoidentificacion_id
     * @param integer cttipoidentificacion
     * @return Aceptavacuna
     **/
    public function setCttipoidentificacionId($cttipoidentificacion_id){
        $this->cttipoidentificacion_id = $cttipoidentificacion_id;
        return $this;
    }

    /**
     * Get cttipoidentificacion_id
     * @return integer
     **/
    public function getCttipoidentificacionId(){
        return $this->cttipoidentificacion_id;
    }

    /**
     * Set Aceptavacuna
     * @param \Core\AppBundle\Entity\Catalogo $cttipoidentificacion
     **/
    public function setCttipoidentificacion(\Core\AppBundle\Entity\Catalogo $cttipoidentificacion){
        $this->cttipoidentificacion = $cttipoidentificacion;
        return $this;
    }

    /**
     * Get cttipoidentificacion
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCttipoidentificacion(){
        return $this->cttipoidentificacion;
    }

    /**
     * Set identificacion
     * @param string identificacion
     * @return Aceptavacuna
     **/
    public function setIdentificacion($identificacion){
        $this->identificacion = $identificacion;
       // return $this;
    }

    /**
     * Get identificacion
     * @return string
     **/
    public function getIdentificacion(){
        return $this->identificacion;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return Aceptavacuna
     **/
    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get identificacion
     * @return string
     **/
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * Set correo
     * @param string correo
     * @return Aceptavacuna
     **/
    public function setCorreo($correo){
        $this->correo = $correo;
        return $this;
    }

    /**
     * Get correo
     * @return string
     **/
    public function getCorreo(){
        return $this->correo;
    }
    /**
     * Set entidad_id
     * @param integer entidad_id
     * @return Aceptavacuna
     **/
    public function setEntidadId($entidad_id){
        $this->entidad_id = $entidad_id;
        return $this;
    }

    /**
     * Get entidad_id
     * @return integer
     **/
    public function getEntidadId(){
        return $this->entidad_id;
    }

    /**
     * Set entidad
     * @param \Core\AppBundle\Entity\Entidad $entidad
     **/
    public function setEntidad(\Core\AppBundle\Entity\Entidad $entidad){
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return \Core\AppBundle\Entity\Entidad
     **/
    public function getEntidad(){
        return $this->entidad;
    }

    /**
     * Set fechanacimiento
     * @param datetime fechanacimiento
     * @return Aceptavacuna
     **/
    public function setFechanacimiento($fechanacimiento){
        $this->fechanacimiento = new \DateTime($fechanacimiento);
        return $this;
    }

    /**
     * Get fechanacimiento
     * @return datetime
     **/
    public function getFechanacimiento(){
        return $this->fechanacimiento;
    }

    /**
     * Set ctsexo_id
     * @param integer ctsexo_id
     * @return Aceptavacuna
     **/
    public function setCtsexoId($ctsexo_id){
        $this->ctsexo_id = $ctsexo_id;
        return $this;
    }

    /**
     * Get ctsexo_id
     * @return integer
     **/
    public function getCtsexoId(){
        return $this->ctsexo_id;
    }

    /**
     * Set ctsexo
     * @param \Core\AppBundle\Entity\Catalogo $ctsexo
     **/
    public function setCtsexo(\Core\AppBundle\Entity\Catalogo $ctsexo){
        $this->ctsexo = $ctsexo;
        return $this;
    }

    /**
     * Get$ctsexo
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtsexo(){
        return $this->ctsexo;
    }

    /**
     * Set ctestadocivil_id
     * @param integer ctestadocivil_id
     * @return Aceptavacuna
     **/
    public function setCtestadocivilId($ctestadocivil_id){
        $this->ctestadocivil_id = $ctestadocivil_id;
        return $this;
    }

    /**
     * Get ctestadocivil_id
     * @return integer
     **/
    public function geCtestadocivilId(){
        return $this->ctestadocivil_id;
    }

    /**
     * Set ctestadocivil
     * @param \Core\AppBundle\Entity\Catalogo $ctestadocivil
     **/
    public function setCtestadocivil(\Core\AppBundle\Entity\Catalogo $ctestadocivil){
        $this->ctestadocivil = $ctestadocivil;
        return $this;
    }

    /**
     * Get$ctestadocivil
     * @return \Core\AppBundle\Entity\Catalogo
     **/
    public function getCtestadocivil(){
        return $this->ctestadocivil;
    }


    /**
     * Set estado
     * @param integer estado
     * @return aceptavacuna
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set vacuna_id
     * @param integer vacuna_id
     * @return aceptavacuna
     **/
    public function setVacunaId($vacuna_id)
    {
        $this->vacuna_id = $vacuna_id;
        return $this;
    }

    /**
     * Get vacuna_id
     * @return integer
     **/
    public function getVacunaId()
    {
        return $this->vacuna_id;
    }

    /**
     * Set vacuna
     * @param \HCUE\AtencionMedicBundle\Entity\Vacuna $vacuna
     **/
    public function setVacuna(\HCUE\AtencionMedicBundle\Entity\Vacuna $vacuna)
    {
        $this->vacuna = $vacuna;
        return $this;
    }

    /**
     * Get vacuna
     * @return \HCUE\AtencionMedicBundle\Entity\Vacuna
     **/
    public function getVacuna()
    {
        return $this->vacuna;
    }

    /**
     * Set alergia
     * @param integer alergia
     * @return aceptavacuna
     **/
    public function setAlergia($alergia){
        $this->alergia = $alergia;
        return $this;
    }

    /**
     * Get alergia
     * @return integer
     **/
    public function getAlergia(){
        return $this->alergia;
    }

    /**
     * Set respiratoria
     * @param integer respiratoria
     * @return aceptavacuna
     **/
    public function setRespiratoria($respiratoria){
        $this->respiratoria = $respiratoria;
        return $this;
    }

    /**
     * Get respiratoria
     * @return integer
     **/
    public function getRespiratoria(){
        return $this->respiratoria;
    }
    /**
     * Set hemorragia
     * @param integer hemorragia
     * @return aceptavacuna
     **/
    public function setHemorragia($hemorragia){
        $this->hemorragia = $hemorragia;
        return $this;
    }

    /**
     * Get hemorragia
     * @return integer
     **/
    public function getHemorragia(){
        return $this->hemorragia;
    }


    /**
     * Set inmuno
     * @param integer inmuno
     * @return aceptavacuna
     **/
    public function setInmuno($inmuno){
        $this->inmuno = $inmuno;
        return $this;
    }

    /**
     * Get inmuno
     * @return integer
     **/
    public function getInmuno(){
        return $this->inmuno;
    }


    /**
     * Set enfermedad
     * @param integer enfermedad
     * @return aceptavacuna
     **/
    public function setEnfermedad($enfermedad){
        $this->enfermedad = $enfermedad;
        return $this;
    }

    /**
     * Get enfermedad
     * @return integer
     **/
    public function getEnfermedad(){
        return $this->enfermedad;
    }

    /**
     * Set embarazo
     * @param integer embarazo
     * @return aceptavacuna
     **/
    public function setEmbarazo($embarazo){
        $this->embarazo = $embarazo;
        return $this;
    }

    /**
     * Get embarazo
     * @return integer
     **/
    public function getEmbarazo(){
        return $this->embarazo;
    }

    /**
     * Set lactancia
     * @param integer lactancia
     * @return aceptavacuna
     **/
    public function setLactancia($lactancia){
        $this->lactancia = $lactancia;
        return $this;
    }

    /**
     * Get lactancia
     * @return integer
     **/
    public function getLactancia(){
        return $this->lactancia;
    }

    /**
     * Set covid
     * @param integer covid
     * @return aceptavacuna
     **/
    public function setCovid($covid){
        $this->covid = $covid;
        return $this;
    }

    /**
     * Get covid
     * @return integer
     **/
    public function getCovid(){
        return $this->covid;
    }


    /**
     * @return int
     */
    public function getCttiporegistroId()
    {
        return $this->cttiporegistro_id;
    }

    /**
     * @param int $cttiporegistro_id
     */
    public function setCttiporegistroId($cttiporegistro_id)
    {
        $this->cttiporegistro_id = $cttiporegistro_id;
    }

    /**
     * @return mixed
     */
    public function getCttiporegistro()
    {
        return $this->cttiporegistro;
    }

    /**
     * @param \Core\AppBundle\Entity\Catalogo $cttiporegistro
     */
    public function setCttiporegistro(\Core\AppBundle\Entity\Catalogo $cttiporegistro)
    {
        $this->cttiporegistro = $cttiporegistro;
    }

    /**
     * Set usuariocreacion_id
     * @param integer usuariocreacion_id
     * @return estrategiapaciente
     **/
    public function setUsuariocreacionId($usuariocreacion_id){
        $this->usuariocreacion_id = $usuariocreacion_id;
        return $this;
    }

    /**
     * Get usuariocreacion_id
     * @return integer
     **/
    public function getUsuariocreacionId(){
        return $this->usuariocreacion_id;
    }

    /**
     * Set usuariomodificacion_id
     * @param integer usuariomodificacion_id
     * @return estrategiapaciente
     **/
    public function setUsuariomodificacionId($usuariomodificacion_id){
        $this->usuariomodificacion_id = $usuariomodificacion_id;
        return $this;
    }

    /**
     * Get usuariomodificacion_id
     * @return integer
     **/
    public function getUsuariomodificacionId(){
        return $this->usuariomodificacion_id;
    }

    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();

    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

    }

    public function __toString() {

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
?>
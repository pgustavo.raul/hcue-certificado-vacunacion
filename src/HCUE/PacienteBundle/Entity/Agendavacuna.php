<?php
/*
* @autor Luis Núñez
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.agendavacuna")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\AgendavacunaRepository")
 * @ORM\HasLifecycleCallbacks()
 */


class Agendavacuna{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="hcue_amed.seq_id_agendavacuna", allocationSize=1, initialValue=1)
     **/
    private $id;

    /**
     * @var datetime $fechacreacion
     * @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
     **/
    private $fechacreacion;

    /**
     * @var datetime $fechamodificacion
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     **/
    private $fechamodificacion;

    /**
     * @var string $identificacion
     * @ORM\Column(name="identificacion", type="string",length=17)
     **/
    private $identificacion;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string",length=17)
     **/
    private $nombre;

    /**
     * @var string $denominacion
     * @ORM\Column(name="denominacion", type="string",length=250)
     **/
    private $denominacion;

    /**
     * @var string $crp
     * @ORM\Column(name="crp", type="string",length=250)
     **/
    private $crp;

    /**
     * @var string $dependencia
     * @ORM\Column(name="dependencia", type="string",length=250)
     **/
    private $dependencia;

    /**
     * @var string $provincia
     * @ORM\Column(name="provincia", type="string",length=50)
     **/
    private $provincia;

    /**
     * @var string $ciudad
     * @ORM\Column(name="ciudad", type="string",length=50)
     **/
    private $ciudad;

    /**
     * @var string $clasificacion
     * @ORM\Column(name="clasificacion", type="string",length=50)
     **/
    private $clasificacion;

    /**
     * @var string $fecha
     * @ORM\Column(name="fecha", type="string",length=20)
     **/
    private $fecha;

    /**
     * @var string $hora
     * @ORM\Column(name="hora", type="string",length=20)
     **/
    private $hora;


    /**
     * @var integer $estado
     * @ORM\Column(name="estado", type="integer", nullable=false)
     * @Assert\NotNull(message="Debe ingresar el campo estado")
     **/
    private $estado;

    /**
     * @var string $cod_vacunador
     * @ORM\Column(name="cod_vacunador", type="string",length=10)
     **/
    private $cod_vacunador;

    /**
     * @var string $cod_equipo
     * @ORM\Column(name="cod_equipo", type="string",length=10)
     **/
    private $cod_equipo;

    /**
     * @var string $cod_termo
     * @ORM\Column(name="cod_termo", type="string",length=15)
     **/
    private $cod_termo;

    /**
     * Get id
     * @return integer
     **/
    public function getId(){
        return $this->id;
    }

    /**
     * Get fechacreacion
     * @return datetime
     **/
    public function getFechacreacion(){
        return $this->fechacreacion;
    }

    /**
     * Get fechamodificacion
     * @return datetime
     **/
    public function getFechamodificacion(){
        return $this->fechamodificacion;
    }


    /**
     * Set identificacion
     * @param string identificacion
     * @return Agendavacuna
     **/
    public function setIdentificacion($identificacion){
        $this->identificacion = $identificacion;
        return $this;
    }

    /**
     * Get identificacion
     * @return string
     **/
    public function getIdentificacion(){
        return $this->identificacion;
    }

    /**
     * Set nombre
     * @param string nombre
     * @return Agendavacuna
     **/
    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * Get identificacion
     * @return string
     **/
    public function getNombre(){
        return $this->nombre;
    }

    /**
     * Set denominacion
     * @param string denominacion
     * @return Agendavacuna
     **/
    public function setDenominacion($denominacion){
        $this->denominacion = $denominacion;
        return $this;
    }

    /**
     * Get denominacion
     * @return string
     **/
    public function getDenominacion(){
        return $this->denominacion;
    }

    /**
     * Set crp
     * @param string crp
     * @return Agendavacuna
     **/
    public function setCrp($crp){
        $this->crp = $crp;
        return $this;
    }

    /**
     * Get crp
     * @return string
     **/
    public function getCrp(){
        return $this->crp;
    }

    /**
     * Set dependencia
     * @param string dependencia
     * @return Agendavacuna
     **/
    public function setDependencia($dependencia){
        $this->dependencia = $dependencia;
        return $this;
    }

    /**
     * Get dependencia
     * @return string
     **/
    public function getDependencia(){
        return $this->dependencia;
    }


    /**
     * Set provincia
     * @param string provincia
     * @return Agendavacuna
     **/
    public function setProvincia($provincia){
        $this->provincia = $provincia;
        return $this;
    }

    /**
     * Get provincia
     * @return string
     **/
    public function getProvincia(){
        return $this->provincia;
    }



    /**
     * Set ciudad
     * @param string ciudad
     * @return Agendavacuna
     **/
    public function setCiudad($ciudad){
        $this->ciudad = $ciudad;
        return $this;
    }

    /**
     * Get ciudad
     * @return string
     **/
    public function getCiudad(){
        return $this->ciudad;
    }

    /**
     * Set clasificacion
     * @param string clasificacion
     * @return Agendavacuna
     **/
    public function setClasificacion($clasificacion){
        $this->clasificacion = $clasificacion;
        return $this;
    }

    /**
     * Get clasificacion
     * @return string
     **/
    public function getClasificacion(){
        return $this->clasificacion;
    }

    /**
     * Set fecha
     * @param string fecha
     * @return Agendavacuna
     **/
    public function setFecha($fecha){
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * Get fecha
     * @return string
     **/
    public function getFecha(){
        return $this->fecha;
    }

    /**
     * Set hora
     * @param string hora
     * @return Agendavacuna
     **/
    public function setHora($hora){
        $this->hora = $hora;
        return $this;
    }

    /**
     * Get hora
     * @return string
     **/
    public function getHora(){
        return $this->hora;
    }


    /**
     * Set estado
     * @param integer estado
     * @return agendavacuna
     **/
    public function setEstado($estado){
        $this->estado = $estado;
        return $this;
    }

    /**
     * Get estado
     * @return integer
     **/
    public function getEstado(){
        return $this->estado;
    }

    /**
     * Set cod_vacunador
     * @param string cod_vacunador
     * @return agendavacuna
     **/
    public function setCodvacunador($codvacunador){
        $this->cod_vacunador = $codvacunador;
        return $this;
    }

    /**
     * Get cod_vacunador
     * @return string
     **/
    public function getCodvacunador(){
        return $this->cod_vacunador;
    }

    /**
     * Set cod_equipo
     * @param string cod_equipo
     * @return agendavacuna
     **/
    public function setCodequipo($codequipo){
        $this->cod_equipo = $codequipo;
        return $this;
    }

    /**
     * Get cod_equipo
     * @return string
     **/
    public function getCodequipo(){
        return $this->cod_equipo;
    }

    /**
     * Set cod_termo
     * @param string cod_termo
     * @return agendavacuna
     **/
    public function setCodtermo($codtermo){
        $this->cod_termo = $codtermo;
        return $this;
    }

    /**
     * Get cod_termo
     * @return string
     **/
    public function getCodtermo(){
        return $this->cod_termo;
    }



    /**
     * @ORM\PrePersist
     **/
    public function prePersist(){
        $this->fechacreacion=new \DateTime();
        $this->fechamodificacion=new \DateTime();
        $this->estado=1;
    }

    /**
     * @ORM\PreUpdate
     **/
    public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

    }

    public function __toString() {

    }

    public function __clone() {
        $this->id=null;
    }

    public function copy() {
        return clone $this;
    }

}
?>
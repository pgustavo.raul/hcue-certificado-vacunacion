<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace  HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="hcue_amed.pacientecovid")
 * @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\PacientecovidRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Pacientecovid{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     **/
    private $id;

    /**
     * @var string $cedula
     * @ORM\Column(name="cedula", type="string",length=17)
     **/
    private $cedula;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ciudadvacunacion", referencedColumnName="id")
     **/
    private $ciudadvacunacion;

    /**
     * @var \Datetime $fechanacimiento
     * @ORM\Column(name="fechanacimiento", type="datetime", nullable=false)
     **/
    private $fechanacimiento;


    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="ctsexo", referencedColumnName="id")
     **/
    private $ctsexo;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Catalogo")
     * @ORM\JoinColumn(name="entidad", referencedColumnName="id")
     **/
    private $entidad;

    /**
     * @var integer $aceptavacuna
     * @ORM\Column(name="aceptavacuna", type="integer", nullable=false)
     **/
    private $aceptavacuna;


    /**
     * @var string $correo
     * @ORM\Column(name="correo", type="string", nullable=false)
     **/
    private $correo;

    /**
     * @var integer $organico_id
     * @ORM\Column(name="organico_id", type="integer", nullable=false)
     **/
    private $organico_id;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\Organico")
     * @ORM\JoinColumn(name="organico_id", referencedColumnName="id")
     **/
    private $organico;


    /**
     * Set cedula
     * @param integer cedula
     * @return pacientecovid
     **/
    public function setCedula($cedula){
        $this->cedula = $cedula;
        return $this;
    }

    /**
     * Get cedula
     * @return integer
     **/
    public function getCedula(){
        return $this->cedula;
    }

    /**
     * Set ciudadvacunacion
     * @param integer ciudadvacunacion
     * @return pacientecovid
     **/
    public function setCiudadvacunacion($ciudadvacunacion)
    {
        $this->ciudadvacunacion = $ciudadvacunacion;
        return $this;
    }

    /**
     * Get ciudadvacunacion
     * @return integer
     **/
    public function getCiudadvacunacion(){
        return $this->ciudadvacunacion;
    }

    /**
     * Set fechanacimiento
     * @param \DateTime fechanacimiento
     * @return pacientecovid
     * */
    public function setFechanacimiento($fechanacimiento)
    {
        $this->fechanacimiento = $fechanacimiento;
        return $this;
    }

    /**
     * Get fechanacimiento
     * @return \DateTime
     **/
    public function getFechanacimiento(){
        return $this->fechanacimiento;
    }


    /**
     * Set ctsexo
     * @param integer ctsexo
     * @return pacientecovid
     **/
    public function setCtsexo($ctsexo)
    {
        $this->ctsexo = $ctsexo;
        return $this;
    }

    /**
     * Get ctsexo
     * @return integer
     **/
    public function getCtsexo(){
        return $this->ctsexo;
    }

    /**
     * Set entidad
     * @param integer entidad
     * @return pacientecovid
     **/
    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
        return $this;
    }

    /**
     * Get entidad
     * @return integer
     **/
    public function getEntidad(){
        return $this->entidad;
    }


    /**
     * Set aceptavacuna
     * @param integer activo
     * @return pacientecovid
     **/
    public function setAceptavacuna($aceptavacuna)
    {
        $this->aceptavacuna = $aceptavacuna;
        return $this;
    }

    /**
     * Get aceptavacuna
     * @return integer
     **/
    public function getAceptavacuna()
    {
        return $this->aceptavacuna;
    }


    /**
     * Set correo
     * @param string correo
     * @return pacientecovid
     **/
    public function setCorreo($correo)
    {
        $this->correo = $correo;
        return $this;
    }

    /**
     * Get correo
     * @return string
     **/
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set organico_id
     * @param integer organico_id
     * @return pacientecovid
     **/
    public function setOrganicoId($organico_id){
        $this->organico_id = $organico_id;
        return $this;
    }

    /**
     * Get organico_id
     * @return integer
     **/
    public function getOrganicoId(){
        return $this->organico_id;
    }

    /**
     * Set Pacientecovid
     * @param \Core\AppBundle\Entity\Organico $organico
     **/
    public function setOrganico(\Core\AppBundle\Entity\Organico $organico){
        $this->organico = $organico;
        return $this;
    }

    /**
     * Get organico
     * @return \Core\AppBundle\Entity\Organico
     **/
    public function getOrganico(){
        return $this->organico;
    }

}
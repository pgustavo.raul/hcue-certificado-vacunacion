<?php
/**
 * Created by PhpStorm.
 * User: rv-pc
 * Date: 07/03/17
 * Time: 12:14
 */

namespace HCUE\PacienteBundle\Entity;

use Core\AppBundle\Entity\Constantes as AppConstantes;

class Constantes extends AppConstantes
{
    /**
     * @const integer Id del tipo de catalogo para nacionalidades y pueblos indigenas
     */
    const TC_NACIONALIDADESPUEBLOS = 7;

    /**
     * @const integer Id del tipo de catalogo para parentezcos
     */
    const TC_PARENTEZCOS = 8;

    /**
     * @const integer Id del tipo de catalogo para áreas de residencias
     */
    const TC_AREASRESIDENCIA = 9;

    /**
     * @const integer Id del tipo de catalogo para los estados de los niveles de instrucción
     */
    const TC_ESTADOSNIVELESINSTRUCCION = 14;

    /**
     * @const integer Id del registro detalle catalogo nivel de educacion ninguna
     */
    const CT_NIVELEDUCACION_NINGUNA = 89;

    /**
     * @const integer Id del registro detalle catalogo estado nivel de educacion ninguna
     */
    const CT_ESTADONIVELEDUCACION_NINGUNA = 97;

    /**
     * @const integer Id del registro detalle catalogo tipo empresa ninguna
     */
    const CT_TIPOEMPRESA_NINGUNA = 74;

    /**
     * @const integer Id del registro detalle catalogo etnia no aplica
     */
    const CT_ETNIA_NOAPLICA = 485;

    /**
     * @const integer Id del registro detalle catalogo etnia indígena
     */
    const CT_ETNIA_INDIGENA = 18;
    /**
     * @const integer Id del registro detalle catalogo nacionalidad etnica kichwa
     */
    const CT_NACIONALIDADETNICA_KICHWA = 32;

    /**
     * @const integer Id del registro de profesion ninguna
     */
    const PROFESION_NINGUNA = 442;

    /**
     * @const integer Id del registro detalle de catalogo webservis sin respuesta
     */
    const CT_WS_SEGUROSALUD_SIN_RESPUESTA = 628;

    /**
     * @const integer Numero de que identifica la no validacion de la cedula
     */
    const NO_VALIDA_CEDULA = 9;

    /**
     * @const integer Numero que identifica la no validacion de la cedula
     */
    const ANIO_EDAD_MAXIMO = 120;

    /**
     * @const integer Numero que identifica hasta que edad los niños es obligario registrarle el representante
     */
    const ANIO_EDAD_REGISTRO_REPRESENTANTE = 5;

    /**
     * @const integer Identificador de tipo de Representante Nacional
     */
    const REPRESENTANTE_NACIONAL = 0;

    /**
     * @const integer Identificador de tipo de Representante Extranjero
     */
    const REPRESENTANTE_EXTRANJERO = 1;

    /**
     * @const integer Numero minimo de caracteres de la identificacion de un extranjero
     */
    const MINIMO_CARACTERES_EXTRANJERO = 3;

    /**
     * @const integer Numero maximo de caracteres de la identificacion de un extranjero
     */
    const MAXIMO_CARACTERES_EXTRANJERO = 15;

    /**
     * @const integer Numero maximo de caracteres de la identificacion de un nacional
     */
    const MAXIMO_CARACTERES_NACIONAL = 10;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de enfermeria
     */
    const CT_ESPECIALIDAD_ENFERMERIA = 665;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Enfermería Rural
     */
    const CT_ESPECIALIDAD_ENFERMERIA_RURAL = 667;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de Auxiliar de Enfermería
     */
    const CT_ESPECIALIDAD_ENFERMERIA_AUXILIAR = 666;

    /**
     * @const integer Id del registro detalle catalogo estado de la atención Finalizada
     */
    const CT_ATENCIONFINALIZADA = 222;

    /**
     * @const integer catalogo de especialidad de enfermeria
     */
    const CT_ESPECIALIDADES_ENFERMERIA = array(
        Constantes::CT_ESPECIALIDAD_ENFERMERIA,
        Constantes::CT_ESPECIALIDAD_ENFERMERIA_RURAL,
        Constantes::CT_ESPECIALIDAD_ENFERMERIA_AUXILIAR
    );

    /**
     * @const integer Id del registro detalle catalogo estado aprobado de Solicitud de impresión Historia Clinica
     */
    const CT_SOLICITUD_APROBADA = 1520;

    /**
     * @const integer Id del registro detalle catalogo estado asignado de Solicitud de impresión Historia Clinica
     */
    const CT_SOLICITUD_ASIGNADA = 1521;

    /**
     * @const integer Id del registro detalle catalogo estado impresa de Solicitud de impresión Historia Clinica
     */
    const CT_SOLICITUD_IMPRESA = 1522;

    /**
     * @const integer Id del registro detalle catalogo estado reimpresa de Solicitud de impresión Historia Clinica
     */
    const CT_SOLICITUD_REIMPRESA = 1523;

    /**
     * @const integer Id del registro detalle catalogo estado reimpresa asignada de Solicitud de impresión Historia Clinica
     */
    const CT_SOLICITUD_REIMPRESA_ASIGNADA = 1524;

    /**
     * @const integer Id del registro detalle catalogo estado Salida de la estrategia de Estrategias médicas MSP motivos del registro
     */
    const CT_SALIDA_ESTRATEGIA = 1663;

    /**
     * @const integer Id de la Nacionalidad Ecuatoriana
     */
    const CT_NACIONALIDAD_EC = 1;

    /**
     * @const Lista de parentesco representante
     */
    const LIST_PARENTESCO_REPRESENTANTE = array(63,67,4763,4764,4765,68,69);

    /**
     * @const integer Id  estrategia medico del barrio
     */
    const CT_ESTRATEGIA_MEDICO_BARRIO = 1645;



    /**
     * @const constante para definir que el paciente aceptó la vacuna por un formulario público
     */
    const CT_ACEPTA_VACUNA_REGISTRO_PACIENTE = 5117;

    /**
     * @const constante para definir que el paciente aceptó  y fue registrado por un usuario del PRAS
     */
    const CT_ACEPTA_VACUNA_REGISTRO_USUARIO_PRAS = 5118;


    /**
     * @const integer Id del registro catalogo para identificar las solicitudes de certificado de vacunacion PENDIENTES
     */
    const CT_CERTIFICADO_VACUNACION_PENDIENTE = 5138;

        /**
     * @const integer Id del registro catalogo para identificar las solicitudes de certificado de vacunacion ENVIADAS
     */
    const CT_CERTIFICADO_VACUNACION_ENVIADA = 5139;

    /**
     * @const integer Id del tipo de catalogo para los grupos de riegos de las Vacunaciones
     */
    const TC_GRUPORIESGO_VACUNACION = 53;

    /**
     * @const integer Id del tipo de catalogo para los grupos de riegos de vacunas
     */
    const CT_ORIGENID_VACUNA_PRAS = 5137;

    /**
     * @const integer cantidad de registros permitidos en carga vacunación
     */
    const CT_REGISTROS_PERMITIDOS = 101;

    /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de enfermeria
     */
    const CT_ESPECIALIDAD_VACUNACION = 5116;

     /**
     * @const integer Id  Detalle Catalogo para registrar la especiadad de enfermeria
     */
    const CT_ESTADOACTIVOVACUNOMETRO = 2;

    /**
     * @const metodo de encriptación para enviar data publica
     */
    const CT_METODO_ENCRIPT = 'aes-256-cbc';

    /**
     * @const constante para base decode
     */
    const CT_BASE_DECODE = 'cGFzzQ4NzIzODcvJSVhMjM=';

    /**
     * @const clave de desencriptación
     */
    const CT_CLAVE_DECODE = '%YdMAT8zR@A%fWeRvH^vJth7UK8fbSb*tJG&39ANxe!32u^ZC$';
    //const CT_CLAVE_DECODE = '6ZTOycTDn;-=}*s@hgtKv';
}
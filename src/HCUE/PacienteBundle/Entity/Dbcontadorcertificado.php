<?php 
/*
* @autor Jipson Montalbán
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.db_contadorcertificado")
* @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\DbcontadorcertificadoRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Dbcontadorcertificado{

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.db_contadorcertificado_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $numidentificacion
	* @ORM\Column(name="numidentificacion", type="string",length=20, nullable=true)
	**/
	private $numidentificacion;

	/**
	* @var \Datetime $fechasolicitud
	* @ORM\Column(name="fechasolicitud", type="datetime", nullable=true)
	**/
	private $fechasolicitud;

	/**
	* @var decimal $score
	* @ORM\Column(name="score", type="decimal", nullable=true)
	**/
	private $score;

	/**
	* @var integer $estado
	* @ORM\Column(name="estado", type="integer", nullable=true)
	**/
	private $estado;

	/**
	* @var \Datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=true)
	**/
	private $fechacreacion;

	/**
	* @var integer $activo
	* @ORM\Column(name="activo", type="integer", nullable=true)
	**/
	private $activo;

	/**
	* @var string $credit
	* @ORM\Column(name="credit", type="string", nullable=true)
	**/
	private $credit;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

	/**
	* Set numidentificacion
	* @param string numidentificacion
	* @return db_contadorcertificado
	**/
	public function setNumidentificacion($numidentificacion){
		 $this->numidentificacion = $numidentificacion;
		 return $this;
	}

	/**
	* Get numidentificacion
	* @return string
	**/
	public function getNumidentificacion(){
		 return $this->numidentificacion;
	}

	/**
	* Set fechasolicitud
	* @ORM\PrePersist
	* @return db_contadorcertificado
	**/
	public function setFechasolicitud(){
		 $this->fechasolicitud = new \DateTime(date('d-M-Y H:i:s'));
		 return $this;
	}

	/**
	* Get fechasolicitud
	* @return datetime
	**/
	public function getFechasolicitud(){
		 return $this->fechasolicitud;
	}

	/**
	* Set score
	* @param decimal score
	* @return db_contadorcertificado
	**/
	public function setScore($score){
		 $this->score = $score;
		 return $this;
	}

	/**
	* Get score
	* @return decimal
	**/
	public function getScore(){
		 return $this->score;
	}

	/**
	* Set credit
	* @param string credit
	* @return db_contadorcertificado
	**/
	public function setCredit($credit){
		$this->credit = $credit;
		return $this;
   }

    /**
   * Get credit
   * @return string
   **/
    public function getCredit(){
		return $this->credit;
    }

	/**
	* Set estado
	* @param integer estado
	* @return db_contadorcertificado
	**/
	public function setEstado($estado){
		 $this->estado = $estado;
		 return $this;
	}

	/**
	* Get estado
	* @return integer
	**/
	public function getEstado(){
		 return $this->estado;
	}
	/**
	* Set fechacreacion
	* @ORM\PrePersist
	* @return db_contadorcertificado
	**/
	public function setFechacreacion(){
		$this->fechacreacion = new \DateTime(date('d-M-Y H:i:s'));
		return $this;
    }

	/**
	* Get fechacreacion
	* @return datetime
	**/
	public function getFechacreacion(){
		 return $this->fechacreacion;
	}

	/**
	* Set activo
	* @param integer activo
	* @return db_contadorcertificado
	**/
	public function setActivo($activo){
		if($this->activo){
                          $this->activo=$activo;                            
                    }
		return $this;
	}

	/**
	* Get activo
	* @return integer
	**/
	public function getActivo(){
		 return $this->activo;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		$this->activo=1;
		$this->estado=1;

	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
		
	}

	public function __toString() {

             }

	public function __clone() {
               $this->id=null;
             }

	public function copy() {
               return clone $this;
             }

}
 ?>

<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\PacienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="hcue_amed.impresreporthistclinica")
* @ORM\Entity(repositoryClass="HCUE\PacienteBundle\Entity\Repository\ImpresreporthistclinicaRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Impresreporthistclinica {

	/**
	* @var integer $id
	* @ORM\Column(name="id", type="integer", nullable=false)
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	* @ORM\SequenceGenerator(sequenceName="hcue_amed.imprereporthistclin_id_seq", allocationSize=1, initialValue=1)
	**/
	private $id;

	/**
	* @var string $motivoreimpresion
	* @ORM\Column(name="motivoreimpresion", type="string")
	**/
	private $motivoreimpresion;

    /**
     * @var integer $solicreporthistoriacli_id
     * @ORM\Column(name="solicreporthistoriacli_id", type="integer", nullable=false)
     **/
	private $solicreporthistoriacli_id;

    /**
     * @ORM\ManyToOne(targetEntity="HCUE\PacienteBundle\Entity\Solicreporthistoriaclinica")
     * @ORM\JoinColumn(name="solicreporthistoriacli_id", referencedColumnName="id")
     **/
    private $solicreporthistoriacli;

	/**
	* @var datetime $fechaimpresion
	* @ORM\Column(name="fechaimpresion", type="datetime", nullable=false)
	* @Assert\NotNull(message="Debe ingresar el campo fechaimpresion")
	**/
	private $fechaimpresion;

	/**
	* @var datetime $fechacreacion
	* @ORM\Column(name="fechacreacion", type="datetime", nullable=false)
	**/
	private $fechacreacion;

	/**
	* @var datetime $fechamodificacion
	* @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
	**/
	private $fechamodificacion;

	/**
	* @var integer $usuariocreacion_id
	* @ORM\Column(name="usuariocreacion_id", type="integer", nullable=false)
	**/
	private $usuariocreacion_id;

	/**
	* @var integer $usuariomodificacion_id
	* @ORM\Column(name="usuariomodificacion_id", type="integer", nullable=true)
	**/
	private $usuariomodificacion_id;


	/**
	* Get id
	* @return integer
	**/
	public function getId(){
		 return $this->id;
	}

    /**
     * Set motivo
     * @param string motivoreimpresion
     * @return impresreporthistclinica
     **/
    public function setMotivoreimpresion($motivoreimpresion){
        $this->motivoreimpresion = $motivoreimpresion;
        return $this;
    }

    /**
     * Get motivo
     * @return string
     **/
    public function getMotivoreimpresion(){
        return $this->motivoreimpresion;
    }

	/**
	* Set solicreporthistoriacli_id
	* @param integer solicreporthistoriacli_id
	* @return impresreporthistclinica
	**/
	public function setSolicreporthistoriacli_Id($solicreporthistoriacli_id){
		 $this->solicreporthistoriacli_id = $solicreporthistoriacli_id;
		 return $this;
	}

	/**
	* Get reportehistoriaclinica_id
	* @return integer
	**/
	public function getSolicreporthistoriacli_Id(){
		 return $this->solicreporthistoriacli_id;
	}

	/**
	* Set solicreporthistoriacli
	* @param \HCUE\PacienteBundle\Entity\Solicreporthistoriaclinica $solicreporthistoriacli
	**/
	public function setSolicreporthistoriacli(\HCUE\PacienteBundle\Entity\Solicreporthistoriaclinica $solicreporthistoriacli){
		 $this->solicreporthistoriacli = $solicreporthistoriacli;
		 return $this;
	}

	/**
	* Get reportehistoriaclinica
	* @return \HCUE\PacienteBundle\Entity\Solicreporthistoriaclinica
	**/
	public function getSolicreporthistoriacli(){
		 return $this->solicreporthistoriacli;
	}

	/**
	* Set fechaimpresion
	* @param datetime fechaimpresion
	* @return impresreporthistclinica
	**/
	public function setFechaimpresion($fechaimpresion){
		 $this->fechaimpresion = $fechaimpresion;
		 return $this;
	}

	/**
	* Get fechaimpresion
	* @return datetime
	**/
	public function getFechaimpresion(){
		 return $this->fechaimpresion;
	}

	/**
	* Get fechacreacion
	* @return datetime
	**/
	public function getFechacreacion(){
		 return $this->fechacreacion;
	}

	/**
	* Get fechamodificacion
	* @return datetime
	**/
	public function getFechamodificacion(){
		 return $this->fechamodificacion;
	}

	/**
	* Set usuariocreacion_id
	* @param integer usuariocreacion_id
	* @return impresreporthistclinica
	**/
	public function setUsuariocreacionId($usuariocreacion_id){
		 $this->usuariocreacion_id = $usuariocreacion_id;
		 return $this;
	}

	/**
	* Get usuariocreacion_id
	* @return integer
	**/
	public function getUsuariocreacionId(){
		 return $this->usuariocreacion_id;
	}

	/**
	* Set usuariomodificacion_id
	* @param integer usuariomodificacion_id
	* @return impresreporthistclinica
	**/
	public function setUsuariomodificacionId($usuariomodificacion_id){
		 $this->usuariomodificacion_id = $usuariomodificacion_id;
		 return $this;
	}

	/**
	* Get usuariomodificacion_id
	* @return integer
	**/
	public function getUsuariomodificacionId(){
		 return $this->usuariomodificacion_id;
	}

	/**
	* @ORM\PrePersist
	**/
	public function prePersist(){
		$this->fechacreacion=new \DateTime();
        $this->fechaimpresion=new \DateTime();
	}

	/**
	* @ORM\PreUpdate
	**/
	public function preUpdate(){
        $this->fechamodificacion=new \DateTime();

	}

	public function __toString() {

    }

	public function __clone() {
        $this->id=null;
    }

	public function copy() {
       return clone $this;
    }

}
 ?>
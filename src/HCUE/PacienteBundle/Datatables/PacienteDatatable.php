<?php

namespace HCUE\PacienteBundle\Datatables;

use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;
use Sg\DatatablesBundle\Datatable\Data\DatatableDataManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Core\SeguridadBundle\Security\Authorization\Voter\AppVoter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PacienteDatatable
 *
 * @package HCUE\PacienteBundle\Datatables
 */
class PacienteDatatable extends AbstractDatatableView
{
    /**
     * Variable de tipo DatatableDataManager para el manejo del QueryBuilder del datatables
     * @var DatatableDataManager $dtmanager
     */
    private $dtmanager;

    /**
     * Asigna a la variable $dtmanager una instancia de la clase DatatableManager
     * @param DatatableDataManager $dtmanager
     */
    public function setDatatablesDataManager(DatatableDataManager $dtmanager)
    {
        $this->dtmanager = $dtmanager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {

        $this->topActions->set(array(
            'start_html' => '<div class="table-header">Ingrese el dato de búsqueda</div>',
            'actions'    => array()
        ));

        $this->features->set(array(
            'auto_width'    => true,
            'defer_render'  => false,
            'info'          => false,
            'jquery_ui'     => false,
            'length_change' => false,
            'ordering'      => false,
            'paging'        => true,            
            'processing'    => true,
            'scroll_x'      => false,
            'scroll_y'      => '',
            'searching'     => true,
            'state_save'    => false,
            'delay'         => 0,
            'extensions'    => array(
                //'dom'=> 'Bfrtip',
                //'dom'     => "<'row'<'col-sm-6'l><'col-sm-6'>>rt<'row'<'col-sm-5'i><'col-sm-7'p>>",
                'dom'     => "<'row'<'col-sm-6'l><'col-sm-6'>>rt<'row'<'col-sm-5'i>>",
                'buttons' => array(
                    ($this->authorizationChecker->isGranted(AppVoter::ADD)) ?
                        array(
                            "className"   => " btn-white btn-primary btn-bold",
                            'text'        => "<i class='fa fa-plus bigger-110 blue'></i>",
                            'titleAttr'   => 'Agregar nuevo registro',
                            'action_name' => 'newAction'
                        ) : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'copy',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => '<i class="fa fa-copy bigger-110 pink"></i>',
                            'titleAttr'     => 'Copiar al portapapeles',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'csv',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-database bigger-110 orange'></i>",
                            'titleAttr'     => 'Exportar a CSV',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'excel',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-file-excel-o bigger-110 green'></i>",
                            'titleAttr'     => 'Exportar a Excel',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'pdf',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-file-pdf-o bigger-110 red'></i>",
                            'titleAttr'     => 'Exportar a PDF',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'print',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-print bigger-110 grey'></i>",
                            'titleAttr'     => 'Imprimir',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        ) : array(),
                    array(
                        "className"   => " btn-white btn-primary btn-bold",
                        'text'        => "<i class='fa fa-refresh bigger-110 green'></i>",
                        'titleAttr'   => 'Recargar',
                        'action_name' => 'reload'
                    ),

                    array(
                        'extend'    => 'pageLength',
                        "className" => " btn-white btn-primary btn-bold",
                        'text'      => "<i class='fa fa-bars bigger-110 blue'></i>",
                        'titleAttr' => 'Visualización de registros'
                    )
                ),

                'responsive' => true,
                'select'     => array("style" => "single"),
                'keys'       => false
            )
        ));

        $this->ajax->set(array(
            'url'  => $this->router->generate('hcue_paciente_paciente_results'),
            'type' => 'GET'
        ));

        $this->options->set(array(
            'display_start'                 => 0,
            'defer_loading'                 => -1,
            'dom'                           => 'lfrtip',            
            'length_menu'                   => array(5, 10, 25, 50, 100),
            'order_classes'                 => true,
            //'order'                         => array(array(0, 'desc')),
            'order_multi'                   => true,
            'page_length'                   => 10,
            'paging_type'                   => Style::FULL_NUMBERS_PAGINATION,
            'renderer'                      => '',
            'scroll_collapse'               => false,
            'search_delay'                  => 0,
            'state_duration'                => 7200,
            'stripe_classes'                => array(),
            'class'                         => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering'          => true,
            'individual_filtering_position' => 'head',
            'use_integration_options'       => true,
            'force_dom'                     => false,
            'count_all_results'             => false,
            'count_filter_results'          => false,
        ));

//        $tipoIdenficacion = $this->em->getRepository('CoreAppBundle:Catalogo')
//            ->getCatalogo(Constantes::TC_TIPOIDENTIFICACION);

        $this->columnBuilder
            ->add('id', 'column', array(
                'title'   => 'Id',
                'visible' => false
            ))
            ->add('persona.primernombre', 'column', array(
                'visible' => false,
                'class' => 'primernombre',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.segundonombre', 'column', array(
                'visible' => false,
                'class' => 'segundonombre',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.apellidopaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidopaterno',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.apellidomaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidomaterno',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.cttipoidentificacion_id', 'column', array(
                'visible' => false,
                'class' => 'tipoidentificacion_id',                
                'filter' => array('text', array(                    
                    'search_type' => 'eq'                  
                ))
            ))
            ->add('persona.nombrecompleto', 'column', array(
                'title' => 'Apellidos y Nombres',
                'width' => '200px',
            ))
            ->add('persona.cttipoidentificacion.descripcion', 'column', array(
                'title'  => 'Tipo Identificación',
                'width'  => '70px',
                'searchable'=>false,
            ))
            ->add('persona.numeroidentificacion', 'column', array(
                'title' => 'Nro. Identificación',
                'width' => '100px',
                'class' => 'numeroidentificacion',
                'filter' => array('text', array(                    
                    'search_type' => 'eq'                  
                ))
            ))
            ->add('numerohistoriaclinica', 'column', array(
                'title' => 'Nro. Historia Clínica',
                'width' => '120px',
                'filter' => array('text', array(                    
                    'search_type' => 'eq'                  
                ))
            ))
            ->add('persona.fechanacimiento', 'column', array(
                'title'       => 'Fecha Nacimiento',
                'width'       => '80px'

            ))
           ->add('poractualizar', 'column', array(
                'title' => 'Por actualizar',
                'width' => '50px'
            ))
            ->add(null, 'action', array(
                'title'      => $this->translator->trans('datatables.actions.title'),
                'start_html' => '<div class="btn-group" >',
                'end_html'   => '</div>',
                //                    'class'=>'hidden-sm hidden-xs action-buttons',
                'width'      => '60px',

                'actions' => array(
                    array(
                        'route'            => 'hcue_paciente_paciente_show',
                        'route_parameters' => array(
                            'valor' => 'id'
                        ),
                        'action_name'      => 'showAction',
                        'icon'             => 'ace-icon fa fa-eye  bigger-120 fa-fw',
                        'attributes'       => array(
                            'rel'   => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'blue',
                            'role'  => 'button'
                        ),
                        'render_if'        => function () {
                            return $this->authorizationChecker->isGranted(AppVoter::VIEW);
                        }
                    ),
                    array(
                        'route'            => 'hcue_paciente_paciente_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'icon'             => 'ace-icon fa fa-pencil bigger-120 fa-fw',
                        'attributes'       => array(
                            'rel'   => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit'),
                            'class' => 'green',
                            'role'  => 'button'
                        ),
                        'render_if'        => function () {
                            return $this->authorizationChecker->isGranted(AppVoter::EDIT);
                        }
                    ),
                    array(
                        'route'            => 'hcue_paciente_paciente_print',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'action_name'=>'printAction',
                        'icon'             => 'ace-icon fa fa-print bigger-120 fa-fw',
                        'attributes'       => array(
                            'rel'   => 'tooltip',
                            'title' => 'Imprimir Formulario 001',
                            'class' => 'grey',
                            'role'  => 'button'
                        ),
                        'render_if'        => function () {
                            return $this->authorizationChecker->isGranted(AppVoter::EXPORT);
                        }
                    ),
                    //                    array(
                    //                        'route' => 'hcue_paciente_paciente_eliminar',
                    //                        'route_parameters' => array(
                    //                            'id' => 'id'
                    //                        ),
                    //                        'action_name'=>'deleteAction',
                    //                        'icon'=>'ace-icon fa fa-trash-o bigger-120',
                    //                        'attributes' => array(
                    //                            'rel' => 'tooltip',
                    //                            'title' => 'Eliminar',
                    //                            'class'=>'red',
                    //                            'role' => 'button'
                    //                        ),
                    //                    )
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'HCUE\PacienteBundle\Entity\Paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'paciente_datatable';
    }

    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse(Request $request)
    {
        $this->buildDatatable();
        $queryRequest = $request->query;
        $globalSearch = $queryRequest->get('search')['value'];
        $individualFilter = false;
        if ($this->getOptions()->getIndividualFiltering()) {
            $columns = $queryRequest->get('columns');
            foreach ($columns as $key => $params) {
                if ($params['search']['value']) {
                    $individualFilter = true;
                    break;
                }
            }
        }
        $response = null;
        if ($globalSearch || $individualFilter) {
            $query = $this->dtmanager->getQueryFrom($this);
            $query->addWhereAll(function ($qb) {
                $qb->andWhere("paciente.activo = 1")
                    ->andWhere("persona.activo = 1");
            });
            $response = $query->getResponse();
        } else {
            $fulldata = array('draw'            => $queryRequest->get('draw'),
                              'recordsTotal'    => 0,
                              'recordsFiltered' => 0,
                              'data'            => array()
            );
            $response = new JsonResponse($fulldata);
        }
        return $response;
    }

    public function getLineFormatter()
    {
        $formatter = function ($line)  {
            $fechaNacimiento = $line['persona']['fechanacimiento'];
            $line['persona']['fechanacimiento'] = $fechaNacimiento->format('d-m-Y');

            $poractualizar = $line['poractualizar'];            
            $line['poractualizar'] = !empty($poractualizar) ? "SI" : "NO";
            return $line;
        };

        return $formatter;
    }
}

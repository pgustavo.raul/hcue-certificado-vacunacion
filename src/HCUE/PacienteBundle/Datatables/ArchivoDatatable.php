<?php

namespace HCUE\PacienteBundle\Datatables;

use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;
use Sg\DatatablesBundle\Datatable\Data\DatatableDataManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;
use Core\SeguridadBundle\Security\Authorization\Voter\AppVoter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ArchivoDatatable
 *
 * @package HCUE\PacienteBundle\Datatables
 */
class ArchivoDatatable extends AbstractDatatableView
{
    /**
     * Variable de tipo DatatableDataManager para el manejo del QueryBuilder del datatables
     * @var DatatableDataManager $dtmanager
     */
    private $dtmanager;

    private $container;

    private $requestParams;

    /**
     * @var array
     */
    private $searchColumns;

    /**
     * @var array
     */
    private $orderColumns;

    /*
     * Set contenedor
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Asigna a la variable $dtmanager una instancia de la clase DatatableManager
     * @param DatatableDataManager $dtmanager
     */
    public function setDatatablesDataManager(DatatableDataManager $dtmanager)
    {
        $this->dtmanager = $dtmanager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->topActions->set(array(
            'start_html' => '<div class="table-header">Ingrese el dato de búsqueda</div>',
            'actions'    => array()
        ));

        $this->features->set(array(
            'auto_width'    => true,
            'defer_render'  => false,
            'info'          => false,
            'jquery_ui'     => false,
            'length_change' => false,
            'ordering'      => true,
            'paging'        => true,            
            'processing'    => true,
            'scroll_x'      => false,
            'scroll_y'      => '',
            'searching'     => true,
            'state_save'    => false,
            'delay'         => 0,
            'extensions'    => array(
                //'dom'=> 'Bfrtip',
                //'dom'     => "<'row'<'col-sm-6'l><'col-sm-6'>>rt<'row'<'col-sm-5'i><'col-sm-7'p>>",
                'dom'     => "<'row'<'col-sm-6'l><'col-sm-6'>>rt<'row'<'col-sm-5'i>>",
                'buttons' => array(
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'copy',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => '<i class="fa fa-copy bigger-110 pink"></i>',
                            'titleAttr'     => 'Copiar al portapapeles',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'csv',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-database bigger-110 orange'></i>",
                            'titleAttr'     => 'Exportar a CSV',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'excel',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-file-excel-o bigger-110 green'></i>",
                            'titleAttr'     => 'Exportar a CSV',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT)) ?
                        array(
                            'extend'        => 'pdf',
                            "className"     => " btn-white btn-primary btn-bold",
                            'text'          => "<i class='fa fa-file-pdf-o bigger-110 red'></i>",
                            'titleAttr'     => 'Exportar a PDF',
                            'exportOptions' => array(
                                "columns" => ':visible:not(:last)'
                            )
                        )
                        : array(),
                    ($this->authorizationChecker->isGranted(AppVoter::EXPORT))
                        ? array(
                        'extend'        => 'print',
                        "className"     => " btn-white btn-primary btn-bold",
                        'text'          => "<i class='fa fa-print bigger-110 grey'></i>",
                        'titleAttr'     => 'Imprimir',
                        'exportOptions' => array(
                            "columns" => ':visible:not(:last)'
                        )
                    ) : array(),
                    array(
                        "className"   => " btn-white btn-primary btn-bold",
                        'text'        => "<i class='fa fa-refresh bigger-110 green'></i> Recargar",
                        'titleAttr'   => 'Recargar',
                        //                        'action'=>'HCUEPacienteBundle::datatablereload.js.twig'
                        'action_name' => 'reload'
                    ),
                    array(
                        'extend'    => 'pageLength',
                        "className" => " btn-white btn-primary btn-bold",
                        'text'      => "<i class='fa fa-bars bigger-110 blue'></i> Registros",
                        'titleAttr' => 'Visualización de registros'
                    ),
                ),

                'responsive' => true,
                'select'     => array("style" => "single"),
                'keys'       => false
            )
        ));

        $this->ajax->set(array(
            'url'  => $this->router->generate('hcue_paciente_archivo_results'),
            'type' => 'GET'
        ));

        $this->options->set(array(
            'display_start'                 => 0,
            'defer_loading'                 => -1,
            'dom'                           => 'lfrtip',
            'length_menu'                   => array(5, 10, 25, 50, 100),
            'order_classes'                 => true,
            'order'                         => array(array(2, 'desc')),
            'order_multi'                   => true,
            'page_length'                   => 10,
            'paging_type'                   => Style::FULL_NUMBERS_PAGINATION,
            'renderer'                      => '',
            'scroll_collapse'               => false,
            'search_delay'                  => 0,
            'state_duration'                => 7200,
            'stripe_classes'                => array(),
            'class'                         => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering'          => true,
            'individual_filtering_position' => 'head',
            'use_integration_options'       => true,
            'force_dom'                     => false,
            'count_all_results'             => false,
            'count_filter_results'          => false,
        ));

//        $tipoIdenficacion = $this->em->getRepository('CoreAppBundle:Catalogo')
//            ->getCatalogo(Constantes::TC_TIPOIDENTIFICACION);

        $this->columnBuilder
            ->add('id', 'column', array(
                'title'   => 'Id',
                'visible' => false,
            ))
            ->add('primernombre', 'column', array(
                'visible' => false,
                'class' => 'primernombre',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('segundonombre', 'column', array(
                'visible' => false,
                'class' => 'segundonombre',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('apellidopaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidopaterno',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('apellidomaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidomaterno',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('entidad_id', 'column', array(
                'title'   => 'Entidad_id',
                'visible' => false,
            ))
            ->add('paciente_id', 'column', array(
                'title'   => 'Paciente_id',
                'visible' => false,
            ))
            ->add('nombrecompleto', 'column', array(
                'title' => 'Apellidos y Nombres',
                'width' => '200px',
                 'filter' => array('text', array(                    
                    'search_type' => 'like'                                      
                ))
                
            ))
            ->add('cttipoidentificacion_id', 'column', array(
                'visible' => false,
                'class' => 'tipoidentificacion_id',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('tipoidentificacion', 'column', array(
                'title' => 'Tipo Identificación',
                'width' => '100px',
                'searchable'=>false,
//                'filter' => array('select', array(
//                    'search_type' => 'eq',
//                    'select_options' =>array('' => 'Ninguno') + $this->getCollectionAsOptionsArray($tipoIdenficacion, 'descripcion', 'descripcion'),
//                ))
            ))
            ->add('numeroidentificacion', 'column', array(
                'title' => 'Número Identificación',
                'width' => '120px',
                'class' => 'numeroidentificacion',
                'filter' => array('text', array(                    
                    'search_type' => 'eq'                  
                ))
            ))
            ->add('numerohistoriaclinica', 'column', array(
                'title' => 'Número Historia Clínica',
                'width' => '120px',
                'filter' => array('text', array(                    
                    'search_type' => 'eq'                  
                ))
            ))
            ->add('numeroarchivo', 'column', array(
                'title' => 'Numero Archivo',
                'width' => '80px',
                'filter' => array('text', array(                    
                    'search_type' => 'eq'                  
                ))
            ))
            ->add(null, 'action', array(
                'title'      => $this->translator->trans('datatables.actions.title'),
                'start_html' => '<div class="btn-group" >',
                'end_html'   => '</div>',
                'width'      => '60px',
                'actions' => array(
                    array(
                        'action_name'      => 'editAction',
                        'route'            => 'hcue_paciente_archivo_edit',
                        'route_parameters' => array(
                            'id'          => 'id',
                            'paciente_id' => 'paciente_id'
                        ),
                        'icon'             => 'ace-icon fa fa-pencil bigger-120 fa-fw',
                        'attributes'       => array(
                            'rel'   => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit'),
                            'class' => 'green',
                            'role'  => 'button'
                        ),
                        'render_if'        => function () {
                            return $this->authorizationChecker->isGranted(AppVoter::EDIT)
                                || $this->authorizationChecker->isGranted(AppVoter::ADD);
                        }
                    ),

                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'HCUE\PacienteBundle\Entity\Paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'archivo_datatable';
    }

    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
//    public function getResponseAnterior(Request $request)
//    {
//        $parameterBag = $request->query;
//        $this->requestParams = $parameterBag->all();
//        $this->buildDatatable();
//        $this->addSearchOrderColumn();
//        $repopaciente = $this->em->getRepository("HCUEPacienteBundle:Paciente");
//        $security_service = $this->container->get('core.seguridad.service.security_token');
//        $entidad_id = $security_service->getEntidad()->getId();
//        $qb = $repopaciente->getQueryBuilderArchivo($entidad_id);
//        $where = $this->setWhere($qb);
//        $qb = $where["QueryBuilder"];
//        $totalData = $where["totalData"];
//        $qb = $this->setOrderBy($qb);
//        $qb = $this->setLimit($qb);
//        return $this->getPaginatorData($qb, $totalData);
//    }

    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse(Request $request)
    {
        $this->buildDatatable();
        $queryRequest = $request->query;
        $this->requestParams = $queryRequest->all();
        $globalSearch = $queryRequest->get('search')['value'];
        $individualFilter = false;
        if ($this->getOptions()->getIndividualFiltering()) {
            $columns = $queryRequest->get('columns');
            foreach ($columns as $key => $params) {
                if ($params['search']['value']) {
                    $individualFilter = true;
                    break;
                }
            }
        }
        $response = null;
        if ($globalSearch || $individualFilter) {
            $repopaciente = $this->em->getRepository("HCUEPacienteBundle:Paciente");
            $security_service = $this->container->get('core.seguridad.service.security_token');
            $entidad_id = $security_service->getEntidad()->getId();
            $buildQuery = $repopaciente->getQueryBuilderArchivo($entidad_id);
            $query = $this->dtmanager->getQueryFrom($this);
            $this->addSearchOrderColumn();
            $buildQuery = $this->setWhere($buildQuery);
            $buildQuery = $this->setOrderBy($buildQuery);
            $buildQuery = $this->setLimit($buildQuery);
            $query->setQuery($buildQuery);
            $response = $query->getResponse(false);
        } else {
            $fulldata = array('draw'            => $queryRequest->get('draw'),
                              'recordsTotal'    => 0,
                              'recordsFiltered' => 0,
                              'data'            => array()
            );
            $response = new JsonResponse($fulldata);
        }
        return $response;
    }

    private function setLimit(QueryBuilder $qb)
    {
        if (isset($this->requestParams['start']) && -1 != $this->requestParams['length']) {
            $qb->setFirstResult($this->requestParams['start'])
                ->setMaxResults($this->requestParams['length']);
        }
        return $qb;
    }

    private function setWhere(QueryBuilder $qb)
    {
        $globalSearch = $this->requestParams['search']['value'];
        $columns = $this->getColumnBuilder()->getColumns();
        // filtro global
        if ('' != $globalSearch) {
            $orExpr = $qb->expr()->orX();
            foreach ($columns as $key => $column) {
                $searchField = $column->getDql();
                if ($searchField == "edad") {
                    continue;
                }
                if (!empty($searchField)) {
                    $searchField = $this->replaceColumn($searchField);
                   /* $searchField = $this->lowerCase($searchField, $column);*/
                    $orExpr->add($qb->expr()->like($searchField, '?' . $key));
                   /* $globalSearch = strtolower($globalSearch);*/
                    $qb->setParameter($key, '%' . $globalSearch . '%');
                }
            }
            $qb->where($orExpr);
        }

        // filtro individual
        if (true === $this->getOptions()->getIndividualFiltering()) {
            $andExpr = $qb->expr()->andX();
            $i = 100;
            foreach ($columns as $key => $column) {
                $filter = $column->getFilter();
                $searchField = $this->searchColumns[$key];
                $searchValue
                    = $this->requestParams['columns'][$key]['search']['value'];
                if ('' != $searchValue && 'null' != $searchValue) {
                   /* $searchField = $this->lowerCase($searchField, $column);*/
                   /* $searchValue = strtolower($searchValue);*/
                    $andExpr = $filter->addAndExpression($andExpr, $qb, $searchField, $searchValue, $i);
                }
            }
            if ($andExpr->count() > 0) {
                $qb->andWhere($andExpr);
            }
        }

        return $qb;
    }

    private function lowerCase($searchField, $column)
    {
        if ('datetime' === $column->getAlias()
            || 'boolean' === $column->getAlias()
            || 'column' === $column->getAlias()
        ) {
            return 'LOWER(' . $searchField . ')';
        }
        return $searchField;
    }

    private function setOrderBy(QueryBuilder $qb)
    {
        if (isset($this->requestParams['order']) && count($this->requestParams['order'])) {
            $counter = count($this->requestParams['order']);

            for ($i = 0; $i < $counter; $i++) {
                $columnIdx
                    = (integer)$this->requestParams['order'][$i]['column'];
                $requestColumn = $this->requestParams['columns'][$columnIdx];
                $column = $this->orderColumns[$columnIdx];
                if ('true' == $requestColumn['orderable']) {
                    $qb->addOrderBy($column, $this->requestParams['order'][$i]['dir']);
                }
            }
        }
        return $qb;
    }

    private function addSearchOrderColumn()
    {
        $columns = $this->getColumnBuilder()->getColumns();
        foreach ($columns as $key => $column) {
            $name = $column->getDql();
            $name = $this->replaceColumn($name);
            true === $column->getOrderable() ? $this->orderColumns[] = $name
                : $this->orderColumns[] = null;
            true === $column->getSearchable() ? $this->searchColumns[] = $name
                : $this->searchColumns[] = null;
        }
    }

    private function replaceColumn($column)
    {
        if ($column == "paciente_id") {
            $column = "paciente.id";
        }
        if ($column == "nombrecompleto" || $column == "numeroidentificacion" || $column=="primernombre" || $column=="segundonombre" || $column=="apellidopaterno" || $column=="apellidomaterno" || $column=="cttipoidentificacion_id") {
            $column = "pp." . $column;
        }
        if ($column == "tipoidentificacion") {
            $column = "ct.descripcion";
        }
        $column = str_replace('numerohistoriaclinica', 'paciente.numerohistoriaclinica', $column);
        if (strpos($column, '.') == false) {
            $column = "pacientearchivo.$column";
        }

        return $column;
    }

}

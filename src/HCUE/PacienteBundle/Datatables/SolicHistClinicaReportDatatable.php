<?php

namespace HCUE\PacienteBundle\Datatables;

use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;
use Sg\DatatablesBundle\Datatable\Data\DatatableDataManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Core\SeguridadBundle\Security\Authorization\Voter\AppVoter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Dump\Container;

/**
 * Class HistorialAtencionDatatable
 *
 * @package HCUE\PacienteBundle\Datatables
 */
class SolicHistClinicaReportDatatable extends AbstractDatatableView
{
    /**
     * Variable de tipo DatatableDataManager para el manejo del QueryBuilder del datatables
     * @var DatatableDataManager $dtmanager
     */
    private $dtmanager;

    /**
     * Variable de tipo Container para el acceso  al contenedor
     * @var Container $container
     */
    private $container;

    /**
     * Asigna a la variable $dtmanager una instancia de la clase DatatableManager
     * @param DatatableDataManager $dtmanager
     */
    public function setDatatablesDataManager(DatatableDataManager $dtmanager)
    {
        $this->dtmanager = $dtmanager;
    }

    /**
     * Asigna a la variable $container una instancia de la clase Container
     * @param Container $container
     */
    function setContainer($container){
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {

        $this->topActions->set(array(
            'start_html' => '<div class="table-header">Ingrese el dato de búsqueda</div>',
            'actions'    => array()
        ));

        $this->features->set(array(
            'auto_width'    => true,
            'defer_render'  => false,
            'info'          => false,
            'jquery_ui'     => false,
            'length_change' => false,
            'ordering'      => true,
            'paging'        => true,
            'processing'    => true,
            'scroll_x'      => false,
            'scroll_y'      => '',
            'searching'     => true,
            'state_save'    => false,
            'delay'         => 0,
            'extensions'    => array(
                'dom'     => "<'row'<'col-sm-6'l><'col-sm-6'>>rt<'row'<'col-sm-5'i>>",
                'buttons' => array(
                    array(
                        "className"   => " btn-white btn-primary btn-bold",
                        'text'        => "<i class='fa fa-refresh bigger-110 green'></i> Recargar",
                        'titleAttr'   => 'Recargar',
                        'action_name' => 'reload'
                    ),
                    array(
                        'extend'    => 'pageLength',
                        "className" => " btn-white btn-primary btn-bold",
                        'text'      => "<i class='fa fa-bars bigger-110 blue'></i> Registros",
                        'titleAttr' => 'Visualización de registros'
                    )
                ),
                'responsive' => true,
                'select'     => array("style" => "single"),
                'keys'       => false
            )
        ));

        $this->ajax->set(array(
            'url'  => $this->router->generate('hcue_paciente_solic_hist_clinica_report_results'),
            'type' => 'GET'
        ));

        $this->options->set(array(
            'display_start'                 => 0,
            'defer_loading'                 => -1,
            'dom'                           => 'lfrtip',
            'length_menu'                   => array(5, 10, 25, 50, 100),
            'order_classes'                 => true,
            'order'                         => array(array(0, 'desc')),
            'order_multi'                   => true,
            'page_length'                   => 50,
            'paging_type'                   => Style::FULL_NUMBERS_PAGINATION,
            'renderer'                      => '',
            'scroll_collapse'               => false,
            'search_delay'                  => 0,
            'state_duration'                => 7200,
            'stripe_classes'                => array(),
            'class'                         => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering'          => true,
            'individual_filtering_position' => 'head',
            'use_integration_options'       => true,
            'force_dom'                     => false,
            'count_all_results'             => false,
            'count_filter_results'          => false,
        ));

        $this->columnBuilder
            ->add('id', 'column', array(
                'title'   => 'Id',
                'visible' => false
            ))
            ->add('atencionmedica_id', 'column', array(
                'title'   => 'Id',
                'visible' => false
            ))
            ->add('atencionmedica.paciente.persona.primernombre', 'column', array(
                'visible' => false,
                'class' => 'primernombre'
            ))
            ->add('atencionmedica.paciente.persona.segundonombre', 'column', array(
                'visible' => false,
                'class' => 'segundonombre'
            ))
            ->add('atencionmedica.paciente.persona.apellidopaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidopaterno'
            ))
            ->add('atencionmedica.paciente.persona.apellidomaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidomaterno'
            ))
            ->add('atencionmedica.paciente.persona.cttipoidentificacion_id', 'column', array(
                'visible' => false,
                'class' => 'tipoidentificacion_id',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('atencionmedica.paciente.persona.nombrecompleto', 'column', array(
                'title' => 'Apellidos y Nombres',
                'width' => '200px',
            ))
            ->add('atencionmedica.paciente.persona.cttipoidentificacion.descripcion', 'column', array(
                'title'  => 'Tipo Identificación',
                'width'  => '100px',
                'searchable'=>false,
            ))
            ->add('atencionmedica.paciente.persona.numeroidentificacion', 'column', array(
                'title' => 'Número Identificación',
                'width' => '120px',
                'class' => 'numeroidentificacion',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('atencionmedica.paciente.numerohistoriaclinica', 'column', array(
                'title' => 'Número Historia Clínica',
                'width' => '120px',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('entidad.nombreoficial', 'column', array(
                'title' => 'Establecimiento',
                'width' => '200px',
            ))
            ->add(null, 'action', array(
                'title'      => $this->translator->trans('datatables.actions.title'),
                'start_html' => '<div class="btn-group" >',
                'class' => 'center',
                'end_html'   => '</div>',
                'width'      => '60px',
                'actions' => array(
                    array(
                        'route' => 'hcue_paciente_atencion_imprimir',
                        'route_parameters' => array(
                            'id' => 'id',
                            'atencionmedica_id' => 'atencionmedica_id'
                        ),
                        'action_name'=>'printAction',
                        'icon' => 'fa fa-file-pdf-o bigger-125 fa-fw red',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Imprimir Atención',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            $solicreporthistoryclinic = $this->container->get("hcue.Paciente.manager.solicreporthistoriaclinica");
                            $isexistsSolic = $solicreporthistoryclinic->isExistsImpSolicReportHistory($row['id'], Constantes::CT_SOLICITUD_ASIGNADA);
                            return (($isexistsSolic) ? $this->authorizationChecker->isGranted(AppVoter::EXPORT): false);
                            //return $this->authorizationChecker->isGranted(AppVoter::EXPORT);
                        }
                    ),
                    array(
                        'route' => 'hcue_paciente_atencion_reimprimir_add',
                        'route_parameters' => array(
                            'id' => 'id',
                            'atencionmedica_id' => 'atencionmedica_id'
                        ),
                        'action_name'=>'reimprimirAction',
                        'icon' => 'fa fa-undo bigger-125 fa-fw blue',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Reimprimir Atención',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            $solicreporthistoryclinic = $this->container->get("hcue.paciente.manager.solicreporthistoriaclinica");
                            $isexistsSolic = $solicreporthistoryclinic->isExistsImpSolicReportHistory($row['id'], Constantes::CT_SOLICITUD_REIMPRESA_ASIGNADA);
                            return (($isexistsSolic) ? $this->authorizationChecker->isGranted(AppVoter::EXPORT): false);
                        }
                    ),
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'HCUE\PacienteBundle\Entity\Solicreporthistoriaclinica';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'historial_atencion_datatable';
    }

    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse()
    {
        $this->buildDatatable();
        $query = $this->dtmanager->getQueryFrom($this);
        $security = $this->container->get("core.seguridad.service.security_token");
        $entidadId = $security->getPerfilEntidad()->getEntidadId();
        $query->addWhereAll(function ($qb) use ($entidadId) {
            $qb->andWhere("solicreporthistoriaclinica.ctestadoimpresion_id = ".Constantes::CT_SOLICITUD_ASIGNADA." OR solicreporthistoriaclinica.ctestadoimpresion_id = ".Constantes::CT_SOLICITUD_REIMPRESA_ASIGNADA."")
                ->andWhere(
                    $qb->expr()->in(
                        'solicreporthistoriaclinica.entidad_id',
                        $entidadId
                    )
                );
        });

        return $query->getResponse();
    }

}
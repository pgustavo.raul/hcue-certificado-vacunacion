<?php

namespace HCUE\PacienteBundle\Datatables;

use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;
use Sg\DatatablesBundle\Datatable\Data\DatatableDataManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Core\SeguridadBundle\Security\Authorization\Voter\AppVoter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HistorialAtencionDatatable
 *
 * @package HCUE\PacienteBundle\Datatables
 */
class HistorialAtencionDatatable extends AbstractDatatableView
{
    /**
     * Variable de tipo DatatableDataManager para el manejo del QueryBuilder del datatables
     * @var DatatableDataManager $dtmanager
     */
    private $dtmanager;

    /**
     * Asigna a la variable $dtmanager una instancia de la clase DatatableManager
     * @param DatatableDataManager $dtmanager
     */
    public function setDatatablesDataManager(DatatableDataManager $dtmanager)
    {
        $this->dtmanager = $dtmanager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {

        $this->topActions->set(array(
            'start_html' => '<div class="table-header">Ingrese el dato de búsqueda</div>',
            'actions'    => array()
        ));

        $this->features->set(array(
            'auto_width'    => true,
            'defer_render'  => false,
            'info'          => false,
            'jquery_ui'     => false,
            'length_change' => false,
            'ordering'      => true,
            'paging'        => true,
            'processing'    => true,
            'scroll_x'      => false,
            'scroll_y'      => '',
            'searching'     => true,
            'state_save'    => false,
            'delay'         => 0,
            'extensions'    => array(
                'dom'     => "<'row'<'col-sm-6'l><'col-sm-6'>>rt<'row'<'col-sm-5'i>>",
                'buttons' => array(
                    array(
                        "className"   => " btn-white btn-primary btn-bold",
                        'text'        => "<i class='fa fa-refresh bigger-110 green'></i> Recargar",
                        'titleAttr'   => 'Recargar',
                        'action_name' => 'reload'
                    ),

                    array(
                        'extend'    => 'pageLength',
                        "className" => " btn-white btn-primary btn-bold",
                        'text'      => "<i class='fa fa-bars bigger-110 blue'></i> Registros",
                        'titleAttr' => 'Visualización de registros'
                    )
                ),
                'responsive' => true,
                'select'     => array("style" => "single"),
                'keys'       => false
            )
        ));

        $this->ajax->set(array(
            'url'  => $this->router->generate('hcue_paciente_historial_atencion_results'),
            'type' => 'GET'
        ));

        $this->options->set(array(
            'display_start'                 => 0,
            'defer_loading'                 => -1,
            'dom'                           => 'lfrtip',
            'length_menu'                   => array(5, 10, 25, 50, 100),
            'order_classes'                 => true,
            'order'                         => array(array(0, 'desc')),
            'order_multi'                   => true,
            'page_length'                   => 10,
            'paging_type'                   => Style::FULL_NUMBERS_PAGINATION,
            'renderer'                      => '',
            'scroll_collapse'               => false,
            'search_delay'                  => 0,
            'state_duration'                => 7200,
            'stripe_classes'                => array(),
            'class'                         => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering'          => true,
            'individual_filtering_position' => 'head',
            'use_integration_options'       => true,
            'force_dom'                     => false,
            'count_all_results'             => false,
            'count_filter_results'          => false,
        ));

        $this->columnBuilder
            ->add('id', 'column', array(
                'title'   => 'Id',
                'visible' => false
            ))
            ->add('persona.primernombre', 'column', array(
                'visible' => false,
                'class' => 'primernombre',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.segundonombre', 'column', array(
                'visible' => false,
                'class' => 'segundonombre',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.apellidopaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidopaterno',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.apellidomaterno', 'column', array(
                'visible' => false,
                'class' => 'apellidomaterno',
                'filter'=>array('text',array(
                    'search_type' => 'llike'
                ))
            ))
            ->add('persona.cttipoidentificacion_id', 'column', array(
                'visible' => false,
                'class' => 'tipoidentificacion_id',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('persona.nombrecompleto', 'column', array(
                'title' => 'Apellidos y Nombres',
                'width' => '200px',
            ))
            ->add('persona.cttipoidentificacion.descripcion', 'column', array(
                'title'  => 'Tipo Identificación',
                'width'  => '100px',
                'searchable'=>false,
            ))
            ->add('persona.numeroidentificacion', 'column', array(
                'title' => 'Número Identificación',
                'width' => '120px',
                'class' => 'numeroidentificacion',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('numerohistoriaclinica', 'column', array(
                'title' => 'Número Historia Clínica',
                'width' => '120px',
                'filter' => array('text', array(
                    'search_type' => 'eq'
                ))
            ))
            ->add('persona.fechanacimiento', 'datetime', array(
                'title'       => 'Fecha Nacimiento',
                'width'       => '80px',
                'class'       => 'datedatatable',
                'date_format' => 'DD-MM-YYYY'
            ))
            ->add('persona.pais.nacionalidad', 'column', array(
                'title' => 'Nacionalidad',
                'width' => '100px'
            ))
            ->add(null, 'action', array(
                'title'      => $this->translator->trans('datatables.actions.title'),
                'start_html' => '<div class="btn-group" >',
                'end_html'   => '</div>',
                'width'      => '60px',
                'actions' => array(
                    array(
                        'route' => 'hcue_paciente_historial_atencion_show',
                        'route_parameters' => array(
                            'paciente_id' => 'id'
                        ),
                        'action_name' => 'showAction',
                        'icon' => 'ace-icon fa fa-eye  bigger-120 fa-fw',
                        'attributes' => array(
                            'rel'   => 'tooltip',
                            'title' => 'Mostrar Historial Médico',
                            'class' => 'blue',
                            'role'  => 'button'
                        ),
                        'render_if' => function () {
                            return $this->authorizationChecker->isGranted(AppVoter::VIEW);
                        }
                    ),
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'HCUE\PacienteBundle\Entity\Paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'historial_atencion_datatable';
    }

    /**
     * Retorna la respuesta de datos en formato json de datatables
     * @return json
     */
    public function getResponse(Request $request)
    {
        $this->buildDatatable();
        $queryRequest = $request->query;

        $globalSearch = $queryRequest->get('search')['value'];
        $individualFilter = false;
        if ($this->getOptions()->getIndividualFiltering()) {
            $columns = $queryRequest->get('columns');
            foreach ($columns as $key => $params) {
                if ($params['search']['value']) {
                    $individualFilter = true;
                    break;
                }
            }
        }
        $response = null;
        if ($globalSearch || $individualFilter) {
            $query = $this->dtmanager->getQueryFrom($this);
            $query->addWhereAll(function ($qb) {
                $qb->andWhere("paciente.activo = 1")
                    ->andWhere("persona.activo = 1");
            });
            $response = $query->getResponse();
        } else {
            $fulldata = array('draw' => $queryRequest->get('draw'),
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => array()
            );
            $response = new JsonResponse($fulldata);
        }
        return $response;
    }
}
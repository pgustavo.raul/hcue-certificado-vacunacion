<?php

/**
 * Servicio para manejar los datos de la Aceptacion de vacuna
 * Descripcion de AceptavacunaManager
 * @author Luis Nuñez
 */
namespace HCUE\PacienteBundle\Manager;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Archivo;
use Core\AppBundle\Manager\BaseManager;
use HCUE\PacienteBundle\Entity\Constantes;

class AgendavacunaManager extends BaseManager
{


    /**
     * Permite guardar un nuevo registro de aceptacion de vacuna
     * @param Request $request
     * @return object
     */
    public function consultaAgenda($request,$pacienteManager,$pacientecovidManager)
    {
        $dataAgenda='';
        $origen = '';
        $correo = '';
        try {
            $identificacion=$request->get('identificacion');
            $dataAgenda = $this->findOneBy(array('identificacion'=>$identificacion));
            if($dataAgenda){
                $dataPacienteCovid = $pacientecovidManager->findOneBy(array('cedula'=>$identificacion));
                $dataPaciente = $pacienteManager->findOneBy(array('numerohistoriaclinica'=>$identificacion));
                if($dataPaciente){
                    $correo = $dataPaciente->getCorreo();
                }
                if($dataPacienteCovid){
                    if($dataPacienteCovid->getOrganico()){
                        $origen = $dataPacienteCovid->getOrganico()->getDescripcion();
                    }
                }
                $status= true;
                $message = 'La consulta se ha realizado con exito';
            }
            else{
                $status= false;
                $message =  '<label>Usted aún no tiene confirmado un agendamiento, si ya registró su <a href="https://sgrdacaa-admision.msp.gob.ec/hcue/paciente/paciente/public/aceptacionvacuna">encuesta</a> se recomienda consultar este link de manera periódica.</label>';
            }
        } catch (Exception $exc) {
            throw $exc;
            $status= false;
            $message = '';
        }
        return array('status'=>$status,'message'=>$message,'data'=>$dataAgenda,'origen'=>$origen,'correo'=>$correo);
    }
}
<?php

/**
 * Servicio para manejar los datos del Numero de Archivo
 * Descripcion de ArchivoManager
 * @author Richard Veliz
 */
namespace HCUE\PacienteBundle\Manager;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Archivo;
use Core\AppBundle\Manager\BaseManager;

class ArchivoManager extends BaseManager
{

    /**
     * Crea el objeto formulario para su respectivo modelo
     * @param integer $id      Id del registro
     * @param  Type   $type
     * @param  array  $options array de opciones
     * @return formType
     */
    public function createForm($id, $type, $options = array())
    {
        $modelo = $this->create();
        if ($id) {
            $modelo = $this->find($id);
            if (empty($modelo)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')
            ->create($type, $modelo, $options);
    }

    /**
     * Guarda los datos del Archivo en la Base de Datos
     * @param Archivo $objArchivo informacion del formulario de archivo
     * @throws \Exception
     * @return boolean true si la informacion se guardo con exito
     */
    public function saveArchivo(Archivo $objArchivo)
    {
        $status = 0;
        $this->beginTransaction();
        $servicepaciente = $this->getContainer()
            ->get("hcue.paciente.entity.manager.paciente");
        try {
            $objArchivo->setPaciente($servicepaciente->find($objArchivo->getPacienteId()));
            $status = 1;
            $this->save($objArchivo);
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }
        return $status;
    }

    /**
     * Verifica si existe creado el mismo número de archivo para la misma Entidad
     * @param integer $numeroarchivo numero de archivo
     * @return array("existe_archivo"=>$existe,"id"=>$id)
     */
    public function existeArchivo($numeroarchivo, $entidad_id)
    {
        $existe = false;
        $id = 0;
        $archivo = $this->findOneBy(array("entidad_id"    => $entidad_id,
                                          "numeroarchivo" => $numeroarchivo,
                                          "estado"        => 1
        ));
        if ($archivo) {
            $existe = true;
            $id = $archivo->getId();
        }
        return array("existe_archivo" => $existe, "id" => $id);
    }

    /**
     * Obtiene el Id del Archivo
     * @param string  $valor         valor a buscar
     * @param integer $entidad_id    Id de la Entidad
     * @param boolean $porpacienteid true si se requiere buscar por el id del paciente
     * @return integer
     */
    public function getIdArchivo($valor, $entidad_id, $porpacienteid)
    {
        return $this->getRepository()
            ->getIdArchivo($valor, $entidad_id, $porpacienteid);
    }

    /**
     * Obtiene el total de paciente con o sin archivo
     * @param boolean $entidad_id Id de la Entidad
     * @param boolean $sin_archivo
     * @return interger
     */
    public function getCountArchivo($entidad_id, $sin_archivo = false)
    {
        return $this->getRepository()
            ->getCountArchivo($entidad_id, $sin_archivo);
    }
}

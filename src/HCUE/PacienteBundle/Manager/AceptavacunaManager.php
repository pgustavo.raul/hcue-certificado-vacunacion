<?php

/**
 * Servicio para manejar los datos de la Aceptacion de vacuna
 * Descripcion de AceptavacunaManager
 * @author Luis Nuñez
 */
namespace HCUE\PacienteBundle\Manager;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Archivo;
use Core\AppBundle\Manager\BaseManager;
use HCUE\PacienteBundle\Form\Type\AceptavacunaType;
use HCUE\PacienteBundle\Entity\Constantes;

class AceptavacunaManager extends BaseManager
{

    /**
     * Crea el objeto formulario para el modelo aceptacion de vacuna
     * @param integer $id Id del registro Aceptacion de vacuna
     * @return formType
     */
    public function createForm($id=0){
        $aceptavacuna= $this->create();
        if($id){
            $aceptavacuna=  $this->find($id);
            if (empty($aceptavacuna)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create(AceptavacunaType::class, $aceptavacuna);
    }

    /**
     * Permite guardar un nuevo registro de aceptacion de vacuna
     * @param Request $request
     * @return object
     */
    public function saveAceptavacuna($request,$aceptavacunaManager,$pacienteCovidManager)
    {

        $ctvacunacovid_id =  $this->getContainer()->getParameter('app_public')['ctvacunacovid_id'];

        $form = $request->get('aceptavacuna');


        $this->beginTransaction();

        try {
            $form = $request->get('aceptavacuna');

            $tipoIdentificacion = $form['cttipoidentificacion'];
            $identificacion = $form['identificacion'];
            $nombre = $form['nombre'];
            $fechanacimiento = $form['fechanacimiento'];
            $ctsexo= $form['ctsexo'];
            $ctestadocivil = $form['ctestadocivil'];
            $estado = 1;
            $correo = $form['correo'];
            /*$entidad = $form ['entidad'];*/

            /*Preguntas*/
            $alergia = 0;
            $respiratoria = 0;
            $hemorragia = 0;
            $inmuno = 0;
            $enfermedad = 0;
            $embarazo = 0;
            $lactancia = 0;
            $covid =0;
            $vacunaid = (isset($form ['vacuna_id']))?$form ['vacuna_id']:$ctvacunacovid_id;
            $cttiporegistro_id = (isset($form ['cttiporegistro_id']))?$form ['cttiporegistro_id']:Constantes::CT_ACEPTA_VACUNA_REGISTRO_PACIENTE;
            if($cttiporegistro_id == Constantes::CT_ACEPTA_VACUNA_REGISTRO_PACIENTE){
                $result = $pacienteCovidManager->findOneBy(array('cedula'=>$identificacion));
                if(!$result){
                    $this->rollback();
                    return array('status'=>false,'message'=>"Usted no está seleccionad@ para esta fase de vacunación");
                }
            }
            /*Termina Preguntas*/
            if(isset($form['id'])){
                $aceptavacuna=$this->find($form['id']);
            }else{
                $aceptavacuna = $this->create();

            }

            $aceptavacuna->setCttipoidentificacion($this->getEntityManager()->getReference('CoreAppBundle:Catalogo', $tipoIdentificacion));
            $aceptavacuna->setIdentificacion($identificacion);
            $aceptavacuna->setNombre($nombre);
            $aceptavacuna->setFechanacimiento($fechanacimiento);
            $aceptavacuna->setCtsexo($this->getEntityManager()->getReference('CoreAppBundle:Catalogo', $ctsexo));
            $aceptavacuna->setCtestadocivil($this->getEntityManager()->getReference('CoreAppBundle:Catalogo', $ctestadocivil));
            $aceptavacuna->setEstado($estado);
            $aceptavacuna->setCorreo($correo);
/*            $aceptavacuna->setEntidad($this->getEntityManager()->getReference('CoreAppBundle:Entidad',$entidad));*/
            $aceptavacuna->setVacuna($this->getEntityManager()->getReference('HCUEAtencionMedicBundle:Vacuna',$vacunaid));
            $aceptavacuna->setAlergia($alergia);
            $aceptavacuna->setRespiratoria($respiratoria);
            $aceptavacuna->setHemorragia($hemorragia);
            $aceptavacuna->setInmuno($inmuno);
            $aceptavacuna->setEnfermedad($enfermedad);
            $aceptavacuna->setEmbarazo($embarazo);
            $aceptavacuna->setLactancia($lactancia);
            $aceptavacuna->setCovid($covid);
            $aceptavacuna->setCttiporegistro($this->getEntityManager()->getReference('CoreAppBundle:Catalogo', $cttiporegistro_id));
            $this->save($aceptavacuna, false);
            $status= true;
            $message = 'El acuerdo de aceptación de vacunación se ha guardado con exito';
        } catch (Exception $exc) {
            $this->rollback();
            throw $exc;
            $status= false;
            $message = 'No se ha podido registar la aceptación de la vacuna';
        }
        return array('status'=>$status,'message'=>$message);
    }

    public function validRegistro($request,$aceptavacunaManager,$pacienteCovidManager){
        $status = true;
        $message = "";
        $correo = "";
        if ($aceptavacunaManager->findOneBy(array('identificacion'=>$request->get('identificacion'),'cttipoidentificacion_id'=>$request->get('tipoidentificacion')))){
            $status = false;
            $message = "Usted no puede registrarse nuevamente";
        }
        $dataValidaMail=$pacienteCovidManager->findOneBy(array('cedula'=>$request->get('identificacion')));
        if(!$dataValidaMail){
            $status = false;
            $message = "Usted no está seleccionad@ para esta fase de vacunación";
        }
        else{
            $correo = $dataValidaMail->getCorreo();
        }
        return array('status'=>$status,'message'=>$message,'correo'=>$correo);
    }

}
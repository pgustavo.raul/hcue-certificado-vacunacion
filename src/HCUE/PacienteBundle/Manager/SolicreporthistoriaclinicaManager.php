<?php
/**
 * Clase de manipulación del repositorio SolicitudHistorialAtencion
 * @author Miguel Faubla
 */
namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
//use HCUE\PacienteBundle\Form\Type\SolicreporthistoriaclinicaType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Constantes;

class SolicreporthistoriaclinicaManager extends BaseManager{

    /**
     * Guarda los datos de un formulario basado en un modelo Solicreporthistoriaclinica
     * @param array $solicreporthistoriaclinica
     * @param integer $atencionmedica_id Id de la AtencionMedica
     **/
    public function saveSolicReportHistory($solicreporthistoriaclinica, $atencionmedica_id) {

        $catalogoManager = $this->getContainer()->get('core.app.entity.manager.catalogo');
        $atencionmedicaManager = $this->getContainer()->get('hcue.amed.manager.atencionmedica');
        $entidadManager = $this->getContainer()->get('core.app.entity.manager.entidad');
        $usuarioManager = $this->getContainer()->get('core.seguridad.manager.usuario');

        $catalogo = $catalogoManager->find(Constantes::CT_SOLICITUD_APROBADA);
        $atencionmedica = $atencionmedicaManager->find($atencionmedica_id);
        $entidad = $entidadManager->find($solicreporthistoriaclinica["entidad_id"]);
        $usuario = $usuarioManager->find($solicreporthistoriaclinica["usuario_id"]);

        try{
            $this->beginTransaction();

            $solicreporthistoriaclinicaObject = $this->create();
            $solicreporthistoriaclinicaObject->setNumerodocumento($solicreporthistoriaclinica["numerodocumento"]);
            $solicreporthistoriaclinicaObject->setMotivo($solicreporthistoriaclinica["motivo"]);
            $solicreporthistoriaclinicaObject->setProfesionalunidad($solicreporthistoriaclinica["profesionalunidad"]);
            $solicreporthistoriaclinicaObject->setEntidad($entidad);
            $solicreporthistoriaclinicaObject->setAtencionmedica($atencionmedica);
            $solicreporthistoriaclinicaObject->setUsuarioresponsable($usuario);
            $solicreporthistoriaclinicaObject->setCtestadoimpresion($catalogo);

            $this->save($solicreporthistoriaclinicaObject);
            $this->commit();
        } catch (\Exception $ex){
            $this->rollBack();
            throw $ex;
        }

    }

    /**
     * Actualiza el estado del historial de solicitud de historias clinicas a estado IMPRESO basado en un modelo Solicreporthistoriaclinica
     * @param integer $solicitud_id Id de la Solicitud
     **/
    public function updateStateSolicitudReport($solicitud_id, $params = array()) {
        $catalogoManager = $this->getContainer()->get('core.app.entity.manager.catalogo');
        $usuarioManager = $this->getContainer()->get('core.seguridad.manager.usuario');
        $catalogo = $catalogoManager->find($params["ctestadosolicitud_id"]);
        $usuario = (array_key_exists('usuario_id', $params)) ? $usuarioManager->find($params["usuario_id"]) : null;

        try{
            $this->beginTransaction();
            $solicreporthistoriaclinica = $this->find($solicitud_id);
            ($usuario) ? $solicreporthistoriaclinica->setUsuarioactual($usuario) : null;
            $solicreporthistoriaclinica->setCtestadoimpresion($catalogo);

            $this->save($solicreporthistoriaclinica);
            $this->commit();
        } catch (\Exception $ex){
            $this->rollBack();
            throw $ex;
        }
    }



    /**
     * Obtiene si ya se ha registrado la solicitud para la impresión del reporte 002
     * @param integer $atencionmedica_id Id de la AtencionMedica
     * @return boolean
     */
    public function isExistsSolicReportHistory($atencionmedica_id) : bool
    {
        return $this->getRepository()->isExistsSolicReportHistory($atencionmedica_id);
    }

    /**
     * Obtiene si la solicitud se encuentra aprobada para la impresión del reporte 002
     * @param integer $id Id del registro Solicreporthistoriaclinica
     * @param integer $estadosolicitud Id del catalogo de estados para la impresion de la historia clinica
     * @return boolean
     */
    public function isExistsImpSolicReportHistory($id, $estadosolicitud) : bool
    {
        return $this->getRepository()->isExistsImpSolicReportHistory($id, $estadosolicitud);
    }


}

<?php 
/*
* @autor Jipson Montalbán
*/

namespace HCUE\PacienteBundle\Manager;
use HCUE\VacunacionBundle\Entity\Dbcontadorcertificado;


use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;


/**
* Clase de manipulación del repositorio Dbcontadorcertificado
*/
class DbcontadorcertificadoManager extends BaseManager{

	/**
        * Inactiva o elimina un registro basado en el modelo Dbcontadorcertificado            
        * @param integer $id Id del registro Dbcontadorcertificado 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $dbcontadorcertificado=  $this->find($id);
            if (!$dbcontadorcertificado) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            $dbcontadorcertificado->setActivo(0); 
            $this->save($dbcontadorcertificado,$flush);                
            
            //$this->delete($dbcontadorcertificado); 
                
            return true;
         }

        /**
         * Guarda los datos de la Vacunacion para el contador de solicitudes de certificadosen la Base de Datos
         * @param array $objVacunacion
        * @return object
        */
    public function saveContadorcertificado($objVacunacion)
    {
        $status = 0;
        $this->beginTransaction();
        try {
            $status = $this->save($objVacunacion);
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }
        return $status;
    }

}
?>
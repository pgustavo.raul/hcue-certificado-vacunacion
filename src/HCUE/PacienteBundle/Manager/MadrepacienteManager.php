<?php
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Form\Type\MadrepacienteType;

/**
 * Clase de manipulación del repositorio Madrepaciente
 */
class MadrepacienteManager extends BaseManager{

    /**
     * Crea el objeto formulario para el modelo Madrepaciente
     * @param integer $id Id del registro Madrepaciente
     * @return formType
     */
    public function createForm($id=0,$options = array()){
        $madrepaciente= $this->create();
        if($id){
            $madrepaciente=  $this->find($id);
            if (empty($madrepaciente)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')->create(MadrepacienteType::class, $madrepaciente,$options);
    }

    /**
     * Inactiva o elimina un registro basado en el modelo Madrepaciente
     * @param integer $id Id del registro Madrepaciente
     * @param boolean $flush Indica si se realiza el flush en la persistencia
     * @return boolean true si se realizó la operación con éxito
     */
    public function deleteById($id,$flush=true) {
        $madrepaciente=  $this->find($id);
        if (!$madrepaciente) {
            throw new NotFoundHttpException("No se encontró un registro con id $id");
        }
        $madrepaciente->setActivo(0);
        $this->save($madrepaciente,$flush);

        //$this->delete($madrepaciente);

        return true;
    }

    /**
     * Verifica si existe creado una Madrepaciente con el mismo numero de idetificación
     * @param integer $numeroidentificacion numero de identificación del paciente
     * @return array("existe_persona"=>$existePersona,"persona_id"=>$persona_id,"existe_madrepcte"=>$existeMadrepcte,"id"=>$madrepcte_id)
     */
    public function existMadrepcte($numeroidentificacion)
    {
        $existePersona = false;
        $existeMadrepcte = false;
        $madrepcte_id = 0;
        $servicePersona = $this->getContainer()->get("core.app.entity.manager.persona");
        if ($servicePersona->existPersona($numeroidentificacion)) {
            $existePersona = true;
            $madrepcte = $this->getRepository()->getMadrepcteByIdentificacion($numeroidentificacion);
            if ($madrepcte) {
                $existeMadrepcte = true;
                $madrepcte_id = $madrepcte->getId();
            }
        }

        return array(
            "existe_persona"   => $existePersona,
            "existe_madrepcte" => $existeMadrepcte,
            "id"               => $madrepcte_id,
        );
    }




    /**
     * Método para guardar la información de los datos de la madre del paciente
     * @param $rmadrepcte
     */
    public function getMadrepcte($request){
        $managerPersona = $this->getContainer()->get("core.app.entity.manager.persona");
        $managerMadrepcte = $this->getContainer()->get("hcue.paciente.entity.manager.madrepaciente");
        $madrepcte=null;
        $rmadrepcte = $request->get('paciente')['madrepcte'];
        $rpersona = $rmadrepcte["persona"];
        $numeroidentificacion = "";
        if (is_array($rpersona)) {
            $numeroidentificacion = (array_key_exists('numeroidentificacion', $rpersona)) ? $rpersona["numeroidentificacion"] : '';
            $cttipoidentificacion = (array_key_exists('cttipoidentificacion', $rpersona)) ? $rpersona["cttipoidentificacion"] : '';

        }
        $id_representante_noiden=(array_key_exists('persona_madre_paciente_id', $rmadrepcte)) ? $rmadrepcte["persona_madre_paciente_id"] : '';
        if($id_representante_noiden){
             $persona = $managerPersona->find($id_representante_noiden);
        }
        else{
            $persona = $managerPersona->findOneBy(array('numeroidentificacion' => $numeroidentificacion,'cttipoidentificacion_id'=>$cttipoidentificacion,'activo'=>Constantes::CT_ESTADOGENERALACTIVO));
        }
        if ($persona){
            $madrepcte=$managerMadrepcte->findOneBy(array("persona" => $persona,'estado'=>Constantes::CT_ESTADOGENERALACTIVO,'activo'=>Constantes::CT_ESTADOGENERALACTIVO));
        }
        return $madrepcte;

    }

}

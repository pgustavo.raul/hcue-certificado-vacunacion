<?php 
/*
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Fausto Suarez
*/

namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
* Clase de manipulación del repositorio Estrategiapaciente
*/
class EstrategiapacienteManager extends BaseManager{

    /**
    * Devuelve la estrategia según el ID del paciente seleccionado
    */
    public function getEstrategiapacienteByIdPaciente($id)
    {                
        $results = $this->getRepository()->getEstrategiapacienteByIdPaciente($id);
        return $results;
    }
}
?>
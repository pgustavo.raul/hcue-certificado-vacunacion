<?php 
/*
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Jipson Montalbán
*/

namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
* Clase de manipulación del repositorio Dbvacunacion
*/
class DbvacunacionManager extends BaseManager{

	/**
        * Crea el objeto formulario para el modelo Dbvacunacion            
        * @param integer $id Id del registro Dbvacunacion     
        * @return formType  
        */
	public function createForm($id=0,$options = array()){
                $dbvacunacion= $this->create();
                if($id){
                    $dbvacunacion=  $this->find($id);
                    if (empty($dbvacunacion)) {                                         
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }       
                }
                return $this->getContainer()->get('form.factory')->create(DbvacunacionType::class, $dbvacunacion,$options); 
        }

	/**
        * Inactiva o elimina un registro basado en el modelo Dbvacunacion            
        * @param integer $id Id del registro Dbvacunacion 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $dbvacunacion=  $this->find($id);
            if (!$dbvacunacion) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            $dbvacunacion->setActivo(0); 
            $this->save($dbvacunacion,$flush);                
            
            //$this->delete($dbvacunacion); 
                
            return true;
         }

        /**
        * Toma los datos basicos de vacunacion del paciente            
        * @param integer $dni
        * @param integer $fechaanacimiento
        * @return object paciente  
        */
        public function getPacienteVacunacionVacunometro($dni,$fechanacimiento){
                $result=$this->getRepository()->getPacienteVacunacionVacunometro($dni,$fechanacimiento);
                return $result;
        } 
        
        /**
        * Toma los datos basicos de vacunacion del paciente            
        * @param integer $dni
        * @return object paciente  
        */
        public function getPacienteVacunacionVacunometroBycedula($dni){
                $result=$this->getRepository()->getPacienteVacunacionVacunometroBycedula($dni);
                return $result;
        } 

}
?>
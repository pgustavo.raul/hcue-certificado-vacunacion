<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Diego Orellana
*/

namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Form\Type\CargavacunacionType;
use HCUE\PacienteBundle\Entity\Constantes;
use HCUE\AtencionMedicBundle\Entity\Registrovacunacion;

/**
* Clase de manipulación del repositorio Cargavacunacion
*/
class CargavacunacionManager extends BaseManager{

	/**
        * Crea el objeto formulario para el modelo Cargavacunacion
        * @param integer $id Id del registro Cargavacunacion
        * @return formType
        */
	public function createForm($id=0,$options = array()){
                $cargavacunacion= $this->create();
                if($id){
                    $cargavacunacion=  $this->find($id);
                    if (empty($cargavacunacion)) {
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }
                }
                return $this->getContainer()->get('form.factory')->create(CargavacunacionType::class, $cargavacunacion,$options);
        }

	/**
        * Inactiva o elimina un registro basado en el modelo Cargavacunacion
        * @param integer $id Id del registro Cargavacunacion
        * @param boolean $flush Indica si se realiza el flush en la persistencia
        * @return boolean true si se realizó la operación con éxito
        */
	public function deleteById($id,$flush=true) {
            $cargavacunacion=  $this->find($id);
            if (!$cargavacunacion) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
            $cargavacunacion->setActivo(0);
            $this->save($cargavacunacion,$flush);

            //$this->delete($cargavacunacion);

            return true;
         }

     /**
     * Valida si el archivo csv es valido, campos obligatorios
     * @param  $arraycsv Archivo con los registros de vacunas
     * @param  $vacunas Catalogo de Vacunas
     * @param  $managerCatalogo Manager de Catalogos para validar Tipo Identificación y Grupo Poblacional
     * @param  $entidades Catálogo de Entidades
     * @param  $esquemas Catálogo de Esquemas de Vacunación
     *
     */
    public function validateArrayCsv($arraycsv, $arrayVacunas, $managerCatalogo, $arrayEntidades, $arrayEsquemas){
        $container = $this->getContainer();
        $utilDateTime=$container->get("core.app.util.datetime");
        $utilTipoIdentificacion=$container->get("core.app.util.tipoidentificacion");
        $utilremoveAccent=$container->get('core.app.util.string');
        $arrayCatTipoIdent = $managerCatalogo->getCatalogo(Constantes::TC_TIPOIDENTIFICACION, true);
        $arrayCatGrupPobla = $managerCatalogo->getCatalogo(Constantes::TC_GRUPORIESGO_VACUNACION, true);
        $managerPais = $container->get("core.app.entity.manager.pais");
        $managerProvincia = $container->get("core.app.entity.manager.provincia");
        $arrayerrores=array();
        $msjerror="";
        $count=count($arraycsv);

        if($count){
            for($x=1;$x< $count;$x++){
                $msjerror="";
                try {
                    $año_aplicacion = empty($arraycsv[$x]['año_aplicacion']) ? '' : $arraycsv[$x]['año_aplicacion'];
                    $mes_aplicacion = empty($arraycsv[$x]['mes_aplicacion']) ? '' : $arraycsv[$x]['mes_aplicacion'];
                    $dia_aplicacion = empty($arraycsv[$x]['dia_aplicacion']) ? '' : $arraycsv[$x]['dia_aplicacion'];
                    $unicodigo_entidad = empty($arraycsv[$x]['unicodigo_entidad']) ? '' : intval($arraycsv[$x]['unicodigo_entidad']);
                    $apellidos = empty($arraycsv[$x]['apellidos']) ? '' : mb_strtoupper($arraycsv[$x]['apellidos'],'UTF-8');
                    $nombres = empty($arraycsv[$x]['nombres']) ? '' : mb_strtoupper($arraycsv[$x]['nombres'],'UTF-8');
                    $tipo_iden = empty($arraycsv[$x]['tipo_iden']) ? '' : mb_strtoupper($arraycsv[$x]['tipo_iden'],'UTF-8');
                    $num_iden = empty($arraycsv[$x]['num_iden']) ? '' : $arraycsv[$x]['num_iden'];
                    $sexo = empty($arraycsv[$x]['sexo']) ? '' : mb_strtoupper($arraycsv[$x]['sexo'],'UTF-8');
                    $año_nacimiento = empty($arraycsv[$x]['año_nacimiento']) ? '' : $arraycsv[$x]['año_nacimiento'];
                    $mes_nacimiento = empty($arraycsv[$x]['mes_nacimiento']) ? '' : $arraycsv[$x]['mes_nacimiento'];
                    $dia_nacimiento = empty($arraycsv[$x]['dia_nacimiento']) ? '' : $arraycsv[$x]['dia_nacimiento'];
                    $correo_electronico = empty($arraycsv[$x]['correo_electronico']) ? '' : $arraycsv[$x]['correo_electronico'];
                    $pobla_vacuna = empty($arraycsv[$x]['pobla_vacuna']) ? '' : mb_strtoupper($arraycsv[$x]['pobla_vacuna'],'UTF-8');
                    $fase_vacuna = empty($arraycsv[$x]['fase_vacuna']) || !is_numeric($arraycsv[$x]['fase_vacuna']) ? 0 : intval($arraycsv[$x]['fase_vacuna']);
                    $nom_vacuna = empty($arraycsv[$x]['nom_vacuna']) ? '' : mb_strtoupper($arraycsv[$x]['nom_vacuna'],'UTF-8');
                    $lote_vacuna = empty($arraycsv[$x]['lote_vacuna']) ? '' : mb_strtoupper($arraycsv[$x]['lote_vacuna'],'UTF-8');
                    $dosis_aplicada = empty($arraycsv[$x]['dosis_aplicada']) ? '' : intval($arraycsv[$x]['dosis_aplicada']);
                    $nom_profesional_aplica = empty($arraycsv[$x]['nom_profesional_aplica']) ? '' : mb_strtoupper($arraycsv[$x]['nom_profesional_aplica'],'UTF-8');
                    $nom_profesional_registra = empty($arraycsv[$x]['nom_profesional_registra']) ? '' : mb_strtoupper($arraycsv[$x]['nom_profesional_registra'],'UTF-8');
                    $iden_profesional_aplica = empty($arraycsv[$x]['iden_profesional_aplica']) ? '' : $arraycsv[$x]['iden_profesional_aplica'];
                    $nacionalidad = empty($arraycsv[$x]['nacionalidad']) ? '' : $arraycsv[$x]['nacionalidad'];
                    $exteriordosisprevia = (mb_strtoupper($arraycsv[$x]['exteriordosisprevia'],'UTF-8') == 'SI') ? 1 : 0;

                    if($apellidos == 'NO IDENTIFICADO' || $apellidos == '' || $nombres == ''){
                        $msjerror = $msjerror . ' Los nombres y apellidos del paciente son obligatorios,';
                    }

                    $registro_valido = false;
                    $getcatalogo_id  = 0;
                    foreach ($arrayCatTipoIdent as $tipoIdent){
                        if ($utilremoveAccent->removeAccent($tipo_iden) === $utilremoveAccent->removeAccent(mb_strtoupper($tipoIdent['descripcion'],'UTF-8'))){
                            $registro_valido = true;
                            $getcatalogo_id = $tipoIdent['id'];
                            $arraycsv[$x]['tipo_iden_id'] = $tipoIdent['id'];
                            break;
                        }
                    }

                    if (!$registro_valido){
                        $msjerror = $msjerror . ' Tipo de identificación inválida, ';
                    }
                    else{
                        if ($getcatalogo_id == 6){
                            $arrayresult=$utilTipoIdentificacion->validarCedula($num_iden);
                            if(!$arrayresult['validacion']){
                                $msjerror = $msjerror . ' Cédula Paciente Inválida,';
                            }
                        }
                        else{
                            $nacionalidad = $managerPais->searchNacionalidad($arraycsv[$x]['nacionalidad']);
                            if ($nacionalidad){
                                $nacionalidad_id = $nacionalidad->getId();
                                $arraycsv[$x]['nacionalidad_id'] = $nacionalidad_id;
                                if ($nacionalidad_id == Constantes::CT_NACIONALIDAD_ECUATORIANA) {
                                    $provincia = $managerProvincia->searchProvincia($arraycsv[$x]['provincia']);
                                    if ($provincia){
                                        $provincia_id = $provincia->getID();
                                    }
                                    else{
                                        $msjerror = $msjerror . ' Provincia no existente,';
                                    }
                                }
                                else {
                                    $provincia_id = "99";
                                }
                                if ($msjerror == ''){
                                    $arraycsv[$x]['provincia_id'] = $provincia_id;
                                }
                            }
                            else{
                                $arraycsv[$x]['nacionalidad_id'] = "";
                                $arraycsv[$x]['provincia_id'] = "";
                            }
                        }
                    }

                    if($nom_profesional_aplica == '' || $iden_profesional_aplica == ''){
                        $msjerror = $msjerror . ' La identificación y el nombre del vacunador son obligatorios,';
                    }

                    if($nom_profesional_registra == 'POR DETERMINAR' || $nom_profesional_registra == 'NOM_PROFESIONAL_REGISTRA' || $nom_profesional_registra == ''){
                        $arraycsv[$x]['nom_profesional_registra'] = 'POR ACTUALIZAR';
                    }

                    if ($sexo <> 'HOMBRE' && ($sexo <> 'MUJER')){
                        $msjerror = $msjerror . ' Sexo inválido HOMBRE/MUJER,';
                    }
                    else{
                        $arraycsv[$x]['sexo_id'] = ($sexo == 'HOMBRE') ? Constantes::CT_SEXO_HOMBRE : Constantes::CT_SEXO_MUJER;
                    }

                    $registro_valido = false;
                    $getvacuna_id = 0;
                    foreach ($arrayVacunas as $vacuna){
                        if ($utilremoveAccent->removeAccent($nom_vacuna) === $utilremoveAccent->removeAccent(mb_strtoupper($vacuna['nombrevacuna'],'UTF-8'))){
                            $registro_valido = true;
                            $getvacuna_id = $vacuna['id'];
                            break;
                        }
                    }

                    if (!$registro_valido){
                        $msjerror = $msjerror . ' La vacuna no existe en el PRAS, ';
                    }

                    if ($getvacuna_id > 0){
                        $sfechavacunacion = $año_aplicacion . str_pad($mes_aplicacion,2,'0',STR_PAD_LEFT) . str_pad($dia_aplicacion,2,'0',STR_PAD_LEFT);
                        $result=$utilDateTime->validarFecha($sfechavacunacion, 'Ymd');
                        if(!$result){
                            $msjerror = $msjerror . ' Fecha de vacuna inválida, ';
                        }
                        else{
                            $fechavacunacion = date_create($sfechavacunacion);
                            foreach ($arrayVacunas as $vacuna){
                                if (!empty($vacuna['fechainicia']) && $getvacuna_id == $vacuna['id']){
                                    if ($fechavacunacion < $vacuna['fechainicia']){
                                        $msjerror = $msjerror . ' La fecha de vacunación debe ser mayor a ' . $vacuna['fechainicia']->format('Y-m-d') . ', ';
                                        break;
                                    }
                                    elseif ($fechavacunacion->format('Ymd') > date('Ymd')){
                                        $msjerror = $msjerror . ' Fecha de vacuna inválida (debe ser menor o igual a la fecha actual) ,';
                                    }
                                }
                            }
                        }

                        foreach ($arrayEsquemas as $esquema){
                            $registro_valido = false;
                            if ($esquema['vacuna_id'] == $getvacuna_id){
                                if ($esquema['dosis'] == $dosis_aplicada){
                                    $arraycsv[$x]['esquemavacunacion_id'] = $esquema['id'];
                                    $arraycsv[$x]['vacunaobligatoria'] = $esquema['vacunaobligatoria'];
                                    $arraycsv[$x]['tiempoesperadosis'] = $esquema['tiempoesperadosis'];
                                    $arraycsv[$x]['tiempoesperadosisdias'] = $esquema['tiempoesperadosisdias'];
                                    $registro_valido = true;
                                    break;
                                }
                            }
                        }

                        if (!$registro_valido){
                            $msjerror = $msjerror . ' Dosis de vacuna inválida,';
                        }

                    }

                    if($num_iden == $iden_profesional_aplica && $num_iden){
                        $msjerror = $msjerror . ' El vacunador debe ser diferente al paciente vacunado,';
                    }

                    $registro_valido = false;
                    foreach ($arrayEntidades as $entidad){
                        if ($unicodigo_entidad === $entidad['id']){
                            $registro_valido = true;
                            break;
                        }
                    }

                    if (!$registro_valido){
                        $msjerror = $msjerror . ' Unicódigo inválido no existe en el PRAS,';
                    }

                    $registro_valido = false;
                    if ($pobla_vacuna){
                        foreach ($arrayCatGrupPobla as $grupPobla){
                            if ($utilremoveAccent->removeAccent($pobla_vacuna) === $utilremoveAccent->removeAccent(mb_strtoupper($grupPobla['descripcion'],'UTF-8'))){
                                $registro_valido = true;
                                break;
                            }
                        }
                    }
                    else{
                        $registro_valido = true;
                    }

                    if (!$registro_valido){
                        $msjerror = $msjerror . ' Grupo población inválido,';
                    }

                    $sfechanacimiento = $año_nacimiento . str_pad($mes_nacimiento,2,'0',STR_PAD_LEFT) . str_pad($dia_nacimiento,2,'0',STR_PAD_LEFT);
                    $result=$utilDateTime->validarFecha($sfechanacimiento, 'Ymd');
                    if(!$result){
                        $msjerror = $msjerror . ' Fecha de nacimiento paciente inválida, ';
                    }
                    else{
                        $fechanacimiento = date_create($sfechanacimiento);
                        if ($fechanacimiento->format('Ymd') >= date('Ymd')){
                            $msjerror = $msjerror . ' Fecha de nacimiento paciente inválida, ';
                        }
                    }

                    if($fase_vacuna < 0 || $fase_vacuna > 9){
                        $msjerror = $msjerror . ' Fase de vacunación inválida,';
                    }
                    else{
                        $arraycsv[$x]['fase_vacuna'] = $fase_vacuna;
                    }

                    if($lote_vacuna === '' || $lote_vacuna === 'POR DETERMINAR'){
                        $msjerror = $msjerror . ' Lote de vacuna inválido,';
                    }

                    if($correo_electronico){
                        if (!filter_var($correo_electronico, FILTER_VALIDATE_EMAIL)) {
                            $msjerror = $msjerror . ' Correo inválido,';
                        }
                    }

                    if($exteriordosisprevia){
                        $arraycsv[$x]['exteriordosisprevia'] = $exteriordosisprevia;
                        $exteriordosisfechaanio = empty($arraycsv[$x]['exteriordosisfechaanio']) ? '' : $arraycsv[$x]['exteriordosisfechaanio'];
                        $exteriordosisfechames = empty($arraycsv[$x]['exteriordosisfechames']) ? '' : $arraycsv[$x]['exteriordosisfechames'];
                        $exteriordosisfechadia = empty($arraycsv[$x]['exteriordosisfechadia']) ? '' : $arraycsv[$x]['exteriordosisfechadia'];
                        $exteriordosispais = empty($arraycsv[$x]['exteriordosispais']) ? '' : $arraycsv[$x]['exteriordosispais'];
                        $sexteriordosisfecha = $exteriordosisfechaanio . str_pad($exteriordosisfechames,2,'0',STR_PAD_LEFT) . str_pad($exteriordosisfechadia,2,'0',STR_PAD_LEFT);
                        $exteriordosislote = empty($arraycsv[$x]['exteriordosislote']) ? '' : $arraycsv[$x]['exteriordosislote'];
                        $result=$utilDateTime->validarFecha($sexteriordosisfecha, 'Ymd');
                        if(!$result){
                            $msjerror = $msjerror . ' Fecha de vacuna aplicada en el exterior inválida, ';
                        }
                        else{
                            $exteriordosisfecha = date_create($sexteriordosisfecha);
                            if ($exteriordosisfecha < date_create('2021-01-01')){
                                $msjerror = $msjerror . ' La fecha de vacunación en el exterior debe ser mayor a 2021-01-01, ';
                            }
                            elseif ($exteriordosisfecha->format('Ymd') > date('Ymd')){
                                $msjerror = $msjerror . ' Fecha de vacuna en el exterior inválida (debe ser menor o igual a la fecha actual) ,';
                            }
                            else{
                                $arraycsv[$x]['exteriordosisfecha'] = $exteriordosisfecha;
                            }
                        }
                        if (!$exteriordosispais){
                            $msjerror = $msjerror . ' El país de la vacuna en el exterior es obligatorio, ';
                        }
                        else{
                            $exteriordosispais = $managerPais->searchPais($arraycsv[$x]['exteriordosispais']);
                            if ($exteriordosispais){
                                $exteriordosispais_id = $exteriordosispais->getId();
                                $arraycsv[$x]['exteriordosispais_id'] = $exteriordosispais_id;
                            }
                            else{
                                $msjerror = $msjerror . ' País de la vacuna en el exterior inválido, ';
                            }
                        }
                        $arraycsv[$x]['exteriordosislote'] = $exteriordosislote;
                    }

                } catch (\Exception $exc) {
                    $msjerror = ($exc->getCode())?"No es posible validar el registro, verifique el formato del archivo. " : $exc->getMessage();
                    $arrayerrores[]=array("linea"=>$x,"identificacion"=>$num_iden,"error"=>$msjerror);
                    return $arrayerrores;
                }

                if($msjerror <> ''){
                    $arrayerrores[]=array("linea"=>$x+1,"identificacion"=>$num_iden,"error"=>$msjerror);
                }
            }
        }

        return array("errorsdata"=>$arrayerrores,"arraycsv"=>$arraycsv);
    }

    /**
     * Valida si la fecha y el sexo del PRAS coinciden con los registros del archivo csv
     * @param $arrayPersona Paciente a validar
     * @param $fechanacpaciente Fecha nacimiento paciente
     * @param @sexo  $fechanacpaciente Sexo paciente
     * @return string
     */
    public function validaFechaSexo($arrayPersona, $fechanacimiento, $sexo1){
        $msjerror="";
        $fechanacpaciente = $arrayPersona['fechanacimiento']->format('Y-m-d');
        $date1 = new \DateTime($fechanacimiento);
        $date2 = new \DateTime($fechanacpaciente);
        $sexo1 = ($sexo1 == 'HOMBRE') ? Constantes::CT_SEXO_HOMBRE : Constantes::CT_SEXO_MUJER;
        $sexo2 = $arrayPersona['ctsexo_id'];

        if(!($date1 == $date2) || !($sexo1 == $sexo2)){
            $msjerror = $msjerror . ' No coincide la fecha de nacimiento o el sexo,';
        }
        return $msjerror;
    }

    /**
     * Renombra el key de un arreglo
     * @param $obj Objeto
     * @return $array Arreglo con las nuevas keys
     */
    public function renombraKeyArray($obj){
        $array = (array) $obj;
        foreach ($array as $key=>$value) {
            $newKey = substr($key,strpos($key,"\x00",1)+1);
            $array[$newKey] = $value;
            unset($array[$key]);
        }
        return $array;
    }

    /**
     * Guarda los datos de la vacuna del Paciente en la Base de Datos
     * @param $arrayVacuna informacion de la vacuna
     * @throws \Exception
     * @return $status Retorna <true> si la informacion se guardo con exito
     */
    public function saveVacunaCarga($arrayVacuna, $userId)
    {
        $status = 0;
        $em=$this->getContainer()->get('doctrine.orm.entity_manager');

        try {
            $this->beginTransaction();
            $strFechaVacuna = $arrayVacuna['año_aplicacion'] . str_pad($arrayVacuna['mes_aplicacion'],2,'0',STR_PAD_LEFT) . str_pad($arrayVacuna['dia_aplicacion'],2,'0',STR_PAD_LEFT);
            $fechaVacuna = new \DateTime($strFechaVacuna);
            $fechaProximaVacuna = new \DateTime($strFechaVacuna);
            $agendado = (mb_strtoupper($arrayVacuna['paciente_agendado'],'UTF-8') == 'SI') ? 1 : 0;
            $exteriordosisprevia = (mb_strtoupper($arrayVacuna['exteriordosisprevia'],'UTF-8') == 'SI') ? 1 : 0;
            $dosisDias = $arrayVacuna['tiempoesperadosisdias'];
            $dosisMeses = $arrayVacuna['tiempoesperadosis'];
            if ( $dosisDias > 0 ||  $dosisMeses > 0){
                $fechaProximaVacuna->modify('+'.$dosisMeses.' month');
                $fechaProximaVacuna->modify('+'.$dosisDias.' days');
            }
            $objRegistroVacunacion = new Registrovacunacion;
            $objRegistroVacunacion->setEntidad($em->getReference('CoreAppBundle:Entidad', $arrayVacuna['unicodigo_entidad']));
            $objRegistroVacunacion->setPaciente($em->getReference('HCUEPacienteBundle:Paciente', $arrayVacuna['paciente_id']));
            $objRegistroVacunacion->setEsquemavacunacion($em->getReference('HCUEAtencionMedicBundle:Esquemavacunacion', $arrayVacuna['esquemavacunacion_id']));
            $objRegistroVacunacion->setEstadovacunacion(1);
            $objRegistroVacunacion->setInternaexterna(0);
            $objRegistroVacunacion->setFechavacunacion($fechaVacuna);
            $objRegistroVacunacion->setProximavacunacion($fechaProximaVacuna);
            $objRegistroVacunacion->setObservacion('Carga archivos.csv por front');
            $objRegistroVacunacion->setCtespecialidadmedica($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_ESPECIALIDAD_VACUNACION));
            $objRegistroVacunacion->setLote($arrayVacuna['lote_vacuna']);
            $objRegistroVacunacion->setEstado(Constantes::CT_ESTADOGENERALACTIVO);
            $objRegistroVacunacion->setActivo(Constantes::CT_ESTADOGENERALACTIVO);
            $objRegistroVacunacion->setUsuariocreacionId($userId);
            $objRegistroVacunacion->setVacunadorid($arrayVacuna['iden_profesional_aplica']);
            $objRegistroVacunacion->setVacunadornombre($arrayVacuna['nom_profesional_aplica']);
            $objRegistroVacunacion->setCttiporegistro($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_ORIGENID_VACUNA_PRAS));
            $objRegistroVacunacion->setPuntovacunaciondesc($arrayVacuna['punto_vacunacion']);
            $objRegistroVacunacion->setPuntovacunacionunicod($arrayVacuna['unicodigo_entidad']);
            $objRegistroVacunacion->setRegistrador($arrayVacuna['nom_profesional_registra']);
            $objRegistroVacunacion->setFasevacunacion($arrayVacuna['fase_vacuna']);
            $objRegistroVacunacion->setAgendado($agendado);
            $objRegistroVacunacion->setExteriordosisprevia($exteriordosisprevia);
            if ($exteriordosisprevia) {
                $objRegistroVacunacion->setExteriordosisfecha($arrayVacuna['exteriordosisfecha']);
                $objRegistroVacunacion->setExteriordosispais($em->getReference("CoreAppBundle:Pais",$arrayVacuna['exteriordosispais_id']));
                $objRegistroVacunacion->setExteriordosislote($arrayVacuna['exteriordosislote']);
            }
            $this->save($objRegistroVacunacion);
            $this->commit();
            $status = 1;
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }

        return $status;
    }
}
?>
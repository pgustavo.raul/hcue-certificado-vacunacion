<?php 
/*
* Esta clase ha sido creada por el generador de código fuente AppGen v3.0,
* requiere los siguientes bundle del Core <AppBundle,SeguridadBundle,GUIBundle> para su correcto funcionamiento
* @autor Luis Malquin
*/

namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Form\Type\CertificadovacunaType;

/**
* Clase de manipulación del repositorio Certificadovacuna
*/
class CertificadovacunaManager extends BaseManager{

	/**
        * Crea el objeto formulario para el modelo Certificadovacuna            
        * @param integer $id Id del registro Certificadovacuna     
        * @return formType  
        */
	public function createForm($id=0,$options = array()){
                $certificadovacuna= $this->create();
                if($id){
                    $certificadovacuna=  $this->find($id);
                    if (empty($certificadovacuna)) {                                         
                            throw new NotFoundHttpException("No se encontró un registro con id $id");
                    }       
                }
                return $this->getContainer()->get('form.factory')->create(CertificadovacunaType::class, $certificadovacuna,$options); 
        }

	/**
        * Inactiva o elimina un registro basado en el modelo Certificadovacuna            
        * @param integer $id Id del registro Certificadovacuna 
        * @param boolean $flush Indica si se realiza el flush en la persistencia   
        * @return boolean true si se realizó la operación con éxito  
        */
	public function deleteById($id,$flush=true) {         
            $certificadovacuna=  $this->find($id);
            if (!$certificadovacuna) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }         
            $certificadovacuna->setActivo(0); 
            $this->save($certificadovacuna,$flush);                
            
            //$this->delete($certificadovacuna); 
                
            return true;
         }

}
?>
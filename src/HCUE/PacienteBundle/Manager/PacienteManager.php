<?php

/**
 * Servicio para manejar los datos del Paciente
 * Descripcion de PacienteManager
 * @author Richard Veliz
 */

namespace HCUE\PacienteBundle\Manager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Paciente;
use Core\AppBundle\Manager\BaseManager;
use HCUE\PacienteBundle\Entity\Constantes;
use Core\AppBundle\Entity\Constantes as constant;
use HCUE\PacienteBundle\Form\Type\PacienteType;
use Symfony\Component\Validator\Constraints\DateTime;

class PacienteManager extends BaseManager
{
    /**
     * Crea el objeto formulario para su respectivo modelo
     * @param integer $id      Id del registro
     * @param  array  $options array de opciones
     * @return formType
     */
    public function createForm($id = 0, $options = array())
    {
        $modelo = $this->create();
        if ($id) {
            $modelo = $this->getPacienteById($id);
            if (empty($modelo)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }
        return $this->getContainer()->get('form.factory')
            ->create(PacienteType::class, $modelo, $options);
    }

    /**
     * Guarda los datos del Paciente en la Base de Datos
     * @param Paciente $objPaciente informacion del formulario de paciente
     * @param integer $numeroidentificacionanterior numero de identificación del anterior registro
     * @throws \Exception
     * @return boolean true si la informacion se guardo con exito
     */
    public function savePaciente(Paciente $objPaciente, Request $request, $numeroidentificacionanterior = "")
    {

        $this->beginTransaction();

        try {


            //Se registran los datos de la madre o representante del paciente
            if(isset($request->get('paciente')['madrepcte'])) {
                $madrepcteService = $this->getContainer()->get("hcue.paciente.service.madrepaciente");
                $objPaciente = $madrepcteService->saveMadrepcte($objPaciente, $request);
            }elseif (!$objPaciente->getMadrepcte()->getId()){
                $objPaciente->setMadrepcte(null);
            }



            $servicePersona = $this->getContainer()->get("core.app.entity.manager.persona");
            $servicelogicaPersona = $this->getContainer()->get("hcue.paciente.service.persona");
            $servicePaciente = $this->getContainer()->get("hcue.paciente.service.paciente");
            $managerInscripcicon = $this->getContainer()->get("hcue.paciente.entity.manager.inscripcion");

            $objPersona = $objPaciente->getPersona();
            $tipoidentificacion = ($objPersona->getCttipoidentificacion()) ? $objPersona->getCttipoidentificacion()->getId() : $objPersona->getCttipoidentificacionId();
            $secuencial = ($tipoidentificacion == Constantes::CT_TIPOIDENTCEDULA) ? 0 : $servicelogicaPersona->GenerarSecuencial($objPersona);

            $formato = 'd-m-Y';
            $fechanacimiento=\DateTime::createFromFormat($formato, $objPersona->getFechanacimiento());
            $fechanacimiento->setTime(0,0,0);
            $objPersona->setFechanacimiento($fechanacimiento);

            if ($tipoidentificacion == Constantes::CT_TIPOIDENTNOIDENTIFICADO) {
                $objPersona->setNumeroidentificacion($secuencial);
                $objPersona->setNoidentificado($secuencial);
            }
            $numerohistoriaclinica = ($tipoidentificacion == Constantes::CT_TIPOIDENTCEDULA) ? $objPersona->getNumeroidentificacion() : $secuencial;
            $servicePaciente->validarPaciente($objPersona, $numeroidentificacionanterior);
            $obj = $servicePersona->savePersona($objPersona,false);

            $objPaciente->setPersona($obj);
            $objPaciente->setNumerohistoriaclinica($numerohistoriaclinica);
            $status = 1;
            $this->save($objPaciente,false);

            $valorId=$objPaciente->getId();
            $managerInscripcicon->saveInscripcionPaciente($request->get('paciente')['entidad'],$valorId);
            $this->flushAllChanges();
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }

        return $status;
    }

    /**
     * Actualiza el campo activo a cero del registro de un Paciente en la Base de Datos
     * @param Paciente $objPaciente modelo paciente
     * @throws \Exception
     * @return boolean true si la informacion se actualizo con exito
     */
    public function eliminarPaciente(Paciente $objPaciente)
    {
        $status = 0;
        $this->beginTransaction();
        try {
            $persona_id = $objPaciente->getPersonaId();
            $servicePersona = $this->getContainer()->get("core.app.entity.manager.persona");
            $status = $servicePersona->deleteById($persona_id, false);
            $objPaciente->setActivo(0);
            $status = 1;
            $this->save($objPaciente);
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }
        return $status;
    }

    /**
     * Verifica si existe creado un Paciente con el mismo numero de idetificación
     * @param integer $numeroidentificacion numero de identificación del paciente
     * @return array("existe_persona"=>$existePersona,"persona_id"=>$persona_id,"existe_paciente"=>$existePaciente,"id"=>$paciente_id)
     */
    public function existePaciente($numeroidentificacion)
    {
        $existePersona = false;
        $existePaciente = false;
        $paciente_id = 0;
        $servicePersona = $this->getContainer()->get("core.app.entity.manager.persona");
        if ($servicePersona->existPersona($numeroidentificacion)) {
            $existePersona = true;
            $paciente = $this->getRepository()->getPacienteByIdentificacion($numeroidentificacion);
            if ($paciente) {
                $existePaciente = true;
                $paciente_id = $paciente->getId();
            }
        }
        return array(
            "existe_persona"  => $existePersona,
            "existe_paciente" => $existePaciente,
            "id"              => $paciente_id,
        );
    }

    /**
     * Obtiene el id del paciente por la cédula
     * @param string $cedula
     * @return integer
     */
    public function getIdporCedula($cedula)
    {
        return $this->getRepository()->getIdporCedula($cedula);
    }

    /**
     * Obtiene un listado de paciente filtrando por texto
     * @param string $texto
     * @return arrayresult
     */
    public function getFilterPaciente($texto)
    {
        return $this->getRepository()->getFilterPaciente($texto);
    }

    /**
     * Obtiene el Paciente por Id
     * @param integer $id Id del Paciente
     * @return interger $id
     */
    public function getPacienteById($id)
    {
        return $this->getRepository()->getPacienteById($id);
    }

    /**
     * Obtiene el historial del registro de paciente
     * @return arrayresult
     */
    public function getHistoryPaciente()
    {
        $repository = $this->getRepository();
        $result = array();
        $result[] = $repository->getPacienteRegisteredToday();
        $result[] = $repository->getPacienteRegisteredYesterday();
        $result[] = $repository->getPacienteRegisteredLastWeek();
        $result[] = $repository->getPacienteRegisteredLastMonth();
        $result[] = $repository->getPacienteAllRegistered();
        return $result;
    }

    /**
     * Obtiene un array normalizado de los datos del paciente por id
     * @param integer $id Id del paciente
     * @return arrayresult
     */
    public function getArrayPaciente($id)
    {
        $timeManager = $this->getContainer()->get('core.app.util.datetime');
        $paciente = $this->getPacienteById($id);
        $persona = $paciente->getPersona();
        $objparroquia=$paciente->getParroquia();
        $residencia="";
        if($objparroquia){
            $parroquia = $paciente->getParroquia()->getDescripcion();
            $canton = $paciente->getParroquia()->getCanton()->getDescripcion();
            $provincia = $paciente->getParroquia()->getCanton()->getProvincia()->getDescripcion();
            $residencia=$provincia . "/" . $canton . "/" . $parroquia;

        }

        $fechaNacimiento = $persona->getFechanacimiento();
        $edad = ($fechaNacimiento) ? $timeManager->tiempoEntreFechas($fechaNacimiento->format('Y-m-d')) : '';
        $rootDir = $this->getContainer()->get('kernel')->getRootDir();
        $fotobase64 = ($persona->getFotoId() > 0) ? $persona->getFoto()->getArchivo() : base64_encode(file_get_contents($rootDir . '/../web/bundles/coregui/app/images/fotodefault.png'));


        $result = array(
            "srcfoto"              => $fotobase64,
            "nombrecompleto"       => $persona->getNombrecompleto(),
            "numeroidentificacion" => $persona->getNumeroidentificacion(),
            "sexo"                 => ($persona->getCtsexo())?$persona->getCtsexo()->getDescripcion():null,
            "estadocivil"          => ($persona->getCtestadocivil())?$persona->getCtestadocivil()->getDescripcion():null,
            "nacionalidad"         => ($persona->getPais())?$persona->getPais()->getNacionalidad():null,
            "lugarnacimiento"      => $persona->getLugarnacimiento(),
            "residencia"           => $residencia,
            "direccion"            => $paciente->getBarrio(),
            "telefono"             => $paciente->getTelefono(),
            "celular"              => $paciente->getCelular(),
            "nombrecontacto"       => $paciente->getNombrecontacto(),
            "parentezco"           => ($paciente->getCtparentezcocontacto())?$paciente->getCtparentezcocontacto()->getDescripcion():"",
            "telefonocontacto"     => $paciente->getTelefonocontacto(),
            "numerorepresentante"  => $paciente->getNumerorepresentante(),
            "edad"                 => ($edad) ? $edad['anios'] . ' años,  ' . $edad['meses'] . ' meses,  ' . $edad['dias'] . ' días' : $edad,
        );

        return $result;
    }

    /**
     * Obtiene un array normalizado de los datos de las atenciones medicas del paciente por id
     * @param integer $id Id del paciente
     * @return arrayresult
     */
    public function getAtencionesMedicasByPaciente($id, $numMaxResults = 5)
    {
        $catalogoRepository = $this->getEntityManager()->getRepository('HCUEPacienteBundle:AtencionmedicaBasic');
        $result = $catalogoRepository->getAtencionmedicaByPaciente($id, $numMaxResults);

        return $result;
    }


    /**
     * Obtiene el QueryBuilder de la lista de catalogos identificados por un tipo de catálogo
     * @param integer $tipocatalogo_id identifica el tipo de catálogo
     * @return QueryBuilder
     */
    public function getCatalogoQueryBuilder($tipocatalogo_id)
    {   $repoCatalogo = $this->getEntityManager()->getRepository("CoreAppBundle:Catalogo");
        return $repoCatalogo->createQueryBuilder('ct')
            ->Where('ct.estado=1')
            ->andWhere('ct.tipocatalogo_id=:tipocatalogo_id')
            ->andWhere('ct.catalogo_id is null')
            ->orderBy(' ct.descripcion', 'ASC')
            ->setParameter('tipocatalogo_id', $tipocatalogo_id);
    }
     /**
     * Obtiene la lista de catalogos identificados por un tipo de catálogo
     * @param integer $tipocatalogo_id identifica el tipo de catálogo
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "Core\AppBundle\Entity\Catalogo"
     */
    public function getCatalogo($tipocatalogo_id, $arrayResult = false)
    {
        $queryBuilder = $this->getCatalogoQueryBuilder($tipocatalogo_id);
        $q = $queryBuilder->getQuery();
        $resultId = ($arrayResult) ? "result_array_catalogo_by_tipo_$tipocatalogo_id" : "result_catalogo_by_tipo_$tipocatalogo_id";
        $q->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }
    /**
     * Obtiene el QueryBuilder de la lista de catalogos identificados por el catálogo padre
     * @param integer $catalogo_id identifica el catálogo padre
     * @return QueryBuilder
     */
    public function getCatalogoChildrensQueryBuilder($catalogo_id)
    {   $repoCatalogo = $this->getEntityManager()->getRepository("CoreAppBundle:Catalogo");
        return $repoCatalogo ->createQueryBuilder('ct')
            ->Where('ct.estado=1')
            ->andWhere('ct.catalogo_id=:catalogo_id')
            ->orderBy(' ct.descripcion', 'ASC')
            ->setParameter('catalogo_id', $catalogo_id);
    }

    /**
     * Obtiene la lista de catalogos identificados por un catálogo padre
     * @param integer $catalogo_id identifica el catálogo padre
     * @param boolean $arrayResult <true> para retornar cada modelo o entidad de la lista como un array
     * @return Array Lista de Objetos de tipo "AppBundle\Entity\Catalogo"
     */
    public function getCatalogoChildrens($catalogo_id, $arrayResult = false)
    {
        $queryBuilder = $this->getCatalogoChildrensQueryBuilder($catalogo_id);
        $q = $queryBuilder->getQuery();
        $resultId = ($arrayResult) ? "result_array_children_by_catalogo_$catalogo_id" : "result_children_by_catalogo_$catalogo_id";
        $q->useQueryCache(true)
            ->useResultCache(true, null, $resultId);

        return ($arrayResult) ? $q->getArrayResult() : $q->getResult();
    }

    /**
     * Verifica si existe creado un Paciente con el mismo numero de idetificación
     * @param integer $numeroidentificacion numero de identificación del paciente
     * @return array("existe_persona"=>$existePersona,"persona_id"=>$persona_id,"existe_paciente"=>$existePaciente,"id"=>$paciente_id)
     */
    public function getPacienteByIdentificacion($numeroidentificacion,$fechanacimiento=null)
    {
        return  $this->getRepository()->getPacienteByIdentificacion($numeroidentificacion,$fechanacimiento);
    }

    /**
     * Guarda a personas y pacientes previo verificación si existe en las tablas del pras
     * @param array $arraypersona información del paciente
     */

    public function savePersonaPacienteCovid($arraypersona)
    {
        $status=false;
        $message='';

        $objpersona=$this->saveOrUpdatePersona($arraypersona);
        $status = $objpersona['status'];
        $message = $objpersona['message'];

        if ($status==true){
            $objpaciente=$this->savePacienteCovid($objpersona,$arraypersona);
            $status = $objpaciente['status'];
            $message = $objpaciente['message'];
        }
        return array('status'=>$status,'message'=>$message);
    }

    /**
     * Método que verifica y guarda si existe la persona
     * @param array $arraypersona información del paciente
     */

    private function saveOrUpdatePersona($arraypersona){
        $em=$this->getContainer()->get('doctrine.orm.entity_manager');
        $personaManager=$this->getContainer()->get('core.app.entity.manager.persona');
        $RegistroCService=$this->getContainer()->get('hcue.paciente.service.registrocivil');
        $usuario_id = Constantes::CT_USUARIO_REGISTRO_DEFECTO;
        $persona=$personaManager->findOneBy(array("numeroidentificacion"=>$arraypersona['identificacion']));

        $message='';
        $personaId='';
        try {
            if(!$persona){
                $persona=$personaManager->create();
                $tipoidentificacion=$arraypersona["cttipoidentificacion"];
                if($tipoidentificacion){
                    $persona->setCttipoidentificacion($em->getReference("CoreAppBundle:Catalogo",$tipoidentificacion));
                }
                $identificacion=$arraypersona["identificacion"];
                if($identificacion){
                    $persona->setNumeroidentificacion($identificacion);
                }
                $persona->setUsuariocreacionId($usuario_id);

            }
            $nombre = explode(" ", $arraypersona["nombre"]);
            $nombre = $RegistroCService->normalizarNombreCompleto($nombre);



            $apellidopaterno=$primernombre=$apellidomaterno=$segundonombre="";
            if(count($nombre)<=2){
                $apellidopaterno = array_key_exists(0, $nombre) ? $nombre[0] : "";
                $primernombre = array_key_exists(1, $nombre) ? $nombre[1] : "";
            }else{
                $apellidopaterno = array_key_exists(0, $nombre) ? $nombre[0] : "";
                $apellidomaterno = array_key_exists(1, $nombre) ? $nombre[1] : "";
                $primernombre = array_key_exists(2, $nombre) ? $nombre[2] : "";
                $segundonombre = array_key_exists(3, $nombre) ? $nombre[3] : "";
            }


            if($apellidopaterno){
                $persona->setApellidopaterno($apellidopaterno);
            }


            if($apellidomaterno){
                $persona->setApellidomaterno($apellidomaterno);
            }

            if($primernombre){
                $persona->setPrimernombre($primernombre);
            }

            if($segundonombre){
                $persona->setSegundonombre($segundonombre);
            }

            $persona->setNombrecompleto();


            $fechanacimiento=new \DateTime($arraypersona["fechanacimiento"]);
            if($fechanacimiento){
                $persona->setFechanacimiento($fechanacimiento);
            }


            $estadocivil=$arraypersona["ctestadocivil"];
            if($estadocivil){
                $persona->setCtestadocivil($em->getReference("CoreAppBundle:Catalogo",$estadocivil));
            }

            $persona->setEstado(1);


            $sexo=$arraypersona["ctsexo"];
            if($sexo){
                $persona->setCtsexo($em->getReference("CoreAppBundle:Catalogo",$sexo));
            }

            $personaManager->save($persona,false);
            $personaId=$persona;
            $status=true;
            $message='Persona guardado exitoso';


        } catch (Exception $exc) {
            $this->rollback();
            throw $exc;
            $status= false;
        }
        return array('status'=>$status,'message'=>$message, 'persona_id'=>$personaId);
    }

    /**
     * Método que verifica y guarda si existe el paciente
     * @param array $arraypaciente información del paciente
     */

    private function savePacienteCovid($objpersona,$arrayperson){
        $status= false;
        $message='';
        $arraypaciente=$objpersona['persona_id'];
        $em=$this->getContainer()->get('doctrine.orm.entity_manager');
        $paciente=$this->findOneBy(array('persona_id'=>$arraypaciente->getId(), 'estado'=>1,'activo'=>1));
        $managerInscripcicon = $this->getContainer()->get("hcue.paciente.entity.manager.inscripcion");
        $repoIncripcion =   $this->getEntityManager()->getRepository("HCUEInscripcionBundle:Inscripcion");
        try {
            if (!$paciente){
                $paciente=$this->create();
                $paciente->setPersona($em->getReference("CoreAppBundle:Persona",$arraypaciente->getId()));
                $paciente->setNumerohistoriaclinica($arraypaciente->getNumeroidentificacion());
                $paciente->setProfesion($em->getReference("CoreAppBundle:Profesion",constant::PROFESION_NINGUNA));
                $paciente->setCtestadoniveleducacion($em->getReference("CoreAppBundle:Catalogo",constant::CT_NIVEL_INSTRUCCION_SE_IGNORA));
                $paciente->setCttipobono($em->getReference("CoreAppBundle:Catalogo",constant::CT_TIPO_BONO_NINGUNO));
                $paciente->setCttipoempresatrabajo($em->getReference("CoreAppBundle:Catalogo",constant::CT_TIPO_EMPRESA_TRABAJO_NINGUNO));
                $paciente->setCtparentezcocontacto($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_PARENTESCO_CONTACTO_OTRO_FAMILIAR));
                $paciente->setCtdiscapacidad($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_WS_DISCAPACIDAD_NO_APLICA));
                $paciente->setUsuariocreacionId(constant::CT_USUARIO_REGISTRO_DEFECTO);
                $paciente->setCttiposegurosalud($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_SEGURO_NO_APORTA));
                $paciente->setCtetniaId($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_ETNIA_NO_SABE_RESPONDE));
                $paciente->setCalleprincipal("N/A");
                $paciente->setUnicpacienteid($em->getReference("CoreAppBundle:Persona",$arraypaciente->getId()));
            }
            $paciente->setCorreo($arrayperson['correo']);
            //envia a guardar parroquia
            $paciente->setParroquiaId($arrayperson['parroquia']);
            $this->save($paciente,false);
            //guarda la inscripción territorial
            $inscripcionPaciente = $repoIncripcion->getInscripcionByPacienteId($paciente->getId());
            $entidades_data = $managerInscripcicon->getEntidadesCercanasQueryBuilder($arrayperson['parroquia'],$arrayperson['canton'])->getQuery()->getResult();
            $managerInscripcicon->saveInscripcionPaciente($entidades_data[0]->getId(),$paciente->getId());
            $this->flushAllChanges();
            $this->commit();
            $status=true;
            $message='Paciente guardado exitoso';
        } catch (Exception $exc) {
            $this->rollback();
            throw $exc;
            $status= false;
        }
        return array('status'=>$status,'message'=>$message);


    }

    /**
     * Método que verifica y guarda el correo del paciente
     * @param Request $request
     */

    public function actualizacorreoPaciente($request){
        $status= false;
        $message='';
        try {
            $this->beginTransaction();
            $identificacion=$request->get('identificacion');
            $correo=$request->get('correo');
            if($identificacion){
                $paciente=$this->findOneBy(array('numerohistoriaclinica'=>$identificacion));
                if($paciente){
                    $paciente->setCorreo($correo);
                    $this->save($paciente);
                    $this->commit();
                    $status=true;
                    $message='Su correo se actualizado correctamente';
                }
            }

        } catch (Exception $exc) {
            $this->rollback();
            throw $exc;
            $status= false;
        }
        return array('status'=>$status,'message'=>$message);
    }

    /**
     * Guarda los datos del Paciente en la Base de Datos
     * @param Persona $objPersona informacion de la Persona
     * @param array Correo del paciente
     * @throws \Exception
     * @return $objPaciente Retorna el paciente creado
     */
    public function savePacienteCarga($objPersona, $auxPersona)
    {
        $em=$this->getContainer()->get('doctrine.orm.entity_manager');

        try {
            $this->beginTransaction();
            $servicelogicaPersona = $this->getContainer()->get("hcue.paciente.service.persona");
            $tipoidentificacion = ($objPersona->getCttipoidentificacion()) ? $objPersona->getCttipoidentificacion()->getId() : $objPersona->getCttipoidentificacionId();
            $secuencial = ($tipoidentificacion == Constantes::CT_TIPOIDENTCEDULA) ? 0 : $servicelogicaPersona->GenerarSecuencial($objPersona);
            $numerohistoriaclinica = ($tipoidentificacion == Constantes::CT_TIPOIDENTCEDULA) ? $objPersona->getNumeroidentificacion() : $secuencial;
            $objPaciente = $this->create();
            $objPaciente->setPersona($em->getReference("CoreAppBundle:Persona",$objPersona->getId()));
            $objPaciente->setNumerohistoriaclinica($numerohistoriaclinica);
            $objPaciente->setProfesion($em->getReference("CoreAppBundle:Profesion",Constantes::PROFESION_NINGUNA));
            $objPaciente->setCtestadoniveleducacion($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_NIVEL_INSTRUCCION_SE_IGNORA));
            $objPaciente->setCttipobono($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_TIPO_BONO_NINGUNO));
            $objPaciente->setCttipoempresatrabajo($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_TIPO_EMPRESA_TRABAJO_NINGUNO));
            $objPaciente->setCtparentezcocontacto($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_PARENTESCO_CONTACTO_OTRO_FAMILIAR));
            $objPaciente->setCtdiscapacidad($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_WS_DISCAPACIDAD_NO_APLICA));
            $objPaciente->setUsuariocreacionId(Constantes::CT_USUARIO_REGISTRO_DEFECTO);
            $objPaciente->setCttiposegurosalud($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_SEGURO_NO_APORTA));
            $objPaciente->setCtetniaId($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_ETNIA_NO_SABE_RESPONDE));
            $objPaciente->setCalleprincipal("N/A");
            $objPaciente->setCtorigen($em->getReference("CoreAppBundle:Catalogo",Constantes::CT_ORIGENID_VACUNA_PRAS));
            $objPaciente->setCorreo($auxPersona['correo']);
            $this->save($objPaciente);
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }

        return $objPaciente;
    }

    /**
     * Retorna los pacientes de acuerdo al arreglo de numero de identificacion
     * @param integer $arrayIdentificacion Arreglo de identificación de pacientes
     * @return array(pacientes)
     */
    public function getPacienteByArray($arraypersona_id, $arrayResult = false)
    {
        return  $this->getRepository()->getPacienteByArray($arraypersona_id, $arrayResult);
    }
}

<?php

/**
 * Manager para manejar los datos del paciente
 * Descripcion de InscripcionManager
 * @author Richard Veliz
 */
namespace HCUE\PacienteBundle\Manager;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Core\AppBundle\Manager\BaseManager;

class PacientecovidManager extends BaseManager
{
    /**
     * Obtiene los pacientes que aceptan la vacuna
     * @return interger $pacientecovid
     */
    public function getPersonaAceptaVacuna(){

            $pacientecovid = $this->findBy(array("aceptavacuna" => 1));

        return $pacientecovid;
    }


}
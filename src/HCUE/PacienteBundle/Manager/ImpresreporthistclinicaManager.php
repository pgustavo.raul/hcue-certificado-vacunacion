<?php
/**
 * Clase de manipulación del repositorio Impresreporthistclinica
 * @author Miguel Faubla
 */
namespace HCUE\PacienteBundle\Manager;

use Core\AppBundle\Manager\BaseManager;
use HCUE\PacienteBundle\Form\Type\ImpresreporthistclinicaType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HCUE\PacienteBundle\Entity\Constantes;

class ImpresreporthistclinicaManager extends BaseManager{

    /**
     * Crea el objeto formulario para el modelo Impresreporthistclinica
     * @param integer $id Id del registro Impresreporthistclinica
     * @return formType
     */
    public function createForm($id=0){
        $impresreporthistclinica = $this->create();
        if($id){
            $impresreporthistclinica = $this->find($id);
            if (empty($impresreporthistclinica)) {
                throw new NotFoundHttpException("No se encontró un registro con id $id");
            }
        }        
        return $this->getContainer()->get('form.factory')->create(ImpresreporthistclinicaType::class, $impresreporthistclinica);
    }

    /**
     * Guarda los datos de un formulario basado en un modelo Impresreporthistclinica
     * @param array $solicreporthistoriaclinica
     * @param integer $idsolicitud Id de la Solicitud
     **/
    public function saveImpresReportHistClinica($impresreporthistclinica = array(), $idsolicitud=0) {

        $solicreporthistoriaclinicaManager = $this->getContainer()->get('hcue.paciente.manager.solicreporthistoriaclinica');
        $solicreporthistoriaclinic = $solicreporthistoriaclinicaManager->find($idsolicitud);

        try {
            $this->beginTransaction();

            $impresreporthistclinicaObject = $this->create();
            (count($impresreporthistclinica) > 0) ? $impresreporthistclinicaObject->setMotivoreimpresion($impresreporthistclinica["motivoreimpresion"]) : null;
            $impresreporthistclinicaObject->setSolicreporthistoriacli($solicreporthistoriaclinic);
            $this->save($impresreporthistclinicaObject);
            $this->commit();
        } catch (\Exception $ex) {
            $this->rollBack();
            throw $ex;
        }
    }

}

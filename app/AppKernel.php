<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),            
            //Assetic
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),                          
            
            //JsRouting
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            //DataTableGrid
            new Sg\DatatablesBundle\SgDatatablesBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            
            //PDF
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),

           
            //Core aplicaciones
            new Core\GUIBundle\CoreGUIBundle(),
            new Core\AppBundle\CoreAppBundle(),
            new Core\SeguridadBundle\CoreSeguridadBundle(),
            
            //Registro de Paciente
            new HCUE\PacienteBundle\HCUEPacienteBundle(),

            //Registro de Atención Medica
            new HCUE\AtencionMedicBundle\HCUEAtencionMedicBundle(),
        );

        if (in_array($this->getEnvironment(), array('test','prod'), true)) {
            //redis
            $bundles[]=new Snc\RedisBundle\SncRedisBundle();
        }

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();            
        }

        return $bundles;
    }
    
    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }    
    
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}

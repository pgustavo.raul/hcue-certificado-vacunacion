<!---
@utor: Lenin Vallejos
Título del issue: Caso Prueba issue #XX: Breve descripción del issue que se va a probar
El caso de prueba debe estar relacionado con el requerimiento en la sección issues relacionados
Ejemplo: Caso Prueba issue #123: Incrementar el módulo de vacunación en  enfermería
Etiquetar el issue con el tag "CAso de prueba"
--->

<!--- ¡PSST! 
- Hay que dejar 4 o más espacios al final del texto para que no se junten las líneas 
- No se debe borrar los símbolos de check: * [ ] 
--->

###  CASO DE PRUEBA
<!--- Se debe iniciar con el verbo en infinitivo -->
**Ingresar una nueva vacuna en módulo de vacunación**

**PERFIL/es:** 

* Enfermería primer nivel de Atención
* Enfermería Rural 


### Pre- condiciones 

* [ ] 1. Funcionalidad versionada en ambiente de pruebas
* [ ] 2. Ingresar paciente en el módulo de admisión 
* [ ] 3. Parametrizar el esquema de vacunación en el módulo de Parametrización PRAS
* [ ] 4. Cargar el datawarehouse a la fecha actual 
* [ ] 5. Cargar el servidor de réplica 

<!--- OBLIGATORIO: Uno o varios casos «felices» en que el software hace lo que se supone debe hacer si no hay errores --->


#### Pasos a realizar
<!--- Se debe iniciar con el verbo en infinitivo -->

1. Ingresar al módulo de enfermería, sub mòdulo de vacunación, seleccionar la especialidad 
2. Visualizar la pantalla principal donde podemos seleccionar al paciente a vacunar 
3. Seleccionar la vacuna aplicada
4. Ingresar información del vacunador, fecha de vacunacion, fecha proxima vacuna 
5. Guardar la información ingresada haciendo click en el boton guardar


#### Resultado esperados
-----------------------------------------------------------------

##### Escenarios POSITIVOS

* [ ]  **PASO 3** - Si la vacuna fue por demanda expontánea se debe bloquear el campo punto de vacunación
* [ ]  **PASO 3** - La segunda dósis debe calcularse automaticamente
* [ ]  **PASO 5** - Al finalizar la acción se debe presentar en amarillo la vacuna aplicada

##### Escenarios NEGATIVOS
 
* [ ]  **PASO 3** - No debe permitir registrar la segunda dósis de la vacuna sin tener la dosis previa 
* [ ]  **PASO 5**- No debe permitir registrar caracteres especiales en el grupo prioritario  






-----------------------------------------------------------------

Realizado QA: @analista QA 1

Aprobado por: @ analista QA 2

Requirente: @funcional

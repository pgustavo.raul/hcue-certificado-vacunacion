# Planificación del Sprint 
Ayuda: [Wiki](https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/Calidad-de-las-Historias-de-Usuario)


* **Fecha:**
* **Objetivo:** Planificar y afinar las necesidades del cliente con la finalidad de priorizar los requerimientos a construir en esta iteracción 

## Tareas
*Requisitos necesarios que se debe cumplir en el afinamiento o planificación del sprint*
*  **Milestone:** – Se debe definir el milestone a trabajar (Sprint) 
*  **Tablero:** – Definir el tablero de trabajo Kanban y relacionarlo con el Milestone

## Tablero de trabajo 

*[Se adjuntar una captura del tablero de trabajo correspondiente al Sprint 



## Equipo de trabajo  (Scrum Team)
* Develoment Team: Roberto Hernandez
* Scrum Master: .....
* Product Owner: Lenin Vallejos
* Stakeholders: Eduardo Canar , Bryan Ayala


## Compromisos  

Los participantes descritos en el equipo de trabajo se comprometen a cumplir con la planificación descrita, en caso de exitir alguna modificación en el alcance de esta iteracción debe ser consensuada con todo el equipo 
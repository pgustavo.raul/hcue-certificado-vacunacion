# Descripción
Ayuda: [Wiki](https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/Calidad-de-las-Historias-de-Usuario)

**Como** un profesional médico del sistema de historia clínica,
**necesito** poder firmar las atenciones que he finalizado dentro del sistema PRAS,
**para** garantizar la legalidad de los documentos.

# Criterios de aceptación
* ...
* ...

*[Se puede adjuntar prototipos de pantallas, ejemplos, flujos de procesos, documentos normativos, etc. en caso de contar con los mismos y siempre que aporten al entendimiento del requerimiento. Ver ejemplo de flujo]*

## Dimensionamiento
* Concurrencia de usuarios (cantidad máxima de usuarios conectados al mismo tiempo)
* Usuarios: población total de usuarios que contendrá la aplicación
* Nivel de atención ojetivo
    * [ ] Primer Nivel 
    * [ ] Segundo Nivel 
    * [ ] Tercer Nivel 
    * [ ] Todo el pùblico
* Porcentaje de estimación de crecimiento anual de usuarios
* Número de registros que puede guardar un usuario diariamente
* Interactua con sistemas o entidades externas (Especifique..)


## Listo para Trabajar (DoR)
*A ser llenado durante la reunión de afinamiento o planificación del sprint*
* [ ] **I**ndependiente – se puede trabajar sin depender de otros requerimientos
* [ ] **N**egociable – puede ser modificada sin perder su objetivo
* [ ] **V**aliosa – debe agregar valor, el beneficio es claro
* [ ] **E**stimable – se pudo ponderar en la planificación
* [ ] **S**imple – tan pequeña para caber en el sprint
* [ ] **T**esteable – se puede probar, los criterios de aceptación son claros



## Tareas (Sprint backlog)
* [ ] ...
* [ ] ...
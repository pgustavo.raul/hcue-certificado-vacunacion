<!---
Título del MR → «PROD-Sistema-Issue XX: Brevísima descripción»
Ejemplo → «PROD-PRA-Issue 999: HC de Cardiología en AMED»

Códigos de sistemas: https://git.msp.gob.ec/ProSalud/smt/blob/master/codigos.md
--->

## Objetivo del cambio

- Describir el objetivo del cambio.

### OTRS:
- Nro Ticket - 📎Adjuntar ticket.pdf 📎Adjuntar requerimiento.pdf

### PRERREQUISITOS P1:

- [ ] Usuarios, concurrencia, proyección.
- [ ] Código fuente
- [ ] Arquitectura
- [ ] Casos de prueba definidos/actualizados por el desarrollador.
- [ ] Escenarios de Pruebas Unitarias del desarrollador.
- [ ] Resultados de Pruebas Unitarias del desarrollador.
- [ ] La lista de archivos modificados por el desarrollador.

📎Adjuntar Prerrequisitos P1.pdf

### PLAN DE PRUEBAS P2:

- [ ] Antecedentes
- [ ] Objetivos
- [ ] Justificación de la necesidad de pruebas de QA/QC/Seguridades
- [ ] Descripción del plan de pruebas y herramientas
- [ ] Responsables
- [ ] Escenario de pruebas QA/QC/Seguridades
- [ ] Calendario
- [ ] Especificar el tipo de propiedades que se quieren probar (Corrección, robustez, fiabilidad, usabilidad, etc).
- [ ] Especificar en qué consiste la prueba (hasta el último detalle de cómo se ejecutará).
- [ ] Dejar en claro cómo se mide el resultado.
- [ ] Definir cuál es el resultado que se espera (identificación, tolerancia)
- [ ] ¿Cómo se decide que el resultado es acorde con lo esperado?
- [ ] Calendario de pruebas

📎Adjuntar Plan de Pruebas P2.pdf

### Historias de Usuario
- Proyecto#Issue (Ej. PRY.2017.05#998)
- Proyecto#Issue (Ej. PRY.2017.05#999)

## Ejecución

### Ventana de Mantenimiento

- Fecha y hora inicio: dd/mm/aaaa hh:mm
- Fecha y hora fin estimado: dd/mm/aaaa hh:mm

📎Adjuntar formato de ventana de mantenimiento (horas tentativas).

### Instrucciones de Respaldo

- N/A

### Instrucciones Infraestructura

- Aceptar el Merge Request del presente proyecto.
    
### Instrucciones Base de Datos

- Ejecutar los scripts adjuntos en la instancia de base de datos Oracle RDAC.

📎Adjuntar scripts...
   
### Instrucciones de Recuperación/Rollback

BDD

- Ejecutar los scripts de rollback adjuntos en la instancia de base de datos Oracle RDAC.

📎Adjuntar scripts...

### Instrucciones de Post-implementación

- Crear el siguiente TAG relacionado a este versionamiento:
    * TAG: v1.0.0
    * Describir funcionalidad incorporada en el versionamiento.

📎Adjuntar scripts...

## Criterios de Aceptación

* Merge Request - Paso a Test:
    * Link

### Aprobado GIIS

- [ ] Prerrequerimientos
- [ ] Plan de Pruebas
- [ ] Memornado de paso a producción (📎Adjuntar memorando.pdf)
- [ ] Cumple con la documentación técnica (metodología) (Link Documentación Técnica)

*NOTA: En caso de no cumplir con algo, el responsable de la GIIS debe poner la justificación como comentario*

### Aprobado GISC

- [ ] Prerrequerimientos
- [ ] Plan de Pruebas
- [ ] Informe de Calidad (QA/QC). (📎Adjuntar y verificar el informe de QA/QC)
- [ ] Informe de Seguridad (📎 Generar y cargar el reporte técnico de pruebas de seguridad)

*NOTA: En caso de no cumplir, el RESATI debe poner la exepción con una justificación o negar el MR según corresponda, como un comentario*

### Aprobado GIIRSC

- [ ] Validación y aprobación de ventana de mantenimiento

* Cuenta con los insumos requeridos para promoverlo al ambiente de producción

### Aprobado GISTC

- [ ] Validación y envío de ventana de mantenimiento

* 📎Adjuntar la ventana de mantenimiento.pfd

*NOTA: Las referencias o evidencias deben ser subidas por quien genera el MR. En caso de no cumplir con algo, el responsable de la GIIS debe poner la justificación como comentario*

[wiki]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas
[PU]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-unitarias
[PI]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-de-integraci%C3%B3n
[metodología]: https://git.msp.gob.ec/ProSalud/aot/wikis/Metodolog%C3%ADa-Software

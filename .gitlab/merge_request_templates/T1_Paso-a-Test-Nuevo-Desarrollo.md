<!---
Título del MR → «TEST-Sistema-Issue XX: Brevísima descripción»
Ejemplo → «TEST-PRA-Issue 999: HC de Cardiología en AMED»

Códigos de sistemas: https://git.msp.gob.ec/ProSalud/smt/blob/master/codigos.md
--->

## *Prerrequisitos T1:

- [ ] Acta de constitución del proyecto.
- [ ] Alcance del proyecto.
- [ ] Usuarios, concurrencia, proyección.
- [ ] Código fuente.
- [ ] Arquitectura
- [ ] Consideraciones de infraestructura tecnológica.
- [ ] Casos de pruebas unitarias definidos/actualizados por el desarrollador.
- [ ] Escenarios de pruebas unitarias del desarrollador.
- [ ] Resultados de pruebas unitarias del desarrollador.
- [ ] Lista de archivos modificados por el desarrollador.

📎Adjuntar documento de Prerrequisitos_T1.pdf

### Historias de usuario:

- Proyecto#Issue (Ej. PRY.2017.05#998).
- Proyecto#Issue (Ej. PRY.2017.05#999).

### Objetivo del desarrollo

- Describir el objetivo del cambio.

### Sistema OTRS:
- Nro Ticket - 📎Adjuntar ticket.pdf

### Requerimiento:
- Requerimiento - 📎Adjuntar requerimiento.pdf

## *Plan de pruebas T2:

- [ ] Antecedentes.
- [ ] Objetivos.
- [ ] Justificación de la necesidad de pruebas de QA/QC/Seguridades.
- [ ] Descripción del plan de pruebas y herramientas.
- [ ] Responsables.
- [ ] Escenario de pruebas QA/QC/Seguridades.
- [ ] Calendario.
- [ ] Especificar el tipo de propiedades que se quieren probar (corrección, robustez, fiabilidad, usabilidad, etc.).
- [ ] Especificar en qué consiste la prueba (hasta el último detalle de cómo se ejecutará).
- [ ] Dejar en claro cómo se mide el resultado.
- [ ] Definir cuál es el resultado que se espera (identificación, tolerancia).
- [ ] ¿Cómo se decide que el resultado es acorde con lo esperado?.

📎Adjuntar documento de Plan_de_pruebas_T2.pdf

## *Ejecución

### Instrucciones de respaldo

- De existir, incluir instrucciones de respaldo.

📎Adjuntar scripts

### Instrucciones de infraestructura

- Analizar y aceptar el Merge Request del presente proyecto.

📎Adjuntar scripts
    
### Instrucciones de base de datos

- Ejecutar los scripts adjuntos en la instancia de base de datos pertinente.

📎Adjuntar scripts

* De existir procedimientos de bdd extensos, deben ser gestionados con al menos 12 horas de anticipación.

### Instrucciones de aplicación

1. Ingresar al servidor de aplicaciones del módulo pertinente del ambiente de pruebas.
2. Obtener la actualización del repositorio Git mediante un pull de la rama “test”.
3. Borrar el almacén de estructura de datos del servidor de pruebas.
4. Borrar la caché de la aplicación del framework, las variables "test y prod" del ambiente de Test.
5. Asignar permisos de lectura y escritura.
6. Iniciar el servidor.
7. Sincronizar hora de servidores.
8. Verificar el funcionamiento del módulo.
    
### Instrucciones de recuperación/Rollback

BDD

- Ejecutar los scripts de Rollback adjuntos en la instancia de base de datos pertinente.

📎Adjuntar scripts

APP

1. Ingresar al servidor de aplicaciones del módulo pertinente del ambiente de pruebas.
2. Listar los archivos (.log) y seleccionar el anterior commit funcional del Git.
3. Realizar un git reset al commit seleccionado.
4. Actualizar el código en el repositorio pertinente con el rollback.

📎Adjuntar scripts

### Instrucciones de post-implementación

- De existir, incluir instrucciones de post-implementación.

📎Adjuntar scripts

## *Criterios de aceptación

### Aprobado GIIS/GIBD

- [ ] Prerrequerimientos T1
    
### Aprobado GISC

- [ ] Plan/Criterio de pruebas T2.
- [ ] Informe/Criterio de Calidad (QA/QC). (📎Verificar y adjuntar el informe de QA/QC).
- [ ] Reporte/Criterio de Seguridad (📎Generar y cargar el reporte técnico de pruebas de seguridad).

### Aprobado GIIRSC

* Cuenta con los insumos y criterios técnicos requeridos para promoverlo al ambiente de pruebas.
- [ ] Aprobación 

### Ejecución GIIRSC

* Existió registro de ejecución exitosa de procedimientos

- [ ] Aprobación 

### Aprobación funcional de cambio GIIS/GIBD

- [ ] Aprobación técnica del requerimiento (GIIS).
- [ ] Aprobación funcional del requerimiento (Área Funcional).

*NOTA: Las referencias o evidencias deben ser subidas por quien genera el Merge Request. En caso de no cumplir algún criterio, el responsable de la GIIS debe justificarlo mediante el comentario correspondiente*

[wiki]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas
[PU]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-unitarias
[PI]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-de-integraci%C3%B3n
[metodología]: https://git.msp.gob.ec/ProSalud/aot/wikis/Metodolog%C3%ADa-Software

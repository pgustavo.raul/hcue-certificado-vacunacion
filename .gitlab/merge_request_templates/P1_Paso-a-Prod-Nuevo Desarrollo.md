<!---
Título del MR → «PROD-Sistema-Issue XX: Brevísima descripción»
Ejemplo → «PROD-PRA-Issue 999: HC de Cardiología en AMED»

Códigos de sistemas: https://git.msp.gob.ec/ProSalud/smt/blob/master/codigos.md
--->

## *Prerrequisitos P1:

- [ ] Acta de constitución del proyecto.
- [ ] Alcance del proyecto.
- [ ] Usuarios, concurrencia, proyección.
- [ ] Código fuente
- [ ] Arquitectura
- [ ] Consideración de Infraestructura tecnológica
- [ ] Casos de pruebas unitarias definidos/actualizados por el desarrollador.
- [ ] Escenarios de Pruebas Unitarias del desarrollador.
- [ ] Resultados de Pruebas Unitarias del desarrollador.
- [ ] La lista de archivos modificados por el desarrollador.

📎Adjuntar documento de Prerrequisitos_P1.pdf

### Historias de usuario

- Proyecto#Issue (Ej. PRY.2017.05#998)
- Proyecto#Issue (Ej. PRY.2017.05#999)

### Objetivo del cambio

- Describir el objetivo del cambio.

### Sistema OTRS:
- Nro Ticket - 📎Adjuntar ticket.pdf 📎Adjuntar requerimiento.pdf

## *Plan de pruebas P2:

- [ ] Antecedentes.
- [ ] Objetivos.
- [ ] Justificación de la necesidad de pruebas de QA/QC/Seguridades.
- [ ] Descripción del plan de pruebas y herramientas.
- [ ] Responsables.
- [ ] Escenario de pruebas QA/QC/Seguridades.
- [ ] Calendario.
- [ ] Especificar el tipo de propiedades que se quieren probar (Corrección, robustez, fiabilidad, usabilidad, etc.)
- [ ] Especificar en qué consiste la prueba (hasta el último detalle de cómo se ejecutará).
- [ ] Dejar en claro cómo se mide el resultado.
- [ ] Definir cuál es el resultado que se espera (identificación, tolerancia).
- [ ] ¿Cómo se decide que el resultado es acorde con lo esperado?.

📎Adjuntar documento de Plan_de_pruebas_P2.pdf

## *Ejecución

### Propuesta de ventana de mantenimiento (GIIS/GIBD)

- Fecha y hora inicio: dd/mm/aaaa

- Rango de hora de inicio: 
- [ ] Mañana
- [ ] Tarde
- [ ] Noche

- Fecha y hora fin estimado: dd/mm/aaaa 
- [ ] Mañana
- [ ] Tarde
- [ ] Noche

📎Adjuntar formato de ventana de mantenimiento (horas tentativas acorde a P1 y P2).

### Instrucciones de respaldo

- De existir, incluir instrucciones de respaldo.

📎Adjuntar scripts

### Instrucciones de infraestructura

- Analizar y aceptar el Merge Request del presente proyecto.
    
### Instrucciones de base de datos

- Ejecutar los scripts adjuntos en la instancia de base de datos pertinente.

📎Adjuntar scripts

* De existir procedimientos de bdd extensos, deben ser gestionados con al menos 12 horas de anticipación

### Instrucciones de aplicación

1. Ingresar al servidor de aplicaciones del módulo pertinente del ambiente de pruebas.
2. Obtener la actualización del repositorio Git mediante un pull de la rama “test”.
3. Borrar el almacén de estructura de datos del servidor de pruebas.
4. Borrar la caché de la aplicación del framework, las variables "test y prod" del ambiente de Test.
5. Asignar permisos de lectura y escritura.
6. Iniciar el servidor.
7. Sincronizar hora de servidores.
8. Verificar el funcionamiento del módulo.
    
### Instrucciones de recuperación/Rollback

BDD

- Ejecutar los scripts de Rollback adjuntos en la instancia de base de datos pertinente.

📎Adjuntar scripts

APP

1. Ingresar al servidor de aplicaciones del módulo pertinente del ambiente de pruebas.
2. Listar los archivos (.log) y seleccionar el anterior commit funcional del Git.
3. Realizar un git reset al commit seleccionado.
4. Actualizar el código en el repositorio pertinente con el rollback.

📎Adjuntar scripts

### Instrucciones de post-implementación

- Crear el siguiente TAG relacionado a este versionamiento:
    * TAG: v1.0.0
    * Describir funcionalidad incorporada en el versionamiento.

📎Adjuntar scripts

## Criterios de aceptación

* Merge Request - Paso a Test:
    * Link

### Aprobado GIIS/GIBD

- [ ] Prerrequisitos P1.
- [ ] Memorando de paso a producción (📎Adjuntar memorando.pdf).
- [ ] Gestión de ventana de mantenimiento.

*NOTA: En caso de no cumplir con algo, el responsable de la GIIS debe poner la justificación como comentario*

### Aprobado GISC

- [ ] Plan/Criterio de pruebas P2.
- [ ] Informe/Criterio de Calidad (QA/QC). (📎Adjuntar y verificar el informe de QA/QC).
- [ ] Reporte/Criterio de Seguridad (📎 Generar y cargar el reporte técnico de pruebas de seguridad).

*NOTA: En caso de no cumplir, el RESATI debe poner la exepción con una justificación o negar el MR según corresponda, como un comentario*

### Aprobado GISTC

- [ ] Envío de ventana de mantenimiento

* 📎Adjuntar la ventana de mantenimiento.pfd

### Aprobado GIIRSC

* Cuenta con los insumos requeridos para promoverlo al ambiente de producción.

- [ ] Aprobación técnica.
- [ ] Validación y aprobación de ventana de mantenimiento.

### Ejecución GIIRSC

* Existió registro de ejecución exitosa de procedimientos.

- [ ] Ejecución exitosa

### Aprobación funcional de cambio GIIS/GIBD

- [ ] Aprobación técnica del requerimiento (GIIS).
- [ ] Aprobación funcional del requerimiento (Área Funcional).


*NOTA: Las referencias o evidencias deben ser subidas por quien genera el Merge Request. En caso de no cumplir algún criterio, el responsable de la GIIS debe justificarlo mediante el comentario correspondiente*

[wiki]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas
[PU]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-unitarias
[PI]: https://git.msp.gob.ec/ProSalud/smt/wikis/mejores-pr%C3%A1cticas/pruebas#pruebas-de-integraci%C3%B3n
[metodología]: https://git.msp.gob.ec/ProSalud/aot/wikis/Metodolog%C3%ADa-Software
